/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ARM_H
#define	_ARM_H

#include <system.h>
#include <build.h>

#ifdef ARCH_arm
#define ARM9_INTEGRATOR_UART0_ADDR 	0x16000000
#define ARM9_INTEGRATOR_UART1_ADDR 	0x17000000

#define ARM_PL011_UARTDR 		0x0
#define ARM_PL011_UARTECR		0x04
#define ARM_PL011_UARTFR 		0x18

#define ARM_PL011_UARTFLAG_RXFE		0x10
#define ARM_PL011_UARTFLAG_TXFE		0x80
#endif

#endif

