/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifndef ARCH_i386
#undef CONFIG_DRV_RTL8029
#endif

#ifdef CONFIG_DRV_RTL8029

#include <system.h>
#include <string.h>
#include <arch/io.h>
#include <pci.h>
#include <task.h>
#include <net/eth.h>
#include <net/net.h>

/* PCI probe IDs */
#define RTL8029_VENDORID	0x10ec
#define RTL8029_DEVICEID	0x8029

/*
 * RTL 8029 read registers PAGE0
 */
#define RTL8029_CR		0x00
#define RTL8029_CLDA0		0x01
#define RTL8029_CLDA1		0x02
#define RTL8029_BNRY		0x03
#define RTL8029_TSR		0x04
#define RTL8029_NCR		0x05
#define RTL8029_FIFO		0x06
#define RTL8029_ISR		0x07
#define RTL8029_CRDA0		0x08
#define RTL8029_CRDA1		0x09
#define RTL8029_RSR		0x0c
#define RTL8029_CNTR0		0x0d
#define RTL8029_CNTR1		0x0e
#define RTL8029_CNTR2		0x0f

#define RTL8029_DMADATA	0x10
#define RTL8029_RESET		0x1f

/*
 * RTL 8029 write registers PAGE0
 */
#define RTL8029_PSTART		0x01
#define RTL8029_PSTOP		0x02
#define RTL8029_TPSR		0x04
#define RTL8029_TBCR0		0x05
#define RTL8029_TBCR1		0x06
#define RTL8029_RSAR0		0x08
#define RTL8029_RSAR1		0x09
#define RTL8029_RBCR0		0x0a
#define RTL8029_RBCR1		0x0b
#define RTL8029_RCR		0x0c
#define RTL8029_TCR		0x0d
#define RTL8029_DCR		0x0e
#define RTL8029_IMR		0x0f

/*
 * RTL 8029 CR bits
 */
#define CR_STP			0x01
#define CR_STA			0x02
#define CR_TXP			0x04
#define CR_READ_DMA		0x08
#define CR_WRITE_DMA		0x10
#define CR_ABORT_DMA		0x20
#define CR_PAGE0		0x00
#define CR_PAGE1		0x40
#define CR_PAGE2		0x80

/*
 * RTL 8029 ISR bits
 */
#define ISR_PRX			0x01
#define ISR_PTX			0x02
#define ISR_PRXE		0x04
#define ISR_PTXE		0x08
#define ISR_OVW			0x10
#define ISR_CNT			0x20
#define ISR_RDC			0x40
#define ISR_RESET		0x80


/*
 * RTL 8029 IMR bits
 */
#define IMR_PRXE		0x01
#define IMR_PTXE		0x02
#define IMR_RXEE		0x04
#define IMR_TXEE		0x08
#define IMR_OVWE		0x10
#define IMR_CNTE		0x20
#define IMR_RDCE		0x40

/*
 * RTL 8029 RCR bits
 */
#define RCR_BROADCAST		0x04
#define RCR_MULTICAST		0x08
#define RCR_PROMISC		0x10


/*
 * RTL 8029 read registers PAGE1
 */

#define RTL8029_CURR		0x07 

/*
 * RTL 8029 read registers PAGE1
 */

#define RTL8029_CURR		0x07 

/*
 * RTL8029 transmit pages
 * 12 pages = 2 ethernet frames max
 * 6 pages  = 1 ethernet frame max
 */ 
#define RTL8029_TX_PAGES	12

/*
 * some defines
 */
#define NET_RECV_MODE_BROADCAST	0x01
#define NET_RECV_MODE_MULTICAST	0x02
#define NET_RECV_MODE_PROMISC	0x04
#define RTL8029_MIN_FRB_LEN	63
#define RTL8029_DEBUG		0


/* rtl8029 device structure */
struct rtl8029_dev_t {
	unsigned char irq;
	pcidev_t *pcidev;
	unsigned short addr_io;
	mac_addr_t addr_mac;

    	unsigned start_page;
        unsigned end_page;
        unsigned tx_start_page;
	unsigned rx_start_page;
    	unsigned tx_busy[1];
};

struct rtl8029_dev_t *rtl8029_dev;


/* I/O operations */
unsigned char rtl8029_read8 (struct rtl8029_dev_t *dev, unsigned short port)
{
	return inb (dev->addr_io + port);
}

void rtl8029_write8 (struct rtl8029_dev_t *dev, unsigned short port, unsigned char val)
{
	outb (dev->addr_io + port, val);
}

unsigned short rtl8029_read16 (struct rtl8029_dev_t *dev, unsigned short port)
{
	return inw (dev->addr_io + port);
}

unsigned rtl8029_read32 (struct rtl8029_dev_t *dev, unsigned short port)
{
	return inl (dev->addr_io + port);
}

void rtl8029_write32 (struct rtl8029_dev_t *dev, unsigned short port, unsigned val)
{
	outl (dev->addr_io + port, val);
}


/* detect rtl8029 device in PC */
pcidev_t *rtl8029_detect ()
{
	/* First detect network card - is connected to PCI bus ?*/
	pcidev_t *pcidev = pcidev_find (RTL8029_VENDORID, RTL8029_DEVICEID);

	if (!pcidev)
		return 0;

	return pcidev;
}

/* reset rtl8029 device */
unsigned rtl8029_reset (struct rtl8029_dev_t *dev)
{
	unsigned char ret = rtl8029_read8 (dev, RTL8029_RESET);
	rtl8029_write8 (dev, RTL8029_RESET, ret);

	while (!(rtl8029_read8 (dev, RTL8029_ISR) & ISR_RESET)) {
		kprintf ("waiting for rtl8029 reset :)\n");
	}

	rtl8029_write8 (dev, RTL8029_ISR, 0xff);

	return 1;
}

/* set mode of packet receiving */
unsigned rtl8029_recv_mode (struct rtl8029_dev_t *dev, int recv_mode)
{
	unsigned rtl8029_rcr = 0;
	
	if (recv_mode & NET_RECV_MODE_BROADCAST)
		rtl8029_rcr |= RCR_BROADCAST;
	
	if (recv_mode & NET_RECV_MODE_MULTICAST)
		rtl8029_rcr |= RCR_MULTICAST;
	
	if (recv_mode & NET_RECV_MODE_PROMISC)
		rtl8029_rcr |= RCR_PROMISC;
	
	
	rtl8029_write8 (dev, RTL8029_RCR, rtl8029_rcr);
	
	return 1;
}


/* nic interrupt handler */
void rtl8029_int ()
{
	struct rtl8029_dev_t *dev = rtl8029_dev;

	kprintf ("rtl8029 -> interrupt (0x%x)\n", 1);
}

/* INIT sequence */
unsigned init_rtl8029 ()
{
	unsigned i = 0;

	pcidev_t *pcidev = rtl8029_detect ();

	if (!pcidev)
		return 0;

	struct rtl8029_dev_t *dev = (struct rtl8029_dev_t *) kmalloc (sizeof (struct rtl8029_dev_t));

	if (!dev)
		return 0;

	/* get irq id */
	dev->irq = pcidev->u.h0.interrupt_line;
	dev->addr_io = pcidev->u.h0.base_registers[0];

	rtl8029_write8 (dev, RTL8029_CR, CR_PAGE0 | CR_ABORT_DMA | CR_STP);

	/* reset device now */
	rtl8029_reset (dev);

	rtl8029_write8 (dev, RTL8029_DCR, 0x40 | 0x08 | 0x01);
	rtl8029_write8 (dev, RTL8029_IMR, 0x00);
	rtl8029_write8 (dev, RTL8029_ISR, 0xff);
	
	rtl8029_write8 (dev, RTL8029_RCR, 0x00);
	rtl8029_write8 (dev, RTL8029_TCR, 0x00);

	rtl8029_write8 (dev, RTL8029_RBCR0, 0x20);
	rtl8029_write8 (dev, RTL8029_RBCR1, 0x00);
	rtl8029_write8 (dev, RTL8029_RSAR0, 0x00);
	rtl8029_write8 (dev, RTL8029_RSAR1, 0x00);
	rtl8029_write8 (dev, RTL8029_CR, CR_READ_DMA | CR_STA);
	
/*
    outb(CR_PAGE0 | CR_ABORT_DMA | CR_STP, rtl_iobase + RTL8029_CR);
    rtl_8029_reset(pcidev);

    outb(0x40 | 0x08 | 0x01, rtl_iobase + RTL8029_DCR);
    outb(0x00, rtl_iobase + RTL8029_IMR);
    outb(0xff, rtl_iobase + RTL8029_ISR);
//	outb(0x20, rtl_iobase + RTL8029_RCR);    
    outb(0x00, rtl_iobase + RTL8029_RCR);    
    outb(0x00, rtl_iobase + RTL8029_TCR);    
//        outb(0x02, rtl_iobase + RTL8029_TCR);    
    outb(0x20, rtl_iobase + RTL8029_RBCR0);
    outb(0x00, rtl_iobase + RTL8029_RBCR1);
    outb(0x00, rtl_iobase + RTL8029_RSAR0);
    outb(0x00, rtl_iobase + RTL8029_RSAR1);
    outb(CR_READ_DMA | CR_STA, rtl_iobase + RTL8029_CR);
*/
	// read eeprom
	char rtl_eeprom[32];
	for (i = 0; i < 32; i++)
		rtl_eeprom[i] = rtl8029_read8 (dev, RTL8029_DMADATA);

	irq_install_handler (dev->irq, rtl8029_int);

	kprintf ("rtl8029 irq: %d\nrtl8029 mac: %x:%x:%x:%x:%x:%x\n", dev->irq, rtl_eeprom[0], rtl_eeprom[1], rtl_eeprom[2], rtl_eeprom[3], rtl_eeprom[4], rtl_eeprom[5]);

	rtl8029_dev = dev;

	rtl8029_write8 (dev, RTL8029_DCR, 0x40 | 0x08 | 0x01);

	rtl8029_write8 (dev, RTL8029_CR, CR_PAGE1 | CR_ABORT_DMA | CR_STP);

   	// get mac address from ethernet
	for (i = 0; i < 6; i++) {
		dev->addr_mac[i] = rtl_eeprom[i];
		rtl8029_write8 (dev, 0x01 + i, rtl_eeprom[i]);
	}

	rtl8029_write8 (dev, RTL8029_CURR, dev->rx_start_page);

	rtl8029_write8 (dev, RTL8029_CR, CR_PAGE0 | CR_ABORT_DMA | CR_STP);
	rtl8029_write8 (dev, RTL8029_DCR, 0x40 | 0x08 | 0x01);
	rtl8029_write8 (dev, RTL8029_RBCR0, 0x00);
	rtl8029_write8 (dev, RTL8029_RBCR1, 0x00);

	rtl8029_write8 (dev, RTL8029_RCR, 0x00);
	rtl8029_write8 (dev, RTL8029_TCR, 0x00);
	rtl8029_write8 (dev, RTL8029_TPSR, dev->tx_start_page);
	rtl8029_write8 (dev, RTL8029_PSTART, dev->rx_start_page);
	rtl8029_write8 (dev, RTL8029_PSTOP, dev->end_page);
	rtl8029_write8 (dev, RTL8029_BNRY, dev->end_page - 1);
	rtl8029_write8 (dev, RTL8029_ISR, 0xff);
	rtl8029_write8 (dev, RTL8029_IMR, 0x00);
/*
    outb(0x40 | 0x08 | 0x01, rtl_iobase + RTL8029_DCR);

    outb(CR_PAGE1 | CR_ABORT_DMA | CR_STP, rtl_iobase + RTL8029_CR);

    // setting mac address in PAR
    for (i = 0; i < 6; i++) {
	rtl_8029_netdev.if_layer_2.if_addr[i] = rtl_eprom[i];
	outb(rtl_eprom[i], rtl_iobase + 0x01 + i);
    }
    rtl_8029_netdev.if_layer_2.if_addr_len = NET_LAYER_2_ETH_MAC_LEN;

    outb(rtl_8029_netdev_param.rx_start_page, rtl_iobase + RTL8029_CURR);

    printk("%s hwaddr=%02x:%02x:%02x:%02x:%02x:%02x\n", rtl_8029_netdev.if_name,
	    rtl_eprom[0], rtl_eprom[1], rtl_eprom[2], rtl_eprom[3], rtl_eprom[4], rtl_eprom[5]);

    outb(CR_PAGE0 | CR_ABORT_DMA | CR_STP, rtl_iobase + RTL8029_CR);
    outb(0x40 | 0x08 | 0x01, rtl_iobase + RTL8029_DCR);
    outb(0x00, rtl_iobase + RTL8029_RBCR0);
    outb(0x00, rtl_iobase + RTL8029_RBCR1);
//	outb(0x20, rtl_iobase + RTL8029_RCR);    
    outb(0x00, rtl_iobase + RTL8029_RCR);    
//      outb(0x02, rtl_iobase + RTL8029_TCR);   
    outb(0x00, rtl_iobase + RTL8029_TCR);   
    outb(rtl_8029_netdev_param.tx_start_page, rtl_iobase + RTL8029_TPSR); 
    outb(rtl_8029_netdev_param.rx_start_page, rtl_iobase + RTL8029_PSTART);
    outb(rtl_8029_netdev_param.end_page, rtl_iobase + RTL8029_PSTOP);
    outb(rtl_8029_netdev_param.end_page - 1, rtl_iobase + RTL8029_BNRY);
    outb(0xff, rtl_iobase + RTL8029_ISR);
    outb(0x00, rtl_iobase + RTL8029_IMR);*/


	rtl8029_write8 (dev, RTL8029_IMR, IMR_PRXE | IMR_PTXE | IMR_RXEE | IMR_TXEE | IMR_OVWE | IMR_CNTE);
	rtl8029_write8 (dev, RTL8029_ISR, 0xff);

	rtl8029_write8 (dev, RTL8029_CR, CR_PAGE0 | CR_ABORT_DMA | CR_STA);

	rtl8029_write8 (dev, RTL8029_TCR, 0x00);

	rtl8029_recv_mode (dev, NET_RECV_MODE_BROADCAST);
/*
    //
     // enable packet receive irq, packet transmitted irq
     // receive error irq, transmit error irq, overwrite irq, counter overflow irq
     ///   
    outb(IMR_PRXE | IMR_PTXE | IMR_RXEE | IMR_TXEE | IMR_OVWE | IMR_CNTE, rtl_iobase + RTL8029_IMR);
    outb(0xff, rtl_iobase + RTL8029_ISR);

    outb(CR_PAGE0 | CR_ABORT_DMA | CR_STA, rtl_iobase + RTL8029_CR);

    ///
     // normal transmit mode
     ///
    outb(0x00, rtl_iobase + RTL8029_TCR);
    
    ///
     // accept broadcasts
     ///
    

*/
	//timer_wait (1);

	netdev_t *ifdev = netdev_create (dev->addr_mac, 0, 0, dev->addr_io);

	if (!ifdev)
		return 0;

	return 0;//pcnet32_start (dev);
}

bool rtl8029_acthandler (unsigned act, char *block, unsigned block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			return init_rtl8029 ();
		}
		break;
		case DEV_ACT_READ:
		{

			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{

			return 1;
		}
		break;
	}

	return 0;
}
#endif

