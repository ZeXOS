/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "net.h"
#include "xmpp.h"

/** XML tags */

char *xml_arg_handler (char *buf, unsigned len, char *arg, unsigned *arg_len)
{
	int arg_start = -1;
	int arg_end = -1;
	unsigned l = *arg_len;
	
	unsigned i;
	for (i = 0; i < len; i ++) {
		if (arg_start == -1) {
			if (buf[i] == ' ')
				arg_start = i + 1;
//			for (; buf[i+1] == ' '; i ++);
		} else if (arg_end == -1) {
			if (buf[i] == ' ')
				arg_end = i - 1;
			else if ((i + 1) == len)
				arg_end = i;
		}
		
		if (arg_start != -1 && arg_end != -1) {
			if (!strncmp (buf + arg_start, arg, *arg_len)) {
				*arg_len = arg_end - arg_start - l - 1;
				return buf + arg_start + l + 1;
			}
			
			arg_start = -1;
			arg_end = -1;
			i --;
		}
	}
  
	return 0;
}

char *xml_value_handler (char *val, unsigned *l)
{
	char *buf = val + *l + 1;
	
	int val_start = -1;
	int val_end = -1;

	unsigned len = strlen (buf);
	
	unsigned i;
	for (i = 0; i < len; i ++) {
		if (val_start == -1) {
			if (buf[i] == '<')
				val_start = i + 1;
//			for (; buf[i+1] == ' '; i ++);
		} else if (val_end == -1)
			if (buf[i] == '>')
				val_end = i;
			
		if (val_start != -1 && val_end != -1) {
			if (buf[val_start - 1] == '<' && 
			    buf[val_start] == '/' &&
			    !strncmp (buf + val_start + 1, val, *l) && 
			    buf[*l + val_start + 1] == '>') {
				*l = val_start - 1;
				return buf;
			}
			
			val_start = -1;
			val_end = -1;
			i --;
		}
	}
	
	return 0;
}

int xml_tag_message (char *buf, unsigned len)
{
	unsigned l = 5;
	
	char *arg = xml_arg_handler (buf, len, "from=", &l);
  
	if (!arg)
		return -1;
  
	char *body = strstr (buf, "<body>");
	
	if (!body)
		return 0;
	
	unsigned l2 = 4;
	
	char *val = xml_value_handler (body+1, &l2);
	
	if (!val)
		return -1;
	
	xmpp_message_from (arg, l, val, l2);
	
	return 0;
}

int xml_tag_stream (char *buf, unsigned len)
{
	unsigned l = 3;
	
	char *arg = xml_arg_handler (buf, len, "id=", &l);
  
	if (!arg)
		return -1;

	return xmpp_session_set (arg, l);
}

int xml_tag_mechanism (char *buf, unsigned len)
{
	unsigned l = len;
	
	char *val = xml_value_handler (buf, &l);
	
	if (!val)
		return -1;

	xmpp_mechanism_set (val, l);
	
	return 0;
}

int xml_tag_success ()
{
	printf ("XMPP -> login successfull\n");
	return 0;
}

int xml_tag_failure ()
{
	printf ("XMPP -> unknown failure\n");
	return -1;
}

int xml_tag (char *buf, unsigned len)
{
	if (!strncmp (buf, "message", 7))
		return xml_tag_message (buf, len);
	if (!strncmp (buf, "?xml", 4))
		return 0;
	if (!strncmp (buf, "stream:stream", 13))
		return xml_tag_stream (buf, len);
	if (!strncmp (buf, "mechanisms", 10))
		return 0;
	if (!strncmp (buf, "mechanism", 9))
		return xml_tag_mechanism (buf, len);
	if (!strncmp (buf, "success", 7))
		return xml_tag_success ();
	if (!strncmp (buf, "failure", 7))
		return xml_tag_failure ();
	
	return 0;
}

int xml_handler (char *buf, unsigned len)
{
	int tag_start = -1;
	int tag_end = -1;

	unsigned i;
	for (i = 0; i < len; i ++) {
		if (tag_start == -1) {
			if (buf[i] == '<')
				tag_start = i + 1;
		} else if (tag_end == -1)
			if (buf[i] == '>')
				tag_end = i;
			
		if (tag_start != -1 && tag_end != -1) {
			if (!xml_tag (buf + tag_start, tag_end - tag_start) == -1)
				return -1;
			
			tag_start = -1;
			tag_end = -1;
		}
	}
  
	return 0;
}
