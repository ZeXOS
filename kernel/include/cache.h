/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _CACHE_H
#define _CACHE_H

#define CACHE_MAGIC	0xe9

typedef struct {
	unsigned char magic;
	unsigned char prealloc;
	unsigned limit;
	unsigned curr;

	char data;
} cache_t;

extern unsigned int init_cache ();
extern cache_t *cache_read ();
extern cache_t *cache_add (char *buf, unsigned len);
extern cache_t *cache_create (char *buf, unsigned len, unsigned char prealloc);
extern int cache_close (cache_t *cache);
extern int cache_closebyptr (void *ptr);

#endif 
