/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _APPCL_H
#define _APPCL_H

#include "bitmap.h"

#define APPCL_STATE_REDRAW		0x1
#define APPCL_STATE_RECVOK		0x2
#define APPCL_STATE_EXIT		0x4
#define APPCL_STATE_RESIZE		0x8
#define APPCL_STATE_LBUTTON		0x10
#define APPCL_STATE_RBUTTON		0x20
#define APPCL_STATE_MBUTTON		0x40

typedef struct {
	unsigned state;
	unsigned short x;
	unsigned short y;
	unsigned short sizex;
	unsigned short sizey;
	short mousex;
	short mousey;
	unsigned char kbd;
} __attribute__ ((__packed__)) appcl_t;

extern appcl_t *zgui_window ();
extern void zgui_puts (unsigned short x, unsigned short y, char *str, unsigned color);
extern int zgui_button (unsigned short x, unsigned short y, char *str);
extern int zgui_resize (unsigned short x, unsigned short y);
extern unsigned zgui_event ();
extern int zgui_init ();
extern int zgui_exit ();

#endif
