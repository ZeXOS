/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Martin 'povik' Poviser (martin.povik@gmail.com)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _UNISTD_H
#define	_UNISTD_H

#include <sys/types.h>

#define F_DUPFD		0x0	/* dup */
#define F_GETFD		0x1	/* get close_on_exec */
#define F_GETFL 	0x3
#define F_SETFL 	0x4

/* access */
#define F_OK		0x0
#define X_OK		0x1
#define W_OK		0x2
#define R_OK		0x4

/* lseek */
#define SEEK_SET	0x0
#define SEEK_CUR	0x1
#define SEEK_END	0x2

#define STDIN_FILENO    0x0
#define STDOUT_FILENO   0x1
#define STDERR_FILENO   0x2

extern unsigned int sleep (unsigned int s);
extern unsigned int usleep (unsigned int s);
extern int dup (int fildes);
extern int dup2 (int fildes, int fildes2);
extern off_t lseek (int fd, off_t offset, int whence);
extern int unlink (const char *pathname);
extern pid_t getpid ();
extern int pipe (int fds[2]);
extern int chdir (const char *path);
extern char *getcwd (char *buf, size_t size);
extern int access (const char *pathname, int mode);

#endif 
