/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


double sqrt (double arg)
{
	/*double x, temp;
	int exp;
	int i;

	if (arg <= 0.)
		return (0.);

	x = frexp (arg, &exp);

	while (x < 0.5) {
		x *= 2;
		exp --;
	}

	if (exp & 1) {
		x *= 2;
		exp--;
	}

	temp = 0.5 * (1.0 + x);

	while (exp > 60) {
		temp *= (1L << 30);
		exp -= 60;
	}

	while (exp < -60) {
		temp /= (1L << 30);
		exp += 60;
	}

	if (exp >= 0)
		temp *= 1L << (exp/2);
	else
		temp /= 1L << (-exp/2);

	for (i = 0; i <= 4; i ++)
		temp = 0.5 * (temp + arg / temp);*/

	return __builtin_sqrt(arg);
}
