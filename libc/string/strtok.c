/*
 *  ZeX/OS
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h>

static char *_strtok_tmp = 0;

char *strtok (char *str, const char *delim)
{
	char *b;
	char *send;

	b  = str ? str : _strtok_tmp;

	if (!b)
		return NULL;

	b += strspn (b, delim);
	
	if (*b == '\0') {
		_strtok_tmp = 0;
		return 0;
	}

	send = strpbrk (b, delim);
	
	if (send && *send != '\0')
		*send ++ = '\0';
	
	_strtok_tmp = send;

	return b;
}
