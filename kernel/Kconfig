# For a description of the syntax of this configuration file,
# see http://lxr.linux.no/source/Documentation/kbuild/kconfig-language.txt.

menu "User interface"

config UI_KBD_LAYOUT
	string "Keyboard layout"
	default "us"
	help
	  Keyboard layout used in shell.

config UI_SHELL_INITCMD
	string "Shell init command"
	default ""
	help
	  Command, which is started when shell is spawned.

config UI_CONSOLELOG
	int "Console log"
	default 32
	help
	  Limit of logged commands in console log.

config UI_CONSOLESERIAL
	bool "Serial console (RS232)"
	default n
	help
	  Enable serial console input/output - usefully on embedded devices.

endmenu

menu "Memory management"

config MEM_PAGING
	bool "Paging"
	default y
	help
          Paging is a transfer of pages between main memory and an auxiliary store, such as hard disk drive. Paging is an important part of virtual memory implementation in most contemporary general-purpose operating systems, allowing them to easily use disk storage for data that does not fit into physical RAM.

config MEM_ZALLOC
	bool "ZAlloc (EXPERIMENTAL)"
	default n
	help
          New kernel memory allocator/deallocator - malloc/free/realloc written by ZeXx86

config MEM_DLALLOC
	bool "DLAlloc (EXPERIMENTAL)"
	default n
	help
          Version of malloc/free/realloc written by Doug Lea

endmenu

menu "Networking"

config HOSTNAME
	string "Hostname"
	default "zexos"
	help
	  Unique network name of current host

config PROTO_IPV4
	bool "IPv4 protocol"
	default y
	help
	  IPv4 is a data-oriented protocol to be used on a packet switched internetwork (e.g., Ethernet).

config PROTO_IPV6
	bool "IPv6 protocol"
	default y
	help
          Internet Protocol version 6 (IPv6) is an Internet Layer protocol for packet-switched internetworks. It is designated as the successor of IPv4, the first and still dominant version of the Internet Protocol, for general use on the Internet.

config PROTO_TUN6
	bool "(tun6) Tunnel broker protocol"
	default y
	help
          This provide tunnel, where IPv6 is tunneled directly inside IPv4 by having the protocol field set to '41' (IPv6) in the IPv4 packet.

endmenu

menu "Compatibility"

config COMPAT_QEMU
	bool "Qemu (x86)"
	default n
	help
	  Provide compatibility for Qemu emulated machines - scheduler optimalization, etc.

endmenu

source "drivers/Kconfig"

