/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifndef ARCH_i386
#undef CONFIG_DRV_RTL8169
#endif

#ifdef CONFIG_DRV_RTL8169

#include <system.h>
#include <string.h>
#include <arch/io.h>
#include <pci.h>
#include <task.h>
#include <net/eth.h>
#include <net/net.h>

/* PCI probe IDs */
#define RTL8169_VENDORID		0x10ec
#define RTL8169_DEVICEID		0x8169
#define RTL8169_DEVICEID2		0x8168

/* RTL8169 register definitions */
#define RTL8169_IDR0			0x00		/* Mac address */
#define RTL8169_MAR0			0x08		/* Multicast filter */
#define RTL8169_TXSTATUS0		0x10		/* Transmit status (4 32bit regs) */
#define RTL8169_TXSTATUS1		0x14		/* Transmit status (4 32bit regs) */
#define RTL8169_TXSTATUS2		0x18		/* Transmit status (4 32bit regs) */
#define RTL8169_TXSTATUS3		0x1c		/* Transmit status (4 32bit regs) */
#define RTL8169_TXADDR0			0x20		/* Tx descriptors (also 4 32bit) */
#define RTL8169_TXADDR1			0x24		/* Tx descriptors (also 4 32bit) */
#define RTL8169_TXADDR2			0x28		/* Tx descriptors (also 4 32bit) */
#define RTL8169_TXADDR3			0x2c		/* Tx descriptors (also 4 32bit) */
#define RTL8169_RXBUF			0x30		/* Receive buffer start address */
#define RTL8169_RXEARLYCNT		0x34		/* Early Rx byte count */
#define RTL8169_RXEARLYSTATUS		0x36		/* Early Rx status */
#define RTL8169_CHIPCMD			0x37		/* Command register */
#define RTL8169_RXBUFTAIL		0x38		/* Current address of packet read (queue tail) */
#define RTL8169_RXBUFHEAD		0x3A		/* Current buffer address (queue head) */
#define RTL8169_INTRMASK		0x3C		/* Interrupt mask */
#define RTL8169_INTRSTATUS		0x3E		/* Interrupt status */
#define RTL8169_TXCONFIG		0x40		/* Tx config */
#define RTL8169_RXCONFIG		0x44		/* Rx config */
#define RTL8169_TIMER			0x48		/* A general purpose counter */
#define RTL8169_RXMISSED		0x4C		/* 24 bits valid, write clears */
#define RTL8169_CFG9346			0x50		/* 93C46 command register */
#define RTL8169_CONFIG0			0x51		/* Configuration reg 0 */
#define RTL8169_CONFIG1			0x52		/* Configuration reg 1 */
#define RTL8169_TIMERINT		0x58		/* Timer interrupt register (32 bits) */
#define RTL8169_MEDIASTATUS		0x58		/* Media status register */
#define RTL8169_CONFIG3			0x59		/* Config register 3 */
#define RTL8169_CONFIG4			0x5A		/* Config register 4 */
#define RTL8169_HLTCLK			0x5B		/* Halted Clock */
#define RTL8169_MULTIINTR		0x5C		/* Multiple interrupt select */
#define RTL8169_MII_TSAD		0x60		/* Transmit status of all descriptors (16 bits) */
#define RTL8169_MII_BMCR		0x62		/* Basic Mode Control Register (16 bits) */
#define RTL8169_MII_BMSR		0x64		/* Basic Mode Status Register (16 bits) */
#define RTL8169_AS_ADVERT		0x66		/* Auto-negotiation advertisement reg (16 bits) */
#define RTL8169_AS_LPAR			0x68		/* Auto-negotiation link partner reg (16 bits) */
#define RTL8169_AS_EXPANSION		0x6A		/* Auto-negotiation expansion reg (16 bits) */
#define RTL8169_CCR			0xE0		/*  */
#define RTL8169_TNPDS_LOW		0x20
#define RTL8169_TNPDS_HIGH		0x24
#define RTL8169_THPDS_LOW		0x28
#define RTL8169_THPDS_HIGH		0x2C
#define RTL8169_RDSAR_LOW		0xE4
#define RTL8169_RDSAR_HIGH		0xE8
#define RTL8169_RMS			0xDA
#define RTL8169_MTPS			0xEC

/* RTL8169 command bits; or these together and write teh resulting value
   into CHIPCMD to execute it. */
#define RTL8169_CMD_RESET		(1<<4)
#define RTL8169_CMD_RX_ENABLE		0x08
#define RTL8169_CMD_TX_ENABLE		0x04
#define RTL8169_CMD_RX_BUF_EMPTY 	0x01

/* RTL8169 interrupt status bits */
#define RTL8169_INT_SYSERR		(1<<15)		/* PCI Bus error */
#define RTL8169_INT_TIMEOUT		(1<<14)		/* Set when TCTR reaches TimerInt value */
#define RTL8169_INT_SWINT		(1<<8)
#define RTL8169_INT_TX_UNDERRUN		(1<<7)		/* Packet underrun */
#define RTL8169_INT_RX_OVERFLOW		(1<<6)		/* Rx BUFFER overflow */
#define RTL8169_INT_LINKCHG		(1<<5)		/* Link change */
#define RTL8169_INT_RX_UNDERRUN		(1<<4)		/* Packet underrun */
#define RTL8169_INT_TX_ERR		(1<<3)
#define RTL8169_INT_TX_OK		(1<<2)
#define RTL8169_INT_RX_ERR		(1<<1)
#define RTL8169_INT_RX_OK		(1<<0)

/* RTL8169 transmit status bits */
#define RTL8169_TX_CARRIER_LOST		0x80000000	/* Carrier sense lost */
#define RTL8169_TX_ABORTED		0x40000000	/* Transmission aborted */
#define RTL8169_TX_OUT_OF_WINDOW	0x20000000	/* Out of window collision */
#define RTL8169_TX_CARRIER_HBEAT 	0x10000000  	/* not sure */ 
#define RTL8169_TX_STATUS_OK		0x00008000	/* Status ok: a good packet was transmitted */
#define RTL8169_TX_UNDERRUN		0x00004000	/* Transmit FIFO underrun */
#define RTL8169_TX_HOST_OWNS		0x00002000	/* Set to 1 when DMA operation is completed */
#define RTL8169_TX_SIZE_MASK		0x00001fff	/* Descriptor size mask */

#define RTL8169_TX_IFG0			0x1000000
#define RTL8169_TX_IFG1			0x2000000
#define RTL8169_TX_MXDMA2		0x400
#define RTL8169_TX_MXDMA1		0x200

/* RTL8169 receive status bits */
#define RTL8169_RX_MULTICAST		0x00008000	/* Multicast packet */
#define RTL8169_RX_PAM			0x00004000	/* Physical address matched */
#define RTL8169_RX_BROADCAST		0x00002000	/* Broadcast address matched */
#define RTL8169_RX_BAD_SYMBOL		0x00000020	/* Invalid symbol in 100TX packet */
#define RTL8169_RX_RUNT			0x00000010	/* Packet size is <64 bytes */
#define RTL8169_RX_TOO_LONG		0x00000008	/* Packet size is >4K bytes */
#define RTL8169_RX_CRC_ERR		0x00000004	/* CRC error */
#define RTL8169_RX_FRAME_ALIGN		0x00000002	/* Frame alignment error */
#define RTL8169_RX_STATUS_OK		0x00000001	/* Status ok: a good packet was received */

#define RTL8169_RX_RBLEN0		0x800
#define RTL8169_RX_MXDMA2		0x400
#define RTL8169_RX_MXDMA1		0x200
#define RTL8169_RX_AB			0x8
#define RTL8169_RX_AM			0x4
#define RTL8169_RX_APM			0x2

/* CONFIG */
#define RTL8169_CFG9346_LOCK		0x00
#define RTL8169_CFG9346_UNLOCK		0xc0

/* RTL8169 descriptors length in bytes */
#define RTL8169_DESC_LEN 16

/* RTL8169 descriptors settings */
#define RTL8169_TX_DESC 64
#define RTL8169_RX_DESC 256

#define BUFSIZE_PER_FRAME 2048

#define RXDESC(r, num) ((dev)->rxdesc[num])
#define TXDESC(r, num) ((dev)->txdesc[num])
#define RXDESC_PHYS(r, num) ((dev)->rxdesc_phys + (num) * sizeof(rtl8169_rx_desc))
#define TXDESC_PHYS(r, num) ((dev)->txdesc_phys + (num) * sizeof(rtl8169_tx_desc))
#define RXBUF(r, num) (&(dev)->rxbuf[(num) * BUFSIZE_PER_FRAME])
#define TXBUF(r, num) (&(dev)->txbuf[(num) * BUFSIZE_PER_FRAME])

enum mac_version {
	RTL_GIGA_MAC_VER_01 = 0x01, // 8169
	RTL_GIGA_MAC_VER_02 = 0x02, // 8169S
	RTL_GIGA_MAC_VER_03 = 0x03, // 8110S
	RTL_GIGA_MAC_VER_04 = 0x04, // 8169SB
	RTL_GIGA_MAC_VER_05 = 0x05, // 8110SCd
	RTL_GIGA_MAC_VER_06 = 0x06, // 8110SCe
	RTL_GIGA_MAC_VER_11 = 0x0b, // 8168Bb
	RTL_GIGA_MAC_VER_12 = 0x0c, // 8168Be
	RTL_GIGA_MAC_VER_13 = 0x0d, // 8101Eb
	RTL_GIGA_MAC_VER_14 = 0x0e, // 8101 ?
	RTL_GIGA_MAC_VER_15 = 0x0f, // 8101 ?
	RTL_GIGA_MAC_VER_16 = 0x11, // 8101Ec
	RTL_GIGA_MAC_VER_17 = 0x10, // 8168Bf
	RTL_GIGA_MAC_VER_18 = 0x12, // 8168CP
	RTL_GIGA_MAC_VER_19 = 0x13, // 8168C
	RTL_GIGA_MAC_VER_20 = 0x14  // 8168C
};

#define _R(NAME,MAC,MASK) \
	{ .name = NAME, .mac_version = MAC, .RxConfigMask = MASK }

static const struct {
	const char *name;
	unsigned char mac_version;
	unsigned RxConfigMask;	/* Clears the bits supported by this chip */
} rtl_chip_info[] = {
	_R("RTL8169",		RTL_GIGA_MAC_VER_01, 0xff7e1880), // 8169
	_R("RTL8169s",		RTL_GIGA_MAC_VER_02, 0xff7e1880), // 8169S
	_R("RTL8110s",		RTL_GIGA_MAC_VER_03, 0xff7e1880), // 8110S
	_R("RTL8169sb/8110sb",	RTL_GIGA_MAC_VER_04, 0xff7e1880), // 8169SB
	_R("RTL8169sc/8110sc",	RTL_GIGA_MAC_VER_05, 0xff7e1880), // 8110SCd
	_R("RTL8169sc/8110sc",	RTL_GIGA_MAC_VER_06, 0xff7e1880), // 8110SCe
	_R("RTL8168b/8111b",	RTL_GIGA_MAC_VER_11, 0xff7e1880), // PCI-E
	_R("RTL8168b/8111b",	RTL_GIGA_MAC_VER_12, 0xff7e1880), // PCI-E
	_R("RTL8101e",		RTL_GIGA_MAC_VER_13, 0xff7e1880), // PCI-E 8139
	_R("RTL8100e",		RTL_GIGA_MAC_VER_14, 0xff7e1880), // PCI-E 8139
	_R("RTL8100e",		RTL_GIGA_MAC_VER_15, 0xff7e1880), // PCI-E 8139
	_R("RTL8168b/8111b",	RTL_GIGA_MAC_VER_17, 0xff7e1880), // PCI-E
	_R("RTL8101e",		RTL_GIGA_MAC_VER_16, 0xff7e1880), // PCI-E
	_R("RTL8168cp/8111cp",	RTL_GIGA_MAC_VER_18, 0xff7e1880), // PCI-E
	_R("RTL8168c/8111c",	RTL_GIGA_MAC_VER_19, 0xff7e1880), // PCI-E
	_R("RTL8168c/8111c",	RTL_GIGA_MAC_VER_20, 0xff7e1880)  // PCI-E
};
#undef _R

enum cfg_version {
	RTL_CFG_0 = 0x00,
	RTL_CFG_1,
	RTL_CFG_2
};

struct rtl8169_tx_desc {
	unsigned short frame_len;
	unsigned short flags;
	unsigned vlan;
	unsigned tx_buffer_low;
	unsigned tx_buffer_high;
};

struct rtl8169_rx_desc {
	union {
		unsigned short buffer_size; /* rx command */
		unsigned short frame_len;   /* rx status - top 2 bits are checksum offload flags */
	};
	unsigned short flags;
	unsigned vlan;
	unsigned rx_buffer_low;
	unsigned rx_buffer_high;
};

/* descriptor bits */
#define RTL_DESC_OWN (1<<15)
#define RTL_DESC_EOR (1<<14)
#define RTL_DESC_FS  (1<<13)
#define RTL_DESC_LS  (1<<12)

/* rtl8169 device structure */
struct rtl8169_dev_t {
	unsigned char irq;
	pcidev_t *pcidev;
	unsigned short addr_io;
	mac_addr_t addr_mac;
	char *txbuf;
	char *rxbuf;

	struct rtl8169_tx_desc *txdesc;
	unsigned txdesc_phys;
	int tx_idx_free; // first free descriptor (owned by us)
	int tx_idx_full; // first full descriptor (owned by the NIC)

	struct rtl8169_rx_desc *rxdesc;
	unsigned rxdesc_phys;
	int rx_idx_free; // first free descriptor (owned by us)
	int rx_idx_full; // first full descriptor (owned by the NIC)
};

struct rtl8169_dev_t *rtl8169_dev;
static netdev_t *ifdev;

/* prototypes */
unsigned rtl8169_start (struct rtl8169_dev_t *dev);
unsigned rtl8169_int_rx (struct rtl8169_dev_t *dev, unsigned short status);
unsigned rtl8169_int_tx (struct rtl8169_dev_t *dev, unsigned short status);
bool rtl8169_acthandler (unsigned act, char *block, unsigned block_len);

/* I/O operations */
unsigned char rtl8169_read8 (struct rtl8169_dev_t *dev, unsigned short port)
{
	return inb (dev->addr_io + port);
}

void rtl8169_write8 (struct rtl8169_dev_t *dev, unsigned short port, unsigned char val)
{
	outb (dev->addr_io + port, val);
}

unsigned short rtl8169_read16 (struct rtl8169_dev_t *dev, unsigned short port)
{
	return inw (dev->addr_io + port);
}

void rtl8169_write16 (struct rtl8169_dev_t *dev, unsigned short port, unsigned short val)
{
	outw (dev->addr_io + port, val);
}

unsigned rtl8169_read32 (struct rtl8169_dev_t *dev, unsigned short port)
{
	return inl (dev->addr_io + port);
}

void rtl8169_write32 (struct rtl8169_dev_t *dev, unsigned short port, unsigned val)
{
	outl (dev->addr_io + port, val);
}

/* READ/WRITE func */
unsigned rtl8169_read (char *buf, unsigned len)
{
	return rtl8169_acthandler (DEV_ACT_READ, buf, len);
}

unsigned rtl8169_write (char *buf, unsigned len)
{
	return rtl8169_acthandler (DEV_ACT_WRITE, buf, len);
}

/* detect rtl8169 device in PC */
pcidev_t *rtl8169_detect ()
{
	/* First detect network card - is connected to PCI bus ?*/
	pcidev_t *pcidev;
	
	pcidev = pcidev_find (RTL8169_VENDORID, RTL8169_DEVICEID);

	if (!pcidev)
		pcidev = pcidev_find (RTL8169_VENDORID, RTL8169_DEVICEID2);
	else
		return 0;

	return pcidev;
}

/* reset rtl8169 device */
unsigned rtl8169_reset (struct rtl8169_dev_t *dev)
{
	rtl8169_write8 (dev, RTL8169_CHIPCMD, RTL8169_CMD_RESET);

	while (rtl8169_read8 (dev, RTL8169_CHIPCMD) & RTL8169_CMD_RESET) {
		kprintf ("waiting for rtl8169 reset :)\n");
		timer_wait (50);
	}

	return 1;
}

/* nic interrupt handler */
void rtl8169_int ()
{
	struct rtl8169_dev_t *dev = rtl8169_dev;

	unsigned short status = rtl8169_read16 (dev, RTL8169_INTRSTATUS);

	kprintf ("rtl8169_int ()\n");

	if (!status)
		return;

	if (status & (RTL8169_INT_RX_OK | RTL8169_INT_RX_ERR | RTL8169_INT_RX_UNDERRUN | RTL8169_INT_RX_OVERFLOW))
		rtl8169_int_rx (dev, status);

	if (status & (RTL8169_INT_TX_OK | RTL8169_INT_TX_ERR | RTL8169_INT_TX_UNDERRUN))
		rtl8169_int_tx (dev, status);

	rtl8169_write16 (dev, RTL8169_INTRSTATUS, status);
}

unsigned rtl8169_int_rx (struct rtl8169_dev_t *dev, unsigned short status)
{
	kprintf ("rtl8169_int_rx (): 0x%x\n", status);

	return 1;
}

unsigned rtl8169_tx (struct rtl8169_dev_t *dev, char *buf, unsigned len)
{

	return 1;
}

unsigned rtl8169_int_tx (struct rtl8169_dev_t *dev, unsigned short status)
{
	kprintf ("rtl8169_int_tx (): 0x%x\n", status);

	return 1;
}

/* INIT sequence */
unsigned init_rtl8169 ()
{
	unsigned i = 0;

	pcidev_t *pcidev = rtl8169_detect ();

	if (!pcidev)
		return 0;

	struct rtl8169_dev_t *dev = (struct rtl8169_dev_t *) kmalloc (sizeof (struct rtl8169_dev_t));

	if (!dev)
		return 0;

	/* get irq id */
	dev->irq = pcidev->u.h0.interrupt_line;
	dev->addr_io = pcidev->u.h0.base_registers[0];

	kprintf ("dev->irq: %d\ndev->addr_io: 0x%x\n", dev->irq, dev->addr_io);

	timer_wait (5000);

	/* reset device now */
	rtl8169_reset (dev);

  	// get mac address from ethernet
	for (i = 0; i < 6; i ++) {
		dev->addr_mac[i] = rtl8169_read8 (dev, RTL8169_IDR0 + i);
	}

	rtl8169_dev = dev;

	return rtl8169_start (dev);
}

unsigned rtl8169_start (struct rtl8169_dev_t *dev)
{
	int i;

	dev->rxdesc_phys = 0x51000;
	dev->txdesc_phys = 0x69000;

	dev->rxdesc = (struct rtl8169_rx_desc *) dev->rxdesc_phys;
	dev->txdesc = (struct rtl8169_tx_desc *) dev->txdesc_phys;

	rtl8169_write8 (dev, RTL8169_CFG9346, RTL8169_CFG9346_UNLOCK);

	/* some kind of magic */
	unsigned short r = rtl8169_read16 (dev, RTL8169_CCR);
	rtl8169_write16 (dev, RTL8169_CCR, r);
	r = rtl8169_read16 (dev, RTL8169_CCR);
	rtl8169_write16 (dev, RTL8169_CCR, r | 0x3);

	/* set up the rx/tx descriptors */
	for (i = 0; i < RTL8169_RX_DESC; i ++) {
		unsigned physaddr = (unsigned) RXBUF (dev, i);

		dev->rxdesc[i].rx_buffer_low = physaddr;
		dev->rxdesc[i].rx_buffer_high = 0;
		dev->rxdesc[i].frame_len = BUFSIZE_PER_FRAME;
		dev->rxdesc[i].flags = RTL_DESC_OWN | ((i == (RTL8169_RX_DESC - 1)) ? RTL_DESC_EOR : 0);
	}

	for (i = 0; i < RTL8169_TX_DESC; i ++) {
		unsigned physaddr = (unsigned) TXBUF (r, i);

		dev->txdesc[i].tx_buffer_low = physaddr;
		dev->txdesc[i].tx_buffer_high = 0;
		dev->txdesc[i].frame_len = 0; // will need to be filled in when transmitting
		dev->txdesc[i].flags = (i == (RTL8169_TX_DESC - 1)) ? RTL_DESC_EOR : 0;
	}

	/* set up our index pointers */
	dev->rx_idx_free = 0;
	dev->rx_idx_full = 0;
	dev->tx_idx_free = 0;
	dev->tx_idx_full = 0;

	/* point the nic at the descriptors */
	rtl8169_write32 (dev, RTL8169_TNPDS_LOW, dev->txdesc_phys);
	rtl8169_write32 (dev, RTL8169_TNPDS_HIGH, 0);
	rtl8169_write32 (dev, RTL8169_RDSAR_LOW, dev->rxdesc_phys);
	rtl8169_write32 (dev, RTL8169_RDSAR_HIGH, 0);

	rtl8169_write8 (dev, RTL8169_CFG9346, RTL8169_CFG9346_LOCK);

	rtl8169_read8 (dev, RTL8169_INTRMASK);

	/* mask all interrupts */
	rtl8169_write32 (dev, RTL8169_INTRMASK, 0);

	/* set up the rx state */
	/* 1024 byte dma threshold, 1024 dma max burst, CRC calc 8 byte+, accept all packets */
	rtl8169_write32 (dev, RTL8169_RXCONFIG, (1 << 16) | (6 << 13) | (6 << 8) | (0xf << 0)); 
	rtl8169_write16 (dev, RTL8169_CCR, rtl8169_read16 (dev, RTL8169_CCR) | (1 << 5)); // rx checksum enable
	rtl8169_write16 (dev, RTL8169_RMS, 1518); // rx mtu

	/* set up the tx state */
	rtl8169_write32 (dev, RTL8169_TXCONFIG, (rtl8169_read16 (dev, RTL8169_TXCONFIG) & ~0x1ff) | (6 << 8)); /* 1024 max burst dma*/
	rtl8169_write8 (dev, RTL8169_MTPS, 0x3f); /* max tx packet size (must be careful to not actually transmit more than mtu) */

	/* set interrupt vector */
	irq_install_handler (dev->irq, rtl8169_int);

	/* clear all pending interrupts */
	rtl8169_write16 (dev, RTL8169_INTRSTATUS, 0xffff);
	
	/* unmask interesting interrupts */
	rtl8169_write16 (dev, RTL8169_INTRMASK, RTL8169_INT_SYSERR | RTL8169_INT_LINKCHG | RTL8169_INT_TX_ERR | RTL8169_INT_TX_OK | RTL8169_INT_RX_ERR | RTL8169_INT_RX_OK | RTL8169_INT_RX_OVERFLOW);

	timer_wait (500);

	unsigned short status = rtl8169_read16 (dev, RTL8169_INTRSTATUS);

	kprintf ("rtl8169_status: 0x%x\n", status);

	timer_wait (2000);

	/* create new network device */
	ifdev = netdev_create (dev->addr_mac, &rtl8169_read, &rtl8169_write, dev->addr_io);

	if (!ifdev)
		return 0;
		
	ifdev->irq = dev->irq;

	return 1;
}

bool rtl8169_acthandler (unsigned act, char *block, unsigned block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			return init_rtl8169 ();
		}
		break;
		case DEV_ACT_READ:
		{

			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{
			return rtl8169_tx (rtl8169_dev, block, block_len);
		}
		break;
	}

	return 0;
}
#endif

