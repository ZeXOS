/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include <ipc.h>

#include "appcl.h"
#include "appsrv.h"
#include "dialog.h"
#include "cursor.h"

int appsrv_fd;
int appcl_fd;
char appsrv_buf[100];

int appsrv_accept (int fd)
{
	ipc_send (fd, "ZDE", 3);

	return 0;
}

int appsrv_redraw (zde_dobj_appcl_t *appcl)
{
	if (!appcl)
		return -1;

	/* new window (client) */
	if (appcl->fd == -1 && appcl_fd != -1) {
		appcl->fd = appcl_fd;
		appcl_fd = -1;
	}

	if (appcl->fd > 0 && !(appcl->state & APPCL_STATE_REDRAW)) {
		appcl->state |= APPCL_STATE_REDRAW;
		
		zde_drawlock ();

		ipc_send (appcl->fd, (char *) appcl, sizeof (appcl_t));

		int ret = ipc_recv (appcl->fd, (char *) appsrv_buf, 99);

		if (ret > 0) {
			memcpy (appcl, appsrv_buf, sizeof (appcl_t));

			if (appcl->state & APPCL_STATE_RECVOK) {
				appcl->state &= ~APPCL_STATE_RECVOK;
				zde_drawunlock ();
			}

			return 0;
		}
	}

	return -1;
}

int appsrv_exit (zde_dobj_appcl_t *appcl)
{
	if (!appcl)
		return -1;

	/* new window (client) ? */
	if (appcl->fd == -1)
		return -1;

	appcl->state |= APPCL_STATE_EXIT;

	ipc_send (appcl->fd, (char *) appcl, sizeof (appcl_t));

	return 0;
}

void *handler_appsrv (void *p)
{
	for (;; schedule ()) {
		int fd = ipc_accept (appsrv_fd);

		if (fd > 0) {
			appsrv_accept (fd);
			appcl_fd = fd;
		}
	}

	return (0);
}

int init_appsrv ()
{
	appsrv_fd = ipc_create ("/zdeserver", 0);

	if (appsrv_fd < 1)
		return -1;

	appcl_fd = -1;

	pthread_t thread;

	pthread_create (&thread, NULL, handler_appsrv, 0);

	return 0;
}
