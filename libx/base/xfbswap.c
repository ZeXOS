/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libx/base.h>

void xfbswap ()
{
	asm volatile (
		"movl $36, %%eax;"
	     	"int $0x80;"
		::: "%eax");

/*	if (vgadb != vgafb) {
		unsigned l = vgafb_res_x*vgafb_res_y;
		unsigned i = 0;
		for (i = 0; i < l; i ++) {
			vgafb[i] = vgadb[i];
			vgadb[i] = 0;
		}
	}*/
}
