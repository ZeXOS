/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>

static unsigned long next = 1;

static int do_rand (unsigned long *ctx)
{
	return ((*ctx = *ctx * 1103515245 + 12345) % ((unsigned long) RAND_MAX + 1));
}

int rand_r (unsigned int *ctx)
{
	unsigned long val = (unsigned long) *ctx;
	*ctx = do_rand (&val);

	return (int) *ctx;
}

int rand ()
{
	return do_rand (&next);
}

void srand (unsigned int seed)
{
	next = seed;
} 
