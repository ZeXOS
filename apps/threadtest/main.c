/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define	ESC	1

unsigned key_handler ()
{
	int scancode = getkey ();

	switch (scancode) {
		case ESC:
			exit (1);
			return 0;
	}

	return 0;
}

void *handler_thread (void *r)
{
	for (;;) {
		printf ("Thread %s\n", (char *) r);

		sleep (1);
		schedule ();
	}

	return 0;
}

int main (int argc, char **argv)
{
	printf ("This is example program for pthread library\nPress ESC to exit\n");

	pthread_t thread;
	pthread_create (&thread, NULL, handler_thread, "1");

	pthread_t thread2;
	pthread_create (&thread2, NULL, handler_thread, "2");
	
	for (;;) {
		key_handler ();

		schedule ();
	}

	return 0;
}
