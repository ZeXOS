/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <dev.h>
#include <partition.h>

extern dev_t dev_list;
extern partition_t partition_list;
extern char *argparse (char *cmd);

int fdisk (char *parm, unsigned len)
{
	dev_t *dev;

	if (len) {
		char str[64];

		if (len > 63)
			return 0;

		memcpy (str, parm, len);
		str[len] = '\0';

		char *a1 = argparse (str);
		char *a2 = argparse (a1);
		char *a3 = argparse (a2);

		unsigned i;
		for (i = 0; i < len; i ++) {
			if (str[i] == ' ')
				str[i] = '\0';
		}

		dev = dev_find (str);

		if (!dev) {
			printf ("fdisk: Device '%s' is not found\n", parm);
			return 0;
		}

		int id = atoi (a1);

		if (id < 0 || id > 3) {
			printf ("fdisk: Wrong partition ID, values 0-3 are correct\n");
			return 0;
		}

		int mbytes = atoi (a3);

		if (!mbytes) {
			printf ("fdisk: Wrong partition size, please set size in MBytes\n");
			return 0;
		}

		ptable_t ntable[4];

		/*ntable[0] = (ptable_t *) kmalloc (sizeof (ptable_t));
		ntable[1] = (ptable_t *) kmalloc (sizeof (ptable_t));
		ntable[2] = (ptable_t *) kmalloc (sizeof (ptable_t));
		ntable[3] = (ptable_t *) kmalloc (sizeof (ptable_t));

		if (!ntable[0] || !ntable[1] || !ntable[2] || !ntable[3])
			return 0;*/

		memset (&ntable[0], 0, sizeof (ptable_t));
		memset (&ntable[1], 0, sizeof (ptable_t));
		memset (&ntable[2], 0, sizeof (ptable_t));
		memset (&ntable[3], 0, sizeof (ptable_t));

		ntable[id].type = strtol (a2, 0, 16);

		printf ("Partition %s%d (type: 0x%x; size: %dMBytes)\nDo you want to continue ? (y/N)\n", dev->devname, id, ntable[id].type, mbytes);

		while (1) {
			char c = getkey ();

			if (c == 'y' || c == 'Y')
				break;
			if (c == 'n' || c == 'N')
				return 0;

			schedule ();
		}

		ntable[id].blocks = (mbytes * 1024 * 1024) / 512;
		ntable[id].lba = 63;

		partition_table_new (dev, (ptable_t *) &ntable);

		printf ("fdisk: Partition %s%d (type: 0x%x; size: %dMBytes) was created\n", dev->devname, id, ntable[id].type, mbytes);

		return 1;
	}

	for (dev = dev_list.next; dev != &dev_list; dev = dev->next) {
		if (dev->attrib == DEV_ATTR_BLOCK) {
			printf ("%s: %s\n", dev->devname, dev->desc);

			unsigned l = strlen (dev->devname);

			partition_t *p;
			for (p = partition_list.next; p != &partition_list; p = p->next) {
				if (!strncmp (p->name, dev->devname, l))
					printf ("    %c %s : %s\n", 192, p->name, p->fs->name);
			}

			printf ("\n");
		}
	}

	return 1;
}
 
