/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "net.h"

int sock;

static struct sockaddr_in serverSock;		// Remote socket
static struct hostent *host;             	// Remote host

int net_nonblock ()
{
   	/* set socket "sock" to non-blocking */
	int oldFlag = fcntl (sock, F_GETFL, 0);
	if (fcntl (sock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 1;
	} 
  
	return 0;
}

int net_send (char *buf, unsigned len)
{
	return send (sock, buf, len, 0);
}

int net_recv (char *buf, unsigned len)
{
	return recv (sock, buf, len, 0);
}

int net_connect (char *server, int port)
{
	// Let's get info about remote computer
	if ((host = gethostbyname ((char *) server)) == NULL) {
		printf ("Wrong address -> %s:%d\n", server, port);
		return 0;
	}

	// Create socket
	if ((sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return 0;
	}
	
	// Fill structure sockaddr_in
	// 1) Family of protocols
	serverSock.sin_family = AF_INET;
	// 2) Number of server port
	serverSock.sin_port = htons (port);
	// 3) Setup ip address of server, where we want to connect
	memcpy (&(serverSock.sin_addr), host->h_addr, host->h_length);
	
	// Now we are able to connect to remote server
	if (connect (sock, (struct sockaddr *) &serverSock, sizeof (serverSock)) == -1) {
		printf ("Connection cant be estabilished -> %s:%d\n", server, port);
		return -1;
	}

	printf ("zjab -> connected to %s:%d server\n", server, port);
	
	return 0;
}

int net_close ()
{
	close (sock);
	
	return 0;
}
