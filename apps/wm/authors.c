/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "window.h"
#include "cursor.h"
#include "button.h"
#include "menu.h"
#include "dialog.h"
#include "filemanager.h"
#include "config.h"

unsigned authors_act = 0;
wmdialog *authors_dialog;

unsigned iauthors ()
{
	if (authors_act)
		return 0;

	authors_act = 1;

	authors_dialog = dialog_create (160-90, 100-25, 180, 50, "Authors");

	return 1;
}

unsigned authors_handle ()
{
	if (authors_dialog != 0)
	if (authors_dialog->active) {
		xtext_puts (authors_dialog->x+1, authors_dialog->y+11, 0, "Programmer:");
		xtext_puts (authors_dialog->x+1, authors_dialog->y+20, 0, "Tomas 'ZeXx86' Jedrzejek");
		xtext_puts (authors_dialog->x+1, authors_dialog->y+38, 0, "Licensed under GNU/GPL 3");
	}
}

unsigned init_authors ()
{
	authors_dialog = 0;
	authors_act = 0;

	return 1;
}
