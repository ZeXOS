/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "platform.h"
#include "config.h"

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

config_t *cfg;

void config_set (char *variable, char *value)
{
	printf ("config -> %s : %s\n", variable, value);

	if (!strcmp (variable, "nick"))
		cfg->nick = strdup (value);
	else if (!strcmp (variable, "password"))
		cfg->password = strdup (value);
	else if (!strcmp (variable, "server"))
		cfg->server = strdup (value);
	else if (!strcmp (variable, "port"))
		cfg->port = atoi (value);
}

void config_parse (char *cfgfile, unsigned cfglen)
{
	unsigned i = 0, y = cfglen, z = 0;
	unsigned a = 0;

	while (i < y) {
		if (cfgfile[i] == '\n') {
			char var[16];
			char val[32];

			a = 0;

			while (a < 16) {
				if (cfgfile[a+z] == ' ')
					break;

				a ++;
			}

			if (a >= 15)
				continue;

			memcpy (var, cfgfile+z, a);
			var[a] = '\0';

			a ++;

			memcpy (val, cfgfile+z+a, i-z-a);
			val[i-z-a] = '\0';

			config_set (var, val);

			i ++;

			z = i;
		}

		i ++;
	}
}

int config_load (char *file)
{
	char *cfgfile = (char *) malloc (sizeof (char) * DEFAULT_CONFIG_MAXLENGTH);

	if (!cfgfile)
		return 0;

	// html web page
	int fd = open (file, O_RDONLY);
	
	if (!fd) {
		printf ("error -> file 'config' not found\n");
		return 0;
	}
	
	int cfglen = read (fd, cfgfile, DEFAULT_CONFIG_MAXLENGTH-1);

	if (!cfglen) {
		printf ("error -> something was wrong !\n");
		return 0;
	}

	cfgfile[DEFAULT_CONFIG_MAXLENGTH-1] = '\0';

	cfg = (config_t *) malloc (sizeof (config_t));

	if (!cfg)
		return 0;

	config_parse (cfgfile, (unsigned) cfglen);

	free (cfgfile);

	close (fd);

	return 1;
}

/** INIT CONFIG function */
int init_config ()
{
	if (!config_load (DEFAULT_CONFIG_FILE))
		return 0;

	if (!strcmp (cfg->nick, "#") || !strcmp (cfg->password, "#")) {
		printf ("WARNING -> Please re-write nick and password variable in 'config' file for autologin\nNOTE -> For register new account use command: /r <nick> <password>\nNOTE -> For more information use command /h for help\n");

		cfg->autolog = 0; 
	} else
		cfg->autolog = 1;

	return 1;
}
