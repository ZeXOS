/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <config.h>

#ifdef CONFIG_DRV_COMMOUSE
#include <system.h>
#include <arch/io.h>
#include <rs232.h>
#include <mouse.h>
#include <dev.h>

dev_mouse_t commouse;

unsigned commouse_handler ()
{
	char b1 = rs232_read_nonblock ();

	if ((b1 & (1 << 6)) == 0) {
	  	commouse.pos_x = 0;
		commouse.pos_y = 0;
		return 0;
	}

	// beginning of packet
	char b2 = rs232_read ();
	char b3 = rs232_read ();
	
	commouse.pos_x = (int) (char) ((b1 << 6) | (b2 & 0x3f));
	commouse.pos_y = (int) (char) (((b1 << 4) & 0xc0) | (b3 & 0x3f));

	unsigned short flags = (b1 >> 4) & 3;

	commouse.flags = (flags == 0x2) ? MOUSE_FLAG_BUTTON1 : 0;
	commouse.flags |= (flags == 0x1) ? MOUSE_FLAG_BUTTON2 : 0;
	commouse.flags |= (flags == 0x4) ? MOUSE_FLAG_BUTTON3 : 0;

	return 1;
}

unsigned commouse_init ()
{
	unsigned timeout;

	for (timeout = 3; timeout; timeout --) {
		char protocol = rs232_read_nonblock ();

		if (protocol == 'M')
			return 1;
	}

	return 0;
}

bool commouse_acthandler (unsigned act, char *block, unsigned block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			dev_flags_t *flags = (dev_flags_t *) block;

			if (!flags)
				return 0;

			if (block_len != sizeof (dev_flags_t))
				return 0;

			unsigned r = commouse_init ();

			if (!r)
				return 0;

			flags->iomem = (void *) &commouse;
			flags->iolen = sizeof (dev_mouse_t);

			memset (flags->iomem, 0, sizeof (dev_mouse_t));

			return 1;
		}
		break;
		case DEV_ACT_READ:
		{
			memcpy (block, &commouse, sizeof (commouse));

			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{
			memcpy (&commouse, block, sizeof (commouse));

			return 1;
		}
		break;
		case DEV_ACT_UPDATE:
		{
			commouse_handler ();
			
			return 1;
		}
		break;
	}

	return 0;
}
#endif
