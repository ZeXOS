/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <config.h>

#define VBE_DISPI_IOPORT_INDEX		0x01CE
#define VBE_DISPI_IOPORT_DATA		0x01CF

#define VBE_DISPI_ID4			0xB0C4

#define VBE_DISPI_INDEX_ID		0
#define VBE_DISPI_INDEX_XRES		1
#define VBE_DISPI_INDEX_YRES		2
#define VBE_DISPI_INDEX_BPP		3
#define VBE_DISPI_INDEX_ENABLE		4
#define VBE_DISPI_INDEX_BANK		5
#define VBE_DISPI_INDEX_VIRT_WIDTH	6
#define VBE_DISPI_INDEX_VIRT_HEIGHT	7
#define VBE_DISPI_INDEX_X_OFFSET	8
#define VBE_DISPI_INDEX_Y_OFFSET	9

#define VBE_DISPI_DISABLED              0x00
#define VBE_DISPI_ENABLED               0x01
#define VBE_DISPI_GETCAPS               0x02
#define VBE_DISPI_8BIT_DAC              0x20
#define VBE_DISPI_LFB_ENABLED           0x40
#define VBE_DISPI_NOCLEARMEM            0x80


#define VBE_DISPI_BPP_4			0x04
#define VBE_DISPI_BPP_8			0x08
#define VBE_DISPI_BPP_15		0x0F
#define VBE_DISPI_BPP_16		0x10
#define VBE_DISPI_BPP_24		0x18
#define VBE_DISPI_BPP_32		0x20

void BgaWriteRegister (unsigned short IndexValue, unsigned short DataValue)
{
	outw (VBE_DISPI_IOPORT_INDEX, IndexValue);
	outw (VBE_DISPI_IOPORT_DATA, DataValue);
}

unsigned short BgaReadRegister (unsigned short IndexValue)
{
	outw (VBE_DISPI_IOPORT_INDEX, IndexValue);
	return inw (VBE_DISPI_IOPORT_DATA);
}

int BgaIsAvailable (void)
{
	return (BgaReadRegister (VBE_DISPI_INDEX_ID) == VBE_DISPI_ID4);
}

void BgaSetVideoMode (unsigned int Width, unsigned int Height, unsigned int BitDepth, int UseLinearFrameBuffer, int ClearVideoMemory)
{
	BgaWriteRegister (VBE_DISPI_INDEX_ENABLE, VBE_DISPI_DISABLED);
	BgaWriteRegister (VBE_DISPI_INDEX_XRES, Width);
	BgaWriteRegister (VBE_DISPI_INDEX_YRES, Height);
	BgaWriteRegister (VBE_DISPI_INDEX_BPP, BitDepth);
	BgaWriteRegister (VBE_DISPI_INDEX_ENABLE, VBE_DISPI_ENABLED |
		UseLinearFrameBuffer ? VBE_DISPI_LFB_ENABLED : 0 |
		ClearVideoMemory ? 0 : VBE_DISPI_NOCLEARMEM);
}

void BgaSetBank (unsigned short BankNumber)
{
	BgaWriteRegister (VBE_DISPI_INDEX_BANK, BankNumber);
}


unsigned init_video_bga ()
{
	/*if (!BgaIsAvailable ()) {
		kprintf ("Bochs VESA VBE is not avaiable");
		return 0;
	}*/

	init_vgafb ();

	unsigned m = BgaReadRegister (VBE_DISPI_INDEX_ID);

	kprintf ("bga mem: 0x%x\n", m);

	//return 0;
	BgaSetVideoMode (1024, 768, VBE_DISPI_BPP_4, 1, 1);

	unsigned i = 0;

	/*unsigned *bgaaddr = (unsigned *) 0xE0000000;

	for (i = 0; i < 1024; i ++) {
		bgaaddr[i] = i;
	}*/

	return 1;
}
#endif
