/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <config.h>
#include <smp.h>
#include <vfs.h>

smp_cpu_t smp_cpu_list;

smp_cpu_t *smp_cpu_getnextcpu (smp_cpu_t *cpu)
{
	if (!cpu)
		return smp_cpu_list.next;

	smp_cpu_t *nextcpu = cpu->next;

	if (cpu == &smp_cpu_list)
		return 0;

	return nextcpu;
}

void smp_cpu_check ()
{
	unsigned cpu_cnt = 1;

	/*char *str = (char *) kmalloc (sizeof (char) * (cpu_cnt * 256));

	if (!str)
		return;
	
	str[0] = '\0';*/

	/*char strcpu[80];

	char cpuarch[16];

	smp_cpu_t *cpu;
	for (cpu = smp_cpu_list.next; cpu != &smp_cpu_list; cpu = cpu->next) {*/
		/*if (cpu->arch == SMP_ARCH_x86)
			strcpy (cpuarch, "x86");
		else if (cpu->arch == SMP_ARCH_ARM)
			strcpy (cpuarch, "ARM");
		else if (cpu->arch != SMP_ARCH_x86)
			strcpy (cpuarch, "unknown");

		sprintf (strcpu, "CPU %d (%s)\n\tarch: %s\n\tstate: %s\n\tfeatures: ", cpu_cnt, (cpu->flags == SMP_FLAGS_CPU_BS ) ? "BS" : "AP",
			cpuarch, (cpu->state == SMP_STATE_CPU_UP ) ? "running" : "down");

		strcpy (str, strcpu);

		smp_arch_x86 *arch_spec = cpu->arch_spec;

		if (!arch_spec) {
			strcat (str, "N/A");
			goto r;
		}

		if (arch_spec->features & SMP_ARCH_x86_FEATURE_FPU)
			strcat (str, "fpu ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_VME)
			strcat (str, "vme ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_DE)
			strcat (str, "de ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_PSE)
			strcat (str, "pse ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_TSC)
			strcat (str, "tsc ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_MSR)
			strcat (str, "msr ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_PAE)
			strcat (str, "pae ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_MCE)
			strcat (str, "mce ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_CX8)
			strcat (str, "cx8 ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_APIC)
			strcat (str, "apic ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_SEP)
			strcat (str, "sep ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_MTRR)
			strcat (str, "mttr ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_PGE)
			strcat (str, "pge ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_MCA)
			strcat (str, "mca ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_CMOV)
			strcat (str, "cmov ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_PAT)
			strcat (str, "pat ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_PSE36)
			strcat (str, "pse36 ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_PSN)
			strcat (str, "psn ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_CLFSH)
			strcat (str, "clfsh ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_DS)
			strcat (str, "ds ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_ACPI)
			strcat (str, "acpi ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_MMX)
			strcat (str, "mmx ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_FXSR)
			strcat (str, "fxsr ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_SSE)
			strcat (str, "sse ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_SSE2)
			strcat (str, "sse2 ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_SS)
			strcat (str, "ss ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_HTT)
			strcat (str, "htt ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_TM)
			strcat (str, "tm ");
		if (arch_spec->features & SMP_ARCH_x86_FEATURE_PBE)
			strcat (str, "pbe ");
r:
		strcat (str, "\n");

		cpu_cnt ++;*/
	/*}
*/
	//vfs_list_add ("cpuinfo", VFS_FILEATTR_FILE | VFS_FILEATTR_READ | VFS_FILEATTR_SYSTEM, "/proc/");

	/*vfs_content_t content;
	content.ptr = "prd";
	content.len = 3;*/
	
	//vfs_mmap ("/proc/cpuinfo", 13, &content);
}

/* mostly x86 stuff */
unsigned smp_cpu_register (unsigned char arch, unsigned char state, unsigned char flags, void *arch_spec)
{
	smp_cpu_t *cpu;
	
	/* alloc and init context */
	cpu = (smp_cpu_t *) kmalloc (sizeof (smp_cpu_t));

	if (!cpu)
		return 0;

	cpu->arch = arch;
	cpu->arch_spec = arch_spec;
	cpu->flags = flags;
	cpu->state = state;

	/* add into list */
	cpu->next = &smp_cpu_list;
	cpu->prev = smp_cpu_list.prev;
	cpu->prev->next = cpu;
	cpu->next->prev = cpu;

	return 1;
}

unsigned int init_smp ()
{
	smp_cpu_list.next = &smp_cpu_list;
	smp_cpu_list.prev = &smp_cpu_list;

	return arch_smp_init ();
}
