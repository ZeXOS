/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>


/* This will keep track of how many ticks that the system
*  has been running for */
unsigned long timer_ticks = 0;

/* Software timer - ticks count to 1ms */
unsigned long timer_swticks = 0;

/* This will continuously loop until the given time has
*  been reached */
void timer_wait (int ticks)
{
	int eticks;
	
	eticks = timer_ticks + ticks;
		
	while (timer_ticks < eticks)
		kprintf("");;
}

char *time_tostring (unsigned long s)
{
	return "N/A";
}

void uptime (void)
{
	printf ("Uptime: %s\n", time_tostring (timer_ticks/((3579545L / 3) / HZ)));
}

void calc_freqency ()
{
	/* Kernel with 1000Hz timer - dont need it ? */
	if (kernel_attr & KERNEL_18HZ) {
		int eticks;
		
		eticks = timer_ticks + 18;
	
		while (timer_ticks < eticks) {
			ctps ++;
			kprintf("");;
		}
	
		kprintf ("Freqency: %lluHz\n", ctps);
	}
}

void calc_swtimer ()
{
	unsigned long timer = timer_ticks;

	while ((timer_ticks - timer) >= 1)
		timer_swticks ++;
}

void usleep (unsigned len)
{
	if (kernel_attr & KERNEL_18HZ) {
		unsigned long long t = ctps * len;
		unsigned long long i = 0;
	
		while (t > i) {
			i += 1000;
			kprintf("");;
		}
	} else
		timer_wait (1);
}


unsigned int timer_install ()
{

	return 1;
}
