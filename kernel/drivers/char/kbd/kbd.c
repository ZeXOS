/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <signal.h>
#include <config.h>
#include <console.h>
#include <proc.h>
#include "kbd.h"


unsigned char scancode = 0;
unsigned modes = 0;
bool upped = 0;

kbd_quaue kbd_q;

char kbdmap[3];

#ifndef ARCH_i386
void keyboard_handler ();
#endif

unsigned keyboard_setlayout (char *layout)
{
	if (!strcmp (layout, "us")) {
		kbd_layout[0] = (unsigned char *) &kbdus;
		kbd_layout[1] = (unsigned char *) &kbdus_shift;

		strcpy (kbdmap, "us");

		return 1;
	}

	if (!strcmp (layout, "cz")) {
		kbd_layout[0] = (unsigned char *) &kbdcz;
		kbd_layout[1] = (unsigned char *) &kbdcz_shift;

		strcpy (kbdmap, "cz");

		return 1;
	}

	return 0;
}

unsigned keyboard_getlayout (char *layout)
{
	if (!strcmp (kbdmap, layout))
		return 1;

	return 0;
}

char getkey ()
{
#ifdef ARCH_arm
	char k = rs232_read ();

	if (!k)
		return 0;

	if ((k >= '0' && k <= '9') || (k >= 'A' && k <= 'Z') || (k >= 'a' && k <= 'z')) {
		kbd_q.key[0] = k;
		kbd_q.p = 1;
	} else if (k == 13) {
		kbd_q.key[0] = '\n';
		kbd_q.p = 1;
	} else if (k == 32) {
		kbd_q.key[0] = ' ';
		kbd_q.p = 1;
	} else if (k == 127) {
		kbd_q.key[0] = '\b';
		kbd_q.p = 1;
	}

	keyboard_handler ();
#endif
	char s[KBD_MAX_QUAUE];

	if (!kbd_q.p)
		return 0;

	char key = kbd_q.key[0];

	kbd_q.p --;

	memcpy (s, kbd_q.key+1, kbd_q.p);

	memcpy (kbd_q.key, s, kbd_q.p);

	return key;
}

void setkey (char key)
{
	char s[KBD_MAX_QUAUE];

	if (kbd_q.p >= KBD_MAX_QUAUE)
		return;

	kbd_q.p ++;

	memcpy (s+1, kbd_q.key, kbd_q.p);

	s[0] = key;

	memcpy (kbd_q.key, s, kbd_q.p);
}

unsigned key_pressed (int keycode)
{
	if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;
}

#ifdef ARCH_i386
void keyboard_handler (struct regs *r)
#endif
#ifdef ARCH_arm
void keyboard_handler ()
#endif
{
	int i, setsignal = 0;

	/* Read from the keyboard's data buffer */
#ifdef ARCH_i386
	scancode = inb (0x60);

	outb (0x20, 0x20);
#endif
#ifdef ARCH_arm
	if (!armbd_kbd_data ())
		return;

	scancode = armbd_kbd_scancode ();

	armbd_kbd_ack ();
#endif
	/* If the top bit of the byte we read from the keyboard is
	*  set, that means that a key has just been released */
	if (scancode & 0x80) {
		/* If the user released the shift, alt, or control keys... */
		//DPRINT ("key up: %d", scancode);

		//kbd_q.state[0] = 2;	// up

		if (key_pressed (CTRL) == 2)
			modes &= ~0x1;
		if (key_pressed (ALT) == 2)
			modes &= ~0x2;
		if (key_pressed (DEL) == 2)
			modes &= ~0x4;
		if (key_pressed (SHIFTL) == 2 || key_pressed (SHIFTR) == 2)
			modes &= ~0x8;
	} else {

		/* Here, a key was just pressed. Please note that if you
		*  hold a key down, you will get repeated key press
		*  interrupts. */
	
		/* Just to show you how this works, we simply translate
		*  the keyboard scancode into an ASCII value, and then
		*  display it to the screen. You can get creative and
		*  use some flags to see if a shift is pressed and use a
		*  different layout, or you can add another 128 entries
		*  to the above layout to correspond to 'shift' being
		*  held. If shift is held using the larger lookup table,
		*  you would add 128 to the scancode when you look for it */

		//DPRINT ("key down: %d", scancode);

		/* CTRL+ALT+DEL - magic keys */
		if (key_pressed (CTRL))
			modes |= 0x1;
		if (key_pressed (ALT))
			modes |= 0x2;
		if (key_pressed (DEL))
			modes |= 0x4;

		/* big letters */
		if (key_pressed (SHIFTL) || key_pressed (SHIFTR))
			modes |= 0x8;
		
		/* TTY changing - alt+F1-F4*/
		if (modes & 0x2) {
			if (key_pressed (F1))
				tty_change ((tty_t *) tty_find ("tty0"));
			else if (key_pressed (F2))
				tty_change ((tty_t *) tty_find ("tty1"));
			else if (key_pressed (F3))
				tty_change ((tty_t *) tty_find ("tty2"));
			else if (key_pressed (F4))
				tty_change ((tty_t *) tty_find ("tty3"));
			else if (key_pressed (PAGEUP))
				tty_switch ('+');
			else if (key_pressed (PAGEDOWN))
				tty_switch ('-');
		}

		if (key_pressed (ARROWUP))
			consolelog_prev ();
		if (key_pressed (ARROWDOWN))
			consolelog_next ();

		/* reboot pc, when are pressed ctrl+alt+del */
		if (modes & 0x1 && modes & 0x2) {
			if (modes & 0x4) {
				kprintf ("\nRebooting ..");

				arch_cpu_reset ();
				while (1);
			}
		}

		/* CTRL + C - sigterm */
		if (modes & 0x1) {
			if (key_pressed (KB_C)) {
				setsignal = 1;
				scancode = 0;
			}
		}

		/* SHIFT - BIG LETTERS */
		if (modes & 0x8)
			setkey (kbd_layout[1][scancode]);
		else
			setkey (kbd_layout[0][scancode]);

	}

	/* CTRL + C was pressed - send sigterm to app */
	if (setsignal) {
		if (!signal (SIGTERM, (sighandler_t) SIG_IGN))
			DPRINT (DBG_DRIVER, "signal () - error\n");
	}
}

unsigned int init_keyboard ()
{
#ifdef CONFIG_DRV_KEYBOARD
	/* set default kbd layout - US */
	kbd_layout[0] = (unsigned char *) &kbdus;
	kbd_layout[1] = (unsigned char *) &kbdus_shift;

	strcpy (kbdmap, CONFIG_UI_KBD_LAYOUT);

	if (!strcmp (kbdmap, "us")) {
		keyboard_setlayout ("us");
	
		kprintf ("Loaded en_US keyboard layout\n");
	}

	if (!strcmp (kbdmap, "cz")) {
		keyboard_setlayout ("cz");
	
		kprintf ("Loaded cs_CZ keyboard layout\n");
	}

	int i;
   	for (i = 0; i < 20; i ++)
		currtty->shell[i] = '\0';

   	for (i = 0; i < KBD_MAX_QUAUE; i ++)
		kbd_q.key[i] = '\0';

	kbd_q.p = 0;

#ifdef ARCH_i386
	irq_install_handler (1, keyboard_handler);
#endif
#ifdef ARCH_arm
	armbd_kbd_init ();
#endif
#endif
	return 1;
}
