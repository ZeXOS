/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <system.h>

#ifdef ARCH_i386
#include <multiboot/grub.h>

int main (multiboot_info_t *boot_info)
{
	if (!boot_info->flags & 2)
		return 0;

	/* get memory in MB */
	mem_ext = ((boot_info->mem_lower + boot_info->mem_upper) / 1024);

	/* first initialize all needed things */
	init ((char *) boot_info->cmdline);

#else
int main ()
{
	/* first initialize all needed things */
	init ((char *) "");
#endif
	/* kernel loop */
	for (;;)
		schedule ();

	return 0;
}
