/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "net.h"
#include "game.h"
#include "client.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>

/** INIT function */
int init ()
{
	if (!init_game ())
		return 0;

	if (!init_client ())
		return 0;

	if (!init_net ())
		return 0;

	return 1;
}

/** MAIN LOOP */
int loop ()
{
	return net_loop ();
}

/** MAIN Function */
int main (int argc, char **argv)
{
	printf ("OpenChess - game server\n");

	if (!init ())
		return 0;

	while (1) {
		if (loop () == -1)
			break;
#ifdef __zexos__
		schedule ();
#endif
	}

	printf ("-> exit\n");

	return 1;
}
