/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <dev.h>
#include <fs.h>
#include <partition.h>
#include <cache.h>
#include <vfs.h>

#define EXT2_MAGIC			0xEF53		// magic number for ext2 filesystem

#define EXT2_SUPERBLOCK_SECTOR		1		// sector, where is placed superblock

#define EXT2_GOOD_OLD_REV		0		// original format
#define EXT2_DYNAMIC_REV		1		// V2 format with dynamic inode sizes

#define EXT2_BAD_INO			0x01		// bad blocks inode
#define EXT2_ROOT_INO			0x02		// root directory inode
#define EXT2_ACL_IDX_INO		0x03		// ACL index inode (deprecated?)
#define EXT2_ACL_DATA_INO		0x04		// ACL data inode (deprecated?)
#define EXT2_BOOT_LOADER_INO		0x05		// boot loader inode
#define EXT2_UNDEL_DIR_INO		0x06		// undelete directory inode

#define EXT2_S_IFMT			0xF000		// format mask
#define EXT2_S_IFSOCK			0xC000		// socket
#define EXT2_S_IFLNK			0xA000		// symbolic link
#define EXT2_S_IFREG			0x8000		// regular file
#define EXT2_S_IFBLK			0x6000		// block device
#define EXT2_S_IFDIR			0x4000		// directory
#define EXT2_S_IFCHR			0x2000		// character device
#define EXT2_S_IFIFO			0x1000		// fifo

#define EXT2_S_ISUID			0x0800		// SUID
#define EXT2_S_ISGID			0x0400		// SGID
#define EXT2_S_ISVTX			0x0200		// sticky bit
#define EXT2_S_IRWXU			0x01C0		// user access rights mask
#define EXT2_S_IRUSR			0x0100		// read
#define EXT2_S_IWUSR			0x0080		// write
#define EXT2_S_IXUSR			0x0040		// execute
#define EXT2_S_IRWXG			0x0038		// group access rights mask
#define EXT2_S_IRGRP			0x0020		// read
#define EXT2_S_IWGRP			0x0010		// write
#define EXT2_S_IXGRP			0x0008		// execute
#define EXT2_S_IRWXO			0x0007		// others access rights mask
#define EXT2_S_IROTH			0x0004		// read
#define EXT2_S_IWOTH			0x0002		// write
#define EXT2_S_IXOTH			0x0001		// execute

#define EXT2_FT_UNKNOWN			0
#define EXT2_FT_REG_FILE		1
#define EXT2_FT_DIR			2
#define EXT2_FT_CHRDEV			3
#define EXT2_FT_BLKDEV			4
#define EXT2_FT_FIFO			5
#define EXT2_FT_SOCK			6
#define EXT2_FT_SYMLINK			7
#define EXT2_FT_MAX			8

typedef struct {
	unsigned inodecount;
	unsigned blockcount;
	unsigned r_blockcount;
	unsigned free_blockcount;
	unsigned free_inodecount;
	unsigned first_data_block;
	unsigned log_block_size;
	unsigned log_frag_size;
	unsigned blocks_per_group;
	unsigned frags_per_group;
	unsigned inodes_per_group;
	unsigned mtime;
	unsigned wtime;
	unsigned short mnt_count;
	unsigned short max_mnt_count;
	unsigned short magic;
	unsigned short state;
	unsigned short errors;
	unsigned short minor_rev_level;
	unsigned lastcheck;
	unsigned checkinterval;
	unsigned creator_os;
	unsigned rev_level;
	unsigned short def_resuid;
	unsigned short def_resgid;
	//   -- EXT2_DYNAMIC_REV Specific --
	unsigned first_ino;
	unsigned short inode_size;
	unsigned short block_group_nr;
	unsigned feature_compat;
	unsigned feature_incompat;
	unsigned feature_ro_compat;
	unsigned char uuid[16];
	unsigned char volume_name[16];
	unsigned char last_mounted[64];
	unsigned algo_bitmap;
	//   -- Performance Hints --
	unsigned char prealloc_blocks;
	unsigned char prealloc_dir_blocks;
	unsigned short alignment;
	//   -- Journaling Support --
	unsigned char journal_uuid[16];
	unsigned journal_inum;
	unsigned journal_dev;
	unsigned last_orphan;
	//   -- Unused	--
	unsigned char padding[788];
} ext2_superblock_t;

typedef struct {
	unsigned block_bitmap;
	unsigned inode_bitmap;
	unsigned inode_table;
	unsigned short free_blocks_count;
	unsigned short free_inodes_count;
	unsigned short used_dirs_count;
	unsigned short pad;
	unsigned char reserved[12];
} ext2_group_t;

typedef struct {
	unsigned short mode;
	unsigned short uid;
	unsigned size;
	unsigned atime;
	unsigned ctime;
	unsigned mtime;
	unsigned dtime;
	unsigned short gid;
	unsigned short links_count;
	unsigned blocks;
	unsigned flags;
	unsigned osd1;
	unsigned block[15];
	unsigned generation;
	unsigned file_acl;
	unsigned dir_acl;
	unsigned faddr;
	unsigned char osd2[12];
} ext2_inode_t;

typedef struct {
	unsigned inode;
	unsigned short rec_len;
	unsigned char name_len;
	unsigned char file_type;
	unsigned char name[255];
} ext2_dir_t;

static unsigned block_size = 0;
static unsigned block_free = 0;
static unsigned inode_curr = 0;

ext2_superblock_t *superblock;

unsigned ext2_sector_read (partition_t *p, unsigned sector, unsigned char *buffer)
{
	/* ext2 sector is 1kB length */
	unsigned address = sector * 2;

	/* lba28_drive_read () read 512 bytes from drive 2 times +512 bytes */
	lba28_drive_read (p, address+p->sector_start, (unsigned char *) buffer);
	lba28_drive_read (p, address+p->sector_start+1, (unsigned char *) buffer+512);

	return 1;
}

unsigned ext2_sector_write (partition_t *p, unsigned sector, unsigned char *buffer)
{
	/* ext2 sector is 1kB length */
	unsigned address = sector * 2;

	/* lba28_drive_write () write 512 bytes to drive 2 times +512 bytes */
	lba28_drive_write (p, address+p->sector_start, (unsigned char *) buffer);
	lba28_drive_write (p, address+p->sector_start+1, (unsigned char *) buffer+512);

	return 1;
}

extern unsigned long file_cache_id;
/* read data block of file by inode */
unsigned ext2_block_read (partition_t *p, ext2_inode_t *inode, vfs_content_t *content)
{
	unsigned address = inode->block[0] * 2 + p->sector_start;
	unsigned long len = 0;
	unsigned i = 0;

	DPRINT (DBG_DRIVER | DBG_FS, "blocks: %d %d %d %d %d %d %d %d %d %d", inode->block[0], inode->block[1], inode->block[2], inode->block[3], inode->block[4], inode->block[5], inode->block[6], inode->block[12], inode->block[13], inode->block[14]);
	//timer_wait (1000);
	char *block = (char *) kmalloc (1024);

	unsigned block_new[15];
	/* indirect block */
	if (inode->block[12]) {
		ext2_sector_read (p, inode->block[12], block);

		memcpy (&block_new, block, sizeof (unsigned) * 15);
	}

	cache_t *cache = cache_create (0, inode->size, 1);

	if (!cache)
		return 0;

	for (;;) {
		/* first 12 address are direct and next are from indirect block */
		if (i < 12)
			address = inode->block[i];
		else
			address = block_new[i-12];

		if (!address)
			break;

		/* read 1024 bytes block and put data to file_cache */
		ext2_sector_read (p, address, (unsigned char *) block);

		/*unsigned x = 0;
		while (x < 1024) {
			putch (block[x]);
			x ++;
		}*/
		
		if ((inode->size - len) > 1024)
			cache_add (block, 1024);
		else
			cache_add (block, inode->size - len);

		if ((len+1024) > inode->size)
			break;

		len += 1024;
		i ++;
	}

	kfree (block);

	content->ptr = (char *) &cache->data;
	content->len = cache->limit;
	
	return 1;
}

/* write data block of file by inode */
unsigned ext2_block_write (partition_t *p, ext2_inode_t *inode)
{
	/* when it is new file, lets assign free data block */
	if (!inode->block[0])
		inode->block[0] = 10000;

	/* REWRITE */
	unsigned address = inode->block[0] * 2 + p->sector_start;
	unsigned long len = 0;
	unsigned i = 0;

	cache_t *cache = cache_read ();
	
	if (!cache) {
		printf ("ext2 -> nothing to write\n");
		return 0;
	}
	
	for (;;) {
		/* write 512 bytes block from file_cache */
		lba28_drive_write (p, address+i, &cache->data+len);

		if ((len+512) > cache->limit)
			break;

		len += 512;
		i ++;
	}

	inode->size = cache->limit;

	return 1;
}

/* return inode structure by inode id number */
ext2_inode_t *ext2_inode_read (partition_t *p, char *block, unsigned id)
{
	/* group descriptors */
	char buf[1024];
	ext2_group_t *group = (ext2_group_t *) &buf;//kmalloc (1024);

	if (!group)
		return 0;

	ext2_sector_read (p, 2, (unsigned char *) group);

	/* locate inode group and structure */
	unsigned group_num = (id - 1) / superblock->inodes_per_group;
	unsigned id_new = (id - 1) % superblock->inodes_per_group;

	ext2_sector_read (p, group[group_num].inode_table, (unsigned char *) block);

	ext2_inode_t *inode = (ext2_inode_t *) block;
	
	DPRINT (DBG_DRIVER | DBG_FS, "inode -> %d / %d - 0x%x - %d - %d - %d", superblock->inodes_per_group, id_new, inode[id_new].mode, inode[id_new].ctime, inode[id_new].size, inode[id_new].block[0]);
	
	//kfree (group);
	
	return &inode[id_new];
}

/* write inode structure by inode id number */
unsigned ext2_inode_write (partition_t *p, ext2_inode_t *inode, unsigned id)
{
	/* group descriptors */
	ext2_group_t *group = (ext2_group_t *) kmalloc (1024);

	if (!group)
		return 0;

	ext2_sector_read (p, 2, (unsigned char *) group);

	/* locate inode group and structure */
	unsigned group_num = (id - 1) / superblock->inodes_per_group;
	unsigned id_new = (id - 1) % superblock->inodes_per_group;

	ext2_inode_t *block = (ext2_inode_t *) kmalloc (1024);

	if (!block) {
		kfree (group);
		return 0;
	}
	
	ext2_sector_read (p, group[group_num].inode_table, (unsigned char *) block);
	
	memcpy (&block[id_new], inode, sizeof (ext2_inode_t));
	
	ext2_sector_write (p, group[group_num].inode_table, (unsigned char *) block);
	
	//DPRINT (DBG_DRIVER | DBG_FS, "inode -> %d / %d - 0x%x - %d - %d - %d", superblock->inodes_per_group, id_new, inode[id_new].mode, inode[id_new].ctime, inode[id_new].size, inode[id_new].block[0]);
	
	kfree (block);
	kfree (group);
	
	return 1;
}

/* scan current directory and assign to dir[] structure */
unsigned ext2_dir_scan (partition_t *p, ext2_inode_t *inode)
{
	char buf[1024];
	ext2_dir_t *ext2dir = (ext2_dir_t *) &buf;//kmalloc (1024);

	if (!ext2dir)
		return 0;

	/* read direct block */
	ext2_sector_read (p, inode->block[0], (unsigned char *) ext2dir);

	unsigned dir_next = 0;
	unsigned entry = 0;

	for (;;) {
		ext2_dir_t *dir_new = (ext2_dir_t *) ((void *) ext2dir + dir_next);

		DPRINT (DBG_DRIVER | DBG_FS, "dir -> 0x%x : %d - %d - %d - %s - %d", dir_new, dir_new->inode, dir_new->rec_len, dir_new->name_len, dir_new->name, dir_new->file_type);

		if (!dir_new->rec_len || !dir_new->inode || dir_next >= 1024)
			break;

		/* we won't unknown file type */
		if (dir_new->file_type == EXT2_FT_UNKNOWN) {
			dir_next += dir_new->rec_len;
			continue;
		}

		/* yeah :( .. limit to 10 characters for dir[] entry */
		if (dir_new->name_len < 10) {
			memcpy (dir[entry].name, dir_new->name, dir_new->name_len);
			dir[entry].name[dir_new->name_len] = '\0';
		} else {
			memcpy (dir[entry].name, dir_new->name, 9);
			dir[entry].name[9] = '\0';
		}

		/* check for directory type or file */
		if (dir_new->file_type == EXT2_FT_DIR)
			dir[entry].dir = 1;
		else
			dir[entry].dir = 0;

		/* increase dir_next with current record length for find next ext2_dir_t * structure */
		dir_next += dir_new->rec_len;
		/* increase dir[] entry */
		entry ++;
	}

	//kfree (ext2dir);

	/* clear last+1 entry of dir[] for prevent */
	memset (dir[entry].name, 0, 10);

	return 1;
}

unsigned ext2_create_entity (partition_t *p, char *name, unsigned char type)
{
	if (!name)
		return 0;

	char *block = (char *) kmalloc (1024);

	if (!block)
		return 0;

	DPRINT (DBG_DRIVER | DBG_FS, "ext2_create_entity -> %d : %s", inode_curr, name);

	/* read inode structure by current inode id number */
	ext2_inode_t *inode = ext2_inode_read (p, block, inode_curr);

	if (!inode) {
		kfree (block);
		return 0;
	}

	ext2_dir_t *ext2dir = (ext2_dir_t *) kmalloc (1024);

	if (!ext2dir) {
		kfree (block);
		return 0;
	}

	/* read direct block */
	ext2_sector_read (p, inode->block[0], (unsigned char *) ext2dir);

	unsigned dir_next = 0;

	for (;;) {
		ext2_dir_t *dir_new = (ext2_dir_t *) ((char *) ext2dir + dir_next);

		if (dir_new->rec_len && dir_new->inode) {
			dir_next += dir_new->rec_len;
			printf ("dir_next: %d | %d | %d\n", dir_next, dir_new->rec_len, dir_new->inode);

			if (dir_next >= 1024) {
				if (dir_new->rec_len < 264) {
					printf ("ERROR -> out of dirent\n");
					break;
				}
				
				dir_next -= dir_new->rec_len;
				dir_new->rec_len = (dir_new->name_len+9) <= 12 ? 12 : 20;
				dir_next += dir_new->rec_len;
 				printf ("dir_new->rec_len: %d dir_next: %d\n", dir_new->rec_len, dir_next);
				
				int inode = dir_new->inode + 1;
				
				if (type == EXT2_FT_DIR)
					inode += 2000;

				dir_new = (ext2_dir_t *) ((char *) ext2dir + dir_next);
				
				dir_new->inode = inode;
			} else
				continue;
		}

		/* set the file type */
		dir_new->file_type = type;

		unsigned name_len = strlen (name);

		if (name_len > 9)
			name_len = 9;

		/* yeah :( .. limit to 10 characters for dir[] entry */
		memcpy (dir_new->name, name, name_len);
		dir_new->name_len = name_len;
		dir_new->rec_len = 1024 - dir_next;

		ext2_inode_t *inode_new = (ext2_inode_t *) kmalloc (sizeof (ext2_inode_t));
			
		if (!inode_new)
			return 0;

		tm *t = rtc_getcurrtime ();

		inode_new->mode = 0;
		inode_new->uid = 0;
		inode_new->size = 0;
		inode_new->atime = t->__tm_gmtoff;
		inode_new->ctime = t->__tm_gmtoff;
		inode_new->mtime = t->__tm_gmtoff;
		inode_new->dtime = 0;
		inode_new->gid = 0;
		inode_new->links_count = 0;
		inode_new->blocks = 0;
		inode_new->flags = 0;
		inode_new->osd1 = 0;
		
		if (dir_new->file_type != EXT2_FT_DIR)
			inode_new->block[0] = block_free;
		else
			inode_new->block[0] = 0;

		if (!ext2_inode_write (p, inode_new, dir_new->inode)) {
			kfree (ext2dir);
			kfree (block);

			return 0;
		}
		
		/* write direct block */
		ext2_sector_write (p, inode->block[0], (unsigned char *) ext2dir);

		block_free += 32;

		DPRINT (DBG_DRIVER | DBG_FS, "dir -> dir_new->inode: %d | %d | %d", dir_new->inode, dir_new->rec_len, sizeof (ext2_inode_t));

		kfree (inode_new);

			//

			//if (!inode_new)
			//	goto flush;

			//DPRINT (DBG_DRIVER | DBG_FS, "dir -> %d - %d %d %d %d %d\n", inode_new->size, inode_new->block[0], inode_new->block[1], inode_new->block[2], inode_new->block[13], inode_new->block[14]);

			/* write chunk of data */
			//ext2_block_write (p, inode_new);

		break;
	}

	kfree (ext2dir);
	kfree (block);

	return 1;
}

unsigned ext2_cd_root (partition_t *p)
{
	/* group descriptors */
	char buf[1024];
	ext2_group_t *group = (ext2_group_t *) &buf; //kmalloc (1024);

	if (!group)
		return 0;

	/* read group decriptor */
	ext2_sector_read (p, 2, (unsigned char *) group);

	DPRINT (DBG_DRIVER | DBG_FS, "group -> %d - %d - %d", group[0].block_bitmap, group[0].inode_bitmap, group[0].inode_table);

	/* inode tables */
	ext2_inode_t *inode = (ext2_inode_t *) kmalloc (1024);

	if (!inode)
		return 0;

	/* read root inode */
	ext2_sector_read (p, group[0].inode_table, (unsigned char *) inode);

	DPRINT (DBG_DRIVER | DBG_FS, "inode -> 0x%x - %d - %d - %d", inode[1].mode, inode[1].ctime, inode[1].size, inode[1].block[0]);

	/* read dir entries from root inode */
	ext2_dir_scan (p, &inode[EXT2_ROOT_INO-1]);

	inode_curr = EXT2_ROOT_INO;

	kfree (inode);
//	kfree (group);

	return 1;
}

unsigned ext2_cd (partition_t *p, char *name)
{
	char buf[1024];
	char *block = (char *) &buf;// kmalloc (1024);
		
	if (!block)
		return 0;

	/* read inode structure by current inode id number */
	ext2_inode_t *inode = ext2_inode_read (p, block, inode_curr);

	if (!inode) {
		kfree (block);
		return 0;
	}

	ext2_dir_t *ext2dir = (ext2_dir_t *) kmalloc (1024);

	if (!ext2dir) {
		kfree (block);
		return 0;
	}

	/* read direct block by inode */
	ext2_sector_read (p, inode->block[0], (unsigned char *) ext2dir);

	unsigned dir_next = 0;
	unsigned entry = 0;
	unsigned name_len = strlen (name);

	/* loop for find name of directory, where we want go */
	for (;;) {
		ext2_dir_t *dir_new = (ext2_dir_t *) ((void *) ext2dir + dir_next);

		if (!dir_new->rec_len || !dir_new->inode || dir_next >= 1024)
			break;

		DPRINT (DBG_DRIVER | DBG_FS, "dir -> 0x%x : %d - %d - %d - %s", dir_new, dir_new->inode, dir_new->rec_len, dir_new->name_len, dir_new->name);

		/* is it really directory ? */
		if (dir_new->file_type == EXT2_FT_DIR)
		if (!strncmp (name, dir_new->name, name_len)) {
			/* we've found inode of directory, what we need. Let's get this inode structure */
			ext2_inode_t *inode_new = ext2_inode_read (p, block, dir_new->inode);

			DPRINT (DBG_DRIVER | DBG_FS, "dir -> dir_new->inode: %d", dir_new->inode);

			if (!inode_new)
				goto flush;

			inode_curr = dir_new->inode;

			DPRINT (DBG_DRIVER | DBG_FS, "dir -> %d %d %d %d %d", inode_new->block[0], inode_new->block[1], inode_new->block[2], inode_new->block[13], inode_new->block[14]);
			
			/* assign new content to dir[] structure */
			ext2_dir_scan (p, inode_new);

			goto flush;
		}

		dir_next += dir_new->rec_len;
		entry ++;
	}

flush:
	kfree (ext2dir);
//	kfree (block);

	return 1;
}

unsigned ext2_cat (partition_t *p, char *name, vfs_content_t *content)
{
	char *block = (char *) kmalloc (1024);
		
	if (!block)
		return 0;

	DPRINT (DBG_DRIVER | DBG_FS, "cat -> %d", inode_curr);

	/* read inode structure by current inode id number */
	ext2_inode_t *inode = ext2_inode_read (p, block, inode_curr);

	if (!inode) {
		kfree (block);
		return 0;
	}

	ext2_dir_t *ext2dir = (ext2_dir_t *) kmalloc (1024);

	if (!ext2dir) {
		kfree (block);
		return 0;
	}

	/* read direct block */
	ext2_sector_read (p, inode->block[0], (unsigned char *) ext2dir);

	unsigned dir_next = 0;
	unsigned name_len = strlen (name);

	for (;;) {
		ext2_dir_t *dir_new = (ext2_dir_t *) ((void *) ext2dir + dir_next);

		if (!dir_new->rec_len || !dir_new->inode || dir_next >= 1024)
			break;

		/* check for file type - regular file */
		if (dir_new->file_type == EXT2_FT_REG_FILE)
		if (!strncmp (name, dir_new->name, name_len)) {
			ext2_inode_t *inode_new = ext2_inode_read (p, block, dir_new->inode);

			DPRINT (DBG_DRIVER | DBG_FS, "dir -> dir_new->inode: %d", dir_new->inode);

			if (!inode_new)
				goto flush;

			DPRINT (DBG_DRIVER | DBG_FS, "dir -> %d - %d %d %d %d %d", inode_new->size, inode_new->block[0], inode_new->block[1], inode_new->block[2], inode_new->block[13], inode_new->block[14]);

			/* when file is not clear and direct block is available,
					read file content*/
			if (inode_new->size && inode_new->block[0])
				ext2_block_read (p, inode_new, content);

			goto flush;
		}

		dir_next += dir_new->rec_len;
	}

flush:
	kfree (ext2dir);
	kfree (block);

	return 1;
}

unsigned ext2_write_file (partition_t *p, char *name)
{
	char *block = (char *) kmalloc (1024);
		
	if (!block)
		return 0;

	DPRINT (DBG_DRIVER | DBG_FS, "write_file -> %d", inode_curr);

	/* read inode structure by current inode id number */
	ext2_inode_t *inode = ext2_inode_read (p, block, inode_curr);

	if (!inode) {
		kfree (block);
		return 0;
	}

	ext2_dir_t *ext2dir = (ext2_dir_t *) kmalloc (1024);

	if (!ext2dir) {
		kfree (block);
		return 0;
	}

	/* read direct block */
	ext2_sector_read (p, inode->block[0], (unsigned char *) ext2dir);

	unsigned dir_next = 0;
	unsigned name_len = strlen (name);

	for (;;) {
		ext2_dir_t *dir_new = (ext2_dir_t *) ((void *) ext2dir + dir_next);

		if (!dir_new->rec_len || !dir_new->inode || dir_next >= 1024)
			break;

		/* check for file type - regular file */
		if (dir_new->file_type == EXT2_FT_REG_FILE)
		if (!strncmp (name, dir_new->name, name_len)) {
			ext2_inode_t *inode_new = ext2_inode_read (p, block, dir_new->inode);

			DPRINT (DBG_DRIVER | DBG_FS, "dir -> dir_new->inode: %d", dir_new->inode);

			if (!inode_new)
				goto flush;

			DPRINT (DBG_DRIVER | DBG_FS, "dir -> %d - %d %d %d %d %d", inode_new->size, inode_new->block[0], inode_new->block[1], inode_new->block[2], inode_new->block[13], inode_new->block[14]);

			/* write chunk of data */
			ext2_block_write (p, inode_new);

			goto flush;
		}

		dir_next += dir_new->rec_len;
	}

flush:
	kfree (ext2dir);
	kfree (block);

	return 1;
}

unsigned mkext2 (partition_t *p)
{
	if (!p)
		return 0;

	if (!superblock)
		superblock = (ext2_superblock_t *) kmalloc (sizeof (ext2_superblock_t));

	if (!superblock)
		return 0;

	superblock->inodecount = 2;
	//superblock->uuid = ;
	superblock->magic = EXT2_MAGIC;
	superblock->inode_size = 128;
	superblock->log_block_size = 0;
	superblock->first_ino = 2;
	superblock->rev_level = 0;
	superblock->inodes_per_group = 1832;

	/* write superblock from drive on first sector */
	ext2_sector_write (p, EXT2_SUPERBLOCK_SECTOR, (unsigned char *) superblock);

	DPRINT (DBG_DRIVER | DBG_FS, "superblock -> %d, %d - %s - %x - %d", sizeof (ext2_superblock_t), superblock->inodecount, superblock->uuid, superblock->magic, superblock->inode_size);

	/* check ext2 magic number */
	if (superblock->magic != EXT2_MAGIC) {
		kfree (superblock);
		return 0;
	}

	/* read block size (default is 1024) */
	block_size = 1024 << superblock->log_block_size;

	DPRINT (DBG_DRIVER | DBG_FS, "superblock -> %d - %d - %d - %d", block_size, superblock->first_ino, superblock->rev_level, superblock->inodes_per_group);


	/* group descriptors */
	ext2_group_t *group = (ext2_group_t *) kmalloc (1024);

	if (!group)
		return 0;

	/* read group decriptor */
	ext2_sector_write (p, 2, (unsigned char *) group);

	DPRINT (DBG_DRIVER | DBG_FS, "group -> %d - %d - %d", group[0].block_bitmap, group[0].inode_bitmap, group[0].inode_table);

	/* inode tables */
	ext2_inode_t *inode = (ext2_inode_t *) kmalloc (1024);

	if (!inode)
		return 0;

	/* read root inode */
	ext2_sector_read (p, group[0].inode_table, (unsigned char *) inode);

	DPRINT (DBG_DRIVER | DBG_FS, "inode -> 0x%x - %d - %d - %d", inode[1].mode, inode[1].ctime, inode[1].size, inode[1].block[0]);

	/* read dir entries from root inode */
	ext2_dir_scan (p, &inode[EXT2_ROOT_INO-1]);

	inode_curr = EXT2_ROOT_INO;

	kfree (inode);
	kfree (group);

	return 1;
}

unsigned ext2_init (partition_t *p)
{
	if (!p)
		return 0;

	superblock = (ext2_superblock_t *) kmalloc (sizeof (ext2_superblock_t));

	if (!superblock)
		return 0;

	/* read superblock from drive on first sector */
	ext2_sector_read (p, EXT2_SUPERBLOCK_SECTOR, (unsigned char *) superblock);

	DPRINT (DBG_DRIVER | DBG_FS, "superblock -> %d, %d - %s - %x - %d", sizeof (ext2_superblock_t), superblock->inodecount, superblock->uuid, superblock->magic, superblock->inode_size);

	/* check ext2 magic number */
	if (superblock->magic != EXT2_MAGIC) {
		kfree (superblock);
		return 0;
	}

	/* read block size (default is 1024) */
	block_size = 1024 << superblock->log_block_size;

	block_free = 128;

	DPRINT (DBG_DRIVER | DBG_FS, "superblock -> %d - %d - %d - %d", block_size, superblock->first_ino, superblock->rev_level, superblock->inodes_per_group);

	return 1;
}

bool ext2_handler (unsigned act, char *block, unsigned n, unsigned long l)
{
	switch (act) {
		case FS_ACT_INIT:
		{
			return (bool) ext2_init ((partition_t *) block);
		}
		break;
		case FS_ACT_READ:
		{
			return (bool) ext2_cat (curr_part, dir[n].name, (vfs_content_t *) block);
		}
		break;
		case FS_ACT_WRITE:
		{
			ext2_write_file (curr_part, dir[n].name);

			return 1;
		}
		break;
		case FS_ACT_CHDIR:
		{
			/* root directory - mount action*/
			if (n == -1)
				return (bool) ext2_cd_root (curr_part);

			/* walk to another directory */
			return (bool) ext2_cd (curr_part, block);
		}
		break;
		case FS_ACT_MKDIR:
		{
			return ext2_create_entity (curr_part, block, EXT2_FT_DIR);
		}
		break;
		case FS_ACT_TOUCH:
		{
			return ext2_create_entity (curr_part, block, EXT2_FT_REG_FILE);
		}
	}

	return 0;
}
#endif
