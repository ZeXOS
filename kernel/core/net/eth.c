/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <dev.h>
#include <net/eth.h>
#include <net/if.h>
#include <mutex.h>

/* Mutex for data queue */
MUTEX_CREATE (mutex_queue_rx);

unsigned char netdev_count = 0;

netdev_t *netdev_create (mac_addr_t addr_mac, unsigned (*read) (char *, unsigned), unsigned (*write) (char *, unsigned), unsigned addr_io)
{
	struct netdev_t *dev;

	/* alloc and init context */
	dev = (struct netdev_t *) kmalloc (sizeof (struct netdev_t));

	if (!dev)
		return 0;

	memcpy (dev->dev_addr, (void *) addr_mac, sizeof (mac_addr_t));

	/* assign ethernet id name */
	dev->name = (unsigned char *) kmalloc (sizeof (unsigned char) * 6);

	if (!dev->name) {
		kfree (dev);
		return 0;
	}

	sprintf (dev->name, "eth%d", netdev_count);

	dev->base_addr = addr_io;

	dev->read = read;
	dev->write = write;

	dev->info_rx = 0;
	dev->info_tx = 0;

	dev->queue_rx_cnt = 0;
	dev->queue_tx_cnt = 0;

	/* alloc structure for received data queue */
	dev->queue_rx_list.next = &dev->queue_rx_list;
	dev->queue_rx_list.prev = &dev->queue_rx_list;

	dev->queue_tx_list.next = &dev->queue_tx_list;
	dev->queue_tx_list.prev = &dev->queue_tx_list;

	if (!netif_create (dev)) {
		kfree (dev->name);
		kfree (dev);
		return 0;
	}

	netdev_count ++;

	return dev;
}

unsigned netdev_rx_add_queue (struct netdev_t *dev, char *buffer, unsigned len)
{
	mutex_lock (&mutex_queue_rx);

	if (!dev)
		goto unlock;

	dev->info_rx += len;

	netdev_buffer_queue_t *queue;

	/* alloc and init context */
	queue = (netdev_buffer_queue_t *) kmalloc (sizeof (netdev_buffer_queue_t));

	if (!queue)
		goto unlock;

	queue->len = len;
	queue->buf = (char *) kmalloc (sizeof (char) * (len + 1));

	if (!queue->buf) {
		kfree (queue);
		goto unlock;
	}

	memcpy (queue->buf, buffer, len);
	queue->buf[len] = '\0';

	/* add into list */
	queue->next = &dev->queue_rx_list;
	queue->prev = dev->queue_rx_list.prev;
	queue->prev->next = queue;
	queue->next->prev = queue;
	
	mutex_unlock (&mutex_queue_rx);

	return 1;
unlock:
	mutex_unlock (&mutex_queue_rx);

	return 0;
}

netdev_buffer_queue_t *netdev_rx_queue (struct netdev_t *dev)
{
	if (!dev)
		return 0;

	netdev_buffer_queue_t *queue;
	for (queue = dev->queue_rx_list.next; queue != &dev->queue_rx_list; queue = queue->next)
		return queue;

	return 0;
}

unsigned netdev_rx_queue_flush (struct netdev_t *dev, netdev_buffer_queue_t *queue)
{
	if (!dev)
		return 0;

	queue->next->prev = queue->prev;
	queue->prev->next = queue->next;

	kfree (queue->buf);
	kfree (queue);

	return 1;
}

int netdev_rx (struct netdev_t *dev, char *buf, unsigned len)
{
	if (!dev)
		return 0;

	/* are there any available data in queue ? */
	netdev_buffer_queue_t *queue = netdev_rx_queue (dev);

	if (!queue)
		return 0;

	unsigned l = queue->len;

	if (l >= len)	/* TODO - received packet is longer then len (> ~MTU) */
		return -1;

	/* copy available data from queue to our buffer */
	memcpy (buf, queue->buf, l);

	buf[l] = '\0';
	
	/* clean old queue entry */
	netdev_rx_queue_flush (dev, queue);

	/* return available data */
	return l;
}
