/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <appcl.h>

int result;
int pos;
int mem;
char op;

void zcalc_draw ()
{
	zgui_puts (10, 10, "ZCalc", 0);
	
	char res[32];
	sprintf (res, "= %d", result);
	
	zgui_puts (10, 20, res, 0);

	char btn[16];
	
	btn[1] = zgui_button (10, 100, "1");
	btn[2] = zgui_button (35, 100, "2");
	btn[3] = zgui_button (60, 100, "3");
	btn[12] = zgui_button (85, 100, "*");
	
	btn[4] = zgui_button (10, 75, "4");
	btn[5] = zgui_button (35, 75, "5");
	btn[6] = zgui_button (60, 75, "6");
	btn[13] = zgui_button (85, 75, "/");
	
	btn[7] = zgui_button (10, 50, "7");
	btn[8] = zgui_button (35, 50, "8");
	btn[9] = zgui_button (60, 50, "9");
	btn[15] = zgui_button (85, 50, "C");
	
	btn[0] = zgui_button (10, 125, "0");
	btn[10] = zgui_button (35, 125, "+");
	btn[11] = zgui_button (60, 125, "-");
	btn[14] = zgui_button (85, 125, "=");
	
	int i;
	for (i = 0; i < 10; i ++)
		if (!btn[i]) {
			result *= pos;
			result += i;
			pos = 10;
		}
	
	/* C */
	if (!btn[15]) {
		result = 0;
		pos = 1;
		mem = 0;
	}
	
	/* + */
	if (!btn[10]) {
		mem += result;
		result = 0;
		pos = 1;
		op = '+';
	}
	
	/* - */
	if (!btn[11]) {
		if (mem)
			mem -= result;

		result = 0;
		pos = 1;
		op = '-';
	}
	
	/* * */
	if (!btn[12]) {
		if (mem)
			mem *= result;
		
		result = 0;
		pos = 1;
		op = '*';
	}
	
	/* / */
	if (!btn[13]) {
		if (mem)
			mem /= result;
		
		result = 0;
		pos = 1;
		op = '/';
	}
	
	/* = */
	if (!btn[14]) {
		switch (op) {
			case '+':
				mem += result;
				result = mem;
				break;
			case '-':
				mem -= result;
				result = mem;
				break;
			case '*':
				mem *= result;
				result = mem;
				break;
			case '/':
				mem /= result;
				result = mem;
				break;
		}

		mem = 0;
		op = 0;
	}
}

int main (int argc, char **argv)
{
	int exit = 0;
	
	result = 0;
	pos = 1;
	mem = 0;
	op = 0;

	if (zgui_init () == -1)
		return -1;

	zgui_resize (120, 200);
	
	while (!exit) {
		unsigned state = zgui_event ();

		if (state & APPCL_STATE_REDRAW)
			zcalc_draw ();

		if (state & APPCL_STATE_EXIT)
			exit = 1;
	}

	return zgui_exit ();
}
 
