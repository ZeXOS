/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define		ESC	1

int sock;

static struct sockaddr_in serverSock;		// Remote socket
static struct hostent *host;             	// Remote host

extern int http_request (char *address, unsigned address_len, char *dir, unsigned dir_len);

int init (char *address)
{
	// Let's get info about remote computer
	if ((host = gethostbyname ((char *) address)) == NULL) {
		//printf ("Wrong address -> %s:%d\n", addr, port);
		printf ("Wrong address\n");
		return 0;
	}

	// Create socket
	if ((sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return 0;
	}
	
	// Fill structure sockaddr_in
	// 1) Family of protocols
	serverSock.sin_family = AF_INET;
	// 2) Number of server port
	serverSock.sin_port = htons (80);
	// 3) Setup ip address of server, where we want to connect
	memcpy (&(serverSock.sin_addr), host->h_addr, host->h_length);
	
	// Now we are able to connect to remote server
	if (connect (sock, (struct sockaddr *) &serverSock, sizeof (serverSock)) == -1) {
		printf ("Connection cant be estabilished -> %s:%d\n", address, 80);
		return -1;
	}

	printf ("webcl -> connected to %s:80 server\n", address);

	return http_request (address, strlen (address), "/", 1);
}

int loop ()
{

	return 1;
}
