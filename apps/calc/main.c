/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define		ESC				1
#define		ARROWLEFT			75
#define		ARROWRIGHT			77
#define		ARROWUP				72
#define		ARROWDOWN			80
#define		ENTER				28

char *buf;
char opt;
unsigned buf_len;

unsigned char curr_button_x = 0;
unsigned char curr_button_y = 0;

int number = 0;

unsigned key_pressed (int keycode)
{
	int scancode = getkey ();

	if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;
}

int number_get ()
{
	int num = 0;
	char key = 0;
	
	while (key != '\n') {
		key = getch ();

		if ((key >= '0' && key <= '9') || key == '\b') {
			buf[num] = key;
			num ++;
			putch (key);
		}

		schedule ();
	}

	buf[num] = '\0';

	return atoi (buf);
}

char opt_get ()
{
	int num = 0;
	char key = 0;
	
	while (key != '\n') {
		key = getch ();
		if (key) {
			buf[num] = key;
			num ++;
			putch (key);
		}
	}

	return buf[0];
}

void draw_button (unsigned char x, unsigned char y, unsigned active, char c)
{
	

	if (active == 1) {
		gotoxy (x+1, y+1);
		setcolor (14, 0);
		putch (175);
		putch (c);
		putch (174);
	} else {
		gotoxy (x+1, y+1);
		setcolor (7, 0);
		putch (' ');
		putch (c);
		putch (' ');
	}


	setcolor (15, 0);

	// levy horni roh
	gotoxy (x, y);
	putch (218);

	putch (196);
	putch (196);
	putch (196);

	// pravy horni roh
	putch (191);

	gotoxy (x, y+1);
	putch (179);
	
	gotoxy (x+4, y+1);
	putch (179);

	// levy dolni roh
	gotoxy (x, y+2);
	putch (192);

	putch (196);
	putch (196);
	putch (196);

	// pravy dolni roh
	putch (217);
}

void draw_calc ()
{
	unsigned r = 0;

	/* okraje */
	gotoxy (0, 0);
	// levy horni roh
	putch (201);

	for (r = 0; r < 25; r ++)
		putch (205);

	// pravy horni roh
	putch (187);

	for (r = 1; r < 20; r ++) {
		gotoxy (0, r);
		putch (186);
	}

	for (r = 1; r < 20; r ++) {
		gotoxy (26, r);
		putch (186);
	}

	putch ('\n');

	// levy dolni roh
	putch (200);

	for (r = 0; r < 25; r ++)
		putch (205);

	// pravy dolni roh
	putch (188);

	
	/* display */

	// levy horni roh
	gotoxy (2, 2);
	putch (218);

	for (r = 0; r < 21; r ++)
		putch (196);

	// pravy horni roh
	putch (191);

	gotoxy (2, 3);
	putch (179);
	
	gotoxy (24, 3);
	putch (179);

	// levy dolni roh
	gotoxy (2, 4);
	putch (192);

	for (r = 0; r < 21; r ++)
		putch (196);

	// pravy dolni roh
	putch (217);
}

void display_write (char *str)
{
	/* clear display */
	gotoxy (3, 3);
/*	for (r = 0; r < 21; r ++)
		putch (' ');*/

	//unsigned l = strlen (str);

	//gotoxy (24-l, 3);
	printf (str);
}

unsigned buf_i = 0;
unsigned key_handler ()
{
	int scancode = getkey ();

	if (!scancode)
		return 2;

	if (buf_i)
		display_write (buf);

	char key = 0;
	switch (scancode) {
		case ESC:
			return 0;
		case ARROWUP:
			if (curr_button_y > 0)
				curr_button_y --;

			break;
		case ARROWDOWN:
			if (curr_button_y < 3)
				curr_button_y ++;
			break;
		case ARROWLEFT:
			if (curr_button_x > 0)
				curr_button_x --;
			break;
		case ARROWRIGHT:
			if (curr_button_x < 3)
				curr_button_x ++;
			break;
		case ENTER:
			/* 7 */
			if (curr_button_x == 0 && curr_button_y == 1) {
				buf[buf_i] = '7';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 8 */
			if (curr_button_x == 1 && curr_button_y == 1) {
				buf[buf_i] = '8';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 9 */
			if (curr_button_x == 2 && curr_button_y == 1) {
				buf[buf_i] = '9';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 4 */
			if (curr_button_x == 0 && curr_button_y == 2) {
				buf[buf_i] = '4';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 5 */
			if (curr_button_x == 1 && curr_button_y == 2) {
				buf[buf_i] = '5';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 6 */
			if (curr_button_x == 2 && curr_button_y == 2) {
				buf[buf_i] = '6';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 1 */
			if (curr_button_x == 0 && curr_button_y == 3) {
				buf[buf_i] = '1';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 2 */
			if (curr_button_x == 1 && curr_button_y == 3) {
				buf[buf_i] = '2';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 3 */
			if (curr_button_x == 2 && curr_button_y == 3) {
				buf[buf_i] = '3';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}
			/* 0 */
			if (curr_button_x == 3 && curr_button_y == 2) {
				buf[buf_i] = '0';
				buf_i ++;
				buf[buf_i+1] = '\0';
				break;
			}

			/* + */
			if (curr_button_x == 3 && curr_button_y == 0) {
				if (buf_i > 0)
					break;

				number += atoi (buf);

				//memset (buf, 0, 15);
				itoa (number, buf, 10);
				//buf[15] = '\0';
				//display_write (buf);
				//buf_i = strlen (buf);
				break;
			}

			/* = */
			if (curr_button_x == 3 && curr_button_y == 3) {
				if (buf_i > 0)
					break;

				//memset (buf, 0, 15);
				itoa (number, buf, 10);
				//gotoxy (3,3);
				printf (buf);
				sleep (5);
				//buf_i = strlen (buf);
				break;
			}

			break;
	}

			key = getch ();
			if (key >= '0' && key <= '9') {
				buf[buf_i] = key;
				buf_i ++;
				buf[buf_i+1] = '\0';
				//gotoxy (3,3);
				//unsigned x = 0;
				//for (x = 0; x < buf_i; x ++)
				//	putch (buf[x]);
			}

/*
	// 1. lajna
	draw_button (2, 7, 0, '/');
	draw_button (8, 7, 0, '*');
	draw_button (14, 7, 0, '-');
	draw_button (20, 7, 0, '+');

	// 2. lajna
	draw_button (2, 10, 0, '7');
	draw_button (8, 10, 0, '8');
	draw_button (14, 10, 0, '9');
	draw_button (20, 10, 0, 'C');

	// 3. lajna
	draw_button (2, 13, 0, '4');
	draw_button (8, 13, 0, '5');
	draw_button (14, 13, 0, '6');
	draw_button (20, 13, 0, '0');

	// 4. lajna
	draw_button (2, 16, 0, '1');
	draw_button (8, 16, 0, '2');
	draw_button (14, 16, 0, '3');
	draw_button (20, 16, 0, '=');
*/

	if (curr_button_x == 0 && curr_button_y == 0)
		draw_button (2, 7, 1, '/');
	else
		draw_button (2, 7, 0, '/');

	if (curr_button_x == 1 && curr_button_y == 0)
		draw_button (8, 7, 1, '*');
	else
		draw_button (8, 7, 0, '*');

	if (curr_button_x == 2 && curr_button_y == 0)
		draw_button (14, 7, 1, '-');
	else
		draw_button (14, 7, 0, '-');

	if (curr_button_x == 3 && curr_button_y == 0)
		draw_button (20, 7, 1, '+');
	else
		draw_button (20, 7, 0, '+');

	if (curr_button_x == 0 && curr_button_y == 1)
		draw_button (2, 10, 1, '7');
	else
		draw_button (2, 10, 0, '7');

	if (curr_button_x == 1 && curr_button_y == 1)
		draw_button (8, 10, 1, '8');
	else
		draw_button (8, 10, 0, '8');

	if (curr_button_x == 2 && curr_button_y == 1)
		draw_button (14, 10, 1, '9');
	else
		draw_button (14, 10, 0, '9');

	if (curr_button_x == 3 && curr_button_y == 1)
		draw_button (20, 10, 1, 'C');
	else
		draw_button (20, 10, 0, 'C');

	if (curr_button_x == 0 && curr_button_y == 2)
		draw_button (2, 13, 1, '4');
	else
		draw_button (2, 13, 0, '4');

	if (curr_button_x == 1 && curr_button_y == 2)
		draw_button (8, 13, 1, '5');
	else
		draw_button (8, 13, 0, '5');

	if (curr_button_x == 2 && curr_button_y == 2)
		draw_button (14, 13, 1, '6');
	else
		draw_button (14, 13, 0, '6');

	if (curr_button_x == 3 && curr_button_y == 2)
		draw_button (20, 13, 1, '0');
	else
		draw_button (20, 13, 0, '0');

	if (curr_button_x == 0 && curr_button_y == 3)
		draw_button (2, 16, 1, '1');
	else
		draw_button (2, 16, 0, '1');

	if (curr_button_x == 1 && curr_button_y == 3)
		draw_button (8, 16, 1, '2');
	else
		draw_button (8, 16, 0, '2');

	if (curr_button_x == 2 && curr_button_y == 3)
		draw_button (14, 16, 1, '3');
	else
		draw_button (14, 16, 0, '3');

	if (curr_button_x == 3 && curr_button_y == 3)
		draw_button (20, 16, 1, '=');
	else
		draw_button (20, 16, 0, '=');

	setcolor (15, 0);

	draw_calc ();

	return 1;
}

void textualgui ()
{
	draw_calc ();

	//display_write ("Ahoj svete !!");

	unsigned t = 1;
	curr_button_x = 0;
	curr_button_y = 0;
	buf_i = 0;

	buf = (char *) malloc (sizeof (char) * 16);
	buf[0] = '\0';
	buf[1] = '\0';
	buf[2] = '\0';
	buf_len = 0;

	number = 0;

	while (1) {
		if (t > 1000) {
			/* exit app */
			if (!key_handler ())
				break;

			t = 0;
		}

		schedule ();
		t ++;
	}
}

void textual ()
{
	int a = 0, b = 0;
	int res = 0;
	char oper = 0;

	buf = (char *) malloc (sizeof (char) * 16);

	printf ("Calc\n\n");

	printf ("\nType first number: ");
	a = number_get ();

	printf ("\nSecond number: ");
	b = number_get ();

	printf ("\nChoose operation (+,-,*,/): ");
	
	oper = opt_get ();

	if (oper == '+')
		res = a + b;
	else if (oper == '-')
		res = a - b;
	else if (oper == '*')
		res = a * b;
	else if (oper == '/')
		res = a / b;
	
	printf ("\nResult: %d\n", res);

	free (buf);
}

int main (int argc, char **argv) // like main in a normal C program
{
	cls ();

	if (argc > 1) {
		if (!strcmp (argv[1], "-g"))
			textualgui ();
	} else
		textual ();

	return 0;
}
