/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _COMMANDS_H
#define _COMMANDS_H

#include <system.h>

/* Command structure */
typedef unsigned (command_handler_t) (char *command, unsigned len);

typedef struct command_context {
	struct command_context *next, *prev;
      
	char *name;
	char *desc;
	unsigned flags;
	command_handler_t *handler;
} command_t;


/* registering commands */
extern unsigned command_register (char *name, char *desc, command_handler_t *handler, unsigned flags);
extern unsigned command_unregister (char *name);
extern unsigned command_parser (char *command, unsigned len);
extern unsigned init_commands ();

#endif
