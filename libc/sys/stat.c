/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <vfs.h>

static vfs_ent_t *getent (const char *path)
{
	vfs_ent_t *d = (vfs_ent_t *) 0;
  
	asm volatile (
		"movl $25, %%eax;"
	     	"movl %1, %%ebx;"
	     	"int $0x80;"
		"movl %%eax, %0;"
		: "=g" (d) : "b" (path) : "%eax");

	return d;
}

int stat (const char *path, struct stat *buf)
{
	if (!path || !buf) {
		errno = ENOENT;
		return -1;
	}

	vfs_ent_t *ent = getent (path);
	
	if (!ent) {
		errno = ENOENT;
		return -1;
	}

	if (ent->attrib & VFS_FILEATTR_FILE)
		buf->st_mode |= S_IFREG;
	else if (ent->attrib & VFS_FILEATTR_DIR)
		buf->st_mode |= S_IFDIR;

	if (ent->attrib & VFS_FILEATTR_DEVICE)
		buf->st_mode |= S_IFCHR;

	/*FILE *f = fopen (path, "r");

	if (!f)
		return -1;

	fseek (f, 0, SEEK_END);
	unsigned flen = ftell (f);
	fseek (f, 0, SEEK_SET);
	
	fclose (f);
	
	buf->st_size = flen;*/

	return 0;
}


