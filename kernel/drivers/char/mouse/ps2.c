/*
 *  ZeX/OS
 *  Copyright (C) 2009  Martin 'povik' Poviser (martin.povik@gmail.com)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <arch/io.h>
#include <mouse.h>
#include <task.h>

#define KBD_CCMD_KBD_DISABLE		0xAD

#define PS2MOUSE_TIMEOUT 		500000
#define PS2MOUSE_ACK			0xFA
#define PS2MOUSE_PORT			0x60
#define PS2MOUSE_CTRL			0x64
#define PS2MOUSE_COMMAND		0xD4
#define PS2MOUSE_COMMAND_RESET		0xFF
#define PS2MOUSE_COMMAND_GET_MOUSE_ID	0xF2
#define PS2MOUSE_COMMAND_GET_PACKET	0xEB
#define PS2MOUSE_COMMAND_ENABLE_PACKETS 0xF4
#define PS2MOUSE_COMMAND_SET_SAMPLERATE	0xF3
#define PS2MOUSE_COMMAND_SET_RESOLUTION	0xE8
#define PS2MOUSE_COMMAND_SET_REMOTE	0xF0
#define PS2MOUSE_RESET_ACK		0xAA
#define PS2MOUSE_IRQ			12

#define PS2MOUSE_SAMPLERATE_80		0x50
#define PS2MOUSE_SAMPLERATE_100		0x64
#define PS2MOUSE_SAMPLERATE_200		0xC8

#define PS2MOUSE_SAMPLERATE 		PS2MOUSE_SAMPLERATE_100


static unsigned ps2mouse_extwheel = 0;
static unsigned ps2mouse_extbuttons = 0;

static dev_mouse_t ps2mouse_state;

static unsigned ps2mouse_reset ();
static unsigned ps2mouse_write (unsigned char command);
static unsigned char ps2mouse_read ();

/* ps2mouse_reset - reset mouse */

unsigned ps2mouse_reset ()
{
	int timeout;

	for (timeout = 0; timeout < PS2MOUSE_TIMEOUT; timeout ++) {
		if((inb (PS2MOUSE_CTRL) && 0x02) != 0x02)
			break;
	}

	outb (PS2MOUSE_CTRL, PS2MOUSE_COMMAND);
	outb (PS2MOUSE_PORT, PS2MOUSE_COMMAND_RESET);

	int ack = 0;
	
	for (timeout = 0; timeout < PS2MOUSE_TIMEOUT; timeout ++) {
	  	unsigned char state = ps2mouse_read ();
		
		if (state == PS2MOUSE_ACK)
			ack = 1;
		
		if (ack && state == PS2MOUSE_RESET_ACK)
			return 1;
		
		if (state == -1)
			return 0;
	}

	return -1;
}

/* ps2mouse_write - send a command to mouse */

unsigned ps2mouse_write (unsigned char command)
{
	int timeout;

	for (timeout = 0; timeout < PS2MOUSE_TIMEOUT; timeout ++) {
		if((inb (PS2MOUSE_CTRL) && 0x02) != 0x02)
			break;
	}

	outb (PS2MOUSE_CTRL, PS2MOUSE_COMMAND);
	outb (PS2MOUSE_PORT, command);

	short read = ps2mouse_read ();

	if (read == PS2MOUSE_ACK)
		return 1;
	
	if (read == -1)
		return 0;

	return 0;
}

/* ps2mouse_read - read a value from mouse */

unsigned char ps2mouse_read ()
{
	int timeout;

	for (timeout = 0; timeout < PS2MOUSE_TIMEOUT; timeout ++) {
		if ((inb (PS2MOUSE_CTRL) && 0x01) == 0x01)
			return inb (PS2MOUSE_PORT);
	}

	return -1;
}

/* ps2mouse_get_state - get a mouse state */

unsigned ps2mouse_get_state ()
{
	unsigned char b1, b2, b3, b4 = 0;
	
	if (ps2mouse_write (PS2MOUSE_COMMAND_GET_PACKET) == -1)
		return 0;
	
	b1 = ps2mouse_read ();
	b2 = ps2mouse_read ();
	b3 = ps2mouse_read ();
	
	if (ps2mouse_extwheel || ps2mouse_extbuttons)
		b4 = ps2mouse_read ();

	ps2mouse_state.flags = 0;
	ps2mouse_state.flags |= ((b1 & 1) ? MOUSE_FLAG_BUTTON1 : 0);
	ps2mouse_state.flags |= ((b1 & 2) ? MOUSE_FLAG_BUTTON2 : 0);
	ps2mouse_state.flags |= ((b1 & 3) ? MOUSE_FLAG_BUTTON3 : 0);
	ps2mouse_state.flags |= ((b1 & 4) ? MOUSE_FLAG_BUTTON4 : 0);
	ps2mouse_state.flags |= ((b1 & 5) ? MOUSE_FLAG_BUTTON5 : 0);

	short dx, dy, dz;

	dx = (b1 & 0x10) ? b2 - 256 : b2;
	dy = (b1 & 0x20) ? -(b3 - 256) : -b3;
	dz = (b4 & 0x08) ? (b4 & 7) - 8 : b4 & 7;

	if (dx > 5 || dx < -5)
		dx *= 4;
	if (dy > 5 || dy < -5)
		dy *= 4;
	
	ps2mouse_state.pos_x = dx;
	ps2mouse_state.pos_y = dy;

	if (dz > 0)
		ps2mouse_state.flags = ps2mouse_state.flags | MOUSE_FLAG_SCROLLUP;
	if (dz < 0)
		ps2mouse_state.flags = ps2mouse_state.flags | MOUSE_FLAG_SCROLLDOWN;

	return 1;
}

unsigned char ps2mouse_get_mouseid ()
{
	short mouse_id;
  
	ps2mouse_write (PS2MOUSE_COMMAND_SET_SAMPLERATE);
	ps2mouse_write (PS2MOUSE_SAMPLERATE_200);

	ps2mouse_write (PS2MOUSE_COMMAND_SET_SAMPLERATE);
	ps2mouse_write (PS2MOUSE_SAMPLERATE_100);

	ps2mouse_write (PS2MOUSE_COMMAND_SET_SAMPLERATE);
	ps2mouse_write (PS2MOUSE_SAMPLERATE_80);

	ps2mouse_write (PS2MOUSE_COMMAND_GET_MOUSE_ID);
	mouse_id = ps2mouse_read ();

	if (mouse_id < 3)
		goto ret;
	else 
		ps2mouse_extwheel = true;

	ps2mouse_write (PS2MOUSE_COMMAND_SET_SAMPLERATE);
	ps2mouse_write (PS2MOUSE_SAMPLERATE_200);

	ps2mouse_write (PS2MOUSE_COMMAND_SET_SAMPLERATE);
	ps2mouse_write (PS2MOUSE_SAMPLERATE_200);

	ps2mouse_write (PS2MOUSE_COMMAND_SET_SAMPLERATE);
	ps2mouse_write (PS2MOUSE_SAMPLERATE_80);

	ps2mouse_write (PS2MOUSE_COMMAND_GET_MOUSE_ID);
	mouse_id = ps2mouse_read ();

	if (mouse_id > 3)
		ps2mouse_extbuttons = true;
ret:
	return mouse_id;
}

unsigned ps2mouse_init ()
{
	int timeout;

	ps2mouse_extbuttons = false;
	ps2mouse_extwheel = false;

	if (ps2mouse_reset () != 1)
		return 0;
		
	unsigned char type = ps2mouse_read ();

	if (type != 0) {	/* mouse type: 0 = PS2, 3 = IMPS/2, 4 = IMEX */
		kprintf ("> ps2mouse : failed while detecting mouse type (%d), should be (0)\n", type);
		return 0;
	}
	
	if (ps2mouse_get_mouseid () == -1)
		return 0;
	
	ps2mouse_write (PS2MOUSE_COMMAND_SET_REMOTE);
	ps2mouse_write (PS2MOUSE_COMMAND_ENABLE_PACKETS);

	return 1;
}

void ps2mouse_update ()
{
	for (;; schedule ()) {
		if (!ps2mouse_state.pos_x && !ps2mouse_state.pos_y)
			ps2mouse_get_state ();
	}
}

bool ps2mouse_acthandler (unsigned act, char *block, unsigned block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{	
			dev_flags_t *flags = (dev_flags_t *) block;

			if (!flags)
				return 0;

			if (block_len != sizeof (dev_flags_t))
				return 0;

			unsigned r = ps2mouse_init ();
			
			if (!r)
				return 0;

			flags->iomem = (void *) &ps2mouse_state;
			flags->iolen = sizeof (dev_mouse_t);

			//task_t *task = task_create ("mouseps2", (unsigned) &ps2mouse_update, 255);

			return r;
		}
		break;
		case DEV_ACT_READ:
		{
			memcpy (block, &ps2mouse_state, sizeof (dev_mouse_t));

			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{
			memcpy (&ps2mouse_state, block, sizeof (dev_mouse_t));

			return 1;
		}
		break;
		case DEV_ACT_UPDATE:
		{
			ps2mouse_get_state ();
			
			return 1;
		}
		break;
	}

	return 0;
}

#endif
