/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <arch/ctx.h>
#include <task.h>

static task_t *task_old;
int arm_critical_section;


static void task_init ()
{
	/* enable interrupts when we're in task entry point for first time */
	int_enable ();

	/* get current task */
	task_t *c = _curr_task;

	/* get non-real entry point - just function what we want to call */
	task_handler_t *handler = (task_handler_t *) c->entry;

	/* call our function */
	handler ();

	/* when non-real task function is finished, we should push out this task from task_list
	 * we won't schedule it now anymore 
	task_done (c);

	/* scheduler switch _curr_task to next valid running task */
	schedule ();
}

unsigned arch_task_init (task_t *task, unsigned entry)
{
	/* we're not interested in exception, so we must check pointer and entry point address first */
	if (!task || !entry)
		return 0;

	/* we have to point to top of stack */
	unsigned *stack_top = (unsigned *) task->stacks + USER_STACK_SIZE;

	/* stack must be 8byte aligned */
	stack_top = (unsigned *) ARM_STACKALIGN ((unsigned) stack_top, 8);

	struct arm_ctx_frame *frame = (struct arm_ctx_frame *) (stack_top);

	/* decrease address - sizeof (arm_ctx_frame) : we want one empty frame at the top of stack */ 
	frame --;

	/* clear frame structure */
	memset (frame, 0, sizeof (struct arm_ctx_frame));

	/* set the entry point of new task */
	frame->lr = (unsigned *) &task_init;
	
	/* set the stack pointer for our task */
	task->sp = (unsigned *) frame;
	/* save entry point of function involved in our task */
	task->entry = (unsigned *) entry;

	return 1;
}

unsigned arch_task_set (task_t *task)
{
	if (!task)
		return 1;

	if (task == task_old)
		return 1;

	/* this is important for save old task stack pointer */
	task_old = task;

	return 0;
}

unsigned arch_task_switch (task_t *task)
{
	if (!task_old || !task->sp)
		return 0;

	if (!task_old->sp || !task->sp)
		return 0;

	/* call function for jump to new cs */
	arm_task_switch (&task_old->sp, task->sp);

	return 1;
}

void arch_task_preempt ()
{
	puts ("We have preempt !\n");
} 
