/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"
#include "proto.h"
#include "user.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static user_t user_list;
static user_t *talkto;


int user_list_add (char *id)
{
	/* alloc and init context */
	user_t *user = (user_t *) malloc (sizeof (user_t));

	if (!user)
		return 0;

	user->id = strdup (id);

	/* add into list */
	user->next = &user_list;
	user->prev = user_list.prev;
	user->prev->next = user;
	user->next->prev = user;

	return 1;
}

static int strnchrn (char *str, unsigned len)
{
	unsigned l = 0;

	while (l < len) {
		if (str[l] == '\n') {
			str[l] = '\0';

			return l;
		}

		l ++;
	}

	return 0;
}

int user_list_get (char *data, unsigned len)
{
	unsigned l = 0;

	while (l < len) {
		unsigned i = strnchrn (data+l, len-l);

		if (!i)
			break;
	      
		if (!strcmp (data+l, "THEY:"))
			break;

		printf ("user_list_add: %s\n", data+l);
		if (!user_list_add (data+l))
			return 0;

		l += (i + 1);
	}

	return 1;
}

user_t *user_list_find (char *target, unsigned len)
{
	user_t *c;

	for (c = user_list.next; c != &user_list; c = c->next) {
		if (!strncmp (c->id, target, len))
			return c;
	}

	return 0;
}

int user_list_del (char *id)
{
	user_t *user = user_list_find (id, strlen (id));

	if (!user)
		return 0;

	user->next->prev = user->prev;
	user->prev->next = user->next;

	free (user->id);
	user->len = 0;

	free (user);

	return 1;
}

int user_list_show ()
{
	printf ("Contact list:\n");

	user_t *c;

	for (c = user_list.next; c != &user_list; c = c->next) {
		if (c == user_list.next)
			printf ("%s", c->id);
		else
			printf (", %s", c->id);
	}

	printf ("\n");

	return 1;
}

int user_cmd_talkto (char *target, unsigned len)
{
	user_t *c = user_list_find (target, len);

	if (!c)
		return 0;

	talkto = c;

	return 1;
}

static int strnchrr (char *str, unsigned len)
{
	unsigned l = 0;

	while (l < len) {
		if (str[l] == ' ') {
			str[l] = '\0';

			return l;
		}

		l ++;
	}

	return 0;
}

/* Some user announce us about new status */
int user_statusincoming (char *str, unsigned len)
{
	printf ("status: %s\n", str);
	int l = strnchrr (str, len);

	if (!l)
		return 0;

	int nick_len = strlen (str);

	if (!nick_len)
		return 0; 
		
	user_t *user = user_list_find (str, nick_len);

	if (!user) {
		printf ("WARNING -> Incoming status (%s) ID (%s) not agree with contact list items\n", str+nick_len+1, str);

		return 0;
	}

	if (!strncmp (user->id, cfg->nick, strlen (cfg->nick)))
		printf ("* Your status is changed to: %s\n", str+nick_len+1);
	else
		printf ("* User %s change status to: %s\n", user->id, str+nick_len+1);

	return 1;
}

/* Some user send to us new message, let's print it to screen */
int user_msgincoming (char *str, unsigned len)
{
	int l = strnchrr (str, len);

	if (!l)
		return 0;

	int nick_len = strlen (str);

	if (!nick_len)
		return 0; 
	else {
	      nick_len --;
	      str[nick_len] = '\0';
	}

	user_t *user = user_list_find (str, nick_len);

	if (!user) {
		printf ("WARNING -> Unauthorized message from ID (%s): %s\n", str, str+nick_len+2);

		return 0;
	}

	if (!talkto) {
		talkto = user;

		printf ("Autoswitched to user %s\n> %s: %s\n", user->id, user->id, str+nick_len+2);
	} else {
		if (talkto == user)
			printf ("> %s\n", str+nick_len+2);
		else
			printf ("> %s: %s\n", user->id, str+nick_len+2);
	}

	return 1;
}

int user_islogged ()
{
	if (!cfg->autolog) {
		printf ("ERROR -> Please login first, use /h for help\n");

		return 0;
	}

	return 1;
}

int user_msgparse (char *str, unsigned len)
{
	/* direct message */
	if (str[0] != '/') {
		if (talkto)
			return proto_msg (talkto->id, str, len);
		else {
			printf ("WARNING -> No user is selected for conversation\nPlease type: /t <user@server>\n");

			return 0;
		}

		return 1;
	}

	/* user commands */

	
	if (str[1] == 'm') {	/* message */
		if (!user_islogged ())
			return 0;

		int l = strnchrr (str+3, len-3);

		if (!l) {
			printf ("WARNING -> wrong message syntax\nSYNTAX: /m <nick@server> <message>\n");

			return 0;
		}

		int nick_len = strlen (str+3);

		if (!nick_len)
			return 0; 
		
		user_t *user = user_list_find (str+3, nick_len);

		if (!user) {
			printf ("WARNING -> this ID not agree with contact list items\n");

			return 0;
		}

		return proto_msg (user->id, str+4+nick_len, len-4-nick_len);
	} else if (str[1] == 'c') {	/* contact list */
		if (!user_islogged ())
			return 0;

		return user_list_show ();
	} else if (str[1] == 't') {	/* select user to talk */
		if (!user_islogged ())
			return 0;

		if (user_cmd_talkto (str+3, len-3))
			printf ("Selected user: %s\n", str+3);
		else
			printf ("WARNING -> this ID (%s) not agree with contact list items\n", str+3);

		return 1;
	} else if (str[1] == 's') {	/* change your status */
		if (!user_islogged ())
			return 0;

		if (proto_status (str+3, len-3))
			printf ("* Status is: %s\n", str+3);

		return 1;
	} else if (str[1] == 'a') {	/* add new user to contact list */
		if (!user_islogged ())
			return 0;

		if (!proto_add (str+3))
			return 0;

		if (!user_list_add (str+3))
			return 0;

		printf ("+ ID %s added to contact list\n", str+3);

		return 1;
	} else if (str[1] == 'd') {	/* delete user from contact list */
		if (!user_islogged ())
			return 0;

		if (!user_list_del (str+3))
			return 0;

		if (!proto_del (str+3))
			return 0;

		printf ("- ID %s deleted from contact list\n", str+3);

		return 1;
	} else if (str[1] == 'r') {	/* register new account on server */
		int l = strnchrr (str+3, len-3);

		if (!l) {
			printf ("WARNING -> wrong register syntax\nSYNTAX: /r <nick> <password>\n");

			return 0;
		}

		int nick_len = strlen (str+3);

		if (!nick_len)
			return 0; 

		if (!proto_register (str+3, str+4+nick_len))
			return 0;

		printf ("! Registration request for ID (%s) was send\n", str+3);
		
		return 1;
	} else if (str[1] == 'l') {	/* login to your account */
		/*if (cfg->autolog) {
			printf ("ERROR -> You are already logged in\n");

			return 0;
		}*/

		int l = strnchrr (str+3, len-3);

		if (!l) {
			printf ("WARNING -> wrong register syntax\nSYNTAX: /l <nick> <password>\n");

			return 0;
		}

		int nick_len = strlen (str+3);

		if (!nick_len)
			return 0; 

		if (!proto_login (str+3, str+4+nick_len))
			return 0;

		cfg->autolog = 1;

		return proto_list ();;
	} else if (str[1] == 'h') {	/* show help */
		printf ("Command list:\n/m <nick@server> <message>\twrite message to your buddy\n"
			"/c\t\t\t\tdisplay contact list\n"
			"/t <nick@server>\t\tselect user for conversation window\n"
			"/s <status>\t\t\tchange your status\n"
			"/a <nick@server>\t\tadd new user to contact list\n"
			"/d <nick@server>\t\tdelete user from contact list\n"
			"/r <nick> <password>\t\tregister new account on server\n"
			"/l <nick> <password>\t\tlogin to your account\n"
			"/h\t\t\t\tshow this help\n");

		return 1;
	}

	printf ("WARNING -> command '%s' is invalid, try /h for help\n", str);

	return 1;
}

int user_error (char *str, unsigned len)
{
	printf ("ERROR -> %s\n", str);

	return 1;
}

int user_info (char *str, unsigned len)
{
	if (!strncmp (str, "msg send", len))
		return 0;

	printf ("INFO -> %s\n", str);

	return 1;
}

int init_user ()
{
	user_list.next = &user_list;
	user_list.prev = &user_list;

	talkto = 0;

	return 1;
}
