/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <libx/base.h>
#include <libx/cursor.h>

/* kb mouse */
#define	ARROWLEFT			75
#define	ARROWRIGHT			77
#define	ARROWUP				72
#define	ARROWDOWN			80
#define	ENTER				28
#define	DEL				14

static unsigned mouse_accel = 0;

/* ps2 mouse */
#define	MOUSE_FLAG_BUTTON1	0x1	/* Left mouse button */
#define	MOUSE_FLAG_BUTTON2	0x2	/* Right mouse button */
#define	MOUSE_FLAG_BUTTON3	0x4	/* Middle mouse button */
#define	MOUSE_FLAG_BUTTON4	0x8	/* Extra mouse button */
#define	MOUSE_FLAG_BUTTON5	0x10	/* Extra mouse button */
#define	MOUSE_FLAG_SCROLLUP	0x20	/* Scroll button/wheel - step up */
#define	MOUSE_FLAG_SCROLLDOWN	0x40	/* Scroll button/wheel - step down  */

static int ps2_fd;
static int com_fd;

static int cursor_x;
static int cursor_y;
static int cursor_state;

static char mouse_type = -1;


static unsigned xcursor_commouse_init ()
{
	com_fd = open ("/dev/mousecom", O_RDONLY);

	if (com_fd < 1)
		return 0;

	return 1;
}

static unsigned xcursor_ps2mouse_init ()
{
	ps2_fd = open ("/dev/mouseps2", O_RDONLY);

	if (ps2_fd < 1)
		return 0;

	return 1;
}

unsigned xcursor_init ()
{
	/* initial cursor position */
	cursor_x = 0;
	cursor_y = 0;

	/* automatic mouse type selection */
	if (mouse_type != -1)
		goto manual;

	if (xcursor_ps2mouse_init ())
		mouse_type = 0x2;
	else if (xcursor_commouse_init ())
		mouse_type = 0x1;
	else
		mouse_type = 0x0;
	
	return 1;
manual:
	switch (mouse_type) {
		case 0x0:
			mouse_accel = 1;
			return 1;
		case 0x1:
			/* serial mouse over rs232 */
			return xcursor_commouse_init ();
		case 0x2:
			/* ps/2 mouse */
			return xcursor_ps2mouse_init ();
	}

	return 0;
}

static void xcursor_ps2mouse_handler ()
{
	/* low-level mouse structure */
	typedef struct {
		unsigned short flags;
		short pos_x;			/* Difference - horizontal position */
		short pos_y;			/* Difference - vertical position */
	} dev_mouse_t;

	dev_mouse_t mouse;

	int r = read (ps2_fd, &mouse, sizeof (dev_mouse_t));

	if (!r)
		return;

	lseek (ps2_fd, 0, SEEK_SET);

	cursor_x += mouse.pos_x;
	cursor_y += mouse.pos_y;

	if (mouse.flags & MOUSE_FLAG_BUTTON2)
		cursor_state = XCURSOR_STATE_RBUTTON;
	else if (mouse.flags & MOUSE_FLAG_BUTTON1)
		cursor_state = XCURSOR_STATE_LBUTTON;
	else
		cursor_state = 0x0;
}

static void xcursor_commouse_handler ()
{
	/* low-level mouse structure */
	typedef struct {
		unsigned short flags;
		short pos_x;			/* Difference - horizontal position */
		short pos_y;			/* Difference - vertical position */
	} dev_mouse_t;

	dev_mouse_t mouse;

	int r = read (com_fd, &mouse, sizeof (dev_mouse_t));

	if (!r)
		return;

	lseek (com_fd, 0, SEEK_SET);

	cursor_x += (int) mouse.pos_x;
	cursor_y += (int) mouse.pos_y;

	if (mouse.flags & MOUSE_FLAG_BUTTON2)
		cursor_state = XCURSOR_STATE_RBUTTON;
	else if (mouse.flags & MOUSE_FLAG_BUTTON1)
		cursor_state = XCURSOR_STATE_LBUTTON;
	else
		cursor_state = 0x0;
}

static void xcursor_kbmouse_handler ()
{
	int scancode = getkey ();

	switch (scancode) {
		case ARROWLEFT:
			mouse_accel ++;
			cursor_x -= mouse_accel;
			return;
		case ARROWRIGHT:
			mouse_accel ++;
			cursor_x += mouse_accel;
			return;
		case ARROWUP:
			mouse_accel ++;
			cursor_y -= mouse_accel;
			return;
		case ARROWDOWN:
			mouse_accel ++;
			cursor_y += mouse_accel;
			return;
		case DEL:
			cursor_state = XCURSOR_STATE_RBUTTON;
			return;
		case ENTER:
			cursor_state = XCURSOR_STATE_LBUTTON;
			return;
	}

	mouse_accel = 4;

	cursor_state = 0x0;
}

int xcursor_update ()
{
	switch (mouse_type) {
		case 0x0:
			/* TODO: autodetect */
			xcursor_kbmouse_handler ();
			break;
		case 0x1:
			/* serial mouse over rs232 */
			xcursor_commouse_handler ();
			break;
		case 0x2:
			/* ps/2 mouse */
			xcursor_ps2mouse_handler ();
			break;
	}

	if (cursor_y < 0)
		cursor_y = 0;

	if (cursor_y > (signed) (vgafb_res_y-1))
		cursor_y = (vgafb_res_y-1);

	if (cursor_x < 0)
		cursor_x = 0;

	if (cursor_x > (signed) (vgafb_res_x-1))
		cursor_x = (vgafb_res_x-1);

	return cursor_state;
}

void xcursor_getpos (int *x, int *y)
{
	*x = cursor_x;
	*y = cursor_y;
}

unsigned xcursor_settype (unsigned char type)
{
	if (mouse_type == type)
		return 0;

	mouse_type = type;

	return 1;
}
