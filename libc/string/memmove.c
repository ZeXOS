/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h> /* size_t */

typedef long word;

#define lmask (sizeof (long) - 1)

void *memmove (void *dest, void const *src, size_t count)
{
	char *d = (char *) dest;
	const char *s = (const char *) src;
	int len;

	if (!count || dest == src)
		return dest;

	if ((long) d < (long) s) {
		if (((long) d | (long) s) & lmask) {
			if ((((long) d ^ (long) s) & lmask) || (count < sizeof (long)))
				len = count;
			else
				len = sizeof (long) - ((long) d & lmask);

			count -= len;
			for(; len > 0; len --)
				*d ++ = *s ++;
		}
		
		for (len = count / sizeof (long); len > 0; len --) {
			*(word *) d = *(word *) s;
			d += sizeof (long);
			s += sizeof (long);
		}
		
		for (len = count & lmask; len > 0; len --)
			*d ++ = *s ++;
	} else {
		d += count;
		s += count;

		if (((long) d | (long) s) & lmask) {
			if ((((long) d ^ (long) s) & lmask) || (count <= sizeof (long)))
				len = count;
			else
				len = ((long) d & lmask);

			count -= len;

			for (; len > 0; len --)
				*-- d = *-- s;
		}

		for (len = count / sizeof (long); len > 0; len --) {
			d -= sizeof (long);
			s -= sizeof (long);
			*(word *) d = *(word *) s;
		}

		for (len = count & lmask; len > 0; len --)
			*-- d = *-- s;
	}

	return dest;
}
