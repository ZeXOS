/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <_printf.h> /* fnptr_t */
#include <string.h> /* strlen() */
#include <stdarg.h> /* va_list, va_arg() */
#include <stdio.h>
#include <stdlib.h>

static int scanf_getint (int *n)
{
	char *str = (char *) malloc (sizeof (char) * 128);

	if (!str)
		return 0;

	unsigned i = 0;

	for (;;) {
		schedule ();

		char c = getch ();

		if (!c)
			continue;

		if (c == '\n' || i > 127)
			break;

		putchar (c);

		if (c == ' ' && i)
			break;

		str[i] = c;

		if (c == '\b' && i)
			i --;
		else
			i ++;
	}

	if (!i)
		return 0;

	str[i] = '\0';

	i = 0;

	*n = atoi (str);

	free (str);

	return 1;
}

/* TODO: implement real floatint point format */
static int scanf_getfloat (float *n)
{
	char *str = (char *) malloc (sizeof (char) * 128);

	if (!str)
		return 0;

	unsigned i = 0;

	for (;;) {
		schedule ();

		char c = getch ();

		if (!c)
			continue;

		if (c == '\n' || i > 127)
			break;

		putchar (c);

		if (c == ' ' && i)
			break;

		str[i] = c;

		if (c == '\b' && i)
			i --;
		else
			i ++;
	}

	if (!i)
		return 0;

	str[i] = '\0';

	i = 0;

	*n = (float) atoi (str);

	free (str);

	return 1;
}

static int scanf_gethex (int *n)
{
	char *str = (char *) malloc (sizeof (char) * 128);

	if (!str)
		return 0;

	unsigned i = 0;

	for (;;) {
		schedule ();

		char c = getch ();

		if (!c)
			continue;

		if (c == '\n' || i > 127)
			break;

		putchar (c);

		if (c == ' ' && i)
			break;

		str[i] = c;

		if (c == '\b' && i)
			i --;
		else
			i ++;
	}

	if (!i)
		return 0;

	str[i] = '\0';

	i = 0;

	char *endptr;

	*n = strtol (str, &endptr, 16);

	free (str);

	return 1;
}

static int scanf_getstring (unsigned char *n)
{
	unsigned i = 0;

	for (;;) {
		schedule ();

		char c = getch ();

		if (!c)
			continue;

		if (c == '\n')
			break;

		putchar (c);

		if (c == ' ' && i)
			break;

		n[i] = c;

		if (c == '\b' && i)
			i --;
		else
			i ++;
	}

	if (!i)
		return 0;

	n[i] = '\0';

	return 1;
}

static int scanf_getchar (unsigned char *n)
{
	unsigned i = 0;

	for (;;) {
		schedule ();

		char c = getch ();

		if (!c)
			continue;

		if (c == '\n')
			break;

		putchar (c);

		if (c == ' ' && i)
			break;

		if (i == 0)
			n[i] = c;

		if (c == '\b' && i)
			i --;
		else
			i ++;
	}

	if (!i)
		return 0;

	return 1;
}

int vscanf (const char *fmt, va_list args)
{
	unsigned count = 0;
	int *num = 0;
	float *fp = 0;
	unsigned char *buf = 0;

	for (; *fmt; fmt ++) {
		/* probably argument is here */
		if (*fmt == '%') {
			fmt ++;

			if (!*fmt)
				break;

			if (*fmt == 'd' || *fmt == 'i' || *fmt == 'u') {
				num = va_arg (args, int *);
					
				if (scanf_getint (num))
					count ++;

				continue;
			}

			if (*fmt == 'x' || *fmt == 'X') {
				num = va_arg (args, int *);

				if (scanf_gethex (num))
					count ++;

				continue;
			}
				
			if (*fmt == 'c') {
				buf = va_arg (args, unsigned char *);

				if (scanf_getchar (buf))
					count ++;

				continue;
			}

			if (*fmt == 'f') {
				fp = va_arg (args, float *);
					
				if (scanf_getfloat (fp))
					count ++;

				continue;
			}

			if (*fmt == 's') {
				buf = va_arg (args, unsigned char *);

				if (scanf_getstring (buf))
					count ++;

				continue;
			}

			continue;
		}

		count ++;

	}
	
	putchar ('\n');

	return count;
}

