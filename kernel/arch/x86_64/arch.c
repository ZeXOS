/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

unsigned int_enable ()
{
	__asm__ __volatile__("sti"
		:
		:
		);

	return 1;
}

unsigned int_disable ()
{
	__asm__ __volatile__("cli");

	return 1;
}

static void draw_arch ()
{
	unsigned char *textmemptr = (unsigned char *) 0xB8000;

	textmemptr += 8*160;

	textmemptr[0] = 'x';
	textmemptr[1] = 0x0f;
	textmemptr[2] = '8';
	textmemptr[3] = 0x0f;
	textmemptr[4] = '6';
	textmemptr[5] = 0x0f;
	textmemptr[6] = '_';
	textmemptr[7] = 0x0f;
	textmemptr[8] = '6';
	textmemptr[9] = 0x0f;
	textmemptr[10] = '4';
	textmemptr[11] = 0x0f;
}

unsigned init_arch ()
{


	if (!gdt_install ())
		return 0;
	draw_arch ();
	if (!idt_install ())
		return 0;

	if (!isrs_install ())
		return 0;

	if (!irq_install ())
		return 0;

	return 1;
}

void halt ()
{
	__asm__ __volatile__("hlt");
}
