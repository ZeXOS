/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifndef ARCH_i386
#undef CONFIG_DRV_RTL8139
#endif

#ifdef CONFIG_DRV_RTL8139

#include <system.h>
#include <string.h>
#include <arch/io.h>
#include <pci.h>
#include <task.h>
#include <net/eth.h>
#include <net/net.h>

/* PCI probe IDs */
#define RTL8139_VENDORID		0x10ec
#define RTL8139_DEVICEID		0x8139

/* RTL8139C register definitions */
#define RTL8139_IDR0			0x00		/* Mac address */
#define RTL8139_MAR0			0x08		/* Multicast filter */
#define RTL8139_TXSTATUS0		0x10		/* Transmit status (4 32bit regs) */
#define RTL8139_TXSTATUS1		0x14		/* Transmit status (4 32bit regs) */
#define RTL8139_TXSTATUS2		0x18		/* Transmit status (4 32bit regs) */
#define RTL8139_TXSTATUS3		0x1c		/* Transmit status (4 32bit regs) */
#define RTL8139_TXADDR0			0x20		/* Tx descriptors (also 4 32bit) */
#define RTL8139_TXADDR1			0x24		/* Tx descriptors (also 4 32bit) */
#define RTL8139_TXADDR2			0x28		/* Tx descriptors (also 4 32bit) */
#define RTL8139_TXADDR3			0x2c		/* Tx descriptors (also 4 32bit) */
#define RTL8139_RXBUF			0x30		/* Receive buffer start address */
#define RTL8139_RXEARLYCNT		0x34		/* Early Rx byte count */
#define RTL8139_RXEARLYSTATUS		0x36		/* Early Rx status */
#define RTL8139_CHIPCMD			0x37		/* Command register */
#define RTL8139_RXBUFTAIL		0x38		/* Current address of packet read (queue tail) */
#define RTL8139_RXBUFHEAD		0x3A		/* Current buffer address (queue head) */
#define RTL8139_INTRMASK		0x3C		/* Interrupt mask */
#define RTL8139_INTRSTATUS		0x3E		/* Interrupt status */
#define RTL8139_TXCONFIG		0x40		/* Tx config */
#define RTL8139_RXCONFIG		0x44		/* Rx config */
#define RTL8139_TIMER			0x48		/* A general purpose counter */
#define RTL8139_RXMISSED		0x4C		/* 24 bits valid, write clears */
#define RTL8139_CFG9346			0x50		/* 93C46 command register */
#define RTL8139_CONFIG0			0x51		/* Configuration reg 0 */
#define RTL8139_CONFIG1			0x52		/* Configuration reg 1 */
#define RTL8139_TIMERINT		0x54		/* Timer interrupt register (32 bits) */
#define RTL8139_MEDIASTATUS		0x58		/* Media status register */
#define RTL8139_CONFIG3			0x59		/* Config register 3 */
#define RTL8139_CONFIG4			0x5A		/* Config register 4 */
#define RTL8139_HLTCLK			0x5B		/* Halted Clock */
#define RTL8139_MULTIINTR		0x5C		/* Multiple interrupt select */
#define RTL8139_MII_TSAD		0x60		/* Transmit status of all descriptors (16 bits) */
#define RTL8139_MII_BMCR		0x62		/* Basic Mode Control Register (16 bits) */
#define RTL8139_MII_BMSR		0x64		/* Basic Mode Status Register (16 bits) */
#define RTL8139_AS_ADVERT		0x66		/* Auto-negotiation advertisement reg (16 bits) */
#define RTL8139_AS_LPAR			0x68		/* Auto-negotiation link partner reg (16 bits) */
#define RTL8139_AS_EXPANSION		0x6A		/* Auto-negotiation expansion reg (16 bits) */

/* RTL8193C command bits; or these together and write teh resulting value
   into CHIPCMD to execute it. */
#define RTL8139_CMD_RESET		0x10
#define RTL8139_CMD_RX_ENABLE		0x08
#define RTL8139_CMD_TX_ENABLE		0x04
#define RTL8139_CMD_RX_BUF_EMPTY 	0x01

/* RTL8139C interrupt status bits */
#define RTL8139_INT_PCIERR		0x8000		/* PCI Bus error */
#define RTL8139_INT_TIMEOUT		0x4000		/* Set when TCTR reaches TimerInt value */
#define RTL8139_INT_RXFIFO_OVERFLOW	0x0040		/* Rx FIFO overflow */
#define RTL8139_INT_RXFIFO_UNDERRUN	0x0020		/* Packet underrun / link change */
#define RTL8139_INT_RXBUF_OVERFLOW	0x0010		/* Rx BUFFER overflow */
#define RTL8139_INT_TX_ERR		0x0008
#define RTL8139_INT_TX_OK		0x0004
#define RTL8139_INT_RX_ERR		0x0002
#define RTL8139_INT_RX_OK		0x0001

/* RTL8139C transmit status bits */
#define RTL8139_TX_CARRIER_LOST		0x80000000	/* Carrier sense lost */
#define RTL8139_TX_ABORTED		0x40000000	/* Transmission aborted */
#define RTL8139_TX_OUT_OF_WINDOW	0x20000000	/* Out of window collision */
#define RTL8139_TX_CARRIER_HBEAT 	0x10000000  	/* not sure */ 
#define RTL8139_TX_STATUS_OK		0x00008000	/* Status ok: a good packet was transmitted */
#define RTL8139_TX_UNDERRUN		0x00004000	/* Transmit FIFO underrun */
#define RTL8139_TX_HOST_OWNS		0x00002000	/* Set to 1 when DMA operation is completed */
#define RTL8139_TX_SIZE_MASK		0x00001fff	/* Descriptor size mask */

#define RTL8139_TX_IFG0			0x1000000
#define RTL8139_TX_IFG1			0x2000000
#define RTL8139_TX_MXDMA2		0x400
#define RTL8139_TX_MXDMA1		0x200

/* RTL8139C receive status bits */
#define RTL8139_RX_MULTICAST		0x00008000	/* Multicast packet */
#define RTL8139_RX_PAM			0x00004000	/* Physical address matched */
#define RTL8139_RX_BROADCAST		0x00002000	/* Broadcast address matched */
#define RTL8139_RX_BAD_SYMBOL		0x00000020	/* Invalid symbol in 100TX packet */
#define RTL8139_RX_RUNT			0x00000010	/* Packet size is <64 bytes */
#define RTL8139_RX_TOO_LONG		0x00000008	/* Packet size is >4K bytes */
#define RTL8139_RX_CRC_ERR		0x00000004	/* CRC error */
#define RTL8139_RX_FRAME_ALIGN		0x00000002	/* Frame alignment error */
#define RTL8139_RX_STATUS_OK		0x00000001	/* Status ok: a good packet was received */

#define RTL8139_RX_RBLEN0		0x800
#define RTL8139_RX_MXDMA2		0x400
#define RTL8139_RX_MXDMA1		0x200
#define RTL8139_RX_AB			0x8
#define RTL8139_RX_AM			0x4
#define RTL8139_RX_APM			0x2

#define RTL8139_BMCR_FDUPLEX		0x0100
#define RTL8139_MSR_SPEED10		0x08

#define RTL8139_PM_ENABLE		0x01
#define RTL8139_PM_LWAKE		0x10
#define RTL8139_PM_LWPTN		(1 << 2)
#define RTL8139_PM_SLEEP		(1 << 1)
#define RTL8139_PM_PWRDN		(1 << 0)

/*  EEPROM_Ctrl bits. */
#define RTL8139_EE_SHIFT_CLK    0x04    /* EEPROM shift clock. */
#define RTL8139_EE_CS           0x08    /* EEPROM chip select. */
#define RTL8139_EE_DATA_WRITE   0x02    /* EEPROM chip data in. */
#define RTL8139_EE_WRITE_0      0x00
#define RTL8139_EE_WRITE_1      0x02
#define RTL8139_EE_DATA_READ    0x01    /* EEPROM chip data out. */
#define RTL8139_EE_ENB          (0x80 |  RTL8139_EE_CS)

/* The EEPROM commands include the alway-set leading bit. */
#define RTL8139_EE_WRITE_CMD    (5 << 6)
#define RTL8139_EE_READ_CMD     (6 << 6)
#define RTL8139_EE_ERASE_CMD    (7 << 6)

#define TAILREG_TO_TAIL(in) \
	(unsigned short)(((unsigned)(in) + 16) % 0x10000)

#define TAIL_TO_TAILREG(in) \
	(unsigned short)((unsigned)(in) - 16)

#define MYRT_INTS 			(RTL8139_INT_PCIERR | RTL8139_INT_RX_ERR | RTL8139_INT_RX_OK | \
					RTL8139_INT_TX_ERR | RTL8139_INT_TX_OK | RTL8139_INT_RXBUF_OVERFLOW)

#define HW_REVID(b30, b29, b28, b27, b26, b23, b22) \
	(b30<<30 | b29<<29 | b28<<28 | b27<<27 | b26<<26 | b23<<23 | b22<<22)
#define HW_REVID_MASK	HW_REVID(1, 1, 1, 1, 1, 1, 1)

#define RTL8139_CONFIG3_MAGIC		(1 << 5)

#define RTL8139_MULTIINTR_CLEAR		0xf000

enum chip_flags {
	HasHltClk	= (1 << 0),
	HasLWake	= (1 << 1),
};

typedef enum {
	CH_8139	= 0,
	CH_8139_K,
	CH_8139A,
	CH_8139A_G,
	CH_8139B,
	CH_8130,
	CH_8139C,
	CH_8100,
	CH_8100B_8139D,
	CH_8101,
} chip_t;

/* directly indexed by chip_t, above */
static const struct {
	const char *name;
	unsigned version; /* from RTL8139C/RTL8139D docs */
	unsigned flags;
} rtl_chip_info[] = {
	{ "RTL-8139",
	  HW_REVID(1, 0, 0, 0, 0, 0, 0),
	  HasHltClk,
	},

	{ "RTL-8139 rev K",
	  HW_REVID(1, 1, 0, 0, 0, 0, 0),
	  HasHltClk,
	},

	{ "RTL-8139A",
	  HW_REVID(1, 1, 1, 0, 0, 0, 0),
	  HasHltClk, /* XXX undocumented? */
	},

	{ "RTL-8139A rev G",
	  HW_REVID(1, 1, 1, 0, 0, 1, 0),
	  HasHltClk, /* XXX undocumented? */
	},

	{ "RTL-8139B",
	  HW_REVID(1, 1, 1, 1, 0, 0, 0),
	  HasLWake,
	},

	{ "RTL-8130",
	  HW_REVID(1, 1, 1, 1, 1, 0, 0),
	  HasLWake,
	},

	{ "RTL-8139C",
	  HW_REVID(1, 1, 1, 0, 1, 0, 0),
	  HasLWake,
	},

	{ "RTL-8100",
	  HW_REVID(1, 1, 1, 1, 0, 1, 0),
 	  HasLWake,
 	},

	{ "RTL-8100B/8139D",
	  HW_REVID(1, 1, 1, 0, 1, 0, 1),
	  HasHltClk /* XXX undocumented? */
	| HasLWake,
	},

	{ "RTL-8101",
	  HW_REVID(1, 1, 1, 0, 1, 1, 1),
	  HasLWake,
	},
};

/* rtl8139 device structure */
struct rtl8139_dev_t {
	unsigned char irq;
	pcidev_t *pcidev;
	unsigned short addr_io;
	mac_addr_t addr_mac;
	char *txbuf;
	char *rxbuf;
	unsigned char txbn;
	unsigned char txbn_last;
	unsigned chipset;
};

struct rtl8139_dev_t *rtl8139_dev;
static netdev_t *ifdev;

/* prototypes */
unsigned rtl8139_start (struct rtl8139_dev_t *dev);
unsigned rtl8139_int_rx (struct rtl8139_dev_t *dev);
unsigned rtl8139_int_tx (struct rtl8139_dev_t *dev);
bool rtl8139_acthandler (unsigned act, char *block, unsigned block_len);

/* I/O operations */
unsigned char rtl8139_read8 (struct rtl8139_dev_t *dev, unsigned short port)
{
	return inb (dev->addr_io + port);
}

void rtl8139_write8 (struct rtl8139_dev_t *dev, unsigned short port, unsigned char val)
{
	outb (dev->addr_io + port, val);
}

unsigned short rtl8139_read16 (struct rtl8139_dev_t *dev, unsigned short port)
{
	return inw (dev->addr_io + port);
}

void rtl8139_write16 (struct rtl8139_dev_t *dev, unsigned short port, unsigned short val)
{
	outw (dev->addr_io + port, val);
}

unsigned rtl8139_read32 (struct rtl8139_dev_t *dev, unsigned short port)
{
	return inl (dev->addr_io + port);
}

void rtl8139_write32 (struct rtl8139_dev_t *dev, unsigned short port, unsigned val)
{
	outl (dev->addr_io + port, val);
}

/* READ/WRITE func */
unsigned rtl8139_read (char *buf, unsigned len)
{
	return rtl8139_acthandler (DEV_ACT_READ, buf, len);
}

unsigned rtl8139_write (char *buf, unsigned len)
{
	return rtl8139_acthandler (DEV_ACT_WRITE, buf, len);
}


/* Serial EEPROM section. */
static int rtl8139_eeprom_read (struct rtl8139_dev_t *dev, int location)
{
	int i;
	unsigned int retval = 0;
	int read_cmd = location | RTL8139_EE_READ_CMD;

	rtl8139_write8 (dev, RTL8139_CFG9346, RTL8139_EE_ENB & ~RTL8139_EE_CS);
	rtl8139_write8 (dev, RTL8139_CFG9346, RTL8139_EE_ENB);

	/* Shift the read command bits out. */
	for (i = 10; i >= 0; i--) {
		int dataval = (read_cmd & (1 << i)) ? RTL8139_EE_DATA_WRITE : 0;

		rtl8139_write8 (dev, RTL8139_CFG9346, RTL8139_EE_ENB | dataval);
		rtl8139_read8 (dev, RTL8139_CFG9346);

		rtl8139_write8 (dev, RTL8139_CFG9346, RTL8139_EE_ENB | dataval | RTL8139_EE_SHIFT_CLK);
		rtl8139_read8 (dev, RTL8139_CFG9346);
	}

	rtl8139_write8 (dev, RTL8139_CFG9346, RTL8139_EE_ENB);

	rtl8139_read8 (dev, RTL8139_CFG9346);

	for (i = 16; i > 0; i --) {
		rtl8139_write8 (dev, RTL8139_CFG9346, RTL8139_EE_ENB | RTL8139_EE_SHIFT_CLK);
		rtl8139_read8 (dev, RTL8139_CFG9346);

		retval = (retval << 1) | ((rtl8139_read8 (dev, RTL8139_CFG9346) & RTL8139_EE_DATA_READ) ? 1 : 0);

		rtl8139_write8 (dev, RTL8139_CFG9346, RTL8139_EE_ENB);
		rtl8139_read8 (dev, RTL8139_CFG9346);
	}

	/* Terminate the EEPROM access. */
	rtl8139_write8 (dev, RTL8139_CFG9346, ~RTL8139_EE_CS);

	return retval;
}

/* detect rtl8139 device in PC */
pcidev_t *rtl8139_detect ()
{
	/* First detect network card - is connected to PCI bus ?*/
	pcidev_t *pcidev = pcidev_find (RTL8139_VENDORID, RTL8139_DEVICEID);

	if (!pcidev)
		return 0;

	return pcidev;
}

/* reset rtl8139 device */
unsigned rtl8139_reset (struct rtl8139_dev_t *dev)
{
	int i;

	rtl8139_write8 (dev, RTL8139_CHIPCMD, RTL8139_CMD_RESET);

	/* wait until chip is ready */
	for (i = 0; i < 1000; i ++) {
		if (!(rtl8139_read8 (dev, RTL8139_CHIPCMD) & RTL8139_CMD_RESET))
			return 1;

		/* 10ms timeout */
		timer_wait (10);
	}

	kprintf ("ERROR -> waiting for rtl8139 reset\n");

	return 0;
}

/* nic interrupt handler */
void rtl8139_int ()
{
	//int_disable ();

	struct rtl8139_dev_t *dev = rtl8139_dev;

	unsigned short status = rtl8139_read16 (dev, RTL8139_INTRSTATUS);

	// disable interrupts
	rtl8139_write16 (dev, RTL8139_INTRMASK, 0);

	rtl8139_write16 (dev, RTL8139_INTRSTATUS, status);

	if (status & RTL8139_INT_RX_OK)
		rtl8139_int_rx (dev);

	if (status & RTL8139_INT_RX_ERR)
		DPRINT (DBG_DRIVER | DBG_ETH, "rtl8139 -> read error :(\n");

	if(status & RTL8139_INT_RXBUF_OVERFLOW) {
		rtl8139_write32 (dev, RTL8139_RXMISSED, 0);
		rtl8139_write16 (dev, RTL8139_RXBUFTAIL, TAIL_TO_TAILREG (rtl8139_read16 (dev, RTL8139_RXBUFHEAD)));
	}

	if (status & RTL8139_INT_TX_OK)
		rtl8139_int_tx (dev);

	if (status & RTL8139_INT_TX_ERR)
		DPRINT (DBG_DRIVER | DBG_ETH, "rtl8139 -> write error:(\n");
	
	//kprintf ("rtl8139 -> interrupt (0x%x)\n", status);

	// reenable interrupts
	rtl8139_write16 (dev, RTL8139_INTRMASK, MYRT_INTS);

	//int_enable ();
}

/* rx descriptor structure */
typedef struct rx_entry {
	volatile unsigned short status;
	volatile unsigned short len;
	volatile unsigned char data[1];
} rx_entry;

unsigned rtl8139_int_rx (struct rtl8139_dev_t *dev)
{
	unsigned tail = TAILREG_TO_TAIL (rtl8139_read16 (dev, RTL8139_RXBUFTAIL));

	rx_entry *entry = (rx_entry *)((unsigned char *) dev->rxbuf + tail);

	if (!entry)
		return 0;

	unsigned len = 2048;

	unsigned l = entry->len - 4; // minus the crc

	if ((entry->status & RTL8139_RX_STATUS_OK) == 0) {
		// error, lets reset the card
		DPRINT (DBG_DRIVER | DBG_ETH, "rtl8139_rx () -> rx error");

		// stop the rx and tx and mask all interrupts
		rtl8139_write8 (dev, RTL8139_CHIPCMD, RTL8139_CMD_RESET);
		rtl8139_write16 (dev, RTL8139_INTRMASK, 0x0);
	
		// reset the rx pointers
		rtl8139_write16 (dev, RTL8139_RXBUFTAIL, TAIL_TO_TAILREG (0));
		rtl8139_write16 (dev, RTL8139_RXBUFHEAD, 0x0);
	
		// start it back up
		rtl8139_write16 (dev, RTL8139_INTRMASK, MYRT_INTS);
	
		// Enable RX/TX once more
		rtl8139_write8 (dev, RTL8139_CHIPCMD, RTL8139_CMD_RX_ENABLE | RTL8139_CMD_TX_ENABLE);
		return 0;
	}

	if (l > len) {
		DPRINT (DBG_DRIVER | DBG_ETH, "rtl8139_rx () -> packet too large for buffer (len %d, buf_len %ld)\n", l, len);
		rtl8139_write16 (dev, RTL8139_RXBUFTAIL, TAILREG_TO_TAIL (rtl8139_read16 (dev, RTL8139_RXBUFHEAD)));
		return 0;
	}

	if(tail + len > 0xffff) {
		DPRINT (DBG_DRIVER | DBG_ETH, "rtl8139 -> packet wraps around\n");
		//netdev_rx_add_queue (ifdev, (char *) &entry->data[0], 0x10000 - (tail + 4));
		//netdev_rx_add_queue (ifdev, (char *) dev->rxbuf, l - (0x10000 - (tail + 4)));

		//memcpy (buf, (const void *) &entry->data[0], l - (0x10000 - (tail + 4)));
		//memcpy ((unsigned char *) buf + 0x10000 - (tail + 4), (const void *) dev->rxbuf, len - (0x10000 - (tail + 4)));
	} else {
		netdev_rx_add_queue (ifdev, (char *) &entry->data[0], entry->len);
		//memcpy (buf, (const void *) &entry->data[0], l);
	}

	// calculate the new tail
	tail = ((tail + entry->len + 4 + 3) & ~3) % 0x10000;

	//kprintf ("new tail at 0x%x, tailreg will say 0x%x\n", tail, TAIL_TO_TAILREG(tail));

	rtl8139_write16 (dev, RTL8139_RXBUFTAIL, TAIL_TO_TAILREG (tail));

	if(tail != rtl8139_read16 (dev, RTL8139_RXBUFHEAD)) {
		// we're at last one more packet behind
	}

	return l;
}

unsigned rtl8139_tx (struct rtl8139_dev_t *dev, char *buf, unsigned len)
{
	/* copy our buffer to txbuf */
	memcpy ((char *) (dev->txbuf + dev->txbn * 0x800), buf, len);

	/* we need send crc too */
	//len += 4;

	/* ethernet min. packet length is 64 with crc */
	if(len < 64)
		len = 64;

	rtl8139_write32 (dev, RTL8139_TXSTATUS0 + dev->txbn * 4, len | 0x80000);

	if (++ dev->txbn >= 4)
		dev->txbn = 0;

	return 1;
}

unsigned rtl8139_int_tx (struct rtl8139_dev_t *dev)
{
	unsigned i;
	unsigned txstat;

	for (i = 0; i < 4; i ++) {
		if (i > 0 && dev->txbn_last == dev->txbn)
			break;

		txstat = rtl8139_read32 (dev, RTL8139_TXSTATUS0 + dev->txbn_last * 4);

		//kprintf ("txstat[%d] = 0x%x %u %u %u\n", dev->txbn_last, txstat, (txstat & RTL8139_TX_STATUS_OK) ? 1 : 0, (txstat & RTL8139_TX_UNDERRUN) ? 1 : 0, (txstat & RTL8139_TX_ABORTED) ? 1 : 0);

		if((txstat & (RTL8139_TX_STATUS_OK | RTL8139_TX_UNDERRUN | RTL8139_TX_ABORTED)) == 0)
			break;

		if(++ dev->txbn_last >= 4)
			dev->txbn_last = 0;
	}

	return 1;
}

/* INIT sequence */
unsigned init_rtl8139 ()
{
	unsigned i = 0;

	pcidev_t *pcidev = rtl8139_detect ();

	if (!pcidev)
		return 0;

	struct rtl8139_dev_t *dev = (struct rtl8139_dev_t *) kmalloc (sizeof (struct rtl8139_dev_t));

	if (!dev)
		return 0;

	/* get irq id */
	dev->irq = pcidev->u.h0.interrupt_line;
	dev->addr_io = pcidev->u.h0.base_registers[0] & ~3;

	dev->txbn = 0;
	dev->txbn_last = 0;

	pci_device_enable (pcidev);

	pci_device_adjust (pcidev);

	/* reset device now */
	//rtl8139_reset (dev);

	// reset config1
	//rtl8139_write8 (dev, RTL8139_CONFIG1, 0x0);

	/* go out from low-power mode */
	rtl8139_write8 (dev, RTL8139_HLTCLK, 'R');

	// check for broken hardware
	unsigned respond = rtl8139_read32 (dev, RTL8139_TXCONFIG);

	if (respond == 0xffffffff) {
		kprintf ("rtl8139 -> Chip not respond, probably it's broken\n");
		return 0;
	}

	/* get mac address from ethernet */
	if (rtl8139_eeprom_read (dev, 0) != 0xffff) {
		unsigned short m[3];
		for (i = 0; i < 3; i ++)
			m[i] = rtl8139_eeprom_read (dev, i + 7);
		
		memcpy (dev->addr_mac, &m, sizeof (mac_addr_t));
	} else {
		for (i = 0; i < 6; i ++)
			dev->addr_mac[i] = rtl8139_read8 (dev, RTL8139_IDR0 + i);
	}

	unsigned char speed10 = rtl8139_read8 (dev, RTL8139_MEDIASTATUS) & RTL8139_MSR_SPEED10;
	unsigned short fullduplex = rtl8139_read16 (dev, RTL8139_MII_BMCR) & RTL8139_BMCR_FDUPLEX;

	kprintf ("rtl8139 -> ethernet speed %sMbps %s-duplex\n", speed10 ? "10" : "100",	fullduplex ? "full" : "half");

	/* identify chip attached to board */
	unsigned version = respond & HW_REVID_MASK;
	for (i = 0; i < 10; i ++)
		if (version == rtl_chip_info[i].version) {
			dev->chipset = i;
			break;
		}
		
	kprintf ("rtl8139 -> identified chip type as '%s'\n", rtl_chip_info[i].name);
	
	rtl8139_dev = dev;

	unsigned char r;
	if (dev->chipset >= CH_8139B) {
		unsigned char r2 = r = rtl8139_read8 (dev, RTL8139_CONFIG1);

		if ((rtl_chip_info[dev->chipset].flags & HasLWake) && (r & RTL8139_PM_LWAKE))
			r2 &= ~RTL8139_PM_LWAKE;

		r2 |= RTL8139_PM_ENABLE;

		if (r != r2) {
			rtl8139_write8 (dev, RTL8139_CFG9346, 0xc0);
			rtl8139_write8 (dev, RTL8139_CONFIG1, r);
			rtl8139_write8 (dev, RTL8139_CFG9346, 0x0);
		}
			
		if (rtl_chip_info[dev->chipset].flags & HasLWake) {
			r = rtl8139_read8 (dev, RTL8139_CONFIG4);
			if (r & RTL8139_PM_LWPTN) {
				rtl8139_write8 (dev, RTL8139_CFG9346, 0xc0);
				rtl8139_write8 (dev, RTL8139_CONFIG4, r & ~RTL8139_PM_LWPTN);
				rtl8139_write8 (dev, RTL8139_CFG9346, 0x0);
			}
		}
	} else {
		r = rtl8139_read8 (dev, RTL8139_CONFIG1);
		r &= ~(RTL8139_PM_SLEEP | RTL8139_PM_PWRDN);
		rtl8139_write8 (dev, RTL8139_CONFIG1, r);
	}

	/* reset device now */
	rtl8139_reset (dev);
	
	/* Put the chip into low-power mode. */
	if (rtl_chip_info[dev->chipset].flags & HasHltClk)
		rtl8139_write8 (dev, RTL8139_HLTCLK, 'H');	/* 'R' would leave the clock running. */

	/* set interrupt vector */
	irq_install_handler (dev->irq, rtl8139_int);

	return rtl8139_start (dev);
}

unsigned rtl8139_init_ring (struct rtl8139_dev_t *dev)
{
	memset (dev->rxbuf, 0, 2048);
	memset (dev->txbuf, 0, 4*2048);
	
	return 1;
}

unsigned rtl8139_start (struct rtl8139_dev_t *dev)
{
	dev->rxbuf = (char *) 0x63000;
	dev->txbuf = (char *) 0x66000;
  
	rtl8139_init_ring (dev);

  	/* Bring old chips out of low-power mode. */
	if (rtl_chip_info[dev->chipset].flags & HasHltClk)
		rtl8139_write8 (dev, RTL8139_HLTCLK, 'R');

	/* reset device now */
	rtl8139_reset (dev);
		
	// enable writing to the config registers
	rtl8139_write8 (dev, RTL8139_CFG9346, 0xc0);

	// reset config1
	//rtl8139_write8 (dev, RTL8139_CONFIG1, 0x0);

	unsigned i;
	for (i = 0; i < 6; i ++)
		rtl8139_write8 (dev, RTL8139_IDR0 + i, dev->addr_mac[i]);

	// enable rx and tx functions
	rtl8139_write8 (dev, RTL8139_CHIPCMD, RTL8139_CMD_RX_ENABLE | RTL8139_CMD_TX_ENABLE);

	// set rx fifo threashold to 256bytes
	rtl8139_write32 (dev, RTL8139_RXCONFIG, 0x00009c00);/*RTL8139_RX_RBLEN0 | RTL8139_RX_MXDMA2 | RTL8139_RX_MXDMA1 |
						RTL8139_RX_AB | RTL8139_RX_AM | RTL8139_RX_APM);*/

	// set tx fifo threashold to 256bytes
	rtl8139_write32 (dev, RTL8139_TXCONFIG, 0x03000400);/*RTL8139_TX_IFG0 | RTL8139_TX_IFG1 | RTL8139_TX_MXDMA2 |
						RTL8139_TX_MXDMA1);*/
	
	// disable lan-wake and set the driver-loaded bit
	rtl8139_write8 (dev, RTL8139_CONFIG1, (rtl8139_read8 (dev, RTL8139_CONFIG1) & ~0x30) | 0x20);
	
	// enable fifo auto-clear
	rtl8139_write8 (dev, RTL8139_CONFIG4, rtl8139_read8 (dev, RTL8139_CONFIG4) | 0x80);

	// enable receiving broadcast and physical packets
	rtl8139_write32 (dev, RTL8139_RXCONFIG, rtl8139_read32 (dev, RTL8139_RXCONFIG) | 0x0000000f);

	// setup rx buffer
	rtl8139_write32 (dev, RTL8139_RXBUF, (unsigned) dev->rxbuf);
	
	// go back to normal mode
	rtl8139_write8 (dev, RTL8139_CFG9346, 0x0);
	
	// setup tx buffer
	rtl8139_write32 (dev, RTL8139_TXADDR0, (unsigned) dev->txbuf);
	rtl8139_write32 (dev, RTL8139_TXADDR1, (unsigned) dev->txbuf + 2*1024);
	rtl8139_write32 (dev, RTL8139_TXADDR2, (unsigned) dev->txbuf + 4*1024);
	rtl8139_write32 (dev, RTL8139_TXADDR3, (unsigned) dev->txbuf + 6*1024);

	// reset rx missed counter
	rtl8139_write32 (dev, RTL8139_RXMISSED, 0x0);

	// filter all multicast packets
	rtl8139_write32 (dev, RTL8139_MAR0, 0x0);
	rtl8139_write32 (dev, RTL8139_MAR0 + 4, 0x0);

	// no early-rx interrupts
	rtl8139_write16 (dev, RTL8139_MULTIINTR, rtl8139_read16 (dev, RTL8139_MULTIINTR) & RTL8139_MULTIINTR_CLEAR);

	// enable rx and tx
	unsigned char chipstatus = rtl8139_read8 (dev, RTL8139_CHIPCMD);

	if (chipstatus & RTL8139_CMD_TX_ENABLE || chipstatus & RTL8139_CMD_RX_ENABLE)
		rtl8139_write8 (dev, RTL8139_CHIPCMD, RTL8139_CMD_RX_ENABLE | RTL8139_CMD_TX_ENABLE);
	
	if (dev->chipset >= CH_8139B) {
		/* Disable magic packet scanning, which is enabled
		 * when PM is enabled in Config1. */
		rtl8139_write8 (dev, RTL8139_CONFIG3, rtl8139_read8 (dev, RTL8139_CONFIG3) & ~RTL8139_CONFIG3_MAGIC);
	}

	rtl8139_write16 (dev, RTL8139_INTRMASK, MYRT_INTS);

	/* create new network device */
	ifdev = netdev_create (dev->addr_mac, &rtl8139_read, &rtl8139_write, dev->addr_io);

	if (!ifdev)
		return 0;

	return 1;
}

bool rtl8139_acthandler (unsigned act, char *block, unsigned block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			return init_rtl8139 ();
		}
		break;
		case DEV_ACT_READ:
		{

			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{
			return rtl8139_tx (rtl8139_dev, block, block_len);
		}
		break;
	}

	return 0;
}
#endif

