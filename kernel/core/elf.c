/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <elf.h>

int exec_elf (unsigned char *image, unsigned *entry, unsigned *elf_data, unsigned *elf_data_off, unsigned *elf_bss)
{
	unsigned s, r, reloc_size;

	elf_reloc_t *reloc;
	elf_sect_t *sect;
	elf_file_t *file;

	/* validate */
	file = (elf_file_t *) image;

	if (!arch_elf_detect (file))
		return -1;

	/* find the BSS and allocate memory for it
		This must be done BEFORE doing any relocations */
	for (s = 0; s < file->num_sects; s ++) {
		sect = (elf_sect_t *) (image + file->shtab_offset + file->shtab_ent_size * s);

		if(sect->type != 8)	/* NOBITS */
			continue;

		r = sect->size;

		if (r < 1) {
			DPRINT (DBG_ELF, "ELF .bss section is too small (%d bytes)", r);
			continue;
		}

		*elf_bss = (unsigned) sect->virt_adr;

		if (!*elf_bss) {
			DPRINT (DBG_ELF, "Section .bss obtain wrong address: 0x%x", *elf_bss);
			continue;
		}

		/* clean memory for bss section */
		memset (image+sect->offset, 0, sect->size);

		break;
	}

	elf_sect_t *rodata = 0;

	/* for each section... */
	for (s = 0; s < file->num_sects; s ++) {
		sect = (elf_sect_t *) (image + file->shtab_offset + file->shtab_ent_size * s);

		if (sect->type == 1) {
			if (sect->sect_name == 0x17 || sect->sect_name == 0x27) {
				rodata = sect;
				break;
			}
		}

		/* is it a relocation section?
		xxx - we don't handle the extra addend for RELA relocations */
		if(sect->type == 4)	/* RELA */
			reloc_size = 12;
		else if(sect->type == 9)/* REL */
			reloc_size = 8;
		else
			continue;
	}

	/* rodata section is not required */
	if (!rodata)
		DPRINT (DBG_ELF, "rodata section does not exist");

	/* find start of .text and make it the entry point */
	(*entry) = 0;

	for (s = 0; s < file->num_sects; s ++) {
		sect = (elf_sect_t *) (image + file->shtab_offset + file->shtab_ent_size * s);
		
		if (sect->sect_name != 0x0b && sect->sect_name != 0x1b)
			continue;

		(*entry) = (unsigned) sect->virt_adr;

		break;
	}

	/* copy rodata section to our memory area section */
	if (rodata) {
		*elf_data = (unsigned) rodata->virt_adr;
		*elf_data_off = (unsigned) rodata->offset;
	} else {
		*elf_data = (unsigned) 0;
		*elf_data_off = (unsigned) 0;
	}

	if (!(*entry)) {
		printf ("ELF -> Can't find section .text, so entry point is unknown\n");
		return -1;
	}

	return 0;
}
