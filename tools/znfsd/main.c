#include <stdio.h>
#include <stdlib.h>
#include "client.h"

int init ()
{
	if (!net_init ())
		return 0;

	if (!client_init ())
		return 0;

	chroot ("root");

	return 1;
}

int loop ()
{
	int ret = 1;

	while (ret)
		ret = net_loop ();
}

int main (int argc, char **argv)
{
	printf ("ZeX/OS Network FileSystem Daemon\n");

	if (!init ()) {
		printf ("> ERROR -> !init ()\n");
		return -1;
	}

	loop ();

	return 0;
} 
