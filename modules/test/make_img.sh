#!/bin/bash
# Folder name of your application
APPNAME="mykmod"

# Compile source
make -C $APPNAME

# Optimized for 1,4MB floppy
dd if=/dev/zero of=$APPNAME.img bs=1440k count=1
mkfs.vfat $APPNAME.img

mkdir floppy
mount -oloop $APPNAME.img floppy
cp -r ./$APPNAME/* floppy
umount floppy
rmdir floppy