/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>
#include "buffer.h"
#include "source.h"

var_t var_list;
var_t *function_curr;
unsigned bin_pos;
unsigned data_pos;


var_t *var_find (char *name)
{
	var_t *var;

	for (var = var_list.next; var != &var_list; var = var->next) {
		if (!strcmp (var->name, name))
			return var;
	}

	return 0;
}

var_t *var_create (char type, char *name, unsigned len, void *address, unsigned line)
{
	var_t *var;
	
	/* alloc and init context */
	var = (var_t *) malloc (sizeof (var_t));

	if (!var)
		return 0;

	var->type = type;

	var->name = (char *) malloc (sizeof (char) * (len + 1));

	if (!var->name)
		return 0;

	memcpy (var->name, name, len);
	var->name[len] = '\0';

	var->address = address;
	var->offset = 0;

	/* add into list */
	var->next = &var_list;
	var->prev = var_list.prev;
	var->prev->next = var;
	var->next->prev = var;

	printf ("var_create () -> '%s' on line %d\n", var->name, line);

	return var;
}

int source_nextline (char *source, unsigned size)
{
	unsigned i;

	for (i = 0; i < size; i ++) {
		//printf ("znak: %c : %d\n", source[i], source[i]);
		if (source[i] == '\n') {
			//printf ("source_nextline: %d\n", i);
			return i;
		}
	}

	return 0;
}

char *source_param (char *source, unsigned *size)
{
	unsigned i;
	char *param = (char *) 0;
	unsigned len = *size;

	for (i = 0; i < len; i ++) {
		if (!param) {
			if (source[i] == ' ' || source[i] == '\t') {
				i ++;
				continue;
			}

			if ((source[i] >= 'a' && source[i] <= 'z') ||
			    (source[i] >= 'A' && source[i] <= 'Z') || 
			    (source[i] >= '0' && source[i] <= '9'))
				param = source + i;
		} else if (source[i] == ' ' || source[i] == '\t' || source[i] == '\n' || source[i] == ';') {
			*size = i;
			return param;
		}
	}

	return 0;
}

char *source_paramfirst (char *source, unsigned *size)
{
	unsigned i;
	char *param = (char *) 0;
	unsigned len = *size;

	for (i = 0; i < len; i ++) {
		if (!param) {
			if (source[i] == ' ' || source[i] == '\t')
				continue;

			if ((source[i] >= 'a' && source[i] <= 'z') ||
			    (source[i] >= 'A' && source[i] <= 'Z') || 
			    (source[i] >= '0' && source[i] <= '9'))
				param = source + i;
		} else if (source[i] == ' ' || source[i] == ',' || source[i] == '\t') {
			*size = i;
			return param;
		}
	}

	return 0;
}

char *source_paramsecond (char *source, unsigned *size)
{
	unsigned i;
	char *param = (char *) 0;
	unsigned len = *size;

	for (i = 0; i < len; i ++) {
		if (!param) {
			if (source[i] == ' ' || source[i] == '\t' || source[i] == ',')
				continue;

			if ((source[i] >= 'a' && source[i] <= 'z') ||
			    (source[i] >= 'A' && source[i] <= 'Z') || 
			    (source[i] >= '0' && source[i] <= '9'))
				param = source + i;
			else if (source[i] == '\'')
				param = source + i;
		} else if (source[i] == ' ' || source[i] == '\t' || source[i] == '\n' || source[i] == ';') {
			*size = i-2;
			return param;
		}
	}

	return 0;
}

int source_parse (char *source, unsigned size)
{
	unsigned i;
	unsigned line = 1;

	for (i = 0; i < size; i ++) {
		/* GLOBAL */
		if (!strncmp ("global ", source+i, 7)) {
			unsigned var = size-i-7;
			char *param = source_param (source+i+7, &var);

			if (!param) {
				printf ("ERROR -> !param\n");
				return 0;
			}

			var_create (VAR_TYPE_GLOBAL, param, var, (void *) 0x0 + i, line);

			i += source_nextline (source+i, size-i);

			line ++;
			continue;
		}

		/* MOV */
		if (!strncmp ("mov ", source+i, 4)) {
			unsigned reg_len = size-i-4;
			char *paramreg = source_paramfirst (source+i+4, &reg_len);

			if (!paramreg) {
				printf ("ERROR -> !paramreg\n");
				return 0;
			}

			unsigned val_len = size-reg_len-i-4;
			char *paramval = source_paramsecond (source+i+reg_len+4, &val_len);

			if (!paramval) {
				printf ("ERROR -> !val_len\n");
				return 0;
			}

			i += source_nextline (source+i, size-i);

			paramreg[reg_len] = '\0';
			paramval[val_len] = '\0';

			printf ("mov: %s, %s\n", paramreg, paramval);

			unsigned char reg = 0;

			if (paramval[0] != 'e') {	/* MOV 8bit/32bit */
				if (paramreg[0] == 'e' && paramreg[1] == 'a' && paramreg[2] == 'x')
					reg = 0xb8;	// eax
				else if (paramreg[0] == 'e' && paramreg[1] == 'b' && paramreg[2] == 'x')
					reg = 0xbb;	// ebx
				else if (paramreg[0] == 'e' && paramreg[1] == 'c' && paramreg[2] == 'x')
					reg = 0xb9;	// ecx
				else if (paramreg[0] == 'e' && paramreg[1] == 'd' && paramreg[2] == 'x')
					reg = 0xba;	// edx

				buffer_copy (bin_pos, (void *) &reg, 1);
				bin_pos ++;

				/* Let's convert parameter to binary number */
				if (paramval[0] == '\'') {
					if (val_len < 3) {
						printf ("ERROR -> wrong parameter syntax - %d != 3, line: %d\n", val_len, line);
						return 0;
					}

					if (paramval[2] != '\'' || paramval[1] == '\'') {
						printf ("ERROR -> wrong parameter syntax, line: %d\n", line);
						return 0;
					}

					int c = paramval[1]; 
					//printf ("mov val char = %c\n", c);

					buffer_copy (bin_pos, (void *) &c, sizeof (int));
					bin_pos += sizeof (int);

				} else if (paramval[0] == '0' && paramval[1] == 'x') {
					char *endptr, *str = paramval+2;

					long val = strtol (str, &endptr, 16);

					//printf ("mov val hexa = %d\n", val);

					buffer_copy (bin_pos, (void *) &val, sizeof (int));
					bin_pos += sizeof (int);
				} else if (paramval[0] >= '0' && paramval[0] <= '9') {
					unsigned x;
					for (x = 1; x < val_len-1; x ++) {
						if (!(paramval[x] >= '0' && paramval[x] <= '9')) {
							printf ("ERROR -> wrong parameter syntax - only numbers are allowed, line: %d\n", line);
							return 0;
						}
					}

					int num = atoi (paramval); 

					//printf ("mov val digit = %d\n", num);

					buffer_copy (bin_pos, (void *) &num, sizeof (int));
					bin_pos += sizeof (int);

				} else {
					var_t *var = var_find (paramval);

					if (!var) {
						printf ("ERROR -> unknown mov variable '%s', line %d\n", paramval, line);
						return 0;
					}

					unsigned num = (unsigned) var->offset + 0x800000;

					buffer_copy (bin_pos, (void *) &num, sizeof (unsigned));
					bin_pos += sizeof (unsigned);
				}

			} else {	/*MOV 32bit */

				reg = 0x89;	// 32bit
				
				buffer_copy (bin_pos, (void *) &reg, 1);
				bin_pos ++;

				unsigned char regb = 0;

				if (!strncmp (paramreg, "eax", 3)) {
					if (!strncmp (paramval, "eax", 3))
						regb = 0xc0;
					else if (!strncmp (paramval, "ebx", 3))
						regb = 0xd8;
					else if (!strncmp (paramval, "ecx", 3))
						regb = 0xc8;
					else if (!strncmp (paramval, "edx", 3))
						regb = 0xd0;
				} else if (!strncmp (paramreg, "ebx", 3)) {
					if (!strncmp (paramval, "eax", 3))
						regb = 0xc3;
					else if (!strncmp (paramval, "ebx", 3))
						regb = 0xdb;
					else if (!strncmp (paramval, "ecx", 3))
						regb = 0xcb;
					else if (!strncmp (paramval, "edx", 3))
						regb = 0xd3;
				} else if (!strncmp (paramreg, "ecx", 3)) {
					if (!strncmp (paramval, "eax", 3))
						regb = 0xc1;
					else if (!strncmp (paramval, "ebx", 3))
						regb = 0xd9;
					else if (!strncmp (paramval, "ecx", 3))
						regb = 0xc9;
					else if (!strncmp (paramval, "edx", 3))
						regb = 0xd1;
				} else if (!strncmp (paramreg, "edx", 3)) {
					if (!strncmp (paramval, "eax", 3))
						regb = 0xc2;
					else if (!strncmp (paramval, "ebx", 3))
						regb = 0xda;
					else if (!strncmp (paramval, "ecx", 3))
						regb = 0xca;
					else if (!strncmp (paramval, "edx", 3))
						regb = 0xd2;
				}

				buffer_copy (bin_pos, (void *) &regb, 1);
				bin_pos ++;

			}

			line ++;
			continue;
		}

		/* CMP */
		if (!strncmp ("cmp ", source+i, 4)) {
			unsigned reg_len = size-i-4;
			char *paramreg = source_paramfirst (source+i+4, &reg_len);

			if (!paramreg) {
				printf ("ERROR -> !paramreg\n");
				return 0;
			}

			unsigned val_len = size-reg_len-i-4;
			char *paramval = source_paramsecond (source+i+reg_len+4, &val_len);

			if (!paramval) {
				printf ("ERROR -> !val_len\n");
				return 0;
			}

			i += source_nextline (source+i, size-i);

			paramreg[reg_len] = '\0';
			paramval[val_len] = '\0';

			printf ("cmp: %s, %s\n", paramreg, paramval);

			unsigned short reg = 0;
			unsigned x = 2;

			if (paramval[0] != 'e') {	/* CMP 8bit/32bit */
				if (paramreg[0] == 'e' && paramreg[1] == 'a' && paramreg[2] == 'x') {
					reg = 0x3d;	// eax
					x = 1;
				} else if (paramreg[0] == 'e' && paramreg[1] == 'b' && paramreg[2] == 'x')
					reg = 0xfb81;	// ebx
				else if (paramreg[0] == 'e' && paramreg[1] == 'c' && paramreg[2] == 'x')
					reg = 0xf981;	// ecx
				else if (paramreg[0] == 'e' && paramreg[1] == 'd' && paramreg[2] == 'x')
					reg = 0xfa81;	// edx

				buffer_copy (bin_pos, (void *) &reg, x);
				bin_pos += x;

				/* Let's convert parameter to binary number */
				if (paramval[0] == '\'') {
					if (val_len < 3) {
						printf ("ERROR -> wrong parameter syntax - %d != 3, line: %d\n", val_len, line);
						return 0;
					}

					if (paramval[2] != '\'' || paramval[1] == '\'') {
						printf ("ERROR -> wrong parameter syntax, line: %d\n", line);
						return 0;
					}

					int c = paramval[1]; 
					//printf ("mov val char = %c\n", c);

					buffer_copy (bin_pos, (void *) &c, sizeof (int));
					bin_pos += sizeof (int);

				} else if (paramval[0] == '0' && paramval[1] == 'x') {
					char *endptr, *str = paramval+2;

					long val = strtol (str, &endptr, 16);

					//printf ("mov val hexa = %d\n", val);

					buffer_copy (bin_pos, (void *) &val, sizeof (int));
					bin_pos += sizeof (int);
				} else if (paramval[0] >= '0' && paramval[0] <= '9') {
					unsigned x;
					for (x = 1; x < val_len-1; x ++) {
						if (!(paramval[x] >= '0' && paramval[x] <= '9')) {
							printf ("ERROR -> wrong parameter syntax - only numbers are allowed, line: %d\n", line);
							return 0;
						}
					}

					int num = atoi (paramval); 

					//printf ("mov val digit = %d\n", num);

					buffer_copy (bin_pos, (void *) &num, sizeof (int));
					bin_pos += sizeof (int);

				} else {
					var_t *var = var_find (paramval);

					if (!var) {
						printf ("ERROR -> unknown mov variable '%s', line %d\n", paramval, line);
						return 0;
					}

					unsigned num = (unsigned) var->offset + 0x800000;

					buffer_copy (bin_pos, (void *) &num, sizeof (unsigned));
					bin_pos += sizeof (unsigned);
				}

			} else {	/* CMP 32bit */

				reg = 0x39;	// 32bit
				
				buffer_copy (bin_pos, (void *) &reg, 1);
				bin_pos ++;

				unsigned char regb = 0;

				if (!strncmp (paramreg, "eax", 3)) {
					if (!strncmp (paramval, "eax", 3))
						regb = 0xc0;
					else if (!strncmp (paramval, "ebx", 3))
						regb = 0xd8;
					else if (!strncmp (paramval, "ecx", 3))
						regb = 0xc8;
					else if (!strncmp (paramval, "edx", 3))
						regb = 0xd0;
				} else if (!strncmp (paramreg, "ebx", 3)) {
					if (!strncmp (paramval, "eax", 3))
						regb = 0xc3;
					else if (!strncmp (paramval, "ebx", 3))
						regb = 0xdb;
					else if (!strncmp (paramval, "ecx", 3))
						regb = 0xcb;
					else if (!strncmp (paramval, "edx", 3))
						regb = 0xd3;
				} else if (!strncmp (paramreg, "ecx", 3)) {
					if (!strncmp (paramval, "eax", 3))
						regb = 0xc1;
					else if (!strncmp (paramval, "ebx", 3))
						regb = 0xd9;
					else if (!strncmp (paramval, "ecx", 3))
						regb = 0xc9;
					else if (!strncmp (paramval, "edx", 3))
						regb = 0xd1;
				} else if (!strncmp (paramreg, "edx", 3)) {
					if (!strncmp (paramval, "eax", 3))
						regb = 0xc2;
					else if (!strncmp (paramval, "ebx", 3))
						regb = 0xda;
					else if (!strncmp (paramval, "ecx", 3))
						regb = 0xca;
					else if (!strncmp (paramval, "edx", 3))
						regb = 0xd2;
				}

				buffer_copy (bin_pos, (void *) &regb, 1);
				bin_pos ++;

			}

			line ++;
			continue;
		}

		/* INT */
		if (!strncmp ("int ", source+i, 4)) {
			unsigned var = size-i-4;
			char *param = source_param (source+i+4, &var);

			if (!param) {
				printf ("ERROR -> !param\n");
				return 0;
			}

			i += source_nextline (source+i, size-i);

			unsigned char intr = 0xcd; // int
			buffer_copy (bin_pos, (void *) &intr, 1);
			bin_pos ++;

			param[var] = '\0';

			if (param[0] == '0' && param[1] == 'x') {
				char *endptr, *str = param+2;

				long val = strtol (str, &endptr, 16);

				//printf ("int: 0x%x\n", val);

				buffer_copy (bin_pos, (void *) &val, 1);
				bin_pos ++;
			} else {
				unsigned x;
				for (x = 0; x < var; x ++) {
					if (!isdigit (param[x])) {
						printf ("ERROR -> wrong parameter syntax - only numbers are allowed, line: %d\n", line);
						return 0;
					}
				}

				int num = atoi (param); 

				buffer_copy (bin_pos, (void *) &num, 1);
				bin_pos ++;
			}

			printf ("int: %s\n", param);

			line ++;
			continue;
		}

		/* JMP */
		if (!strncmp ("jmp ", source+i, 4)) {
			unsigned v = size-i-4;
			char *param = source_param (source+i+4, &v);

			if (!param) {
				printf ("ERROR -> !param\n");
				return 0;
			}

			i += source_nextline (source+i, size-i);

			unsigned char jmp = 0xeb; // jmp
			buffer_copy (bin_pos, (void *) &jmp, 1);
			bin_pos ++;

			param[v] = '\0';

			var_t *var = var_find (param);

			if (!var) {
				printf ("ERROR -> unknown variable '%s', line %d\n", source+i, line);
				return 0;
			}

			unsigned diff = (bin_pos - var->offset);

			if (diff > 0xff) {
				printf ("ERROR -> it is not 'near jump', line %d\n", line);
				return 0;
			}

			unsigned char val = 0xff - diff;

			buffer_copy (bin_pos, (void *) &val, 1);
			bin_pos ++;

			printf ("jmp: %s\n", param);

			line ++;
			continue;
		}

		/* JNZ */
		if (!strncmp ("jnz ", source+i, 4)) {
			unsigned v = size-i-4;
			char *param = source_param (source+i+4, &v);

			if (!param) {
				printf ("ERROR -> !param\n");
				return 0;
			}

			i += source_nextline (source+i, size-i);

			unsigned char jmp = 0x75; // jnz
			buffer_copy (bin_pos, (void *) &jmp, 1);
			bin_pos ++;

			param[v] = '\0';

			var_t *var = var_find (param);

			if (!var) {
				printf ("ERROR -> unknown variable '%s', line %d\n", source+i, line);
				return 0;
			}

			unsigned diff = (bin_pos - var->offset);

			if (diff > 0xff) {
				printf ("ERROR -> it is not 'near jump not-zero', line %d\n", line);
				return 0;
			}

			unsigned char val = 0xff - diff;

			buffer_copy (bin_pos, (void *) &val, 1);
			bin_pos ++;

			printf ("jnz: %s\n", param);

			line ++;
			continue;
		}

		/* JZ */
		if (!strncmp ("jz ", source+i, 3)) {
			unsigned v = size-i-3;
			char *param = source_param (source+i+3, &v);

			if (!param) {
				printf ("ERROR -> !param\n");
				return 0;
			}

			i += source_nextline (source+i, size-i);

			unsigned char jmp = 0x74; // jz
			buffer_copy (bin_pos, (void *) &jmp, 1);
			bin_pos ++;

			param[v] = '\0';

			var_t *var = var_find (param);

			if (!var) {
				printf ("ERROR -> unknown variable '%s', line %d\n", source+i, line);
				return 0;
			}

			unsigned diff = (bin_pos - var->offset);

			if (diff > 0xff) {
				printf ("ERROR -> it is not 'near jump zero', line %d\n", line);
				return 0;
			}

			unsigned char val = 0xff - diff;

			buffer_copy (bin_pos, (void *) &val, 1);
			bin_pos ++;

			printf ("jz: %s\n", param);

			line ++;
			continue;
		}

		/* INC */
		if (!strncmp ("inc ", source+i, 4)) {
			unsigned v = size-i-4;
			char *param = source_param (source+i+4, &v);

			if (!param) {
				printf ("ERROR -> !param\n");
				return 0;
			}

			i += source_nextline (source+i, size-i);

			param[v] = '\0';

			if (v < 2) {
				printf ("ERROR -> inc: bad register name '%s', line %d\n", param, line);
				return 0;
			}

			unsigned char reg = 0;

			if (!strncmp (param, "eax", 3))
				reg = 0x40;
			else if (!strncmp (param, "ebx", 3))
				reg = 0x43;
			else if (!strncmp (param, "ecx", 3))
				reg = 0x41;
			else if (!strncmp (param, "edx", 3))
				reg = 0x42;

			buffer_copy (bin_pos, (void *) &reg, 1);
			bin_pos ++;

			printf ("inc: %s\n", param);

			line ++;
			continue;
		}

		/* RET */
		if (!strncmp ("ret", source+i, 3)) {
			if (!function_curr) {
				printf ("ERROR -> ret cannot be called out of function, line: %d\n", line);
				return 0;
			}

			printf ("Function %s end on line %d\n", function_curr->name, line);

			function_curr = 0;

			unsigned char ret = 0xc3;
			buffer_copy (bin_pos, (void *) &ret, 1);
			bin_pos ++;

			i += source_nextline (source+i, size-i);

			line ++;
			continue;
		}

		/* HLT */
		if (!strncmp ("hlt", source+i, 3)) {
			unsigned char hlt = 0xf4;
			buffer_copy (bin_pos, (void *) &hlt, 1);
			bin_pos ++;

			i += source_nextline (source+i, size-i);

			line ++;
			continue;
		}

		if ((source[i] >= 'a' && source[i] <= 'z') || (source[i] >= 'A' && source[i] <= 'Z')) {
			unsigned y;

			for (y = i; y < size-i; y ++) {
				/* Function */
				if (source[y] == ':') {
					source[y] = '\0';
					
					var_t *var = var_find (source+i);

					if (!var) {
						printf ("ERROR -> unknown variable '%s', line %d\n", source+i, line);
						return 0;
					}

					printf ("Function '%s' on line %d\n", var->name, line);

					var->offset = bin_pos;

					function_curr = var;		// set current function
					break;
				}

				if (source[y] == ' ' || source[y] == '\t' || source[y] == '\n' || source[y] == 32 || source[y] == ';') {
					/* register db variable */
					if (!strncmp (source+y, " db ", 4)) {
						unsigned m = 0;
						while (m < y) {
							if (source[i+m] == ' ')
								break;

							m ++;
						}
						var_t *var = var_create (VAR_TYPE_DB, source+i, m, (void *) 0x0 + i, line);

						if (!var)
							return 0;

						var->offset = data_pos;

						unsigned r = source_nextline (source+y+4, size-y-4);

						if (r > 2) {
							if (source[y+4] == '\'') {
								unsigned k = 0;
								while (k < r) {
									if (source[y+6+k] == '\'')
										break;

									k ++;
								}

								buffer_copy (data_pos, (char *) source+y+5, k+1);
								data_pos += (k+1);

								if (source[y+7+k] == ',') {
									char *param = source+y+8+k;

									/* jump over space */
									if (param[0] == ' ')
										param ++;

									k = 0;
									while (k < r) {
										if (!(param[k] >= 'a' && param[k] <= 'z') &&
										    !(param[k] >= 'A' && param[k] <= 'Z') && 
										    !(param[k] >= '0' && param[k] <= '9')) {
											param[k] = '\0';
											break;
										}

										k ++;
									}

									if (param[0] == '0' && param[1] == 'x') {
										char *endptr2, *str2 = param+2;

										long v = strtol (str2, &endptr2, 16);

										buffer_copy (data_pos, (void *) &v, 1);
										data_pos ++;
									} else {
										unsigned x;
										for (x = 0; x < 4; x ++) {
											if (!isdigit (param[x])) {
												printf ("ERROR -> wrong parameter syntax - only numbers are allowed, line: %d\n", line);
												return 0;
											}
										}

										int num = atoi (param); 

										buffer_copy (data_pos, (void *) &num, 1);
										data_pos ++;
									}
								}
							}
						} else {
							printf ("ERROR -> db variable - wrong syntax, line: %d\n", line);
							return 0;
						}

						y += r+4;

						break;
					} else {
						source[y] = '\0';
						printf ("ERROR -> unknown command '%s', line: %d\n", source+i, line);
						return 0;
					}
				}
			}

			i = y;
		} else {
			//if (source[i] != ' ' || source[i] != ';')
			//printf ("ERROR -> unspecified character '%c', line: %d\n", source[i], line);
			if (source[i] == ';') {
				i += source_nextline (source+i, size-i);
				line ++;
				continue;
			}
		}

		if (source[i] == '\n')
			line ++;
	}

	return 1;
}

char *source_open (char *file, unsigned *size)
{
/*	int fd = open (file, O_RDONLY);
	
	if (!fd) {
		printf ("error -> file '%s' not found !\n", file);
		return 0;
	}
	
	char *buffer = (char *) malloc (sizeof (char) * 10240);

	if (!buffer) {
		printf ("ERROR -> out of memory !\n");
		return 0;
	}

	int ret;
	unsigned l = 0;

	while (1) {
		ret = read (fd, buffer+l, 512);

		if (ret < 1)
			break;

		l += (unsigned) ret;
	}
	
	*size = l;

	close (fd);

	printf ("Source: %s\n-=-=-=-=-=-=-=-=-\n\n", buffer);*/

	FILE *f = fopen (file, "r");

	if (!f) {
		printf ("error -> file '%s' not found !\n", file);
		return 0;
	}

	fseek (f, 0, SEEK_END);
	unsigned flen = ftell (f);
	fseek (f, 0, SEEK_SET);

	*size = flen;
	
	char *buffer = (char *) malloc (sizeof (char) * flen + 1);
	
	if (!buffer) {
		printf ("ERROR -> out of memory !\n");
		return 0;
	}
	
	fread (buffer, flen, 1, f);
	
	fclose (f);

	return buffer;
}

int source_init ()
{
	var_list.next = &var_list;
	var_list.prev = &var_list;

	bin_pos = 0x1000;
	data_pos = 0x2000;

	function_curr = 0;

	return 1;
}
