/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _TASK_H
#define _TASK_H

#include <build.h>
#include <setjmp.h>
#include <spinlock.h>
#include <paging.h>
#include <mytypes.h>

/* jmp_buf (E)IP and (E)SP register names for various environments:
			Tinylib		TurboC	DJGPP	Linux (glibc5) */
#define	JMPBUF_IP	eip	/*	j_ip	__eip	__pc	*/
#define	JMPBUF_SP	esp	/*	j_sp	__esp	__sp	*/
#define	JMPBUF_FLAGS	eflags  /*	j_flag	__eflags ?	*/

#ifdef ARCH_i386
#define	USER_STACK_SIZE	10240
#endif

#ifdef ARCH_arm
#define	USER_STACK_SIZE	8192
#endif

/* Task structure */
typedef struct task_context {
	struct task_context *next, *prev;

	char name[25];
	unsigned char stacks[USER_STACK_SIZE];
#ifdef ARCH_i386
	jmp_buf state;
#endif
#ifdef ARCH_arm
	unsigned int *sp;
	unsigned int *entry;
#endif
	unsigned char priority;
	unsigned char attick;
	unsigned short id;
	unsigned lasttick;
	
	spinlock_t *spinlock;
	page_ent_t *page_cover;

	enum {
		TS_NULL = 0, TS_RUNNABLE = 1, TS_BLOCKED = 2, TS_ZOMBIE = 3
	} status;
} task_t;


extern task_t *_curr_task;
extern task_t *task_create (char *name, unsigned entry, unsigned priority);
extern bool task_done (task_t *task);
extern task_t *task_find (unsigned short id);

#endif
