/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PARTITION_H
#define _PARTITION_H

#include <dev.h>
#include <fs.h>

/* Partition structure */
typedef struct partition_context {
	struct partition_context *next, *prev;

	char name[15];
	unsigned char id;
	unsigned sector_start;
	unsigned base_io;
	fs_t *fs;
} partition_t;

typedef struct {
	unsigned char status;
	unsigned char chs_f[3];
	unsigned char type;
	unsigned char chs_l[3];
	unsigned lba;
	unsigned blocks;
} ptable_t;

/* externs */
extern partition_t *partition_add (dev_t *dev, fs_t *fs, unsigned sector);
extern partition_t *partition_find (char *name);
extern partition_t *partition_findbydev (dev_t *dev);
extern unsigned partition_table (dev_t *dev);
extern unsigned partition_table_new (dev_t *dev, ptable_t *ntable);
extern unsigned int init_partition ();

#endif
