/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <errno.h>
#include <stdio.h>
#include <errno.h>

extern int errno;

static char *errno_list[] = { "Undefined error",
			      "Out of the memory" };

void perror (const char *s)
{
	if (s)
		printf ("%s: ", s);

	/* TODO more errno messages */
	switch (errno) {
		case ENOMEM:
			puts (errno_list[1]);
			break;
		default:
			puts (errno_list[0]);
	}
}
