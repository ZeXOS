/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <appcl.h>
#include <stdio.h>
#include <time.h>

void ztest_draw ()
{
	time_t t = time (0);

	struct tm time;
	localtime_r (&t, &time);

	char str[128];
	sprintf (str, "%d:%d:%d", time.tm_hour, time.tm_min, time.tm_sec+1);

	zgui_puts (28, 15, "CLOCK", 0);
	zgui_puts (20, 30, str, 0);
}

int main (int argc, char **argv)
{
	int exit = 0;

	if (zgui_init () == -1)
		return -1;

	zgui_resize (100, 70);

	while (!exit) {
		unsigned state = zgui_event ();

		if (state & APPCL_STATE_REDRAW)
			ztest_draw ();

		if (state & APPCL_STATE_EXIT)
			exit = 1;
	}

	return zgui_exit ();
}
 
