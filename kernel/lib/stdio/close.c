/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Martin 'povik' Poviser (martin.povik@gmail.com)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <file.h>
#include <fd.h>
#include <net/socket.h>
#include <pipe.h>
#include <cache.h>

int close (int fd)
{
	fd_t *f = fd_get (fd);

	if (!f)
		return 0;

	if (f->flags & FD_SOCK)	// socket close
		sclose (fd);
	
	if (f->flags & FD_PIPE) // pipe close
		pipe_close (fd);
	
	f->id = 0;
	f->flags = 0;
	f->e = 0;

	if (f->s)
		cache_closebyptr (f->s);

	/* delete old file descriptor from fd_list */
	f->next->prev = f->prev;
	f->prev->next = f->next;

	return 1;
}
 
