/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <time.h>
#include "proto.h"
#include "net.h"

int fd;
static struct sockaddr_in serverSock;
static struct sockaddr_in6 serverSock6;
static struct hostent *host;

char *buf;

/** Network Send function */
int net_send (char *buf, unsigned len)
{
	int ret = send (fd, buf, len, 0);
#ifndef LINUX
	schedule ();
#endif
	return ret;
}

unsigned strlenn (char *str, unsigned len)
{
	unsigned i = len;

	while (str[i] != '\n') {
		i --;

		if (!i)
			return 0;

		return i+1;
	}

	return 0;
}

int net_loop ()
{
	int ret = recv (fd, buf, BUFSIZE-1, 0);

	if (ret > 0) {
		buf[ret] = '\0';

		//printf ("net_loop () -> recv (): '%s' : %d\n", buf, ret);

		if (!proto_handler (buf, ret)) {
			printf ("ERROR -> proto_handler () failed\n");
			return 0;
		}
	} else {
#ifdef LINUX
		if (ret == 0) {
#else
		if (ret == -1) {
#endif
			printf ("INFO -> Connection closed by server\n");
			return 0;
		}
	}

	usleep (60);

	return 1;
}

int net_close ()
{
	close (fd);
	
	printf ("net_close () -> close (%d)\n", fd);

	return 1;
}

int init_net (char *address, int port, int flags)
{
	int proto = AF_INET;

	if (flags & NET_IPV6)
		proto = AF_INET6;

	if ((host = gethostbyname ((char *) address)) == NULL) {
		printf ("Wrong address -> %s:%d\n", address, port);
		return 0;
	}

	if ((fd = socket (proto, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return 0;
	}

	if (flags & NET_IPV6) {
		serverSock6.sin6_family = proto;
		serverSock6.sin6_port = htons (port);
		memcpy (&(serverSock6.sin6_addr), host->h_addr, host->h_length);

		if (connect (fd, (struct sockaddr *) &serverSock6, sizeof (serverSock6)) == -1) {
			printf ("Connection cant be estabilished -> %s:%d\n", address, port);
			return 0;
		}
	} else {
		serverSock.sin_family = proto;
		serverSock.sin_port = htons (port);
		memcpy (&(serverSock.sin_addr), host->h_addr, host->h_length);

		if (connect (fd, (struct sockaddr *) &serverSock, sizeof (serverSock)) == -1) {
			printf ("Connection cant be estabilished -> %s:%d\n", address, port);
			return 0;
		}
	}

	int oldFlag = fcntl (fd, F_GETFL, 0);
	if (fcntl (fd, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	buf = (char *) malloc (sizeof (char) * BUFSIZE);

	if (!buf) {
		printf ("Receive buffer -> out of memory !\n");
		return 0;
	}

	return 1;
}
