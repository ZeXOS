/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _BUTTON_H
#define _BUTTON_H

#include "window.h"

typedef struct zde_btn_context {
	struct zde_btn_context *next, *prev;

	unsigned short x;
	unsigned short y;
	unsigned short *bitmap;
	unsigned short name_len;
	char *name;
	void *(*entry) (void *);
	void *arg;
	void *object;
} zde_btn_t;

extern zde_btn_t *create_button_window (char *name, int type, void *(*entry) (void *), void *arg, zde_win_t *window);
extern unsigned destroy_button_window (zde_win_t *window);
extern int draw_button_window (zde_win_t *window);
extern int init_button ();

#endif
