/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define NET_IPV4_TO_ADDR(a, b, c, d) \
	(((unsigned)(d) << 24) | (((unsigned)(c) & 0xff) << 16) | (((unsigned)(b) & 0xff) << 8) | ((unsigned)(a) & 0xff))

in_addr_t inet_addr (const char *src)
{
	if (!src)
		return 0;

	unsigned char a = 0;
	unsigned char b = 0;
	unsigned char c = 0;
	unsigned char d = 0;

	unsigned g = 0;
	unsigned i = 0;
	unsigned y = strlen (src);

	if (!y)
		return 0;

	char *str = (char *) malloc (sizeof (char) * (y + 1));

	if (!str)
		return 0;

	memcpy (str, src, y);
	str[y] = '\0';

	unsigned h[4];

	while (i < y) {
		if (str[i] == '.') {
			str[i] = '\0';
			h[g] = i+1;
			g ++;
		}
	}

	if (g != 3) {
		free (str);
		return -1;
	}
	
	a = atoi (str);
	b = atoi (str+h[0]);
	c = atoi (str+h[1]);
	d = atoi (str+h[2]);

	free (str);

	g = NET_IPV4_TO_ADDR (a, b, c, d);
	
	return (in_addr_t) g;
}

