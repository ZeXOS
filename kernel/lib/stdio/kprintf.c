/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <stdarg.h>
#include <string.h>
#include <_printf.h>

static int kprintf_help (unsigned c, void **ptr)
{
	video_putch (c);

	return 0;
}

void kprintf (const char *fmt, ...)
{
	va_list args;

	va_start (args, fmt);
	(void) do_printf (fmt, args, kprintf_help, NULL);
	va_end (args);
}

/* for development use */
extern unsigned debug;
void DPRINT (unsigned type, const char *fmt, ...)
{
	if (!(debug & type))
		return;

	video_color (4, 0);
	kprintf ("DEBUG -> ");
	video_color (15, 0);

	va_list args;

	va_start (args, fmt);
	(void) do_printf (fmt, args, kprintf_help, NULL);
	va_end (args);

	video_putch ('\n');
}
