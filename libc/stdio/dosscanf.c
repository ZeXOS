/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <_printf.h> /* fnptr_t */
#include <string.h> /* strlen() */
#include <stdarg.h> /* va_list, va_arg() */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

static int scanf_getint (const char *str, int *n)
{
	*n = atoi (str);

	return 1;
}

/* TODO: implement real floatint point format */
static int scanf_getfloat (const char *str, float *n)
{
	*n = (float) atoi (str);

	return 1;
}

static int scanf_gethex (const char *str, int *n)
{
	char *endptr;

	*n = strtol (str, &endptr, 16);

	return 1;
}

static int scanf_getstring (const char *str, unsigned char *n)
{
	unsigned i = strlen (str);
	
	memcpy (n, str, i);
	n[i] = '\0';

	return 1;
}

static int scanf_getchar (const char *str, unsigned char *n)
{
	*n = *str;

	return 1;
}

int vsscanf (const char *str, const char *fmt, va_list args)
{
	unsigned count = 0;
	int *num = 0;
	float *fp = 0;
	unsigned char *buf = 0;
	unsigned i = 0;
	
	unsigned s_len = strlen (str);
	char *s = (char *) malloc (sizeof (char) * s_len);
	
	if (!s) {
		errno = ENOMEM;
		return 0;
	}
	
	for (count = 0; count < s_len; count ++) {
		if (str[count] != ' ')
			s[count] = str[count];
		else
			s[count] = '\0';
	}
	
	count = 0;
	
	for (; *fmt; fmt ++) {
		/* probably argument is here */
		if (*fmt == '%') {
			fmt ++;

			if (!*fmt)
				break;

			if (*fmt == 'd' || *fmt == 'i' || *fmt == 'u') {
				num = va_arg (args, int *);
					
				if (scanf_getint (s, num)) {
					count ++;
					i += strlen (s+i)+1;
				}

				continue;
			}

			if (*fmt == 'x' || *fmt == 'X') {
				num = va_arg (args, int *);

				if (scanf_gethex (s+i, num)){
					count ++;
					i += strlen (s+i)+1;
				}

				continue;
			}
				
			if (*fmt == 'c') {
				buf = va_arg (args, unsigned char *);

				if (scanf_getchar (s+i, buf)) {
					count ++;
					i += strlen (s+i)+1;
				}

				continue;
			}

			if (*fmt == 'f') {
				fp = va_arg (args, float *);
					
				if (scanf_getfloat (s+i, fp)) {
					count ++;
					i += strlen (s+i)+1;
				}

				continue;
			}

			if (*fmt == 's') {
				buf = va_arg (args, unsigned char *);

				if (scanf_getstring (s+i, buf)) {
					count ++;
					i += strlen (s+i)+1;
				}
					
				continue;
			}

			continue;
		}
	}
	
	free (s);

	return count;
}

