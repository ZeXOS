/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <task.h>


unsigned arch_task_init (task_t *task, unsigned entry)
{
	(void) setjmp (task->state);
	// ...especially the stack pointer
	unsigned adr = (unsigned) (task->stacks + USER_STACK_SIZE);
	task->state[0].JMPBUF_SP = adr;
	// ...and program counter
	task->state[0].JMPBUF_IP = entry;
	// set EFLAGS value to enable interrupts
	task->state[0].JMPBUF_FLAGS = 0x200;

	return 1;
}

unsigned arch_task_set (task_t *task)
{
	return setjmp (task->state);
}

unsigned arch_task_switch (task_t *task)
{
	/* fake long jump (register switch) to task function */
	longjmp (task->state, 1);

	return 1;
}
