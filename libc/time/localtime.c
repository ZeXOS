/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <time.h>

#define MINUTE		60
#define HOUR		(60 * MINUTE)
#define DAY		(24 * HOUR)
#define YEAR		(365 * DAY)

struct tm tmbuf;

/* 
	The localtime function converts the simple time pointed to by time to broken-down time representation,
	expressed relative to the user's specified time zone.
	The return value is a pointer to a static broken-down time structure,
	which might be overwritten by subsequent calls to ctime, gmtime, or localtime.
	(But no other library function overwrites the contents of this object.)
*/

struct tm *localtime_r (const time_t *time, struct tm *resultp)
{
	if (!resultp || !time)
		return 0;

	time_t c = *time;

	resultp->tm_year = c/YEAR;

	c -= resultp->tm_year * YEAR;
	resultp->tm_mday = c / DAY;

	c -= resultp->tm_mday * DAY;
	resultp->tm_hour = c / HOUR;

	c -= resultp->tm_hour * HOUR;
	resultp->tm_min = c / MINUTE;

	c -= resultp->tm_min * MINUTE;
	resultp->tm_sec = c;

	resultp->tm_year += EPOCH_YEAR;
	resultp->tm_mday -= 8;	// FIXME: hack

	resultp->tm_wday = resultp->tm_mday;

	while (resultp->tm_wday > 6)
		resultp->tm_wday -= 7;

	resultp->tm_isdst = 0;

	return resultp;
}

/* 	NOTE: There is problem in multi-threaded application, because tmpbuf can be rewrited whenever */
struct tm *localtime (const time_t *time)
{
	return localtime_r (time, &tmbuf);
}
