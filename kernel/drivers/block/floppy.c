/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386
#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <dev.h>
#include <vfs.h>
#include <partition.h>

#define	MAX_DISKCHANGE	16

/* FDC_H */
/* globals */
static volatile bool done = false;
static bool dchange = false;
static bool motor = false;
static int mtick = 0;
static volatile int tmout = 0;
static char status[7] = { 0 };
static char statsz = 0;
static char sr0 = 0;
static char fdc_track = 0xff;
static unsigned fdc_diskchange = 0;
static DrvGeom geometry = { DG144_HEADS, DG144_TRACKS, DG144_SPT };

unsigned char *floppy_tbaddr;    /* physical address of track buffer */

/* prototypes */
void sendbyte (int byte);
int getbyte ();
void floppy_handler (void *r);
bool waitfdc (bool sensei);
bool fdc_rw (int block, char *blockbuff, bool read, unsigned long nosectors);

static char *drive_type[6] = { "no floppy drive",
			"360kb 5.25in floppy drive",
			"1.2mb 5.25in floppy drive",
			"720kb3.5in", "1.44mb 3.5in",
			"2.88mb 3.5in" };

/* helper functions */

unsigned int floppy_detect (void)
{
	unsigned char c;
	int a,b;
	outb (0x70, 0x10);
	c = inb (0x71);
	a = c >> 4;
	b = c & 0xF;

	if (!a)
		return 0;

	kprintf ("floppy drive /dev/fd0: %s\n", drive_type[a]);

	if (b)	// if floppy drive not exist, dont write this
		kprintf ("floppy drive /dev/fd1: %s\n", drive_type[b]);

	return 1;
}


/* This is the IRQ6 handler */
void floppy_handler (void *r)
{
	/* signal operation finished */
	done = true;
	/* EOI the PIC */
	//outb (0x20,0x20);
}

/* init driver */
unsigned int floppy_install (void)
{
	irq_install_handler (6, (void *) floppy_handler);

	return 1;
}

unsigned int init_floppy (void)
{
	int i;

	/* allocate track buffer (must be located below 1M) */
	/* see above for address assignment, floppy  buffer is at floppy_tbaddr) */
	/* install IRQ6 handler */
	
	done = false;
	
	reset ();

	/* get floppy controller version */
	sendbyte (CMD_VERSION);
	i = getbyte ();

	if (i == 0x80)
		kprintf ("NEC765 controller found\n");
	else
		kprintf ("Enhanced controller found\n");

	/* spin up the disk */
	//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|MOTORON");
	//motoron ();

	floppy_tbaddr = (unsigned char *) kmalloc (1024);

	return 1;
}

/* sendbyte() routine from intel manual */
void sendbyte (int byte)
{
	volatile int msr;
	int tmo;

	for (tmo = 0; tmo < 128; tmo ++) {
		msr = inb (FDC_MSR);

		if ((msr & 0xc0) == 0x80) {
			outb (FDC_DATA, byte);
			return;
		}

		inb (0x80);   /* delay */
	}
}

/* getbyte() routine from intel manual */
int getbyte ()
{
	volatile int msr;
	int tmo;

	for (tmo = 0; tmo < 128; tmo ++) {
		msr = inb (FDC_MSR);

		if ((msr & 0xd0) == 0xd0)
			return inb (FDC_DATA);

		inb (0x80);   /* delay */
 	}
	return -1;   /* read timeout */
}

/* this waits for FDC command to complete */
bool waitfdc (bool sensei)
{
	tmout = 80000;
	/* wait for IRQ6 handler to signal command finished */
/*	while (!done && tmout) {
		schedule ();
		tmout --;
		delay (10);
	}*/

	while (!done && tmout) {
		kprintf("");
		schedule ();
	}

	/* read in command result bytes */
	statsz = 0;

	while ((statsz < 7) && (inb(FDC_MSR) & 0x10)) {
		status[statsz ++] = getbyte ();
	}

	if (sensei) {
		/* send a "sense interrupt status" command */
		sendbyte (CMD_SENSEI);
		sr0 = getbyte ();
		fdc_track = getbyte ();
	}

	done = false;
/*
	if (!tmout)
	{
		if (inb(FDC_DIR) & 0x80)
			dchange = true;
		return false;
	}
	else*/
		return true;
}



/* This is the timer (int 1ch) handler */
void int1c (void)
{
	if (tmout)
		-- tmout;     /* bump timeout */

	if (mtick > 0)
		-- mtick;
	else if (!mtick && motor) {
		outb (FDC_DOR, 0x0c);  /* turn off floppy motor */
		motor = false;
	}
}

/*
 * converts linear block address to head/track/sector
 * 
 * blocks are numbered 0..heads*tracks*spt-1
 * blocks 0..spt-1 are serviced by head #0
 * blocks spt..spt*2-1 are serviced by head 1
 * 
 * WARNING: garbage in == garbage out
 */
void block2hts (int block,int *head,int *track,int *sector)
{
	*head = (block % (geometry.spt * geometry.heads)) / (geometry.spt);
	*track = block / (geometry.spt * geometry.heads);
	*sector = block % geometry.spt + 1;
}

/**** disk operations ****/

/* this gets the FDC to a known state */
void reset (void)
{
	/* stop the motor and disable IRQ */
	motoroff ();
	outb (FDC_DOR, 0);
	mtick = 0;
	motor = false;

	/* program data rate (500K/s) */
	outb (FDC_DRS, 0);

	/* re-enable interrupts */
	outb (FDC_DOR, 0x0c);

	/* resetting triggered an interrupt - handle it */
	done = true;
	waitfdc (true);
	done = true;
	waitfdc (true);
	done = true;
	waitfdc (true);
	done = true;
	waitfdc (true);

	/* specify drive timings (got these off the BIOS) */
	sendbyte (CMD_SPECIFY);
	sendbyte (0xdf);  /* SRT = 3ms, HUT = 240ms */
	sendbyte (0x02);  /* HLT = 16ms, ND = 0 */

	/* clear "disk change" status */
	//printf("RESET|SEEK\n");
	seek (5);

	recalibrate ();

	dchange = false;
}

/* this returns whether there was a disk change */
bool diskchange (void)
{
	return dchange;
}

/* this turns the motor on */
void motoron (void)
{
	if (!motor) {
		mtick = -1;     /* stop motor kill countdown */
		outb (FDC_DOR, 0x1c);

		delay (100); /* delay 500ms for motor to spin up */
		motor = true;
	}
}

/* this turns the motor off */
void motoroff (void)
{
	if (motor) {
//		mtick = 13500;   /* start motor kill countdown: 36 ticks ~ 2s */
		
		outb (FDC_DOR, 0x0c);
		outb (0x3f2, 0);
		motor = false;
		delay (200);
	}
}

/* recalibrate the drive */
void recalibrate (void)
{
	/* turn the motor on */
	//motoron();

	/* send actual command bytes */
	sendbyte (CMD_RECAL);
	sendbyte (0);
	/* wait until seek finished */
	waitfdc (true);
	
/* turn the motor off */
	//motoroff();

}

/* seek to track */
bool seek (int track)
{

	if (fdc_track == track)  /* already there? */
		return true;

	//motoron();
	/* send actual command bytes */
	sendbyte (CMD_SEEK);
	sendbyte (0);
	sendbyte (track);

	/* wait until seek finished */
	if (!waitfdc (true)) {
		DPRINT (DBG_DRIVER | DBG_BLOCK, "FLOPPY | SEEK | TIMEOUT");
		return false;     /* timeout! */
	}
	/* now let head settle for 15ms */
	delay (20);
	//motoroff();
	/* check that seek worked */
	if ((sr0 != 0x20) || (fdc_track != track)) {
		if (sr0 != 0x20)
			DPRINT (DBG_DRIVER | DBG_BLOCK, "FLOPPY | SEEK | SR0!=0x20");

		if (fdc_track != track)
			DPRINT (DBG_DRIVER | DBG_BLOCK, "FLOPPY | SEEK | FDC_TRACK!=TRACK");

		return false;
	}
	else
		return true;
}

/* checks drive geometry - call this after any disk change */
bool log_disk (DrvGeom *g)
{
	/* get drive in a known status before we do anything */
	reset();

	/* assume disk is 1.68M and try and read block #21 on first track */
	geometry.heads = DG168_HEADS;
	geometry.tracks = DG168_TRACKS;
	geometry.spt = DG168_SPT;

	if (read_block (20, NULL, 1)) {
		/* disk is a 1.68M disk */
		if (g) {
			g->heads = geometry.heads;
			g->tracks = geometry.tracks;
			g->spt = geometry.spt;
		}

		return true;
	}

	/* OK, not 1.68M - try again for 1.44M reading block #18 on first track */
	geometry.heads = DG144_HEADS;
	geometry.tracks = DG144_TRACKS;
	geometry.spt = DG144_SPT;

	if (read_block (17, NULL, 1)) {
		/* disk is a 1.44M disk */
		if (g) {
			g->heads = geometry.heads;
			g->tracks = geometry.tracks;
			g->spt = geometry.spt;
		}

		return true;
	}

	/* it's not 1.44M or 1.68M - we don't support it */
	return false;
}

/* read block (blockbuff is 512 byte buffer) */
bool read_block (int block, char *blockbuff, unsigned long nosectors)
{
	int track = 0, sector = 0, head = 0, track2 = 0, result = 0, loop = 0;

	reset ();

	//The FDC can read multiple sides at once but not multiple tracks
	//printf("READ 1\n");
	block2hts (block, &head, &track, &sector);
	block2hts (block+nosectors, &head, &track2, &sector);

	if(track != track2) {
		for(loop = 0; (unsigned) loop < nosectors; loop ++)
			result = fdc_rw (block+loop, blockbuff + (loop*512), true, 1);

		return result;
	}
// 	printf("FDC_RW\n");
	return fdc_rw (block, blockbuff, true, nosectors);
}

/* write block (blockbuff is 512 byte buffer) */
bool write_block (int block, char *blockbuff, unsigned long nosectors)
{
	return fdc_rw (block, blockbuff, false, nosectors);
}

/*
 * since reads and writes differ only by a few lines, this handles both.  This
 * function is called by read_block() and write_block()
 */
bool fdc_rw (int block, char *blockbuff, bool read, unsigned long nosectors)
{
	int head,track,sector,tries, copycount = 0;
	unsigned char *p_tbaddr = (unsigned char *) floppy_tbaddr;
	unsigned char *p_blockbuff = blockbuff;

	/* convert logical address into physical address */
	block2hts (block, &head, &track, &sector);

	/* spin up the disk */
	//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|MOTORON");
	motoron();

	if (!read && blockbuff) {
		/* copy data from data buffer into track buffer */
		for (copycount = 0; (unsigned) copycount < (nosectors*512); copycount ++) {
			*p_tbaddr = *p_blockbuff;
			p_blockbuff++;
			p_tbaddr++;
		}
	}

	//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|COPY1");

	for (tries = 0; tries < 3; tries ++) {

		//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|RIGHT");
		/* move head to right track */
		if (!seek (track)) {
			motoroff ();
			DPRINT (DBG_DRIVER | DBG_BLOCK, "FLOPPY | CANNOT SEEK\n");
			return false;
		}

		/* check for diskchange */
		//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|DISKCHANGE");

		if (inb (FDC_DIR) & 0x80) {
			dchange = true;
			seek (1);  /* clear "disk change" status */
			recalibrate ();
			motoroff ();
			/*fdc_diskchange ++;
			DPRINT (DBG_DRIVER | DBG_BLOCK, "FLOPPY | DISK CHANGE STATUS => repeat");
			if (fdc_diskchange < MAX_DISKCHANGE)*/
			return fdc_rw (block, blockbuff, read, nosectors);
			
			/*printf ("Floppy disk not response, resetting drive..\n");
			reset ();
			motoroff();
			return false;*/
		}
		//fdc_diskchange = 0;

		//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|RATE");

		/* program data rate (500K/s) */
		outb (FDC_CCR, 0);

		/* send command */
		if (read) {
			//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|DMA");
			dma_xfer (2, (unsigned long) floppy_tbaddr, nosectors * 512, false);
			sendbyte (CMD_READ);
		} else {
			dma_xfer (2, (unsigned long) floppy_tbaddr, nosectors * 512, true);
			sendbyte (CMD_WRITE);
		}

		sendbyte (head << 2);
		sendbyte (track);
		sendbyte (head);
		sendbyte (sector);
		sendbyte (2);               /* 512 bytes/sector */
		sendbyte (geometry.spt);

		if (geometry.spt == DG144_SPT)
			sendbyte (DG144_GAP3RW);  /* gap 3 size for 1.44M read/write */
		else
			sendbyte (DG168_GAP3RW);  /* gap 3 size for 1.68M read/write */

		sendbyte (0xff);            /* DTL = unused */

		//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|STEP1");
		/* wait for command completion */
		/* read/write don't need "sense interrupt status" */
		if (!waitfdc(1)) {
			kprintf ("Timed out, trying operation again after reset()\n");
			reset ();
			return fdc_rw (block, blockbuff, read, nosectors);
        	}

		//DPRINT (DBG_DRIVER | DBG_BLOCK, "FDC_RW|%d",(status[0] & 0xc0));

		if ((status[0] & 0xc0) == 0)
			break;   /* worked! outta here! */

		if (tries)
			DPRINT (DBG_DRIVER | DBG_BLOCK, "Try %d error => read from floppy\n", tries);

		recalibrate ();  /* oops, try again... */
	}


	/* stop the motor */
	//printf("FDC_RW|MOTOROFF\n");
	motoroff ();

	if (read && blockbuff) {
		/* copy data from track buffer into data buffer */
		//printf("FDC_RW|OK\n");
		p_blockbuff = blockbuff;
		p_tbaddr = (unsigned char *) floppy_tbaddr;

		for (copycount = 0; (unsigned) copycount < (nosectors * 512); copycount ++) {
			*p_blockbuff = *p_tbaddr;
			p_blockbuff ++;
			p_tbaddr ++;
		}
	} else
		DPRINT (DBG_DRIVER | DBG_BLOCK, "FLOPPY | ERROR\n");

	return (tries != 3);
}

/* this formats a track, given a certain geometry */
bool format_track (char track, DrvGeom *g)
{
	int i, h, r, r_id, split;
	char tmpbuff[256];
	unsigned char *p_tbaddr = (unsigned char *) floppy_tbaddr;
	unsigned int copycount = 0;

	/* check geometry */
	if (g->spt != DG144_SPT && g->spt != DG168_SPT)
		return false;

	/* spin up the disk */
	motoron ();

	/* program data rate (500K/s) */
	outb (FDC_CCR, 0);

	seek (track);  /* seek to track */

	/* precalc some constants for interleave calculation */
	split = g->spt / 2;

	if (g->spt & 1)
		split ++;

	for (h = 0; h < g->heads; h ++) {
		/* for each head... */
		/* check for diskchange */
		if (inb (FDC_DIR) & 0x80) {
			dchange = true;
			seek (1);  /* clear "disk change" status */
			recalibrate ();
			motoroff ();

			return false;
		}

		i = 0;   /* reset buffer index */
		for (r = 0; r < g->spt; r++) {
			/* for each sector... */

			/* calculate 1:2 interleave (seems optimal in my system) */
			r_id = r / 2 + 1;

			if (r & 1)
				r_id += split;
	 
			/* add some head skew (2 sectors should be enough) */
			if (h & 1) {
				r_id -= 2;

				if (r_id < 1)
					r_id += g->spt;
			}
			/* add some track skew (1/2 a revolution) */
			if (track & 1) {
				r_id -= g->spt / 2;

				if (r_id < 1)
					r_id += g->spt;
			}

			/**** interleave now calculated - sector ID is stored in r_id ****/

			/* fill in sector ID's */
			tmpbuff[i ++] = track;
			tmpbuff[i ++] = h;
			tmpbuff[i ++] = r_id;
			tmpbuff[i ++] = 2;
		}

		/* copy sector ID's to track buffer */
		for (copycount = 0; copycount < (unsigned) i; copycount ++) {
			*p_tbaddr = tmpbuff[copycount];
			p_tbaddr ++;
		}
		//movedata(_my_ds(),(long)tmpbuff,_dos_ds,tbaddr,i);

		/* start dma xfer */
		dma_xfer (2, (unsigned long) floppy_tbaddr, i, true);

		/* prepare "format track" command */
		sendbyte (CMD_FORMAT);
		sendbyte (h << 2);
		sendbyte (2);
		sendbyte (g->spt);

		if (g->spt == DG144_SPT)
			sendbyte (DG144_GAP3FMT);    /* gap3 size for 1.44M format */
		else
			sendbyte (DG168_GAP3FMT);    /* gap3 size for 1.68M format */

		sendbyte (0);     /* filler byte */

		/* wait for command to finish */
		if (!waitfdc (false))
			return false;

		if (status[0] & 0xc0) {
			motoroff ();
			return false;
		}
	}

	motoroff ();

	return true;
}



bool floppy_acthandler (unsigned act, partition_t *p, char *block, char *more, int n)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			if (!floppy_detect ())
				return 0;

			int_disable ();
			floppy_install ();
			int_enable ();
			init_floppy ();

			return 1;
		}
		break;
		case DEV_ACT_READ:
		{
			read_block (n, block, 1);
			return 1;
		}
		break;
		case DEV_ACT_MOUNT:
		{
			int f = 0, t = 0;
			if (!read_dir (-1))
				return 0;

			if (strlen (more)) {
				if (!strcmp (more, "."))
					return read_dir (0);

				if (!strcmp (more, ".."))
					return read_dir (1);

				while (strlen (dir[f].name)) {
					if (!strncmp (dir[f].name, more, strlen (more))) {
						if (!read_dir (f))
							return 0;

						t = 1;
						break;
					}

					f ++;
				}

				if (!t)
					return 0;
			}

			f = 0;
			unsigned fl = strlen (dir[f].name);

			 while (fl) {
				unsigned attrib = VFS_FILEATTR_MOUNTED;

/*
				#define VFS_FILEATTR_FILE	0x1
				#define VFS_FILEATTR_DIR	0x2
				#define VFS_FILEATTR_HIDDEN	0x4
				#define VFS_FILEATTR_SYSTEM	0x8
				#define VFS_FILEATTR_BIN	0x10
				#define VFS_FILEATTR_READ	0x20
				#define VFS_FILEATTR_WRITE	0x40

				case 0: dir[y].read=1; break;
				case 1: dir[y].hidden=1; break;
				case 2: dir[y].system=1; break;
				case 3: dir[y].volume=1; break;
				case 4: dir[y].dir=1; break;
				case 5: dir[y].bin=1; break;
*/

				if (dir[f].read) {
					attrib |= VFS_FILEATTR_READ;
					attrib |= VFS_FILEATTR_WRITE;
				}
				if (dir[f].hidden)
					attrib |= VFS_FILEATTR_HIDDEN;
				if (dir[f].system)
					attrib |= VFS_FILEATTR_SYSTEM;
				if (dir[f].dir)
					attrib |= VFS_FILEATTR_DIR;
				else
					attrib |= VFS_FILEATTR_FILE;
				if (dir[f].bin)
					attrib |= VFS_FILEATTR_BIN;

				dir[f].name[10] = '\0';
				vfs_list_add (dir[f].name, attrib, block);
				//printf ("add: %s to %s\n", dir[f].name, block);

				f ++;
				fl = strlen (dir[f].name);
			}

			return 1;
		}
		break;
	}

	return 0;
}
#endif
