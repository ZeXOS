/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _SPINLOCK_H
#define _SPINLOCK_H

typedef struct spinlock {
	volatile unsigned locked;
	volatile int int_state;
} spinlock_t;

#define SPINLOCK_CREATE(name) spinlock_t name = { .locked = 0 }

/* externs */
extern unsigned spinlock_lock (spinlock_t *spinlock);
extern unsigned spinlock_init (spinlock_t *spinlock);

#endif
