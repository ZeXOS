/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

char *fgets (char *s, int size, FILE *stream)
{
	if (!stream || !s || size < 2)
		goto err;

	if (stream->mode & O_WRONLY)
		goto err;

	int i;
	for (i = 0; i < (size-1); ) {
		int r = fgetc (stream);

		/* EOF or something else ? */
		if (stream->error)
			break;

		s[i ++] = r;

		if (r == '\n')
			break;
	}

	s[i] = '\0';

	if (!i)
		goto err;
		
	stream->off += i;

	return s;
err:
	errno_update ();

	return 0;
}
