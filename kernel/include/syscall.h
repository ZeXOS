/*
 *  ZeX/OS
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _SYSCALL_H
#define _SYSCALL_H

#define	SYSV_GETKEY		(unsigned *) 0x9004
#define	SYSV_FORK		(unsigned *) 0x9008
#define	SYSV_WRITE		(int *) 0x900C
#define	SYSV_SOCKET		(int *) 0x9010
#define	SYSV_CONNECT		(int *) 0x9014
#define	SYSV_SEND		(int *) 0x9018
#define	SYSV_RECV		(int *) 0x901C
#define	SYSV_OPEN		(int *) 0x9020
#define	SYSV_READ		(int *) 0x9024
#define	SYSV_TIME		(unsigned *) 0x9028
#define	SYSV_CHDIR		(int *) 0x902C
#define	SYSV_BIND		(int *) 0x9030
#define	SYSV_LISTEN		(int *) 0x9034
#define	SYSV_ACCEPT		(int *) 0x9038
#define	SYSV_FCNTL		(int *) 0x903C
#define	SYSV_GVGAFB		(unsigned *) 0x9040
#define	SYSV_RS232READ		(int *) 0x9044
#define	SYSV_SENDTO		(int *) 0x9048
#define	SYSV_RECVFROM		(int *) 0x904C
#define	SYSV_GETCHAR		(int *) 0x9050
#define	SYSV_THREADOPEN		(unsigned *) 0x9054
#define	SYSV_THREADCLOSE	(unsigned *) 0x9058
#define	SYSV_IOCTL		(int *) 0x905C
#define	SYSV_LSEEK		(long *) 0x9060
#define	SYSV_GETCH		(unsigned *) 0x9064
#define	SYSV_SELECT		(int *) 0x9068

/* externs */
extern task_t *_curr_task;
extern unsigned long timer_ticks;
extern unsigned char scancode;
extern kbd_quaue kbd_q;
extern unsigned char vgagui;
extern tty_t *gtty;

#endif
