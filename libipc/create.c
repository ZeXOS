/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <ipc.h>
#include <string.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/socket.h>

int ipc_create (const char *queue, unsigned flags)
{
        int fd;
	int l;

        struct sockaddr_un local;

        if ((fd = socket (AF_UNIX, SOCK_STREAM, flags)) == -1)
		return -1;

        local.sun_family = AF_UNIX;
        strcpy (local.sun_path, queue);

        unlink (local.sun_path);

        l = strlen (local.sun_path) + sizeof (local.sun_family);

        if (bind (fd, (struct sockaddr *) &local, l) == -1)
		return -2;

        if (listen (fd, 10) == -1)
		return -3;

	return fd;
}
