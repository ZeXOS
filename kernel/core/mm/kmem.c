/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <build.h>
#include <config.h>
#include <mutex.h>

#ifndef CONFIG_MEM_ZALLOC

#define	_32BIT	1

/* use small (32K) heap for 16-bit compilers,
large (500K) heap for 32-bit compilers */
#if defined(_32BIT)
#define	HEAP_SIZE	0x100000
#else
#define	HEAP_SIZE	8192u
#endif

#define	MALLOC_MAGIC	0x6D92	/* must be < 0x8000 */

typedef struct _malloc		/* Turbo C	DJGPP/GCC */
{
	size_t size;		/* 2 bytes	 4 bytes */
	struct _malloc *next;	/* 2 bytes	 4 bytes */
	unsigned magic : 15;	/* 2 bytes total 4 bytes total */
	unsigned used : 1;
} malloc_t;		/* total   6 bytes	12 bytes */

static char *g_heap_bot, *g_kbrk, *g_heap_top;

MUTEX_CREATE (mutex_malloc);
MUTEX_CREATE (mutex_free);

/*****************************************************************************
*****************************************************************************/
static void kdump_heap (void)
{
	unsigned blks_used = 0, blks_free = 0;
	size_t bytes_used = 0, bytes_free = 0;
	malloc_t *m;
	int total;

	for (m = (malloc_t *)g_heap_bot; m != NULL; m = m->next)	{

		DPRINT (DBG_MM | DBG_KMEM, "blk %5p: %6u bytes %s", m,
			m->size, m->used ? "used" : "free");

		if (m->used) {
			blks_used ++;
			bytes_used += m->size;
		} else {
			blks_free ++;
			bytes_free += m->size;
		}
	}

	DPRINT (DBG_MM | DBG_KMEM, "blks:  %6u used, %6u free, %6u total", blks_used,
		blks_free, blks_used + blks_free);
	DPRINT (DBG_MM | DBG_KMEM, "bytes: %6u used, %6u free, %6u total", bytes_used,
		bytes_free, bytes_used + bytes_free);
	DPRINT (DBG_MM | DBG_KMEM, "g_heap_bot=0x%p, g_kbrk=0x%p, g_heap_top=0x%p",
		g_heap_bot, g_kbrk, g_heap_top);

	total = (bytes_used + bytes_free) +
			(blks_used + blks_free) * sizeof(malloc_t);

	if(total != g_kbrk - g_heap_bot)
		DPRINT (DBG_MM | DBG_KMEM, "*** some heap memory is not accounted for");
}

/*****************************************************************************
POSIX sbrk() looks like this
	void *sbrk(int incr);
Mine is a bit different so I can signal the calling function
if more memory than desired was allocated (e.g. in a system with paging)
If your kbrk()/sbrk() always allocates the amount of memory you ask for,
this code can be easily changed.

			int brk(	void *sbrk(		void *kbrk(
function		 void *adr);	 int delta);		 int *delta);
----------------------	------------	------------		-------------
POSIX?			yes		yes			NO
return value if error	-1		-1			NULL
get break value		.		sbrk(0)			int x=0; kbrk(&x);
set break value to X	brk(X)		sbrk(X - sbrk(0))	int x=X, y=0; kbrk(&x) - kbrk(&y);
enlarge heap by N bytes	.		sbrk(+N)		int x=N; kbrk(&x);
shrink heap by N bytes	.		sbrk(-N)		int x=-N; kbrk(&x);
can you tell if you're
  given more memory
  than you wanted?	no		no			yes
*****************************************************************************/
static void *kbrk (int *delta)
{
	static char heap[HEAP_SIZE];

	char *new_brk, *old_brk;

	/* heap doesn't exist yet */
	if(g_heap_bot == NULL) {
		g_heap_bot = g_kbrk = heap;
		g_heap_top = g_heap_bot + HEAP_SIZE;
	}

	new_brk = g_kbrk + (*delta);

	/* too low: return NULL */
	if(new_brk < g_heap_bot)
		return NULL;

	/* too high: return NULL */
	if(new_brk >= g_heap_top)
		return NULL;

	/* success: adjust brk value... */
	old_brk = g_kbrk;
	g_kbrk = new_brk;

	/* ...return actual delta... (for this sbrk(), they are the same)
	(*delta) = (*delta); */
	/* ...return old brk value */
	return old_brk;
}

/*****************************************************************************
kmalloc() and kfree() use g_heap_bot, but not g_kbrk nor g_heap_top
*****************************************************************************/
void *kmalloc (size_t size)
{
	//kprintf ("kmalloc (): %d\n", size);

	unsigned total_size;
	malloc_t *m, *n;
	int delta;

	if (size == 0)
		return NULL;

	mutex_lock (&mutex_malloc);

	total_size = size + sizeof (malloc_t);

	/* search heap for free block (FIRST FIT) */
	m = (malloc_t *) g_heap_bot;

	/* g_heap_bot == 0 == NULL if heap does not yet exist */
	if (m != NULL) {
		if (m->magic != MALLOC_MAGIC) {
			kprintf ("*** kernel heap is corrupt in kmalloc()\n");
			mutex_lock (&mutex_malloc);
			return NULL;
		}

		for (; m->next != NULL; m = m->next) {
			if (m->used)
				continue;

			/* prevent before page fault :-B */
			if (m == (malloc_t *) ~0)
				continue;

			/* size == m->size is a perfect fit */
			if (size == m->size)
				m->used = 1;
			else {
				/* otherwise, we need an extra sizeof(malloc_t) bytes for the header
				of a second, free block */
				if (total_size > m->size)
					continue;

				/* create a new, smaller free block after this one */
				n = (malloc_t *) ((char *) m + total_size);
				n->size = m->size - total_size;
				n->next = m->next;
				n->magic = MALLOC_MAGIC;
				n->used = 0;

				/* reduce the size of this block and mark it used */
				m->size = size;
				m->next = n;
				m->used = 1;
			}

			mutex_unlock (&mutex_malloc);

			return (char *) m + sizeof (malloc_t);
		}
	}

	/* use kbrk() to enlarge (or create!) heap */
	delta = total_size;
	n = kbrk (&delta);

	/* uh-oh */
	if (n == NULL) {
		DPRINT (DBG_MM | DBG_KMEM, "kbrk () == NULL");
		mutex_unlock (&mutex_malloc);
		return NULL;
	}

	if (m != NULL)
		m->next = n;

	n->size = size;
	n->magic = MALLOC_MAGIC;
	n->used = 1;

	/* did kbrk() return the exact amount of memory we wanted?
	cast to make "gcc -Wall -W ..." shut the hell up */
	if ((int) total_size == delta)
		n->next = NULL;
	else {
		/* it returned more than we wanted (it will never return less):
		create a new, free block */
		m = (malloc_t *)((char *)n + total_size);
		m->size = delta - total_size - sizeof(malloc_t);
		m->next = NULL;
		m->magic = MALLOC_MAGIC;
		m->used = 0;

		n->next = m;
	}

	mutex_unlock (&mutex_malloc);

	return (char *) n + sizeof (malloc_t);
}
/*****************************************************************************
*****************************************************************************/
void kfree (void *blk)
{
	if (!blk)
		return;

	mutex_lock (&mutex_free);

	malloc_t *m, *n;

	/* get address of header */
	m = (malloc_t *)((char *) blk - sizeof(malloc_t));

	if (m->magic != MALLOC_MAGIC) {
		kprintf ("*** attempt to kfree() block at 0x%p "
			"with bad magic value\n", blk);
		goto cleanup;
	}

	/* find this block in the heap */
	n = (malloc_t *)g_heap_bot;

	if (n->magic != MALLOC_MAGIC) {
		kprintf ("*** kernel heap is corrupt in kfree()\n");
		goto cleanup;
	}

	for (; n != NULL; n = n->next) {
		if(n == m)
			break;
	}

	/* not found? bad pointer or no heap or something else? */
	if (n == NULL) {
		kprintf ("*** attempt to kfree() block at 0x%p "
			"that is not in the heap\n", blk);

		mutex_unlock (&mutex_free);
		goto cleanup;
	}

	/* free the block */
	m->used = 0;

	/* coalesce adjacent free blocks Hard to spell, hard to do */
	for (m = (malloc_t *)g_heap_bot; m != NULL; m = m->next) {
		while (!m->used && m->next != NULL && !m->next->used) {
			/* resize this block */
			m->size += sizeof(malloc_t) + m->next->size;
			/* merge with next block */
			m->next = m->next->next;
		}
	}

cleanup:
	mutex_unlock (&mutex_free);
}
/*****************************************************************************
*****************************************************************************/
void *krealloc (void *blk, size_t size)
{
	void *new_blk;
	malloc_t *m;

	/* size == 0: free block */
	if (size == 0) {
		if(blk != NULL)
			kfree (blk);

		new_blk = NULL;
	} else {
		/* allocate new block */
		new_blk = kmalloc (size);

		/* if allocation OK, and if old block exists, copy old block to new */
		if (new_blk != NULL && blk != NULL) {
			m = (malloc_t *) ((char *) blk - sizeof (malloc_t));

			if (m->magic != MALLOC_MAGIC) {
				kprintf ("*** attempt to krealloc() block at 0x%p with bad magic value\n", blk);
				return NULL;
			}

			/* copy minimum of old and new block sizes */
			if (size > m->size)
				size = m->size;

			memcpy (new_blk, blk, size);

			/* free the old block */
			kfree (blk);
		}
	}
	return new_blk;
}


/*****************************************************************************
*****************************************************************************/
unsigned short kmem_get ()
{
#ifdef ARCH_i386
        register unsigned long *mem;
        unsigned long mem_count, a, mem_end, bse_end;
        unsigned short memkb;
        unsigned char irq1, irq2;
        unsigned long cr0;

        /* save IRQ's */
        irq1 = inb (0x21);
        irq2 = inb (0xA1);

        /* kill all irq's */
        outb (0x21, 0xFF);
        outb (0xA1, 0xFF);

        mem_count = 0;
        memkb = 0;

        // store a copy of CR0
        asm volatile ("movl %%cr0, %%eax;" : "=a"(cr0));

        // invalidate the cache
        // write-back and invalidate the cache
        asm volatile ("wbinvd;");

        // plug cr0 with just PE/CD/NW
        // cache disable(486+), no-writeback(486+), 32bit mode(386+)
        asm volatile ("movl %%eax, %%cr0;" ::
                             "a" (cr0 | 0x00000001 | 0x40000000 | 0x20000000));

        do
        {
                memkb ++;
                mem_count += 1024*1024;
                mem = (unsigned long *) mem_count;

                a = *mem;

                *mem = 0x55AA55AA;

                /* the empty asm calls tell gcc not to rely on whats in its registers
                   as saved variables (this gets us around GCC optimisations) */
                asm (";" ::: "memory");
                if (*mem != 0x55AA55AA)
                        mem_count = 0;
                else
                {
                        *mem = 0xAA55AA55;
                        asm ("":::"memory");
                        if (*mem!=0xAA55AA55)
                                mem_count = 0;
                }

                asm (";":::"memory");
                *mem = a;
        } while (memkb < 4096 && mem_count != 0);

        asm volatile ("movl %%eax, %%cr0;" :: "a" (cr0));

        mem_end = memkb << 20;
        mem = (unsigned long *) 0x413;
        bse_end = ((*mem) &0xFFFF) << 6;

        outb (0x21, irq1);
        outb (0xA1, irq2);

	return memkb;
#endif
	return 0;
}

void *sbrk (int *delta)
{
	static char heap[HEAP_SIZE];

	char *new_brk, *old_brk;

	/* heap doesn't exist yet */
	if(g_heap_bot == NULL) {
		g_heap_bot = g_kbrk = heap;
		g_heap_top = g_heap_bot + HEAP_SIZE;
	}

	new_brk = g_kbrk + (*delta);
	/* too low: return NULL */
	if(new_brk < g_heap_bot)
		return NULL;

	/* too high: return NULL */
	if(new_brk >= g_heap_top)
		return NULL;

	/* success: adjust brk value... */
	old_brk = g_kbrk;
	g_kbrk = new_brk;

	/* .. return actual delta... (for this sbrk(), they are the same)
	   .. return old brk value */
	return old_brk;
}

/*****************************************************************************
*****************************************************************************/
void util_cmdfree ()
{
		unsigned blks_used = 0, blks_free = 0;
		size_t bytes_used = 0, bytes_free = 0;
		malloc_t *m;
		int total;
	
		for (m = (malloc_t *)g_heap_bot; m != NULL; m = m->next) {
			/*DPRINT (DBG_MM | DBG_KMEM, "blk %5p: %6u bytes %s\n", m,
				m->size, m->used ? "used" : "free");*/
	
			if (m->used) {
				blks_used++;
				bytes_used += m->size;
			} else {
				blks_free++;
				bytes_free += m->size;
			}
		}
	
		DPRINT (DBG_MM | DBG_KMEM, "blks:  %6u used, %6u free, %6u total", blks_used,
			blks_free, blks_used + blks_free);
		DPRINT (DBG_MM | DBG_KMEM, "bytes: %6u used, %6u free, %6u total", bytes_used,
			bytes_free, bytes_used + bytes_free);
		DPRINT (DBG_MM | DBG_KMEM, "g_heap_bot=0x%p, g_kbrk=0x%p, g_heap_top=0x%p",
			g_heap_bot, g_kbrk, g_heap_top);
	
		/* we have to increase used memory from user-space apps */
		bytes_used += umem_used () / 1024;

		printf ("\ttotal\tused\tfree\tshared\tbuffers\tcached\n");
		printf ("Mem: \t%u\t%u\t%u\t%u\t%u\t%u\n", 
			mem_ext*1024, bytes_used/1024, mem_ext*1024 - bytes_used/1024, 0, 0, blks_free);
}

/*****************************************************************************
*****************************************************************************/
unsigned int init_mm ()
{
	//mem_ext = kmem_get ();	// get extended memory (in Mb)

	kdump_heap ();

	pmem_init ();
	swmem_init ();
	umem_init ();

	//file_cache = (char *) kmalloc (sizeof (char));

	return 1;
}

#endif
