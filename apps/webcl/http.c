/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>

extern int sock;

char num[20];

unsigned char color_font;
unsigned char color_bg;

void color_apply ()
{
#ifndef LINUX
	setcolor (color_font, color_bg);
#endif
}

int http_proto (char *data, int len)
{
	/*data[len] = '\0';
	puts ("PROTO: '");
	puts (data);
	puts ("' - ");

	itoa (len, num, 10);
	puts (num);
	puts ("\n");*/

	if (!strncmp (data, "html", len)) {
		printf ("<WEB PAGE>\n");
		return 1;
	}

	if (!strncmp (data, "/html", len)) {
		printf ("\n<END OF WEB>\n");
		return 1;
	}

	if (!strncmp (data, "br", len)) {
		putchar ('\n');
		return 1;
	}

	if (!strncmp (data, "h1", len)) {
		color_bg = 2;
		color_apply ();
		return 1;
	}

	if (!strncmp (data, "/h1", len)) {
		color_bg = 8;
		color_apply ();
		return 1;
	}

	if (!strncmp (data, "p", len)) {
		putchar ('|');
		return 1;
	}

	if (!strncmp (data, "/p", len)) {
		putchar ('|');
		return 1;
	}

	if (!strncmp (data, "hr", len)) {
		putchar ('\n');

		unsigned i = 0;
		while (i < 80) {
			putchar (205);
			i ++;
		}

		putchar ('\n');
		return 1;
	}

	if (!strncmp (data, "a href", 6)) {
		color_font = 8;
		color_apply ();
		return 1;
	}

	if (!strncmp (data, "/a", len)) {
		color_font = 15;
		color_apply ();
		return 1;
	}

	return 0;
}

int http_handler (char *data, int len)
{
	int i = 0;
	unsigned tag_s = 0;
	unsigned tag_e = 0;
	unsigned l = 0;


	while (i < len) {
		/* start of html tag */
		if (data[i] == '<')
			tag_s = i;

		if (data[i] == '>') {
			tag_e = i;
			l = tag_s;
		}

		if (!(tag_s && tag_e)) {
			if (l > tag_s)
				putchar (data[i]);
		}

		if (tag_e > tag_s) {
			if (tag_s && tag_e) {
				http_proto (data+tag_s+1, tag_e-tag_s-1);
				tag_s = 0;
				tag_e = 0;
			}
		}

		i ++;
	}

	return 0;
}

int http_request (char *address, unsigned address_len, char *dir, unsigned dir_len)
{
/*GET / HTTP/1.1
Host: 127.0.0.1
Accept: text/xml,text/html;text/plain
Accept-Charset: ISO-8859-2,utf-8
Keep-Alive: 300
Connection: keep-alive*/

	char *buf = (char *) malloc (sizeof (char) * 200240);          		// Read buffer
	//char buf[10240];

	memcpy (buf, "GET ", 4);
	memcpy (buf+4, dir, dir_len);
	memcpy (buf+4+dir_len, " HTTP/1.1\r\nHost: www.", 21);
	memcpy (buf+25+dir_len, address, address_len);
	memcpy (buf+25+dir_len+address_len, "\r\nAccept: */*\r\nAccept-Charset: ISO-8859-2,utf-8\r\nKeep-Alive: 300\r\nConnection: Keep-Alive\r\n\r\n", 92);

	unsigned len = 117+dir_len+address_len;
	buf[len] = '\0';

	int ret = send (sock, buf, len, 0);
	int buf_len = 0;

	/* receive header */
	while (1) {
		ret = recv (sock, buf+buf_len, 1024, 0);
printf ("ret: %d : %d\n", ret, buf_len);
		if (ret < 1)
			break;
		else
			buf_len += ret;

		if (buf_len >= 200240) {
			printf ("WEB page is too big\n");
			goto disconnect;
		}
	}

	len = 0;

	if (buf_len > 0) {
		buf[buf_len] = '\0';

		char *r = strstr (buf, "Content-Length: ");

		if (r) {
			unsigned x = 0;
			while (x < 7) {
				if (r[x+16] == '\n')
					break;

				x ++;
			}

			char num[9];
			memcpy (num, r+16, x);
			num[x-1] = '\0';

			len = atoi (num);

			ret = 1;

			if (!len) {
				puts ("ERROR 404 - Page not found\n");
				ret = 0;
				goto disconnect;
			}
		} else {
			int p;
			for (p = 0; p < buf_len; p ++) {
				if (buf[p] == '\n')
				if (buf[p+1] == '\r')
				/*if (buf[p+2] == '\n')
				if (buf[p+3] == '\r') */{
					len = buf_len - p;
					break;
				}
			}
			printf ("len: %d\n", len);
		}
	}


	printf ("WEB PAGE is loaded\n");

	color_font = 15;
	color_bg = 0;

	http_handler (buf+(buf_len-len), len);

disconnect:
	free (buf);
	close (sock);

	return ret;
}
