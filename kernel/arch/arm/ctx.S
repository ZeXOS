/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Thanks for lk project maintained by Geist for usefully parts of code
 */

/* Just make it simplier - EXP create new function accesible from C */
#define EXP(f) .global f; f:

/* arm specific function for enable interrupts */
EXP(arm_int_enable)
	mrs	r0, cpsr
	bic	r0, r0, #(1<<7)
	msr	cpsr_c, r0
	bx	lr

/* arm specific function for disable interrupts */
EXP(arm_int_disable)
	mrs	r0, cpsr
	orr	r0, r0, #(1<<7)
	msr	cpsr_c, r0
	bx	lr

/* context frame is same like struct arm_ctx_frame, but reversed
 * register sequence: ulr, usp, lr, r11, r10, r9, r8, r7, r6, r5, r4
 */

/* arm_context_switch (unsigned *old_sp, unsigned new_sp) */
EXP(arm_task_switch)
	/* here we should first save all registers */
	sub	r3, sp, #(11*4)		/* remember, we can't use stack pointer in user mode */
	mov	r12, lr
	stmia	r3, { r4-r11, r12, r13, r14 }^
	
	/* old stack pointer will saved */
	str	r3, [r0] 

	/* arm version dependend stuff */
#if ARM_VER_ARMV7
	/* nice magic word */
	.word	0xf57ff01f // clrex
#elif ARM_VER_ARMV6
	/* I dont recommend it .. */
	ldr	r0, =strex_spot
	strex	r3, r2, [r0]
#endif

	/* We have to load new registers into stack */
	ldmia	r1, { r4-r11, r12, r13, r14 }^
	mov	lr, r12			/* restore lr from task */
	add	sp, r1, #(11*4)    	/* restore sp from task */
	bx	lr

.ltorg

.data
strex_spot:
	.word	0
	

