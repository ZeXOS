/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "proto.h"
#include "user.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static char *uibuffer;
static unsigned len;

int getstring (char *str)
{
	char c = getchar ();

	if (c != -1) {
		if (c == '\n') {
			unsigned l = len;
			len = 0;

			return l;
		}
		if (c == '\b') {
			if (len > 0)
				len --;

			return 0;
		}

		str[len] = c;
		len ++;
	}

	return 0;
}

int ui_getcommand ()
{
	int len = getstring (uibuffer);
	
	if (len > 0) {
		uibuffer[len] = '\0';

		return user_msgparse (uibuffer, len);
	}

	return 0;
}

/** INIT USER INTERFACE function */
int init_ui ()
{
	uibuffer = (char *) malloc (sizeof (char) * 512);

	if (!uibuffer)
		return 0;

	/* set socket "sock" to non-blocking */
	int oldFlag = fcntl (1, F_GETFL, 0);
	if (fcntl (1, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	len = 0;

	return 1;
}
