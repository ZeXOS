/*
 *  ZeX/OS
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>

int fd;
fd_set myset;
struct sockaddr_in sockname;

/* Client structure */
typedef struct client_context {
	struct client_context *next, *prev;
	
	int fd;
} client_t;

client_t client_list;

int mserver_bind (int port)
{
	if ((fd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("> cant create socket\n");
		return -1;
	}

	sockname.sin_family = AF_INET;
	sockname.sin_port = htons (port);

	// Create listen socket
	if (bind (fd, (struct sockaddr *) &sockname, sizeof (sockname)) == -1) {
		printf ("bind () - port is used\n");
		return -1;
	}

	// Create queue for accept
	if (listen (fd, 10) == -1) {
		printf ("ERROR -> listen () == -1\n");
		return -1;
	}
	
	client_list.next = &client_list;
	client_list.prev = &client_list;
	
	return 0;
}

int mserver_newclient ()
{
	int client;
	struct sockaddr_in clientInfo;
	socklen_t addrlen = sizeof (clientInfo);

	if (FD_ISSET (fd, &myset)) {
		printf ("> new client connected\n");

		client = accept (fd, (struct sockaddr *) &clientInfo, &addrlen);

		if (client > 0) {
			// alloc and init context
			client_t *ctx = (client_t *) malloc (sizeof (client_t));

			if (!ctx)
				return 0;

			ctx->fd = client;

			ctx->next = &client_list;
			ctx->prev = client_list.prev;
			ctx->prev->next = ctx;
			ctx->next->prev = ctx;
			
			return 1;
		}
	}
	
	return 0;
}

int mserver_sendtoall (client_t *me, char *buf, unsigned len)
{
	if (!me)
		return -1;
  
	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next)
		if (c != me)
			send (c->fd, buf, len, 0);
		
	return 0;
}

int mserver_handle (client_t *c)
{
	if (!FD_ISSET (c->fd, &myset))
		return 0;

	char str[80];
	int r = recv (c->fd, str, 79, 0);
			
	if (r > 0) {
		str[r] = '\0';
		      
		printf ("data: %d: %s", c->fd, str);
		mserver_sendtoall (c, str, r);
	} else {
		printf ("client %d is disconnected\n", c->fd);
		/* disconnect socket */
		close (c->fd);

		/* delete client_t * struct from list */
		c->next->prev = c->prev;
		c->prev->next = c->next;

		free (c);
		return -1;
	}
	
	return 0;
}

int mserver_loop ()
{
	struct timeval tv;

	FD_ZERO (&myset);
	FD_SET (fd, &myset);
	
	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next)
		FD_SET (c->fd, &myset);

	tv.tv_sec = 1;
	tv.tv_usec = 0;

	int ret = select (0, &myset, NULL, NULL, &tv);

	if (ret == -1) {
		printf ("select error\n");
		return -1;
	} else if (ret == 0) {
		printf ("select timeout\n");
		return -1;
	}

	/* check for incoming connections */
	if (mserver_newclient ())
		return 0;

	for (c = client_list.next; c != &client_list; c = c->next) {
		if (mserver_handle (c) == -1)
			break;
	}
		
	return 0;
}

int main (int argc, char **argv)
{
	int ret = mserver_bind (1234);
  
	if (ret == -1)
		return -1;
	
	for (;; schedule ())
		mserver_loop ();

	return 0;
}
