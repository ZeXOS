/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

size_t fwrite (const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	if (!stream)
		goto err;

	if (stream->mode & O_RDONLY)
		goto err;

	int fd = stream->fd;
	
	int len = write (fd, (void *) ptr, size * nmemb);

	if (!len)
		goto err;

	stream->off += len;

	return len;
err:
	errno_update ();

	return 0;
}
