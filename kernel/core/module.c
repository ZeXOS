/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <module.h>
#include <file.h>
#include <task.h>
#include <fd.h>

module_t module_list;

void module_display ()
{
	printf ("Module\t\tSize\n");
	module_t *kmod;
	for (kmod = module_list.next; kmod != &module_list; kmod = kmod->next)
		if (kmod->name)
			printf ("%s\t\t%u\n", kmod->name, kmod->mem_usage/1024);
		else
			printf ("?\t\t%u\n", kmod->mem_usage/1024);
}

module_t *module_find (task_t *task)
{
	module_t *kmod;
	for (kmod = module_list.next; kmod != &module_list; kmod = kmod->next) {
		if (kmod->task == task)
			return kmod;
	}

	return 0;
}

module_t *module_load (char *modname)
{
	int f = open (modname, O_RDONLY);
	
	if (!f)
		return 0;

	fd_t *fd = fd_get (f);

	if (!fd)
		return 0;

	module_t *kmod = (module_t *) kmalloc (sizeof (module_t));

	if (!kmod)
		return 0;

	kmod->mem_usage = fd->e + sizeof (module_t) + 1;

	kmod->image = (char *) kmalloc (sizeof (char) * fd->e + 1);

	if (!kmod->image)
		return 0;

	read (f, kmod->image, fd->e);


	unsigned entry;
	unsigned data, data_off, bss;
	int err = exec_elf (kmod->image, &entry, &data, &data_off, &bss);

	if (err != NULL) {
		printf ("ERROR -> invalid module %s\n", modname);
		kfree (kmod->image);
		kfree (kmod);
		close (f);
		return 0;
	}

	task_t *task = (task_t *) task_create (modname, entry, KMOD_MODULE_PRIORITY);

	if (!task) {
		printf ("ERROR -> invalid module task\n");
		kfree (kmod->image);
		kfree (kmod);
		close (f);
		return 0;
	}

	kmod->task = task;

	unsigned l = strlen (modname);
	kmod->name = (char *) kmalloc (sizeof (char) * l + 1);

	if (!kmod->name) {
		task_done (task);
		kfree (kmod->image);
		kfree (kmod);
		close (f);
		return 0;
	}

	memcpy (kmod->name, modname, l);
	kmod->name[l] = '\0';	

	/* add into list */
	kmod->next = &module_list;
	kmod->prev = module_list.prev;
	kmod->prev->next = kmod;
	kmod->next->prev = kmod;

	return kmod;
}

unsigned int init_module (void)
{
	module_list.next = &module_list;
	module_list.prev = &module_list;

	return 1;
}
