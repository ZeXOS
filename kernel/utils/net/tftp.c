/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <net/socket.h>


unsigned tftp_client_connect (char *address, unsigned short port, char *file, unsigned *l)
{
	int sock = 0;

	hostent *host;
	sockaddr_in serverSock;

	// Check info about remote computer
	if ((host = gethostbyname (address)) == NULL) {
		printf ("tftp -> wrong address\n");
		return 0;
	}

	// Create socket
	if ((sock = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		printf ("tftp -> cant create socket\n");
		return 0;
	}

	// Zapln�me strukturu sockaddr_in
	// 1) Rodina protokol�
	serverSock.sin_family = AF_INET;
	// 2) ��slo portu, ke kter�mu se p�ipoj�me
	serverSock.sin_port = swap16 (port);
	// 3) Nastaven� IP adresy, ke kter� se p�ipoj�me
	memcpy (&(serverSock.sin_addr), host->h_addr, host->h_length);
	
	if (connect (sock, (sockaddr *) &serverSock, sizeof (serverSock)) == -1) {
		printf ("tftp -> cant connect to server\n");
		return 0;
	}

	unsigned len = *l;

	char *buf = (char *) kmalloc (sizeof (char) * (len + 64));

	if (!buf)
		return 0;

	/* Read request for tftp server */
	unsigned short opcode = swap16 (1);

	unsigned char tsize = '0';

	memcpy (buf, (char *) &opcode, 2);
	memcpy (buf+2, file, len+1);
	memcpy (buf+3+len, "octet", 6);
	memcpy (buf+9+len, "blksize", 8);
	memcpy (buf+17+len, "1024", 5);
	memcpy (buf+22+len, "tsize", 6);
	memcpy (buf+28+len, (char *) &tsize, 1);

	buf[29+len] = '\0';

	int ret = send (sock, buf, 30+len, 0);

	if (ret == -1) {
		kfree (buf);
		sclose (sock);
		return 0;
	}

	ret = recv (sock, buf, 30+len, 0);

	if (ret > 0) {
		buf[ret] = '\0';

		unsigned short opcode2 = 0;

		memcpy (&opcode2, buf, 2);
		
		if (swap16 (opcode2) != 6) {
			printf ("tftp -> server send error message: %s\n", buf+4);
			kfree (buf);
			sclose (sock);
			return 0;
		}

		unsigned blksize2 = atoi (buf+10);

		if (blksize2 != 1024) {
			kfree (buf);
			sclose (sock);
			return 0;
		}

		*l = atoi (buf+21);
	} else {
		kfree (buf);
		sclose (sock);
		return 0;
	}

	kfree (buf);

	return sock;
}

unsigned tftp_client_read (int sock, char *buf, unsigned len)
{
	unsigned l;
	unsigned short i;
	unsigned short block = 0;
	char b[1101];

	/* acknowledgement opcode */
	unsigned short opcode = swap16 (4);

	i = swap16 (block);

	memcpy (b, (char *) &opcode, 2);
	memcpy (b+2, (char *) &i, 2);

	if ((send (sock, b, 4, 0)) == -1) {
		printf ("tftp -> something go wrong with sending acknowledgement packet");
		return 0;
	}

	block ++;

	int ret = 0;
	for (l = 0; l < len; l += 1024) {
		ret = recv (sock, b, 1100, 0);

		if (ret > 0) {
			memcpy (buf+l, b+4, 1024);

			i = swap16 (block);

			memcpy (b, (char *) &opcode, 2);
			memcpy (b+2, (char *) &i, 2);

			if ((send (sock, b, 4, 0)) == -1) {
				printf ("tftp -> something go wrong with sending acknowledgement packet");
				return 0;
			}

			block ++;
		}
	}

	return 1;
}

