/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *getcwd (char *buf, size_t size)
{
	char *cwd = getenv ("PWD");
	
	if (!cwd) {
		errno = ENOENT;
		return 0;
	}

	unsigned l = strlen (cwd); 

	if (!buf) {
		if (!size)
			size = l;
		else {
			errno = EINVAL;
			return 0;
		}
		
		buf = (char *) malloc (sizeof (char) * size);
		
		if (!buf) {
			errno = EFAULT;
			return 0;
		}
			
	}
	
	if (l > size) {
		errno = ERANGE;
		return 0;
	}
	
	memcpy (buf, cwd, l);
	buf[l] = '\0';
	
	return buf;
}
