/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <snd/audio.h>

#define KB_ESC		1
#define RIFF_MAGIC	0x46464952
#define WAVE_MAGIC	0x20746d66
#define DATA_MAGIC	0x61746164

/* RIFF header */
typedef struct {
	unsigned chunkid;
	unsigned chunksize;
	unsigned format;
} riff_head_t;

/* WAVE header */
typedef struct {
	unsigned subchunk1id;
	unsigned subchunk1size;
	unsigned short audioformat;
	unsigned short numchannels;
	unsigned samplerate;
	unsigned byterate;
	unsigned short blockalign;
	unsigned short bitspersample;
} fmt_head_t;

/* DATA header */
typedef struct {
	unsigned subchunk2id;
	unsigned subchunk2size;
} data_head_t;

int main (int argc, char **argv)
{
	printf ("play - The Wav Player\n");

	if (argc <= 1) {
		printf ("-> Please specify *.wav file to play\nSyntax: exec play <wavfile>\n");
		return -1;
	}

	FILE *f = fopen (argv[1], "r");

	if (!f) {
		printf ("error -> file '%s' not found\n", argv[1]);
		return -1;
	}

	fseek (f, 0, SEEK_END);
	unsigned flen = ftell (f);
	fseek (f, 0, SEEK_SET);

	/* HACK: because of virtualbox & qemu ac'97 bug */
	char *file = (char *) 0x400000;

	if (!file)
		return 0;

	fread (file, flen, 1, f);

	fclose (f);

	riff_head_t *riff = (riff_head_t *) file;
	fmt_head_t *fmt = (fmt_head_t *) ((char *) file + sizeof (riff_head_t));
	data_head_t *data = (data_head_t *) ((char *) file + sizeof (riff_head_t) + sizeof (fmt_head_t));

	if (riff->chunkid != RIFF_MAGIC) {
		printf ("-> Wrong RIFF header magic: 0x%x, should be: 0x%x\n", riff->chunkid, RIFF_MAGIC);
		goto clean;
	}

	if (fmt->subchunk1id != WAVE_MAGIC) {
		printf ("-> Wrong WAVE header magic: 0x%x, should be: 0x%x\n", fmt->subchunk1id, WAVE_MAGIC);
		goto clean;
	}

	if (data->subchunk2id != DATA_MAGIC) {
		printf ("-> Wrong DATA header magic: 0x%x, should be: 0x%x\n", data->subchunk2id, DATA_MAGIC);
		goto clean;
	}

	snd_cfg_t cfg;
	cfg.dev = "/dev/ac97";
	cfg.rate = fmt->samplerate;
	cfg.format = SOUND_FORMAT_S16;
	cfg.channels = fmt->numchannels;

	snd_audio_t *aud = audio_open (&cfg);

	if (!aud) {
		printf ("Device is too busy\n");
		goto clean;
	}

	audio_write (aud, file+44, flen-44);	

	printf ("Press ESC to stop playing\n");
	
	for (; getkey () != KB_ESC; schedule ());

	audio_close (aud);
clean:
	free (file);

	return 0;
}
