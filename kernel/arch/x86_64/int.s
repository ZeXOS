;  ZeX/OS
;  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
;  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
;
;  This program is free software: you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this program.  If not, see <http://www.gnu.org/licenses/>.


%macro	IMP 1
%ifdef UNDERBARS
	EXTERN _%1
	%define %1 _%1
%else
	EXTERN %1
%endif
%endmacro

%macro	EXP	1
	GLOBAL $_%1
	$_%1:
	GLOBAL $%1
	$%1:
%endmacro

; IMPORTS
; from KRNL.LD
IMP bss
IMP end

; from MAIN.C
IMP g_curr_thread
IMP fault
IMP main

[BITS 32]
; This will set up our new segment registers. We need to do
; something special in order to set CS. We do what is called a
; far jump. A jump that includes a segment as well as an offset.
; This is declared in C as 'extern void gdt_flush();'
EXP gdt_flush     ; Allows the C code to link to this
IMP gp            ; Says that '_gp' is in another file
;_gdt_flush:
    lgdt [gp]        ; Load the GDT with our '_gp' which is a special pointer
    mov ax, 0x10      ; 0x10 is the offset in the GDT to our data segment
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    jmp 0x08:flush2   ; 0x08 is the offset to our code segment: Far jump!
flush2:
    ret               ; Returns back to the C code!

; Loads the IDT defined in '_idtp' into the processor.
; This is declared in C as 'extern void idt_load();'
EXP idt_load
IMP idtp
    lidt [idtp]
    ret

;  0: Divide By Zero Exception
EXP isr0
    cli
    push byte 0
    push byte 0
    jmp isr_common_stub

;  1: Debug Exception
EXP isr1
    cli
    push byte 0
    push byte 1
    jmp isr_common_stub

;  2: Non Maskable Interrupt Exception
EXP isr2
    cli
    push byte 0
    push byte 2
    jmp isr_common_stub

;  3: Int 3 Exception
EXP isr3
    cli
    push byte 0
    push byte 3
    jmp isr_common_stub

;  4: INTO Exception
EXP isr4
    cli
    push byte 0
    push byte 4
    jmp isr_common_stub

;  5: Out of Bounds Exception
EXP isr5
    cli
    push byte 0
    push byte 5
    jmp isr_common_stub

;  6: Invalid Opcode Exception
EXP isr6
    cli 
    push byte 0
    push byte 6
    jmp isr_common_stub

;  7: Coprocessor Not Available Exception
EXP isr7
    cli
    push byte 0
    push byte 7
    jmp isr_common_stub

;  8: Double Fault Exception (With Error Code!)
EXP isr8
    cli
    push byte 8
    jmp isr_common_stub

;  9: Coprocessor Segment Overrun Exception
EXP isr9
    cli
    push byte 0
    push byte 9
    jmp isr_common_stub

; 10: Bad TSS Exception (With Error Code!)
EXP isr10
    cli
    push byte 10
    jmp isr_common_stub

; 11: Segment Not Present Exception (With Error Code!)
EXP isr11
    cli
    push byte 11
    jmp isr_common_stub

; 12: Stack Fault Exception (With Error Code!)
EXP isr12
    cli
    push byte 12
    jmp isr_common_stub

; 13: General Protection Fault Exception (With Error Code!)
EXP isr13
    cli
    push byte 13
    jmp isr_common_stub

; 14: Page Fault Exception (With Error Code!)
EXP isr14
    cli
    push byte 14
    jmp isr_common_stub

; 15: Reserved Exception
EXP isr15
    cli
    push byte 0
    push byte 15
    jmp isr_common_stub

; 16: Floating Point Exception
EXP isr16
    cli
    push byte 0
    push byte 16
    jmp isr_common_stub

; 17: Alignment Check Exception
EXP isr17
    cli
    push byte 0
    push byte 17
    jmp isr_common_stub

; 18: Machine Check Exception
EXP isr18
    cli
    push byte 0
    push byte 18
    jmp isr_common_stub

; 19: Reserved
EXP isr19
    cli
    push byte 0
    push byte 19
    jmp isr_common_stub

; 20: Reserved
EXP isr20
    cli
    push byte 0
    push byte 20
    jmp isr_common_stub

; 21: Reserved
EXP isr21
    cli
    push byte 0
    push byte 21
    jmp isr_common_stub

; 22: Reserved
EXP isr22
    cli
    push byte 0
    push byte 22
    jmp isr_common_stub

; 23: Reserved
EXP isr23
    cli
    push byte 0
    push byte 23
    jmp isr_common_stub

; 24: Reserved
EXP isr24
    cli
    push byte 0
    push byte 24
    jmp isr_common_stub

; 25: Reserved
EXP isr25
    cli
    push byte 0
    push byte 25
    jmp isr_common_stub

; 26: Reserved
EXP isr26
    cli
    push byte 0
    push byte 26
    jmp isr_common_stub

; 27: Reserved
EXP isr27
    cli
    push byte 0
    push byte 27
    jmp isr_common_stub

; 28: Reserved
EXP isr28
    cli
    push byte 0
    push byte 28
    jmp isr_common_stub

; 29: Reserved
EXP isr29
    cli
    push byte 0
    push byte 29
    jmp isr_common_stub

; 30: Reserved
EXP isr30
    cli
    push byte 0
    push byte 30
    jmp isr_common_stub

; 31: Reserved
EXP isr31
    cli
    push byte 0
    push byte 31
    jmp isr_common_stub


; We call a C function in here. We need to let the assembler know
; that '_fault_handler' exists in another file


; This is our common ISR stub. It saves the processor state, sets
; up for kernel mode segments, calls the C-level fault handler,
; and finally restores the stack frame.
isr_common_stub:
IMP fault_handler
    pusha
    push ds
    push es
    push fs
    push gs
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, esp
    push eax
    mov eax, fault_handler
    call eax
    pop eax
    pop gs
    pop fs
    pop es
    pop ds
    popa
    add esp, 8
    iret

;global _irq0
;global _irq1
;global _irq2
;global _irq3
;global _irq4
;global _irq5
;global _irq6
;global _irq7
;global _irq8
;global _irq9
;global _irq10
;global _irq11
;global _irq12
;global _irq13
;global _irq14
;global _irq15

; 32: IRQ0
EXP irq0
    cli
    push byte 0
    push byte 32
    jmp irq_common_stub

; 33: IRQ1
EXP irq1
    cli
    push byte 0
    push byte 33
    jmp irq_common_stub

; 34: IRQ2
EXP irq2
    cli
    push byte 0
    push byte 34
    jmp irq_common_stub

; 35: IRQ3
EXP irq3
    cli
    push byte 0
    push byte 35
    jmp irq_common_stub

; 36: IRQ4
EXP irq4
    cli
    push byte 0
    push byte 36
    jmp irq_common_stub

; 37: IRQ5
EXP irq5
    cli
    push byte 0
    push byte 37
    jmp irq_common_stub

; 38: IRQ6
EXP irq6
    cli
    push byte 0
    push byte 38
    jmp irq_common_stub

; 39: IRQ7
EXP irq7
    cli
    push byte 0
    push byte 39
    jmp irq_common_stub

; 40: IRQ8
EXP irq8
    cli
    push byte 0
    push byte 40
    jmp irq_common_stub

; 41: IRQ9
EXP irq9
    cli
    push byte 0
    push byte 41
    jmp irq_common_stub

; 42: IRQ10
EXP irq10
    cli
    push byte 0
    push byte 42
    jmp irq_common_stub

; 43: IRQ11
EXP irq11
    cli
    push byte 0
    push byte 43
    jmp irq_common_stub

; 44: IRQ12
EXP irq12
    cli
    push byte 0
    push byte 44
    jmp irq_common_stub

; 45: IRQ13
EXP irq13
    cli
    push byte 0
    push byte 45
    jmp irq_common_stub

; 46: IRQ14
EXP irq14
    cli
    push byte 0
    push byte 46
    jmp irq_common_stub

; 47: IRQ15
EXP irq15
    cli
    push byte 0
    push byte 47
    jmp irq_common_stub

IMP irq_handler

irq_common_stub:
    pusha
    push ds
    push es
    push fs
    push gs

    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, esp

    push eax
    mov eax, irq_handler
    call eax
    pop eax

    pop gs
    pop fs
    pop es
    pop ds
    popa
    add esp, 8
    iret

;extern testf
;global testf_c

;testf_c:
;	mov ecx, testf
;	jmp syscall0

; syscall with 0 args

;IMP syscall_handler
;EXP system_call
;	push ds				; switch to kernel space
;	push es
;	mov eax, 0x10
;	mov ds, eax
;	mov es, eax
	
;	call syscall_handler			; make call
	
;	pop es				; back to userland
;	pop ds	
;	retf 0 
;	iret

IMP syscall_handler
EXP system_call
	pusha
	push ds
	push es
	push fs
	push gs
	
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

;	mov eax, esp

;	push edx
;	push ecx
;	push ebx
	push esp

	call syscall_handler			; make call

	add esp, 4
;	pop eax
;	pop ebx
;	pop ecx
;	pop edx

	pop gs
	pop fs
	pop es
	pop ds

	popa
	
	iret