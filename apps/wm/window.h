/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _WINDOW_H
#define _WINDOW_H

#define WINDOW_FLAG_DRAG	0x1
#define WINDOW_FLAG_MINIMALIZED	0x2
#define WINDOW_FLAG_MAXIMALIZED	0x4

typedef struct wmwindow_context {
	struct wmwindow_context *next, *prev;

	unsigned x;
	unsigned y;
	unsigned size_x;
	unsigned size_y;
	unsigned char active;
	unsigned char flags;
	char *caption;
} wmwindow;

/* externs */
extern wmwindow *window_create (const char *caption);
extern unsigned window_delete (wmwindow *window);
extern unsigned window_draw (wmwindow *window);
extern unsigned window_draw_all ();
extern unsigned init_window ();

#endif
