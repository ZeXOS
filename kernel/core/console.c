/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <console.h>
#include <tty.h>
#include <config.h>
#include <net/hostname.h>

static consolelog_t clog_list;
static consolelog_t *clog_curr;

static unsigned clog_num;

extern task_t *_curr_task;

int gets ()
{
	if (!currtty->active)
		return 0;

	char key = getkey ();

	if (key) {
		//DPRINT ("currtty->task %s | _curr_task %s\n", currtty->task->name, _curr_task->name);

		int i = 0;
		video_color (2, 0);

		char c[1];
		c[0] = key;

		if (key == 8 && currtty->shell_len == 0 || key == 27)
			return i;

        	if (currtty->shell_len < 64)
			tty_write (currtty, c, 1);

		if (key == '\n') { // we press enter
			currtty->shell[currtty->shell_len] = '\0';
			currtty->shell_len = 0;
			i = 1;
		} else {
			/* if you has wroted some text, and backspace is pressed, lets move cursor back */
			if (key == '\b' && currtty->shell_len > 0) {
				currtty->shell_len --;			// first we have to descrease text length
				currtty->shell[currtty->shell_len] = '\0';		// we can put \0 character under cursor
				//printf ("len: %d\n", currtty->shell_len);
			}
			else if (currtty->shell_len < 64) { /* you type normal text */
				currtty->shell[currtty->shell_len] = key;
				currtty->shell_len ++;
			}
		}

		key = 0;
		video_color (15, 0);
	
		return i;
	}

	return 0;
}

unsigned consolelog_list ()
{
	consolelog_t *c;

	for (c = clog_list.next; c != &clog_list; c = c->next)
		printf ("%s\n", c->cmd);

	printf ("-> %d\n", clog_num);

	return 1;
}

unsigned consolelog_prev ()
{
	if (!currtty->active)
		return 0;

	if (!currtty->logged)
		return 0;

	if (!clog_curr)
		return 0;

	/* go to previous entry of log list */
	consolelog_t *prev = clog_curr;

	if (!prev)
		return 0;

	unsigned short l = currtty->shell_len;

	/* delete all current characters from line () */
	while (l) {
		tty_write (currtty, "\b", 1);

		l --;
	}

	memcpy (currtty->shell, prev->cmd, prev->len);
	currtty->shell[prev->len] = '\0';

	currtty->shell_len = prev->len;

	video_color (2, 0);

	tty_write (currtty, currtty->shell, prev->len);

	clog_curr = prev->prev;

	return 1;
}

unsigned consolelog_next ()
{
	if (!currtty->active)
		return 0;

	if (!currtty->logged)
		return 0;

	if (!clog_curr)
		return 0;

	/* go to next entry of log list */
	consolelog_t *next = clog_curr->next;

	if (!next)
		return 0;

	unsigned short l = currtty->shell_len;

	/* delete all current characters from line () */
	while (l) {
		tty_write (currtty, "\b", 1);

		l --;
	}

	memcpy (currtty->shell, next->cmd, next->len);
	currtty->shell[next->len] = '\0';

	currtty->shell_len = next->len;

	video_color (2, 0);

	tty_write (currtty, currtty->shell, next->len);

	clog_curr = next;

	return 1;
}

unsigned int console_refresh ()
{
	currtty->shell_len = 0;

	if (currtty->user) {
		if (currtty->user->attrib == USER_GROUP_ADMIN) {
			video_color (12, 0);
			printf ("%s@%s ", currtty->user->name, hostname_get ());
			video_color (9, 0);
			printf ("%s # ", (char *) env_get ("PWD"));
		} else {
			video_color (14, 0);
			printf ("%s@%s ", currtty->user->name, hostname_get ());
			video_color (9, 0);
			printf ("%s $ ", (char *) env_get ("PWD"));
		}
	}

	video_color (15, 0);

	return 1;
}

unsigned int init_console ()
{
	clog_num = 0;
	clog_curr = 0;

	clog_list.next = &clog_list;
	clog_list.prev = &clog_list;

	return 1;
}

void console (int i)
{
	unsigned len = strlen (currtty->shell);

	/* commands are active only, if you type one or more chars */
	if (len) {
		/* parse command */
		commands (i);

		/* save this command to consolelog now */
		consolelog_t *ctx;

		/* when we reach limit, lets delete first one from list */
		if (clog_num > CONFIG_UI_CONSOLELOG) {
			ctx = clog_list.next; 

			if (ctx != &clog_list && ctx) {
				kfree (ctx->cmd);

				ctx->next->prev = ctx->prev;
				ctx->prev->next = ctx->next;

				kfree (ctx);

				clog_num --;
			}
		}

		// alloc and init context
		ctx = (consolelog_t *) kmalloc (sizeof (consolelog_t));

		if (!ctx)
			return;

		ctx->cmd = (char *) kmalloc (sizeof (char) * (len + 1));
		
		if (!ctx->cmd)
			return;

		memcpy (ctx->cmd, currtty->shell, len);
		ctx->cmd[len] = '\0';

		ctx->len = len;
	
		ctx->next = &clog_list;
		ctx->prev = clog_list.prev;
		ctx->prev->next = ctx;
		ctx->next->prev = ctx;
 
		/* increase number of list content */
		clog_num ++;

		/* go to last entry */
		clog_curr = ctx;
	}

	/* clean console */
	for(i = 0; i < currtty->shell_len; i ++)
		currtty->shell[i] = '\0';

	console_refresh ();
}

