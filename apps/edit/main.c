/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#define		ESC				1
#define		ARROWLEFT			75
#define		ARROWRIGHT			77
#define		ARROWUP				72
#define		ARROWDOWN			80

#define 	VERSION				"0.1.0"

#define		MENU_REQUEST_OPEN		0
#define		MENU_REQUEST_CLOSE		1


#define		KBD_BUF_LEN			32

#define		LINE_MAX_LEN			512


typedef struct line_context {
	struct line_context *next, *prev;
	char *buffer;
	unsigned short len;
} line_t;
line_t line_list;

typedef struct {
	short csr_x;
	short csr_y;
	char buffer[LINE_MAX_LEN];
	unsigned short len;
} page_t;
page_t page;

#ifdef LINUX
void gotoxy ()
{
  
}

void cls ()
{
  
}

void schedule ()
{
  
}
#endif

unsigned quit;

unsigned cmd_handler (char *buffer, unsigned len);

line_t *line_add (char *buf, unsigned len)
{
	if (cmd_handler (buf, len))
		return 0;

	/* alloc and init context */
	line_t *line = (line_t *) malloc (sizeof (line_t));

	if (!line)
		return 0;

	line->buffer = len ? strndup (buf, len) : 0;
	line->len = len;

	/* add into list */
	line->next = &line_list;
	line->prev = line_list.prev;
	line->prev->next = line;
	line->next->prev = line;

	return line;
}

int line_print (line_t *line)
{
	if (line->buffer)
		puts (line->buffer);
	else
		putchar ('\n');
		
	return 0;
}

int line_printall ()
{
	line_t *line;
	for (line = line_list.next; line != &line_list; line = line->next)
		line_print (line);
	
	return 0;
}

int file_load (char *name)
{
	FILE *f = fopen (name, "r");

	if (!f) {
		printf ("err: %s\n", name);
		return -1;
	}

	for (;;) {
		char *r = fgets (page.buffer, LINE_MAX_LEN, f);

		if (!r)
			break;

		unsigned l = strlen (page.buffer);

		if (l >= LINE_MAX_LEN)
			l = LINE_MAX_LEN-1;
		
		line_add (page.buffer, l-1);
	}

	fclose (f);

	return 0;
}

int file_save (char *name)
{
	FILE *f = fopen (name, "w");

	if (!f) {
		printf ("err: %s\n", name);
		return -1;
	}

	unsigned len = 0;

	line_t *line;
	for (line = line_list.next; line != &line_list; line = line->next)
		len += line->len + 1;

	char *p = (char *) malloc (sizeof (char) * (len + 1));

	if (!p) {
		fclose (f);
		return -1;
	}
	
	len = 0;
	for (line = line_list.next; line != &line_list; line = line->next) {
		memcpy (p+len, line->buffer, line->len);
		p[len+line->len] = '\n';
		len += line->len + 1;
	}

	int r = fwrite (p, len, 1, f);

	if (r < 1) {
		printf ("ERROR -> fwrite ()\n");
	}

	free (p);

	fclose (f);

	return 0;
}

unsigned cmd_handler (char *buffer, unsigned len)
{
	if (len >= 4) {
		if (!strncmp (buffer, ":quit", len)) {
			quit = 1;
			return 1;
		}

		if (!strncmp (buffer, ":save", 4)) {
			if (len > 6) {
				if (buffer[5] != ' ')
					return 1;

				//creat (buffer+6, O_CREAT);

				file_save (buffer+6);
				printf ("Saved: '%s'\n", buffer+6);
			}

			return 1;
		}
	}

	return 0;
}

void cursor_update (unsigned short x, unsigned short y)
{
	gotoxy (x, y);
}

char kbd_getchar ()
{
	char c = getchar ();

	if (c == -1)
		return 0;

	switch (c) {
		case '\b':
			if (page.csr_x > 0)
				page.csr_x -= 1;

			if (page.csr_x > 79)
				page.csr_x = 79;

			if (page.len > 0)
				page.len --;

			//cursor_update (page.csr_x, page.csr_y);
			return 0;
		case '\n':
			page.csr_y ++;
			page.csr_x = 0;
			page.buffer[page.len] = '\0';

			line_add (page.buffer, page.len);
	
			memset (page.buffer, 0, page.len);
			page.len = 0;

			//cursor_update (page.csr_x, page.csr_y);
			return 0;
		default:
			page.csr_x ++;
			page.len ++;
			
			break;
	}

	return c;
}

unsigned text_handler ()
{
	char c = kbd_getchar ();

	if (c)
		page.buffer[page.csr_x - 1] = c;

	return c ? 1 : 0;
}

int loop ()
{
	for (;;) {
		text_handler ();

		schedule ();

		if (quit)
			break;
	}

	return 0;
}

int init ()
{
	quit = 0;

	page.csr_x = 0;
	page.csr_y = 0;

	page.len = 0;

	line_list.next = &line_list;
	line_list.prev = &line_list;

	cls ();

	return 0;
}

int main (int argc, char **argv)
{
	init ();

	int oldFlag = fcntl (1, F_GETFL, 0);
	if (fcntl (1, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	if (argc > 1)
		file_load (argv[1]);

	line_printall ();

	loop ();

	return 0;
}
