/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "cursor.h"
#include "menu.h"

wmmenu wmmenu_list;

extern wmcursor *cursor;

wmmenu *menu_create (unsigned x, unsigned y, const char *caption)
{
	wmmenu *menu = (wmmenu *) malloc (sizeof (wmmenu));

	if (!menu)
		return 0;

	menu->x = x;
	menu->y = y;

	unsigned l = strlen (caption);
	if (l) {
		menu->caption = (char *) malloc (sizeof (char) * l + 1);
		memcpy (menu->caption, caption, l);
		menu->caption[l] = '\0';
	} else
		menu->caption = 0;

	menu->size_x = l*6+4;
	menu->size_y = 10;
	menu->flags = 0;

	/* add into list */
	menu->next = &wmmenu_list;
	menu->prev = wmmenu_list.prev;
	menu->prev->next = menu;
	menu->next->prev = menu;

	return menu;
}

wmmenuitem *menu_additem (wmmenu *menu, const char *caption, wmmenu_handler *handler)
{
	if (!menu)
		return 0;

	wmmenuitem *item = (wmmenuitem *) malloc (sizeof (wmmenuitem));

	if (!item)
		return 0;

	menu->size_y += 10;

	unsigned l = strlen (caption);
	if (l) {
		item->caption = (char *) malloc (sizeof (char) * l + 1);
		memcpy (item->caption, caption, l);
		item->caption[l] = '\0';
	} else
		item->caption = 0;

	if (menu->size_x < l*6+3)
		menu->size_x = l*6+3;

	item->handler = handler;

	/* add into list */
	item->next = &menu->item_list;
	item->prev = menu->item_list.prev;
	item->prev->next = item;
	item->next->prev = item;

	return item;
}

unsigned menu_show (wmmenu *menu)
{
	if (menu->flags & MENU_FLAG_SHOW)
		menu->flags &= ~MENU_FLAG_SHOW;
	else
		menu->flags |= MENU_FLAG_SHOW;

	return 1;
}

unsigned char menuitem_flags (wmmenuitem *item)
{
	if (!item)
		return 0;

	return item->flags;
}

unsigned menu_draw (wmmenu *menu)
{
	if (menu->flags & MENU_FLAG_SHOW) {
		xrect (menu->x, menu->y, menu->x+menu->size_x, menu->y+menu->size_y, 35*256);
		xrectfill (menu->x+1, menu->y+1, menu->x+menu->size_x-1, menu->y+8, 60*256);
		xrectfill (menu->x+1, menu->y+9, menu->x+menu->size_x-1, menu->y+menu->size_y-1, 0x00AEF8);
	
		if (menu->caption)
			xtext_puts (menu->x+2, menu->y+1, 0, menu->caption);

		unsigned i = 1;
		wmmenuitem *item;
		for (item = menu->item_list.next; item != &menu->item_list; item = item->next) {
			if (!item)
				continue;

			/* mame mys na nabidce ? */
			int l = (10*i);
			if (item->caption) {
				if (cursor->x > (signed) menu->x && cursor->x < (signed) menu->x+(signed) menu->size_x && 
					cursor->y > (signed) menu->y+l && cursor->y <= (signed) menu->y+l+10) {
	
					if (!(item->flags & MENUITEM_FLAG_MOUSE))
						item->flags |= MENUITEM_FLAG_MOUSE;

					xtext_puts (menu->x+2, menu->y+l, 50*256, item->caption);

					/* klik ? */
					if (!cursor->action) {
						if (cursor->state == XCURSOR_STATE_LBUTTON) {
							/*if (item->handler) 			FIXME: general protection fault ..
								item->handler ();*/
							if (!(item->flags & MENUITEM_FLAG_CLICKED))
								item->flags |= MENUITEM_FLAG_CLICKED;
	
							cursor->action = 1;
						}
					} else {
						if (cursor->state != XCURSOR_STATE_LBUTTON) {
							if (item->flags & MENUITEM_FLAG_CLICKED)
								item->flags &= ~MENUITEM_FLAG_CLICKED;
	
							cursor->action = 0;
						}
					}
				} else {
					if (item->flags & MENUITEM_FLAG_MOUSE)
						item->flags &= ~MENUITEM_FLAG_MOUSE;

					xtext_puts (menu->x+2, menu->y+l, 0, item->caption);
				}
			}	

			i ++;
		}
	}

	return 1;
}

unsigned menu_draw_all ()
{
	wmmenu *menu;
	for (menu = wmmenu_list.next; menu != &wmmenu_list; menu = menu->next) {
		if (!menu)
			continue;

		menu_draw (menu);
	}

	return 1;
}


unsigned init_menu ()
{
	wmmenu_list.next = &wmmenu_list;
	wmmenu_list.prev = &wmmenu_list;

	return 1;
}
