/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _NET_H
#define _NET_H

#include "platform.h"

#include <netinet/in.h>
#include <sys/socket.h>

#ifdef __linux__
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h>
#endif

#define NET_BUFFER_SIZE		2048

typedef struct {
	struct sockaddr_in conn;
	int sock;
	char *buffer;
} net_t;

extern int net_send (char *data, unsigned len);
extern int net_switchmode ();
extern int net_loop ();
extern int init_net ();

#endif
