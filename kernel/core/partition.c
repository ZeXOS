/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <dev.h>
#include <partition.h>
#include <fs.h>
#include <vfs.h>

partition_t partition_list;
extern vfs_t vfs_list;

partition_t *partition_find (char *name)
{
	partition_t *partition;
	for (partition = partition_list.next; partition != &partition_list; partition = partition->next) {
		if (!strcmp (partition->name, name))
			return partition;
	}

	return 0;
}

/* limited only for _one_ partition per device */
partition_t *partition_findbydev (dev_t *dev)
{
	unsigned l = strlen (dev->devname);

	partition_t *partition;
	for (partition = partition_list.next; partition != &partition_list; partition = partition->next) {
		if (!strncmp (partition->name, dev->devname, l))
			return partition;
	}

	return 0;
}

/* find partition by file in current directory */
partition_t *partition_findbyfile (char *file)
{
	char pwd[64];
	strcpy (pwd, (char *) env_get ("PWD"));
	
	vfs_t *vfs;
	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next)
		//printf ("pwd: %s : %s\n", vfs->mountpoint, pwd);
		if (!strcmp (vfs->mountpoint, pwd) &&
		    !cstrcmp (file, vfs->name)) {
			if (vfs->attrib & VFS_FILEATTR_DIR) {
				printf ("ERROR -> this is a directory, not an file\n");
				return 0;
			}
	
			/* check permissions */
			if (vfs->attrib & VFS_FILEATTR_SYSTEM && strcmp ((char *) env_get ("USER"), "root")) {
				printf ("ERROR -> only root can do that\n");
				return 0;
			}

			if (vfs->attrib & VFS_FILEATTR_MOUNTED) {
				partition_t *p = (partition_t *) mount_find ((char *) env_get ("PWD"));
	
				if (p)
					return p;
				else {
					printf ("ERROR -> device not respond\n");
					return 0;
				}
			}
	
			env_set ("PWD", pwd);
		
			return 0;
		}

	printf ("No such file : %s\n", file);

	return 0;
}

partition_t *partition_add (dev_t *dev, fs_t *fs, unsigned sector)
{
	if (!dev)
		return 0;

	char name[20];
	unsigned i;
	for (i = 0; i < 256; i ++) {
		sprintf (name, "%s%u", dev->devname, i);

		if (partition_find (name) == 0)
			break;
	}

	kprintf ("-> %s\n", name);

	partition_t *partition;

	/* alloc and init context */
	partition = (partition_t *) kmalloc (sizeof (partition_t));

	if (!partition)
		return 0;

	memset (partition, 0, sizeof (partition_t));

	strcpy (partition->name, name);

	partition->id = i;

	/* check for hd'c' name - hda, hdb are pair of first cable; hdc, hdb are pair of second cable */
	if (strlen (dev->devname) > 7) {
		if (dev->devname[7] < 'c')
			partition->base_io = 0x1F0;
		else
			partition->base_io = 0x170;
	} else
		partition->base_io = 0;

	if (!fs)
		partition->fs = (fs_t *) fs_detect (partition);
	else
		partition->fs = fs;

	if (!partition->fs) {
		kfree (partition);
		return 0;
	}

	/* first partition sector on hdd */
	partition->sector_start = sector;

	if (!partition->fs->handler) {
		kprintf ("ERROR -> fs handler is missing !\n");
		kfree (partition);
		return 0;
	}

	if (!partition->fs->handler (FS_ACT_INIT, (char *) partition, NULL, NULL)) {
		kfree (partition);
		return 0;
	}

	/* add into list */
	partition->next = &partition_list;
	partition->prev = partition_list.prev;
	partition->prev->next = partition;
	partition->next->prev = partition;

	return partition;
}

bool partition_del (partition_t *partition)
{
	if (partition) {
		partition->next->prev = partition->prev;
		partition->prev->next = partition->next;

		//kfree (vfs);
		return 1;
	}

	return 0;
}

unsigned partition_table (dev_t *dev)
{
	unsigned char block[512];

	if (!dev)
		return 0;

	unsigned l = strlen (dev->devname);

	if (l < 8)
		return 0;

	partition_t p;
	p.base_io = 0;

	switch (dev->devname[7]) {
		case 'a':
			p.id = 0;
			p.base_io = 0x1F0;
			break;
		case 'b':
			p.id = 1;
			p.base_io = 0x1F0;
			break;
		case 'c':
			p.id = 0;
			p.base_io = 0x170;
			break;
		case 'd':
			p.id = 1;
			p.base_io = 0x170;
			break;
	}

	/* read first sector from drive */
	if (!dev->handler (DEV_ACT_READ, &p, block, "", 0))
		return 0;

	unsigned i;
	for (i = 0; i < 4; i ++) {
		/* calculate MBR partition table address */
		ptable_t *table = (ptable_t *) ((void *) block + 446 + i * sizeof (ptable_t));

		if (!table->type)
			break;

		kprintf ("-> partition type %x - 0x%x (%d)\n", table->type, table->lba, table->blocks);

		partition_t *pnew = 0;

		switch (table->type) {
			case 0x83:
				  pnew = partition_add (dev, fs_supported ("ext2"), table->lba);
				  break;
			case 0x90:
				  pnew = partition_add (dev, fs_supported ("zexfs"), table->lba);
				  break;
		}
	}

	return 1;
}

unsigned partition_table_new (dev_t *dev, ptable_t *ntable)
{
	unsigned char block[512];

	if (!dev)
		return 0;

	unsigned l = strlen (dev->devname);

	if (l < 8)
		return 0;

	partition_t p;
	p.base_io = 0;

	switch (dev->devname[7]) {
		case 'a':
			p.id = 0;
			p.base_io = 0x1F0;
			break;
		case 'b':
			p.id = 1;
			p.base_io = 0x1F0;
			break;
		case 'c':
			p.id = 0;
			p.base_io = 0x170;
			break;
		case 'd':
			p.id = 1;
			p.base_io = 0x170;
			break;
	}

	/* read first sector from drive */
	if (!dev->handler (DEV_ACT_READ, &p, block, "", 0))
		return 0;

	unsigned i;
	for (i = 0; i < 4; i ++) {
		/* calculate MBR partition table address */
		ptable_t *table = (ptable_t *) ((void *) block + 446 + i * sizeof (ptable_t));

		/* when partition type is filled, rewrite old entry */
		if (ntable[i].type)
			memcpy (table, &ntable[i], sizeof (ptable_t));
		else
			memcpy (&ntable[i], table, sizeof (ptable_t));

		DPRINT (DBG_FS, "-> partition type %x - 0x%x (%d)", table->type, table->lba, table->blocks);
	}

	/* write into first sector of drive */
	if (!dev->handler (DEV_ACT_WRITE, &p, block, "", 0))
		return 0;

	return 1;
}

unsigned int init_partition ()
{
	partition_list.next = &partition_list;
	partition_list.prev = &partition_list;

	return 1;
}
