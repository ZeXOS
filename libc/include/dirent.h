/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DIRENT_H
#define _DIRENT_H

#include <vfs.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#define NAME_MAX	32

struct dirent {
	long d_ino;			/* inode number */
	off_t d_off;			/* offset to this dirent */
	unsigned short d_reclen;	/* length of this d_name */
	char d_name [NAME_MAX+1];	/* filename (null-terminated) */
};

typedef struct {
	char name[NAME_MAX+1];
	struct dirent *entry;
} DIR;

extern DIR *opendir (const char *name);
extern struct dirent *readdir (DIR *dir);
extern int closedir (DIR *dir);

#endif

