/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DIALOG_H
#define _DIALOG_H

#include "window.h"

#define ZDE_DOBJ_TYPE_TEXT	0x0
#define ZDE_DOBJ_TYPE_APPCL	0x1

typedef struct {
	char *data;
	unsigned len;
	unsigned short sizex;
	unsigned short sizey;
} zde_dobj_text_t;

typedef struct {
	unsigned state;
	unsigned short x;
	unsigned short y;
	unsigned short sizex;
	unsigned short sizey;
	short mousex;
	short mousey;
	unsigned char kbd;
	int fd;
} __attribute__ ((__packed__)) zde_dobj_appcl_t;

typedef struct zde_dobj_context {
	struct zde_dobj_context *next, *prev;

	unsigned short x;
	unsigned short y;
	unsigned type;
	void *arg;
	void *(*entry) (void *);
	void *object;
} zde_dobj_t;

extern zde_dobj_t *create_dialog_object (int type, void *arg, unsigned x, unsigned y, zde_win_t *window);
extern unsigned destroy_dialog_object (zde_win_t *window);
extern int draw_dialog_object (zde_win_t *window);
extern int init_dialog ();

#endif
