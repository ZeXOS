/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>

#include "icon.h"
#include "button.h"
#include "cursor.h"
#include "window.h"
#include "dialog.h"
#include "handler.h"

unsigned short *bitmap_folder;
unsigned short *bitmap_file;

zde_icon_t zde_icon_list;

extern zde_cursor_t *zde_cursor;

zde_icon_t *create_icon (char *name, int type, void *(*entry) (void *), void *arg)
{
	zde_icon_t *icon = (zde_icon_t *) malloc (sizeof (zde_icon_t));

	if (!icon)
		return 0;

	if (type == 0) {
		icon->arg = arg;

		icon->bitmap = bitmap_folder;
	} else
		icon->bitmap = 0;

	icon->name_len = strlen (name);
	icon->name = strdup (name);
	icon->entry = entry;
	icon->object = 0;
	icon->type = (char) type;
	
	/* add into list */
	icon->next = &zde_icon_list;
	icon->prev = zde_icon_list.prev;

	icon->x = 14;
	icon->y = icon->prev->y + 54;

	icon->prev->next = icon;
	icon->next->prev = icon;

	return icon;
}

zde_icon_t *create_icon_window (char *name, int type, void *(*entry) (void *), void *arg, zde_win_t *window)
{
	if (!window)
		return 0;

	zde_icon_t *icon = (zde_icon_t *) malloc (sizeof (zde_icon_t));

	if (!icon)
		return 0;

	icon->x = window->x + 14 + (window->objectid * 40);
	icon->y = window->y + 28;

	window->objectid ++;

	char *s = 0;
	unsigned arg_len;
	unsigned name_len;

	arg_len = strlen ((char *) arg);
	name_len = strlen (name);

	icon->arg = malloc (arg_len + name_len + 2);

	if (!icon->arg)
		return 0;

	s = icon->arg;

	memcpy (s, arg, arg_len);
	memcpy (s+arg_len, "/", 1);
	memcpy (s+arg_len+1, name, name_len);
	s[arg_len+name_len+1] = '\0';
	
	switch (type) {
		case 0:
			icon->bitmap = bitmap_folder;
			break;
		case 1:
			icon->bitmap = bitmap_file;
			break;
	}

	icon->name_len = strlen (name);
	icon->name = strdup (name);
	icon->entry = entry;
	icon->object = (void *) window;
	icon->type = (char) type;
	
	/* add into list */
	icon->next = &zde_icon_list;
	icon->prev = zde_icon_list.prev;
	icon->prev->next = icon;
	icon->next->prev = icon;

	return icon;
}

unsigned destroy_icon_window (zde_win_t *window)
{
	unsigned i;

	for (;;) {
		i = 0;

		zde_icon_t *icon;
		for (icon = zde_icon_list.next; icon != &zde_icon_list; icon = icon->next) {
			if (icon->object == (void *) window) {
				icon->next->prev = icon->prev;
				icon->prev->next = icon->next;

				free (icon);

				i = 1;
				break;
			}
		}

		if (!i)
			break;
	}

	return 1;
}

static void draw_bitmap (unsigned short *bitmap, unsigned short x, unsigned short y)
{
	unsigned i = 0;
	unsigned j = 0;
	unsigned k = 0;
	unsigned pix = 0;

	for (i = 0; i < 32*32; i ++) {
		if (k >= 32) {
			j ++;
			k = 0;
		}
		
		pix = (1024-i)+34;

		if (pix > 2100)
			continue;

		if (bitmap[pix] != (unsigned short) 0)
		if (k <= 32 && j <= 32)
			xpixel ((unsigned) x+k, (unsigned) y+j, (unsigned) bitmap[pix]);

		k ++;
	}
}

int draw_icon_desktop ()
{
	zde_icon_t *icon;
	for (icon = zde_icon_list.next; icon != &zde_icon_list; icon = icon->next) {
		if (icon->object)
			continue;

		/* button event */
		switch (zde_cursor->state) {
			case XCURSOR_STATE_LBUTTON:
			{
				if (!(zde_cursor->flags & CURSOR_FLAG_LBCLICK))
				if (!(zde_cursor->flags & CURSOR_FLAG_MOVE)) {
					if (zde_cursor->x >= icon->x && zde_cursor->x <= icon->x+32)
					if (zde_cursor->y >= icon->y && zde_cursor->y <= icon->y+32) {
						zde_cursor->state = 0;
						zde_cursor->flags |= CURSOR_FLAG_LBCLICK;

						/* create new thread as entry function */
						pthread_t thread;
						pthread_create (&thread, NULL, icon->entry, icon->arg);
					}
				}
				break;
			}
			case XCURSOR_STATE_RBUTTON:
			{
				if (!(zde_cursor->flags & CURSOR_FLAG_RBCLICK))
				if (!(zde_cursor->flags & CURSOR_FLAG_MOVE)) {
					if (zde_cursor->x >= icon->x && zde_cursor->x <= icon->x+32)
					if (zde_cursor->y >= icon->y && zde_cursor->y <= icon->y+32) {
						zde_cursor->state = 0;
						zde_cursor->flags |= CURSOR_FLAG_RBCLICK;
						
						/* create test window */
						zde_win_t *window = create_window ("Properties", 1);

						if (!window)
							return (0);
					  
						window->sizex = 95;
						window->sizey = 100;

						if (icon->type == 0)
							create_button_window ("Open", 0, handler_icon_filesystem, icon->arg, window);
						if (icon->type == 1)
							create_button_window ("Read", 0, handler_icon_reader, icon->arg, window);
						//create_button_window ("Delete", 0, handler_icon_filesystem, icon->arg, window);
					}
				}
	
				break;
			}
		}

		/* 32x32 bitmap */
		if (icon->bitmap)
			draw_bitmap (icon->bitmap, icon->x, icon->y);
		else
			xrectfill (icon->x, icon->y, icon->x+32, icon->y+32, ~0);

		if (icon->name)
			xtext_puts (icon->x-(icon->name_len*3)+16, icon->y+35, ~0, icon->name);
	}

	return 0;
}

int draw_icon_window (zde_win_t *window)
{
	unsigned obj = 0;
	unsigned obj_x = 0;
	unsigned obj_y = 0;

	zde_icon_t *icon;
	for (icon = zde_icon_list.next; icon != &zde_icon_list; icon = icon->next) {
		if (icon->object != (void *) window)
			continue;

		/* update icon position */
		icon->x = window->x + 14 + obj_x;
		icon->y = window->y + 28 + obj_y;

		obj ++;

		obj_x += 48;

		if (icon->x >= window->x+window->sizex-48) {
			obj_x = 0;
			obj_y += 50;
		}

		/* button event */
		switch (zde_cursor->state) {
			case XCURSOR_STATE_LBUTTON:
			{
				if (!(zde_cursor->flags & CURSOR_FLAG_LBCLICK))
				if (!(zde_cursor->flags & CURSOR_FLAG_MOVE)) {
					if (zde_cursor->x >= icon->x && zde_cursor->x <= icon->x+32)
					if (zde_cursor->y >= icon->y && zde_cursor->y <= icon->y+32) {
						if (isactive_window () != window)
							continue;

						zde_cursor->state = 0;
						zde_cursor->flags |= CURSOR_FLAG_LBCLICK;

						pthread_t thread;

						pthread_create (&thread, NULL, icon->entry, icon->arg);
					}
				}
				
				break;
			}
			case XCURSOR_STATE_RBUTTON:
			{
				if (!(zde_cursor->flags & CURSOR_FLAG_RBCLICK))
				if (!(zde_cursor->flags & CURSOR_FLAG_MOVE)) {
					if (zde_cursor->x >= icon->x && zde_cursor->x <= icon->x+32)
					if (zde_cursor->y >= icon->y && zde_cursor->y <= icon->y+32) {
						zde_cursor->state = 0;
						zde_cursor->flags |= CURSOR_FLAG_RBCLICK;
						
						/* create test window */
						zde_win_t *window = create_window ("Properties", 1);

						if (!window)
							return (0);
					  
						window->sizex = 95;
						window->sizey = 100;

						if (icon->type == 0)
							create_button_window ("Open", 0, handler_icon_filesystem, icon->arg, window);
						if (icon->type == 1)
							create_button_window ("Read", 0, handler_icon_reader, icon->arg, window);
						//create_button_window ("Delete", 0, handler_icon_filesystem, icon->arg, window);
					}
				}
	
				break;
			}
		}

		/* 32x32 bitmap */
		if (icon->bitmap)
			draw_bitmap (icon->bitmap, icon->x, icon->y);
		else
			xrectfill (icon->x, icon->y, icon->x+32, icon->y+32, ~0);

		if (icon->name)
			xtext_puts (icon->x-(icon->name_len*3)+16, icon->y+35, 0, icon->name);
	}

	return 0;
}

unsigned short *load_icon (char *filename)
{
	/* FOLDER bitmap */
	unsigned short *bitmap = (unsigned short *) malloc (2120);

	if (!bitmap)
		return 0;

	memset (bitmap, 0, 2048+70);

	// open icon bitmap
	int fd = open (filename, O_RDONLY);
	
	if (!fd) {
		printf ("error -> file '%s' not found\n", filename);
		return 0;
	}
	
	/* read icon bitmap data */
	if (!read (fd, (unsigned char *) bitmap, 2048+70)) {
		puts ("error -> something was wrong !\n");
		return 0;
	}

	return bitmap;
}

int init_icon ()
{
	zde_icon_list.next = &zde_icon_list;
	zde_icon_list.prev = &zde_icon_list;

	zde_icon_list.prev->x = 0;
	zde_icon_list.prev->y = 0;
	
	/* FOLDER bitmap */
	bitmap_folder = load_icon ("folder");

	if (!bitmap_folder)
		return -1;

	/* FILE bitmap */
	bitmap_file = load_icon ("file");

	if (!bitmap_file)
		return -1;

	/* create test icon */
	create_icon ("Filesystem", 0, handler_icon_filesystem, "");

	/* create test icon */
	create_icon ("Terminal", 0, handler_icon_terminal, "");

	return 0;
}
