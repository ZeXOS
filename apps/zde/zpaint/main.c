/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <appcl.h>

unsigned short *zpaint_bitmap;
zbitmap_t zbmp;

void zweb_draw ()
{
	zgui_puts (5, 5, "ZPaint", 0);
	
	unsigned short x;
	unsigned short y;
	
	int s = zgui_cursor (&x, &y);

	int yy = y - 20;

	if (yy < 0)
		yy = 0;
	else if (yy >= 200)
	  yy = 199;
	
	if (s)
		zbmp.data[zbmp.sizex*yy+x] = 0;
	
	zgui_bitmap (&zbmp);
}

int main (int argc, char **argv)
{
	int exit = 0;

	if (zgui_init () == -1)
		return -1;

	zgui_resize (328, 240);

	zpaint_bitmap = (unsigned short *) malloc (sizeof (unsigned short) * 320 * 200);
	
	if (!zpaint_bitmap)
		goto end;
	
	memset (zpaint_bitmap, ~0, sizeof (unsigned short) * 320 * 200);
	
	zbmp.x = 0;
	zbmp.y = 20;
	zbmp.resx = 320;
	zbmp.resy = 200;
	
	zbmp.posx = 0;
	zbmp.posy = 0;
	zbmp.sizex = 320;
	zbmp.sizey = 200;
	
	zbmp.data = zpaint_bitmap;
	
	while (!exit) {
		unsigned state = zgui_event ();

		if (state & APPCL_STATE_REDRAW)
			zweb_draw ();

		if (state & APPCL_STATE_EXIT)
			exit = 1;
	}
	
	free (zpaint_bitmap);
end:
	return zgui_exit ();
}
 
