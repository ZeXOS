/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <string.h>
#include <dev.h>
#include <fs.h>
#include <partition.h>
#include <cache.h>
#include <vfs.h>

#define ISOFS_SUPERBLOCK_SECTOR		16	// NOTE: sector is 2kB length

#define ISOFS_DIR_FLAGS_HIDDEN		0x1
#define ISOFS_DIR_FLAGS_DIR		0x2

#define	__PACKED__ __attribute__ ((__packed__))

typedef struct {
	unsigned char one;
	unsigned char dst[6];
	unsigned char zero;
	unsigned char sys_id[32];
	unsigned char vol_id[32];
	unsigned char zeros[8];
	unsigned long sectors[2];
	unsigned char zeros2[32];
	unsigned short set_size[2];
	unsigned short seq_num[2];
	unsigned short sector_size[2];
	unsigned long path_tbl[2];
	unsigned first_sec_fle_ptbl;
	unsigned first_sec_sle_ptbl;
	unsigned first_sec_fbe_ptbl;
	unsigned first_sec_sbe_ptbl;
	unsigned char root_dir_rec[34]; // 158
	unsigned char set_id[128];
	unsigned char pub_id[128];
	unsigned char datap_id[128];  // 542
	unsigned char app_id[128];
	unsigned char copy_id[37];
	unsigned char abst_id[37];
	unsigned char bibl_id[37];
	unsigned char date_volcr[17];
	unsigned char date_mod[17];
	unsigned char date_expir[17];
	unsigned char date_eff[17];
	unsigned char one2;
	unsigned char zero2;
	unsigned char reserved[512];
	unsigned char zeros3[653];
} __PACKED__ isofs_volume_t;	// 1395

typedef struct {
	unsigned char name_len;
	unsigned char sect_num;
	unsigned sect_data;
	unsigned short rec_num;
	unsigned char name;
} __PACKED__ isofs_path_t;

typedef struct {
	unsigned char rec_len;
	unsigned char sec_num;
	unsigned long data_sec[2];
	unsigned long data_len[2];
	unsigned char year;
	unsigned char month;
	unsigned char day;
	unsigned char hour;
	unsigned char minute;
	unsigned char second;
	unsigned char time_offset;
	unsigned char flags;
	unsigned char unit_size;
	unsigned char gap_size;
	unsigned short vol_seq_num[2];
	unsigned char id_len;
	unsigned char id[32];
	unsigned char id2[50];
} __PACKED__ isofs_dir_t;

typedef struct isofs_path_context {
	struct isofs_path_context *next, *prev;

	unsigned char name_len;
	unsigned char sect_num;
	unsigned sect_data;
	unsigned short rec_num;
	unsigned char *name;
} isofs_pathtbl_t;

isofs_pathtbl_t path_table_list;

/* FIXME: limit for one mounted iso fs */
static isofs_pathtbl_t *path_curr;
static isofs_pathtbl_t *path_prev;


static unsigned char tolower (char c)
{
	return ((c >= 'A' && c <= 'Z') ? c + 32: c);
}

static void toupper (char *str)
{
	unsigned i;

	for (i = 0; str[i]; i ++ ) {
		if (str[i] >= 'a' && str[i] <= 'z')
			str[i] -= 32;
	}
}

unsigned isofs_sector_read (partition_t *p, unsigned sector, unsigned char *buffer)
{
	/* iso9660 sector is 2kB length */
	unsigned address = sector * 4;

	/* lba28_drive_read () read 512 bytes from drive 4 times +512 bytes */
	lba28_drive_read (p, address, (unsigned char *) buffer);
	lba28_drive_read (p, address+1, (unsigned char *) buffer+512);
	lba28_drive_read (p, address+2, (unsigned char *) buffer+1024);
	lba28_drive_read (p, address+3, (unsigned char *) buffer+1536);

	return 1;
}

isofs_pathtbl_t *isofs_path_find (char *name)
{
	isofs_pathtbl_t *path;
	for (path = path_table_list.next; path != &path_table_list; path = path->next) {
		if (!strcmp (path->name, name))
			return path;
	}

	return 0;
}

unsigned isofs_dir_scan (partition_t *p, char *block)
{
	/*dir = (isofs_dir_t *) (block + dir->rec_len);

	dir = (isofs_dir_t *) (block + dir->rec_len);*/

	unsigned dir_next = 0;
	unsigned entry = 0;

	for (;;) {
		isofs_dir_t *isodir = (isofs_dir_t *) (block + dir_next);

		if (!isodir->rec_len)
			break;

		//kprintf ("dir -> %d:%d:%d:%d:%d - %d - %d - %s\n", isodir->year+1900, isodir->month, isodir->day, isodir->hour, isodir->minute, isodir->rec_len, isodir->id_len, isodir->id);
		/* clear name for prevent */
		memset (dir[entry].name, 0, 11);
		
		/* there are directories and files */
		if (entry > 1) {
			unsigned p = isodir->id[isodir->id_len] ? 0 : 1;
			unsigned l = isodir->id_len;

			if (isodir->id_len > 10)
				l = 10;
			if (!(isodir->flags & ISOFS_DIR_FLAGS_DIR))
				l -= 2;
			if (isodir->id[l-1] == '.')
				l --;

			memcpy (dir[entry].name, isodir->id+isodir->id_len+10+p, l);
		} else {
			// "." and ".." directories are not correctly named in iso9660

			/* "." directory */
			if (entry == 0)
				memcpy (dir[entry].name, ".", 1);
			if (entry == 1)
				memcpy (dir[entry].name, "..", 2);
		}

		/* assign flags */
		if (isodir->flags & ISOFS_DIR_FLAGS_DIR)
			dir[entry].dir = 1;
		else
			dir[entry].dir = 0;

		/*if (!isodir->id[isodir->id_len])
			kprintf (": isodir->rec_len: %d | '%s' | 0x%x\n", isodir->rec_len, isodir->id+isodir->id_len+11, isodir->id[isodir->id_len]);
		else
			kprintf (":1 isodir->rec_len: %d | '%s' | 0x%x\n", isodir->rec_len, isodir->id+isodir->id_len+10, isodir->id[isodir->id_len]);*/

		dir_next += isodir->rec_len;
		entry ++;
	}

	/* for prevent clear last+1 entry of dir[] */
	memset (dir[entry].name, 0, 10);

	return 1;
}

isofs_dir_t *isofs_dir_find (partition_t *p, char *block, char *name)
{
	unsigned dir_next = 0;
	unsigned entry = 0;
	unsigned name_len = strlen (name);

	for (;;) {
		isofs_dir_t *isodir = (isofs_dir_t *) (block + dir_next);

		if (!isodir->rec_len)
			break;

		if (!strncmp (name, isodir->id, name_len))
			return isodir;

		dir_next += isodir->rec_len;
		entry ++;
	}

	return 0;
}

unsigned isofs_path_gettable (partition_t *p)
{
	char block[2049];

	unsigned path_prev = 0;
	unsigned path_num = 0;

	isofs_sector_read (p, 20, (unsigned char *) block);
	
	for (;;) {
		isofs_path_t *path = (isofs_path_t *) ((void *) block + path_prev);

		if (!path)
			return 0;

		if (!path->name_len)
			break;

		/* alloc and init context */
		isofs_pathtbl_t *table = (isofs_pathtbl_t *) kmalloc (sizeof (isofs_pathtbl_t));

		if (!table)
			return 0;

		table->name_len = path->name_len;
		table->sect_num = path->sect_num;
		table->sect_data = path->sect_data;
		table->rec_num = path->rec_num;

		table->name = (unsigned char *) kmalloc (sizeof (unsigned char) * (path->name_len+1));

		if (!table->name)
			return 0;

		memcpy (table->name, &path->name, path->name_len);
		table->name[path->name_len] = '\0';
		//kprintf ("dd: %s : %d\n", table->name, table->sect_data);
		//memcpy (table->path, path, sizeof (isofs_path_t)+path->name_len);
		
		/* add into list */
		table->next = &path_table_list;
		table->prev = path_table_list.prev;
		table->prev->next = table;
		table->next->prev = table;

		path_prev += 8 + path->name_len;

		/* padding */
		if (block[path_prev+path->name_len])
			path_prev ++;

		path_num ++;
	}

	return 1;
}

unsigned isofs_cd_root (partition_t *p)
{
	isofs_pathtbl_t *path;
	for (path = path_table_list.next; path != &path_table_list; path = path->next) {
		/* we need find root dir (ussualy it is first in list) */
		if (path->name_len == 1) {
			char *block = (char *) kmalloc (sizeof (char) * 2049);
		
			if (!block)
				return 0;

			path_prev = path_curr;
			path_curr = path;

			isofs_sector_read (p, path->sect_data, block);
	
			isofs_dir_scan (p, block);

			kfree (block);

			return 1;
		}
	}

	return 0;
}

/* go to next directory placed in current directory */
unsigned isofs_cd (partition_t *p, unsigned char *name)
{
	isofs_pathtbl_t *path;

	if (!strcmp (name, "..")) {
		path = path_prev;

		char *block = (char *) kmalloc (sizeof (char) * 2049);
		
		if (!block)
			return 0;

		path_prev = path_curr;
		path_curr = path;
		
		isofs_sector_read (p, path->sect_data, block);
	
		isofs_dir_scan (p, block);

		kfree (block);

		return 1;
	} 

	toupper (name);

	for (path = path_table_list.next; path != &path_table_list; path = path->next) {
		/* we need to find wanted dir */
		if (!strncmp (path->name, name, path->name_len)) {
			char *block = (char *) kmalloc (sizeof (char) * 2049);
		
			if (!block)
				return 0;

			path_prev = path_curr;
			path_curr = path;

			isofs_sector_read (p, path->sect_data, block);
	
			isofs_dir_scan (p, block);

			kfree (block);

			return 1;
		}
	}

	return 0;
}

unsigned isofs_cat (partition_t *p, unsigned char *name, vfs_content_t *content)
{
	char n[12];

	if (!path_curr)
		return 0;
	
	isofs_pathtbl_t *path = path_curr;

	char block[2049];

	isofs_sector_read (p, path->sect_data, block);

	memcpy (n, name, 11);

	toupper (n);

	isofs_dir_t *isodir = isofs_dir_find (p, block, n);

	if (!isodir)
		return 0;

	if (isodir->flags & ISOFS_DIR_FLAGS_DIR ||
		!isodir->data_sec[0] ||
		!isodir->data_len[0]) {
		return 0;
	}

	/* NOTE: this is prevent before memory corruption (Max file length is ~47KB) */
/*	if (isodir->data_len[0] > 47000) {
		kfree (block);
		file_cache_id = 0;
		return 0;
	}*/

	cache_t *cache = cache_create (0, isodir->data_len[0], 1);

	if (!cache)
		return 0;

	unsigned long len = 0;

	unsigned i = 0;

	char data[2049];

	//if (!data)
	//	return 0;

	for (;;) {
		isofs_sector_read (p, isodir->data_sec[0]+i, data);

		if ((isodir->data_len[0] - len) > 2048)
			cache_add (data, 2048);
		else
			cache_add (data, isodir->data_len[0] - len);
		
		if ((len+2048) > isodir->data_len[0])
			break;

		len += 2048;
		i ++;
	}

	content->ptr = (char *) &cache->data;
	content->len = cache->limit;

	return 1;
}

/* iso9660 filesystem initialization function */
unsigned isofs_init (partition_t *p)
{
	//isofs_volume_t volume;

	//isofs_sector_read (p, ISOFS_SUPERBLOCK_SECTOR, (unsigned char *) &volume);

	//kprintf ("isofs -> %d, %d:%d:%s:%s:%d:%s:%s:%s:%d\n", sizeof (isofs_volume_t), volume->one, volume->zero, volume->sys_id, volume->vol_id, volume->sector_size[0], volume->pub_id, volume->root_dir_rec, volume->zeros, volume->seq_num[0]);

	path_table_list.next = &path_table_list;
	path_table_list.prev = &path_table_list;

	isofs_path_gettable (p);

	path_curr = 0;
	path_prev = 0;

	char *block = (char *) kmalloc (sizeof (char) * 2048);

	if (!block)
		return 0;

	/*isofs_pathtbl_t *path;
	for (path = path_table_list.next; path != &path_table_list; path = path->next) {
		kprintf ("path - %s - %d\n", path->name, path->sect_data);

		isofs_sector_read (p, path->sect_data, block);

		isofs_dir_scan (p, block);
	}*/

	//timer_wait (1000);

	return 1;
}

/* handler which is used by device management and pretty needed
	 for communication between zexos and this filesystem */
bool isofs_handler (unsigned act, char *block, unsigned n, unsigned long l)
{
	switch (act) {
		case FS_ACT_INIT:
		{
			return (bool) isofs_init ((partition_t *) block);
		}
		break;
		case FS_ACT_READ:
		{
			return (bool) isofs_cat (curr_part, dir[n].name, (vfs_content_t *) block);
		}
		break;
		case FS_ACT_WRITE:
		{
			return 0;
		}
		break;
		case FS_ACT_CHDIR:
		{
			if (n == -1)
				return (bool) isofs_cd_root (curr_part);

			/* go to another directory */
			return (bool) isofs_cd (curr_part, dir[n].name);
		}
		break;
		case FS_ACT_MKDIR:
		{
			return 0;
		}
		break;
		case FS_ACT_TOUCH:
		{
			return 0;
		}
		break;
		case FS_ACT_EXIT:
		{
			/* TODO */

			return 0;
		}
		break;
	}

	return 0;
}
#endif
