/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <sound/audio.h>
#include <dev.h>

snd_audio_t snd_audio_list;

/* find audio structure by specified device */
snd_audio_t *audio_find (dev_t *dev)
{
	snd_audio_t *aud;
	for (aud = snd_audio_list.next; aud != &snd_audio_list; aud = aud->next) {
		if (aud->dev == dev)
			return aud;
	}

	return 0;
}

/* open audio sound system (prepare sound card to play) */
snd_audio_t *audio_open (snd_cfg_t *cfg)
{
	if (!cfg->dev || !cfg->rate || !cfg->format)
		return 0;

	dev_t *dev = dev_find (cfg->dev);

	if (!dev)
		return 0;

	if (!(dev->attrib & DEV_ATTR_SOUND))
		return 0;

	/* we dont accept multiple device using yet */
	if (audio_find (dev))
		return 0;

	/* alloc and init context */
	snd_audio_t *aud = (snd_audio_t *) kmalloc (sizeof (snd_audio_t));

	if (!aud)
		return 0;

	aud->dev = dev;
	aud->rate = cfg->rate;
	aud->format = cfg->format;
	aud->channels = cfg->channels;
	aud->flags = cfg->flags;

	printf ("rate: %d f: %d, ch: %d\n", aud->rate, aud->format, aud->channels);

	/* add into list */
	aud->next = &snd_audio_list;
	aud->prev = snd_audio_list.prev;
	aud->prev->next = aud;
	aud->next->prev = aud;

	/* setup sound card */
	dev->handler (DEV_ACT_UPDATE, (void *) aud, sizeof (snd_audio_t));

	return aud;
}

/* write data into device buffer */
int audio_write (snd_audio_t *aud, char *buf, unsigned len)
{
	if (!aud || !buf || !len)
		return -1;

	aud->dev->handler (DEV_ACT_WRITE, buf, len);

	return 0;
}

int audio_close (snd_audio_t *aud)
{
	if (!aud)
		return -1;

	aud->next->prev = aud->prev;
	aud->prev->next = aud->next;

	aud->rate = 0;
	aud->format = 0;
	aud->channels = 0;
	aud->flags = 0;

	aud->dev->handler (DEV_ACT_UPDATE, (void *) aud, sizeof (snd_audio_t));

	kfree (aud);

	return 0;
}

/* init function of sound system */
unsigned int init_audio ()
{
	snd_audio_list.next = &snd_audio_list;
	snd_audio_list.prev = &snd_audio_list;

	return 1;
}
