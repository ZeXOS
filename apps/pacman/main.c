/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>

#define		ESC				1
#define		ARROWLEFT			75
#define		ARROWRIGHT			77
#define		ARROWUP				72
#define		ARROWDOWN			80

#define		MAXBEASTS			5

unsigned short *img_pacman;
unsigned short *img_wall;
unsigned short *img_beast;

int pacman_x;
int pacman_y;
int pacman_rot;
int pacman_rotn;
int pacman_points;
int pacman_maxpoints;

int scancode;

char *level_data;

unsigned char level_num;

typedef struct {
	unsigned short x;
	unsigned short y;
	unsigned char rot;
} beast_t;

beast_t beasts[MAXBEASTS];
unsigned char beasts_count;


/** INIT **/
unsigned short *init_bitmap (char *str)
{
	/* FOLDER bitmap */
	unsigned short *bitmap = (unsigned short *) malloc (2120);

	if (!bitmap)
		return 0;

	memset (bitmap, 0, 2048+70);

	// html web page
	int fd = open (str, O_RDONLY);
	
	if (!fd) {
		puts ("error -> file not found\n");
		return 0;
	}
	
	if (!read (fd, (unsigned char *) bitmap, 2048+70)) {
		puts ("error -> something was wrong !\n");
		return 0;
	}

	//close (fd);

	return bitmap;
}

int init_level ()
{
	level_data = (char *) malloc (26 * 18 + 1);

	if (!level_data)
		return 0;

	memset (level_data, 0, 26 * 18);

	int fd = open ("level", O_RDONLY);
	
	if (!fd) {
		puts ("error -> file 'level' not found\n");
		return 0;
	}
	
	if (!read (fd, (char *) level_data, 26 * 18)) {
		puts ("error -> something was wrong !\n");
		return 0;
	}

	//close (fd);

	img_wall = init_bitmap ("imgwall");

	if (!img_wall)
		return 0;

	level_num = 1;

	return 1;
}

int init_pacman ()
{
	/* Try open image */
	img_pacman = init_bitmap ("imgpac");

	if (!img_pacman)
		return 0;

	pacman_x = 32;
	pacman_y = 32;

	pacman_rot = -1;

	pacman_points = 0;

	return 1;
}

void init_beast ()
{
	/* Try open image */
	img_beast = init_bitmap ("imgobl");

	if (!img_beast)
		return;

	beasts_count = 1;

	pacman_maxpoints = 0;

	unsigned char x;
	unsigned char y;
	unsigned char i = 0;

	for (x = 0; x < 25; x ++) {
		for (y = 0; y < 18; y ++) {
			if (level_data[x + y * 26] == '3') {
				if (i >= beasts_count)
					continue;

				beasts[i].x = x * 32;
				beasts[i].y = y * 32;
				beasts[i].rot = 0;

				i ++;
			} else
			if (level_data[x + y * 26] == '0')
				pacman_maxpoints ++;
		}
	}

}

int init ()
{
	init_pacman ();
	init_level ();
	init_beast ();

	/* Switch to VGA graphics mode */
	xinit ();

	/* Clean screen */
	xcls (0);

	scancode = 0;

	return 1;
}

/** DRAW **/
void draw_bitmap (unsigned short *bitmap, unsigned short x, unsigned short y)
{
	unsigned i = 0;
	unsigned j = 0;
	unsigned k = 0;
	unsigned pix = 0;

	for (i = 0; i < 32*32; i ++) {
		if (k >= 32) {
			j ++;
			k = 0;
		}
		
		pix = (1024-i)+35;

		if (pix >= 1059)
			continue;

		if (bitmap[pix] != (unsigned short) 0)
		if (k <= 32 && j <= 32)
			xpixel ((unsigned) x+k, (unsigned) y+j, bitmap[pix]);

		k ++;
	}
}


void draw_field (unsigned char x, unsigned char y)
{
	draw_bitmap (img_wall, x * 32, y * 32);
}

void draw_point (unsigned char x, unsigned char y)
{
	xline (x * 32 + 11, y * 32 + 10, x * 32 + 20, y * 32 + 10, ~0);
	xrectfill (x * 32 + 10, y * 32 + 11, x * 32 + 21, y * 32 + 20, ~0);
	xline (x * 32 + 11, y * 32 + 21, x * 32 + 20, y * 32 + 21, ~0);
}

void draw_beasts ()
{
	unsigned char i;
	for (i = 0; i < beasts_count; i ++)
		draw_bitmap (img_beast, beasts[i].x, beasts[i].y);
}

void draw_level ()
{
	unsigned char x;
	unsigned char y;

	for (x = 0; x < 25; x ++) {
		for (y = 0; y < 18; y ++) {
			if (level_data[x + y * 26] == '1')
				draw_field (x, y);
			else
			if (level_data[x + y * 26] == '0')
				draw_point (x, y);
		}
	}
}

void draw_pacman ()
{
	/* Draw image to vga buffer */
	draw_bitmap (img_pacman, pacman_x, pacman_y);

	if (pacman_points % 2) {
		if (pacman_rot == 0) {
			xrectfill (pacman_x, pacman_y+11, pacman_x+3, pacman_y+20, 0);
			xrectfill (pacman_x+4, pacman_y+13, pacman_x+7, pacman_y+18, 0);
			xrectfill (pacman_x+8, pacman_y+15, pacman_x+11, pacman_y+16, 0);
		} else if (pacman_rot == 1) {
			xrectfill (pacman_x+29, pacman_y+11, pacman_x+32, pacman_y+20, 0);
			xrectfill (pacman_x+25, pacman_y+13, pacman_x+28, pacman_y+18, 0);
			xrectfill (pacman_x+21, pacman_y+15, pacman_x+24, pacman_y+16, 0);
		} else if (pacman_rot == 2) {
			xrectfill (pacman_x+11, pacman_y, pacman_x+20, pacman_y+3, 0);
			xrectfill (pacman_x+13, pacman_y+4, pacman_x+18, pacman_y+7, 0);
			xrectfill (pacman_x+15, pacman_y+8, pacman_x+16, pacman_y+11, 0);
		} else if (pacman_rot == 3) {
			xrectfill (pacman_x+11, pacman_y+29, pacman_x+20, pacman_y+32, 0);
			xrectfill (pacman_x+13, pacman_y+25, pacman_x+18, pacman_y+28, 0);
			xrectfill (pacman_x+15, pacman_y+21, pacman_x+16, pacman_y+24, 0);
		}
	}
}

int draw ()
{
	draw_pacman ();
	draw_level ();
	draw_beasts ();

	char buf_pts[5];
	itoa (pacman_points, buf_pts, 10);

	xtext_puts (20, 585, ~0, "Points:");
	xtext_puts (80, 585, ~0, buf_pts);

	itoa (pacman_maxpoints, buf_pts, 10);

	xtext_puts (100, 585, ~0, "/");
	xtext_puts (110, 585, ~0, buf_pts);

	itoa (level_num, buf_pts, 10);

	xtext_puts (200, 585, ~0, "Level:");
	xtext_puts (250, 585, ~0, buf_pts);

	return 1;
}

/** UPDATE **/
void level_next ()
{
	level_num ++;

	beasts_count ++;

	pacman_points = 0;

	unsigned char x;
	unsigned char y;
	unsigned char i = 0;

	for (x = 0; x < 25; x ++) {
		for (y = 0; y < 18; y ++) {
			if (level_data[x + y * 26] == '3') {
				if (i >= beasts_count)
					continue;

				beasts[i].x = x * 32;
				beasts[i].y = y * 32;
				beasts[i].rot = 0;

				i ++;
			} else
			if (level_data[x + y * 26] == '2')
				level_data[x + y * 26] = '0';
		}
	}
}

void level_gameover ()
{
	level_num = 1;

	beasts_count = 1;

	pacman_points = 0;

	unsigned char x;
	unsigned char y;
	unsigned char i = 0;

	for (x = 0; x < 25; x ++) {
		for (y = 0; y < 18; y ++) {
			if (level_data[x + y * 26] == '3') {
				if (i >= beasts_count)
					continue;

				beasts[i].x = x * 32;
				beasts[i].y = y * 32;
				beasts[i].rot = 0;

				i ++;
			} else
			if (level_data[x + y * 26] == '2')
				level_data[x + y * 26] = '0';
		}
	}
}

void beast_stop (unsigned char i)
{
	if (beasts[i].rot == 0) {
		beasts[i].x ++;
	} else if (beasts[i].rot == 1) {
		beasts[i].x -= 2;
	} else if (beasts[i].rot == 2) {
		beasts[i].y ++;
	} else if (beasts[i].rot == 3) {
		beasts[i].y -= 2;
	}
}

void pacman_stop ()
{
	if (pacman_rot == 0) {
		pacman_x ++;
	} else if (pacman_rot == 1) {
		pacman_x -= 2;
	} else if (pacman_rot == 2) {
		pacman_y ++;
	} else if (pacman_rot == 3) {
		pacman_y -= 2;
	}

	pacman_rot = -1;
}

int update_pacman ()
{
	if (scancode == ARROWLEFT)
		pacman_rotn = 0;
	else if (scancode == ARROWRIGHT)
		pacman_rotn = 1;
	else if (scancode == ARROWUP)
		pacman_rotn = 2;
	else if (scancode == ARROWDOWN)
		pacman_rotn = 3;

	if (pacman_rot == 0) {
		if (pacman_x > 0)
			pacman_x --;
	} else if (pacman_rot == 1) {
		if (pacman_x < 800-32)
			pacman_x ++;
	} else if (pacman_rot == 2) {
		if (pacman_y > 0)
			pacman_y --;
	} else if (pacman_rot == 3) {
		if (pacman_y < 600-32)
			pacman_y ++;
	}

	if (!(pacman_x % 32) && !(pacman_y % 32))
		pacman_rot = pacman_rotn;

	return 1;
}


int update_beasts ()
{
	unsigned char i;
	for (i = 0; i < beasts_count; i ++) {
		if (beasts[i].rot == 0) {
			if (beasts[i].x > 0)
				beasts[i].x --;
		} else if (beasts[i].rot == 1) {
			if (beasts[i].x < 800-32)
				beasts[i].x ++;
		} else if (beasts[i].rot == 2) {
			if (beasts[i].y > 0)
				beasts[i].y --;
		} else if (beasts[i].rot == 3) {
			if (beasts[i].y < 600-32)
				beasts[i].y ++;
		}

		/* collision with pacman and beasts */
		if (beasts[i].x/32 == pacman_x/32 && beasts[i].y/32 == pacman_y/32)
			level_gameover ();
	}

	return 1;
}


int update ()
{
	scancode = getkey ();

	if (scancode == ESC)
		return -1;

	/* Flip double buffer - show image */
	xfbswap ();

	/* collision */
	unsigned char x;
	unsigned char y;

	for (x = 0; x < 25; x ++) {
		for (y = 0; y < 18; y ++) {
			if (level_data[x + y * 26] == '1') {
				if (pacman_x > x*32 && pacman_y > y*32 && pacman_x < x*32+32 && pacman_y < y*32+32)
					 pacman_stop ();
				else if (pacman_x+31 > x*32 && pacman_y+31 > y*32 && pacman_x+31 < x*32+32 && pacman_y+31 < y*32+32)
					 pacman_stop ();
				else if (pacman_x+31 > x*32 && pacman_y > y*32 && pacman_x < x*32+32 && pacman_y < y*32+32)
					 pacman_stop ();
				else if (pacman_x > x*32 && pacman_y+31 > y*32 && pacman_x < x*32+32 && pacman_y < y*32+32)
					 pacman_stop ();

				unsigned char i;
				for (i = 0; i < beasts_count; i ++) {
					if (beasts[i].x > x*32 && beasts[i].y > y*32 && beasts[i].x < x*32+32 && beasts[i].y < y*32+32) {
						beast_stop (i);
						beasts[i].rot = rand () % 4;
					} else if (beasts[i].x+31 > x*32 && beasts[i].y+31 > y*32 && beasts[i].x+31 < x*32+32 && beasts[i].y+31 < y*32+32) {
						beast_stop (i);
						beasts[i].rot = rand () % 4;
					} else if (beasts[i].x+31 > x*32 && beasts[i].y > y*32 && beasts[i].x < x*32+32 && beasts[i].y < y*32+32) {
						beast_stop (i);
						beasts[i].rot = rand () % 4;
					} else if (beasts[i].x > x*32 && beasts[i].y+31 > y*32 && beasts[i].x < x*32+32 && beasts[i].y < y*32+32) {
						beast_stop (i);
						beasts[i].rot = rand () % 4;
					}
				}
			} else
			if (level_data[x + y * 26] == '0') {
				unsigned p = 0;

				if (pacman_x > x*32 && pacman_y > y*32 && pacman_x < x*32+32 && pacman_y < y*32+32)
					p = 1;
				if (pacman_x+31 > x*32 && pacman_y+31 > y*32 && pacman_x+31 < x*32+32 && pacman_y+31 < y*32+32)
					p = 1;
				if (pacman_x+31 > x*32 && pacman_y > y*32 && pacman_x < x*32+32 && pacman_y < y*32+32)
					p = 1;
				if (pacman_x > x*32 && pacman_y+31 > y*32 && pacman_x < x*32+32 && pacman_y < y*32+32)
					p = 1;

				if (p) {
					pacman_points ++;
					level_data[x + y * 26] = '2';
				}
			}
		}
	}

	update_pacman ();
	update_beasts ();

	if (pacman_points == pacman_maxpoints)
		level_next ();

	return 1;
}

int main (int argc, char **argv)
{
	init ();

	while (1) {
		draw ();

		if (update () == -1)
			break;
	}
  
	free (img_pacman);
	free (img_wall);
	free (img_beast);

	/* Go back to textual mode */
	xexit ();

	/* Exit app */
	return 0;
}
