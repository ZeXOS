/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _INET_H
#define	_INET_H

#include <sys/socket.h>
#include <netinet/in.h>

/* externs */
extern unsigned short htons (unsigned short hostshort);
extern unsigned int htonl (unsigned int hostlong);
extern unsigned short ntohs (unsigned short netshort);
extern unsigned int ntohl (unsigned int netlong);
extern const char *inet_ntop (int af, const void *src, char *dst, socklen_t cnt);
extern int inet_pton (int af, const char *src, void *dst);
extern char *inet_ntoa (struct in_addr in);
extern in_addr_t inet_addr (const char *src);
extern int inet_aton (const char *cp, struct in_addr *inp);

#endif

