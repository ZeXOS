/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "kbd.h"


unsigned char kbd_us[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '=', '\b',	/* Backspace */
  '\t',			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   0,		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/',   0,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};

static char kbd_buffer[KBD_BUFFER_LEN];
static unsigned char kbd_buffer_pos;

unsigned char kbd_getkey ()
{
	if (kbd_buffer_pos > 0)
		kbd_buffer_pos --;
	
	unsigned char c = kbd_buffer[kbd_buffer_pos];
	
	kbd_buffer[kbd_buffer_pos] = '\0';
	
	return c;
}

void kbd_update ()
{
	int scancode = getkey ();

	/*if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;*/
	
	/* KP_DOWN */
	if (scancode >= 128 && kbd_buffer_pos < KBD_BUFFER_LEN) {
		kbd_buffer[kbd_buffer_pos] = kbd_us[scancode - 128];
		kbd_buffer_pos ++;
	}
}

int init_kbd ()
{
	/* set socket "1" to non-blocking */
	/*int oldFlag = fcntl (1, F_GETFL, 0);
	if (fcntl (1, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return -1;
	}*/
	
	kbd_buffer_pos = 0;
	memset (kbd_buffer, 0, KBD_BUFFER_LEN);

	return 0;
}
