/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <build.h>
#include <config.h>
#include <mutex.h>

#ifdef CONFIG_MEM_ZALLOC

#define KMEM_HEAP_ADDR		0x200000	// MEMORY HEAP is placed on 2MB address
#define KMEM_MALLOC_MAGIC	0xebc9		// MALLOC STRUCTURE signature

#define KMEM_MALLOC_STATE_FREE	0xaa		// MEMORY is FREE at moment
#define KMEM_MALLOC_STATE_USED	0xcc		// MEMORY is USED at moment


typedef struct {
	unsigned magic : 16;
	unsigned state : 8;
	void *next;
} __attribute__ ((__packed__)) malloc_t;

typedef struct {
	void *addr;		/* heap structure is covered by own heap structure address */
	void *first;		/* first malloc structure */
	void *last;		/* last malloc structure - usefully for calculate memory usage */
	void *curr;		/* current malloc structure */
} __attribute__ ((__packed__)) heap_t;


heap_t *heap;			/* heap structure of memory */
MUTEX_CREATE (mutex_malloc);
MUTEX_CREATE (mutex_free);

void *kmalloc (size_t size)
{
	if (!size)
		return 0;

	mutex_lock (&mutex_malloc);

	unsigned malloc_len = sizeof (malloc_t);		/* check malloc structure size */
	unsigned need_alloc = 0;

	/* let's find new or free malloc structure in memory */

	malloc_t *malloc = (malloc_t *) heap->first;

	for (;;) {
		malloc = (malloc_t *) malloc->next;

		//DPRINT ("malloc: 0x%x\n", malloc);

		if (!malloc) {
			DPRINT (DBG_MM | DBG_KMEM, "!malloc");
			need_alloc = 1;
			break;
		}

		if (malloc->magic != KMEM_MALLOC_MAGIC) {
			DPRINT (DBG_MM | DBG_KMEM, "malloc->magic != KMEM_MALLOC_MAGIC");
			need_alloc = 1;
			break;
		}

		if (malloc->state == KMEM_MALLOC_STATE_USED) {	/* It's OK */
			//DPRINT ("DBG_MM | DBG_KMEM, malloc->state == KMEM_MALLOC_STATE_USED");
			//need_alloc = 1;
			continue;
		}

		/* we've found FREE memory block, at moment it is not used */
		if (malloc->state == KMEM_MALLOC_STATE_FREE) {
			/* read next malloc structure, right behind free block */
			malloc_t *next = (malloc_t *) malloc;

			DPRINT (DBG_MM | DBG_KMEM, "malloc->state == KMEM_MALLOC_STATE_FREE");

			while (1) {
				//kprintf ("hhhe\n");
				next = (malloc_t *) next->next;

				if (next->magic != KMEM_MALLOC_MAGIC) {
					DPRINT (DBG_MM | DBG_KMEM, "next->magic != KMEM_MALLOC_MAGIC");
					need_alloc = 1;
					break;
				}

				if (next->state == KMEM_MALLOC_STATE_USED)
					break;

				if (next->state != KMEM_MALLOC_STATE_FREE)
					break;
			}

			if (need_alloc)
				break;

			unsigned diff = (unsigned) ((void *) next - (void *) malloc);

			DPRINT (DBG_MM | DBG_KMEM, "diff: %d", diff);

			if (diff < size)
				continue;

			break;
		}
	}

	if (need_alloc) {
		/* when old malloc structures are full, let's grab new block of memory */
		malloc = heap->curr;

		malloc->magic = KMEM_MALLOC_MAGIC;
		
		malloc->next = (void *) ((void *) malloc + malloc_len + size);
	}

	malloc->state = KMEM_MALLOC_STATE_USED;

	heap->curr = malloc->next;

	/* save last malloc structure pointer to heap */
	if ((unsigned) heap->curr > (unsigned) heap->last)
		heap->last = heap->curr;

	DPRINT (DBG_MM | DBG_KMEM, "malloc: 0x%x", malloc);

	//kprintf ("kmalloc: 0x%x : %d\n", ((void *) malloc + malloc_len), size);

	mutex_unlock (&mutex_malloc);

	return (void *) ((void *) malloc + malloc_len);
}

void *krealloc (void *ptr, size_t size)
{
	return 0;
}

void kfree (void *ptr)
{
	if (!ptr)
		return;
 
	mutex_lock (&mutex_free);

	/* we need find malloc structure - it could be risk working with lower memory address */
	malloc_t *malloc = (void *) ((void *) ptr - sizeof (malloc_t));

	/* check malloc signature, when not agree, ptr pointing to wrong memory address */
	if (malloc->magic != KMEM_MALLOC_MAGIC) {
		kprintf ("free: malloc->magic != KMEM_MALLOC_MAGIC\n");
		return;
	}

	if (malloc->state == KMEM_MALLOC_STATE_FREE) {
		kprintf ("kfree () - memory is FREE - double free !\n");
		return;
	}

	if (malloc->state != KMEM_MALLOC_STATE_USED) {
		kprintf ("malloc->state != KMEM_MALLOC_STATE_USED\n");
		return;
	}

	/* match malloc as FREE */
	malloc->state = KMEM_MALLOC_STATE_FREE;

	mutex_unlock (&mutex_free);
}

unsigned kmem_init ()
{
	  heap = (heap_t *) KMEM_HEAP_ADDR;

	  /* we rather clear heap structure */
	  heap->addr = (void *) heap;
	  heap->first = (void *) ((void *) heap + sizeof (heap_t)); 	/* first malloc structure should be right behind heap structure */
	  heap->last = heap->first;					/* last malloc structure - it is first at init */
	  heap->curr = heap->first;					/* point to first malloc structure - well, it is clear and not used yet */

	  return 1;
}


unsigned short kmem_get ()
{
#ifdef ARCH_i386
        register unsigned long *mem;
        unsigned long mem_count, a, mem_end, bse_end;
        unsigned short memkb;
        unsigned char irq1, irq2;
        unsigned long cr0;

        /* save IRQ's */
        irq1 = inb (0x21);
        irq2 = inb (0xA1);

        /* kill all irq's */
        outb (0x21, 0xFF);
        outb (0xA1, 0xFF);

        mem_count = 0;
        memkb = 0;

        // store a copy of CR0
        asm volatile ("movl %%cr0, %%eax;" : "=a"(cr0));

        // invalidate the cache
        // write-back and invalidate the cache
        asm volatile ("wbinvd;");

        // plug cr0 with just PE/CD/NW
        // cache disable(486+), no-writeback(486+), 32bit mode(386+)
        asm volatile ("movl %%eax, %%cr0;" ::
                             "a" (cr0 | 0x00000001 | 0x40000000 | 0x20000000));

        do {
                memkb ++;
                mem_count += 1024*1024;
                mem = (unsigned long *) mem_count;

                a = *mem;

                *mem = 0x55AA55AA;

                /* the empty asm calls tell gcc not to rely on whats in its registers
                   as saved variables (this gets us around GCC optimisations) */
                asm (";" ::: "memory");
                if (*mem != 0x55AA55AA)
                        mem_count = 0;
                else
                {
                        *mem = 0xAA55AA55;
                        asm ("":::"memory");
                        if (*mem!=0xAA55AA55)
                                mem_count = 0;
                }

                asm (";":::"memory");
                *mem = a;
        } while (memkb < 4096 && mem_count != 0);

        asm volatile ("movl %%eax, %%cr0;" :: "a" (cr0));

        mem_end = memkb << 20;
        mem = (unsigned long *) 0x413;
        bse_end = ((*mem) &0xFFFF) << 6;

        outb (0x21, irq1);
        outb (0xA1, irq2);

	return memkb;
#endif
	return 0;
}

void util_cmdfree ()
{
	unsigned used = (unsigned) heap->last - (unsigned) heap->first;
	used /= 1024;	// convert value to kB size

	printf ("\ttotal\tused\tfree\tshared\tbuffers\tcached\n");
	printf ("Mem: \t%u\t%u\t%u\t%u\t%u\t%u\n", 
		mem_ext * 1024, used, mem_ext * 1024 - used, 0, 0, 0);
}

/*****************************************************************************
*****************************************************************************/
unsigned int init_mm ()
{
	if (!kmem_init ())
		return 0;

	//mem_ext = kmem_get ();	// get extended memory (in Mb)

	pmem_init ();

	return 1;
}

#endif
