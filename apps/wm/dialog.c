/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "dialog.h"
#include "cursor.h"

wmdialog wmdialog_list;

xbitmap *dialog_img;

extern wmcursor *cursor;

wmdialog *dialog_create (unsigned x, unsigned y, unsigned size_x, unsigned size_y, const char *caption)
{
	wmdialog *dialog = (wmdialog *) malloc (sizeof (wmdialog));

	if (!dialog)
		return 0;

	dialog->x = x;
	dialog->y = y;
	dialog->size_x = size_x;
	dialog->size_y = size_y;
	dialog->active = 1;

	unsigned l = strlen (caption);
	if (l) {
		dialog->caption = (char *) malloc (sizeof (char) * l + 1);
		memcpy (dialog->caption, caption, l);
		dialog->caption[l] = '\0';
	} else
		dialog->caption = 0;

	/* add into list */
	dialog->next = &wmdialog_list;
	dialog->prev = wmdialog_list.prev;
	dialog->prev->next = dialog;
	dialog->next->prev = dialog;

	return dialog;
}

unsigned dialog_delete (wmdialog *dialog)
{
	if (!dialog)
		return 0;

	dialog->active = 0;

	if (dialog->caption)
		free (dialog->caption);

	/* delete from list */
	dialog->next->prev = dialog->prev;
	dialog->prev->next = dialog->next;

	free (dialog);

	return 1;
}

unsigned dialog_draw (wmdialog *dialog)
{
	if (!dialog)
		return 0;

	/* 	50 - zluta 
		60 - cervena
		70 - cerna
		30 - vybledle zluta
		35 - seda
		40 - ruzova
		45 - fialova
		55 - jasne zluta
		15 - jasne modra
		18 - jasne razici zelena
		1 - modra
		3 - trochu tmavsi seda
		4 - tmavsi cervena
		5 - fialova
		6 - zluto-hneda
		7 - jasne seda
		8 - tmave modra
		9 - pekna modra
		27 - biele-modra
		
	*/

	xrect (dialog->x, dialog->y, dialog->x+dialog->size_x, dialog->y+dialog->size_y, 0xd3d3d3);

	if (dialog->active) {
		xline (dialog->x+1, dialog->y+1, dialog->x+dialog->size_x-1, dialog->y+1, 190*256);
		xline (dialog->x+1, dialog->y+2, dialog->x+dialog->size_x-1, dialog->y+2, 190*256+4);
		xline (dialog->x+1, dialog->y+3, dialog->x+dialog->size_x-1, dialog->y+3, 190*256+8);
		xline (dialog->x+1, dialog->y+4, dialog->x+dialog->size_x-1, dialog->y+4, 190*256+12);
		xline (dialog->x+1, dialog->y+5, dialog->x+dialog->size_x-1, dialog->y+5, 190*256+15);
		xline (dialog->x+1, dialog->y+6, dialog->x+dialog->size_x-1, dialog->y+6, 190*256+17);
		xline (dialog->x+1, dialog->y+7, dialog->x+dialog->size_x-1, dialog->y+7, 190*256+20);
		xline (dialog->x+1, dialog->y+8, dialog->x+dialog->size_x-1, dialog->y+8, 190*256+23);
		xline (dialog->x+1, dialog->y+9, dialog->x+dialog->size_x-1, dialog->y+9, 190*256+26);
		xline (dialog->x+1, dialog->y+10, dialog->x+dialog->size_x-1, dialog->y+10, 190*256+28);
	} else
		xrectfill (dialog->x+1, dialog->y+1, dialog->x+dialog->size_x-1, dialog->y+10, 1*256);

	xrectfill (dialog->x+1, dialog->y+11, dialog->x+dialog->size_x-1, dialog->y+dialog->size_y-1, 0x00AEF8);

	if (dialog->caption)
		xtext_puts (dialog->x+5, dialog->y+2, 0, dialog->caption);

	return 1;
}

unsigned dialog_draw_all ()
{
	wmdialog *dialog;
	for (dialog = wmdialog_list.next; dialog != &wmdialog_list; dialog = dialog->next) {
		if (!dialog)
			continue;

		if (cursor->action)
		if (cursor->state != XCURSOR_STATE_LBUTTON) {
			if (dialog->flags & DIALOG_FLAG_DRAG) {
				dialog->flags &= ~DIALOG_FLAG_DRAG;
				cursor->action = 0;
			}
		}

		/* mame mys na zahlavi okna ? */
		if (cursor->x > (signed) dialog->x && cursor->x < (signed) dialog->x+(signed) dialog->size_x && 
			cursor->y > (signed) dialog->y && cursor->y <= (signed) dialog->y+11) {

			if (!cursor->action)
 			if (cursor->state == XCURSOR_STATE_LBUTTON) {

				if (!(dialog->flags & DIALOG_FLAG_DRAG)) {
					dialog->flags |= DIALOG_FLAG_DRAG;
					cursor->action = 1;
				}
			}

			if (cursor->state == XCURSOR_STATE_RBUTTON) {
				if (dialog_delete (dialog))
					return 1;
			}
		}

		if (dialog->flags & DIALOG_FLAG_DRAG) {
			dialog->x = cursor->x;
			dialog->y = cursor->y;
		}

		dialog_draw (dialog);
	}

	return 1;
}

unsigned init_dialog ()
{
	wmdialog_list.next = &wmdialog_list;
	wmdialog_list.prev = &wmdialog_list;

	return 1;
}
