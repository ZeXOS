/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PTHREAD_H
#define _PTHREAD_H

#define	SYSV_THREADOPEN		(unsigned *) 0x9054
#define	SYSV_THREADCLOSE	(unsigned *) 0x9058

typedef struct {
	unsigned flags; 
} pthread_attr_t;

typedef struct {
	void *(*start_routine) (void *);
	void *arg;
	pthread_attr_t *attr;
} pthread_t;

extern int pthread_create (pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);
extern int pthread_destroy ();

#endif
