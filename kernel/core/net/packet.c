/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <system.h>
#include <string.h>
#include <dev.h>
#include <net/eth.h>
#include <net/net.h>
#include <net/if.h>
#include <net/packet.h>
#include <task.h>
#include <mutex.h>

MUTEX_CREATE (mutex_packet);

extern netif_t netif_list;
extern unsigned long timer_ticks;

static task_t *task_netcore = NULL;

unsigned net_packet_send (netif_t *netif, packet_t *packet, char *buf, unsigned len)
{
	mutex_lock (&mutex_packet);

	if (!netif) {
		mutex_unlock (&mutex_packet);
		return 0;
	}

	memcpy (netif->buf_tx, (char *) packet, 14);
	memcpy (netif->buf_tx+14, buf, len);

	netif->dev->write ((char *) netif->buf_tx, 14+len);

	/* add transfered bytes to stats */
	netif->dev->info_tx += 14+len;

	mutex_unlock (&mutex_packet);

	return 1;
}

/* Here we process a received packets */
unsigned net_packet_handler (char *buf, unsigned len)
{
	packet_t *packet = (packet_t *) buf;

	if (!packet)
		return 0;

	//printf ("net_packet->type: 0x%04x\n", swap16 (packet->type));

	switch (packet->type) {
#ifdef CONFIG_PROTO_IPV4
		case NET_PACKET_TYPE_IPV4:
			return net_proto_ip_handler (packet, buf+sizeof (packet_t), len-sizeof (packet_t));
#endif
		case NET_PACKET_TYPE_ARP:
			return net_proto_arp_handler (packet, buf+sizeof (packet_t), len-sizeof (packet_t));
#ifdef CONFIG_PROTO_IPV6
		case NET_PACKET_TYPE_IPV6:
			return net_proto_ipv6_handler (packet, buf+sizeof (packet_t), len-sizeof (packet_t));
#endif
	}

	return 1;
}

/* Task for checking new incoming packets */
unsigned task_net_packet ()
{
	/* let's find network device */
	netif_t *netif;
	for (netif = netif_list.next; netif != &netif_list; netif = netif->next) {
		if (!netif) {
			task_done (task_netcore);
			schedule ();
			return 0;
		}

		/* alloc 2k packet receive and transfer buffer */
		netif->buf_rx = (char *) kmalloc (2048 * sizeof (char));
		netif->buf_tx = (char *) kmalloc (2048 * sizeof (char));

		if (!netif->buf_rx || !netif->buf_tx) {
			task_done (task_netcore);
			schedule ();
			return 0;
		}
	}

	/* thread loop */
	for (;; schedule ()) {
		for (netif = netif_list.next; netif != &netif_list; netif = netif->next) {
			/* check for incoming data */
			int ret = netdev_rx (netif->dev, netif->buf_rx, 2048);
		
			/* are there some data ? */
			if (ret) {
				net_packet_handler (netif->buf_rx, ret);

				/* clear buffer */
				memset (netif->buf_rx, 0, 2048);
			}
		}
	}

	/* free tx and rx buffers for all ethernet devices */
	for (netif = netif_list.next; netif != &netif_list; netif = netif->next) {
		kfree (netif->buf_tx);
		kfree (netif->buf_rx);
	}

	task_done (task_netcore);
	schedule ();

	return 1;
}


unsigned init_packet ()
{
	/* initialize unix domain sockets - we could use it without any network devices */
	if (!init_net_proto_unix ())
		return 0;

	/* setup hostname of the current machine */
	if (!init_hostname ())
		return 0;
	
	/* let's find network device first */
	netif_t *netif = netif_findbyname ("eth0"); /* TODO: all interfaces */

	/* without one ethernet device it does not matter on netstack */
	if (!netif)
		return 1;

	if (!init_net_proto_tcp ())
		return 0;

	if (!init_net_proto_tcp6 ())
		return 0;

	if (!init_net_proto_udp ())
		return 0;

	if (!init_net_proto_udp6 ())
		return 0;

	if (!init_net_proto_dns ())
		return 0;

	if (!init_net_proto_tun6 ())
		return 0;

	/* start new task/thread for network core */
	task_netcore = (task_t *) task_create ("netcore", (unsigned) task_net_packet, 255);

	if (!task_netcore)
		return 0;

	return 1;
}
