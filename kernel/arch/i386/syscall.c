/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Martin 'povik' Poviser (martin.povik@gmail.com)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <tty.h>
#include <proc.h>
#include <net/socket.h>
#include <file.h>
#include <signal.h>
#include <module.h>
#include <rs232.h>
#include <ioctl.h>
#include <fd.h>
#include <paging.h>
#include <pipe.h>
#include <errno.h>
#include <sound/audio.h>
#include <select.h>
#include <syscall.h>

void sys_exit (struct regs *r)
{
	task_t *taskc = task_find (0);

	if (!taskc)
		return;

	page_dir_switch (taskc->page_cover->page_dir);

	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	task_t *task = proc->tty->task;

	proc->pid = 0;

	if (int_disable ())
		longjmp (task->state, 1);
	else
		for (;;)
			schedule ();
}

void sys_getch (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->tty != currtty)
		return;

	unsigned char c = (unsigned char) getkey ();

	if (c)
		setkey (0);

        *SYSV_GETCH = (unsigned) c;
}


void sys_sleep (struct regs *r)
{
	unsigned long timer_start = timer_ticks + ((unsigned) r->ebx * 1000);

	while (timer_start > timer_ticks)
		schedule ();
}

void sys_putch (struct regs *r)
{ 
	//page_dir_switch (taskc->page_cover->page_dir);

	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	int c = r->ebx;
	
	tty_putnch (proc->tty, c);

	//page_dir_switch (proc->task->page_cover->page_dir);
}

void sys_color (struct regs *r)
{
	video_color (r->ebx, r->ecx);
}


void sys_cls (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->tty != currtty)
		return;

	/* NOTE: Why we should disable interrupts ? Nobody know, but works better */
	if (int_disable ()) {
		tty_cls (proc->tty);
		int_enable ();
	}
}

void sys_getkey (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->tty != currtty)
		return;

	*SYSV_GETKEY = (unsigned) scancode;

	scancode = 0;
}

void sys_gotoxy (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	tty_gotoxy (proc->tty, r->ebx, r->ecx);
}

void sys_fork (struct regs *r)
{
	*SYSV_FORK = fork ();
}

void sys_schedule (struct regs *r)
{
	schedule ();
}

void sys_write (struct regs *r)
{
	unsigned len = r->ecx;
	unsigned fd = r->edx;

	unsigned char *buf = (unsigned char *) r->ebx;

	if (!buf)
		return;

	int ret = 0;

	proc_t *proc = 0;
	char *mem = 0;

	switch (fd) {
		case 0:
			proc = proc_find (_curr_task);

			if (!proc) {
				ret = -1;
				break;
			}

			mem = (char *) buf;

			//printf ("data: %s | 0x%x | %d\n", mem, proc->data, len);
			
			tty_write (proc->tty, (char *) mem, len);

			ret = len;

			break;
		case 1:
			proc = proc_find (_curr_task);

			if (!proc) {
				ret = -1;
				break;
			}

			if (len > KBD_MAX_QUAUE)
				len = KBD_MAX_QUAUE;

			mem = (char *) buf;

			int i = 0;
			for (i = len; i >= 0; i --)
				setkey (buf[i]);

			//mem[len] = '\0';

			ret = len;
			break;
		default:
			mem = (char *) buf;

			ret = write (fd, (char *) mem, len);
			break;
	}

        *SYSV_WRITE = ret;
}

void sys_socket (struct regs *r)
{
	*SYSV_SOCKET = socket (r->ebx, r->ecx, r->edx);
}

void sys_connect (struct regs *r)
{
	sockaddr *addr = (sockaddr *) r->ebx;

	*SYSV_CONNECT = connect (r->ecx, addr, r->edx);
}

void sys_malloc (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);
		
	paging_disable ();
        r->eax = (unsigned) umalloc (proc, (int) r->ebx);
	paging_enable ();

	DPRINT (DBG_SYSCALL, "malloc (): 0x%x", r->eax);
}

void sys_send (struct regs *r)
{
	*SYSV_SEND = send ((int) r->ecx, (char *) r->ebx, (unsigned) r->edx, 0);
}

void sys_recv (struct regs *r)
{
	unsigned char *msg = (unsigned char *) r->ebx;
	unsigned size = (unsigned) r->edx;
	int fd = (int) r->ecx;

	*SYSV_RECV = recv (fd, (char *) msg, size, 0);
}

void sys_close (struct regs *r)
{
	close (r->ebx);
}

void sys_open (struct regs *r)
{
	*SYSV_OPEN = open ((char *) r->ebx, (unsigned) r->ecx);
}

void sys_pcspk (struct regs *r)
{
	dev_t *dev = dev_find ("/dev/pcspk");

	if (dev)
		dev->handler (DEV_ACT_PLAY, (unsigned) r->ebx);
}

void sys_usleep (struct regs *r)
{
	//unsigned long timer_start = timer_ticks + ((unsigned) r->ebx);

	//while (timer_start > timer_ticks)
	//	schedule ();
	usleep ((unsigned) r->ebx);
}

void sys_read (struct regs *r)
{
	unsigned fd = r->ecx;
	unsigned len = r->edx;

	unsigned char *buf = (unsigned char *) r->ebx;

	int ret = 0;

	proc_t *proc = 0;

	switch (fd) {
		case 0:
			break;
		case 1:
			proc = proc_find (_curr_task);
				
			if (!proc)
				break;

			//if (proc->tty != currtty)
			//	return;
			
			ret = tty_read (proc->tty, (char *) buf, len);
			break;
		default:
			ret = read (fd, (char *) buf, len);
			break;
	}

        *SYSV_READ = ret;
}

void sys_time (struct regs *r)
{
	time_t *memptr = (time_t *) SYSV_TIME;

	tm *t = rtc_getcurrtime ();

	if (t)
		*memptr = (time_t) t->__tm_gmtoff;
}

void sys_system (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	task_t *oldtask = _curr_task;

	unsigned char *cmd = (unsigned char *) r->ebx;

	/*paging_disable ();
	page_dir_switch (task->page_cover->page_dir);
	paging_enable ();
	
	_curr_task = task;
	schedule ();*/

	command_parser ((char *) cmd, strlen (cmd));
	
	page_dir_switch (oldtask->page_cover->page_dir);
	
	/* needed for correct scheduling (when you execute new process, _curr_task is tasj of current tty) */
	 _curr_task = oldtask;

	schedule ();

}

void sys_chdir (struct regs *r)
{
	char *dir = (unsigned char *) r->ebx;

        *SYSV_CHDIR = vfs_cd (dir, strlen (dir)) ? 0 : -1;
}

void sys_getdir (struct regs *r)
{
	r->eax = (unsigned) vfs_dirent ();
}

void sys_procarg (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	switch (r->ecx) {
		case 0:
			r->eax = (unsigned) proc->argv;
			break;
		case 1:
			r->eax = (unsigned) proc->argc;
			break;
		default:
			r->eax = 0;
	}
}

void sys_signal (struct regs *r)
{
	signal (r->ebx, (sighandler_t) r->ecx);
}

void sys_mount (struct regs *r)
{
	partition_t *p = partition_find ((char *) r->ebx);

	if (p) {
		mount (p, "", (char *) r->ecx);
		r->eax = 1;
	} else
		r->eax = 0;
}

void sys_kputs (struct regs *r)
{
	int_disable ();

	module_t *kmod = module_find (_curr_task);

	if (!kmod)
		return;

	if (kmod->task != _curr_task)
		return;

	unsigned char *buf = (unsigned char *) r->ebx;
	kprintf (buf);

	//kprintf ("yeah, sys_kputs: '%s' '0x%x'\n", buf, buf);

	int_enable ();
}

void sys_bind (struct regs *r)
{
        *SYSV_BIND = bind (r->ecx, (sockaddr *) r->ebx, r->edx);
}

void sys_listen (struct regs *r)
{
	*SYSV_LISTEN = listen (r->ebx, r->ecx);
}

void sys_accept (struct regs *r)
{
	*SYSV_ACCEPT = accept (r->ecx, r->ebx, r->edx);
}

void sys_fcntl (struct regs *r)
{
	*SYSV_FCNTL = fcntl (r->ebx, r->ecx, r->edx);
}

void sys_gvgafb (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (vgagui != 2) {
		printf ("Please start this program in graphical VESA mode\n");
		sys_exit (r);
		return;
	}

	*SYSV_GVGAFB = (unsigned) init_vgafb ();
}

void sys_gcls (struct regs *r)
{
	//gcls (r->ebx);
}

void sys_gfbswap (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->tty == currtty)
		video_gfx_fbswap ();

	schedule ();
}

void sys_rs232read (struct regs *r)
{
	*SYSV_RS232READ = (int) rs232_read ();
}

void sys_rs232write (struct regs *r)
{
	rs232_write ((char) r->ebx);
}

void sys_gttyexit (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	tty_change (proc->tty);
}

void sys_gexit (struct regs *r)
{
	video_gfx_exit ();
}

void sys_gttyinit (struct regs *r)
{
	tty_t *tty = gtty_init ();

	if (!tty)
		return;

	r->eax = (unsigned) &tty->screen;
}

void sys_getenv (struct regs *r)
{
	char *name = (char *) r->ebx;
	
	char *val = (char *) env_get (name);

	r->eax = (unsigned) val;
}

void sys_free (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	paging_disable ();

        ufree (proc, (void *) r->ebx);

	DPRINT (DBG_SYSCALL, "free (): 0x%x", r->ebx);

	paging_enable ();
}

void sys_realloc (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	paging_disable ();

        r->eax = (unsigned) urealloc (proc, (void *) r->ebx, (size_t) r->ecx);

	DPRINT (DBG_SYSCALL, "realloc (): 0x%x", r->eax);

	paging_enable ();
}

void sys_gethostbyname (struct regs *r)
{
	char *buf = (char *) r->ebx;

	if (!buf) {
		r->eax = 0;
		return;
	}

	r->eax = (unsigned) gethostbyname (buf);
}

void sys_vfsent (struct regs *r)
{
	char *pathname = (char *) r->ebx;

	r->eax = (unsigned) vfs_find (pathname, strlen (pathname));
}

void sys_sendto (struct regs *r)
{
	/* HACK: bleeh, there is problem with fourth argument,
		so we need to use another way to getting address */
	sockaddr_in *to = (sockaddr_in *) *SYSV_SENDTO;

	*SYSV_SENDTO = sendto ((int) r->ecx, (char *) r->ebx, (unsigned) r->edx, 0, to, 0);
}

void sys_recvfrom (struct regs *r)
{
	unsigned char *msg = (unsigned char *) r->ebx;

	/* HACK: bleeh, there is problem with fourth argument,
		so we need to use another way to getting address */
	sockaddr_in *from = (sockaddr_in *) *SYSV_RECVFROM;

	*SYSV_RECVFROM = recvfrom ((int) r->ecx, (char *) msg, (unsigned) r->edx, 0, from, 0);
}

void sys_getchar (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	int i = 1;
	unsigned char s = '\n';

	int *memptr = SYSV_GETCHAR;

	/* non-blocking mode */
	if (stdin->flags & O_NONBLOCK) {
		if (!proc)
			return;

		if (proc->tty != currtty)
			return;

		unsigned char c = getkey ();

		if (!c) {
			*memptr = -1;
			return;
		}

		tty_putnch (proc->tty, c);

		*memptr = c;

		return;
	}

	/* blocking - clasical mode */
	for (;;) {
		if (!proc)
			return;

		if (proc->tty != currtty)
			return;

		char c = getkey ();

		if (c) {
			if (i > 0 && c != '\b')
				tty_write (proc->tty, &c, 1);

			if (c == '\n')
				break;
			else if (i == 1)
				s = c;

			if (c == '\b') {
				if (i > 1) {
					i --;
					tty_write (proc->tty, "\b", 1);
				}
			} else
				i ++;
		}
	}

	*memptr = (int) s;
}

void sys_threadopen (struct regs *r)
{
	void *ptr = (void *) r->ebx;

	unsigned m = *SYSV_THREADOPEN;
	
	*SYSV_THREADOPEN = proc_thread_create (ptr, (void *) m);
}

void sys_threadclose (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	task_t *task = _curr_task;

	/* free child process thread */
	*SYSV_THREADCLOSE = proc_thread_destroy (proc, task);

	task = proc->tty->task;

	if (int_disable ())
		longjmp (task->state, 1);
	else
		for (;;)
			schedule ();
}

void sys_ioctl (struct regs *r)
{
	void *s = (void *) r->ebx;
	unsigned id = r->ecx;
	int l = r->edx;

	*SYSV_IOCTL = ioctl_call (id, s, l);
}

void sys_getpid (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	r->eax = (int) &proc->pid;
}

void sys_lseek (struct regs *r)
{
	int fd = r->ebx;
	long offset = r->ecx;
	int whence = r->edx;

	*SYSV_LSEEK = lseek (fd, offset, whence);
}

void sys_pipe (struct regs *r)
{
	int *p = (int *) r->ebx;
	
	int ret = pipe (p);

	r->eax = (int) &ret;
}

void sys_creat (struct regs *r)
{
	char *file = (char *) r->ebx;
	unsigned mode = r->ecx;	/* TODO: mode */

	int ret = -1;

	if (file) {
		if (mode == O_CREAT)
			ret = (touch (file) == 1) ? 0 : -1;
	} else
		errno_set (EFAULT);

	r->eax = (int) &ret;
}

void sys_mkdir (struct regs *r)
{
	char *dir = (char *) r->ebx;
	unsigned mode = r->ecx;	/* TODO: mode */

	int ret = -1;

	if (dir) {
		if (mode)
			ret = (mkdir (dir) == 1) ? 0 : -1;
	} else
		errno_set (EFAULT);

	r->eax = (int) &ret;
}

void sys_rmdir (struct regs *r)
{
	char *dir = (char *) r->ebx;

	int ret = -1;

	if (dir) {
		ret = (rm (dir) == 1) ? 0 : -1;
	} else
		errno_set (EFAULT);

	r->eax = (int) &ret;
}

void sys_sndopen (struct regs *r)
{
	snd_cfg_t *cfg = (snd_cfg_t *) r->ebx;

	int ret = 0;

	if (!cfg)
		r->eax = (int) &ret;
	else
		r->eax = (int) audio_open (cfg);
}

void sys_sndclose (struct regs *r)
{
	snd_audio_t *aud = (snd_audio_t *) r->ebx;

	int ret = -1;

	if (!aud)
		r->eax = (int) &ret;
	else
		r->eax = (int) audio_close (aud);
}

void sys_sndwrite (struct regs *r)
{
	snd_audio_t *aud = (snd_audio_t *) r->ebx;
	char *buf = (char *) r->ecx;
	unsigned len = (unsigned) r->edx;

	int ret = -1;

	if (!aud)
		r->eax = (int) &ret;
	else
		r->eax = (int) audio_write (aud, buf, len);
}

void sys_remove (struct regs *r)
{
	char *file = (char *) r->ebx;

	int ret = -1;

	if (file) {
		ret = (vfs_rm (file, strlen (file)) == 1) ? 0 : -1;
	} else
		errno_set (EFAULT);

	r->eax = (int) &ret;
}

void sys_rename (struct regs *r)
{
	char *file = (char *) r->ebx;
	char *file2 = (char *) r->ecx;
	
	int ret = 0;

	if (file) {
		if (cp (file, file2) != 1)
			ret = -1;
		
		if (!ret && rm (file) != 1)
			ret = -1;
	} else
		errno_set (EFAULT);

	r->eax = (int) &ret;
}

void sys_select (struct regs *r)
{
	struct select_t {
		int nfds;
		fd_set *readfds;
		fd_set *writefds;
		fd_set *exceptfds;
		struct timeval *timeout;
	};
   
	struct select_t *s = (struct select_t *) r->ebx;
	
	*SYSV_SELECT = select (s->nfds, s->readfds, s->writefds, s->exceptfds, s->timeout);
}

void syscall_handler (struct regs *r)
{
	switch (r->eax) {
		case 1:
			return sys_exit (r);
		case 2:
			return sys_getch (r);
		case 3:
			return sys_sleep (r);
		case 4:
			return sys_putch (r);
		case 5:
			return sys_color (r);
		case 6:
			return sys_cls (r);
		case 7:
			return sys_getkey (r);
		case 8:
			return sys_gotoxy (r);
		case 9:
			return sys_fork (r);
		case 10:
			return sys_schedule (r);
		case 11:
			return sys_write (r);
		case 12:
			return sys_socket (r);
		case 13:
			return sys_connect (r);
		case 14:
			return sys_malloc (r);
		case 15:
			return sys_send (r);
		case 16:
			return sys_recv (r);
		case 17:
			return sys_close (r);
		case 18:
			return sys_open (r);
		case 19:
			return sys_pcspk (r);
		case 20:
			return sys_usleep (r);
		case 21:
			return sys_read (r);
		case 22:
			return sys_time (r);
		case 23:
			return sys_system (r);
		case 24:
			return sys_chdir (r);
		case 25:
			return sys_getdir (r);
		case 26:
			return sys_procarg (r);
		case 27:
			return sys_signal (r);
		case 28:
			return sys_mount (r);
		case 29:
			return sys_kputs (r);
		case 30:
			return sys_bind (r);
		case 31:
			return sys_listen (r);
		case 32:
			return sys_accept (r);
		case 33:
			return sys_fcntl (r);
		case 34:
			return sys_gvgafb (r);
		case 35:
			return sys_gcls (r);
		case 36:
			return sys_gfbswap (r);
		case 37:
			return sys_rs232read (r);
		case 38:
			return sys_rs232write (r);	
		case 39:
			return sys_gttyexit (r);
		case 40:
			return sys_gexit (r);
		case 41:
			return sys_gttyinit (r);
		case 42:
			return sys_getenv (r);
		case 43:
			return sys_free (r);
		case 44:
			return sys_realloc (r);
		case 45:
			return sys_gethostbyname (r);
		case 46:
			return sys_vfsent (r);
		case 47:
			return sys_sendto (r);
		case 48:
			return sys_recvfrom (r);
		case 49:
			return sys_getchar (r);
		case 50:
			return sys_threadopen (r);
		case 51:
			return sys_threadclose (r);
		case 52:
			return sys_ioctl (r);
		case 53:
			return sys_getpid (r);
		case 54:
			return sys_lseek (r);
		case 55:
			return sys_pipe (r);
		case 56:
			return sys_creat (r);
		case 57:
			return sys_mkdir (r);
		case 58:
			return sys_rmdir (r);
		case 59:
			return sys_sndopen (r);
		case 60:
			return sys_sndclose (r);
		case 61:
			return sys_sndwrite (r);
		case 62:
			return sys_remove (r);
		case 63:
			return sys_rename (r);
		case 64:
			return sys_select (r);
	}
}
