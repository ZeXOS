/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _SMP_H
#define _SMP_H

#define SMP_ARCH_x86		0x1
#define SMP_ARCH_ARM		0x2
#define SMP_ARCH_PPC		0x3
#define SMP_ARCH_SPARC		0x4
#define SMP_ARCH_ALPHA		0x5

#define SMP_STATE_CPU_UP	0x1
#define SMP_STATE_CPU_DOWN	0x2

#define SMP_FLAGS_CPU_BS	0x1
#define SMP_FLAGS_CPU_AP	0x2

/* x86 specific cpu features */
#define SMP_ARCH_x86_FEATURE_FPU   	0x00000001 // x87 fpu
#define SMP_ARCH_x86_FEATURE_VME   	0x00000002 // virtual 8086
#define SMP_ARCH_x86_FEATURE_DE    	0x00000004 // debugging extensions
#define SMP_ARCH_x86_FEATURE_PSE   	0x00000008 // page size extensions
#define SMP_ARCH_x86_FEATURE_TSC   	0x00000010 // rdtsc instruction
#define SMP_ARCH_x86_FEATURE_MSR   	0x00000020 // rdmsr/wrmsr instruction
#define SMP_ARCH_x86_FEATURE_PAE   	0x00000040 // extended 3 level page table addressing
#define SMP_ARCH_x86_FEATURE_MCE   	0x00000080 // machine check exception
#define SMP_ARCH_x86_FEATURE_CX8   	0x00000100 // cmpxchg8b instruction
#define SMP_ARCH_x86_FEATURE_APIC  	0x00000200 // local apic on chip
#define SMP_ARCH_x86_FEATURE_SEP   	0x00000800 // SYSENTER/SYSEXIT
#define SMP_ARCH_x86_FEATURE_MTRR  	0x00001000 // MTRR
#define SMP_ARCH_x86_FEATURE_PGE   	0x00002000 // paging global bit
#define SMP_ARCH_x86_FEATURE_MCA   	0x00004000 // machine check architecture
#define SMP_ARCH_x86_FEATURE_CMOV  	0x00008000 // cmov instruction
#define SMP_ARCH_x86_FEATURE_PAT   	0x00010000 // page attribute table
#define SMP_ARCH_x86_FEATURE_PSE36 	0x00020000 // page size extensions with 4MB pages
#define SMP_ARCH_x86_FEATURE_PSN   	0x00040000 // processor serial number
#define SMP_ARCH_x86_FEATURE_CLFSH 	0x00080000 // cflush instruction
#define SMP_ARCH_x86_FEATURE_DS    	0x00200000 // debug store
#define SMP_ARCH_x86_FEATURE_ACPI  	0x00400000 // thermal monitor and clock ctrl
#define SMP_ARCH_x86_FEATURE_MMX   	0x00800000 // mmx instructions
#define SMP_ARCH_x86_FEATURE_FXSR  	0x01000000 // FXSAVE/FXRSTOR instruction
#define SMP_ARCH_x86_FEATURE_SSE   	0x02000000 // SSE
#define SMP_ARCH_x86_FEATURE_SSE2  	0x04000000 // SSE2
#define SMP_ARCH_x86_FEATURE_SS    	0x08000000 // self snoop
#define SMP_ARCH_x86_FEATURE_HTT   	0x10000000 // hyperthreading
#define SMP_ARCH_x86_FEATURE_TM    	0x20000000 // thermal monitor
#define SMP_ARCH_x86_FEATURE_PBE   	0x80000000 // pending break enable

#define SMP_ARCH_x86_TRAMPOLINE		0x7000

/* x86 specific SMP attributes */
typedef struct {
	unsigned char lapic_id;
	unsigned char lapic_ver;
	
	unsigned cpuid;
	unsigned features;
} smp_arch_x86;

/* SMP CPU structure */
typedef struct smp_cpu_context {
	struct smp_cpu_context *next, *prev;

	unsigned char arch;

	void *arch_spec;
	
	unsigned char flags;
	unsigned char state;
} smp_cpu_t;


/* externs */
extern smp_cpu_t *smp_cpu_getnextcpu (smp_cpu_t *cpu);
extern void smp_cpu_check ();
extern unsigned smp_cpu_register (unsigned char arch, unsigned char state, unsigned char flags, void *arch_spec);
extern unsigned int init_smp ();

#endif
