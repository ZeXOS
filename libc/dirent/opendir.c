/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <vfs.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>

static vfs_dirent_t *getdir (const char *path)
{
	vfs_dirent_t *d;
  
	char *cwd = getcwd (0, 0);
	
	if (!cwd)
		return 0;
	
	chdir (path);
	
	asm volatile (
		"movl $25, %%eax;"
	     	"int $0x80;"
		"movl %%eax, %0;"
		: "=g" (d) :: "%eax");

	chdir (cwd);
	
	free (cwd);

	return d;
}

DIR *opendir (const char *name)
{
	if (!name) {
		errno = ENOENT;
		return 0;
	}

	vfs_dirent_t *ent = getdir (name);
	
	if (!ent) {
		errno = ENOENT;
		return 0;
	}

	DIR *dir = (DIR *) malloc (sizeof (DIR));
	
	if (!dir) {
		errno = ENOMEM;
		return 0;
	}

	unsigned l = strlen (name);
	
	if (l > NAME_MAX)
		l = NAME_MAX;
	
	memcpy (dir->name, name, l);
	dir->name[l] = '\0';
	
	unsigned id, m;

	/* count entries in dir structure */
	for (id = 0; ent[id].next; id ++);

	m = id + 1;
	
	dir->entry = (struct dirent *) malloc (sizeof (struct dirent) * (m + 1));
	
	if (!dir->entry) {
		free (dir);
		errno = ENOMEM;
		return 0;
	}
	
	/* create entires as dirents */
	for (id = 0; id < m; id ++) {
		dir->entry[id].d_ino = 0;	/* TODO */
		dir->entry[id].d_off = id * sizeof (struct dirent);
		dir->entry[id].d_reclen = sizeof (struct dirent);
		
		if (!ent[id].name)
			continue;

		l = strlen (ent[id].name);
		
		if (l > NAME_MAX)
			l = NAME_MAX;
	
		memcpy (dir->entry[id].d_name, ent[id].name, l);
		dir->entry[id].d_name[l] = '\0';
	}
	
	/* create last entry as eof */
	dir->entry[m].d_ino = -1;
	dir->entry[m].d_off = m * sizeof (struct dirent);
	dir->entry[m].d_reclen = 0;
	dir->entry[m].d_name[0] = '\0';
	
	return dir;
}
