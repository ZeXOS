/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <env.h>

env_t env_list;

void env_display ()
{
	env_t *env;
	for (env = env_list.next; env != &env_list; env = env->next)
		printf ("%s=%s\n", env->name, env->value);
}

env_t *env_find (char *name)
{
	if (!name)
		return 0;
  
	env_t *env;
	for (env = env_list.next; env != &env_list; env = env->next) {
		if (!strcmp (env->name, name))
			return env;
	}

	return 0;
}

unsigned int env_set (char *name, char *value)
{
	env_t *env = env_find (name);

	if (env == 0)
		return 0;

	strcpy (env->value, value);

	return 1;
}

char *env_get (char *name)
{
	env_t *env = env_find (name);

	if (env == 0)
		return 0;

	return env->value;
}

unsigned int env_create (char *name, char *value)
{
	/* first check length of name and value */
	if (strlen (name) >= DEFAULT_MAX_NAMELENGTH ||
	    strlen (value) >= DEFAULT_MAX_VALUELENGTH)
		return 0;

	/* env name havent to exists */
	if (env_find (name) != 0)
		return 0;

	/* alloc and init context */
	env_t *env = (env_t *) kmalloc (sizeof (env_t));

	if (!env)
		return 0;
	
	strcpy (env->name, name);
	strcpy (env->value, value);

	/* add into list */
	env->next = &env_list;
	env->prev = env_list.prev;
	env->prev->next = env;
	env->next->prev = env;

	DPRINT (DBG_CORE | DBG_ENV, "env_create () -> %s=%s", name, value);

	return 1;
}

/* install all default env */
void env_install ()
{
	env_create ("PWD", "/");
	env_create ("HOME", "/");
	env_create ("USER", "N/A");
	env_create ("LANGUAGE", "en");

	/* Keyboard layout */
	if (keyboard_getlayout ("us"))
		env_create ("KBD", "us");
	if (keyboard_getlayout ("cz"))
		env_create ("KBD", "cz");
}

unsigned int init_env ()
{
	env_list.next = &env_list;
	env_list.prev = &env_list;

	// set default env values
	env_list.name[0] = '\0';
	env_list.value[0] = '\0';

	env_install ();

	return 1;
}

