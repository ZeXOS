/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "net.h"
#include "http.h"

int arguments (int argc, char **argv)
{
	if (argc < 2)
		return 0;

	unsigned i = argc - 1;
	unsigned len = strlen (argv[i]);
	int flags = 0;

	if (len < 2)
		return 0;

	if (!strcmp (argv[i], "-6"))
		flags |= FLAG_IPV6;

	return flags;
}

int main (int argc, char **argv)
{
	int port = DEFAULT_HTTP_PORT;
	int flags = arguments (argc, argv);

	printf ("websrv\n-=-=-=-\nExit -- (ESC)\n");

	if ((!flags && argc == 2) || argc == 3)
		port = atoi (argv[1]);

	if (init (port, flags) < 1) {
		printf ("websrv -> init (0) !\n");
		return -1;
	}

	for (;;) {
		if (loop () == -1)
			break;
#ifndef LINUX
		/* Escape was pressed */
		if (getkey () == 1)
			break;

		schedule ();
#endif
	}

	http_destroy ();

	printf ("websrv -> Bye !\n");

	return 0;
}
