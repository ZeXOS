/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "client.h"
#include "console.h"

struct sockaddr_in sockName;         	// "Jmeno" portu
struct sockaddr_in clientInfo;       	// Klient, ktere se pripojil 
int mainSocket;               		// Soket
socklen_t addrlen;            		// Velikost adresy vzdaleneho pocitace
char *buffer;		      		// Buffer
int client_count;	      		// Pocet pripojenych clientu

client_t client_list;


int client_send (client_t *c, char *data, unsigned len)
{
	int ret;
	if ((ret = send (c->fd, data, len, 0)) == -1) {
		printf ("ERROR -> send () = -1\n");
		return -1;
	}

	return ret;
}

int clients_send (char *data, unsigned len)
{
	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next)
		client_send (c, data, len);

	return 1;
}

int client_recv (client_t *c, char *data, unsigned len)
{
	memset (data, 0, len);

	int ret = recv (c->fd, data, len, 0);

	if (ret > 0)
		data[ret] = '\0';

	return ret;
}

client_t *client_connected (int fd)
{
	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next)
		if (c->fd == fd)
			return 0;

	// alloc and init context
	client_t *ctx = (client_t *) malloc (sizeof (client_t));

	if (!ctx)
		return 0;

	ctx->fd = fd;
	ctx->state = CLIENT_STATE_CONNECTED;
	ctx->buf_len = 0;

	ctx->next = &client_list;
	ctx->prev = client_list.prev;
	ctx->prev->next = ctx;
	ctx->next->prev = ctx;

	return ctx;
}

int client_handle (client_t *c)
{
	if (c->state == CLIENT_STATE_CONNECTED)
		return console_motd (c);
	if (c->state == CLIENT_STATE_LOGIN)
		return console_login (c);
	if (c->state == CLIENT_STATE_READY)
		return console_handler (c);
	if (c->state == CLIENT_STATE_DONE) {
		/* disconnect socket */
		close (c->fd);

		/* delete client_t * struct from list */
		c->next->prev = c->prev;
		c->prev->next = c->next;

		free (c);
		return 0;
	}

	return 0;
}

int init (int port)
{
	if ((mainSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return -1;
	}

	// set mainSocket to non-blocking mode
	int oldFlag = fcntl (mainSocket, F_GETFL, 0);
	if (fcntl (mainSocket, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	// 1) Family of protocols
	sockName.sin_family = AF_INET;
	// 2) Set number of listen port
	sockName.sin_port = htons (port);
	// 3) Listen IP address
	sockName.sin_addr.s_addr = INADDR_ANY;

	// Bind on our socket
	if (bind (mainSocket, (struct sockaddr *) &sockName, sizeof (sockName)) == -1) {
		printf ("bind () - port is used\n");
		return -1;
	}

	// Create queue for accept
	if (listen (mainSocket, 10) == -1) {
		printf ("ERROR -> listen () == -1\n");
		return -1;
	}

	addrlen = sizeof (clientInfo);

	printf ("> telnetd is running !\n> listen on port: %d\n", port);

	client_list.next = &client_list;
	client_list.prev = &client_list;

	client_count = 0;

	return console_init ();
}

int loop ()
{
	// "client" is new socket;
	int client = accept (mainSocket, (struct sockaddr *) &clientInfo, &addrlen);

	if (client > 0) {
		printf ("> New client connected !\n");

		// socket client will be in non-blocking mode
		int oldFlag = fcntl (client, F_GETFL, 0);
		if (fcntl (client, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
			printf ("Cant set socket to nonblocking mode\n");
			return 0;
		}

		client_t *c = client_connected (client);

		if (!c)
			close (client);

		usleep (80);
	}

	client_count = 0;

	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next) {
		if (!c)
			continue;

		client_count ++;
		if (!client_handle (c))
			break;
	}

	return 1;
} 
