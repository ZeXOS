#!/bin/bash

echo "-= OpenChess =-"

cd src

if [ "$1" = "clean" ] ; then
	rm -rf *.o platform/zexos/*.o openchess ../bin/openchess ../bin/openchess.img

	echo "All object and binary files ware deleted"

	exit
fi

echo "Compilation process starting, please wait ..."

if [ "$1" = "linux" ] ; then
make -f makefile-linux && OK=1
else
make -f makefile-zexos && OK=1
fi

cd ..

if [ $OK ]; then
	cp src/openchess bin/
	
	echo
	echo "! Congratulations ! -- OpenChess is compiled and prepared to run"
	echo "Please start game server over: bin/openchess"
else
	echo "ERROR - Compilation fault ! Please contact authors or try update your sources"
fi