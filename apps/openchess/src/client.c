/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "client.h"
#include "net.h"

client_t client_list;

int client_pmmsg (client_t *client, char *buf, unsigned len)
{
	return net_send (client->sock, buf, len);
}

int client_msgtoall (char *buf, unsigned len)
{
	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next)
		net_send (c->sock, buf, len);

	return 1;
}

int client_msgtoall2 (client_t *client, char *buf, unsigned len)
{
	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next)	
		if (c != client)
			net_send (c->sock, buf, len);

	return 1;
}

/** Create new client's structure */
client_t *client_new (int sock)
{
	/* alloc and init context */
	client_t *client = (client_t *) malloc (sizeof (client_t));

	if (!client)
		return 0;

	client->sock = sock;
	client->nick = 0;

	/* add into list */
	client->next = &client_list;
	client->prev = client_list.prev;
	client->prev->next = client;
	client->next->prev = client;

	return client;
}

/** Delete client's structure */
int client_quit (client_t *client)
{
	client->next->prev = client->prev;
	client->prev->next = client->next;

	if (client->nick)
		free (client->nick);

	free (client);

	return 1;
}

/** Client Init function */
int init_client ()
{
	client_list.next = &client_list;
	client_list.prev = &client_list;

	return 1;
}

