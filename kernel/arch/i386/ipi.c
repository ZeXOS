/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <arch/ipi.h>

static unsigned *lapic_addr = 0;

static unsigned ipi_lapic_read (int reg)
{
	return lapic_addr[reg];
}

static unsigned ipi_lapic_write (int reg, unsigned value)
{
	lapic_addr[reg] = value;

	return 1;
}

unsigned ipi_lapic_init (unsigned addr)
{
	lapic_addr = (unsigned *) addr;

	return 1;
}

unsigned arch_ipi_send (unsigned dest, unsigned type, unsigned char vector)
{
	ipi_lapic_write (LAPIC_REG_INT_HIGH, dest << 24);
	ipi_lapic_write (LAPIC_REG_INT_LOW, LAPIC_REG_FLAG_WRITE | type | vector);

	return 1;
}
