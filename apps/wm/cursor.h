/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _CURSOR_H
#define _CURSOR_H

typedef struct wmcursor_context {
	int x;
	int y;
	unsigned char active;
	unsigned char state;
	unsigned char action;
} wmcursor;

/* externs */
extern unsigned cursor_setpos (unsigned x, unsigned y);
extern unsigned cursor_getpos (unsigned x, unsigned y);
extern unsigned cursor_setstate (unsigned state);
extern unsigned char cursor_getstate ();
extern unsigned cursor_draw ();
extern unsigned init_cursor ();

#endif
