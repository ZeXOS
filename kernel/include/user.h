/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _USER_H
#define _USER_H

#include <mytypes.h>

#define DEFAULT_MAX_NAMELENGTH		12
#define	DEFAULT_MAX_PWDLENGTH		32

#define USER_ATTR_FILE		0x1
#define USER_ATTR_DIR		0x2
#define USER_ATTR_HIDDEN	0x4
#define USER_ATTR_SYSTEM	0x8
#define USER_ATTR_BIN		0x10
#define USER_ATTR_READ		0x20
#define USER_ATTR_WRITE		0x40

#define USER_GROUP_ADMIN (USER_ATTR_FILE | USER_ATTR_DIR | USER_ATTR_HIDDEN | USER_ATTR_SYSTEM | \
			    USER_ATTR_BIN | USER_ATTR_READ | USER_ATTR_WRITE)

#define USER_GROUP_GUEST (USER_ATTR_FILE | USER_ATTR_DIR | USER_ATTR_READ | USER_ATTR_BIN)

/* Device structure */
typedef struct user_context {
	struct user_context *next, *prev;

	char *name;
	char *pass;
	unsigned attrib;
} user_t;

/* externs */
extern unsigned int init_user ();
extern bool getlogin (int i);

#endif

