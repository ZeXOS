/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>

#include "bitmap.h"
#include "button.h"
#include "cursor.h"
#include "window.h"
#include "handler.h"

static zbitmap_t bitmap_button;

zde_btn_t zde_btn_list;

extern zde_cursor_t *zde_cursor;
extern unsigned short image_button[];

#ifdef TEST
zde_btn_t *create_button (char *name, int type, void *(*entry) (void *), void *arg)
{
	zde_btn_t *button = (zde_btn_t *) malloc (sizeof (zde_btn_t));

	if (!button)
		return 0;

	if (type == 0) {
		button->arg = arg;

		button->bitmap = image_button;
	} else
		button->bitmap = 0;

	button->name = strdup (name);
	button->entry = entry;

	button->object = 0;

	/* add into list */
	button->next = &zde_btn_list;
	button->prev = zde_btn_list.prev;

	button->x = 14 + (button->prev->x * 40);
	button->y = 14;

	button->prev->next = button;
	button->next->prev = button;


	return button;
}
#endif

zde_btn_t *create_button_window (char *name, int type, void *(*entry) (void *), void *arg, zde_win_t *window)
{
	if (!window)
		return 0;

	zde_btn_t *button = (zde_btn_t *) malloc (sizeof (zde_btn_t));

	if (!button)
		return 0;

	button->x = 0 + (window->objectid * 40);
	button->y = 0;

	window->objectid ++;
	
	unsigned short name_len = strlen (name);

	if (type == 0) {
		unsigned arg_len = strlen ((char *) arg);
		
		button->arg = malloc (arg_len + 1);

		if (!button->arg)
			return 0;

		char *s = button->arg;

		memcpy (s, arg, arg_len);
		//memcpy (s+arg_len, "/", 1);
		//memcpy (s+arg_len+1, name, name_len);
		s[arg_len] = '\0';

		button->bitmap = (unsigned short *) image_button;
	} else
		button->bitmap = 0;

	button->name_len = name_len;
	button->name = strdup (name);
	button->entry = entry;
	button->object = (void *) window;

	/* add into list */
	button->next = &zde_btn_list;
	button->prev = zde_btn_list.prev;
	button->prev->next = button;
	button->next->prev = button;

	return button;
}

unsigned destroy_button_window (zde_win_t *window)
{
	unsigned i;

	for (;;) {
		i = 0;

		zde_btn_t *button;
		for (button = zde_btn_list.next; button != &zde_btn_list; button = button->next) {
			if (button->object == (void *) window) {
				button->next->prev = button->prev;
				button->prev->next = button->next;

				free (button);

				i = 1;
				break;
			}
		}

		if (!i)
			break;
	}

	return 1;
}

static void draw_bitmapspec (zbitmap_t *bitmap)
{
	unsigned i = 0;
	unsigned j = 0;
	unsigned k = 0;
	unsigned pix = 0;

	if (!bitmap)
		return;
	
	if (!bitmap->data)
		return;

	unsigned short w = bitmap->data[299+34];

	for (i = bitmap->posx+(bitmap->posy*13); i < 13*25; i ++) {
		if (k >= 14) {
			j ++;
			k = 0;
		}
		
		pix = 355-i;

		//if (pix < 355)
		//	continue;

		if (bitmap->data[pix] != w)
		if (k < bitmap->sizex && j < bitmap->sizey)
			xpixel ((unsigned) bitmap->x+k, (unsigned) bitmap->y+j, image_button[pix]);

		k ++;
	}
}

int draw_button_window (zde_win_t *window)
{
	zde_btn_t *button;
	for (button = zde_btn_list.next; button != &zde_btn_list; button = button->next) {
		if (button->object != (void *) window)
			continue;

		/* we click on left button */
		if (zde_cursor->state == XCURSOR_STATE_LBUTTON)
		if (!(zde_cursor->flags & CURSOR_FLAG_LBCLICK))
		if (!(zde_cursor->flags & CURSOR_FLAG_MOVE)) {
		  	if (window->x+4+button->x <= zde_cursor->x && window->x+button->x+12+((short) button->name_len*6) >= zde_cursor->x &&
			    window->y+button->y+22 <= zde_cursor->y && window->y+button->y+43 >= zde_cursor->y) {
				zde_cursor->state = 0;
				zde_cursor->flags |= CURSOR_FLAG_LBCLICK;

				pthread_t thread;
				pthread_create (&thread, NULL, button->entry, button->arg);
			}
		}

		/* 32x32 bitmap */
		if (button->bitmap) {
			unsigned len = button->name_len;

			bitmap_button.data = button->bitmap;
			bitmap_button.x = window->x+4+button->x;
			bitmap_button.y = window->y+21+button->y;
			bitmap_button.posx = 0;
			bitmap_button.posy = 0;
			bitmap_button.sizex = 4;
			bitmap_button.sizey = 23;
			
			draw_bitmapspec (&bitmap_button);

			unsigned l;
			for (l = 0; l < len; l ++) {
				bitmap_button.x = window->x+8+button->x+(l*6);
				bitmap_button.y = window->y+21+button->y;
				bitmap_button.posx = 4;
				bitmap_button.posy = 0;
				bitmap_button.sizex = 6;
				bitmap_button.sizey = 23;

				draw_bitmapspec (&bitmap_button);
			}
				
			bitmap_button.x = window->x+8+button->x+(len*6);
			bitmap_button.y = window->y+21+button->y;
			bitmap_button.posx = 9;
			bitmap_button.posy = 0;
			bitmap_button.sizex = 4;
			bitmap_button.sizey = 23;
				
			draw_bitmapspec (&bitmap_button);
		} else
			xrectfill (button->x+window->x, button->y+window->y, button->x+32+window->x, button->y+32+window->y, ~0);

		if (button->name)
			xtext_puts (button->x+8+window->x, button->y+28+window->y, 0, button->name);
	}

	return 0;
}

int init_button ()
{
	zde_btn_list.next = &zde_btn_list;
	zde_btn_list.prev = &zde_btn_list;

	/* FOLDER bitmap */
/*	bitmap_button = (unsigned short *) malloc (1100);

	if (!bitmap_button)
		return -1;

	memset (bitmap_button, 0, 1082);

	// html web page
	int fd = open ("button", O_RDONLY);
	
	if (!fd) {
		puts ("error -> file 'button' not found\n");
		return -1;
	}
	
	if (!read (fd, (unsigned char *) bitmap_button, 1082)) {
		puts ("error -> something was wrong !\n");
		return -1;
	}*/


	/* create test button */
	//create_button ("Filesystem", 0, handler_button_filesystem, "");

	//pthread_t thread;

	//pthread_create (&thread, NULL, handler_button_filesystem, "");

	return 0;
}
