/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <partition.h>
#include <env.h>
#include <dev.h>
#include <vfs.h>
#include <fs.h>
#include <tty.h>
#include <proc.h>
#include <stdio.h>
#include <stdlib.h>
#include <net/socket.h>
#include <net/dhcp.h>
#include <module.h>
#include <commands.h>
#include <signal.h>
#include <smp.h>
#include <build.h>
#include <config.h>
#include <net/if.h>
#include <net/net.h>
#include <net/ip.h>
#include <net/ndp.h>
#include <net/tun.h>
#include <net/icmp.h>
#include <net/hostname.h>
#include <mouse.h>
#include <pipe.h>
#include <cache.h>
#include <sound/audio.h>

command_t command_list;
extern partition_t partition_list;

unsigned char cmd_buf[256];

typedef void (*void_fn_void_t)(void);

extern task_t task_list;
extern vfs_t vfs_list;
extern unsigned long file_cache_id;
extern task_t *_curr_task;
extern module_t *module_load (char *modname);
extern unsigned long timer_ticks;


char *argparse (char *cmd)
{
	unsigned cmd_len = strlen (cmd);
	unsigned p = 1;

	while (p < cmd_len) {
		if (cmd[p] == ' ')
			return cmd + p + 1;
	
		p ++;
	}

	return "";
}

/*************************************************************\
|		           OMMANDs	  		      |
\*************************************************************/


unsigned command_help (char *command, unsigned len)
{
	command_t *ctx;

	tty_t *tty = currtty;

	tty_cls (tty);

	for (ctx = command_list.next; ctx != &command_list; ctx = ctx->next) {
		printf ("%s : %s\n", ctx->name, ctx->desc);

		if (tty->screen_y >= (tty_char.y-1)) {
			if (!vgagui)
				video_color (0, 15);

			printf ("Press enter to continue");

			video_color (15, 0);
			printf (" ");

			/* wait, until enter key is pressed */
			for (;;)
				if (tty == currtty) {
					if (getkey () != '\n')
						schedule ();
					else
						break;
				} else
					schedule ();

			tty_cls (tty);
		}
	}

	return 1;
}

unsigned command_hdd (char *command, unsigned len)
{
#ifdef ARCH_i386
	int t;
	int m;
	int c;
	int c2;
	int h;
	int s;

	outb (0x70, 0x12);
	t = inb (0x71);

	if (t >> 4 == 0)
		printf ("/dev/hda not installed\n");
	else {
		outb (0x70, 0x1b);
		c = inb (0x71);
		outb (0x70, 0x1c);
		c2 = inb (0x71);
		outb (0x70, 0x1d);
		h = inb (0x71);
		outb (0x70, 0x23);
		s = inb (0x71);

		printf ("/dev/hda installed - CHS=%d-%d:%d:%d\n", c, c2, h, s);
	}
	
	if (t & 0xF == 0)
		printf ("/dev/hdb not installed\n");
	else {
		outb (0x70, 0x24);
		c = inb (0x71);
		outb (0x70, 0x25);
		c2 = inb (0x71);
		outb (0x70, 0x26);
		h = inb (0x71);
		outb (0x70, 0x2c);
		s = inb (0x71);

		printf ("/dev/hdb installed - CHS=%d-%d:%d:%d\n", c, c2, h, s);
	}
#endif

	return 1;
}

unsigned command_reboot (char *command, unsigned len)
{
	arch_cpu_reset ();

	while (1);

	return 1;
}

unsigned command_halt (char *command, unsigned len)
{
	arch_cpu_shutdown ();

	kprintf ("\nSystem halted\nPlease press power button ..");

	int_disable ();

	while (1);

	return 1;
}

unsigned command_tasks (char *command, unsigned len)
{
	printf ("id\tname\tpriority\n");

	task_t *task;
	for (task = task_list.next; task != &task_list; task = task->next)
		printf ("%d\t%s\t%u\n", task->id, task->name, task->priority);

	return 1;
}

unsigned command_ps (char *command, unsigned len)
{
	proc_display ();

	return 1;
}

unsigned command_uptime (char *command, unsigned len)
{
	uptime ();

	return 1;
}

unsigned command_version (char *command, unsigned len)
{
	osinfo ();

	return 1;
}

unsigned command_debug (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));
	
	unsigned l = strlen (cmd_buf);
  
	if (l) {
		debug = atoi (cmd_buf);
		
		printf ("Developer mode was enabled with mask 0x%x.\n", debug);

		return 1;
	}

	printf ("Developer mode was disabled.\n");
	debug = 0;

	return 1;
}

unsigned command_mount (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));
	char devname[20];
	char mountpoint[32];

	unsigned l = strlen (cmd_buf);
	unsigned x = 0;
	while (x < l) {
		if (cmd_buf[x] == ' ')
			break;
		x ++;
	}

	memcpy (devname, cmd_buf, x);
	devname[x] = '\0';
	strcpy (mountpoint, argparse (cmd_buf));
	unsigned y = strlen (mountpoint);

	if (mountpoint[y-1] != '/') {
		mountpoint[y] = '/';
		mountpoint[y+1] = '\0';
	}

	if (x && y) {
		partition_t *p = partition_find (devname);

		if (p)
			mount (p, "", mountpoint);
		else
			printf ("ERROR -> partition %s does not exists\n", devname);
	} else
		mount_display ();

	return 1;
}

unsigned command_umount (char *command, unsigned len)
{
	char mountpoint[32];
	strcpy (mountpoint, argparse (command));

	unsigned y = strlen (mountpoint);
	unsigned x = 0;

	if (mountpoint[y-1] != '/') {
		mountpoint[y] = '/';
		mountpoint[y+1] = '\0';
	}

	if (y) {
		partition_t *partition;
		for (partition = partition_list.next; partition != &partition_list; partition = partition->next) {
			if (umount (partition, mountpoint))
				x ++;
		}

		if (!x)
			printf ("ERROR -> directory %s is not mounted\n", mountpoint);

		if (x > 1)
			printf ("WARNING -> multiple (%d) umount of %s\n", x, mountpoint);
	} else
		return 0;

	return 1;
}

unsigned command_env (char *command, unsigned len)
{
	env_display ();

	return 1;
}

unsigned command_cd (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	cd (cmd_buf);

	return 1;
}

unsigned command_ls (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	ls (cmd_buf);

	return 1;
}

unsigned command_cat (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	cat (cmd_buf);

	return 1;
}

unsigned command_cp (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	unsigned l = strlen (cmd_buf);
	unsigned p = 0;

	while (l) {
		if (cmd_buf[l] == ' ') {
			cmd_buf[l] = '\0';
			p = l;
		}

		l --;
	}

	if (!p) {
		printf ("Syntax:\ncp <source> <destination>\n");
		return 0;
	}

	cp (cmd_buf, cmd_buf+p+1);

	return 1;
}


unsigned command_rm (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	rm (cmd_buf);

	return 1;
}

unsigned command_mkdir (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	mkdir (cmd_buf);

	return 1;
}

unsigned command_touch (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	touch (cmd_buf);

	return 1;
}

unsigned command_exec (char *command, unsigned len)
{
	if (!strncmp ("./", command, 2))
		strcpy (cmd_buf, command+2);
	else
		strcpy (cmd_buf, argparse (command));

	unsigned arg_len = strlen (argparse (cmd_buf));
	
	if (arg_len >= 128)
		return 0;
	
	char arg[128];	
	strcpy (arg, argparse (cmd_buf));
	arg[arg_len] = '\0';

	unsigned cmd_buf_len = strlen (cmd_buf);

	tty_t *tty = currtty;
	
	if (!tty)
		return 0;
	
	/* lock current tty keyboard */
	tty_lock (tty);

	/* if there is argument after name, rewrite space character to zero */
	if (cmd_buf[cmd_buf_len-(arg_len+1)] == ' ')
		cmd_buf[cmd_buf_len-(arg_len+1)] = '\0';
	else
		cmd_buf[cmd_buf_len-arg_len] = '\0';

	vfs_content_t content;
	/* read file data from filesystem */
	int ret = vfs_read (cmd_buf, strlen (cmd_buf), &content);

	if (ret < 0) {
		printf ("%s: command not found\n", cmd_buf);
		DPRINT (DBG_CMDS, "ERROR -> vfs_read () - '%s'\n", cmd_buf);
		tty_unlock (tty);
		return 0;
	}

	unsigned bin_len = content.len;

	unsigned char *bin = (unsigned char *) swmalloc (bin_len);

	if (!bin) {
		printf ("ERROR -> out of memory\n");
		return 0;
	}
	
	//paging_disable ();
	
	/* copy image to page aligned memory */
	memcpy (bin, content.ptr, bin_len);
	
	//paging_enable ();
	
	cache_closebyptr (content.ptr);

	unsigned entry, code, data, data_off, bss;
	int err = exec_elf (bin, &entry, &data, &data_off, &bss);

	if (err) {
		printf ("ERROR -> invalid ELF exec\n");
		
		/* free program image */
		swfree (bin, bin_len);
		
		tty_unlock (tty);
		return 0;
	}

	/* create process */
	proc_t *proc = proc_create (tty, cmd_buf, entry);

	/* check for & character as app parameter */
	unsigned d = 0;
	while (d < arg_len) {
		if (arg[d] == '&') {
			if (proc_flag_set (proc, PROC_FLAG_DAEMON))
				printf ("%s: started as daemon\n", cmd_buf);
				
			arg[d] = '\0';

			if (arg_len > 2 && arg[d-1] == ' ') {
				arg[d-1] = '\0';
				arg_len --;
			}

			arg_len --;

			break;
		}
		d ++;
	}

	if (!proc) {
		printf ("ERROR -> Invalid process: %s\n", cmd_buf);
		
		/* free program image */
		swfree (bin, bin_len);
		
		tty_unlock (tty);
		return 0;
	}

	/* save process information to structure for later use */
	proc->start = (unsigned) bin;
	proc->code = entry;
	proc->data = data;
	proc->data_off = data_off;
	proc->bss = bss;
	proc->end = (unsigned) (bin + bin_len);

	/* well, set argument for out app */
	proc_arg_set (proc, arg, arg_len);

	proc_vmem_map (proc);

	/* Is this process started as daemon ? */
	/* wait here, until pid != 0*/
	if (!(proc->flags & PROC_FLAG_DAEMON))
		while (proc->pid)
			schedule ();

	/* enable interrupts */
	int_enable ();

	/* unlock tty console - user can write in */
	tty_unlock (tty);

	_curr_task = proc->tty->task;	// this is pretty needed for correct return to our tty task

	if (!(proc->flags & PROC_FLAG_DAEMON)) {
		if (!proc_done (proc))
			for (;;)	// NOTE: heh, this is good stuff :P
				schedule ();

		/* free program image */
		swfree (bin, bin_len);
	}

	/* switch to next task */
	schedule ();

	return 1;
}

unsigned command_lsdev (char *command, unsigned len)
{
	dev_display ();

	return 1;
}

unsigned command_login (char *command, unsigned len)
{
	currtty->user = NULL;
	currtty->logged = false;

	video_color (7, 0);
	printf ("login: ");

	return 1;
}

unsigned command_serialw (char *command, unsigned len)
{
	dev_t *dev = dev_find ("/dev/com0");

	if (dev) {
		unsigned len = strlen (argparse (command));
		char data[len+1];
		memcpy (data, argparse (command), len);
		data[len] = '\0';

		dev->handler (DEV_ACT_WRITE, data, len);

		printf ("serialw: %s\n", data);
	}

	return 1;
}

unsigned command_serialr (char *command, unsigned len)
{
	dev_t *dev = dev_find ("/dev/com0");

	if (dev) {
		char data[11];

		dev->handler (DEV_ACT_READ, data, 1);

		printf ("serialr: %s\n", data);
	}

	return 1;
}

unsigned command_fdisk (char *command, unsigned len)
{
	char *parm = argparse (command);

	fdisk (parm, strlen (parm));

	return 1;
}

unsigned command_hdcat (char *command, unsigned len)
{
	tty_lock (currtty);

	dev_t *dev = (dev_t *) dev_find (argparse (command));

	if (dev) {
		partition_t p;
		p.base_io = 0;

		switch (dev->devname[7]) {
			case 'a':
				p.id = 0;
				p.base_io = 0x1F0;
				break;
			case 'b':
				p.id = 1;
				p.base_io = 0x1F0;
				break;
			case 'c':
				p.id = 0;
				p.base_io = 0x170;
				break;
			case 'd':
				p.id = 1;
				p.base_io = 0x170;
				break;
		}

		int c, d = 0;
		unsigned char block[512];

		printf ("dev: %s\n", dev->devname);

		while (!key_pressed (1)) {
			if (key_pressed (72) == 1) {
				printf ("##block: %d\n", d);
				dev->handler (DEV_ACT_READ, &p, block, "", d);
					
				for (c = 0; c < 512; c ++)
					printf ("%c", (unsigned) block[c]);
				d ++;
			}

			usleep (5);
		}
	} else
		printf ("Please specify block device, example: hdcat /dev/hda\n");

	tty_unlock (currtty);

	return 1;
}

unsigned command_free (char *command, unsigned len)
{
	util_cmdfree ();

	return 1;
}

unsigned command_top (char *command, unsigned len)
{
	tty_lock (currtty);
  
	proc_top ();
	
	tty_unlock (currtty);
	
	return 1;
}

unsigned command_date (char *command, unsigned len)
{
	tm *t = rtc_getcurrtime ();

	printf ("%02u:%02u:%02u, %u.%u.%u\n",
		t->tm_hour, t->tm_min, t->tm_sec, t->tm_mday, t->tm_mon, t->tm_year);

	return 1;
}

/*
     Tone	      Frequency (Hz)
	C		130.8
	D		146.8
	E		164.8
	F		174.6
	G		196.0
	A		220.0
	H		246.9

	C		261.6
	D		293.7
	E		329.6
	F		349.2
	G		392.0
	A		440.0
	H		493.9

	C		523.3
	D		587.3
	E		659.3
	F		698.5
	G		784.0
	A		880.0
	H		987.8

	C		1046.5
	D		1174.7
	E		1318.5
	F		1396.9
	G		1568.0
	A		1760.0
	H		1975.5
*/

void play_tone (dev_t *dev, char tone, char type, char time)
{
	unsigned t = 2;

	switch (type) {
		case '1':
			t = 1;
			break;
		case '2':
			t = 2;
			break;
		case '3':
			t = 3;
			break;
		case '4':
			t = 4;
			break;
	}


	unsigned tt = 1;

	printf ("play_tone (.., %c, %d, %d);\n", tone, t, tt);

	switch (tone) {
		case 'c':
			dev->handler (DEV_ACT_PLAY, 131*t);
			break;
		case 'd':
			dev->handler (DEV_ACT_PLAY, 147*t);
			break;
		case 'e':
			dev->handler (DEV_ACT_PLAY, 165*t);
			break;
		case 'f':
			dev->handler (DEV_ACT_PLAY, 175*t);
			break;
		case 'g':
			dev->handler (DEV_ACT_PLAY, 196*t);
			break;
		case 'a':
			dev->handler (DEV_ACT_PLAY, 220*t);
			break;
		case 'h':
			dev->handler (DEV_ACT_PLAY, 247*t);
			break;
		case 'C':
			dev->handler (DEV_ACT_PLAY, 136*t);
			break;
		case 'F':
			dev->handler (DEV_ACT_PLAY, 188*t);
			break;
	}

	switch (time) {
		case '0':
			timer_wait (2500);
			break;
		case '1':
			timer_wait (1250);
			break;
		case '2':
			timer_wait (625);
			break;
	}

	

	printf ("end: play_tone ();\n");

	dev->handler (DEV_ACT_PLAY, 0);
}

void play_song (dev_t *dev, char *data)
{
	printf ("play_song ();\n");
	unsigned i = 0, l = strlen (data);
	unsigned time = 1;
	while (i != l) {
		switch (data[i]) {
			printf ("switch ()\n");
			case 'p':
				
				switch (data[i+1]) {
					case '0':
						timer_wait (2500);
						break;
					case '1':
						timer_wait (1250);
						break;
					case '2':
						timer_wait (625);
						break;
				}

				
				i ++;
				break;
			case ' ':
				break;
			default:
				play_tone (dev, data[i], data[i+1], data[i+2]);
				i += 2;
				break;
		}

		i ++;
	}

}

unsigned command_spk (char *command, unsigned len)
{
	unsigned int freq = 20;
		
	dev_t *dev = dev_find ("/dev/pcspk");

	if (dev) {
/*
We wish you a merry christmas v1.0

D2(1/8) G2(1/8) G2(1/16) A2(1/16) G2(1/16) Fis2(1/16) E2(1/8) C2(1/8) E2(1/8) A2(1/8) A2(1/16) H2(1/16) A2(1/16)  G2(1/16) Fis2(1/8) D2(1/4) H2(1/8) H2(1/16) C3(1/16) H2(1/16) A2(1/16) G2(1/8) E2(1/8) D2(1/16) D2(1/16) E2(1/8)  A2(1/8) Fis2(1/8) G2(1/8)
*/

		play_song (dev, "d21 g21 g22 a22 g22 F22 e21 c21 e21 a21 a22 h22 a22 g22 F21 d20 h21 h22 c32 h22 a22 g21 e21 d22 d22 e21 a21 F21 g21");

		timer_wait (2000);

/*
We wish you a merry christmas v3.0

G2(1/8) C3(1/8) C3(1/16) D3(1/16) C3(1/16) H2(1/16) A2(1/4) D3(1/8) D3(1/16) E3(1/16) D3(1/16) C3(1/16) H2(1/4)  G2(1/8) E3(1/8) E3(1/16) F3(1/16) E3(1/16) D3(1/16) C3(1/8) A2(1/8) G2(1/8) A2(1/8) D3(1/8) H2(1/8) C3(1/8)
*/

		play_song (dev, "g21 c31 c32 d32 c32 h22 a20 d31 d32 e32 d32 c32 h20 g21 e31 e32 f32 e32 d32 c31 a21 g21 a21 d31 h21 c31");

		timer_wait (2000);
	
/*
 Dallas v2.0
G2(1/8) C3(1/8) P(1/16) G2(1/16) G3(1/8) C3(1/16) E3(1/8) D3(1/16) E3(1/16) C3(1/8) G2(1/8) C3(1/8) A3(1/8) G3(1/8) E3(1/16) F3(1/16) G3(1/8) G3(1/16) P(1/16) G2(1/8) C3(1/8) A3(1/8) G3(1/8) E3(1/16) F3(1/16) G3(1/8) D3(1/16) E3(1/16) C3(1/8) G2(1/8) C3(1/8) E3(1/16) F3(1/16) D3(1/8) G3(1/8) G3(1/8) 
*/
	/*	play_song (dev, "g21 c31 p2 g22 g31 c32 e31 d32 e32 c31 g21 c31 a31 g31 e32 f32 g31 g32 p2 g21 c31 a31 g31 e32 f32 g31 d32 e32 c31 g21 c31 e32 f32 d31 g31 g31");*/

		//timer_wait (2000);
	}

	return 1;
}

extern bool bus_pci_acthandler (unsigned act, char *block, unsigned block_len);
extern netif_t netif_list;
extern void ext2_dump ();
void thread_test ()
{
	while (1) {
		
 		kprintf ("hu\n");

		unsigned long timer_start = timer_ticks + ((unsigned) 1 * 1000);

		while (timer_start > timer_ticks)
			schedule ();
	}
}
unsigned command_netcp (char *command, unsigned len);
unsigned command_test (char *command, unsigned len)
{
/*	partition_t *p = partition_find ("/dev/hda0");

	if (p)
		mount (p, "", "/mnt/hdd/");
	else
		return 0;

	cd ("/mnt/hdd");*/

	//command_znfscl ("znfcl 192.168.0.2", strlen ("znfcl 192.168.0.2"));

	char *file = "netcp 192.168.0.2 iso/file /bin/file";
	char *border = "netcp 192.168.0.2 iso/border /bin/border";
	char *folder = "netcp 192.168.0.2 iso/folder /bin/folder";
	char *zde = "netcp 192.168.0.2 iso/zde /bin/zde";
	
	command_netcp (file, strlen (file));
	command_netcp (border, strlen (border));
	command_netcp (folder, strlen (folder));
	command_netcp (zde, strlen (zde));
	
	cd ("/bin");
	
	command_exec ("exec zde", 8);
	
	return 1;
}

unsigned command_test2 (char *command, unsigned len)
{
	partition_t *p = partition_find ("/dev/cdc0");

	if (p)
		mount (p, "", "/mnt/cdrom/");
	else
		return 0;

	cd ("/mnt/cdrom");

/*	tty_t *tty = tty_find ("tty0");
	
	if (!tty)
		return 0;
	char str[513];
	int r = tty_read (tty, str, 512);
	
	if (r > 0) {
		printf ("str: r: %d\n", r);
		int i;
		for (i = 0; i < r; i ++)
			tty_putch (str[i]);
	}
  */
  
/*	int ret = vfs_read ("wav", 3);

	if (ret < 0) {
		printf ("chyba\n");
		return 0;
	}

	cache_t *cache = cache_read ();

	unsigned char *p = (unsigned char *) &cache->data;

	unsigned bin_len = cache->limit;

	unsigned char *bin = (unsigned char *) 0x400000 - 44;

	memcpy (bin, p, bin_len);

	typedef struct {
		unsigned chunkid;
		unsigned chunksize;
		unsigned format;
	} riff_head_t;

	typedef struct {
		unsigned subchunk1id;
		unsigned subchunk1size;
		unsigned short audioformat;
		unsigned short numchannels;
		unsigned samplerate;
		unsigned byterate;
		unsigned short blockalign;
		unsigned short bitspersample;
	} fmt_head_t;

	typedef struct {
		unsigned subchunk2id;
		unsigned subchunk2size;
	} data_head_t;

	riff_head_t *r = (riff_head_t *) bin;
	fmt_head_t *fmt = (fmt_head_t *) ((char *) bin + sizeof (riff_head_t));
	data_head_t *data = (data_head_t *) ((char *) bin + sizeof (riff_head_t) + sizeof (fmt_head_t));

	printf ("riff_head_t 0x%x : %d : 0x%x\n", r->chunkid, r->chunksize, r->format);
	printf ("fmt_head_t %d : %d : %d : %d : %d : %d\n", fmt->audioformat, fmt->subchunk1size, fmt->numchannels, fmt->samplerate, fmt->byterate, fmt->bitspersample);
	printf ("data_head_t 0x%x : %d\n", data->subchunk2id, data->subchunk2size);
	//memset (bin+44, 0, bin_len-44);
	//dev->handler (DEV_ACT_WRITE, bin+44, bin_len-44);

	snd_cfg_t cfg;
	cfg.dev = "/dev/ac97";
	cfg.rate = fmt->samplerate;
	cfg.format = SOUND_FORMAT_S16;
	cfg.channels = fmt->numchannels;

	snd_audio_t *aud = audio_open (&cfg);

	if (!aud)
		return 0;

	audio_write (aud, bin+44, (bin_len-44));

	//swfree (bin, bin_len);*/



	return 1;
}

unsigned command_test3 (char *command, unsigned len)
{
	netif_t *netif = netif_findbyname ("eth0");
	dhcp_config_if(netif);
	
	return 1;
}

unsigned command_test4 (char *command, unsigned len)
{


	return 1;
}

unsigned command_mouse (char *command, unsigned len)
{
	printf ("PS/2 Mouse test\n");

	dev_t *dev = dev_find ("/dev/mousecom");	

	if (!dev)
		return 0;

	dev_mouse_t mouse;

	int ps2_fd = open ("/dev/mousecom", O_RDONLY);

	if (ps2_fd < 1)
		return 0;

	while (!key_pressed (1)) {
		int r = read (ps2_fd, &mouse, sizeof (dev_mouse_t));

		if (r < 1) {
			kprintf ("oj oj\n");
			return 0;
		}

		kprintf ("ps2mouse: x,y: %d, %d | 0x%x \n", mouse.pos_x, mouse.pos_y, mouse.flags);

		lseek (ps2_fd, 0, SEEK_SET);

		schedule ();
	}

	return 1;
}

unsigned command_vesa (char *command, unsigned len)
{
	if (!init_video_vesa ())
		printf ("vesa -> failed\n");

	return 1;
}

unsigned command_kill (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (currtty->shell));

	pid_t pid = (pid_t) atoi (cmd_buf);

	proc_t *proc = proc_findbypid (pid);

	if (!proc) {
		printf ("kill: (%s) - No such process\n", cmd_buf);
		return 0;
	}

	/* send SIGTERM to selected process */
	if (!proc_signal (proc, SIGTERM))
		return 0;

	/* check for daemon */
	if (proc->flags & PROC_FLAG_DAEMON)
		if (!proc_done (proc))
			return 0;	

	return 1;
}

unsigned command_modprobe (char *command, unsigned len)
{
	unsigned arg_len = strlen (argparse (currtty->shell));
	char arg[arg_len+1];
	strcpy (arg, argparse (currtty->shell));
	arg[arg_len] = '\0';

	module_load (arg);

	return 1;
}

unsigned command_lsmod (char *command, unsigned len)
{
	module_display ();

	return 1;
}

#ifdef CONFIG_DRV_PCI
unsigned command_lspci (char *command, unsigned len)
{
#ifdef ARCH_i386
	pcidev_display ();
#endif
	return 1;
}
#endif

unsigned command_iflist (char *command, unsigned len)
{
	iflist_display ();

	return 1;
}

unsigned command_ifconfig (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));
	char ifid[20];
	char ip[32];

	unsigned l = strlen (cmd_buf);
	unsigned x = 0;
	while (x < l) {
		if (cmd_buf[x] == ' ')
			break;
		x ++;
	}

	memcpy (ifid, cmd_buf, x);
	ifid[x] = '\0';

	strcpy (ip, argparse (cmd_buf));
	unsigned y = strlen (ip);

	netif_t *netif = netif_findbyname (ifid);

	if (!netif) {
		printf ("ifconfig -> bad network interface name, example: eth0\n");
		return 0;
	}

	net_ipv4 ipv4;
	net_ipv6 ipv6;
	if (ipv4 = net_proto_ip_convert (ip)) {
		netif_ip_addr (netif, ipv4, IF_CFG_TYPE_STATIC);

		printf ("%s: IPv4 ", ifid);
		net_proto_ip_print (ipv4);
		printf (" - OK\n");
	} else if (net_proto_ipv6_convert (ipv6, ip)) {
		netif_ipv6_addr (netif, ipv6, IF_CFG_TYPE_STATIC);

		printf ("%s: IPv6 ", ifid);
		net_proto_ipv6_print (ipv6);
		printf (" - OK\n");
	} else {
		printf ("ifconfig -> bad ip address format, example:\n\t\t192.168.1.1 for IPv4\n\t\tfc00:0:0:0:0:0:0:10 for IPv6\n");
		return 0;
	}

	return 1;
}

unsigned command_ifroute (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));
	char ifid[20];
	char ip[32];

	unsigned l = strlen (cmd_buf);
	unsigned x = 0;
	while (x < l) {
		if (cmd_buf[x] == ' ')
			break;
		x ++;
	}

	memcpy (ifid, cmd_buf, x);
	ifid[x] = '\0';

	strcpy (ip, argparse (cmd_buf));
	unsigned y = strlen (ip);

	netif_t *netif = netif_findbyname (ifid);

	if (!netif) {
		printf ("ifroute -> bad network interface name, example: eth0\n");
		return 0;
	}

	unsigned char a;
	unsigned char b;
	unsigned char c;
	unsigned char d;

	unsigned g = 0;
	unsigned i = 0;

	unsigned h[4];

	while (i < y) {
		if (ip[i] == '.') {
			ip[i] = '\0';
			h[g] = i+1;
			g ++;
		}

		i ++;
	}

	if (g != 3) {
		printf ("ifroute -> bad ip address format, example: 192.168.1.254\n");
		return 0;
	}
	
	a = atoi (ip);
	b = atoi (ip+h[0]);
	c = atoi (ip+h[1]);
	d = atoi (ip+h[2]);

	printf ("%s->gw: %d.%d.%d.%d - OK\n", ifid, a, b, c, d);

	return netif_gw_addr (netif, NET_IPV4_TO_ADDR (a, b, c, d));
}

unsigned command_dnsconfig (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (currtty->shell));

	if (!cmd_buf)
		return 0;

	unsigned l = strlen (cmd_buf);

	if (!l) {
		printf ("dnsconfig -> your domain name server is: ");
		net_proto_ip_print (dns_addr_get ());
		printf ("\n");

		return 0;
	}

	printf ("dnsconfig: %s - OK\n", cmd_buf);

	net_ipv4 dns = net_proto_ip_convert (cmd_buf);

	if (!dns)
		return 0;

	dns_addr (dns);

	return 1;
}

unsigned command_dhcpcl (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (currtty->shell));

	if (!cmd_buf)
		return 0;

	unsigned l = strlen (cmd_buf);
	
	if (!l) {
		printf ("dhcpcl -> please specify interface\n");
		return 0;
	}

	netif_t *netif = netif_findbyname (cmd_buf);

	if (!netif) {
		printf ("dhcpcl -> bad network interface name, example: eth0\n");
		return 0;
	}

	unsigned ret = dhcp_config_if (netif);

	if (ret < 0) {
		switch (ret) {
			case DHCP_OUT_OF_MEMORY:
				printf ("dhcpcl -> out of memory\n");
				break;
			case DHCP_SOCKET_FAILED:
			case DHCP_CONNECT_FAILED:
			case DHCP_CANT_SEND_REQUEST:
				printf ("dhcpcl -> problem with network layer\n");
				break;
			case DHCP_CANT_RECV_RESPONSE:
				printf ("dhcpcl -> can't receive response\n");
				break;
			case DHCP_BAD_PACKET:
				printf ("dhcpcl -> bad packet received\n");
				break;
			case DHCP_SRV_DIDNT_UNDERSTAND:
				printf ("dhcpcl -> server didn't understand\n");
				break;
		}
	} else
		printf ("dhcpcl -> interface %s successfully configured\n", cmd_buf);

	return 0;
}

unsigned command_tunconfig (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (currtty->shell));

	if (!cmd_buf)
		return 0;

	unsigned l = strlen (cmd_buf);

	if (!l) {
		printf ("tunconfig -> wrong syntax !\ntunconfig <ipv4tunnel> for enable or tunconfig 0 for disable\n");

		return 0;
	}

	/* you can disable this service over character '0' as parameter */
	if (l == 1 && cmd_buf[0] == '0') {
		tun6_addr (0);
	
		printf ("tunconfig: disabled - OK\n");

		return 1;
	}

	/* classical ipv4 address from tunnel server */
	net_ipv4 tunnel = net_proto_ip_convert (cmd_buf);

	if (!tunnel) {
		printf ("tunconfig -> wrong syntax !\nExample: tunconfig 216.66.80.30\n");

		return 0;
	}

	tun6_addr (tunnel);

	printf ("tunconfig: ::%s - OK\n", cmd_buf);

	return 1;
}


unsigned command_hostname (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (currtty->shell));

	if (!cmd_buf)
		return 0;

	unsigned l = strlen (cmd_buf);

	if (!l) {
		printf ("%s\n", hostname_get ());

		return 0;
	}

	printf ("hostname: %s - OK\n", cmd_buf);

	hostname_set (cmd_buf);

	return 1;
}

unsigned command_kbdmap (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (currtty->shell));

	if (!cmd_buf)
		return 0;

	unsigned ret = keyboard_setlayout (cmd_buf);

	if (ret)
		printf ("Keyboard layout was changed to '%s'\n", cmd_buf);

	return ret;
}

unsigned command_mkzexfs (char *command, unsigned len)
{
#ifdef ARCH_i386
	strcpy (cmd_buf, argparse (currtty->shell));

	partition_t *p = partition_find (cmd_buf);

	if (!p)
		return 0;

	mkzexfs (p);
#endif
	return 1;
}

unsigned command_mkext2 (char *command, unsigned len)
{
#ifdef ARCH_i386
	strcpy (cmd_buf, argparse (currtty->shell));

	if (!cmd_buf)
		return 0;

	partition_t *p = partition_find ("/dev/hda0");

	mkext2 (p);
#endif
	return 1;
}

unsigned command_ping (char *command, unsigned len)
{
  
	strcpy (cmd_buf, argparse (currtty->shell));

	if (!cmd_buf)
		return 0;
	
	netif_t *netif = netif_findbyname ("eth0");
	
	if (!netif) {
		printf ("ping -> network interface does not exists\n");
		return 0;
	}

	unsigned char a;
	unsigned char b;
	unsigned char c;
	unsigned char d;

	unsigned g = 0;
	unsigned i = 0;
	unsigned y = strlen (cmd_buf);

	if (!y) {
		printf ("ping -> wrong syntax, please specify address\n");
		return 0;
	}

	unsigned h[4];

	while (i < y) {
		if (cmd_buf[i] == '.') {
			cmd_buf[i] = '\0';
			h[g] = i+1;
			g ++;
		}

		i ++;
	}

	net_ipv4 target = 0;

	if (g != 3) {
		strcpy (cmd_buf, argparse (currtty->shell));

		if (dns_cache_get (cmd_buf, &target, 4) != 1)
			if (dns_send_request (cmd_buf, &target, 4) != 1) {
				printf ("ping -> bad hostname or ip address format, example: 192.168.1.1\n");
				return 0;
			}
	} else {
		a = atoi (cmd_buf);
		b = atoi (cmd_buf+h[0]);
		c = atoi (cmd_buf+h[1]);
		d = atoi (cmd_buf+h[2]);

		target = NET_IPV4_TO_ADDR (a, b, c, d);

		strcpy (cmd_buf, argparse (currtty->shell));
	}

	char address[20];
	net_proto_ip_convert2 (target, address);

	i = 0;
	
	while (i < 8) {
		unsigned long time_a = timer_ticks;
		unsigned ret = net_proto_icmp_ping (netif, target);
		if (ret) {
			printf ("ping -> %s (%s) - %ums\n", cmd_buf, address, (timer_ticks - time_a));
		} else {
			printf ("ping -> Host %s is unreachable\n", cmd_buf);
			break;
		}

		i ++;
	}

	return 1;
}

unsigned command_ping6 (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (currtty->shell));

	if (!cmd_buf)
		return 0;

	netif_t *netif = netif_findbyname ("eth0");

	if (!netif) {
		printf ("ping6 -> network interface does not exists\n");
		return 0;
	}

	unsigned short a;
	unsigned short b;
	unsigned short c;
	unsigned short d;
	unsigned short e;
	unsigned short f;
	unsigned short g;
	unsigned short h;

	unsigned j = 0;
	unsigned i = 0;
	unsigned y = strlen (cmd_buf);

	if (!y) {
		printf ("ping6 -> wrong syntax, please specify ipv6 address\n");
		return 0;
	}

	unsigned k[8];

	while (i < y) {
		if (cmd_buf[i] == ':') {
			cmd_buf[i] = '\0';
			k[j] = i+1;
			j ++;
		}

		i ++;
	}

	net_ipv6 target;

	if (j != 7) {
		strcpy (cmd_buf, argparse (currtty->shell));

		if (dns_cache_get (cmd_buf, &target, 16) != 1)
			if (dns_send_request (cmd_buf, &target, 16) != 1) {
				printf ("ping6 -> bad hostname or ip address format, example: 2001:4d3:8fe:2:0:0:0:1\n");
				return 0;
			}
	} else {
		char *endptr;

		a = strtol (cmd_buf, &endptr, 16);
		b = strtol (cmd_buf+k[0], &endptr, 16);
		c = strtol (cmd_buf+k[1], &endptr, 16);
		d = strtol (cmd_buf+k[2], &endptr, 16);
		e = strtol (cmd_buf+k[3], &endptr, 16);
		f = strtol (cmd_buf+k[4], &endptr, 16);
		g = strtol (cmd_buf+k[5], &endptr, 16);
		h = strtol (cmd_buf+k[6], &endptr, 16);

		NET_IPV6_TO_ADDR (target, a, b, c, d, e, f, g, h);

		//printf ("ipv6: %04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x\n", a, b, c, d, e, f, g, h);

		strcpy (cmd_buf, argparse (currtty->shell));
	}

	char address[40];
	//net_proto_ipv6_convert2 (target, address);

	i = 0;

	while (i < 8) {
		unsigned long time_a = timer_ticks;
		unsigned ret = net_proto_icmp6_ping (netif, target);
		if (ret) {
			printf ("ping6 -> %s (%s) - %ums\n", cmd_buf, cmd_buf, (timer_ticks - time_a));
		} else {
			printf ("ping6 -> Host %s is unreachable\n", cmd_buf);
			break;
		}

		i ++;
	}

	return 1;
}

unsigned command_netexec (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	if (!cmd_buf)
		return 0;

	if (!strlen (cmd_buf)) {
		printf ("Syntax:\nnetexec <ip_of_tftp> <filename> [parameters]\n");
		return 0;
	}

	unsigned arg_len = strlen (argparse (cmd_buf));
	
	if (arg_len >= 128)
		return 0;
	
	char arg[128];
	strcpy (arg, argparse (cmd_buf));
	arg[arg_len] = '\0';

	unsigned cmd_buf_len = strlen (cmd_buf);
	cmd_buf [cmd_buf_len-arg_len] = '\0';

	if (cmd_buf[cmd_buf_len-arg_len-1] == ' ') {
		cmd_buf[cmd_buf_len-arg_len-1] = '\0';
		cmd_buf_len --;
	}

	char file[128];
	unsigned file_len = 0;
	memcpy (file, arg, arg_len);
	while (file_len < arg_len) {
		if (file[file_len] == ' ') {
			file[file_len] = '\0';
			break;
		}
		file_len ++;
	}

	if (file[arg_len] != '\0')
		file[arg_len] = '\0';

	unsigned l = file_len;

	unsigned ret = tftp_client_connect (cmd_buf, 69, file, &l);

	if (!ret) {
		printf ("tftp -> something go wrong !\n");
		return 0;
	}

	printf ("tftp -> file length: %ubytes\t\t[OK]\n", l);

	char *bin = (unsigned char *) swmalloc (sizeof (char) * (l+1));

	if (!bin) {
		printf ("ERROR -> out of memory\n");
		return 0;
	}

	int ret2 = tftp_client_read (ret, bin, l);

	if (!ret2) {
		printf ("tftp -> tftp_client_read () error\n");
		swfree (bin, l);
		return 0;
	}

	unsigned entry, code, data, data_off, bss;
	int err = exec_elf (bin, &entry, &data, &data_off, &bss);

	if (err != NULL) {
		printf ("ERROR -> invalid ELF exec\n");
		return 0;
	}

	tty_lock (currtty);

	/* create process */
	proc_t *proc = proc_create (currtty, "netexec", entry);

	/* check for & character as app parameter */
/*	unsigned d = 0;
	while (d < arg_len) {
		if (arg[d] == '&') {
			if (proc_flag_set (proc, PROC_FLAG_DAEMON))
				printf ("%s: started as daemon\n", cmd_buf);
				
			arg[d] = '\0';

			if (arg_len > 2 && arg[d-1] == ' ') {
				arg[d-1] = '\0';
				arg_len --;
			}

			arg_len --;

			break;
		}
		d ++;
	}*/

	if (!proc) {
		printf ("ERROR -> Invalid process: %s\n", cmd_buf);
		
		/* free program image */
		swfree (bin, l);
		
		tty_unlock (currtty);
		return 0;
	}

	/* save process information to structure for later use */
	proc->start = (unsigned) bin;
	proc->code = entry;
	proc->data = data;
	proc->data_off = data_off;
	proc->bss = bss;
	proc->end = (unsigned) (bin + l);

	/* well, set argument for out app */
	proc_arg_set (proc, arg+file_len+1, arg_len-file_len-1);

	proc_vmem_map (proc);

	/* Is this process started as daemon ? */
	/* wait here, until pid != 0*/
	if (!(proc->flags & PROC_FLAG_DAEMON))
		while (proc->pid)
			schedule ();

	/* enable interrupts */
	int_enable ();

	/* unlock tty console - user can write in */
	tty_unlock (proc->tty);

	_curr_task = proc->tty->task;	// this is pretty needed for correct return to our tty task

	if (!(proc->flags & PROC_FLAG_DAEMON)) {
		if (!proc_done (proc))
			for (;;)	// NOTE: heh, this is good stuff :P
				schedule ();

		/* free image of app */
		swfree (bin, l);
	}

	/* switch to next task */
	schedule ();

	sclose (ret);

	return 1;
}

unsigned command_netcp (char *command, unsigned len)
{
	strcpy (cmd_buf, argparse (command));

	if (!cmd_buf)
		return 0;

	if (!strlen (cmd_buf)) {
		printf ("Syntax:\nnetcp <ip_of_tftp> <tftp_file> <output_file>\n");
		return 0;
	}

	unsigned arg_len = strlen (argparse (cmd_buf));

	if (arg_len >= 128)
		return 0;
	
	char arg[128];
	strcpy (arg, argparse (cmd_buf));
	arg[arg_len] = '\0';

	unsigned cmd_buf_len = strlen (cmd_buf);
	cmd_buf [cmd_buf_len-arg_len] = '\0';

	if (cmd_buf[cmd_buf_len-arg_len-1] == ' ') {
		cmd_buf[cmd_buf_len-arg_len-1] = '\0';
		cmd_buf_len --;
	}

	char file[128];
	unsigned file_len = 0;
	memcpy (file, arg, arg_len);
	while (file_len < arg_len) {
		if (file[file_len] == ' ') {
			file[file_len] = '\0';
			break;
		}
		file_len ++;
	}

	if (file[arg_len] != '\0')
		file[arg_len] = '\0';

	unsigned l = file_len;

	unsigned ret = tftp_client_connect (cmd_buf, 69, file, &l);

	if (!ret) {
		printf ("tftp -> something go wrong !\n");
		return 0;
	}

	printf ("tftp -> file length: %ubytes\t\t[OK]\n", l);

	char *bin = (unsigned char *) swmalloc (sizeof (char) * (l+1));

	if (!bin)
		return 0;

	int ret2 = tftp_client_read (ret, bin, l);

	if (!ret2) {
		printf ("tftp -> tftp_client_read () error\n");
		swfree (bin, l);
		return 0;
	}

	sclose (ret);

	char *output = argparse (arg);

	if (!output) {
		swfree (bin, l);
		printf ("ERROR -> output file name is empty\n");
		return 0;
	}

	int fd = open (output, O_WRONLY | O_CREAT);

	if (!fd) {
		swfree (bin, l);
		printf ("ERROR -> something go wrong with %s\n", output);
		return 0;
	}

	ret = write (fd, bin, l);

	if (!ret) {
		swfree (bin, l);
		printf ("ERROR -> File '%s' could'nt be writed\n", output);
	}

	return 1;
}

extern bool ide_acthandler (unsigned act, partition_t *p, char *block, char *more, int n);
unsigned command_znfscl (char *command, unsigned len)
{
#ifdef ARCH_i386
	strcpy (cmd_buf, argparse (command));

	if (!strlen (cmd_buf)) {
		printf ("Syntax:\nznfscl <ip_of_znfs>\n");
		return 0;
	}

	dev_t *dev = dev_find ("/dev/vbd");

	if (dev) {
		printf ("znfscl: You are already connected to znfs server\n");
		return 0;
	}

	dev = (dev_t *) dev_register ("vbd", "Virtual Block Device", DEV_ATTR_BLOCK, (dev_handler_t *) &ide_acthandler);

	if (!dev)
		return 0;

	partition_t *p = partition_add (dev, fs_supported ("znfs"), 0);

	if (!p)
		return 0;

	/* connect to specified address */
	if (znfs_init (p, cmd_buf) != 1) {
		printf ("znfs: Wrong znfs server\n");
		return 0;
	}

	char str[16];

	sprintf (str, "/mnt/%s", p->name+5);

	mkdir (str);

	//mount (p, "", str);
	
	command_mount ("mount /dev/vbd0 /mnt/vbd0", 25);

	printf ("znfs: You are succefully connected to ZNFS server '%s'\nZNFS was mounted into '%s'\n", cmd_buf, str);
#else
	printf ("ERROR -> this architecture is not supported yet\n");
#endif
	return 1;
}

void task_savemode ()
{
  	/* disable cpu for cycle */
	for (;; schedule ())
		arch_cpu_hlt ();
}

unsigned command_savemode (char *command, unsigned len)
{
	task_t *task = (task_t *) task_create ("savemode", (unsigned) &task_savemode, 16);

	if (!task) {
		printf ("ERROR -> Save mode thread was'nt created\n");
		return 0;
	}

	printf ("Save mode was succefully started\n");

	return 1;
}

unsigned command_cpuinfo (char *command, unsigned len)
{
	printf ("Processor info:\n");

	cat ("/proc/cpuinfo");

	return 1;
}

unsigned command_adm (char *command, unsigned len)
{
	printf ("Automatic Device Mounter - init ()\n");

	init_adm ();

	return 1;
}

unsigned command_ttyctrl (char *command, unsigned len)
{
	char *parm = argparse (command);

	if (!strlen (parm)) {
		printf ("ttyctrl <operation> [parameter]\n\toperations: add; list; change <ttyid>\n");
		return 0;
	}

	if (!strncmp (parm, "add", 3)) {
		tty_t *tty = tty_create ();

		if (!tty)
			return 0;

		video_color (7, 0);

		tty_write (tty, "\nlogin: ", 8);

		printf ("New tty '%s' was created\n", tty->name);
	} else
	if (!strncmp (parm, "list", 4)) {
		tty_listview ();
	} else
	if (!strncmp (parm, "change", 6)) {
		tty_t *tty = tty_find ((char *) argparse (parm));

		if (!tty)
			return 0;

		tty_change (tty);

		printf ("\n");
	}

	return 1;
}

unsigned command_cmd (char *command, unsigned len)
{
	unsigned s = 0;
	unsigned e = 0;
	
	if (!strlen (argparse (command))) {
		printf ("cmd <command1>;<command2>;...;\n");
		return 0;
	}
	
	char *cmds = kmalloc (len - 3);
	
	if (!cmds)
		return 0;
	
	memcpy (cmds, command + 4, len - 4);
	cmds[len - 4] = '\0';
	
	unsigned i;
	for (i = 1; i < len - 4; i ++) {
		if (cmds[i] == ';') {
			cmds[i] = '\0';
			e = i-1;

			command_parser (cmds+s, e-s+1);
			
			s = i+1;
		}
	}

	kfree (cmds);
	
	return 1;
}

/*************************************************************\
|		        COMMAND's UTILS	  		      |
\*************************************************************/

unsigned command_parser (char *command, unsigned len)
{
	/* find first non-space in command */
	int i = 0;
	for (i = 0; i < len; i ++) {
		if (command[i] != ' ')
			break;
	}
	command_t *ctx;

	/* try to find the command */
	for (ctx = command_list.next; ctx != &command_list; ctx = ctx->next) {
		if (!cstrcmp (ctx->name, command+i)) {
			ctx->handler (command+i, len-i);	/* execute context function */
			return 1;
		}
	}

	printf ("%s: command not found\n", command);

	return 0;
}

void commands (int i)
{
	// currtty->shell_len has been set to zero already
	command_parser (currtty->shell, strlen(currtty->shell)); 
} 

unsigned command_register (char *name, char *desc, command_handler_t *handler, unsigned flags)
{
	command_t *c;	
	/* we dont want commands with same name */
	for (c = command_list.next; c != &command_list; c = c->next)
		if (!strcmp(c->name, name))
			return 0;

	command_t *ctx;

	// alloc and init context
	ctx = (command_t *) kmalloc (sizeof (command_t));

	if (!ctx)
		return 0;

	unsigned name_len = strlen (name);
	ctx->name = (char *) kmalloc (sizeof (char) * name_len +1);

	if (!ctx->name)
		return 0;

	memcpy (ctx->name, name, name_len);
	ctx->name[name_len] = '\0';

	unsigned desc_len = strlen (desc);
	ctx->desc = (char *) kmalloc (sizeof (char) * desc_len +1);

	if (!ctx->desc)
		return 0;

	memcpy (ctx->desc, desc, desc_len);
	ctx->desc[desc_len] = '\0';

	ctx->handler = handler;
	ctx->flags = flags;

	ctx->next = &command_list;
	ctx->prev = command_list.prev;
	ctx->prev->next = ctx;
	ctx->next->prev = ctx;

	return 1;
}

unsigned command_unregister (char *name)
{
	command_t *ctx;

	for (ctx = command_list.next; ctx != &command_list; ctx = ctx->next) {
		if (!strcmp(ctx->name, name)) {
			ctx->next->prev = ctx->prev;
			ctx->prev->next = ctx->next;

			return 1;
		}
	}
	
	return 0;
}

/*************************************************************\
|		  INITIALIZATION OF COMMANDS		      |
\*************************************************************/

unsigned int init_commands ()
{
	command_list.next = &command_list;
	command_list.prev = &command_list;

	// reg commands
	command_register ("help", "Displays list of available commands", &command_help, 0);
	command_register ("hdd", "Detect HDD", &command_hdd, 0);
	command_register ("reboot", "Reboot computer", &command_reboot, 0);
	command_register ("halt", "Shutdown computer", &command_halt, 0);
	command_register ("tasks", "Print all tasks", &command_tasks, 0);
	command_register ("ps", "Print all process", &command_ps, 0);
	command_register ("uptime", "Print uptime in seconds", &command_uptime, 0);
	command_register ("version", "Displays a version of system", &command_version, 0);
	command_register ("debug", "Change to developer mode", &command_debug, 0);
	command_register ("mount", "Mount device to selected directory", &command_mount, 0);
	command_register ("umount", "Unmount mounted directory", &command_umount, 0);
	command_register ("env", "Displays all env variables", &command_env, 0);
	command_register ("cd", "Change directory", &command_cd, 0);
	command_register ("ls", " Displays files in current directory", &command_ls, 0);
	command_register ("cat", "Displays text in selected file", &command_cat, 0);
	command_register ("cp", "Copy a file", &command_cp, 0);
	command_register ("rm", "Remove a file", &command_rm, 0);
	command_register ("mkdir", "Create a directory", &command_mkdir, 0);
	command_register ("touch", "Create a file", &command_touch, 0);
	command_register ("exec", "Execute a selected program", &command_exec, 0);
	command_register ("lsdev", "Displays found devices", &command_lsdev, 0);
	command_register ("login", "Login as another user", &command_login, 0);
	command_register ("fdisk", "Partition table manipulator", &command_fdisk, 0);
	command_register ("hdcat", "Read selected block of data from drive", &command_hdcat, 0);
	command_register ("free", "Display amount of free and used memory", &command_free, 0);
	command_register ("top", "Stats of cpu usage", &command_top, 0);
	command_register ("date", "Show current date and time", &command_date, 0);
	//command_register ("xmastime", "play noels on pc-speaker", &command_spk, 0);
	command_register ("test", "Some test", &command_test, 0);
	//command_register ("test2", "Some test", &command_test2, 0);
	//command_register ("test3", "Some test", &command_test3, 0);
	//command_register ("testd", "Some test", &command_test4, 0);
	//command_register ("vesa", "Some test", &command_vesa, 0);
	command_register ("kill", "Send a signal to process", &command_kill, 0);
	command_register ("modprobe", "program to add modules from the kernel", &command_modprobe, 0);
	command_register ("lsmod", "program to show the status of modules", &command_lsmod, 0);
#ifdef CONFIG_DRV_PCI
	command_register ("lspci", "list all pci devices", &command_lspci, 0);
#endif
	command_register ("mouse", "ps2 mouse test", &command_mouse, 0);
	command_register ("iflist", "Show network interface", &command_iflist, 0);
	command_register ("ifconfig", "Configure network interface", &command_ifconfig, 0);
	command_register ("ifroute", "Configure gateway address", &command_ifroute, 0);
	command_register ("dnsconfig", "Configure domain name server address", &command_dnsconfig, 0);
	command_register ("dhcpcl", "Configure interface using DHCP", &command_dhcpcl, 0);
#ifdef CONFIG_PROTO_TUN6
	command_register ("tunconfig", "Configure IPv6 tunnel server address", &command_tunconfig, 0);
#endif
	command_register ("hostname", "Configure network hostname", &command_hostname, 0);
	command_register ("kbdmap", "Change keyboard layout", &command_kbdmap, 0);
#ifdef CONFIG_DRV_ZEXFS
	command_register ("mkzexfs", "Create zexfs filesystem on hdd", &command_mkzexfs, 0);
#endif
#ifdef CONFIG_DRV_EXT2
	command_register ("mkext2", "Create ext2 filesystem on hdd", &command_mkext2, 0);
#endif
#ifdef CONFIG_PROTO_IPV4
	command_register ("ping", "Send ICMP echo request", &command_ping, 0);
#endif
#ifdef CONFIG_PROTO_IPV6
	command_register ("ping6", "Send ICMPv6 echo request", &command_ping6, 0);
#endif
	command_register ("netexec", "Execute file from network", &command_netexec, 0);
	command_register ("netcp", "Copy file from network to filesystem", &command_netcp, 0);
#ifdef CONFIG_DRV_ZNFS
	command_register ("znfscl", "Connect to znfs server - network filesystem", &command_znfscl, 0);
#endif
	command_register ("savemode", "Make less cpu load", &command_savemode, 0);
	command_register ("cpuinfo", "Show CPU info", &command_cpuinfo, 0);

	command_register ("adm", "Automatic Device Mounter", &command_adm, 0);
	command_register ("ttyctrl", "TTY Console control", &command_ttyctrl, 0);
	command_register ("cmd", "Execute command sequence", &command_cmd, 0);
	
	return 1;
}
