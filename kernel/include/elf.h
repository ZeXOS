/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ELF_H
#define _ELF_H

#define	__PACKED__ __attribute__ ((__packed__))


typedef struct
{
	unsigned long sect_name		__PACKED__;
	unsigned long type		__PACKED__;
	unsigned long flags		__PACKED__;
	unsigned long virt_adr		__PACKED__;
	unsigned long offset		__PACKED__;
	unsigned long size		__PACKED__;
	unsigned long link		__PACKED__;
	unsigned long info		__PACKED__;
	unsigned long align		__PACKED__;
	unsigned long ent_size		__PACKED__;
} elf_sect_t;

typedef struct
{
	unsigned long adr		__PACKED__;
	unsigned char type;
	unsigned long symtab_index : 24 __PACKED__;
	unsigned long addend		__PACKED__;
} elf_reloc_t;

typedef struct
{
	unsigned long name		__PACKED__;
	unsigned long value		__PACKED__;
	unsigned long size		__PACKED__;
	unsigned type : 4		__PACKED__;
	unsigned binding : 4		__PACKED__;
	unsigned char zero;
	unsigned short section		__PACKED__;
} elf_sym_t;

typedef struct
{
	unsigned long magic;
	unsigned char bitness;
	unsigned char endian;
	unsigned char elf_ver_1;
	unsigned char res[9];
	unsigned short file_type	__PACKED__;
	unsigned short machine		__PACKED__;
	unsigned long elf_ver_2		__PACKED__;
	unsigned long entry_pt		__PACKED__;
	unsigned long phtab_offset	__PACKED__;
	unsigned long shtab_offset	__PACKED__;
	unsigned long flags		__PACKED__;
	unsigned short file_hdr_size	__PACKED__;
	unsigned short phtab_ent_size	__PACKED__;
	unsigned short num_phtab_ents	__PACKED__;
	unsigned short shtab_ent_size	__PACKED__;
	unsigned short num_sects		__PACKED__;
	unsigned short shstrtab_index	__PACKED__;
} elf_file_t;


/* externs */

#endif
