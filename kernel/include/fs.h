/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _FS_H
#define _FS_H

#include <system.h>
#include <dev.h>
#include <partition.h>

#define FS_NAME_LEN	11

#define	FS_ACT_INIT		0x1
#define	FS_ACT_READ		0x2
#define	FS_ACT_WRITE		0x4
#define	FS_ACT_MKDIR		0x8
#define	FS_ACT_TOUCH		0x10
#define	FS_ACT_RMDIR		0x20
#define	FS_ACT_RM		0x40
#define	FS_ACT_CHDIR		0x80
#define	FS_ACT_EXIT		0x100

typedef bool (fs_handler_t) (unsigned act, char *block, unsigned n, unsigned long l);

/* Filesystem structure */
typedef struct fs_context {
  struct fs_context *next, *prev;

  char *name;
  fs_handler_t *handler;
} fs_t;

/* externs */
//extern bool fs_list_add (char *name, fs_handler_t *handler);
extern fs_t *fs_supported (char *name);
//extern fs_t *fs_detect (partition_t *p);
extern unsigned int init_fs ();

#endif
