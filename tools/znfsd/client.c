#include <stdio.h>
#include <stdlib.h>
#include "client.h"

client_t client_list;

int client_send (client_t *c, char *data, unsigned len)
{
	return net_send (c->fd, data, len);
}

int client_add (int fd)
{
	// alloc and init context
	client_t *ctx = (client_t *) malloc (sizeof (client_t));

	if (!ctx)
		return 0;

	ctx->fd = fd;
	ctx->state = CLIENT_STATE_CONNECTED;

	ctx->next = &client_list;
	ctx->prev = client_list.prev;
	ctx->prev->next = ctx;
	ctx->next->prev = ctx;

	return 1;
}

int client_handle (client_t *c, char *data, unsigned len)
{
	if (c->state == CLIENT_STATE_CONNECTED) {
		data[len] = '\0';
		return proto_handle (c, data, len);
	} else if (c->state == CLIENT_STATE_DISCONNECTED) {
		/* disconnect socket */
		close (c->fd);

		/* delete client_t * struct from list */
		c->next->prev = c->prev;
		c->prev->next = c->next;

		free (c);
		return 0;
	}

	return 1;
}

int client_init ()
{
	client_list.next = &client_list;
	client_list.prev = &client_list;

	return 1;
}
