/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <mount.h>
#include <partition.h>

/**
 *	Automatic Device Mounter
 *	Daemon for mounting	
 *
 */

unsigned init_adm ()
{
	char dev[10];
	memcpy (dev, "/dev/cd 0", 9);
	dev[9] = '\0';
	
	char *mp = "/mnt/cdrom/";
	
	char id;
	for (id = 'a'; id != 'e'; id ++) {
		dev[7] = id;

		partition_t *p = partition_find (dev);
		
		if (p) {
			int r = mount (p, "", mp);

			if (r)
				printf ("CD-ROM drive '%s' mounted succefully to %s\n", dev, mp);
			break;
		}
	}
	
	return 1;
}
