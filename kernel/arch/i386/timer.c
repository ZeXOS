/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>

/* This will keep track of how many ticks that the system
*  has been running for */
unsigned long timer_ticks = 0;

/* Software timer - ticks count to 1ms */
unsigned long timer_swticks = 0;

/* Handles the timer. In this case, it's very simple: We
*  increment the 'timer_ticks' variable every time the
*  timer fires. By default, the timer fires 18.222 times
*  per second. Why 18.222Hz? Some engineer at IBM must've
*  been smoking something funky */
void timer_handler (struct regs *r)
{
	/* Increment our 'tick count' */
	timer_ticks ++;
	
	/* Every 18 clocks (approximately 1 second), we will
	*  display a message on the screen */
	/*    if (timer_ticks % (HZ/18) == 0)
	{
		kprintf (": %d\n", timer_ticks);
	}*/

	//if (timer_ticks > 200)
	//	schedule ();
}

/* This will continuously loop until the given time has
*  been reached */
void timer_wait (int ticks)
{
	int eticks;
	
	eticks = timer_ticks + ticks;
		
	while (timer_ticks < eticks)
		schedule ();
}

char *time_tostring (unsigned long s)
{
	static char buffer[512];
	unsigned int weeks, days, hours, minutes, seconds, total;
	
	seconds = s % 60;
	minutes = s / 60;
	hours = minutes / 60;
	days = hours / 24;
	weeks = days / 7;
	minutes = minutes % 60;
	hours = hours % 24;
	days = days % 7;
	
	total = 0;
	
	if (weeks)
		total += sprintf (buffer, "%u week%s, ", weeks, (weeks > 1) ? "s" : "");
	if (days)
		total += sprintf (buffer + total, "%u day%s, ", days, (days > 1) ? "s" : "");
	
	if ((hours || minutes || seconds) || (!(weeks || days)))
		total += sprintf (buffer + total, "%2uh %2umin %2usec", hours, minutes, seconds);
	
	return buffer;
}

void uptime (void)
{
	printf ("Uptime: %s\n", time_tostring (timer_ticks/((3579545L / 3) / HZ)));
}

void calc_freqency ()
{
	/* Kernel with 1000Hz timer - dont need it ? */
	if (kernel_attr & KERNEL_18HZ) {
		int eticks;
		
		eticks = timer_ticks + 18;
	
		while (timer_ticks < eticks) {
			ctps ++;
			kprintf("");;
		}
	
		kprintf ("Freqency: %lluHz\n", ctps);
	}
}

void calc_swtimer ()
{
	unsigned long timer = timer_ticks;

	while ((timer_ticks - timer) >= 1)
		timer_swticks ++;
}

void usleep (unsigned len)
{
	if (kernel_attr & KERNEL_18HZ) {
		unsigned long long t = ctps * len;
		unsigned long long i = 0;
	
		while (t > i) {
			i += 1000;
			kprintf("");;
		}
	} else
		timer_wait (1);
}

void init_8253 ()
{
	unsigned foo = 0;

	if (kernel_attr & KERNEL_18HZ) {	// Dont set new timer frequency, when is setted KERNEL_18HZ
		kprintf ("Warning -> kernel is set to 18HZ timer frequency !\n");
		foo = (3579545L / 3) / 18;
		return;
	} else
		foo = (3579545L / 3) / HZ;

	/* reprogram the 8253 timer chip to run at 'HZ', instead of 18 Hz */
	outb (0x43, 0x36);	/* channel 0, LSB/MSB, mode 3, binary */
	outb (0x40, (unsigned short) foo & 0xFF);	/* LSB */
	outb (0x40, (unsigned short) foo >> 8);	/* MSB */
}

/* Sets up the system clock by installing the timer handler into IRQ0 */
unsigned int timer_install ()
{
	/* set hw timer (irq0) to HZ frequency */
	init_8253 ();

	/* Installs 'timer_handler' to IRQ0 */
	irq_install_handler (0, timer_handler);

	return 1;
}
