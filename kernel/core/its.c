/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Interval Time Scheduler - NOTE: it's not a task/process scheduler */

#include <system.h>
#include <its.h>

its_t its_list;

extern unsigned long timer_ticks;

unsigned int its_unregister (its_t *its)
{
	if (!its)
		return 0;
	
	its->next->prev = its->prev;
	its->prev->next = its->next;

	kfree (its);
  
	return 1;
}

unsigned task_its ()
{
	for (;; schedule ()) {
		its_t *its;
		for (its = its_list.next; its != &its_list; its = its->next)
			if ((timer_ticks-its->timer) >= its->interval) {
				if (!its->handler (its->arg)) {
					its_unregister (its);
					break;
				}
				
				its->timer = timer_ticks;
			}
	}

	return 1;
}

unsigned int its_register (unsigned long interval, its_handler_t *handler, void *arg)
{
	its_t *its = its_list.next;
	/* create ITS' task when it's really needed */
	if (its == &its_list)
		task_create ("its", (unsigned) task_its, 255);

	/* alloc and init context */
	its = (its_t *) kmalloc (sizeof (its_t));
	
	if (!its)
		return 0;
	
	its->timer = timer_ticks;
	its->interval = interval;
	its->handler = handler;
	its->arg = arg;
	
	/* add into list */
	its->next = &its_list;
	its->prev = its_list.prev;
	its->prev->next = its;
	its->next->prev = its;

	DPRINT (DBG_CORE, "its_register () -> 0x%x every %dms", handler, interval);
	
	return 1;
}

#ifdef TEST
unsigned int test_its_handler (void *arg)
{
	if (arg)
		kprintf ("%s", arg);
  
	return 1;
}
#endif

unsigned int init_its ()
{
	its_list.next = &its_list;
	its_list.prev = &its_list;
	
#ifdef TEST
	its_register (10000, &test_its_handler, "test");
#endif
	return 1;
}
