/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <elf.h>

unsigned arch_elf_detect (elf_file_t *file)
{
	if(file->magic != 0x464C457FL) { /* "ELF" */
		kprintf ("File is not relocatable ELF; has bad magic value "
			"0x%lX (should be 0x464C457F)\n", file->magic);
		return 0;
	}

	if(file->bitness == 1) {
		kprintf ("File is 32-bit ELF, not 64-bit\n");
		return 0;
	}

	if(file->endian != 1) {
		kprintf ("File is big endian ELF, not little\n");
		return 0;
	}

	if(file->elf_ver_1 != 1) {
		kprintf ("File has bad ELF version %u\n", file->elf_ver_1);
		return 0;
	}

	if(file->file_type != 1 && file->file_type != 2) {
		kprintf ("File is not relocatable ELF (could be "
			"executable, DLL, or core file) - currently is %d\n", file->file_type);
		return 0;
	}

	if(file->machine == 3) {
		kprintf ("File is i386 ELF\n");
		return 0;
	}

	if(file->elf_ver_2 != 1) {
		kprintf ("File has bad ELF version %lu\n", file->elf_ver_2);
		return 0;
	}

	return 1;
}
