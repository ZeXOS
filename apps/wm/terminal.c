/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "window.h"
#include "cursor.h"
#include "button.h"
#include "menu.h"
#include "dialog.h"
#include "filemanager.h"
#include "config.h"

unsigned terminal_act = 0;

char *term_base = 0;
wmwindow *term_window;

static unsigned i = 0;
static unsigned j = 0;
static unsigned k = 0;

unsigned iterminal ()
{
	if (terminal_act)
		return 0;

	terminal_act = 1;

	asm volatile (
		"movl $41, %%eax;"
	     	"int $0x80;"
		"movl %%eax, %0;"
		: "=b" (term_base) :: "%eax");

	if (!term_base)
		return 0;
	
	term_window = window_create ("Terminal");

	return 1;
}

unsigned iterminal_exit ()
{
	if (!terminal_act)
		return 0;

	terminal_act = 0;

	/* change current tty to process tty */
	asm volatile (
		"movl $39, %eax;"
	     	"int $0x80;");

	if (term_window) {
		unsigned ret = window_delete (term_window);

		term_window = 0;

		return ret;
	}

	return 0;
}

unsigned terminal_handle ()
{
	if (!terminal_act)
		return 0;

	i = 0;
	j = 0;
	k = 0;

	if (term_window != 0) {
		while (i < 2000) {
			switch (term_base[i*2+1]) {
				case '\0':
					break;
				case '\n':
					/* parse command */
					if (j > 0) {
						if (term_base[i*2-7] == 'e')
						if (term_base[i*2-5] == 'x')
						if (term_base[i*2-3] == 'i')
						if (term_base[i*2-1] == 't')
							iterminal_exit ();
					}

					k ++;
					j = 0;
					break;
				case '\b':
					j --;
					xrectfill (term_window->x+1+(j*5), term_window->y+11+(k*9), term_window->x+6+(j*5), term_window->y+19+(k*9), 0xffffff);
					break;
				default:
					xtext_putch (term_window->x+1+(j*5), term_window->y+11+(k*9), term_base[i*2], term_base[i*2+1]);
		
					j ++;

					if (j*5 > term_window->size_x-6) {
						term_window->size_x += j*5;
						j = 0;
						k ++;
					}

					if (k*9 > term_window->size_y)
						term_window->size_y += k*9;

					break;
			}
		
			i ++;
		}
	}

	return 1;
}

unsigned init_terminal ()
{
	term_window = 0;
	terminal_act = 0;

	return 1;
}
