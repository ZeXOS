/*
**
**  PCNet-PCI (Lance) network card implementation, derived from
**  AMD whitepaper's pseudo-code at:
**  http://www.amd.com/us-en/assets/content_type/white_papers_and_tech_docs/19669.pdf
**  
**  (c)2001 Graham Batty
**  License: NewOS License
**
*/

#ifndef _PCNET32_DEV_H
#define _PCNET32_DEV_H

/* AMD Vendor ID */
#define AMD_VENDORID                          0x1022

/* PCNet/Home Device IDs */
#define PCNET_DEVICEID                        0x2000
#define PCHOME_DEVICEID                       0x2001

/* externs */
extern bool pcnet32_acthandler (unsigned act, char *block, unsigned block_len);

#endif
