/*
 *  ZeX/OS
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _BITMAP_H
#define _BITMAP_H

typedef struct {
	unsigned short x;
	unsigned short y;
	unsigned short resx;
	unsigned short resy;
	
	unsigned short posx;
	unsigned short posy;
	unsigned short sizex;
	unsigned short sizey;
	
	unsigned short *data;
} zbitmap_t;

#endif
