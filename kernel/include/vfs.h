/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _VFS_H
#define _VFS_H

#define VFS_FILEATTR_FILE	0x1
#define VFS_FILEATTR_DIR	0x2
#define VFS_FILEATTR_HIDDEN	0x4
#define VFS_FILEATTR_SYSTEM	0x8
#define VFS_FILEATTR_BIN	0x10
#define VFS_FILEATTR_READ	0x20
#define VFS_FILEATTR_WRITE	0x40
#define VFS_FILEATTR_MOUNTED	0x80
#define VFS_FILEATTR_DEVICE	0x100

#define VFS_FILENAME_LEN	10
#define VFS_MOUNTPOINT_LEN	32

/* data container for virtual files */
typedef struct {
	char *ptr;
	unsigned long len;
} vfs_content_t;

/* Virtual filesystem structure */
typedef struct vfs_context {
	struct vfs_context *next, *prev;

	char *name;
	char mountpoint[VFS_MOUNTPOINT_LEN];
	unsigned attrib;
	vfs_content_t *content;
} vfs_t;

/* structure for directory entries */
typedef struct {
	char *name;
	unsigned attrib;
	unsigned char next;
	unsigned char unused;
} __attribute__ ((__packed__)) vfs_dirent_t;

/* externs */
extern vfs_t *vfs_list_find (char *name, char *mountpoint);
extern vfs_t *vfs_list_findbymp (char *mountpoint);
extern vfs_t *vfs_find (char *file, unsigned file_len);
extern vfs_t *vfs_list_add (char *name, unsigned attrib, char *mountpoint);
extern bool vfs_list_del (vfs_t *vfs);
extern bool vfs_list_delbymp (char *mountpoint);
extern int vfs_mmap (char *file, unsigned file_len, vfs_content_t *content);
extern int vfs_read (char *file, unsigned file_len, vfs_content_t *content);
extern int vfs_cat (char *file, unsigned file_len);
extern int vfs_cp (char *what, unsigned what_len, char *where, unsigned where_len);
extern int vfs_ls (char *file, unsigned file_len);
extern int vfs_cd (char *file, unsigned file_len);
extern int vfs_mkdir (char *file, unsigned file_len);
extern int vfs_touch (char *file, unsigned file_len);
extern int vfs_rm (char *file, unsigned file_len);
extern vfs_dirent_t *vfs_dirent ();
extern unsigned int init_vfs ();

#endif
