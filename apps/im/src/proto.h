/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PROTO_H
#define _PROTO_H

extern int proto_msg (char *target, char *msg, unsigned len);
extern int proto_register (char *nick, char *password);
extern int proto_login (char *nick, char *password);
extern int proto_handler (char *str, unsigned len);
extern int proto_status (char *msg, unsigned len);
extern int proto_add (char *target);
extern int proto_del (char *target);
extern int proto_list ();
extern int init_proto ();

#endif
