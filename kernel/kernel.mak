.SUFFIXES: .asm

# defines
MAKEFILE=Makefile
MAKEDEP=$(MAKEFILE)
LDSCRIPT=arch/main/link.ld
INCDIR	=include/
NASM	=nasm -f elf
CFLAGS	=-g -O2 -nostdinc -fno-builtin -fno-stack-protector -I$(INCDIR) -Ibuild/main/
LDFLAGS	=-g -T $(LDSCRIPT) -nostdlib

LIBC    =lib/libc.a
LARCH	=arch/arch.a

ifeq ($(ARCH),arm)
	CFLAGS=-g -Os -fno-builtin -ffreestanding -nostdinc -pipe -mcpu=arm926ej-s -mabi=apcs-gnu -nostdlib -nostartfiles -nodefaultlibs  -ffixed-r8 -msoft-float -Wno-unused-parameter -I$(INCDIR) -Ibuild/main/
endif

# object/source files
BOOT	=arch/main/boot/start.o
CORE	=core/main.o core/init.o core/console.o core/commands.o core/mm/kmem.o core/mm/kzmem.o core/mm/dlmem.o core/mm/swmem.o core/mm/pmem.o core/mm/usermem.o core/env.o core/elf.o core/dev.o core/vfs.o core/fs.o core/user.o core/sched.o core/proc.o core/tty.o core/partition.o core/module.o core/spinlock.o core/mutex.o core/smp.o core/ioctl.o core/fd.o core/errno.o core/cache.o core/its.o
NET	=core/net/socket.o core/net/ips.o core/net/eth.o core/net/if.o core/net/arp.o core/net/packet.o core/net/tcp.o core/net/tcp6.o core/net/ip.o core/net/ipv6.o core/net/icmp.o core/net/icmp6.o core/net/ndp.o core/net/checksum.o core/net/udp.o core/net/udp6.o core/net/dns.o core/net/tun6.o core/net/unix.o core/net/hostname.o core/net/dhcp.o
SOUND	=core/sound/audio.o
DRIVERS	=drivers/char/video/video.o drivers/char/video/vesa.o drivers/char/video/gtext.o drivers/char/video/bga.o drivers/char/kbd/kbd.o drivers/char/rs232/rs232.o drivers/char/sound/speaker.o drivers/char/sound/sb16.o drivers/char/sound/es1370.o drivers/char/sound/ac97.o drivers/block/floppy.o drivers/block/drive.o drivers/fs/fat.o drivers/fs/fat16.o drivers/fs/zexfs.o drivers/fs/ext2.o drivers/fs/isofs.o drivers/fs/znfs.o drivers/bus/pci/pci.o drivers/bus/usb/usb.o drivers/net/pcnet32/pcnet32.o drivers/net/rtl8029/rtl8029.o drivers/net/rtl8139/rtl8139.o drivers/net/rtl8169/rtl8169.o drivers/char/mouse/com.o drivers/char/mouse/ps2.o
UTILS	=utils/fs/ls.o utils/fs/cat.o utils/fs/cp.o utils/fs/rm.o utils/fs/cd.o utils/fs/mount.o utils/fs/mkdir.o utils/fs/fdisk.o utils/fs/touch.o utils/net/tftp.o utils/adm/adm.o

OBJS	=$(BOOT) $(CORE) $(NET) $(SOUND) $(DRIVERS) $(UTILS)

ifneq ($(strip $(wildcard .config)),)

# targets
all: $(OBJS) kernel.bin

endif

install: kernel.bin
	mount /dev/fd0 mnt
	cp -f kernel.bin mnt/kernel.bin
	umount mnt
	qemu -fda /dev/fd0

clean:
	$(Q)make clean -C lib
	$(Q)make clean -C arch/main
	$(Q)rm include/arch
	$(Q)rm -f *.o kernel.bin .directory .kconfig.d .tmpconfig.h .config .config.old arch/main $(OBJS)
	$(Q)rm -rf build/main/utils build/main/config.h build/main/build.h build/main build/utils
	@printf "  CLEAN\n";

# implicit rules
.S.o:
	@printf " ASM  $(subst $(shell pwd)/,,$(@))\n";
	$(Q)$(CC) -c -o$@ $<

.s.o:
	@printf " ASM  $(subst $(shell pwd)/,,$(@))\n";
	$(Q)$(NASM) -o$@ $<

.c.o:
	@printf "  CC  $(subst $(shell pwd)/,,$(@))\n";
	$(Q)$(CC) $(CFLAGS) -c -o$@ $<

# explicit rules
kernel.bin: $(OBJS) $(LARCH) $(LDSCRIPT) $(LIBC) $(MAKEDEP)
	@printf "  LD  $(subst $(shell pwd)/,,$(@))\n";
	$(Q)$(LD) $(LDFLAGS) -o$@ $(OBJS) $(LARCH) $(LIBC)
