/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _XCURSOR_H
#define _XCURSOR_H

#define	XCURSOR_STATE_RBUTTON		0x1
#define	XCURSOR_STATE_LBUTTON		0x2
#define	XCURSOR_STATE_BBUTTON		0x4

#define XCURSOR_MOUSE_AUTO		-1
#define XCURSOR_MOUSE_KB		0x0
#define XCURSOR_MOUSE_SERIAL		0x1
#define XCURSOR_MOUSE_PS2		0x2
#define XCURSOR_MOUSE_USB		0x4

extern unsigned xcursor_init ();
extern int xcursor_update ();
extern void xcursor_getpos (int *x, int *y);
extern unsigned xcursor_settype (unsigned char type);

#endif
