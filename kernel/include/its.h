/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ITS_H
#define _ITS_H

typedef unsigned (its_handler_t) (void *arg);

/* Interval Time Scheduler  */
typedef struct its_context {
	struct its_context *next, *prev;

	unsigned long interval;
	unsigned long timer;
	its_handler_t *handler;
	void *arg;
} its_t;

/* externs */
extern unsigned int its_register (unsigned long interval, its_handler_t *handler, void *arg);
extern unsigned int init_its ();

#endif
