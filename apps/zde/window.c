/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>

#include "icon.h"
#include "dialog.h"
#include "window.h"
#include "cursor.h"

unsigned short *bitmap_border;

zde_win_t zde_win_list;
zde_win_t *zde_win_last;
unsigned short zde_win_newx;
unsigned short zde_win_newy;

bitmap_coord_t *bitmap_coord;

extern zde_cursor_t *zde_cursor;

zde_win_t *create_window (char *name, unsigned char type)
{
	zde_win_t *window = (zde_win_t *) malloc (sizeof (zde_win_t));

	if (!window)
		return 0;

	window->x = zde_win_newx += 50;
	window->y = zde_win_newy += 50;
	window->sizex = 300;
	window->sizey = 200;

	if (zde_win_newx+window->sizex > 800)
		zde_win_newx = 0;
	if (zde_win_newy+window->sizey > 600)
		zde_win_newy = 0;

	window->name = strdup (name);
	window->name_len = strlen (name);

	window->state = 0;
	window->objectid = 0;
	window->type = type;

	/* add into list */
	window->next = &zde_win_list;
	window->prev = zde_win_list.prev;
	window->prev->next = window;
	window->next->prev = window;

	zde_win_last = window;

	return window;
}

unsigned destroy_window (zde_win_t *window)
{
	if (!window)
		return 0;

	window->next->prev = window->prev;
	window->prev->next = window->next;

	window->state |= WINDOW_STATE_CLOSE;

	return 1;
}

unsigned active_window (zde_win_t *window)
{
	if (!window)
		return 0;

	window->next->prev = window->prev;
	window->prev->next = window->next;

	/* add into list */
	window->next = &zde_win_list;
	window->prev = zde_win_list.prev;
	window->prev->next = window;
	window->next->prev = window;

	zde_win_last = window;

	return 1;
}

zde_win_t *isactive_window ()
{
	return zde_win_last;
}

static void draw_bitmap (unsigned short x, unsigned short y)
{
	unsigned i = 0;
	unsigned j = 0;
	unsigned k = 0;
	unsigned pix = 0;

	unsigned short w = bitmap_border[1024+34];

	for (i = bitmap_coord->x+(bitmap_coord->y*32); i < 32*32; i ++) {
		if (k >= 32) {
			j ++;
			k = 0;
		}
		
		pix = (1024-i)+34;

		if (pix > 2100)
			continue;

		if (bitmap_border[pix] != w)
		if (k <= bitmap_coord->sizex && j <= bitmap_coord->sizey)
			xpixel ((unsigned) x+k, (unsigned) y+j, (unsigned) bitmap_border[pix]);

		k ++;
	}
}

void draw_rectfill (unsigned x1, unsigned y1, unsigned x2, unsigned y2, unsigned color)
{
	if (x1 > x2) {
		int temp = x1;
		x2 = temp;
		x1 = x2;
	}
	if (y1 > y2) {
		int temp = y1;
		y2 = temp;
		y1 = y2;
	}

	int delkax = (signed) x2 - (signed) x1;
	int delkay = (signed) y2 - (signed) y1;

	int a = 0;
	int b = 0;

	for (a = 0; a <= delkax; a ++)
		for (b = 0; b <= delkay; b ++) {
			unsigned c = xpixelget (a+x1, b+y1);

			if (c > 20)
				c /= 2000;

			xpixel (a+x1, b+y1, color - c);
		}
}

int draw_window ()
{
	zde_win_t *window;
	for (window = zde_win_list.next; window != &zde_win_list; window = window->next) {
		/* we click on left button */
		if (zde_cursor->state == XCURSOR_STATE_LBUTTON) {
			zde_win_t *curr = 0;
			zde_win_t *cw;
			for (cw = zde_win_list.next; cw != &zde_win_list; cw = cw->next) {
				if (zde_cursor->x >= cw->x && zde_cursor->x <= (cw->x+cw->sizex))
				if (zde_cursor->y >= cw->y && zde_cursor->y <= (cw->y+cw->sizey)) {
					curr = cw;
				}

			}

			/* we cant move with any window */
			if (!(zde_cursor->flags & CURSOR_FLAG_MOVE))
			if (curr) {
				/* exit window */
				if (zde_cursor->x >= (curr->x+curr->sizex-18) && zde_cursor->x <= (curr->x+curr->sizex-6))
				if (zde_cursor->y >= curr->y+3 && zde_cursor->y <= curr->y+17) {
					zde_cursor->state = 0;

					destroy_window (curr);
					destroy_icon_window (curr);
					destroy_dialog_object (curr);
					break;
				}

				/* window header */
				if (zde_cursor->x >= curr->x && zde_cursor->x <= (curr->x+curr->sizex))
				if (zde_cursor->y >= curr->y && zde_cursor->y <= (int) (curr->y+22)) {
					curr->state |= WINDOW_STATE_MOVE;
					zde_cursor->flags |= CURSOR_FLAG_MOVE;
				}

				/* make active window (front of screen) */
				if (curr != zde_win_last) {
					active_window (curr);
					break;
				}
			}
		} else  {
			window->state &= ~WINDOW_STATE_MOVE;
			zde_cursor->flags &= ~CURSOR_FLAG_MOVE;
		}

		/* when some window is moving, we need update postitions of it */
		if (window->state & WINDOW_STATE_MOVE) {
			window->x = zde_cursor->x;
			window->y = zde_cursor->y;
		}

		/* it's classical window */
		if (window->type == 0) {
			unsigned x = window->x+window->sizex-4;
			unsigned y = window->y+window->sizey-3;

			/* window background */
			draw_rectfill (window->x+4, window->y+21, x, y, ~0);
			//xrectfill (window->x+4, window->y+21, window->x+window->sizex-4, window->y+window->sizey-3, ~0-0xf);
			/* vykreslime vsechny ikony naseho okna */
			draw_icon_window (window);
		/* it's dialog window */
		} else if (window->type == 1) {
			unsigned x = window->x+window->sizex-4;
			unsigned y = window->y+window->sizey-3;

			/* window background */
			draw_rectfill (window->x+4, window->y+21, x, y, ~0-0x1);

			/* let's draw all objects of this window */
			draw_dialog_object (window);
		}
		
		/* draw window's buttons */
		draw_button_window (window);

		bitmap_coord->x = 0;
		bitmap_coord->y = 0;
		bitmap_coord->sizex = 4;
		bitmap_coord->sizey = 21;

		/* vrchni roh vlevo */
		draw_bitmap (window->x, window->y);

		bitmap_coord->x = 7;
		bitmap_coord->y = 0;
		bitmap_coord->sizex = 24;
		bitmap_coord->sizey = 21;

		/* vrchni roh vpravo */
		draw_bitmap (window->x+window->sizex-24, window->y);

		bitmap_coord->x = 6;
		bitmap_coord->y = 0;
		bitmap_coord->sizex = 1;
		bitmap_coord->sizey = 21;

		/* vrchni cast */
		int l;
		for (l = 0; l < window->sizex-24; l ++)
			draw_bitmap (window->x+4+l, window->y);

		bitmap_coord->x = 0;
		bitmap_coord->y = 8;
		bitmap_coord->sizex = 4;
		bitmap_coord->sizey = 1;

		/* bocni leva cast */
		for (l = 0; l < window->sizey-24; l ++)
			draw_bitmap (window->x, window->y+l+21);

		bitmap_coord->x = 27;
		bitmap_coord->y = 8;
		bitmap_coord->sizex = 4;
		bitmap_coord->sizey = 1;

		/* bocni prava cast */
		for (l = 0; l < window->sizey-24; l ++)
			draw_bitmap (window->x+window->sizex-4, window->y+l+21);

		bitmap_coord->x = 0;
		bitmap_coord->y = 25;
		bitmap_coord->sizex = 5;
		bitmap_coord->sizey = 4;

		/* dolni levy roh */
		draw_bitmap (window->x, window->y+window->sizey-2);

		bitmap_coord->x = 26;
		bitmap_coord->y = 25;
		bitmap_coord->sizex = 5;
		bitmap_coord->sizey = 4;

		/* dolni pravy roh */
		draw_bitmap (window->x+window->sizex-5, window->y+window->sizey-2);

		bitmap_coord->x = 7;
		bitmap_coord->y = 25;
		bitmap_coord->sizex = 1;
		bitmap_coord->sizey = 4;

		/* dolni cast */
		for (l = 0; l < window->sizex-11; l ++)
			draw_bitmap (window->x+5+l, window->y+window->sizey-2);

		if (window->name) {
			if (window->sizex > (window->name_len * 6))
				xtext_puts (window->x+8, window->y+5, 0, window->name);
		}
	}

	return 0;
}

int init_window ()
{
	zde_win_list.next = &zde_win_list;
	zde_win_list.prev = &zde_win_list;

	zde_win_newx = 50;
	zde_win_newy = 50;

	bitmap_coord = (bitmap_coord_t *) malloc (sizeof (bitmap_coord_t));

	if (!bitmap_coord)
		return -1;

	/* BORDER bitmap */
	bitmap_border = (unsigned short *) malloc (2120);

	if (!bitmap_border)
		return -1;

	memset (bitmap_border, 0, 2048+70);

	int fd = open ("border", O_RDONLY);
	
	if (!fd) {
		puts ("error -> file 'border' not found\n");
		return -1;
	}
	
	if (!read (fd, (unsigned char *) bitmap_border, 2048+70)) {
		puts ("error -> something was wrong !\n");
		return -1;
	}

	return 0;
}
