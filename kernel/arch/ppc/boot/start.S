#define EXP(f) .global f; f:
 
.text			@ text section

EXP(_start)		@ _start function
	
	b	reset
	b	arm_undefined
	b	arm_syscall
	b	arm_prefetch_abort
	b	arm_data_abort
	b	arm_reserved
	b	arm_irq
	b	arm_fiq

reset:			@ reset function

#if ARM_WITH_CP15
	mrc		p15, 0, r0, c1, c0, 0

	bic		r0, r0, #(1<<15 | 1<<13 | 1<<12)
	bic		r0, r0, #(1<<2 | 1<<0)

	orr		r0, r0, #(1<<1)
	mcr		p15, 0, r0, c1, c0, 0
#endif

#if PLATFORM_OMAP3	
	mov 	r12, #1
	.word 0xe1600070
#endif

	mov	r0, pc
	sub	r0, r0, #(.Laddr - _start)
.Laddr:
	ldr	r1, =_start
	cmp	r0, r1
	beq	.Lstack_setup

	ldr	r2, =__data_end	

.Lrelocate_loop:
	ldr	r3, [r0], #4
	str	r3, [r1], #4
	cmp	r1, r2
	bne	.Lrelocate_loop

	ldr	r0, =.Lstack_setup
	bx	r0

.ltorg

.Lstack_setup:
	mrs     r0, cpsr
	bic     r0, r0, #0x1f

	ldr	r2, =abort_stack_top
	orr     r1, r0, #0x12 		@ irq
	msr     cpsr_c, r1
	ldr	r13, =irq_save_spot
	    
	orr     r1, r0, #0x11 		@ fiq
	msr     cpsr_c, r1
	mov	sp, r2
	            
	orr     r1, r0, #0x17 		@ abort
	msr     cpsr_c, r1
	mov	sp, r2
	    
	orr     r1, r0, #0x1b 		@ undefined
	msr     cpsr_c, r1
	mov	sp, r2
	    
	orr     r1, r0, #0x1f 		@ system
	msr     cpsr_c, r1
	mov	sp, r2

	orr	r1, r0, #0x13 		@ supervisor
	msr	cpsr_c, r1
	mov	sp, r2

	ldr	r0, =__data_start_rom
	ldr	r1, =__data_start
	ldr	r2, =__data_end

	cmp	r0, r1
	beq	.L__do_bss

.L__copy_loop:
	cmp	r1, r2
	ldrlt	r3, [r0], #4
	strlt	r3, [r1], #4
	blt	.L__copy_loop

.L__do_bss:
	ldr	r0, =__bss_start
	ldr	r1, =_end
	mov	r2, #0

.L__bss_loop:
	cmp	r0, r1
	strlt	r2, [r0], #4
	blt	.L__bss_loop

	bl	main	@ jump to main () function
	b	.

.ltorg

.bss

.align 2
abort_stack:
	.skip 1024
abort_stack_top:

