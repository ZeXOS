#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "client.h"

int mainsocket;
struct sockaddr_in sockserver;
struct sockaddr_in sockclient;
socklen_t addrlen;
char buffer[1530];

extern client_t client_list;

int net_send (int fd, char *str, unsigned len)
{
	int oldFlag = fcntl (fd, F_GETFL, 0);
	if (fcntl (fd, F_SETFL, oldFlag & ~O_NONBLOCK) == -1) {
		printf ("Cant set socket to blocking mode\n");
		return 0;
	}

	int ret = send (fd, str, len, 0);

	oldFlag = fcntl (fd, F_GETFL, 0);
	if (fcntl (fd, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	return ret;
}

int net_accept ()
{
	int newfd = accept (mainsocket, (struct sockaddr *) &sockclient, &addrlen);

	if (newfd > 0) {
		printf ("> New client connected fd (%d) - %s\n", newfd, inet_ntoa (sockclient.sin_addr));

		client_add (newfd);

		return newfd;
	}

	return 0;
}

int net_loop ()
{
	net_accept ();
	
	usleep (32);

	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next) {
		if (!c)
			continue;

		int ret = recv (c->fd, buffer, 1500, 0);

		if (ret > 0)
			if (!client_handle (c, buffer, ret))
				break;
	}

	return 1;
}

int net_init ()
{
	int port = 5333;

	if ((mainsocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return -1;
	}

	// 1) Family of protocols
	sockserver.sin_family = AF_INET;
	// 2) Set number of listen port
	sockserver.sin_port = htons (port);
	// 3) Listen IP address
	sockserver.sin_addr.s_addr = INADDR_ANY;

	unsigned long yes = 1;
	setsockopt (mainsocket, SOL_SOCKET, SO_REUSEADDR, (char *) &yes, sizeof (yes));

	// Bind on our socket
	if (bind (mainsocket, (struct sockaddr *) &sockserver, sizeof (sockserver)) == -1) {
		printf ("bind () - port is used\n");
		return -1;
	}

	// Create queue for accept
	if (listen (mainsocket, 10) == -1) {
		printf ("ERROR -> listen () == -1\n");
		return -1;
	}

	// set mainSocket to non-blocking mode
	int oldFlag = fcntl (mainsocket, F_GETFL, 0);
	if (fcntl (mainsocket, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	return 1;
}
