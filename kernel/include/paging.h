/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PAGING_H
#define _PAGING_H

#include <build.h>

/* x86 dependent */
#ifdef ARCH_i386

#define PAGE_MEM_LOW		0x400000	// 0-4MB for kernel stuff
#define PAGE_MEM_FB		0x500000	// 4-5MB for framebuffer
#define PAGE_MEM_HIGH		0x800000	// 5-8MB for storage (binaries, additional data)

#define PAGING_BIT		0x80000000

#define PAGE_ATTR_MAPPED	3
#define PAGE_ATTR_UNMAPPED	2

#define PAGE_TABLE_FLAG_FREE	0x1
#define PAGE_TABLE_FLAG_KERNEL	0x2
#define PAGE_TABLE_FLAG_USER	0x4

#define PAGE_TABLE_BLOCK_SIZE	4096		// 4kB
#define PAGE_DIR_BLOCK_SIZE	1024		// 4kB * 1024 = 4MB

// Macros used in the bitset algorithms.
#define index_from_bit(a) (a/(8*4))
#define offset_from_bit(a) (a%(8*4))

#endif

#define	__PACKED__ __attribute__ ((__packed__))

/* Page structure */
typedef struct page_context
{
	unsigned present : 1;
	unsigned rw : 1;
	unsigned user : 1;
	unsigned accessed : 1;
	unsigned dirty : 1;
	unsigned unused : 7;
	unsigned frame : 20;
} __PACKED__ page_t ;

typedef struct page_table_context
{
	page_t pages[1024];
} page_table_t;

typedef struct page_dir_context
{
	page_table_t *tables[1024];
	unsigned tables_phys[1024];
	unsigned addr_phys;
} page_dir_t;

typedef struct page_entity_context
{
	page_dir_t *page_dir;
	unsigned *frames;
} page_ent_t;

/* externs */
extern unsigned int init_paging ();
extern unsigned paging_enable ();
extern unsigned paging_disable ();
extern page_ent_t *page_cover_create ();
extern unsigned page_cover_delete (page_ent_t *page_cover);
extern unsigned page_mmap (page_ent_t *page_cover, void *from, void *to, unsigned kernel, unsigned writeable);
extern unsigned page_unmmap (page_ent_t *page_cover, void *from, void *to);
extern void page_dir_switch (page_dir_t *page_dir);

#endif
 
