/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __SYSTEM_H
#define __SYSTEM_H

#include "fdc.h"
#include "mytypes.h"
#include "dev.h"
#include "user.h"
#include <tty.h>
#include "partition.h"
#include "file.h"
#include "time.h"
#include "_null.h"
#include "_size_t.h"
#include "../drivers/char/video/video.h"
#include "elf.h"
#include "paging.h"

/* irq0 timer */
#define HZ		1000

/* kernel attributes */
unsigned kernel_attr;

#define KERNEL_NOLIVE	0x01
#define KERNEL_18HZ	0x02
#define KERNEL_NOHDD	0x04
#define KERNEL_NOATA	0x08
#define KERNEL_NOFLOPPY	0x10
#define KERNEL_NOCDROM  0x20
#define KERNEL_NOETH	0x40
#define KERNEL_NOVESA	0x80

/* debug masks */
#define DBG_LIB		0x1
#define DBG_STDIO	0x2
#define DBG_STDLIB	0x4
#define DBG_STRING	0x8
#define DBG_CORE	0x10
#define DBG_ENV		0x20
#define DBG_CMDS	0x100
#define DBG_MM		0x200
#define DBG_KMEM	0x400
#define DBG_UMEM	0x800
#define DBG_ELF		0x1000
#define DBG_VFS		0x2000
#define DBG_SCHED	0x4000
#define DBG_PROC	0x8000
#define DBG_FS		0x10000
#define DBG_NET		0x20000
#define DBG_SOCKET	0x40000
#define DBG_TCP		0x80000
#define DBG_UDP		0x100000
#define DBG_DNS		0x200000
#define DBG_DHCP	0x400000
#define DBG_DRIVER	0x1000000
#define DBG_SOUND	0x2000000
#define DBG_BLOCK	0x4000000
#define DBG_BUS		0x8000000
#define DBG_ETH		0x10000000
#define DBG_CHAR	0x20000000
#define DBG_UTILS	0x40000000
#define DBG_SYSCALL	0x80000000

#define swap16(x) ({unsigned _x = (x); (((_x)>>8)&0xff) | (((_x)<<8)&0xff00); })

#define swap32(x) ({\
	unsigned _x = (x); \
	(((_x)>>24)&0xff)\
	|\
	(((_x)>>8)&0xff00)\
	|\
	(((_x)<<8)&0xff0000)\
	|\
	(((_x)<<24)&0xff000000);\
})

#define swap64(_v) (((unsigned long)swap32((unsigned)(_v))<<32)|(unsigned long)swap32((unsigned)((_v)>>32)))

unsigned debug;			// developer mode

unsigned long long ctps;	// cpu ticks per second

tty_t *currtty;

partition_t *curr_part;

unsigned int fd_count;

page_dir_t *page_cover_curr;

unsigned char *kbd_layout[2];

extern unsigned short *vesafb;

#define KBD_MAX_QUAUE 16
typedef struct
{
	char key[KBD_MAX_QUAUE];
	int p;
	//unsigned char state[KBD_MAX_QUAUE];
} kbd_quaue;

/* memory vars */
unsigned short mem_ext;

/* fs directory structure */
struct dir {
	char name[10];
	int read;
	int hidden;
	int system;
	int volume;
	int dir;
	int bin;
	unsigned int start;
} dir[223];

/** externs **/

/* init.c */
extern int init ();

/* kmem.c */
extern void *malloc (unsigned size);
extern void *realloc(void *blk, unsigned size);
extern void free (void *blk);
extern void *kmalloc (unsigned size);
extern void *krealloc(void *blk, unsigned size);
extern void kfree (void *blk);

extern unsigned int init_mm ();

/* pmem.c */
extern void *pmalloc (size_t size);
extern void pfree (void *blk);
extern void *palign (void *p);

/* swmem.c */
extern void *swmalloc (unsigned size);
extern unsigned swfree (void *ptr, unsigned size);
extern unsigned int swmem_init ();

/* protmem.c */
extern void *memcpy_to_user (void *dest, const void *src, size_t count);
extern void *memcpy_from_user (void *dest, const void *src, size_t count);
extern unsigned memtest_data (void *src, void *data, size_t offset);

/* console.c */
extern unsigned int init_console ();
extern void console (int i);

/* elf.c */
extern int exec_elf (unsigned char *image, unsigned *entry, unsigned *elf_data, unsigned *elf_data_off, unsigned *elf_bss);
extern int flush_elf ();

/* exec.c */
extern bool exec (partition_t *p, unsigned char *fi);

/* keyboard.c */
extern unsigned keyboard_setlayout (char *layout);
extern unsigned keyboard_getlayout (char *layout);

extern char getkey ();
extern void setkey (char key);

extern unsigned int init_tasks (void);
extern unsigned sched_lock ();
extern unsigned sched_unlock ();
extern void schedule (void);

/* kprintf.c */
extern void DPRINT (unsigned type, const char *fmt, ...);

/* video.c */
extern unsigned char vgagui;

extern void uptime (void);

/* fat.c */
extern unsigned read_dir (int aa);

/* ls.c */
extern int ls (unsigned char *fi);

/* cd.c */
extern int cd (unsigned char *fi);

/* cp.c */
extern int cp (unsigned char *fi, unsigned char *fo);

/* cat.c */
extern int cat (unsigned char *fi);

/* read.c */
extern int read (unsigned fd, void *buf, unsigned len);

/* fopen.c */
extern FILE *fopen (const char *filename, const char *mode);

/* fputs.c */
extern char *fgets (char *string, int n, FILE *file);

/* fclose.c */
extern int fclose (FILE *file);

/* keyboard.c */
extern unsigned int init_keyboard ();

/* arch depend rtc.c */
extern tm *rtc_getcurrtime ();

/* arch depend task.c */
extern unsigned arch_task_init (task_t *task, unsigned entry);
extern unsigned arch_task_set (task_t *task);
extern unsigned arch_task_switch (task_t *task);

/* arch depend arch.c */
extern unsigned int_enable ();
extern unsigned int_disable ();
extern unsigned arch_cpu_hlt ();
extern unsigned init_arch ();

/* arch depend elf*.c */
extern unsigned arch_elf_detect (elf_file_t *file);

#endif
