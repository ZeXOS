/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _XMPP_H
#define _XMPP_H

#define XMPP_MECHANISM_PLAIN	0x1
#define XMPP_MECHANISM_DIGEST	0x2

typedef struct {
	char *server;
	int port;
	char *session;
	char *mechanism;
	char *user;
	char *password;
	char *resource;
} xmpp_t;

extern int xmpp_presence (char *show, char *status);
extern int xmpp_message (char *to, char *message, unsigned len);
extern int xmpp_session_set (char *id, unsigned len);
extern int xmpp_mechanism_set (char *mechanism, unsigned len);
extern int xmpp_bind ();
extern int xmpp_session ();
extern int xmpp_message_from (char *from, unsigned from_len, char *msg, unsigned msg_len);
extern int xmpp_connect (char *server, int port);
extern int xmpp_close ();
extern void xmpp_setup (char *user, char *password, char *resource);
extern int xmpp_loop ();

#endif
