/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

const char *inet_ntop (int af, const void *src, char *dst, size_t cnt)
{
	if (!src || !dst || !cnt)
		return (char *) 0;

	if (af == AF_INET && cnt > 15) {
		unsigned *ip = (unsigned *) src;

		unsigned char a = (unsigned char) *ip;
		unsigned char b = (unsigned char) (*ip >> 8);
		unsigned char c = (unsigned char) (*ip >> 16);
		unsigned char d = (unsigned char) (*ip >> 24);
	
		sprintf (dst, "%d.%d.%d.%d", a, b, c, d);

		return dst;
	}

	if (af == AF_INET6 && cnt > 39) {
		unsigned short ip[8];
		memcpy (ip, src, 16);
	
		sprintf (dst, "%x:%x:%x:%x:%x:%x:%x:%x", htons (ip[0]), htons (ip[1]), htons (ip[2]), htons (ip[3]),
			htons (ip[4]), htons (ip[5]), htons (ip[6]), htons (ip[7]));

		return dst;
	}

	return (char *) 0;
}

