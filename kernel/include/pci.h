/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PCI_H
#define _PCI_H

/* ---
	offsets in PCI configuration space to the elements of the predefined
	header common to all header types
--- */

#define PCI_vendor_id				0x00		/* (2 byte) vendor id */
#define PCI_device_id				0x02		/* (2 byte) device id */
#define PCI_command				0x04		/* (2 byte) command */
#define PCI_status				0x06		/* (2 byte) status */
#define PCI_revision				0x08		/* (1 byte) revision id */
#define PCI_class_api				0x09		/* (1 byte) specific register interface type */
#define PCI_class_sub				0x0a		/* (1 byte) specific device function */
#define PCI_class_base				0x0b		/* (1 byte) device type (display vs network, etc) */
#define PCI_line_size				0x0c		/* (1 byte) cache line size in 32 bit words */
#define PCI_latency				0x0d		/* (1 byte) latency timer */
#define PCI_header_type				0x0e		/* (1 byte) header type */
#define PCI_bist				0x0f		/* (1 byte) built-in self-test */

/* ---
	pci commands
--- */
#define PCI_command_io			0x1	/* Enable response in I/O space */
#define PCI_command_mem			0x2	/* Enable response in mem space */
#define PCI_command_master		0x4	/* Enable bus mastering */

/* ---
	masks for header type register
--- */

#define PCI_header_type_mask			0x7F		/* header type field */
#define PCI_multifunction			0x80		/* multifunction device flag */

/* ---
	offsets in PCI configuration space to the elements of the predefined
	header common to header types 0x00 and 0x01
--- */
#define PCI_base_registers		0x10		/* base registers (size varies) */
#define PCI_interrupt_line		0x3c		/* (1 byte) interrupt line */
#define PCI_interrupt_pin		0x3d		/* (1 byte) interrupt pin */


/* ---
	masks for flags in i/o space base address registers
--- */

#define PCI_address_io_mask		0xFFFFFFFC	/* mask to get i/o space base address */


/* ---
	masks for flags in expansion rom base address registers
--- */

#define PCI_rom_enable			0x00000001	/* 1 = expansion rom decode enabled */
#define PCI_rom_address_mask	0xFFFFF800	/* mask to get expansion rom addr */

/** PCI interrupt pin values */
#define PCI_pin_mask            0x07
#define PCI_pin_none            0x00
#define PCI_pin_a               0x01
#define PCI_pin_b               0x02
#define PCI_pin_c               0x03
#define PCI_pin_d               0x04
#define PCI_pin_max             0x04

/** PCI Capability Codes */
#define PCI_cap_id_reserved     0x00
#define PCI_cap_id_pm           0x01      /* Power management */
#define PCI_cap_id_agp          0x02      /* AGP */
#define PCI_cap_id_vpd          0x03      /* Vital product data */
#define PCI_cap_id_slotid       0x04      /* Slot ID */
#define PCI_cap_id_msi          0x05      /* Message signalled interrupt ??? */
#define PCI_cap_id_chswp        0x06      /* Compact PCI HotSwap */
#define PCI_cap_id_pcix         0x07
#define PCI_cap_id_ldt          0x08
#define PCI_cap_id_vendspec     0x09
#define PCI_cap_id_debugport    0x0a
#define PCI_cap_id_cpci_rsrcctl 0x0b
#define PCI_cap_id_hotplug      0x0c

/** Power Management Control Status Register settings */
#define PCI_pm_mask             0x03
#define PCI_pm_ctrl             0x02
#define PCI_pm_d1supp           0x0200
#define PCI_pm_d2supp           0x0400
#define PCI_pm_status           0x04
#define PCI_pm_state_d0         0x00
#define PCI_pm_state_d1         0x01
#define PCI_pm_state_d2         0x02
#define PCI_pm_state_d3         0x03


enum {
	PCI_DEVICE = 0,
	PCI_HOST_BUS,
	PCI_BRIDGE,
	PCI_CARDBUS
};

/* PCI device structure */
typedef struct pcidev_context {
  struct pcidev_context *next, *prev;
	unsigned char bus, dev, fn;
	unsigned long id;

	unsigned char	revision;				/* revision id */
	unsigned char	class_api;				/* specific register interface type */
	unsigned char	class_sub;				/* specific device function */
	unsigned char	class_base;				/* device type (display vs network, etc) */
	unsigned char	line_size;				/* cache line size in 32 bit words */
	unsigned char	latency;				/* latency timer */
	unsigned char	header_type;				/* header type */
	unsigned char	bist;					/* built-in self-test */
	unsigned char	reserved;				/* filler, for alignment */
	union {
		struct {
			unsigned long	cardbus_cis;			/* CardBus CIS pointer */
			unsigned short	subsystem_id;			/* subsystem (add-in card) id */
			unsigned short	subsystem_vendor_id;		/* subsystem (add-in card) vendor id */
			unsigned long	rom_base;			/* rom base address, viewed from host */
			unsigned long	rom_base_pci;			/* rom base addr, viewed from pci */
			unsigned long	rom_size;			/* rom size */
			unsigned long	base_registers[6];		/* base registers, viewed from host */
			unsigned long	base_registers_pci[6];		/* base registers, viewed from pci */
			unsigned long	base_register_sizes[6];		/* size of what base regs point to */
			unsigned char	base_register_flags[6];		/* flags from base address fields */
			unsigned char	interrupt_line;			/* interrupt line */
			unsigned char	interrupt_pin;			/* interrupt pin */
			unsigned char	min_grant;			/* burst period @ 33 Mhz */
			unsigned char	max_latency;			/* how often PCI access needed */
		} h0;
		struct {
			unsigned long	base_registers[2];		/* base registers, viewed from host */
			unsigned long	base_registers_pci[2];		/* base registers, viewed from pci */
			unsigned long	base_register_sizes[2];		/* size of what base regs point to */
			unsigned char	base_register_flags[2];		/* flags from base address fields */
			unsigned char	primary_bus;
			unsigned char	secondary_bus;
			unsigned char	subordinate_bus;
			unsigned char	secondary_latency;
			unsigned char	io_base;
			unsigned char	io_limit;
			unsigned short	secondary_status;
			unsigned short	memory_base;
			unsigned short	memory_limit;
			unsigned short  prefetchable_memory_base;
			unsigned short  prefetchable_memory_limit;
			unsigned long	prefetchable_memory_base_upper32;
			unsigned long	prefetchable_memory_limit_upper32;
			unsigned short	io_base_upper16;
			unsigned short	io_limit_upper16;
			unsigned long	rom_base;			/* rom base address, viewed from host */
			unsigned long	rom_base_pci;			/* rom base addr, viewed from pci */
			unsigned char	interrupt_line;			/* interrupt line */
			unsigned char	interrupt_pin;			/* interrupt pin */
			unsigned short	bridge_control;
			unsigned short	subsystem_id;			/* subsystem (add-in card) id */
			unsigned short	subsystem_vendor_id;		/* subsystem (add-in card) vendor id */
		} h1;
		struct {
			unsigned short	subsystem_id;			/* subsystem (add-in card) id */
			unsigned short	subsystem_vendor_id;		/* subsystem (add-in card) vendor id */
		} h2;
	} u;
} pcidev_t;

/* externs */
extern void pcidev_display ();
extern void pci_device_adjust (pcidev_t *pci);
extern pcidev_t *pcidev_find (unsigned short vendor, unsigned short device);
extern bool bus_pci_acthandler (unsigned act, char *block, unsigned block_len);
extern int pci_read_config_dword (pcidev_t *pci, unsigned reg, unsigned long *val);

#endif
