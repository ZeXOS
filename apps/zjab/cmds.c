/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "net.h"
#include "xmpp.h"
#include "cmds.h"

static char *cmd_window;
static char *cmd_cache;
static unsigned cmd_len;

#ifdef LINUX
void setcolor (unsigned char t, unsigned char f)
{
	/* TODO */
}
#endif

static int getstring (char *str)
{
	if (cmd_len > CMDS_CACHE_SIZE-1) {
		setcolor (4, 0);
		printf ("\nERROR -> commands cache buffer is full ! Clearing cache ..\n");
		setcolor (15, 0);

		cmd_len = 0;
		return 0;
	}

	char c = getchar ();

	if (c != -1) {
		if (c == '\n') {
			unsigned l = cmd_len;
			cmd_len = 0;

			return l;
		}
		if (c == '\b') {
			if (cmd_len > 0)
				cmd_len --;

			return 0;
		}

		str[cmd_len] = c;
		cmd_len ++;
	}

	return 0;
}

int cmds_handler (char *buffer, unsigned len)
{
	if (!strncmp (buffer, "/to ", 4)) {
		if (cmd_window)
			free (cmd_window);
		
		cmd_window = strndup (buffer+4, len-4);
		
		if (!cmd_window)
			return -1;

		printf ("> Current chat window: %s\n", cmd_window);
		
		return 0;
	} else if (!strncmp (buffer, "/help", len)) {
		printf ("> zjab help\n\t/to\t-\tSpecify JID of chat window\n\t/status\t-\tChange status\n\t/quit\t-\tQuit the zjab\n");
		
		return 0;
	} else if (!strncmp (buffer, "/status ", 8)) {
		printf ("> Status was changed to \"%s\"\n", buffer+8);
		xmpp_presence ("online", buffer+8);
		return 0;
	} else if (!strncmp (buffer, "/quit", len))
		return -1;
	
	if (!cmd_window) {
		printf ("> Unknown chat window, please use /help\n");
		return 0;
	}
	
	if (cmd_window)
		xmpp_message (cmd_window, buffer, len);
	
	setcolor (14, 0);
	printf ("> %s\n", buffer);
	setcolor (7, 0);

	return 0;
}

/* get input from keyboard */
int cmds_get ()
{
	int l = getstring (cmd_cache);
	
	if (l > 0) {
		cmd_cache[l] = '\0';

		return cmds_handler (cmd_cache, l);
	}

	return 1;
}

int cmds_init ()
{
	cmd_cache = (char *) malloc (sizeof (char) * CMDS_CACHE_SIZE);

	if (!cmd_cache)
		return -1;

	/* set socket "1" to non-blocking */
	int oldFlag = fcntl (1, F_GETFL, 0);
	if (fcntl (1, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return -1;
	}

	cmd_len = 0;
	cmd_window = 0;
	
	return 0;
}