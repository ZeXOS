/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include <ipc.h>

#include "appcl.h"

static char zstr[128];

static int zappcl_fd;
static appcl_t *zappcl;
static unsigned short zsizex;
static unsigned short zsizey;
static unsigned zstate;
static zbitmap_t bitmap_button;
static unsigned zitem_sel;
static unsigned zitem_curr;

extern unsigned short image_button[];

appcl_t *zgui_window ()
{
	return zappcl;
}

void zgui_puts (unsigned short x, unsigned short y, char *str, unsigned color)
{
	if (!zappcl)
		return;
	
	int i = 0;
	int j = 0;
	int k = 0;
	
	for (i = 0; str[i]; i ++) {
		if (str[i] == '\n') {
			j += 6;
			k = 0;
			continue;
		} else
		if (str[i] == '\t') {
			k += 4*5;
			continue;
		}

		if (zappcl->sizex < k+15) {
			j += 6;
			k = 0;
			continue;
		}
 
		if (j < zappcl->sizey)
			xtext_putch (zappcl->x+4+x+k, zappcl->y+21+y+j, color, str[i]);

		k += 6;
	}
	  
	//xtext_puts (zappcl->x+4+x, zappcl->y+21+y, color, str);
	
}

int zgui_cursor (unsigned short *x, unsigned short *y)
{
	if (!zappcl || !x || !y)
		return 0;
	
	if (zappcl->mousex-4 > zappcl->sizex)
		*x = zappcl->sizex-1;
	else if (zappcl->mousex-4 < 0)
		*x = 0;
	else
		*x = zappcl->mousex-4;
	
	if (zappcl->mousey-21 > zappcl->sizey)
		*y = zappcl->sizey-1;
	else if (zappcl->mousey-21 < 0)
		*y = 0;
	else
		*y = zappcl->mousey-21;
	
	return (zappcl->state & APPCL_STATE_LBUTTON) ? 1 : 0;
}

void zgui_input (unsigned short x, unsigned short y, char *str, unsigned len, unsigned color)
{
	if (!zappcl)
		return;

	zitem_curr ++;
	
	if (x+4 <= zappcl->mousex && x+8+((short) len*6) >= zappcl->mousex &&
	    y+19 <= zappcl->mousey && y+28 >= zappcl->mousey) {
		if (zappcl->state & APPCL_STATE_LBUTTON) {
			zappcl->state &= ~APPCL_STATE_LBUTTON;
			zitem_sel = zitem_curr;
		}
	}
	
	if (zitem_curr == zitem_sel)
	if (zappcl->kbd) {
		switch (zappcl->kbd) {
			case '\b':
			{
				int i;
				for (i = len-1; i >= 0; i --) {
					if (str[i]) {
						str[i] = 0;
						break;
					}
				}
			}
			break;
			default:
			{
				unsigned i;
				for (i = 0; i < len; i ++) {
					if (!str[i]) {
						str[i] = zappcl->kbd;
						break;
					}
				}
			}
		}

		zappcl->kbd = 0;
	}

	xtext_puts (zappcl->x+4+x, zappcl->y+21+y, color, str);
}

void zgui_bitmap (zbitmap_t *bitmap)
{
	unsigned i = 0;
	unsigned j = 0;
	unsigned k = 0;
	unsigned pix = 0;

	if (!bitmap)
		return;
	
	if (!bitmap->data)
		return;

	for (i = bitmap->posx+(bitmap->posy*bitmap->resx); i < bitmap->resx*bitmap->resy; i ++) {
		if (k >= bitmap->resx) {
			j ++;
			k = 0;
		}

		if (k < bitmap->sizex && j < bitmap->sizey)
			xpixel ((unsigned) zappcl->x+4+bitmap->x+k, (unsigned) zappcl->y+21+bitmap->y+j, (unsigned short) bitmap->data[i]);

		k ++;
	}
}

static void draw_bitmapspec (zbitmap_t *bitmap)
{
	unsigned i = 0;
	unsigned j = 0;
	unsigned k = 0;
	unsigned pix = 0;

	if (!bitmap)
		return;
	
	if (!bitmap->data)
		return;

	unsigned short w = bitmap->data[299+34];

	for (i = bitmap->posx+(bitmap->posy*13); i < 13*25; i ++) {
		if (k >= 14) {
			j ++;
			k = 0;
		}
		
		pix = 355-i;

		//if (pix < 355)
		//	continue;

		if (bitmap->data[pix] != w)
		if (k < bitmap->sizex && j < bitmap->sizey)
			xpixel ((unsigned) bitmap->x+k, (unsigned) bitmap->y+j, image_button[pix]);

		k ++;
	}
}

int zgui_button (unsigned short x, unsigned short y, char *str)
{
	if (!zappcl)
		return -1;

	zitem_curr ++;
	
	if (zitem_curr != zitem_sel)
		return -1;

	unsigned len = strlen (str);

	bitmap_button.x = zappcl->x+4+x;
	bitmap_button.y = zappcl->y+21+y;
	bitmap_button.posx = 0;
	bitmap_button.posy = 0;
	bitmap_button.sizex = 4;
	bitmap_button.sizey = 23;
	
	draw_bitmapspec (&bitmap_button);

	unsigned l;
	for (l = 0; l < len; l ++) {
		bitmap_button.x = zappcl->x+8+x+(l*6);
		bitmap_button.y = zappcl->y+21+y;
		bitmap_button.posx = 4;
		bitmap_button.posy = 0;
		bitmap_button.sizex = 6;
		bitmap_button.sizey = 23;

		draw_bitmapspec (&bitmap_button);
	}
		
	bitmap_button.x = zappcl->x+8+x+(len*6);
	bitmap_button.y = zappcl->y+21+y;
	bitmap_button.posx = 9;
	bitmap_button.posy = 0;
	bitmap_button.sizex = 4;
	bitmap_button.sizey = 23;
		
	draw_bitmapspec (&bitmap_button);
	
	int i = 0;

	if (x+4 <= zappcl->mousex && x+12+((short) len*6) >= zappcl->mousex &&
	    y+22 <= zappcl->mousey && y+43 >= zappcl->mousey) {
		if (zappcl->state & APPCL_STATE_LBUTTON) {
			zappcl->state &= ~APPCL_STATE_LBUTTON;
			i ++;
		}
	}
	
	if (!i)
		xtext_puts (zappcl->x+8+x, zappcl->y+28+y, 0, str);
	else
	  	return 0;
	
	return -1;
}

int zgui_resize (unsigned short x, unsigned short y)
{
	if (!zappcl)
		return -1;

	zsizex = x;
	zsizey = y;

	zstate |= APPCL_STATE_RESIZE;
	
	return 0;
}

unsigned zgui_event ()
{
	zitem_curr = 0;
  
	if (zappcl->state & APPCL_STATE_REDRAW) {
		zappcl->state &= ~APPCL_STATE_REDRAW;
		zappcl->state |= APPCL_STATE_RECVOK;

		ipc_send (zappcl_fd, zstr, sizeof (appcl_t));
	}

	int ret = ipc_recv (zappcl_fd, zstr, 128);

	if (ret > 0) {
		if (zstate & APPCL_STATE_RESIZE) {
			zappcl->sizex = zsizex;
			zappcl->sizey = zsizey;
			zappcl->state |= APPCL_STATE_RESIZE;
		}

		return zappcl->state;
	}

	schedule ();

	return 0;
}

int zgui_init ()
{
	zappcl = 0;
	zstate = 0;
	zitem_sel = 1;
	zitem_curr = 0;
	bitmap_button.data = image_button;

	int ret = 0;
	zappcl_fd = ipc_open ("/zdeserver", 0);

	if (zappcl_fd > 0) {
		int ret = ipc_recv (zappcl_fd, zstr, 127);

		if (ret > 0) {
			zstr[ret] = '\0';

			if (strcmp (zstr, "ZDE"))
				ret = -1;
		}
	} else
		ret = -1;
	
	xinit ();

	zappcl = (appcl_t *) zstr;

	memset (zstr, 0, sizeof (appcl_t));

	return ret;
}

int zgui_exit ()
{
	ipc_close (zappcl_fd);

	return 0;
}
