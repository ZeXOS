/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* BAD - works like inb */
/*unsigned inw (unsigned short port)
{
	unsigned char ret;

	__asm__ __volatile__ ("inw %1,%w0"
		: "=a"(ret)
		: "d"(port));

	return ret;
}*/

void outw (unsigned port, unsigned val)
{
	__asm__ __volatile__ ("outw %w0,%w1"
		:
		: "a"(val), "d"(port));
}

void outb (unsigned port, unsigned val)
{
	__asm__ __volatile__ ("outb %b0,%w1"
		:
		: "a"(val), "d"(port));
}

unsigned inb (unsigned short port)
{
	unsigned char ret;

	__asm__ __volatile__ ("inb %1,%0"
		: "=a"(ret)
		: "d"(port));

	return ret;
}

void outl (unsigned short portno, unsigned long val)
{
	__asm__ __volatile__ ("outl %0, %w1" :: "a" (val), "Nd" (portno));
}

unsigned long inl (unsigned short portno)
{
	unsigned long ret;
	
	__asm__ __volatile__ ("inl %w1, %0" : "=a" (ret) : "Nd" (portno));
	
	return ret;
}
