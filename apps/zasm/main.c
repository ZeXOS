/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "source.h"
#include "buffer.h"

int help ()
{
	printf ("zasm - assembly compiler\nSyntax:\n\tzasm <sourcefile> <outputfile>\n");

	return 0;
}

int param (int argc, char **argv)
{
	if (argc < 3)
		return help ();

	buffer_init ();
	source_init ();

	unsigned size;

	char *source = source_open (argv[1], &size);

	if (!source)
		return 0;

	if (!source_parse (source, size))
		return 0;

	buffer_write (argv[2]);

	free (source);
	//free (buffer_get ());

	return 1;
}

int main (int argc, char **argv)
{
	if (!param (argc, argv))
		return -1;

	return 0;
}
