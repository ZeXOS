/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>

/* These are function prototypes for all of the exception
*  handlers: The first 32 entries in the IDT are reserved
*  by Intel, and are designed to service exceptions! */
extern void isr0 ();
extern void isr1 ();
extern void isr2 ();
extern void isr3 ();
extern void isr4 ();
extern void isr5 ();
extern void isr6 ();
extern void isr7 ();
extern void isr8 ();
extern void isr9 ();
extern void isr10 ();
extern void isr11 ();
extern void isr12 ();
extern void isr13 ();
extern void isr14 ();
extern void isr15 ();
extern void isr16 ();
extern void isr17 ();
extern void isr18 ();
extern void isr19 ();
extern void isr20 ();
extern void isr21 ();
extern void isr22 ();
extern void isr23 ();
extern void isr24 ();
extern void isr25 ();
extern void isr26 ();
extern void isr27 ();
extern void isr28 ();
extern void isr29 ();
extern void isr30 ();
extern void isr31 ();


/* This is a very repetitive function... it's not hard, it's
*  just annoying. As you can see, we set the first 32 entries
*  in the IDT to the first 32 ISRs. We can't use a for loop
*  for this, because there is no way to get the function names
*  that correspond to that given entry. We set the access
*  flags to 0x8E. This means that the entry is present, is
*  running in ring 0 (kernel level), and has the lower 5 bits
*  set to the required '14', which is represented by 'E' in
*  hex. */
unsigned int isrs_install ()
{
	idt_set_gate (0, (unsigned) isr0, 0x08, 0x8E);
	idt_set_gate (1, (unsigned) isr1, 0x08, 0x8E);
	idt_set_gate (2, (unsigned) isr2, 0x08, 0x8E);
	idt_set_gate (3, (unsigned) isr3, 0x08, 0x8E);
	idt_set_gate (4, (unsigned) isr4, 0x08, 0x8E);
	idt_set_gate (5, (unsigned) isr5, 0x08, 0x8E);
	idt_set_gate (6, (unsigned) isr6, 0x08, 0x8E);
	idt_set_gate (7, (unsigned) isr7, 0x08, 0x8E);
	
	idt_set_gate (8, (unsigned) isr8, 0x08, 0x8E);
	idt_set_gate (9, (unsigned) isr9, 0x08, 0x8E);
	idt_set_gate (10, (unsigned) isr10, 0x08, 0x8E);
	idt_set_gate (11, (unsigned) isr11, 0x08, 0x8E);
	idt_set_gate (12, (unsigned) isr12, 0x08, 0x8E);
	idt_set_gate (13, (unsigned) isr13, 0x08, 0x8E);
	idt_set_gate (14, (unsigned) isr14, 0x08, 0x8E);
	idt_set_gate (15, (unsigned) isr15, 0x08, 0x8E);
	
	idt_set_gate (16, (unsigned) isr16, 0x08, 0x8E);
	idt_set_gate (17, (unsigned) isr17, 0x08, 0x8E);
	idt_set_gate (18, (unsigned) isr18, 0x08, 0x8E);
	idt_set_gate (19, (unsigned) isr19, 0x08, 0x8E);
	idt_set_gate (20, (unsigned) isr20, 0x08, 0x8E);
	idt_set_gate (21, (unsigned) isr21, 0x08, 0x8E);
	idt_set_gate (22, (unsigned) isr22, 0x08, 0x8E);
	idt_set_gate (23, (unsigned) isr23, 0x08, 0x8E);
	
	idt_set_gate (24, (unsigned) isr24, 0x08, 0x8E);
	idt_set_gate (25, (unsigned) isr25, 0x08, 0x8E);
	idt_set_gate (26, (unsigned) isr26, 0x08, 0x8E);
	idt_set_gate (27, (unsigned) isr27, 0x08, 0x8E);
	idt_set_gate (28, (unsigned) isr28, 0x08, 0x8E);
	idt_set_gate (29, (unsigned) isr29, 0x08, 0x8E);
	idt_set_gate (30, (unsigned) isr30, 0x08, 0x8E);
	idt_set_gate (31, (unsigned) isr31, 0x08, 0x8E);

	return 1;
}

/* This is a simple string array. It contains the message that
*  corresponds to each and every exception. We get the correct
*  message by accessing like:
*  exception_message[interrupt_number] */
char *exception_messages[] =
{
	"Division By Zero",
	"Debug",
	"Non Maskable Interrupt",
	"Breakpoint",
	"Into Detected Overflow",
	"Out of Bounds",
	"Invalid Opcode",
	"No Coprocessor",
	
	"Double Fault",
	"Coprocessor Segment Overrun",
	"Bad TSS",
	"Segment Not Present",
	"Stack Fault",
	"General Protection Fault",
	"Page Fault",
	"Unknown Interrupt",
	
	"Coprocessor Fault",
	"Alignment Check",
	"Machine Check",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved",
	"Reserved"
};

/* All of our Exception handling Interrupt Service Routines will
*  point to this function. This will tell us what exception has
*  happened! Right now, we simply halt the system by hitting an
*  endless loop. All ISRs disable interrupts while they are being
*  serviced as a 'locking' mechanism to prevent an IRQ from
*  happening and messing up kernel data structures */
void fault_handler (struct regs *r)
{
	if (r->int_no < 32) {
		int ret = 0;

		/* page fault */
		if (r->int_no == 14)
			ret = page_fault (r);

		/* is it page fault of user space (process) or kernel space ? */
		if (ret)
			return;

		/* display kernel panic message */
		video_color (4, 0);
		kprintf ((unsigned char *) "Kernel panic -> ");

		video_color (15, 0);
		kprintf ((unsigned char *) exception_messages[r->int_no]);
		kprintf ((unsigned char *) " Exception. System Halted!\n");

		int_disable ();

		while (1)
			asm ("hlt;");
	}
}

/*****************************************************************************
*****************************************************************************/
int fault (uregs_t *regs)
{
	static const char *msg[] = {
		"divide error", "debug exception", "NMI", "INT3",
		"INTO", "BOUND exception", "invalid opcode", "no coprocessor",
		"double fault", "coprocessor segment overrun",
		"bad TSS", "segment not present",
		"stack fault", "GPF", "page fault", "??",
		"coprocessor error", "alignment check", "??", "??",
		"??", "??", "??", "??",
		"??", "??", "??", "??",
		"??", "??", "??", "??",
		"IRQ0", "IRQ1", "IRQ2", "IRQ3",
		"IRQ4", "IRQ5", "IRQ6", "IRQ7",
		"IRQ8", "IRQ9", "IRQ10", "IRQ11",
		"IRQ12", "IRQ13", "IRQ14", "IRQ15",
		"syscall"
	};

	int i;

	/* "reflect" hardware interrupts (IRQs) to V86 mode */
	if(regs->which_int >= 0x20 && regs->which_int < 0x30) {
		/* for timer interrupt, blink character in upper left corner of screen */
		//if(regs->which_int == 0x20)
		//	blink();

		/* IRQs 0-7 -> pmode exception numbers 20h-27h -> real-mode vectors 8-15 */
		if (regs->which_int < 0x28)
			i = (regs->which_int - 0x20) + 8;
		/* IRQs 8-15 -> pmode exception numbers 28h-2Fh -> real-mode vectors 70h-77h */
		else
			i = (regs->which_int - 0x28) + 0x70;

		v86_int (regs, i);

		return 0;
	}
	/* stack fault from V86 mode with SP=0xFFFF means we're done */
	else if (regs->which_int == 0x0C) {
		if ((regs->eflags & 0x20000uL) && regs->user_esp == 0xFFFF)
			return 1;
	}
	/* GPF from V86 mode: emulate instructions */
	else if (regs->which_int == 0x0D) {
		if (regs->eflags & 0x20000uL) {
			i = v86_emulate (regs);

			/* error */
			if (i < 0) {
				kprintf ("virtual mode x86 error !\n");
				while (1);	
			}

			/* pop from empty stack: end V86 session */
			else if(i > 0)
				return 1;
			return 0;
		}
	}

	/* for anything else, display exception number, exception name, and CPU mode */
	kprintf("*** Exception #%u ", regs->which_int);

	if (regs->which_int <= sizeof(msg) / sizeof(char *))
		kprintf ("(%s) ", msg[regs->which_int]);

	if (regs->eflags & 0x20000uL)
		kprintf ("in V86");
	else if (regs->cs & 0x03)
		kprintf ("in user");
	else
		kprintf ("in kernel");

	kprintf (" mode ***\n");

	/* not reached */
	return -1;
}
