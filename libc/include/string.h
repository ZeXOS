/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _STRING_H
#define _STRING_H

#include <sys/types.h>
#include <strings.h>

extern int memcmp (const void *cs, const void *ct, size_t count);
extern void *memcpy (void *dst_ptr, const void *src_ptr, size_t count);
extern void *memchr (void const *buf, int c, size_t len);
extern void *memsetw (void *dst, int val, size_t count);
extern void *memset (void *dst, int val, size_t count);
extern size_t strlen (const char *str);
extern char *strchr (const char *s, int c);
extern char *strcpy (char *to, const char *from);
extern char *strncpy (char *s1, const char *s2, size_t n);
extern char *strcat (char *s, const char *add);
extern char *strncat (char *dest, char const *src, size_t count);
extern int strcmp (const char *cs, const char *ct);
extern int strncmp (const char *cs, const char *ct, size_t count);
extern char *strstr (char const *s1, char const *s2);
extern size_t strspn (char const *s, char const *accept);
extern char *strpbrk (char const *cs, char const *ct);
extern char *strdup (const char *s);
extern char *strndup (const char *s, size_t n);
extern char *strerror (int errnum);
extern void *memmove (void *dest, const void *src, size_t n);
extern char *strrchr (const char *s, int c);
extern char *strtok (char *str, const char *delim);

#endif

