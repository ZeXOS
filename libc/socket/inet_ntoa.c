/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>

static char tmp_addr[16];

char *inet_ntoa (struct in_addr in)
{
	unsigned *ip = (unsigned *) &in;

	unsigned char a = (unsigned char) *ip;
	unsigned char b = (unsigned char) (*ip >> 8);
	unsigned char c = (unsigned char) (*ip >> 16);
	unsigned char d = (unsigned char) (*ip >> 24);
	
	sprintf (tmp_addr, "%d.%d.%d.%d", a, b, c, d);

	return tmp_addr;
}
