/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "window.h"
#include "cursor.h"
#include "button.h"
#include "menu.h"
#include "dialog.h"
#include "filemanager.h"
#include "config.h"

wmwindow *tview;
unsigned tview_act = 0;

char *file;
unsigned len;

unsigned tview_open (char *name)
{
	if (tview_act || !name)
		return 0;

	file = (char *) malloc (sizeof (char) * 2049);

	if (!file)
		return 0;

	// html web page
	int fd = open (name, O_RDONLY);
	
	if (!fd) {
		return 0;
	}

	int ret = 2048;
	len = 0;

	//while (1) {
		ret = read (fd, file+len, 2048);

		if (!ret)
			return 0;

		len += ret;

		file[len] = '\0';

	/*	if (ret != 512)
			break;

		if (ret == 512)
			file = (char *) realloc ((void *) file, sizeof (char) * len + 512);

		if (!file) {
			free (file);
			return 0;
		}
	}*/

	tview = window_create (name);

	if (!tview)
		return 0;

	tview_act = 1;

	return 1;
}

void tview_draw ()
{
	if (!tview_act || !tview)
		return;

	unsigned i = 0;
	unsigned k = 0;
	unsigned j = 0;

	for (i = 0; i < len; i ++) {
		if (file[i] == '\n') {
			k ++;
			j = 0;
		}

		xtext_putch (tview->x+1+(j*5), tview->y+11+(k*9), 0, file[i]);	

		if ((j*5+1) > tview->size_x)
			tview->size_x += 5;

		if ((11+(k*9)) > tview->size_y)
			tview->size_y += 9;

		j ++;
	}
}

unsigned init_tview ()
{
	tview = 0;
	tview_act = 0;
	len = 0;

	return 1;
}
