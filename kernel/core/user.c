/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <env.h>
#include <user.h>
#include <config.h>

user_t user_list;
user_t *tempuser;

user_t *user_find (char *name)
{
	user_t *user;
	for (user = user_list.next; user != &user_list; user = user->next) {
		if (!strcmp (user->name, name))
			return user;
	}

	return NULL;
}

user_t *user_create (char *name, unsigned attrib, char *pass)
{
	if (user_find (name))
		return 0;

	unsigned name_len = strlen (name);
	unsigned pass_len = strlen (pass);

	user_t *user;
	
	/* alloc and init context */
	user = (user_t *) kmalloc (sizeof (user_t));
	memset (user, 0, sizeof (user_t));

	user->name = (char *) kmalloc (sizeof (char) * name_len + 1);
	memset (user->name, 0, name_len);
	memcpy (user->name, name, name_len);
	user->name[name_len] = '\0';

	user->pass = (char *) kmalloc (sizeof (char) * pass_len + 1);
	memset (user->pass, 0, pass_len);
	memcpy (user->pass, pass, pass_len);
	user->pass[pass_len] = '\0';

	user->attrib = attrib;

	/* add into list */
	user->next = &user_list;
	user->prev = user_list.prev;
	user->prev->next = user;
	user->next->prev = user;

	return user;
}

bool getlogin (int i)
{
	if (!currtty->logged) {
		user_t *user = user_find (currtty->shell);
	
		if (user) {
			tempuser = user;
			currtty->logged = true;
			video_color (7, 0);
			printf ("password: ");
		} else {
			video_color (7, 0);
			printf ("login: ");
		}
	} else {
		if (tempuser) {
			if (!strcmp (currtty->shell, tempuser->pass)) {
				env_set ("USER", tempuser->name);
				currtty->user = tempuser;
				tempuser = NULL;

				console_refresh ();	/* well, login was correct */
			} else {
				video_color (7, 0);
				printf ("login: ");
				currtty->logged = false;
			}
		} else
			currtty->logged = false;
	}

	/* clean console */
	for(i = 0; i < currtty->shell_len; i ++)
		currtty->shell[i] = '\0';
}

unsigned int init_user ()
{
	user_list.next = &user_list;
	user_list.prev = &user_list;

	user_t *root = user_create ("root", USER_GROUP_ADMIN, "root");
	user_t *guest = user_create ("guest", USER_GROUP_GUEST, "guest");

	/* Command which is started, when shell is ready */
	char initcmd[] = CONFIG_UI_SHELL_INITCMD;
	unsigned l = strlen (initcmd);

	if (l)
		command_parser (initcmd, l);

	return 1;
}
