/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <build.h>
#include <mount.h>
#include <errno.h>
#include <cache.h>
#include <vfs.h>
#include <smp.h>
#include <fd.h>

vfs_t vfs_list;

extern mount_t mount_list;

vfs_t *vfs_list_find (char *name, char *mountpoint)
{
	unsigned l = strlen (name);

	vfs_t *vfs;
	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next) {
		if (!strncmp (vfs->name, name, l) && !strcmp (vfs->mountpoint, mountpoint))
			return vfs;
	}

	return 0;
}

vfs_t *vfs_list_findbymp (char *mountpoint)
{
	char mdir[128];
	char pwd[64];
	strcpy (pwd, (char *) env_get ("PWD"));
	unsigned l = strlen (pwd);

	vfs_t *vfs;
	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next) {
	  
		if (strcmp (pwd, vfs->mountpoint))
			continue;

		sprintf (mdir, "%s%s/", vfs->mountpoint, vfs->name);

		if (!strcmp (mdir, mountpoint))
			return vfs;
	}

	return 0;
}

vfs_t *vfs_find (char *file, unsigned file_len)
{
	if (!file)
		return 0;
  
 	char pwd[64];
	strcpy (pwd, (char *) env_get ("PWD"));

	char buf[65];
	unsigned pwd_len = strlen (pwd);

	unsigned i = file[0] == '/' ? 1 : 0;

	if (!i) {
		memcpy (buf, pwd, pwd_len);
		memcpy (buf+pwd_len, file+i, file_len-i);
		buf[pwd_len+file_len-i] = '\0';
	} else {
		memcpy (buf, file, file_len);
		buf[file_len] = '\0';
	}

	if (buf[pwd_len+file_len-i-1] == '/')
		buf[pwd_len+file_len-i-1] = '\0';

	vfs_t *vfs;
	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next) {
		unsigned mp_len = strlen (vfs->mountpoint);
		unsigned nm_len = strlen (vfs->name);
		
		char buf2[128];
		memcpy (buf2, vfs->mountpoint, mp_len);
		memcpy (buf2+mp_len, vfs->name, nm_len);
		buf2[mp_len+nm_len] = '\0';

		if (!cstrcmp (buf, buf2))
			return vfs;
	}
	
	return 0;
}

vfs_t *vfs_list_add (char *name, unsigned attrib, char *mountpoint)
{
	unsigned name_len = strlen (name);
	unsigned mp_len = strlen (mountpoint);

	vfs_t *vfs;
	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next) {
		if (!strcmp (vfs->name, name) && !strcmp (vfs->mountpoint, mountpoint)) {
			DPRINT (DBG_VFS, "ERROR -> vfs_list_add () - vfs object already exist !");
			return 0;
		}
	}

	/* alloc and init context */
	vfs = (vfs_t *) kmalloc (sizeof (vfs_t));

	if (!vfs)
		return 0;

	memset (vfs, 0, sizeof (vfs_t));

	vfs->name = (char *) kmalloc (sizeof (char) * VFS_FILENAME_LEN + 2);

	if (!vfs->name) {
		kfree (vfs);
		return 0;
	}

	memset (vfs->name, 0, VFS_FILENAME_LEN);
	memcpy (vfs->name, name, name_len);
	vfs->name[name_len] = '\0';

	memset (vfs->mountpoint, 0, VFS_MOUNTPOINT_LEN);
	memcpy (vfs->mountpoint, mountpoint, mp_len);
	vfs->mountpoint[mp_len] = '\0';

	vfs->attrib = attrib;

	vfs->content = 0;

	/* add into list */
	vfs->next = &vfs_list;
	vfs->prev = vfs_list.prev;
	vfs->prev->next = vfs;
	vfs->next->prev = vfs;

	return vfs;
}

bool vfs_list_del (vfs_t *vfs)
{
	if (!vfs)
		return 0;

	vfs->next->prev = vfs->prev;
	vfs->prev->next = vfs->next;

	if (vfs->content) {
		if (vfs->content->ptr)
			cache_closebyptr (vfs->content->ptr);

		kfree (vfs->content);
	}

	return 1;
}

bool vfs_list_delbymp (char *mountpoint)
{
	bool f = 0;
	vfs_t *vfs;
	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next) {
		if (!strncmp (vfs->mountpoint, mountpoint, strlen (mountpoint))) {
			f = 1;
			break;
		}
	}

	if (f) {
		vfs->next->prev = vfs->prev;
		vfs->prev->next = vfs->next;

		if (vfs->content) {
			if (vfs->content->ptr)
				kfree (vfs->content->ptr);

			kfree (vfs->content);
		}

		//kfree (vfs);
		return 1;
	}

	return 0;
}

int vfs_mmap (char *file, unsigned file_len, vfs_content_t *content)
{
	vfs_t *vfs = vfs_find (file, file_len);

	if (!vfs) {
		errno_set (ENOENT);

		DPRINT (DBG_VFS, "vfs_mmap () - file '%s' not found", file);
		return 0;
	}
		
	if (vfs->attrib & VFS_FILEATTR_DIR) {
		printf ("ERROR -> this is a directory, not an file\n");
		return 0;
	}

	if (vfs->attrib & VFS_FILEATTR_MOUNTED) {
		partition_t *p = (partition_t *) mount_find ((char *) vfs->mountpoint);

		if (p) {
			unsigned i = 0;
			unsigned l = strlen (vfs->name);
			
			while (strlen (dir[i].name)) {
				if (!strncmp (vfs->name, dir[i].name, l))
					return p->fs->handler (FS_ACT_WRITE, (char *) content, i, 0);

				i ++;
			}

			DPRINT (DBG_VFS, "vfs_mmap () - file '%s' not found in mounted filesystem", file);
			return 0;
		} else {
			printf ("ERROR -> device not respond\n");
			errno_set (EIO);
			return 0;
		}
	} else {
		if (vfs->content) {
			printf ("ERROR -> You can't overwrite content of this file\n");
			errno_set (EINVAL);
			return 0;
		}

		vfs->content = (vfs_content_t *) kmalloc (sizeof (vfs_content_t));

		if (!vfs->content) {
			errno_set (ENOMEM);
			return 0;
		}

		/* NOTE: when file in VFS is device, we match it as READable */
		if (!(vfs->attrib & VFS_FILEATTR_READ))
			vfs->attrib |= VFS_FILEATTR_READ;

		/*cache_t *cache = cache_create (data, len, 1);

		if (!cache) {
			errno_set (ENOMEM);
			return 0;
		}*/

		vfs->content->ptr = content->ptr;
		vfs->content->len = content->len;
	}

	return 1;
}

int vfs_read (char *file, unsigned file_len, vfs_content_t *content)
{
	vfs_t *vfs = vfs_find (file, file_len);

	if (!vfs)
		return -2;
	
	if (vfs->attrib & VFS_FILEATTR_DIR) {
		printf ("ERROR -> This is a directory, not an file\n");
		return -1;
	}

	if (vfs->attrib & VFS_FILEATTR_MOUNTED) {
		partition_t *p = (partition_t *) mount_find ((char *) vfs->mountpoint);

		if (p) {
			unsigned i = 0;
			unsigned l = strlen (vfs->name);
	
			while (strlen (dir[i].name)) {
				if (!strncmp (vfs->name, dir[i].name, l)) {
					if (!p->fs->handler (FS_ACT_READ, (char *) content, i, 0)) {
						printf ("ERROR -> Device read action\n");
						return -3;
					}

					return i;
				}

				i ++;
			}
		} else {
			printf ("ERROR -> device not respond\n");
			return -1;
		}
	} else {
		if (vfs->attrib & VFS_FILEATTR_DEVICE && !(vfs->attrib & VFS_FILEATTR_READ)) {
			printf ("ERROR -> This file is non-char or non-block device\n");
			return -1;
		}

		if (!vfs->content) {
			printf ("ERROR -> This file not contain a valid data\n");
			return -1;
		}

		if (!vfs->content->ptr) {
			printf ("ERROR -> This file not contain a valid data\n");
			return -4;
		}
				
		content->ptr = vfs->content->ptr;
		content->len = vfs->content->len;

		return 0;
	}

	return -2;
}

int vfs_cat (char *file, unsigned file_len)
{
	unsigned long i;
	vfs_content_t content;
	content.ptr = 0;
	content.len = 0;

	int index = vfs_read (file, file_len, &content);

	if (index < 0)
		return index;
#ifdef ARCH_i386
	paging_disable ();	/* HACK: we can occasionally access user address space (behind kernel-space which is unmapped for us) */
#endif
	if (content.ptr)
		for (i = 0; i < content.len; i ++)
			tty_putch (content.ptr[i]);
#ifdef ARCH_i386
	paging_enable ();
#endif
	return index;
}

int vfs_cp (char *what, unsigned what_len, char *where, unsigned where_len)
{
	vfs_content_t content;
	
	int index = vfs_read (what, what_len, &content);

	if (index == -1)
		return -2;

	if (!vfs_touch (where, where_len))
		return -1;
	
	if (!vfs_mmap (where, where_len, &content))
		return 0;

	return 1;
}

int vfs_ls (char *file, unsigned file_len)
{
	char ls[64];

	vfs_t *vfs;
	bool res = false;

	if (!file_len) {
		strcpy (ls, (char *) env_get ("PWD"));
		for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next)
			if (!strcmp (vfs->mountpoint, ls)) {
				printf ("%s\t", vfs->name);
				res = true;
			}
	} else {
		if (!strncmp(file,"/",1))
			strcpy (ls,file);
		else {
			strcpy (ls,(char *) env_get ("PWD"));
			strcpy (ls+strlen(ls),file);
		}

		if (strncmp(file+file_len-1,"/",1)) {
			strcpy(ls+strlen(ls),"/");
		}

		for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next)
			if (!strcmp (vfs->mountpoint, ls) ||
			    !strcmp (vfs->mountpoint+1, ls)) {
				printf ("%s\t", vfs->name);
				res = true;
			}
	}

	if (res)
		printf ("\n");

	return 1;
}

int vfs_cd (char *file, unsigned file_len)
{
	/* go back */
	if (!strcmp (file, "..")) {
		char pwd[64];
		strcpy (pwd, (char *) env_get ("PWD"));

		unsigned pwd_len = strlen (pwd);
						
		if (!strcmp (pwd, "/"))
			return 0;
				
		/* vymaze kus retezce - az po predposledni / */
		pwd_len --;	// preskoci / na konci retezce
		while (pwd_len) {
			pwd_len --;
			if (pwd[pwd_len] == '/') {
				pwd[pwd_len+1] = '\0';
				break;
			}
		}

		vfs_t *vfs;
		for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next)
			if (!strcmp (vfs->mountpoint, pwd)) {
				if (vfs->attrib & VFS_FILEATTR_MOUNTED) {
					//printf ("vfs->mountpoint: %s | pwd: %s\n", vfs->mountpoint, pwd);
					partition_t *p = mount_find (pwd);

					if (p) {
						mount (p, file, "");	// first return to last directory

						umount (p, (char *) env_get ("PWD"));	// then umount old directory

						while (vfs_list_delbymp ((char *) env_get ("PWD")));

						break;
					} else {
						printf ("ERROR -> device not respond\n");
						return 0;
					}
				}
			}
		
		env_set ("PWD", pwd);	// set new directory

		return 1;
	}

	/* nothing */
	if (!strcmp (file, "."))
		return 1;

	vfs_t *vfs;
	/* go to root fs dir */
	if (!strcmp (file, "/")) {
		env_set ("PWD", file);
	
		for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next) {
			if (vfs->attrib & VFS_FILEATTR_MOUNTED) {
				partition_t *p = mount_find ((char *) env_get ("PWD"));
	
				if (p)
					mount (p, file, "");
			}
		}

		return 1;
	}

	/* go to another directory */
	char pwd[64];
	strcpy (pwd, (char *) env_get ("PWD"));

	char buf[65];
	unsigned pwd_len = strlen (pwd);

	unsigned i = file[0] == '/' ? 1 : 0;

	if (!i) {
		memcpy (buf, pwd, pwd_len);
		memcpy (buf+pwd_len, file+i, file_len-i);
		buf[pwd_len+file_len-i] = '\0';
	} else {
		memcpy (buf, file, file_len);
		buf[file_len] = '\0';
	}

	if (buf[pwd_len+file_len-i-1] == '/')
		buf[pwd_len+file_len-i-1] = '\0';

	unsigned buf_l = strlen (buf);
	if (buf[buf_l-1] == '/')
		buf[buf_l-1] = '\0';

	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next) {
		unsigned mp_len = strlen (vfs->mountpoint);
		unsigned nm_len = strlen (vfs->name);
		
		char buf2[65];
		memcpy (buf2, vfs->mountpoint, mp_len);
		memcpy (buf2+mp_len, vfs->name, nm_len);
		buf2[mp_len+nm_len] = '\0';

		if (!cstrcmp (buf, buf2)) {
			if (vfs->attrib & VFS_FILEATTR_FILE) {
				printf ("ERROR -> this is a file, not an directory\n");
				return 0;
			}

			// check permissions
			if (vfs->attrib & VFS_FILEATTR_SYSTEM && strcmp ((char *) env_get ("USER"), "root")) {
				printf ("ERROR -> only root can do that\n");
				return 0;
			}
			
			memcpy (pwd, buf2, mp_len+nm_len);
			pwd[mp_len+nm_len] = '/';
			pwd[mp_len+nm_len+1] = '\0';

			if (vfs->attrib & VFS_FILEATTR_MOUNTED) {
				partition_t *p = mount_find ((char *) env_get ("PWD"));

				if (p)
					mount (p, file, pwd);
				else {
					printf ("ERROR -> device not respond\n");
					return 0;
				}
			}

			env_set ("PWD", pwd);

			return 1;
		}
	}

	return 0;
}

int vfs_mkdir (char *file, unsigned file_len)
{
	if (!file_len)
		return 0;
	
	char pwd[64];
	strcpy (pwd, (char *) env_get ("PWD"));

	unsigned i;
	for (i = file_len; i; i --) {
		if (file[i] == '/') {
			vfs_cd (file, i);
			break;
		}
	}

	char *path = 0;
	char *f = 0;

	if (i) {
		path = (char *) kmalloc (sizeof (char) * (file_len+1));

		if (!path)
			return 0;

		memcpy (path, file, i+1);
		path[i+1] = '\0';

		f = file + i + 1;
	} else {
		path = (char *) pwd;
		f = file;
	}

	partition_t *p = mount_find ((char *) env_get ("PWD"));

	if (p) {
		vfs_list_add (f, VFS_FILEATTR_DIR | VFS_FILEATTR_READ | VFS_FILEATTR_BIN | VFS_FILEATTR_MOUNTED, (char *) path);
		p->fs->handler (FS_ACT_MKDIR, file, 0, strlen (file));
	} else {
		vfs_list_add (f, VFS_FILEATTR_DIR | VFS_FILEATTR_READ | VFS_FILEATTR_BIN, (char *) path);
		DPRINT (DBG_VFS, "NOTE -> this directory is created only in virtual file system");
	}

	if (i) {
		kfree (path);
		vfs_cd (pwd, strlen (pwd));
	}

	DPRINT (DBG_VFS, "vfs_mkdir (%s)", file);

	return 1;
}

int vfs_touch (char *file, unsigned file_len)
{
	if (!file_len)
		return 0;
	
	char pwd[64];
	strcpy (pwd, (char *) env_get ("PWD"));

	unsigned i;
	for (i = file_len; i; i --) {
		if (file[i] == '/') {
			vfs_cd (file, i);
			break;
		}
	}

	char *path = 0;
	char *f = 0;

	if (i) {
		path = (char *) kmalloc (sizeof (char) * (file_len+1));

		if (!path)
			return 0;

		memcpy (path, file, i+1);
		path[i+1] = '\0';

		f = file + i + 1;
	} else {
		path = (char *) pwd;
		f = file;
	}

	partition_t *p = mount_find ((char *) env_get ("PWD"));

	if (p) {
		printf ("f: %s\n", f);
		vfs_list_add (f, VFS_FILEATTR_FILE | VFS_FILEATTR_READ | VFS_FILEATTR_BIN | VFS_FILEATTR_MOUNTED, (char *) path);
		p->fs->handler (FS_ACT_TOUCH, f, 0, strlen (f));
	} else {
		vfs_list_add (f, VFS_FILEATTR_FILE | VFS_FILEATTR_READ | VFS_FILEATTR_BIN, (char *) path);
		DPRINT (DBG_VFS, "NOTE -> this file is created only in virtual file system");
	}

	if (i) {
		kfree (path);
		vfs_cd (pwd, strlen (pwd));
	}
		
	DPRINT (DBG_VFS, "vfs_touch (%s)", file);
	
	return 1;
}

int vfs_rm (char *file, unsigned file_len)
{
	if (!file_len)
		return 0;
	
	char pwd[64];
	strcpy (pwd, (char *) env_get ("PWD"));

	unsigned i;
	for (i = file_len; i; i --) {
		if (file[i] == '/') {
			vfs_cd (file, i);
			break;
		}
	}

	char *path = 0;
	char *f = 0;

	if (i) {
		path = (char *) kmalloc (sizeof (char) * (file_len+1));

		if (!path)
			return 0;

		memcpy (path, file, i+1);
		path[i+1] = '\0';

		f = file + i + 1;
	} else {
		path = (char *) pwd;
		f = file;
	}

	vfs_t *vfs = vfs_list_find (f, (char *) path);

	/* file not found */
	if (!vfs)
		return -1;

	if (vfs->attrib & VFS_FILEATTR_SYSTEM || vfs->attrib & VFS_FILEATTR_DIR)
		return -2;

	if (vfs->attrib & VFS_FILEATTR_MOUNTED) {
		partition_t *p = mount_find ((char *) pwd);

		if (!p)
			return 0;

		p->fs->handler (FS_ACT_RM, file, 0, strlen (file));
	} else {
		/* delete vfs file with content */
		vfs_list_del (vfs);
	}

	if (i) {
		kfree (path);
		vfs_cd (pwd, strlen (pwd));
	}

	DPRINT (DBG_VFS, "vfs_rm (%s)", file);
	
	return 1;
}

vfs_dirent_t *vfs_dirent ()
{
	char pwd[64];
	strcpy (pwd, (char *) env_get ("PWD"));

	vfs_t *vfs;
	bool res = false;

	unsigned dirents = 0;

	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next)
		if (!strcmp (vfs->mountpoint, pwd))
			dirents ++;

	if (!dirents)
		return 0;

	vfs_dirent_t *dirent = (vfs_dirent_t *) kmalloc (sizeof (vfs_dirent_t) * (dirents+1));

	if (!dirent)
		return 0;

	unsigned i = 0;
	for (vfs = vfs_list.next; vfs != &vfs_list; vfs = vfs->next)
		if (!strcmp (vfs->mountpoint, pwd)) {
			dirent[i].name = vfs->name;

			dirent[i].attrib = vfs->attrib;

			if ((i+1) == dirents)
				dirent[i].next = 0;
			else
				dirent[i].next = 1;

			i ++;
		}

	dirent[dirents].next = 0;

	return dirent;
}

unsigned int init_vfs ()
{
	mount_list.next = &mount_list;
	mount_list.prev = &mount_list;

	vfs_list.next = &vfs_list;
	vfs_list.prev = &vfs_list;

	if (!(kernel_attr & KERNEL_NOLIVE)) {
		vfs_list_add ("bin", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/");
		vfs_list_add ("cd", VFS_FILEATTR_FILE | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ | VFS_FILEATTR_BIN, "/bin/");
		vfs_list_add ("ls", VFS_FILEATTR_FILE | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ | VFS_FILEATTR_BIN, "/bin/");
		vfs_list_add ("exec", VFS_FILEATTR_FILE | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ | VFS_FILEATTR_BIN, "/bin/");
		vfs_list_add ("dev", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/");
		vfs_list_add ("etc", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/");
		vfs_list_add ("mnt", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/");
		vfs_list_add ("floppy", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/mnt/");
		vfs_list_add ("cdrom", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/mnt/");
		vfs_list_add ("hdd", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/mnt/");
		vfs_list_add ("usr", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/");
		vfs_list_add ("root", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/usr/");
		vfs_list_add ("proc", VFS_FILEATTR_DIR | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_READ, "/");
	}

	/* let's build /proc/cpuinfo */
	smp_cpu_check ();

	return 1;
} 
