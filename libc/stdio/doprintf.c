/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <_printf.h> /* fnptr_t */
#include <string.h> /* strlen() */
#include <stdarg.h> /* va_list, va_arg() */
#include <stdio.h>
#include <stdlib.h>

int do_printf (const char *fmt, va_list args)
{
	unsigned i = 0;
	unsigned count = 0;
	long num = 0;
	char str[16];
	unsigned char *buf = 0;

	for (; *fmt; fmt ++) {
		/* probably argument is here */
		if (*fmt == '%') {
			fmt ++;

			if (!*fmt)
				break;

			if (*fmt == 'd' || *fmt == 'i' || *fmt == 'u') {
				num = va_arg (args, int);
					
				itoa (num, str, 10);
					
				for (i = 0; str[i]; i ++)
					putch (str[i]);

				count += i;

				continue;
			}

			if (*fmt == 'f') {
				num = va_arg (args, double);
				
				int f;

				if (num >= 1)
					f = (int) num;
				else {
					count += 2;

					putch ('0');
					putch ('.');

					f = (int) ((double) num * (double) 10000);
				}

				itoa (f, str, 10);
						
				for (i = 0; str[i]; i ++)
					putch (str[i]);

				count += i;

				continue;
			}

			if (*fmt == 'x' || *fmt == 'X') {
				num = va_arg (args, int);
					
				itoa (num, str, 16);
					
				for (i = 0; str[i]; i ++)
					putch (str[i]);

				count += i;

				continue;
			}
				
			if (*fmt == 'c') {
				num = va_arg (args, unsigned char);

				putch ((unsigned char) num);

				count ++;

				continue;
			}

			if (*fmt == 's') {
				buf = va_arg (args, unsigned char *);

				for (i = 0; buf[i]; i ++)
					putch ((unsigned char) buf[i]);

				count += i;

				continue;
			}

			continue;
		}

		putch (*fmt);
		count ++;

	}

	return count;
}

int do_sprintf (const char *fmt, va_list args, char *ptr, int len)
{
	unsigned i = 0;
	unsigned count = 0;
	long num = 0;
	char str[16];
	unsigned char *buf = 0;

	for (; *fmt; fmt ++) {
		/* probably argument is here */
		if (*fmt == '%') {
			fmt ++;

			if (!*fmt)
				break;

			if (*fmt == 'd' || *fmt == 'i' || *fmt == 'u') {
				num = va_arg (args, int);
					
				itoa (num, str, 10);
					
				for (i = 0; str[i]; i ++)
					ptr[i+count] = str[i];

				count += i;

				continue;
			}

			if (*fmt == 'x' || *fmt == 'X') {
				num = va_arg (args, int);
					
				itoa (num, str, 16);
					
				for (i = 0; str[i]; i ++)
					ptr[i+count] = str[i];

				count += i;

				continue;
			}
				
			if (*fmt == 'c') {
				num = va_arg (args, unsigned char);

				ptr[count] = (unsigned char) num;

				count ++;

				continue;
			}

			if (*fmt == 's') {
				buf = va_arg (args, unsigned char *);

				for (i = 0; buf[i]; i ++)
					ptr[i+count] = buf[i];

				count += i;

				continue;
			}

			continue;
		}

		ptr[count] = *fmt;
		count ++;

		if (len != -1)
		if (count >= (unsigned) len)
			break;
	}

	return count;
}

/*****************************************************************************
VSPRINTF
*****************************************************************************/

int vsprintf (char *buffer, const char *fmt, va_list args)
{
	if (!fmt)
		return 0;

	int ret_val = do_sprintf (fmt, args, (char *) buffer, -1);
	buffer[ret_val] = '\0';

	return ret_val;
}

/*****************************************************************************
VSNPRINTF
*****************************************************************************/

int vsnprintf (char *buffer, size_t size, const char *fmt, va_list args)
{
	if (!fmt)
		return 0;

	int ret_val = do_sprintf (fmt, args, (char *) buffer, size);
	buffer[ret_val] = '\0';

	return ret_val;
}

/*****************************************************************************
VFPRINTF
*****************************************************************************/

int vfprintf (FILE *stream, const char *fmt, va_list args)
{
	if (!stream || !fmt)
		return 0;

	char *buffer = (char *) malloc (sizeof (char) * 256);

	if (!buffer)
		return 0;

	int ret_val = do_sprintf (fmt, args, (char *) buffer, 255);
	buffer[ret_val] = '\0';

	fwrite (buffer, ret_val, 1, stream);

	free (buffer);

	return ret_val;
}


#if 0 /* testing */
/*****************************************************************************
*****************************************************************************/
int sprintf(char *buffer, const char *fmt, ...)
{
	va_list args;
	int ret_val;

	va_start(args, fmt);
	ret_val = vsprintf(buffer, fmt, args);
	va_end(args);
	return ret_val;
}
/*****************************************************************************
PRINTF
You must write your own putchar()
*****************************************************************************/
int vprintf_help(unsigned c, void **ptr)
{
	putchar(c);
	return 0 ;
}
/*****************************************************************************
*****************************************************************************/
int vprintf(const char *fmt, va_list args)
{
	return do_printf(fmt, args, vprintf_help, NULL);
}
/*****************************************************************************
*****************************************************************************/
int printf(const char *fmt, ...)
{
	va_list args;
	int ret_val;

	va_start(args, fmt);
	ret_val = vprintf(fmt, args);
	va_end(args);
	return ret_val;
}
/*****************************************************************************
*****************************************************************************/
int main(void)
{
	char buf[64];

	sprintf(buf, "%u score and %i years ago...\n", 4, -7);
	puts(buf);

	sprintf(buf, "-1L == 0x%lX == octal %lo\n", -1L, -1L);
	puts(buf);

	printf("<%-08s> and <%08s> justified strings\n", "left", "right");
	return 0;
}
#endif
