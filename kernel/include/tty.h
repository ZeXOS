/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _TTY_H
#define _TTY_H

#include <task.h>
#include <user.h>
#include <mytypes.h>

#define FD_TERM			0x2000

#define TTY_CON_BUF		16000
#define TTY_COUNT_MAX		1024
#define TTY_SCREEN_RES_VGA	4000
#define TTY_SCREEN_RES_VESA	4000

/* font type, size */
typedef struct {
	unsigned char *c;
	unsigned char x;
	unsigned char y;
} tty_font_t;

/* character array resolution - 80x25 */
typedef struct {
	unsigned short x;
	unsigned short y;
} tty_char_t;

/* log for read () op purposes */
typedef struct {
	char *buf;
	unsigned short len;
	unsigned short pos;
} tty_log_t;

/* Device structure */
typedef struct tty_context {
	struct tty_context *next, *prev;
      
	task_t *task;
	char *name;
	char *screen;
	unsigned char screen_x;
	unsigned char screen_y;
	user_t *user;
	char shell[64];
	unsigned shell_len;
	bool logged;
	char pwd[64];
	bool active;
	tty_log_t *log;
} tty_t;

/* externs */
extern tty_font_t tty_font;		/* global font settings */
extern tty_char_t tty_char;		/* global character array settings */

extern unsigned int init_tty ();
extern bool tty_write (tty_t *tty, char *str, unsigned len);
extern bool tty_putch (char c);
extern bool tty_putnch (tty_t *tty, char c);
extern bool tty_change (tty_t *tty);
extern tty_t *tty_find (char *name);
extern tty_t *tty_findbytask (task_t *task);
extern tty_t *gtty_init ();
extern tty_t *tty_create ();
extern void tty_listview ();

#endif
 
