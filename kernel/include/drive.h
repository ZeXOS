/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DRIVE_H
#define _DRIVE_H

#define ATA_STATUS_ERR		0x1
#define ATA_STATUS_DRQ		0x8
#define ATA_STATUS_DF		0x20
#define ATA_STATUS_DRDY		0x40
#define ATA_STATUS_BUSY		0x80

#define LBA_FLAG_ERROR		0x0
#define LBA_FLAG_BUSY		0x1
#define LBA_FLAG_DATA		0x2

#define ATA_STATUS		0x7

#define DRIVE_TYPE_HDD		0x1
#define DRIVE_TYPE_CDROM	0x2

extern bool ata_acthandler (unsigned act, char drive, char *block);
#endif

