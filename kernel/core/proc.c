/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <proc.h>
#include <tty.h>
#include <task.h>
#include <signal.h>
#include <build.h>
#include <paging.h>
#include <vfs.h>
#include <net/socket.h>
#include <fd.h>

static void *thread_arg;
static void *(*thread_entry) (void *);

static pid_t newpid = 0;

proc_t proc_list;
proc_t proc_kernel;

extern task_t task_list;
extern task_t *_curr_task;
extern unsigned long timer_ticks;

pid_t fork ()
{
	proc_t *p = proc_find (_curr_task);

	if (!p)
		return 0;

#ifdef ARCH_i386
	return proc_create (p->tty, p->name, p->task->state[0].JMPBUF_IP)->pid;
#endif
}

void proc_display ()
{
	printf ("PID\tCMD\tMEM\n");

	proc_t *proc;
	for (proc = proc_list.next; proc != &proc_list; proc = proc->next)
		printf ("%d\t%s\t%dkB\n", proc->pid, proc->name, umem_used_proc (proc) / 1024);
}

void proc_top ()
{
	tty_t *tty = currtty;
	
	unsigned long ltime = 0;
	
	for (;;) {
		if (tty != currtty) {
			schedule ();
			continue;
		}
		
		tty_cls (tty);
	  
		printf ("TID\tTASK\t\tCPU Usage\n");

		task_t *task;
		for (task = task_list.next; task != &task_list; task = task->next) {
			unsigned usage = 100 - ((task->lasttick - ltime) / 10);
			char name[17];
			memset (name, ' ', 16);
			memcpy (name, task->name, strlen (task->name) > 16 ? 16 : strlen (task->name));
			name[16] = '\0';
			
			printf ("%d\t%s%d%%\n", task->id, name, usage > 100 ? 100 : usage);
			schedule ();
		}
		
		
		proc_t *proc;
		for (proc = proc_list.next; proc != &proc_list; proc = proc->next) {
			printf ("\nPID\tCMD\t\tMEM\tCPU Usage\n");
			break;
		}
		
		for (proc = proc_list.next; proc != &proc_list; proc = proc->next) {
			schedule ();
			unsigned usage = 100 - ((proc->task->lasttick - ltime) / 10);
			char name[17];
			memset (name, ' ', 16);
 			memcpy (name, proc->task->name, strlen (proc->task->name) > 16 ? 16 : strlen (proc->task->name));
			name[16] = '\0';
			printf ("%d\t%s%dkB\t%d%%\n", proc->pid, name, umem_used_proc (proc) / 1024, usage > 100 ? 100 : usage);
		}
		
		unsigned long stime = timer_ticks;
		
		ltime = timer_ticks;
		
		/* timeout for 1s */
		while ((stime+1000) >= timer_ticks) {
			if (getkey () == 'q')
				return;
			
			schedule ();
		}
	}
}

proc_t *proc_find (task_t *task)
{
	proc_t *proc;
	for (proc = proc_list.next; proc != &proc_list; proc = proc->next) {
		if (!proc->task)
			continue;

		if (proc->task->id == task->id)
			return proc;
	}

	return 0;
}

proc_t *proc_findbypid (pid_t pid)
{
	proc_t *proc;
	for (proc = proc_list.next; proc != &proc_list; proc = proc->next) {
		if (proc->pid == pid)
			return proc;
	}

	return 0;
}

unsigned proc_fd_close (proc_t *proc)
{
	fd_t *f;

	unsigned i;

	for (;;) {
		i = 0;
		for (f = fd_list.next; f != &fd_list; f = f->next) {
			if (f->proc == proc) {
				if (f->flags & FD_SOCK)	// socket close
					sclose (f->id);

				fd_delete (f);

				i ++;
				break;
			}
		}

		if (!i)
			return 1;
	}

	return 0;
}

proc_t *proc_create (tty_t *tty, char *name, unsigned entry)
{
	proc_t *proc = NULL;
	task_t *task = (task_t *) task_create (name, entry, PROC_USER_PRIORITY);

	if (!task) {
		DPRINT (DBG_PROC, "proc_create () - !task");
		return 0;
	}

	/* alloc and init context */
	proc = (proc_t *) kmalloc (sizeof (proc_t));

	if (!proc) {
		DPRINT (DBG_PROC, "proc_create () - !proc");
		return 0;
	}

	/* remember task, because we need it for scheduler work */
	proc->task = task;

	/* remember new pid of process */
	proc->pid = ++ newpid;

	/* remember tty, where we start this process */
	proc->tty = tty;

	/* setup process name */
	strcpy (proc->name, name);

	/* clear process info */
	proc->start = 0;
	proc->code = 0;
	proc->data = 0;
	proc->data_off = 0;
	proc->bss = 0;
	proc->end = 0;

	proc->argc = 0;
	proc->argv = 0;

	proc->flags = 0;

	/* add structure into process list */
	proc->next = &proc_list;
	proc->prev = proc_list.prev;
	proc->prev->next = proc;
	proc->next->prev = proc;

	/* create virtual file in /proc with name of new process */
	vfs_list_add (name, VFS_FILEATTR_FILE | VFS_FILEATTR_SYSTEM, "/proc/");

	DPRINT (DBG_PROC, "proc -> %s, pid %d, entry 0x%x", name, newpid, entry);

	return proc;
}

extern bool task_done (task_t *task);
bool proc_done (proc_t *proc)
{
	if (proc) {
		/* verify task structure */
		if (!proc->task)
			return 0;
#ifdef ARCH_i386
		/* unmap memory pages */
		proc_vmem_unmap (proc);
#endif
		/* delete all tasks from process context */
		unsigned short id = proc->task->id;

		for (;;) {
			task_t *task = task_find (id);

			if (!task)
				break;

			if (!task_done (task))
				return 0;
		}

		DPRINT (DBG_PROC, "proc -> pid %d done", proc->pid);

		/* close all un-closed file descriptors */
		proc_fd_close (proc);

		/* delete process from context */
		proc->next->prev = proc->prev;
		proc->prev->next = proc->next;

		proc->task = 0;

		/* free memory from process parameters */
		proc_arg_free (proc);

		/* free all memory blocks allocated by process */
		ufree_proc (proc);

		/* free process structure */
		kfree (proc);

		return 1;
	}

	return 0;
}

bool proc_signal (proc_t *proc, unsigned signal)
{
#ifdef ARCH_i386
	if (!proc)
		return 0;

	/* check for daemon */
	if (proc->flags & PROC_FLAG_DAEMON)
		return 0;

	task_t *task = proc->tty->task;

	/* this unlock tty, enable itrs, etc (see command_exec ())*/
	proc->pid = 0;

	/* disable interrupts */
	if (!int_disable ())
		return 0;

	/* jump back from app code to kernel code (task) */
	longjmp (task->state, 1);
#else
	DPRINT (DBG_PROC, "proc_signal: Not implemented for this platform !");
#endif
	return 1;
}

sighandler_t signal (int signum, sighandler_t handler)
{
	/* try to search current process */
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return 0;

	if (proc->task != _curr_task)
		return 0;

	if (proc->tty != currtty)
		return 0;

	/* send signal to current process */
	proc_signal (proc, signum);

	return handler;
}

bool proc_arg_set (proc_t *proc, char *arg, unsigned argl)
{
	if (!proc)
		return 0;

	unsigned i = 0;
	unsigned y = 0;
	unsigned x = 0;

	if (proc->argc)
		return 0;

	/* First calculate length of array */
	if (argl)
	while (i < (argl+1)) {
		if (arg[i] == ' ' || arg[i] == '\0')
			x ++;

		i ++;
	}

	proc->argv = (char **) kmalloc (sizeof (unsigned) * (x+2));

	if (!proc->argv)
		return 0;

	proc->argc = x+1;	/* +1 because argv[0] is name of process so it is one entry */

	/* setup program name entry */
	i = strlen (proc->name);

	proc->argv[0] = (char *) kmalloc (sizeof (char) * (i+1));
	memcpy (proc->argv[0], proc->name, i);
	proc->argv[0][i] = '\0';

	x = 1;
	i = 0;

	/* assign values to array */
	while (i < (argl+1)) {
		if (arg[i] == ' ' || arg[i] == '\0' || i == argl) {
			if (!y) {
				proc->argv[x] = (char *) kmalloc (sizeof (char) * (i+1));

				if (!proc->argv[x])
					return 0;

				memcpy (proc->argv[x], arg, i);
				proc->argv[x][i] = '\0';
			} else {
				proc->argv[x] = (char *) kmalloc (sizeof (char) * (i+1-y));

				if (!proc->argv[x])
					return 0;

				memcpy (proc->argv[x], arg+y+1, i-y-1);
				proc->argv[x][i-y-1] = '\0';
			}
	
			y = i;
			x ++;
		}

		i ++;
	}

	return 1;
}

bool proc_arg_free (proc_t *proc)
{
	if (!proc)
		return 0;

	if (!proc->argv || !proc->argc)
		return 0;

	unsigned i = 0;

	while (i < proc->argc) {
		kfree (proc->argv[i]);
		i ++;
	}

	kfree (proc->argv);

	proc->argc = 0;

	return 1;
}

bool proc_arg_get (proc_t *proc, unsigned *argc, char **argv)
{
	if (!proc)
		return 0;

	if (!proc->argv || !proc->argc || !argc)
		return 0;

	argv = proc->argv;
	*argc = proc->argc;

	return 1;
}

bool proc_flag_set (proc_t *proc, unsigned short flag)
{
	if (!proc)
		return 0;

	if (proc->flags & flag)
		return 0;

	proc->flags |= flag;

	return 1;
}

unsigned proc_page_fault ()
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return 0;

	video_color (15, 0);
	printf ("%s: signal SIGSEGV\n", proc->name);

	/* send SIGSEGV signal to running process */
	proc_signal (proc, SIGSEGV);

	return 1;
}

unsigned proc_vmem_map (proc_t *proc)
{
#ifdef ARCH_i386
	/* create new page_cover "shield" for process */
	proc->task->page_cover = (page_ent_t *) page_cover_create ();

	/* map kernel space */
	page_mmap (proc->task->page_cover, (void *) 0x0, (void *) 0x400000, 1, 0);

	/* map VESA double buffer memory - enough for 16bit 800x600 - 1MB for framebuffer */
	page_mmap (proc->task->page_cover, (void *) 0x400000, (void *) proc->start, 1, 0);

	/* calculate size of process image (binary file) */
	unsigned p = (unsigned) palign ((void *) (proc->end - proc->start));

	/* map process image - app should think, that it is placed at 8MB in ram */
	page_mmap (proc->task->page_cover, (void *) 0x800000, (void *) 0x800000+p, 0, 1);

	/* map end of kernel space */
	page_mmap (proc->task->page_cover, (void *) proc->start+p, (void *) 0x800000, 1, 0);

	proc->heap = 0x800000;
#endif
	return 1;
}

unsigned proc_vmem_unmap (proc_t *proc)
{
	if (!proc)
		return 0;

	if (!proc->task)
		return 0;
#ifdef ARCH_i386
	if (!proc->task->page_cover)
		return 0;

	page_ent_t *page_cover = proc->task->page_cover;

	/* switch to kernel page directory */
	page_dir_switch (task_list.next->page_cover->page_dir);

	/* unmap kernel area */
	page_unmmap (page_cover, (void *) 0x0, (void *) 0x400000);

	/* unmap framebuffer area */
	//page_unmmap (page_cover, (void *) 0x400000, (void *) 0x500000);

	/* unmap process image */
	page_unmmap (page_cover, (void *) 0x400000, (void *) proc->start);

	unsigned p = (unsigned) palign ((void *) (proc->end - proc->start));

	/* unmap end of user-space */
	page_unmmap (page_cover, (void *) 0x800000, (void *) 0x800000+p);

	/* unmap the rest */
	page_unmmap (page_cover, (void *) proc->start+p, (void *) 0x800000);

	/* unmap the allocated memory */
	page_unmmap (page_cover, (void *) 0x800000+p, (void *) proc->heap+p);

	if (!page_cover_delete (page_cover)) {
		DPRINT (DBG_PROC, "ERROR -> proc_vmem_unmap () - proc: %s", proc->name);
		return 0;
	}

	proc->task->page_cover = 0;
#endif
	return 1;
}

void proc_thread_context ()
{
	thread_entry (thread_arg);
#ifdef ARCH_i386
	sys_threadclose (0);
#endif
}

unsigned proc_thread_create (void *entry, void *arg)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return 0;

	char *name = proc->name;

	unsigned p = (unsigned) entry;

	thread_entry = (void *) p;
	thread_arg = arg;

	/* create new process thread */
	task_t *task = (task_t *) task_create (name, (unsigned) &proc_thread_context, PROC_USER_PRIORITY);

	if (!task)
		return 0;

	/* HACK: set task id same as current task (our process) */
	if (proc->task) {
		task->id = proc->task->id;
#ifdef ARCH_i386
		/* use same page_cover too for correctly paging */
		if (proc->task->page_cover)
			task->page_cover = proc->task->page_cover;
#endif
	}

	DPRINT (DBG_PROC, "process: new thread '%s' : 0x%x", name, p);

	/* NOTE: this is pretty needed, because thread argument and entry point is then same as previous */
	schedule ();

	return 1;
}

unsigned proc_thread_destroy (proc_t *proc, task_t *task)
{
	/* we won't close main process thread */
	if (proc->task == task)
		return 0;

	if (!task_done (task))
		return 0;

	DPRINT (DBG_PROC, "process: close thread '%s'", task->name);

	return 1;
}

unsigned int init_proc (void)
{
	proc_list.next = &proc_list;
	proc_list.prev = &proc_list;
	
	proc_kernel.task = task_find (0);
#ifdef ARCH_i386
	proc_kernel.heap = PAGE_MEM_HIGH;
#endif
	proc_kernel.start = 0;
	proc_kernel.end = 0;

	return 1;
}
