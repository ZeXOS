/*
 *  ZeX/OS
 *  Copyright (C) 2009  Martin 'povik' Poviser (martin.povik@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */                                                                                                                                                                  

#ifndef _PIPE_H
#define _PIPE_H

#include <mytypes.h>

#define PIPE_BUFFER_PART_SIZE 128

#define FD_PIPE 0x1000

typedef struct pipe_buffer {
	BYTE buffer[PIPE_BUFFER_PART_SIZE];
	struct pipe_buffer *next, *prev;
} pipe_buffer_t;

typedef struct pipe {
	pipe_buffer_t *buffer_list_start;
	pipe_buffer_t *buffer_list_end;
	unsigned int buffer_start_pos;
	unsigned int buffer_end_len;
	int fd_a, fd_b;
	struct pipe *next, *prev;
} pipe_t;

pipe_t *pipe_get (int fd);
void pipe_close (int fd);
int pipe_write (pipe_t *p, BYTE *buffer, unsigned int buffer_len);
int pipe_read (pipe_t *p, BYTE *buffer, unsigned int buffer_len);
int pipe (int fds[2]);

#endif
