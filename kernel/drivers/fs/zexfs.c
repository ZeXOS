/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <string.h>
#include <dev.h>
#include <fs.h>
#include <partition.h>
#include <cache.h>
#include <vfs.h>

#define ZEXFS_INITBLOCK_SECTOR			63
#define ZEXFS_DIRENT_SECTOR			64
#define ZEXFS_IDIRENT_SECTOR			ZEXFS_DIRENT_SECTOR+30
#define ZEXFS_BLKENT_SECTOR			1024
#define ZEXFS_IBLKENT_SECTOR			2048

#define ZEXFS_DIRENT_PREALLOC			512	/* you can create # new dirents in fs */
#define ZEXFS_BLKENT_PREALLOC			512	/* you can create # new blkents in fs */

#define ZEXFS_DIRENT_FREE			0x0
#define	ZEXFS_DIRENT_DIR			0x1
#define ZEXFS_DIRENT_FILE			0x2

#define ZEXFS_BLKENT_FREE			0x0
#define ZEXFS_BLKENT_USED			0x1

#define	__PACKED__ __attribute__ ((__packed__))

/* zexos filesystem structures */
typedef struct {
	/* main variables of this ent */
	unsigned char magic[5];			/* Magic ID of zexfs */
	unsigned short dirents;			/* Count of created dirents */
	unsigned short blkents;			/* Count of created blkents */
} zexfs_infoblock;

typedef struct {
	/* main variables of this ent */
	unsigned char flags;			/* Flags for this ent */

	unsigned short parent;			/* Parent node */
	short children;				/* Children node */

	/* time stamp */
	unsigned char tm_sec;			/* Seconds.	[0-60] (1 leap second) */
	unsigned char tm_min;			/* Minutes.	[0-59] */
	unsigned char tm_hour;			/* Hours.	[0-23] */
	unsigned char tm_mday;			/* Day.		[1-31] */
	unsigned char tm_mon;			/* Month.	[0-11] */
	unsigned char tm_year;			/* Year =	2000 + tm_year */

	unsigned char name[21];
} __PACKED__ zexfs_dirent;

typedef struct {
	unsigned short next;
	unsigned short len;
} __PACKED__ zexfs_blk_header;

/* zexos filesystem structure */
typedef struct {
	zexfs_infoblock iblock;

	unsigned char *dirents;
	unsigned char *blkents;

	unsigned char cache[512];

	unsigned short node_curr;
	unsigned short node_prev;
} zexfs_t;

zexfs_t zexfs;

/* clear and pre-set dir[] structure */
void zexfs_dir_flush ()
{
	unsigned i = 0;

//	if (zexfs.node_curr != 0) {
//		printf ("eyk\n");
//	}

	for (; i < 223; i ++)
		memset (dir[i].name, 0, 10);
}

/* read 1024 byte block */
unsigned zexfs_block_read (partition_t *p, unsigned sector, unsigned char *block)
{
	lba28_drive_read (p, sector, (unsigned char *) block);
	lba28_drive_read (p, sector+1, (unsigned char *) block+512);

	return 1;
}

int zexfs_blkent_create (partition_t *p)
{
	unsigned i;
	unsigned y = 0;

	/* find free dirent in array */
	for (i = 0; i < zexfs.iblock.blkents + ZEXFS_BLKENT_PREALLOC; i ++) {
		if (zexfs.blkents[i] == ZEXFS_BLKENT_FREE) {
			/* match dirent as file or directory */
			zexfs.blkents[i] = ZEXFS_BLKENT_USED;

			lba28_drive_write (p, ZEXFS_BLKENT_SECTOR+y, zexfs.blkents+(i/512));

			/* increase dirents count in infoblock structure */
			zexfs.iblock.blkents ++;

			/* update initblock on drive */
			memset (zexfs.cache+sizeof (zexfs.iblock), 0, 512-sizeof (zexfs.iblock));
			memcpy (zexfs.cache, &zexfs.iblock, sizeof (zexfs.iblock));
			lba28_drive_write (p, ZEXFS_INITBLOCK_SECTOR, (unsigned char *) zexfs.cache);

			DPRINT (DBG_DRIVER | DBG_FS, "ZeXFS -> blkent %d:%d created", i, zexfs.iblock.blkents);

			return i;
		}
	}

	kprintf ("ZeXFS -> Sorry, but your filesystem blkent table is full, please reboot or delete some files for correct the problem\n");
}

/* function for write data of file */
unsigned zexfs_write_file (partition_t *p, unsigned char *name, vfs_content_t *content)
{
	unsigned y = 0;
	unsigned x = -1;
	int z = -1;
	unsigned i = 0;
	unsigned s = 0;
	int ret = 0;
	zexfs_dirent *dirent = 0;

	for (i = 0; i < zexfs.iblock.dirents + ZEXFS_DIRENT_PREALLOC; i ++) {
		if (zexfs.dirents[i] != ZEXFS_DIRENT_FREE) {
			s = i * sizeof (zexfs_dirent);

			y = s/512;

			/* read data from sector to cache */
			if (x != y)
				lba28_drive_read (p, ZEXFS_IDIRENT_SECTOR+y, zexfs.cache);
			
			unsigned char *l = zexfs.cache + (s-(y*512));
			dirent = (zexfs_dirent *) l;

			if (zexfs.node_curr == dirent->parent) {
//				kprintf ("cd_root: %d : %s\n", (s-(y*512)), dirent->name);

				if (!strcmp (dirent->name, name)) {
					z = dirent->children;
					ret = 1;
//					kprintf ("z: %d: %d\n", z, y);
					break;
				}
			}

			x = y;
		}
	}

	if (!ret) {
		kprintf ("ZeXFS -> file %s not found\n", name);
		return 0;
	}

	/* is file block already created ? */
	if (z == -1) {
		z = zexfs_blkent_create (p);

		if (z == -1) {
			kprintf ("ZeXFS -> Drive is full\n");
			return 0;
		}

		lba28_drive_read (p, ZEXFS_IDIRENT_SECTOR+y, zexfs.cache);

		dirent->children = z;
		/* in memory just rewrite dirent structure */
		memcpy (zexfs.cache+(s-(y*512)), (void *) dirent, sizeof (zexfs_dirent));

		/* write new data from cache */
		lba28_drive_write (p, ZEXFS_IDIRENT_SECTOR+y, zexfs.cache);
	}

	zexfs_blk_header blk;
	
	unsigned l;
	int z2 = z;
	for (l = 0; l < content->len; l += (512-sizeof (zexfs_blk_header))) {
		blk.next = (content->len > (l + (512-sizeof (zexfs_blk_header)))) ? 1 : 0;

		if (blk.next) {
			blk.next = zexfs_blkent_create (p) - z2;
			blk.len = 512-sizeof (zexfs_blk_header);
		} else
			blk.len = content->len - l;
//kprintf ("next: %d | %d | %d | %d\n", blk.next, ZEXFS_IBLKENT_SECTOR, z, l);

		memcpy (zexfs.cache, &blk, sizeof (zexfs_blk_header));
		memcpy (zexfs.cache+sizeof (zexfs_blk_header), content->ptr+l, 512-sizeof (zexfs_blk_header));

		lba28_drive_write (p, ZEXFS_IBLKENT_SECTOR+z+l, zexfs.cache);
		z2 = z;
	}

	return 1;
}

/* function for read data of file */
unsigned zexfs_read_file (partition_t *p, unsigned char *name, vfs_content_t *content)
{
	unsigned y = 0;
	unsigned x = -1;
	int z = -1;
	unsigned i = 0;
	unsigned s = 0;
	int ret = 0;
	zexfs_dirent *dirent = 0;

	for (i = 0; i < zexfs.iblock.dirents + ZEXFS_DIRENT_PREALLOC; i ++) {
		if (zexfs.dirents[i] != ZEXFS_DIRENT_FREE) {
			s = i * sizeof (zexfs_dirent);

			y = s/512;

			/* read data from sector to cache */
			if (x != y)
				lba28_drive_read (p, ZEXFS_IDIRENT_SECTOR+y, zexfs.cache);
			
			unsigned char *l = zexfs.cache + (s-(y*512));
			dirent = (zexfs_dirent *) l;

			if (zexfs.node_curr == dirent->parent) {
//				kprintf ("cd_root: %d : %s\n", (s-(y*512)), dirent->name);

				if (!strcmp (dirent->name, name)) {
					z = dirent->children;
					ret = 1;
					break;
				}
			}

			x = y;
		}
	}

	if (!ret) {
		kprintf ("ZeXFS -> file %s not found\n", name);
		return 0;
	}

	/* is file block already created ? */
	if (z == -1) {
		kprintf ("ZeXFS -> file %s is empty\n", name);
		return 0;
	}

	zexfs_blk_header *blk = (zexfs_blk_header *) &zexfs.cache;

	cache_t *cache = cache_create (0, 0, 0);

	if (!cache)
		return 0;
	
	unsigned l;

	for (l = 0; ; l += (512-sizeof (zexfs_blk_header))) {
		lba28_drive_read (p, ZEXFS_IBLKENT_SECTOR+z+l, zexfs.cache);

		z += blk->next;
	
		cache_add (zexfs.cache+sizeof (zexfs_blk_header), blk->len);

		if (!blk->next)
			break;
	}

	content->ptr = (char *) &cache->data;
	content->len = cache->limit;

	return 1;
}


/* NOTE: I dont want to use realloc, so we use preallocated memory block for new dirents */
int zexfs_dirent_create (partition_t *p, zexfs_dirent *dirent)
{
	unsigned i;
	unsigned y = 0;
	
	/* find free dirent in array */
	for (i = 0; i < zexfs.iblock.dirents + ZEXFS_DIRENT_PREALLOC; i ++) {
		if (zexfs.dirents[i] == ZEXFS_DIRENT_FREE) {
			/* match dirent as file or directory */
			zexfs.dirents[i] = dirent->flags & ZEXFS_DIRENT_FILE ? ZEXFS_DIRENT_FILE : ZEXFS_DIRENT_DIR;

			lba28_drive_write (p, ZEXFS_DIRENT_SECTOR+y, zexfs.dirents+(i/512));

			/* increase dirents count in infoblock structure */
			zexfs.iblock.dirents ++;

			/* update initblock on drive */
			memset (zexfs.cache+sizeof (zexfs.iblock), 0, 512-sizeof (zexfs.iblock));
			memcpy (zexfs.cache, &zexfs.iblock, sizeof (zexfs.iblock));
			lba28_drive_write (p, ZEXFS_INITBLOCK_SECTOR, (unsigned char *) zexfs.cache);

			DPRINT (DBG_DRIVER | DBG_FS, "ZeXFS -> dirent %d:%d created", i, zexfs.iblock.dirents);

			return i;
		}
	}

	kprintf ("ZeXFS -> Sorry, but your filesystem dirent table is full, please reboot or delete some files for correct the problem\n");

	return -1;
}

int zexfs_dirent_find (partition_t *p, zexfs_dirent *mydirent, unsigned char *name)
{
	unsigned i;
	unsigned y = 0;
	unsigned x = -1;
	
	/* find free dirent in array */
	for (i = 0; i < zexfs.iblock.dirents + ZEXFS_DIRENT_PREALLOC; i ++) {
		if (zexfs.dirents[i] & ZEXFS_DIRENT_DIR) {
			unsigned s = i * sizeof (zexfs_dirent);

			y = s/512;

			/* read data from sector to cache */
			if (x != y)
				lba28_drive_read (p, ZEXFS_IDIRENT_SECTOR+y, zexfs.cache);
			
			unsigned char *l = zexfs.cache + (s-(y*512));
			zexfs_dirent *dirent = (zexfs_dirent *) l;

			/* we need find same parent node as current node */
			if (dirent->parent == zexfs.node_curr)
				if (!strcmp (name, dirent->name)) {	/* verify name of dirent with searched name */
					/* copy structure from founded dirent to our dirent structure */
					memcpy (&mydirent, &dirent, sizeof (zexfs_dirent));  
					return i;
				}

			x = y;
		}
	}

	return -1;
}

int zexfs_dirent_findbynode (partition_t *p, zexfs_dirent *mydirent, unsigned short node)
{
	unsigned i = node;
	unsigned y = 0;
	
	if (i > zexfs.iblock.dirents + ZEXFS_DIRENT_PREALLOC)
		return -1;

	if (zexfs.dirents[i] == ZEXFS_DIRENT_FREE)
		return -1;

	unsigned s = i * sizeof (zexfs_dirent);

	y = s/512;

	/* read data from sector to cache */
	lba28_drive_read (p, ZEXFS_IDIRENT_SECTOR+y, zexfs.cache);
	//kprintf ("r: %d\n", s-(y*512));
	unsigned char *l = zexfs.cache + (s-(y*512));
	zexfs_dirent *dirent = (zexfs_dirent *) l;

	/* copy structure from founded dirent to our dirent structure */
	memcpy (mydirent, dirent, sizeof (zexfs_dirent));

	return 0;
}

/* function for create blank file or directory */
unsigned zexfs_create_entity (partition_t *p, unsigned char *name, unsigned flags)
{
	zexfs_dirent dirent;

	memset (&dirent, 0, sizeof (zexfs_dirent));

	dirent.flags |= flags;
	dirent.parent = zexfs.node_curr;

	if (flags & ZEXFS_DIRENT_FILE)
		dirent.children = -1;

	unsigned l = strlen (name);
	memcpy (dirent.name, name, l);
	dirent.name[l] = '\0';

	int r = zexfs_dirent_create (p, &dirent);

	if (r == -1)
		return 0;

	r *= sizeof (zexfs_dirent);

	unsigned x = r/512;	/* sizeof (zexfs_dirent) must be dividable by sector size */

//	kprintf ("he: %d .. %d .. %d\n", x, r, r-(x*512));

	unsigned i;
	for (i = 0; i < 223; i ++)
		if (!dir[i].name[0])
			break;

	strcpy (dir[i].name, name);

	/* first read old sector data to cache */
	lba28_drive_read (p, ZEXFS_IDIRENT_SECTOR+x, zexfs.cache);

	/* in memory just rewrite dirent structure */
	memcpy (zexfs.cache+(r-(x*512)), &dirent, sizeof (zexfs_dirent));

	/* write new data from cache */
	lba28_drive_write (p, ZEXFS_IDIRENT_SECTOR+x, zexfs.cache);

	return 1;
}

/* go to next directory placed in current directory */
unsigned zexfs_cd (partition_t *p, unsigned char *name)
{
	zexfs_dirent dirent;

	int r = -1;
//	kprintf ("name: %s : %d\n", name, strlen (name));
	if (!strcmp (name, "..")) {
		zexfs.node_curr = zexfs.node_prev;
		
		r = zexfs_dirent_findbynode (p, &dirent, zexfs.node_prev);
//		kprintf ("he: %d\n", r);
		if (r == -1) {
			kprintf ("ZeXFS -> dirent with this name not found\n");
			return 0;
		}

		zexfs.node_prev = dirent.parent;
		r = zexfs.node_prev;
//		kprintf ("zexfs.node_prev: %d\n", zexfs.node_prev);
	} else {
		r = zexfs_dirent_find (p, &dirent, name);
//		kprintf ("he2: %d\n", r);
		if (r == -1) {
			kprintf ("ZeXFS -> dirent with this name not found\n");
			return 0;
		}

		zexfs.node_prev = zexfs.node_curr;
		zexfs.node_curr = r;
	}

//	kprintf ("node_curr: %d\n", r);
//	kprintf ("name: %s\n", dirent.name);

	zexfs_dir_flush ();

	unsigned y = 0;
	unsigned x = -1;
	unsigned id = 0;
	unsigned i = 0;

	if (zexfs.node_curr) {
		strcpy (dir[0].name, "..");
		id ++;
	}

	for (i = 0; i < zexfs.iblock.dirents + ZEXFS_DIRENT_PREALLOC; i ++) {
		if (zexfs.dirents[i] != ZEXFS_DIRENT_FREE) {
			unsigned s = i * sizeof (zexfs_dirent);

			y = s/512;

			/* read data from sector to cache */
			if (x != y)
				lba28_drive_read (p, ZEXFS_IDIRENT_SECTOR+y, zexfs.cache);
			
			unsigned char *l = zexfs.cache + (s-(y*512));
			zexfs_dirent *dirent = (zexfs_dirent *) l;

			if (zexfs.node_curr == dirent->parent) {
//				kprintf ("cd_root: %d : %s\n", (s-(y*512)), dirent->name);

				memcpy (dir[id].name, dirent->name, 10);
				dir[id].dir = dirent->flags & ZEXFS_DIRENT_DIR ? 1 : 0; 

				id ++;
			}

			x = y;
		}
	}

	return 1;
}

/* go to root '/' directory on partition */
unsigned zexfs_cd_root (partition_t *p)
{
	zexfs.node_curr = 0;

	zexfs_dir_flush ();

	unsigned i;
	unsigned y = 0;
	unsigned x = -1;
	unsigned id = 0;
	
	for (i = 0; i < zexfs.iblock.dirents + 32; i ++) {
		if (zexfs.dirents[i] != ZEXFS_DIRENT_FREE) {
			unsigned s = i * sizeof (zexfs_dirent);

			y = s/512;

			/* read data from sector to cache */
			if (x != y)
				lba28_drive_read (p, ZEXFS_IDIRENT_SECTOR+y, zexfs.cache);
			
			unsigned char *l = zexfs.cache + (s-(y*512));
			zexfs_dirent *dirent = (zexfs_dirent *) l;

			if (zexfs.node_curr == dirent->parent) {
				kprintf ("cd_root: %d : %s\n", (s-(y*512)), dirent->name);

				memcpy (dir[id].name, dirent->name, 10);
				dir[id].dir = dirent->flags & ZEXFS_DIRENT_DIR ? 1 : 0; 

				id ++;
			}

			x = y;
		}
	}

	return 1;
}

/* mkzexfs utility for create zexfs filesystem on partition */
unsigned mkzexfs (partition_t *p)
{
	if (!p)
		return 0;

	printf ("mkzexfs\n");

	unsigned i;
	for (i = 0; i < zexfs.iblock.dirents + ZEXFS_DIRENT_PREALLOC; i ++)
		zexfs.dirents[i] = ZEXFS_DIRENT_FREE;

	memcpy (zexfs.iblock.magic, "zexfs", 5);

	zexfs.iblock.dirents = 0;
	zexfs.iblock.blkents = 0;

	memset (zexfs.cache, 0, 512);
	memcpy (zexfs.cache, (void *) &zexfs.iblock, sizeof (zexfs_infoblock));

	if (!lba28_drive_write (p, ZEXFS_INITBLOCK_SECTOR, zexfs.cache))
		return 0;

	memset (zexfs.cache, 0, 512);

	for (i = ZEXFS_DIRENT_SECTOR; i < ZEXFS_IDIRENT_SECTOR; i ++) {
		if (!lba28_drive_write (p, i, zexfs.cache))
			return 0;
	}

	for (i = ZEXFS_IDIRENT_SECTOR; i < ZEXFS_IDIRENT_SECTOR+10; i ++) {
		if (!lba28_drive_write (p, ZEXFS_IDIRENT_SECTOR, zexfs.cache))
			return 0;
	}

	if (!lba28_drive_write (p, ZEXFS_BLKENT_SECTOR, zexfs.cache))
		return 0; 

	zexfs.node_curr = 0;
	zexfs.node_prev = 0;

	/* create dirent with node id 0 - root directory */
	zexfs_create_entity (p, ".", ZEXFS_DIRENT_FILE);

	printf ("zexfs filesystem was succefully created\n");

	return 1;
}

/* zexos filesystem initialization function */
unsigned zexfs_init (partition_t *p)
{
	lba28_drive_read (p, ZEXFS_INITBLOCK_SECTOR, (unsigned char *) zexfs.cache);

	memcpy (&zexfs.iblock, zexfs.cache, sizeof (zexfs.iblock));

	if (strncmp (zexfs.iblock.magic, "zexfs", 5)) {
		kprintf ("ZeXFS not found\n");
		return 1;
	}

	kprintf ("ZeXFS -> filesystem contain %d dirents\n", zexfs.iblock.dirents);
	kprintf ("ZeXFS -> filesystem contain %d blkents\n", zexfs.iblock.blkents);

	/* alloc memory for dirents array */
	zexfs.dirents = (unsigned char *) kmalloc ((zexfs.iblock.dirents+ZEXFS_DIRENT_PREALLOC) * sizeof (unsigned char));

	if (!zexfs.dirents) {
		kprintf ("ZeXFS -> zexfs.dirents : out of memory\n");

		return 0;
	}

	/* alloc memory for blkents array */
	zexfs.blkents = (unsigned char *) kmalloc ((zexfs.iblock.blkents+ZEXFS_BLKENT_PREALLOC) * sizeof (unsigned char));

	if (!zexfs.blkents) {
		kprintf ("ZeXFS -> zexfs.blkents : out of memory\n");
		kfree (zexfs.dirents);

		return 0;
	}

	/* clean end of memory block, where physical data are'nt loaded */
	if (zexfs.iblock.dirents >= ZEXFS_DIRENT_PREALLOC)
		memset (zexfs.dirents-ZEXFS_DIRENT_PREALLOC, 0, ZEXFS_DIRENT_PREALLOC);
	else
		memset (zexfs.dirents, 0, ZEXFS_DIRENT_PREALLOC);

	/* clean end of memory block, where physical data are'nt loaded */
	if (zexfs.iblock.blkents >= ZEXFS_BLKENT_PREALLOC)
		memset (zexfs.blkents-ZEXFS_BLKENT_PREALLOC, 0, ZEXFS_BLKENT_PREALLOC);
	else
		memset (zexfs.blkents, 0, ZEXFS_BLKENT_PREALLOC);

	unsigned i;
	unsigned y = 0;

	/* read dirent database from drive */
	for (i = 0; i < zexfs.iblock.dirents; i += 512)
		lba28_drive_read (p, ZEXFS_DIRENT_SECTOR+(i/512), (unsigned char *) ((unsigned char *) zexfs.dirents+((i/512)*512))); 

	/* read blkent database from drive */
	for (i = 0; i < zexfs.iblock.blkents; i += 512)
		lba28_drive_read (p, ZEXFS_BLKENT_SECTOR+(i/512), (unsigned char *) ((unsigned char *) zexfs.blkents+((i/512)*512))); 

	zexfs.node_curr = 0;

	return 1;
}

/* handler which is used by device management and pretty needed
	 for communication between zexos and this filesystem */
bool zexfs_handler (unsigned act, char *block, unsigned n, unsigned long l)
{
	switch (act) {
		case FS_ACT_INIT:
		{
			return zexfs_init ((partition_t *) block);
		}
		break;
		case FS_ACT_READ:
		{
			return zexfs_read_file (curr_part, dir[n].name, (vfs_content_t *) block);
		}
		break;
		case FS_ACT_WRITE:
		{
			return zexfs_write_file (curr_part, dir[n].name, (vfs_content_t *) block);
		}
		break;
		case FS_ACT_CHDIR:
		{
			if (n == -1)
				return zexfs_cd_root (curr_part);

			/* go to another directory */
			return zexfs_cd (curr_part, block);
		}
		break;
		case FS_ACT_MKDIR:
		{
			return zexfs_create_entity (curr_part, block, ZEXFS_DIRENT_DIR);
		}
		break;
		case FS_ACT_TOUCH:
		{
			return zexfs_create_entity (curr_part, block, ZEXFS_DIRENT_FILE);
		}
	}

	return 0;
}
#endif
