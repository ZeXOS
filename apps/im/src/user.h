/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _USER_H
#define _USER_H

typedef struct user_context {
	struct user_context *next, *prev;

	char *id;
	unsigned char len;
} user_t;

extern int user_list_get (char *data, unsigned len);
extern int user_msgincoming (char *str, unsigned len);
extern int user_statusincoming (char *str, unsigned len);
extern int user_msgparse (char *str, unsigned len);
extern int user_error (char *str, unsigned len);
extern int user_info (char *str, unsigned len);
extern int init_user ();

#endif
