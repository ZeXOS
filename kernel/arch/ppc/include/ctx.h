/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ARM_CTX_H
#define	_ARM_CTX_H

#include <build.h>

#ifdef ARCH_arm

#define ARM_STACKALIGN(a, b) ((a) & ~((b)-1))

/* arm exception iframe */
struct arm_ex_iframe {
	unsigned *spsr;
	unsigned *r0;
	unsigned *r1;
	unsigned *r2;
	unsigned *r3;
	unsigned *r12;
	unsigned *lr;
	unsigned *pc;
};

struct arm_fault_frame {
	unsigned *spsr;
	unsigned *usp;
	unsigned *ulr;
	unsigned r[13];
	unsigned *pc;
};

/* arm context structure for task switch */
struct arm_ctx_frame {
	unsigned *r4;
	unsigned *r5;
	unsigned *r6;
	unsigned *r7;
	unsigned *r8;
	unsigned *r9;
	unsigned *r10;
	unsigned *r11;
	unsigned *lr;
	unsigned *usp;
	unsigned *ulr;
};

extern void arm_task_switch (unsigned **old_sp, unsigned *new_sp);

typedef unsigned (task_handler_t) ();

#endif

#endif

