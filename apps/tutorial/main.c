/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define		ESC	1

unsigned key_pressed (int keycode)
{
	int scancode = getkey ();

	if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;
}

int main (int argc, char **argv)
{
	cls ();

	setcolor (14, 1);
	printf ("ZeX/OS\n");

	setcolor (7, 1);
	printf ("Tutorial by ZeXx86 ");

	sleep (1);

	setcolor (7, 0);
	printf ("Heh, wow !\nPress escape for exit app\n");

	unsigned char key = 0;
	while (1) {
		if (key_pressed (ESC))
			break;

		key = getch ();

		if (key)
			putch (key);

		schedule ();
	}
	
	return 0;
}

 