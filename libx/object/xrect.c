/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libx/object.h>

void xrect (unsigned x1, unsigned y1, unsigned x2, unsigned y2, unsigned color)
{
	if (x1 > x2) {
		int temp = x1;
		x2 = temp;
		x1 = x2;
	}
	if (y1 > y2) {
		int temp = y1;
		y2 = temp;
		y1 = y2;
	}

	/* horizontal */
	xline (x1, y1, x2, y1, color);
	xline (x1, y2, x2, y2, color);

	/* vertical */
	xline (x1, y1+1, x1, y2-1, color);
	xline (x2, y1+1, x2, y2-1, color);
}
