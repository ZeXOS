/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <time.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "commands.h"
#include "config.h"
#include "net.h"

char *cmd_cache;

static unsigned len;

int cstrcmp (char *one, char *two)
{
	int i;

	for(i = 0; (unsigned) i < strlen (one); i ++) {
		if (one[i] != two[i])
			return -1;
	}

	if ((two[i] == ' ' || two[i] == '\0') || two[i] == '\n')
		return 0;

	return -2;
}

static int getstring (char *str)
{
	if (len > COMMANDS_CACHE_SIZE-1) {
		setcolor (4, 0);
		printf ("\nERROR -> commands cache buffer is full ! Clearing cache ..\n");
		setcolor (15, 0);

		len = 0;
		return 0;
	}

	char c = getchar ();

	if (c != -1) {
		if (c == '\n') {
			unsigned l = len;
			len = 0;

			return l;
		}
		if (c == '\b') {
			if (len > 0)
				len --;

			return 0;
		}

		str[len] = c;
		len ++;
	}

	return 0;
}

/* get input from keyboard */
int commands_get ()
{
	int l = getstring (cmd_cache);
	
	if (l > 0) {
		cmd_cache[l] = '\0';

		return commands_handler (cmd_cache, l);
	}

	return 1;
}

int commands_handler (char *buffer, unsigned len)
{
	/* this is client command */
	if (buffer[0] == '/') {
		if (!cstrcmp ("join", buffer+1)) {		/* join to channel ... ? */
			config.channel_len = strlen (buffer+6);

			if (!config.channel_len) {
				printf ("CLIENT -> Please specify channel !\n");
				return 1;
			}

			/* too long channel name */
			if (config.channel_len >= 64)
				return 1;

			config.channel = strdup (buffer+6);

			char str[64];
			sprintf (str, "JOIN %s\n", config.channel);

			return net_send (str, 6+config.channel_len);
		} else if (!cstrcmp ("me", buffer+1)) {		/* send ACTION message */
			/* we want send chat message to server */
			char *msg = (char *) malloc (sizeof (char) * (20+config.channel_len+len));

			if (!msg)
				return 0;

			memcpy (msg, "PRIVMSG ", 8);
			memcpy (msg+8, config.channel, config.channel_len);
			memcpy (msg+8+config.channel_len, " :\1ACTION ", 10);
			memcpy (msg+18+config.channel_len, buffer+4, len-4);
			msg[14+config.channel_len+len] = 1;
			msg[15+config.channel_len+len] = '\n';

			net_send (msg, 16+config.channel_len+len);

			setcolor (6, 0);
			printf ("[***%s] %s\n", config.nick, buffer+4);
			setcolor (15, 0);

			free (msg);

			return 1;
		} else if (!cstrcmp ("s", buffer+1)) {	/* change channel's window */
			if (!config.channel) {
				printf ("ERROR -> Please type /join #yourchannel first !\n");
				return 1;
			}
			
			free (config.channel);

			config.channel_len = strlen (buffer+3);

			if (!config.channel_len) {
				printf ("ERROR -> Please specify channel !\n");
				return 1;
			}
			
			config.channel = strdup (buffer+3);

			printf ("CLIENT -> Your window is switched to channel %s\n", config.channel);
			return 1;
		} else if (!cstrcmp ("nick", buffer+1)) {	/* change nick name */
			
			unsigned nick_len = strlen (buffer+3);

			if (!nick_len) {
				printf ("ERROR -> Please specify nick name !\n");
				return 1;
			}
			
			if (config.nick)
				free (config.nick);

			config.nick = strdup (buffer+6);

			char str[64];
			sprintf (str, "NICK %s\n", config.nick);

			printf ("CLIENT -> Your nick name was changed to %s\n", config.nick);

			return net_send (str, 6+nick_len);
		} else if (!cstrcmp ("os", buffer+1)) {		/* send ACTION message */
			len = 73;
			/* we want send chat message to server */
			char *msg = (char *) malloc (sizeof (char) * (21+config.channel_len+len));

			if (!msg)
				return 0;

			memcpy (buffer, "is running on ZeX/OS - operating system created by ZeXx86 - www.zexos.org", 73);

			memcpy (msg, "PRIVMSG ", 8);
			memcpy (msg+8, config.channel, config.channel_len);
			memcpy (msg+8+config.channel_len, " : ACTION ", 10);
			memcpy (msg+18+config.channel_len, buffer, len);
			msg[10+config.channel_len] = 1;
			msg[18+config.channel_len+len] = 1;
			msg[19+config.channel_len+len] = '\n';
			msg[20+config.channel_len+len] = '\0';

			net_send (msg, 20+config.channel_len+len);

			setcolor (6, 0);
			printf ("[***%s] %s\n", config.nick, buffer);
			setcolor (15, 0);

			free (msg);

			return 1;
		} else if (!cstrcmp ("help", buffer+1)) {	/* exit client */
			printf ("Command\t\t\tDescription\n"
				"/join <channel>\t\tJoin to specified channel\n"
				"/me <message>\t\tSpeak with 3rd person\n"
				"/s <channel>\t\tSwitch channel where you talk\n"
				"/nick <nick>\t\tChange nick name\n"
				"/quit\t\t\tQuit a client\n");
			return 1;
		} else if (!cstrcmp ("quit", buffer+1)) {	/* exit client */
			return 0;
		}

		printf ("CLIENT -> %s : Unknown command\n", buffer+1);
		return 1;
	}

	/* we want send chat message to server */
	char *msg = (char *) malloc (sizeof (char) * (12+config.channel_len+len));

	if (!msg)
		return 0;

	memcpy (msg, "PRIVMSG ", 8);
	memcpy (msg+8, config.channel, config.channel_len);
	memcpy (msg+8+config.channel_len, " :", 2);
	memcpy (msg+10+config.channel_len, buffer, len);
	msg[10+config.channel_len+len] = '\n';

	net_send (msg, 11+config.channel_len+len);

	setcolor (7, 0);
	printf ("[%s]: %s\n", config.nick, buffer);
	setcolor (15, 0);

	free (msg);

	return 1;
}

int commands_init ()
{
	cmd_cache = (char *) malloc (sizeof (char) * COMMANDS_CACHE_SIZE);

	if (!cmd_cache)
		return 0;

	/* set socket "sock" to non-blocking */
	int oldFlag = fcntl (1, F_GETFL, 0);
	if (fcntl (1, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	len = 0;

	return 1;
}
