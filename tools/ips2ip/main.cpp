/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <iostream>
#include <string>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <errno.h>

extern int errno;


#define BUFSIZE 10240

/* baudrate settings are defined in <asm/termbits.h>, which is
included by <termios.h> */
#define BAUDRATE B9600
/* change this definition for the correct port */
#define MODEMDEVICE "/dev/ttyS0"
#define _POSIX_SOURCE 1 /* POSIX compliant source */

#define IPS_PROTO_CONNECT 	'c'
#define IPS_PROTO_CONNECTED 	'k'
#define IPS_PROTO_RECV		'r'
#define IPS_PROTO_EOF 		'e'
#define IPS_PROTO_CLOSE		'q'
#define IPS_PROTO_SOCKET	's'
#define IPS_PROTO_ACCEPT	'a'
#define IPS_PROTO_BIND		'b'
#define IPS_PROTO_LISTEN	'l'
#define IPS_PROTO_FCNTL		'f'

#define IPS_PROTO		"/#ips#"

using namespace std;

int serial_write (char *data, unsigned len);

int fd, c, res;
struct termios oldtio, newtio;

bool connected = false;
bool nonblock = true;

hostent *host;              // Vzd�len� po��ta�;
sockaddr_in serverSock;     // Vzd�len� "konec potrub�"
int mySocket;               // Soket
int port;                   // ��slo portu
char buf[BUFSIZE];          // P�ij�mac� buffer
int size;                   // Po�et p�ijat�ch a odeslan�ch byt�
fd_set set;
timeval tv;

struct sockaddr_un serv_addr;

sockaddr_in sockName;         // "Jm�no" portu
sockaddr_in clientInfo;       // Klient, kter� se p�ipojil 
socklen_t addrlen;            // Velikost adresy vzd�len�ho po��ta�e

int unixsock_init (char *path)
{
	int servlen;

	bzero ((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sun_family = AF_UNIX;
	strcpy (serv_addr.sun_path, path);
	servlen = strlen (serv_addr.sun_path) + sizeof (serv_addr.sun_family);

	if ((fd = socket (AF_UNIX, SOCK_STREAM, 0)) < 0) {
		puts ("socket () < 0 - unix domain socket cant be created !");
		exit (0);
		return 0;
	}

	if (connect (fd, (struct sockaddr *) &serv_addr, servlen) < 0) {
		printf ("connect () < 0 - cant connect to socket '%s'\n", path);
		exit (-1);
		return -1;
	}

	printf ("unix domain socket - connected :P\n");

	return 1;
}

int serial_init ()
{
	/*
		Open modem device for reading and writing and not as controlling tty
		because we don't want to get killed if linenoise sends CTRL-C.
	*/
	fd = open (MODEMDEVICE, O_RDWR | O_NOCTTY);

	if (fd < 0 )
		perror(MODEMDEVICE); exit(-1); 
	
	tcgetattr (fd, &oldtio); /* save current serial port settings */
	bzero (&newtio, sizeof(newtio)); /* clear struct for new port settings */
	
	/*
		BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
		CRTSCTS : output hardware flow control (only used if the cable has
			all necessary lines. See sect. 7 of Serial-HOWTO)
		CS8     : 8n1 (8bit,no parity,1 stopbit)
		CLOCAL  : local connection, no modem contol
		CREAD   : enable receiving characters
	*/
	
	newtio.c_cflag = BAUDRATE | CS8 | /*CLOCAL | */CREAD;
	
	/*
		IGNPAR  : ignore bytes with parity errors
		ICRNL   : map CR to NL (otherwise a CR input on the other computer
			will not terminate input)
		otherwise make device raw (no other input processing)
	*/
	// newtio.c_iflag = /*IGNPAR | ICRNL;*/
	
	/*
		Raw output.
	*/
	newtio.c_oflag = 0;
	
	/*
		ICANON  : enable canonical input
		disable all echo functionality, and don't send signals to calling program
	*/
	// newtio.c_lflag = ICANON;
	
	/*
		initialize all control characters
		default values can be found in /usr/include/termios.h, and are given
		in the comments, but we don't need them here
	*/
	newtio.c_cc[VINTR]    = 0;     /* Ctrl-c */
	newtio.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
	newtio.c_cc[VERASE]   = 0;     /* del */
	newtio.c_cc[VKILL]    = 0;     /* @ */
	newtio.c_cc[VEOF]     = 4;     /* Ctrl-d */
	newtio.c_cc[VTIME]    = 0;     /* inter-character timer unused */
	newtio.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
	newtio.c_cc[VSWTC]    = 0;     /* '\0' */
	newtio.c_cc[VSTART]   = 0;     /* Ctrl-q */
	newtio.c_cc[VSTOP]    = 0;     /* Ctrl-s */
	newtio.c_cc[VSUSP]    = 0;     /* Ctrl-z */
	newtio.c_cc[VEOL]     = 0;     /* '\0' */
	newtio.c_cc[VREPRINT] = 0;     /* Ctrl-r */
	newtio.c_cc[VDISCARD] = 0;     /* Ctrl-u */
	newtio.c_cc[VWERASE]  = 0;     /* Ctrl-w */
	newtio.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
	newtio.c_cc[VEOL2]    = 0;     /* '\0' */
	
	/*
		now clean the modem line and activate the settings for the port
	*/

	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW,&newtio);

	return 1;
}

void irs_proto_sendheader (int ret)
{
	char num[4];
	num[0] = (unsigned char) ret;
	num[1] = (unsigned char) (ret >> 8);
	num[2] = (unsigned char) (ret >> 16);
	num[3] = (unsigned char) (ret >> 24);
	
	serial_write (num, 4);
}

int ips_proto_parse (int sock, char *data, unsigned len)
{
	if (strncmp (data, IPS_PROTO, 6)) {
		// send
		if (!len)
			return -1;

		if (!data)
			return 0;

		// Odesl�n� dat
		int ret = send (sock, data, len, 0);

		printf ("send (%d/%d): %s\n", ret, len, data);

		return 1;
	}
 //	printf ("dta: %s\n", data);
	if (data[6] == IPS_PROTO_RECV) {
		// recv
		if (!len)
			return -1;

		char s[10];
		int buf_len = 0;

		sscanf (data, "%s %d", s, &buf_len);

		if (buf_len <= 0) {
			printf ("recv (): buf_len ! %d\n", buf_len);
			irs_proto_sendheader (-1);
			return -1;
		}

		char buf[buf_len+1];
		memset (buf, 0, buf_len);

		usleep (80);	// prevent for fragmentation

		int ret = recv (sock, buf, buf_len, 0);

		// printf ("len: %d\n", len);
		//if (ret == -1) //to vymaz pak -- test
		//	irs_proto_sendheader (0);
		//else

		irs_proto_sendheader (ret);

		if (ret > 0) {
			serial_write (buf, ret);
			buf[ret] = '\0';
			printf ("recv(%d): %d: %s\n", sock, ret, buf);
		}

		return 1;
	}

	if (data[6] == IPS_PROTO_ACCEPT) {
		// Pozna��m si velikost struktury clientInfo.
		// P�ed�m to funkci accept. 
		addrlen = sizeof (clientInfo);
		// Vyberu z fronty po�adavek na spojen�.
		// "client" je nov� soket spojuj�c� klienta se serverem.
		int client = accept (sock, (sockaddr *) &clientInfo, &addrlen);

		irs_proto_sendheader (client);
		// TODO: dodelat vraceni clientInfo structury .. nebo spis jen ip ktera se pripojila
		/*if (client) {
			
		}*/
// 		printf ("accept (%d)\n", client);

		return 1;
	}

	if (data[6] == IPS_PROTO_SOCKET) {
		// Vytvo��me soket
		if ((sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
		{
			cerr << "Nelze vytvo�it soket" << endl;
			irs_proto_sendheader (sock);
			return -1;
		}

		printf ("socket (%d)\n", sock);

		irs_proto_sendheader (sock);
	}

	if (data[6] == IPS_PROTO_CONNECT) {
		// connect
		if (!len) {
			irs_proto_sendheader (-1);
			return 0;
		}

		char s[10], addr[32];

		sscanf (data, "%s %s %d", s, addr, &port);

		// Zjist�me info o vzd�len�m po��ta�i
		if ((host = gethostbyname (addr)) == NULL)
		{
			printf ("connect () -> wrong address: %s\n", addr);
			irs_proto_sendheader (-1);
			return -1;
		}

		// Zapln�me strukturu sockaddr_in
		// 1) Rodina protokol�
		serverSock.sin_family = AF_INET;
		// 2) ��slo portu, ke kter�mu se p�ipoj�me
		serverSock.sin_port = htons(port);
		// 3) Nastaven� IP adresy, ke kter� se p�ipoj�me
		memcpy(&(serverSock.sin_addr), host->h_addr, host->h_length);
		// P�ipojen� soketu

		int ret = 0;

		if ((ret = connect(sock, (sockaddr *)&serverSock, sizeof(serverSock))) == -1)
		{
			if (errno != EINPROGRESS)
			{
				printf ("ERROR -> Cant resolve - %s:%d\n", addr, port);
				irs_proto_sendheader (ret);
				return -1;
			}
		}

		if (nonblock) {
			FD_ZERO(&set);
			FD_SET(sock, &set);
			tv.tv_sec = 10;
			tv.tv_usec = 0;
			// Zjist�m, jestli je ji� spojen� nav�z�no. Jestli�e ne, po�k�me a� bude nav�z�no.
			if (select (sock + 1, NULL, &set, NULL, &tv) == 0)
			{
				printf ("ERROR -> Cant connect to %s:%d\n", addr, port);
				irs_proto_sendheader (-1);
				return -1;
			}
		}

		connected = true;

		//sleep (1);
		irs_proto_sendheader (1);

		printf ("Connected -> %s:%d\n", addr, port);

		return 1;
	}

	if (data[6] == IPS_PROTO_BIND) {

		unsigned yes = 1;
		setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, (char *) &yes, sizeof (yes));

		char s[10], addr[32];

		sscanf (data, "%s %s %d", s, addr, &port);
		// Zapln�me strukturu sockaddr_in
		// 1) Rodina protokol�
		sockName.sin_family = AF_INET;
		// 2) ��slo portu, na kter�m �ek�me
		sockName.sin_port = htons (port);
		// 3) Nastaven� IP adresy lok�ln� s��ov� karty, p�es kterou je mo�no se
		//    p�ipojit. Nastav�me mo�nost p�ipojit se odkudkoliv. 
		sockName.sin_addr.s_addr = INADDR_ANY;
		// p�i�ad�me soketu jm�no

		int ret = 0;
		if ((ret = bind (sock, (sockaddr *)&sockName, sizeof(sockName))) == -1)
		{
			cerr << "Probl�m s pojmenov�n�m soketu." << endl;
			irs_proto_sendheader (-1);
			return -1;
		}

		printf ("bind () on port %d\n", port);

		irs_proto_sendheader (ret);

		return 1;
	}

	if (data[6] == IPS_PROTO_LISTEN) {
		if (!len) {
			irs_proto_sendheader (-1);
			return -1;
		}

		char s[10];
		int backlog = 0;

		sscanf (data, "%s %d", s, &backlog);
		
		int ret = listen (sock, backlog);

		irs_proto_sendheader (ret);

		return 1;
	}

	if (data[6] == IPS_PROTO_CLOSE) {
		close (sock);
		
		//printf ("close (%d)\n", sock);
		return 1;
	}

	if (data[6] == IPS_PROTO_FCNTL) {
		if (!len) {
			irs_proto_sendheader (-1);
			return 0;
		}

		char s[10];
		int act = 0;
		long flag = 0;

		sscanf (data, "%s %d %ld", s, &act, &flag);

		/* Set to nonblocking socket mode */
		int ret = fcntl (sock, act, flag);
		/*if (fcntl (sock, F_SETFL, oldFlag | O_NONBLOCK) == -1)
		{
			cerr << "> ERROR -> Problem with set socket mode" << endl;
			irs_proto_sendheader (-1);
			return -1;
		}*/

		//if (ret != -1)
		//	printf ("socket is nonblock (%d)\n", sock);

		irs_proto_sendheader (ret);

		return 1;
	}

	return 0;
}

int serial_read (char *data)
{
	char buf[2];

	int len = 0;

	unsigned char num[5];
	memset (num, 0, sizeof (num));

	read (fd, (unsigned char *) &num[0], 1);
	read (fd, (unsigned char *) &num[1], 1);
	read (fd, (unsigned char *) &num[2], 1);
	read (fd, (unsigned char *) &num[3], 1);

	if (num[0] == '\0')
		return 0;

	len = num[0] | (num[1] << 8) | (num[2] << 16) | (num[3] << 24);

//	printf ("##len: %d\n", len);

	if (len >= 4096) {
		printf ("oj ! len: %d\n", len);
		return -1;
	}

	unsigned char fds[5];
	memset (fds, 0, sizeof (fds));

	read (fd, (unsigned char *) &fds[0], 1);
	read (fd, (unsigned char *) &fds[1], 1);
	read (fd, (unsigned char *) &fds[2], 1);
	read (fd, (unsigned char *) &fds[3], 1);

	//if (fds[0] == '\0')
	//	return 0;

	int sock = fds[0] | (fds[1] << 8) | (fds[2] << 16) | (fds[3] << 24);

//	printf ("##sock: %d\n", sock);

/*	if (len < 0)
		return len;*/

	int x = 0;

	while (x < len) {
		read (fd, buf, 1);

		data[x] = buf[0];

		x ++;
	}

//	puts ("##huh");

	data[len] = '\0';

//	puts ("##lol");

	return ips_proto_parse (sock, data, len);
}

int serial_write (char *data, unsigned len)
{
	write (fd, data, len);

	return 1;
}

int main (int argc, char *argv[])
{
	puts ("-[ ZeX/OS ips2ip gateway ]-");

	int mode = 1;

	if (argc > 1 && argv[1][0] == 'p')
		mode = 0;

	if (mode)
		serial_init ();
	else
		if (argc > 2)
			unixsock_init (argv[2]);
		else
			unixsock_init ("/tmp/zexos");

	char *test = (char *) calloc (sizeof (char), 4096);

	while (1) {
		memset (test, 0, 4095);

		serial_read (test);

		usleep (1);
	}
	
	free (test);

	/* restore the old port settings */
	if (mode)
   		tcsetattr (fd, TCSANOW, &oldtio);

	puts ("ips2ip -> exit ()");

	return 1;
}
