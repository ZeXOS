/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ARM_BD_INTCP
#define ARM_BD_INTCP

#define ARM_INTCP_PL011_BASE0		0x16000000
#define ARM_INTCP_PL011_BASE1 		0x17000000
#define ARM_INTCP_PL011_DR 		0x0
#define ARM_INTCP_PL011_ECR		0x04
#define ARM_INTCP_PL011_FR 		0x18
#define ARM_INTCP_PL011_RXFE		0x10
#define ARM_INTCP_PL011_TXFE		0x80

#define ARM_INTCP_PL110_BASE		0xc0000000
#define ARM_INTCP_PL110_CR_EN   	0x001
#define ARM_INTCP_PL110_CR_16BPP   	0xf9
#define ARM_INTCP_PL110_CR_BGR  	0x100
#define ARM_INTCP_PL110_CR_BEBO 	0x200
#define ARM_INTCP_PL110_CR_BEPO 	0x400
#define ARM_INTCP_PL110_CR_PWR  	0x800

#define ARM_INTCP_PL050_KB_BASE		0x18000000
#define ARM_INTCP_PL050_MO_BASE		0x19000000
#define ARM_INTCP_PL050_TXEMPTY         (1 << 6)
#define ARM_INTCP_PL050_TXBUSY          (1 << 5)
#define ARM_INTCP_PL050_RXFULL          (1 << 4)
#define ARM_INTCP_PL050_RXBUSY          (1 << 3)
#define ARM_INTCP_PL050_RXPARITY        (1 << 2)
#define ARM_INTCP_PL050_KMIC            (1 << 1)
#define ARM_INTCP_PL050_KMID            (1 << 0)

/* externs */
extern unsigned vgafb_res_x, vgafb_res_y;	/* resolution */
extern char *vgafb;	/* frame buffer */
extern char *vgadb;	/* double buffer */
extern unsigned char vgagui;

#endif
