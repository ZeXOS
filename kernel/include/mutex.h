/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _MUTEX_H
#define _MUTEX_H

#include <task.h>

typedef struct mutex {
	volatile int locked;
} mutex_t;

#define MUTEX_CREATE(name) mutex_t name = { .locked = 0 }

/* externs */
extern unsigned mutex_lock (mutex_t *mutex);
extern unsigned mutex_unlock (mutex_t *mutex);
extern unsigned mutex_init (mutex_t *mutex);

#endif
