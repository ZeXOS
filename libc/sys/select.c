/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <_libc.h>

void _fd_clr (int fd, fd_set *set)
{
	if (!set || fd < 0)
		return;
	
	unsigned i;
	
	for (i = 0; i < set->count; i ++)
		if (set->fd[i] == fd) {
			set->fd[i] = -1;
			
			if (set->count == i+1)
				set->count --;
			break;
		}
}

int _fd_isset (int fd, fd_set *set)
{
	if (!set || fd < 0)
		return 0;
	
	unsigned i;

	for (i = 0; i < set->count; i ++)
		if (set->fd[i] == fd)
			return 1;
	
	return 0;
}

void _fd_set (int fd, fd_set *set)
{
	if (!set || fd < 0)
		return;
	
	unsigned i;
	unsigned y = 0;
	
	/* try to find free entry in actual list */
	for (i = 0; i < set->count; i ++)
		if (set->fd[i] == -1) {
			set->fd[i] = fd;
			y ++;
			break;
		}
		
	/* add new entry into list */
	if (!y) {
		if (set->count < FD_SETSIZE)
			set->fd[set->count ++] = fd;
	}
}

void _fd_zero (fd_set *set)
{
	if (!set)
		return;
	
	set->count = 0;
}

int select (int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
{
	struct select_t {
		int nfds;
		fd_set *readfds;
		fd_set *writefds;
		fd_set *exceptfds;
		struct timeval *timeout;
	};
	
	struct select_t s;
	s.nfds = nfds;
	s.readfds = readfds;
	s.writefds = writefds;
	s.exceptfds = exceptfds;
	s.timeout = timeout;
	
	asm volatile (
		"movl $64, %%eax;"
	     	"movl %0, %%ebx;"
	     	"int $0x80;":: "b" (&s) : "%eax", "memory");

	errno_update ();
	
	/* get return value */
	return (int) *SYSV_SELECT;
}