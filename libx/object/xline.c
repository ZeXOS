/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libx/base.h>

void xline (unsigned x1, unsigned y1, unsigned x2, unsigned y2, unsigned color)
{// 		x1 = 450, y1 = 200, x1 = 500, y2 = 250 -- ok
 //		x1 = 450, y1 = 250, x1 = 500, y2 = 200 -- spatne

	float opacnex = 1;
	float opacney = 1;

	/* vektor s(delkax, delkay) */
	int delkax = 0;
	int delkay = 0;

	if (x1 > x2) {
		opacnex = -1;
		delkax = (signed) x1 - (signed) x2;
	} else
		delkax = (signed) x2 - (signed) x1;

	if (y1 > y2) {
		opacney = -1;
		delkay = (signed) y1 - (signed) y2;
	} else
		delkay = (signed) y2 - (signed) y1;

	int a = 0;
	int b = 0;

	if (delkax == 0) {
		for (b = y1; b <= ((signed) y1+delkay); b ++)
			xpixel (x1, b, color);

		return;
	}

	if (delkay == 0) {
		for (a = x1; a <= ((signed) x1+delkax); a ++)
			xpixel (a, y1, color);

		return;
	}

	/* parametricka rovnice */
	float i = 0;
	unsigned last_x = 0;
	unsigned last_y = 0;

	while (i < 1) {
		float x = (float) x1 + (float) delkax * i * opacnex;
		float y = (float) y1 + (float) delkay * i * opacney;
	
		if (last_x != x && last_y != y)
			xpixel ((unsigned) x, (unsigned) y, color);

		last_x = (unsigned) x;
		last_y = (unsigned) y;

		i += 0.01f;
	}
}
