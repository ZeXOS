/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "platform.h"
#include "config.h"
#include "proto.h"
#include "net.h"
#include "ui.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>


static net_t net;

int net_send (char *data, unsigned len)
{
	if (!data || !len)
		return -1;

	int ret;

	if ((ret = send (net.sock, data, len, 0)) == -1) {
		printf ("ERROR -> send (%d) = -1\n", net.sock);
		return -1;
	}

	return ret;
}

int net_connect (char *server, int port)
{
	struct hostent *host;
	
	/* connect to im server */

	// Check info about remote host
	if ((host = gethostbyname ((char *) server)) == NULL) {
		printf ("Wrong address -> %s:%d\n", server, port);
		return 0;
	}
	// Create socket
	if ((net.sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return 0;
	}
	
	// Fill structure sockaddr_in
	// 1) Protocol family
	net.conn.sin_family = AF_INET;
	// 2) Port of remote server
	net.conn.sin_port = htons (port);
	// 3) Remote ip address
	memcpy (&(net.conn.sin_addr), host->h_addr, host->h_length);
	
	// P�ipojen� soketu
	if (connect (net.sock, (struct sockaddr *) &net.conn, sizeof (net.conn)) == -1) {
		printf ("Connection cant be estabilished -> %s:%d\n", server, port);
		return 0;
	}

	net.buffer = (char *) malloc (sizeof (char) * NET_BUFFER_SIZE);

	if (!net.buffer)
		return 0;

	usleep (10);

	net_switchmode ();

	return 1;
}

int net_switchmode ()
{
	/* set socket "sock" to non-blocking */
	int oldFlag = fcntl (net.sock, F_GETFL, 0);
	if (fcntl (net.sock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	return 1;
}

static int strnchrn (char *str, unsigned len)
{
	unsigned l = 0;

	while (l < len) {
		if (str[l] == '\n') {
			str[l] = '\0';

			return l;
		}

		l ++;
	}

	return 0;
}

int net_loop ()
{
	int ret = recv (net.sock, net.buffer, NET_BUFFER_SIZE-1, 0);

	/* incoming data */
	if (ret > 0) {
		int len = 0;
		net.buffer[ret] = '\0';

		while (len < ret) {
			int l = strnchrn (net.buffer+len, ret-len);

			if (!l)
				break;
 
			proto_handler (net.buffer+len, l);

			len += (l + 1);
		}

		return 1;
	}

#ifndef __zexos__
	if (!ret) {
#else
	if (ret == -1) {
#endif
		printf ("ERROR -> Disconnected by server\n");
		return -1;
	}

	ui_getcommand ();

	usleep (40);

#ifdef __zexos__
	schedule ();
#endif

	return 0;
}

/** INIT NETWORK function */
int init_net ()
{
	return net_connect (cfg->server, cfg->port);;
}
