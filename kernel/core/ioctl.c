/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <config.h>
#include <ioctl.h>

int ioctl_call (unsigned id, void *buf, int l)
{
	switch (id) {
		case IOIFADDRSET:
		{
			struct ioifaddr_t *io = buf;

			if (l != sizeof (struct ioifaddr_t))
				return -1;

			netif_t *netif = netif_findbyname (io->dev);

			if (!netif)
				return -1;

			netif_ip_addr (netif, io->ipv4, IF_CFG_TYPE_STATIC);
			netif_ipv6_addr (netif, io->ipv6, IF_CFG_TYPE_STATIC);

			return 0;
		}
		case IOIFADDRGET:
		{
			struct ioifaddr_t *io = buf;

			if (l != sizeof (struct ioifaddr_t))
				return -1;

			netif_t *netif = netif_findbyname (io->dev);

			if (!netif)
				return -1;

			memcpy (&io->ipv4, &netif->ip, sizeof (net_ipv4));
			memcpy (&io->ipv6, netif->ipv6, sizeof (net_ipv6));

			return 0;
		}
		case IOATAREAD:
		{
			struct ioatarq_t *io = buf;

			if (l != sizeof (struct ioatarq_t))
				return -1;

			dev_t *dev = dev_find (io->dev);

			if (!dev)
				return -1;

			partition_t p;
			p.base_io = 0;
			
			switch (io->dev[7]) {
				case 'a':
					p.id = 0;
					p.base_io = 0x1F0;
					break;
				case 'b':
					p.id = 1;
					p.base_io = 0x1F0;
					break;
				case 'c':
					p.id = 0;
					p.base_io = 0x170;
					break;
				case 'd':
					p.id = 1;
					p.base_io = 0x170;
					break;
			}
			
			if (!dev->handler (DEV_ACT_READ, &p, io->data, "", io->sector))
				return -1;

			return 0;
		}
		case IOATAWRITE:
		{
			struct ioatarq_t *io = buf;

			if (l != sizeof (struct ioatarq_t))
				return -1;

			dev_t *dev = dev_find (io->dev);

			if (!dev)
				return -1;
			
			partition_t p;
			p.base_io = 0;
			
			switch (io->dev[7]) {
				case 'a':
					p.id = 0;
					p.base_io = 0x1F0;
					break;
				case 'b':
					p.id = 1;
					p.base_io = 0x1F0;
					break;
				case 'c':
					p.id = 0;
					p.base_io = 0x170;
					break;
				case 'd':
					p.id = 1;
					p.base_io = 0x170;
					break;
			}
	
			if (!dev->handler (DEV_ACT_WRITE, &p, io->data, "", io->sector))
				return -1;

			return 0;
		}
	}

	return 1;
}
