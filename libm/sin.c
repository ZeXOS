/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Thanks for Oroborus
 */


#include <math.h>
#include <stdlib.h>

static double fact (double x)
{
	if (x == 0.0)
		return 1.0;

	return x * fact (x - 1.0);
}

static double expo (double x, int a)
{
	double ret;
	int i;
	
	ret = 1.0;

	for (i = 0 ; i < abs (a); i ++)
		ret *= x;

	return (a > 0 ? ret : 1 / ret);
}

double sin (double x)
{
	double ret;
	int i;

	ret = 0.0;

	while (x < 0)
		x += 2 * M_PI;

	while (x > 2 * M_PI)
		x -= 2 * M_PI;

	for (i = 0; i < 16; i ++)
		ret += (expo (-1, i) * expo (x, 2 * i + 1)) / fact (2 * i + 1.0);

	return ret;
}
