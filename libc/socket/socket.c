/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h>
#include <sys/socket.h>
#include <_libc.h>

int socket (int family, int type, int protocol)
{
	asm volatile (
		"movl $12, %%eax;"
	     	"movl %0, %%ebx;"
	     	"movl %1, %%ecx;"
	     	"movl %2, %%edx;"
	     	"int $0x80;" :: "g" (family), "g" (type), "g" (protocol) : "%eax", "%ebx", "%ecx", "%edx");

	/* get return value */
	int *memptr = (int *) SYSV_SOCKET;
	int *ret = memptr;

	errno_update ();

	return (int) *ret;
}

