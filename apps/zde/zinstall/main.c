/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <appcl.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>

typedef struct {
	unsigned step;
	char drive[32];
	int fd;
	unsigned flen;
	unsigned long s;
	struct ioatarq_t rq;
	appcl_t *appcl;
} zinstall_t;
zinstall_t zinst;

#define IMAGE_FILE	"inst.img"

void zinstall_draw_step1 ()
{
	zgui_puts (10, 40, "First step is focused on hard-drive ID,\nthat mean, you must select\n correct disk for installation.\n\n", 0);
	
	zgui_input (10, 80, &zinst.drive, 31, 0);
}

void zinstall_draw_step2 ()
{
	zgui_puts (10, 40, "Are you sure you want overwrite\nall data on selected drive ?", 0);

	zgui_puts (20, 80, zinst.drive, 0);
}

int zinstall_open ()
{
	zinst.fd = open (IMAGE_FILE, O_RDONLY);

	if (zinst.fd < 0) {
		printf ("ERROR -> image file '%s' not found\n", IMAGE_FILE);
		return -1;
	}
	printf ("zinst.fd: %d\n", zinst.fd);

	zinst.flen = 2048*1024;	/* 2MB image */

	printf ("Image size: %dkB\n", zinst.flen/1024);

	unsigned l = strlen (zinst.drive);
	memcpy (zinst.rq.dev, zinst.drive, l);
	zinst.rq.dev[l] = '\0';

	zinst.s = 0;
	
	return 0;
}

void zinstall_close ()
{
	close (zinst.fd);
}

int zinstall_draw_step3 ()
{
	unsigned char percent = zinst.s/41+1;
  
	char str[32];
	sprintf (str, "Installing: %d %c", zinst.s/41+1, '%');
	zgui_puts (10, 40, str, 0);

	xrectfill (zinst.appcl->x+50, zinst.appcl->y+75, zinst.appcl->x+50+percent*2, zinst.appcl->y+87, 0xffa000);
	xrectfill (zinst.appcl->x+50+percent*2, zinst.appcl->y+75, zinst.appcl->x+250, zinst.appcl->y+87, 0x0000ff);
	
	if (zinst.s >= zinst.flen/512)
		return 0;
	
	zinst.rq.sector = zinst.s ++;
		
	memset (zinst.rq.data, 0, 512);

	int l = read (zinst.fd, zinst.rq.data, 512);
		
	if (!l) {
		printf ("ERROR -> read () == 0; image data are probably corrupted\n");
		return -1;
	}

	int r = ioctl (IOATAWRITE, &zinst.rq, sizeof (struct ioatarq_t));

	if (r == -1) {
		printf ("WARNING -> ioctl (IOATAWRITE) == -1; repeating\n");
		sleep (1);
		r = ioctl (IOATAWRITE, &zinst.rq, sizeof (struct ioatarq_t));
		if (r == -1) {
			printf ("ERROR -> ioctl (IOATAWRITE) == -1; installation failed !\n");
			return -1;
		}
	}

	return 0;
}

void zinstall_draw_step4 ()
{
	zgui_puts (10, 40, "Installation was finished\nCongratulation !", 0);
}

void zinstall_draw ()
{
	zgui_puts (10, 10, "ZeX/OS Installer\n-=-=-=-=-=-=-=-=-", 0);

	unsigned btn_next = zgui_button (150, 145, "Next");
	
	if (!btn_next && zinst.step < 4) {
		zinst.step ++;
		
		if (zinst.step == 3) {
			int r = zinstall_open ();
			
			if (r == -1)
				zinst.step = 5;
		} else if (zinst.step == 4)
			zinstall_close ();
	}
	
	switch (zinst.step) {
		case 0:
			zgui_puts (10, 40, "Welcome !\nYou are attempting to install ZeX/OS\nonto your hard-drive, please be sure\nabout your backup because of data loss.\n\nWarning: This version is in the alpha-stage,\nit is possible that some error may occur.\n\nPress button \"Next\" for continue\nin installation process", 0);
			break;
		case 1:
			return zinstall_draw_step1 ();
		case 2:
			return zinstall_draw_step2 ();
		case 3:
		{
			int r = zinstall_draw_step3 ();
			
			if (r == -1)
				zinst.step = 5;
			break;
		}
		case 4:
			return zinstall_draw_step4 ();
		case 5:
			zgui_puts (10, 40, "Installation failed !\nprobably installer files are missing", 0);
			break;
	}
}

int main (int argc, char **argv)
{
	int exit = 0;

	zinst.step = 0;
	memcpy (zinst.drive, "/dev/hda", 8);
	zinst.drive[8] = '\0';
	
	if (zgui_init () == -1)
		return -1;

	zinst.appcl = zgui_window ();
	
	while (!exit) {
		unsigned state = zgui_event ();

		if (state & APPCL_STATE_REDRAW)
			zinstall_draw ();

		if (state & APPCL_STATE_EXIT)
			exit = 1;
	}

	return zgui_exit ();
}
 
