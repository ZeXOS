/*
 *  ZeX/OS
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <net/socket.h>
#include <system.h>
#include <select.h>
#include <errno.h>
#include <file.h>
#include <fd.h>

extern unsigned long timer_ticks;

void _fd_clr (int fd, fd_set *set)
{
	if (!set || fd < 0)
		return;
	
	unsigned i;
	
	for (i = 0; i < set->count; i ++)
		if (set->fd[i] == fd) {
			set->fd[i] = -1;
			
			if (set->count == i+1)
				set->count --;
			break;
		}
}

int _fd_isset (int fd, fd_set *set)
{
	if (!set || fd < 0)
		return 0;
	
	unsigned i;

	for (i = 0; i < set->count; i ++)
		if (set->fd[i] == fd)
			return 1;
	
	return 0;
}

void _fd_set (int fd, fd_set *set)
{
	if (!set || fd < 0)
		return;
	
	unsigned i;
	unsigned y = 0;
	
	/* try to find free entry in actual list */
	for (i = 0; i < set->count; i ++)
		if (set->fd[i] == -1) {
			set->fd[i] = fd;
			y ++;
			break;
		}
		
	/* add new entry into list */
	if (!y) {
		if (set->count < FD_SETSIZE)
			set->fd[set->count ++] = fd;
	}
}

void _fd_zero (fd_set *set)
{
	if (!set)
		return;
	
	set->count = 0;
}

int select (int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, const struct timeval *timeout)
{
	unsigned rfds = 0;
	unsigned wfds = 0;
	unsigned efds = 0;
	
	if (readfds)
		rfds = (unsigned) readfds->count;
	if (writefds)
		wfds = (unsigned) writefds->count;
	if (exceptfds)
		efds = (unsigned) exceptfds->count;
	
	/* prevent from memory overwrite */
	if (rfds > FD_SETSIZE)
		rfds = FD_SETSIZE;
	if (wfds > FD_SETSIZE)
		wfds = FD_SETSIZE;
	if (efds > FD_SETSIZE)
		efds = FD_SETSIZE;
	
	unsigned long stime = timer_ticks;
	unsigned long ms;
	
	if (timeout)
		ms = timeout->tv_usec / 1000 + timeout->tv_sec * 1000;	// covert to milisecond precise
	else
		ms = 50000;

	unsigned r;

	for (; (stime+ms) >= timer_ticks; ) {
		for (r = 0; r < rfds; r ++) {
			if (readfds->fd[r] == -1)
				continue;
		  
			fd_t *fd = fd_get (readfds->fd[r]);
			
			if (!fd) {
				_fd_clr (readfds->fd[r], readfds);
				continue;
			}
//printf ("select0: %d\n", readfds->fd[r]);
			/* TODO: stdin, stdout */

			if (fd->flags & FD_SOCK) {	// socket select
				int ret = sselect (fd->id, 0, 0);
				
				if (ret > 0){//printf ("select0: %d - %d\n", readfds->fd[r], ret);
					  goto ready;
				}
				continue;
			}
			
			if (!fd->s) {
				_fd_clr (fd->id, readfds);
				continue;
			}

ready:
			_fd_zero (readfds);
			_fd_zero (writefds);
			_fd_zero (exceptfds);

			_fd_set (fd->id, readfds);

			/* TODO: make select to handle more then one event per call */
			return 1;
		}

		schedule ();
	}
	
	_fd_zero (readfds);
	_fd_zero (writefds);
	_fd_zero (exceptfds);
	
	return 0;
}

 
