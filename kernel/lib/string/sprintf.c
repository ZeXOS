/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h>

int vsprintf (char *buffer, const char *fmt, va_list args)
{
	int ret_val = do_printf (fmt, args, vsprintf_help, (void *) buffer);
	
	buffer[ret_val] = '\0';
	
	return ret_val;
}

int sprintf (char *buffer, const char *fmt, ...)
{
	va_list args;

	va_start (args, fmt);
	int ret_val = vsprintf (buffer, fmt, args);
	va_end (args);
	
	return ret_val;
} 
