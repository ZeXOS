/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

FILE *fopen (const char *filename, const char *mode)
{
	int flags = 0;

	/* TODO - provizorni .. dodelat dalsi mody */
	if (mode) {
		if (mode[0] == 'r')
			flags |= O_RDONLY;
		if (mode[0] == 'w')
			flags |= O_WRONLY;
	}

	int fd = open (filename, flags);

	if (fd < 0)
		return 0;

	int l = lseek (fd, 0, SEEK_END);

	lseek (fd, 0, SEEK_SET);

	FILE *f = (FILE *) malloc (sizeof (FILE));

	if (!f)
		return 0;

	f->mode = flags;
	f->fd = fd;
	f->e = (unsigned) l;

	errno_update ();

	return f;
}
