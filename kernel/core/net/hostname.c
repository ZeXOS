/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <stdlib.h>
#include <config.h>

/* global hostname */
static char *net_hostname;

unsigned hostname_set (const char *name)
{
	if (net_hostname)
		kfree (net_hostname);

	unsigned l = strlen (name);
	
	net_hostname = (char *) kmalloc (sizeof (char) * (l + 1));
	
	if (!net_hostname)
		return 0;
	
	memcpy (net_hostname, name, l);
	net_hostname[l] = '\0';
	
	return 1;
}

char *hostname_get ()
{
	if (!net_hostname)
		return 0;
	
	return net_hostname;
}

unsigned init_hostname ()
{
	net_hostname = 0;
  
	return hostname_set (CONFIG_HOSTNAME);
}
