/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _IN_H
#define _IN_H

typedef unsigned short in_port_t;
typedef unsigned in_addr_t;

struct in_addr
{
	unsigned s_addr;  		/* IPv4 address */
};

struct in6_addr
{
	unsigned short s6_addr[8];  	/* IPv6 address */
};

struct sockaddr_in {
	int sin_family;
	unsigned short sin_port;
	struct in_addr sin_addr;
	unsigned char sin_zero[8];
};

struct sockaddr_in6 {
	unsigned char sin6_len;		/* length of this structure */
	unsigned char sin6_family;	/* AF_INET6                 */
	unsigned short sin6_port;	/* Transport layer port #   */
	unsigned sin6_flowinfo;		/* IPv6 flow information    */
	struct in6_addr sin6_addr;	/* IPv6 address             */
};

#endif
