/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <string.h>
#include <dev.h>
#include <fs.h>
#include <partition.h>
#include <cache.h>
#ifdef TEST
static int cele;

static unsigned char tolower (char c)
{
	return ((c >= 'A' && c <= 'Z') ? c + 32: c);
}

static void dec2bin (long decimal, char *binary)
{
        int  k = 0, n = 0;
        int  neg_flag = 0;
        int  remain;
        char temp[80];

        // take care of negative input
        if (decimal < 0) {
                decimal = -decimal;
                neg_flag = 1;
        } do {
                remain    = decimal % 2;
                decimal   = decimal / 2;   // whittle down decimal
                temp[k++] = remain + 0x30; // makes characters 0 or 1
        } while (decimal > 0);

        if (neg_flag)
		temp[k++] = '-';           // add back - sign
        else
		temp[k++] = ' ';           // or space

        while (k >= 0)
                binary[n++] = temp[--k];   // reverse spelling

        binary[n-1] = 0;             // end with NULL

	
}

static void parse_attr (char attr,int y)
{
/*
	0x01    000001    Read only
	0x02    000010    Hidden
	0x04    000100    System file
	0x08    001000    Volume ID
	0x10    010000    Directory
	0x20    100000    Binary which stands for Archive
*/

//	printf ("attr: %d '%c'\n", attr, attr);

	dir[y].read = 0;
	dir[y].hidden = 0;
	dir[y].system = 0;
	dir[y].volume = 0;
	dir[y].dir = 0;
	dir[y].bin = 0;

	if (attr & 0x01)
		dir[y].read = 1;
	if (attr & 0x02)
		dir[y].hidden = 1;
	if (attr & 0x04)
		dir[y].system = 1;
	if (attr & 0x08)
		dir[y].volume = 1;
	if (attr & 0x10)
		dir[y].dir = 1;
	if (attr & 0x20)
		dir[y].bin = 1;

//	printf ("attr: r%u h%u s%u v%u d%u b%u\n", dir[y].read, dir[y].hidden, dir[y].system, dir[y].volume, dir[y].dir, dir[y].bin);


/*	int i;
	int x=0;

	printf ("file: %s | binary: %s\n", dir[y].name, binary);
	for(i = strlen(binary)-1; i>=0; i--)
	{
		if(binary[i] == '1')
		{
			switch (x)
			{
				case 0: dir[y].read=1; break;
				case 1: dir[y].hidden=1; break;
				case 2: dir[y].system=1; break;
				case 3: dir[y].volume=1; break;
				case 4: dir[y].dir=1; break;
				case 5: dir[y].bin=1; break;
			}
		}
		x++;
	}*/
}

#ifdef TEST
static void parse_attr (char* binary,int y)
{
/*
	0x01    000001    Read only
	0x02    000010    Hidden
	0x04    000100    System file
	0x08    001000    Volume ID
	0x10    010000    Directory
	0x20    100000    Binary which stands for Archive
*/

	int i;
	int x=0;
	dir[y].read=0;
	dir[y].hidden=0;
	dir[y].system=0;
	dir[y].volume=0;
	dir[y].dir=0;
	dir[y].bin=0;

//	printf ("file: %s | binary: %s\n", dir[y].name, binary);

	for (i = strlen (binary)-1; i >= 0; i --) {
		if(binary[i] == '1') {
			switch (x) {
				case 0: dir[y].read=1; break;
				case 1: dir[y].hidden=1; break;
				case 2: dir[y].system=1; break;
				case 3: dir[y].volume=1; break;
				case 4: dir[y].dir=1; break;
				case 5: dir[y].bin=1; break;
			}
		}
		x++;
	}
}
#endif


int fat16_read (unsigned int byte, unsigned int byte2)
{
	BYTE text[512];
	unsigned char *buf = text;
	unsigned int i=0;
	unsigned int x;
	unsigned int start = -1;
	unsigned int next = 0;
	unsigned int test = 0;
	unsigned int pom = 0;

	lba28_drive_read (curr_part, 64, text);//read_block(1,text,1);

	buf = text;

	for(x = 0; x < 512; x ++) {
		//if (i>byte-3 && i<byte+3)
		//kprintf("%x",*buf);
		if (i==byte)
			start = *buf;

		if (i==byte2 && start!=-1) {
            		if (start == 0xf0 && *buf == 0xff)
				return -2;

			next = ((*buf << 8 | start) >> 4);
			test = (*buf << 8 | start);
			
			if (cele == 0)
				next = (*buf << 8 | start)&0x0FFF;

			if (next < 0)
				next = next*(-1);
//kprintf("\nnext=%x %d start=%x end=%x byte=%d byte1=%d cele=%d test=%x\n",next,next,start,*buf,byte,byte2,cele,test);
			
			return next;
		}

		*buf ++;
		i ++;
	}

	return -1;

}

int fat16_nextsector (int i)
{
	cele = (i * 150) % 100;

	return (i * 150) / 100;
}

void fat16_read_f (unsigned int cluster){
	static BYTE text1[512] = "";
	unsigned char *buf;
	unsigned int x;
//	kprintf("\n*** %d\n",cluster);
	printf ("cluster: %d\n", cluster);
	lba28_drive_read (curr_part, 322 + cluster, text1);//read_block (33 + cluster, text1, 1);
	//strcpy(buf,text1);
	//text1[strlen(text1)-4]='\0';

	unsigned len = 512;
	memcpy (file_cache+file_cache_id, text1, len);

	file_cache_id += len;
	
	kprintf("%s",file_cache);
	//kprintf("%s",text1);fat16_read_dir
	//+=*buf;
	//strcat(file_cache,*buf);
/*
	for(x=0; x<512; x++)
	{
		wait();
		kprintf("%c",*buf);
		*buf++;
	}
*/
}

//  33 sector = open space
unsigned long fat16_read_file (partition_t *p, int aa)
{
	int i;
	int byte;
	int next = 0;
	int sect;
	cele=0;
	strcpy (file_cache, "");

	next = fat16_nextsector(dir[aa].start);	// start: 4; fat: 6; sect: 16 .. cluster: 2

	sect = fat16_read(next,next+1);	
//	printf ("name=%s start=%d fat=%d %d\n",dir[aa].name,dir[aa].start,next, sect);
	file_cache_id = 0;
	next = fat16_nextsector(sect);
//	printf ("name=%s start=%d fat=%d %d\n",dir[aa].name,dir[aa].start,next, sect);
	fat16_read_f (dir[aa].start);
// 	file_cache[strlen(file_cache)-4]='\0';
	for(i = 0; i < 100; i++)
	{
		//DPRINT (DBG_DRIVER | DBG_FS, "next: %d sect: %d", next, sect);
		if (sect < 0 || sect == 4095)
			break;
		//file_cache[strlen (file_cache)-4] = '\0';
		fat16_read_f (sect);
		sect = fat16_read (next, next+1);
		next = fat16_nextsector (sect);
	}

	return file_cache_id;
}

void fat16_read_dir (partition_t *p, int aa)
{
	unsigned char *text = (unsigned char *) kmalloc (512 * sizeof (unsigned char));
	unsigned char *buf = text;

	int x;
	int i = 0;
	int y = 0;
	char *bin;
	int label = 0;
	unsigned int start;
	/* vymazeme nazvy vsech souboru */
	for(x = 0; x < 223; x ++)
		dir[x].name[0] = '\0';

	if (aa == -1)
		lba28_drive_read (p, 286, text);//read_block (19, text, 1);
	else			//       321+3-2 = 324-2 = 322
		lba28_drive_read (p, 321+dir[aa].start-2, text);//read_block (33+dir[aa].start-2, text, 1);

	buf = text;

	for (x = 0; x < 512; x ++) {//286, 63
		if (i == 31) {
			i=-1;
			y++;
			label=0;
		}
		//if (*buf)
		//printf ("#i je: %d '%c'", i, *buf);
		if (i == 26)
			start = *buf;
		if (i == 27 && label == 0) {
			dir[y].start = (*buf << 8 | start);
		//	printf ("start: %d\n", dir[y].start);
		}
		if (i == 11 && (*buf == 0x0f || *buf == 0x00)) {
			strcpy (dir[y].name, "");
			label = 1;
			y --;
		}
		if (i == 11 && *buf != 0x0f && *buf != 0x00) {
			//putch (*buf);
			//dec2bin (*buf,bin);
			parse_attr (*buf, y);
		}
		if (i <= 10 && i >= 0)
			dir[y].name[i] = tolower (*buf);

		/*if (i < 32) {
			printf ("> '%c' '%d'#", (unsigned char) *buf, i);
			usleep (100);
		}*/

		i ++;
		*buf ++;
	}


	//free (text);
}
#endif
bool fat16_handler (unsigned act, char *block, unsigned n, unsigned long l)
{
	switch (act) {
		case FS_ACT_INIT:
		{
			/* nothing */
			return 1;
		}
		break;
		case FS_ACT_READ:
		{
			/*fat16_read_file (curr_part, n);
			memcpy (block, file_cache, file_cache_id);
			block[file_cache_id] = '\0';
			l = file_cache_id;*/

			return 1;
		}
		break;
		case FS_ACT_CHDIR:
		{
			/*fat16_read_dir (curr_part, n);

			memcpy (block, file_cache, file_cache_id);
			block[file_cache_id] = '\0';
			l = file_cache_id; */

			return 1;
		}
		break;
	}

	return 0;
}
#endif
