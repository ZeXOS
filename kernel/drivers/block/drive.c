/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <mytypes.h>
#include <partition.h>
#include <dev.h>
#include <vfs.h>
#include <fs.h>
#include <arch/io.h>
#include <drive.h>

extern unsigned long timer_ticks;

static int drive_type (int port)
{
	outb (0x70, port);
	unsigned char c = inb (0x71);
			    
	if (c == 255)
		return DRIVE_TYPE_CDROM;

	return DRIVE_TYPE_HDD;
}

int lba28_drive_detect (int id)
{
	int tmpword;

	switch (id) {
		case 0:
			outb (0x1F6, 0xA0); // use 0xB0 instead of 0xA0 to test the second drive on the controller

			delay (200); // wait 1/250th of a second

			tmpword = inb (0x1F7); // read the status port
			if (tmpword & 0x40) // see if the busy bit is set
				return 1;

			return 0;
		case 1:
			outb (0x1F6, 0xB0); // use 0xB0 instead of 0xA0 to test the second drive on the controller
			
			delay (200); // wait 1/250th of a second

			tmpword = inb (0x1F7); // read the status port
			if (tmpword & 0x40) // see if the busy bit is set
				return 1;

			return 0;
		case 2:
			outb (0x176, 0xA0); // use 0xB0 instead of 0xA0 to test the second drive on the controller
			
			delay (200); // wait 1/250th of a second

			tmpword = inb (0x177); // read the status port
			if (tmpword & 0x40) // see if the busy bit is set
				return 1;

			return 0;
		case 3:
			outb (0x176, 0xB0); // use 0xB0 instead of 0xA0 to test the second drive on the controller
			
			delay (200); // wait 1/250th of a second

			tmpword = inb (0x177); // read the status port
			if (tmpword & 0x40) // see if the busy bit is set
				return 1;

			return 0;
	}
}

/* check status of currently selected device */
int ata_status (partition_t *p, int flag)
{
	int status = inb (p->base_io + ATA_STATUS);

	switch (flag) {
		case LBA_FLAG_ERROR:
			if(status & ATA_STATUS_ERR)
				return 1;
			break;
		case LBA_FLAG_BUSY:
			if(status & ATA_STATUS_BUSY)
				return 1;
			break;
		case LBA_FLAG_DATA:
			if(status & ATA_STATUS_DRQ)
				return 1;
			break;
		default:
			break;
	}

	return 0;
}

int ata_wait (partition_t *p)
{
	unsigned long stime = timer_ticks;

	/* timeout for 5000ms */
	while ((stime+5000) > timer_ticks) {
		if (!ata_status (p, LBA_FLAG_BUSY))
			return 0;

		schedule ();
	}

	return 1;
}

int lba28_drive_read (partition_t *p, unsigned addr, unsigned char *buffer)
{
	if (!p->base_io)
		return 0;

	outb (p->base_io + 1, 0x00);
	outb (p->base_io + 2, (unsigned char) 0x01);
	outb (p->base_io + 3, (unsigned) addr);
	outb (p->base_io + 4, (unsigned) (addr >> 8));
	outb (p->base_io + 5, (unsigned) (addr >> 16));
	outb (p->base_io + 6, 0xE0 | (p->id << 4) | ((addr >> 24) & 0x0F));
	outb (p->base_io + 7, 0x20);

	if (ata_wait (p)) {
		DPRINT (DBG_DRIVER | DBG_BLOCK, "lba28_drive_read: device is busy");
		return 0;
	}

	/*if (ata_status (p, LBA_FLAG_ERROR)) {
		DPRINT (DBG_DRIVER | DBG_BLOCK, "lba28_drive_read: read error - device");
		return 0;
	}*/

	if (!ata_status (p, LBA_FLAG_DATA)) {
		DPRINT (DBG_DRIVER | DBG_BLOCK, "lba28_drive_read: no data are available");
		return 0;
	}

	unsigned short tmpword;
	// for read:
	int idx;
	for (idx = 0; idx < 256; idx ++) {
 		tmpword = inw (p->base_io);
		buffer[idx * 2] = (unsigned char)tmpword;
		buffer[idx * 2 + 1] = (unsigned char) (tmpword >> 8);
	}

	return 1;
}

/* NOTE: this code is buggy, because you need everytime go from 0 to 256 indexes (512 bytes) */
int lba28_drive_read_len (partition_t *p, unsigned addr, unsigned char *buffer, unsigned len)
{
	outb (0x1F1, 0x00);
	outb (0x1F2, (unsigned char) 0x01);
	outb (0x1F3, (unsigned) addr);
	outb (0x1F4, (unsigned) (addr >> 8));
	outb (0x1F5, (unsigned) (addr >> 16));
	outb (0x1F6, 0xE0 | (p->id << 4) | ((addr >> 24) & 0x0F));
	outb (0x1F7, 0x20);

	//usleep (1000);

	while (!(inb (0x1F7) & 0x08))	// 0x08
		schedule ();

	unsigned short tmpword;
	// for read:
	int idx;
	int i;
	for (idx = 0; idx < len; idx ++) {
		tmpword = inw (0x1F0);
		i = (idx * 2);

		if (i < len)
			buffer[i] = (unsigned char) tmpword;
		if (i+1 < len)
			buffer[i + 1] = (unsigned char) (tmpword >> 8);
	}

	return 1;
}

int lba28_drive_write (partition_t *p, unsigned addr, unsigned char *buffer)
{
	if (!p->base_io)
		return 0;

	/* p->id = 0 for master device */
	outb (p->base_io + 1, 0x00);
	outb (p->base_io + 2, (unsigned char) 0x01);
	outb (p->base_io + 3, (unsigned) addr);
	outb (p->base_io + 4, (unsigned) (addr >> 8));
	outb (p->base_io + 5, (unsigned) (addr >> 16));
	outb (p->base_io + 6, 0xE0 | (p->id << 4) | ((addr >> 24) & 0x0F));
	outb (p->base_io + 7, 0x30);

	unsigned long stime = timer_ticks;

	if (ata_wait (p)) {
		DPRINT (DBG_DRIVER | DBG_BLOCK, "lba28_drive_write: device is busy");
		return 0;
	}

	/*if (ata_status (p, LBA_FLAG_ERROR)) {
		DPRINT (DBG_DRIVER | DBG_BLOCK, "lba28_drive_write: write error - device");
		return 0;
	}*/

	/*if (!ata_status (p, LBA_FLAG_DATA)) {
		DPRINT (DBG_DRIVER | DBG_BLOCK, "lba28_drive_write: no data are available");
		return 0;
	}*/

	unsigned short tmpword;
	// for write:
	int idx;
	for (idx = 0; idx < 256; idx ++) {
		tmpword = buffer[idx * 2] | (buffer[idx * 2 + 1] << 8);
		outw (p->base_io, tmpword);
	}

	return 1;
}

/* NOTE: this code is buggy, because you need everytime go from 0 to 256 indexes (512 bytes) */
int lba28_drive_write_len (partition_t *p, unsigned addr, unsigned char *buffer, unsigned len)
{
	outb (0x1F1, 0x00);
	outb (0x1F2, (unsigned char) 0x01);
	outb (0x1F3, (unsigned) addr);
	outb (0x1F4, (unsigned char) (addr >> 8));
	outb (0x1F5, (unsigned char) (addr >> 16));
	outb (0x1F6, 0xE0 | (p->id << 4) | ((addr >> 24) & 0x0F));
	outb (0x1F7, 0x30);

	while (!(inb (0x1F7) & 0x08))
		schedule ();

	unsigned short tmpword;
	unsigned short l = 0;
	unsigned short h = 0;
	// for write:

	int idx;
	for (idx = 0; idx < 256; idx ++) {
		l = 0;
		h = 0;

		if ((idx*2) < len)
			l = buffer[idx * 2];

		if ((idx*2+1) < len)
			h = (buffer[idx * 2 + 1] << 8);

		tmpword = l | h;
		outw (0x1F0, tmpword);
	}

	return 1;
}

int lba28_drive_write_spec (partition_t *p, unsigned addr, unsigned char *buffer, unsigned len, unsigned offset)
{
	outb (0x1F1, 0x00);
	outb (0x1F2, (unsigned char) 0x01);
	outb (0x1F3, (unsigned) addr);
	outb (0x1F4, (unsigned char) (addr >> 8));
	outb (0x1F5, (unsigned char) (addr >> 16));
	outb (0x1F6, 0xE0 | (p->id << 4) | ((addr >> 24) & 0x0F));
	outb (0x1F7, 0x30);

	while (!(inb (0x1F7) & 0x08))
		schedule ();

	unsigned short tmpword;
	unsigned short l = 0;
	unsigned short h = 0;
	// for write:

	int idx;
	int i = 0;
	for (idx = 0; idx < 256; idx ++) {
		l = 0;
		h = 0 << 8;
		i = (idx*2);

		if (i >= offset)
			if (i < len+offset)
				l = buffer[i-offset];
	
		if ((i+1) >= offset)
			if ((i+1) < len+offset)
				h = buffer[i-offset+1] << 8;
		

		tmpword = l | h;
		outw (0x1F0, tmpword);
	}

	return 1;
}

int lba48_drive_write (partition_t *p, unsigned long long addr, unsigned char *buffer)
{
	outb (0x1F1, 0x00);
	outb (0x1F1, 0x00);

	outb (0x1F2, 0x00);
	outb (0x1F2, 0x01);

	outb (0x1F3, (unsigned char) (addr >> 24));
	outb (0x1F3, (unsigned char) addr);
	outb (0x1F4, (unsigned char) (addr >> 32));
	outb (0x1F4, (unsigned char) (addr >> 8));
	outb (0x1F5, (unsigned char) (addr >> 40));
	outb (0x1F5, (unsigned char) (addr >> 16));

	outb (0x1F6, 0x40 | (p->id << 4));

	outb (0x1F7, 0x34);

	while (!(inb (0x1F7) & 0x08))	// 0x08
		schedule ();

	unsigned short tmpword;
	// for write:
	int idx;
	for (idx = 0; idx < 256; idx ++) {
		tmpword = buffer[idx * 2] | (buffer[idx * 2 + 1] << 8);
		outw (0x1F0, tmpword);
	}

	return 1;
}

bool ide_acthandler (unsigned act, partition_t *p, char *block, char *more, int n)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			return 1;
		}
		break;
		case DEV_ACT_READ:
		{
			return (bool) lba28_drive_read (p, n, block);
		}
		break;
		case DEV_ACT_WRITE:
		{
			return (bool) lba28_drive_write (p, n, block);
		}
		break;
		case DEV_ACT_MOUNT:
		{
			//fs_t *fs = fs_detect (p);
			int f = 0, t = 0;

			curr_part = p;

			unsigned l = strlen (more);

			if (!l)
				if (!p->fs->handler (FS_ACT_CHDIR, more, -1, 0))
					return 0;
			/*
			unsigned x = 0;
			while (dir[x].name[0] != '\0') {
				printf ("slozka: '%s'\n", dir[x].name);
				x ++;
			}*/

			if (l) {
				if (!strcmp (more, "."))
					return p->fs->handler (FS_ACT_CHDIR, more, 0, l);

				if (!strcmp (more, ".."))
					return p->fs->handler (FS_ACT_CHDIR, more, 1, l);

				while (strlen (dir[f].name)) {
					//printf ("drive__: '%s' : %d | '%s'\n", dir[f].name, f, more);
					if (!strncmp (dir[f].name, more, l)) {
						if (!p->fs->handler (FS_ACT_CHDIR, more, f, l))
							return 0;

						t = 1;
						break;
					}

					f ++;
				}

				if (!t)
					return 0;
			}

			f = 0;
			unsigned fl = strlen (dir[f].name);

			 while (fl) {
				unsigned attrib = VFS_FILEATTR_MOUNTED;

				/*unsigned x = fl;
				while (x) {
					if (dir[f].name[x] != ' ')
						
					x --;
				}*/
/*
				#define VFS_FILEATTR_FILE	0x1
				#define VFS_FILEATTR_DIR	0x2
				#define VFS_FILEATTR_HIDDEN	0x4
				#define VFS_FILEATTR_SYSTEM	0x8
				#define VFS_FILEATTR_BIN	0x10
				#define VFS_FILEATTR_READ	0x20
				#define VFS_FILEATTR_WRITE	0x40

				case 0: dir[y].read=1; break;
				case 1: dir[y].hidden=1; break;
				case 2: dir[y].system=1; break;
				case 3: dir[y].volume=1; break;
				case 4: dir[y].dir=1; break;
				case 5: dir[y].bin=1; break;
*/

				if (dir[f].read) {
					attrib |= VFS_FILEATTR_READ;
					attrib |= VFS_FILEATTR_WRITE;
				}
				if (dir[f].hidden)
					attrib |= VFS_FILEATTR_HIDDEN;
				if (dir[f].system)
					attrib |= VFS_FILEATTR_SYSTEM;
				if (dir[f].dir)
					attrib |= VFS_FILEATTR_DIR;
				else
					attrib |= VFS_FILEATTR_FILE;
				if (dir[f].bin)
					attrib |= VFS_FILEATTR_BIN;

					int i = 10;
					while (i) {
						if (dir[f].name[i] == ' ')
							dir[f].name[i] = '\0';
						else
							break;
			
						i --;
					}

				vfs_list_add (dir[f].name, attrib, block);
/*
	dir[y].read=0;
	dir[y].hidden=0;
	dir[y].system=0;
	dir[y].volume=0;
	dir[y].dir=0;
	dir[y].bin=0;
*/

				//printf ("add: %s to %s | %d%d%d%d%d\n", dir[f].name, block, dir[f].read, dir[f].hidden, dir[f].system, dir[f].volume, dir[f].dir, dir[f].bin);

				f ++;
				fl = strlen (dir[f].name);
			}

			return 1;
		}
		break;
		case DEV_ACT_UMOUNT:
		{
			if (!p->fs->handler (FS_ACT_EXIT, more, -1, 0))
				return 0;

			return 1;
		}
	}

	return 0;
}

bool ata_acthandler (unsigned act, char drive, char *block)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			if (kernel_attr & KERNEL_NOATA)
				return 0;

			int res = 0;

			if (lba28_drive_detect (0)) {
				/* when it's hdd, check partition table, else pre-set isofs on cdrom drive */
				int type = drive_type (0x1b);
				
				dev_t *dev = (dev_t *) dev_register (type == DRIVE_TYPE_CDROM ? "cda" : "hda", 
								     "Drive controller", DEV_ATTR_BLOCK, (dev_handler_t *) &ide_acthandler);

				if (type == DRIVE_TYPE_HDD)
					partition_table (dev);
				else if (type == DRIVE_TYPE_CDROM)
					partition_add (dev, fs_supported ("isofs"), 0);

				res ++;
			}

			if (lba28_drive_detect (1)) {
				/* when it's hdd, check partition table, else pre-set isofs on cdrom drive */
				int type = drive_type (0x24+9);

				dev_t *dev = (dev_t *) dev_register (type == DRIVE_TYPE_CDROM ? "cdb" : "hdb",
								     "Drive controller", DEV_ATTR_BLOCK, (dev_handler_t *) &ide_acthandler);

				if (type == DRIVE_TYPE_HDD)
					partition_table (dev);
				else if (type == DRIVE_TYPE_CDROM)
					partition_add (dev, fs_supported ("isofs"), 0);

				res ++;
			}

			if (lba28_drive_detect (2)) {
				/* when it's hdd, check partition table, else pre-set isofs on cdrom drive */
				int type = drive_type (0x24);
				
				dev_t *dev = (dev_t *) dev_register (type == DRIVE_TYPE_CDROM ? "cdc" : "hdc",
								     "Drive controller", DEV_ATTR_BLOCK, (dev_handler_t *) &ide_acthandler);

				if (type == DRIVE_TYPE_HDD)
					partition_table (dev);
				else if (type == DRIVE_TYPE_CDROM)
					partition_add (dev, fs_supported ("isofs"), 0);

				res ++;
			}

			if (lba28_drive_detect (3)) {
				/* when it's hdd, check partition table, else pre-set isofs on cdrom drive */
				int type = drive_type (0x24+18);
				
				dev_t *dev = (dev_t *) dev_register (type == DRIVE_TYPE_CDROM ? "cdd" : "hdd",
								     "Drive controller", DEV_ATTR_BLOCK, (dev_handler_t *) &ide_acthandler);

				if (type == DRIVE_TYPE_HDD)
					partition_table (dev);
				else if (type == DRIVE_TYPE_CDROM)
					partition_add (dev, fs_supported ("isofs"), 0);

				res ++;
			}

			return res;
		}
		break;
		/*case DEV_ACT_READ:
		{
			lba28_drive_read (drive, 0x01, n, block);
			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{
			lba28_drive_write (drive, 0x01, n, block);
			return 1;
		}*/
	}

	return 0;
}
#endif
