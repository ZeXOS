/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <stdio.h>
#include <pci.h>
#include "../drivers/bus/usb/usb.h"

void usb_controller_handler (void *r)
{
	DPRINT (DBG_DRIVER | DBG_BUS, "USB -> INT\n");
}

unsigned usb_controller_register (pcidev_t *pcidev)
{
#ifdef CONFIG_DRV_USB
	DPRINT (DBG_DRIVER | DBG_BUS, "irq: %d\nbase: 0x%x\n", pcidev->u.h0.interrupt_line, pcidev->u.h0.base_registers[5]);
	//irq_install_handler (pcidev->u.h0.interrupt_line, usb_controller_handler);
#endif
	/*unsigned i;
	for (i = 0; i < 6; i ++)
		printf ("base addr: 0x%x\n", pcidev->u.h0.base_registers[i]);*/

	return 1;
}
