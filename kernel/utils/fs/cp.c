/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <vfs.h>


int cp (unsigned char *fi, unsigned char *fo)
{
	int i = 0;

	unsigned fi_len = strlen (fi);
	unsigned fo_len = strlen (fo);

	if (!fi_len || !fo_len) {
		printf ("cat: invalid filename\n");
		return 0;
	}
	
	DPRINT (DBG_UTILS | DBG_FS, "fi: '%s' (%d)", fi, fi_len);
	DPRINT (DBG_UTILS | DBG_FS, "fo: '%s' (%d)", fo, fo_len);

	int r = vfs_cp (fi, fi_len, fo, fo_len);

	if (r == -2) {
		printf ("No such file: %s\n", fi);
		return 0;
	}

	return 1;
}
