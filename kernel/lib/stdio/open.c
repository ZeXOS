/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <stdio.h>
#include <file.h>
#include <mount.h>
#include <env.h>
#include <vfs.h>
#include <dev.h>
#include <fd.h>
#include <errno.h>
#include <cache.h>

int open (const char *pathname, int flags)
{
	fd_t *fd = fd_create (flags);

	if (!fd) {
		DPRINT (DBG_LIB | DBG_STDIO, "open () -> !fd");
		return -1; /* fd_create sets errno */
	}

	unsigned fi_len = strlen (pathname);

	if (!fi_len) {
		DPRINT (DBG_LIB | DBG_STDIO, "open () -> !fi_len");
		return 0;
	}

/*	cache_t *cache = 0;

	if (flags & O_RDONLY) {
		int ret = vfs_read ((char *) pathname, fi_len);

		if (ret < 0) {
			DPRINT (DBG_LIB | DBG_STDIO, "fd -> %d : file '%s' not found!", fd->id, pathname);
			errno_set (ENOENT);
			return -1;
		}

		i = (unsigned char) ret;
		
		cache = cache_read ();
	}
	
	if (flags & O_WRONLY) {
		fd->e = 0;
		
		unsigned y;
		for (y = 0; y < 235; y ++) {
			if (!dir[y].name[0]) {
				i = y;
				memcpy (dir[y].name, pathname, fi_len);
				dir[y].name[fi_len] = '\0';
				break;
			}
		}
		printf ("jo\n");
		cache = cache_create (0, 0, 0);
	}
	
	if (!cache) {
		errno_set (ENOMEM);
		return -1;
	}
	
	kprintf ("fd->e %d | b: %s\n", cache->limit, (char *) &cache->data);
	
	fd->e = cache->limit;

	fd->s = (char *) &cache->data;*/
	fd->e = 0;
	fd->s = 0;
	fd->p = 0;
	
	fd->path = kmalloc (fi_len + 1);
	
	if (!fd->path)
		return -1;
	
	memcpy (fd->path, pathname, fi_len);
	fd->path[fi_len] = '\0';
	
	fd->dev = dev_find ((char *) pathname);

	if (flags & O_RDONLY || flags & O_RDWR) {
		vfs_content_t content;
		int ret = vfs_read ((char *) fd->path, fi_len, &content);

		if (ret < 0) {
			DPRINT (DBG_LIB | DBG_STDIO, "fd -> %d : file '%s' not found!", fd->id, fd->path);
			errno_set (ENOENT);
			return -1;
		}
			
		fd->s = content.ptr;
		fd->e = content.len;
	}
	
	return fd->id;
}
 
