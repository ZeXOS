/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <dev.h>
#include <arch/io.h>
#include <pci.h>

#define ES1370_VENDORID 0x1274 
#define ES1370_DEVICEID 0x5000

typedef struct {
	unsigned addr_io;
	int irq;
} es1370_t;

int es1370_inb (es1370_t *dev, int port)
{	
	int value = -1;

	if ((value = inb (dev->addr_io+port)) != 0)
		DPRINT (DBG_DRIVER | DBG_SOUND, "es1370DSP: inb () failed - %d", value);
	
	return value;
}

void es1370_outb (es1370_t *dev, int port, int value)
{
	int s;
	
	outb (dev->addr_io+port, value);
}

/* detect es1370 device in PC */
pcidev_t *es1370_detect ()
{
	/* First detect sound card - is connected to PCI bus ?*/
	pcidev_t *pcidev = pcidev_find (ES1370_VENDORID, ES1370_DEVICEID);

	if (!pcidev)
		return 0;

	return pcidev;
}

unsigned init_es1370 ()
{
	pcidev_t *pcidev = es1370_detect ();

	if (!pcidev)
		return 0;

	es1370_t *dev = (es1370_t *) kmalloc (sizeof (es1370_t));

	if (!dev)
		return 0;

	/* get irq id */
	dev->irq = pcidev->u.h0.interrupt_line;
	dev->addr_io = pcidev->u.h0.base_registers[0];

	kprintf ("es1370 -> found at 0x%x, irq: %d\n", dev->addr_io, dev->irq);

	unsigned char *c = (unsigned char *) dev->addr_io;

	//es1370_outb (dev, 0x0, 1);

	//kprintf ("Prvni znak: 0x%x\n", c[0]);

	return 1;
}

bool es1370_acthandler (unsigned act, unsigned freq)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			//init_es1370 ();

			return 1;
		}
		break;
		case DEV_ACT_STOP:
		{
			

			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{
			

			return 1;
		}
		break;
	}

	return 0;
}
#endif
 
