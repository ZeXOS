/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _MATH_H
#define _MATH_H

#define E 2.7182818284590452354 //e
#define PI 3.14159265358979323846 //pi
#define PI_X2 6.28318530717958647692 //2*pi
#define PI_2 1.57079632679489661923 //pi/2
#define PI_4 0.78539816339744830962 //pi/4
#define T 0.5

#define M_PI PI

/* math function's externs */
extern double fabs (double x);
extern double fmod (double x, double y);
extern double sin (double x);
extern double cos (double x);
extern double pow (double x, double y);
extern double floor (double x);
extern double sqrt (double x);

#endif
