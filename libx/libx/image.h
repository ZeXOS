/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _IMAGE_H
#define _IMAGE_H

typedef struct xbitmap_struct              /* the structure for a bitmap. */
{
	unsigned short width;
	unsigned short height;
	unsigned char *data;
} xbitmap;

extern xbitmap *ximage_open (const char *filename);
extern void ximage_draw (xbitmap *bitmap, unsigned x, unsigned y, int transparency);
extern void ximage_draw_spec (xbitmap *bitmap, unsigned x, unsigned y, unsigned size_x, unsigned size_y, unsigned offset_x, unsigned offset_y, int transparency);

#endif
