/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _IPI_H
#define	_IPI_H

/* Local APIC registers */
#define LAPIC_REG_INT_LOW		0xc0
#define LAPIC_REG_INT_HIGH		0xc4

/* Local APIC int vectors */
#define LAPIC_IV_TIMER			0xe0
#define LAPIC_IV_LINT0			0xe1
#define LAPIC_IV_LINT1			0xe2
#define LAPIC_IV_ERROR			0xe3
#define LAPIC_IV_SPURIOUS		0xef

/* Local APIC register's flags */
#define LAPIC_REG_FLAG_WRITE		0x4000

/* Interprocessor interrupt types */
#define IPI_TYPE_FIXED			0x0
#define IPI_TYPE_INIT			0x500
#define IPI_TYPE_SIPI			0x600

extern unsigned ipi_lapic_init (unsigned addr);

#endif
