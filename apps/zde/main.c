/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>

#include "dialog.h"
#include "window.h"
#include "cursor.h"
#include "appsrv.h"
#include "button.h"
#include "icon.h"
#include "kbd.h"
#include "bg.h"

#define		ESC				1
#define		ARROWLEFT			75
#define		ARROWRIGHT			77
#define		ARROWUP				72
#define		ARROWDOWN			80

int lock;

int zde_drawlock ()
{
	lock = 1;

	return 0;
}

int zde_drawunlock ()
{
	lock = 0;

	return 0;
}

int error_print (char *err)
{
	printf ("ERROR - ZDE -> init of %s failed !\n", err);

	return 0;
}

int init_zde ()
{
	if (init_background ())
		return error_print ("background");
	if (init_cursor ())
		return error_print ("cursor");
	if (init_icon ())
		return error_print ("icon");
	if (init_button ())
		return error_print ("button");
	if (init_dialog ())
		return error_print ("dialog");
	if (init_window ())
		return error_print ("window");
	if (init_appsrv ())
		return error_print ("appsrv");
	if (init_kbd ())
		return error_print ("kbd");
	
	return 1;
}

int draw_zde ()
{
	draw_background ();
	draw_icon_desktop ();
	draw_window ();
	draw_cursor ();

	return 0;
}

int main (int argc, char **argv)
{
	/* Switch to VGA graphics mode */
	if (!xinit ()) {
		printf ("Something is already using VGA !\n");
		return -1;
	}
	
	/* Clean screen */
	xcls (0);

	if (!init_zde ())
		return -1;

	zde_drawunlock ();

	for (;;) {
		kbd_update ();
		draw_zde ();

		if (!lock)
			xfbswap ();
	}

	/* Go back to textual mode */
	xexit ();

	/* Exit app */
	return 0;
}
