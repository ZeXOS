/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Based on JamesM's paging tutorial
 */

#include <arch/io.h>
#include <system.h>
#include <paging.h>
#include <config.h>
#include <task.h>

extern unsigned *code, *bss, *data, *end;

// A bitset of frames - used or free.
static unsigned page_nframes;

// Amount of RAM Memory in Bytes
static unsigned long mem_end_page;

// Static function to set a bit in the frames bitset
static void page_set_frame (page_ent_t *page_cover, unsigned frame_addr)
{
	unsigned frame = frame_addr / 0x1000;
	unsigned idx = index_from_bit (frame);
	unsigned off = offset_from_bit (frame);
	page_cover->frames[idx] |= (0x1 << off);
}

// Static function to clear a bit in the frames bitset
static void page_clear_frame (page_ent_t *page_cover, unsigned frame_addr)
{
	unsigned frame = frame_addr / 0x1000;
	unsigned idx = index_from_bit (frame);
	unsigned off = offset_from_bit (frame);
	page_cover->frames[idx] &= ~(0x1 << off);
}

// Static function to test if a bit is set.
static unsigned page_test_frame (page_ent_t *page_cover, unsigned frame_addr)
{
	unsigned frame = frame_addr / 0x1000;
	unsigned idx = index_from_bit (frame);
	unsigned off = offset_from_bit (frame);

	return (page_cover->frames[idx] & (0x1 << off));
}

// Static function to find the first free frame.
static unsigned page_first_frame (page_ent_t *page_cover)
{
	unsigned i, j;
	for (i = 0; i < index_from_bit (page_nframes); i++) {
		// nothing free, exit early.
		if (page_cover->frames[i] != ~0) {
			// at least one bit is free here.
			for (j = 0; j < 32; j++) {
				unsigned toTest = 0x1 << j;

				if (!(page_cover->frames[i] & toTest))
					return (i * 4 * 8 + j);
			}
		}
	}

	return 0;
}

// Function to allocate a frame.
void page_alloc_frame (page_ent_t *page_cover, page_t *page, int supervisor, int rw)
{
	if (page->frame != 0)
		return; // Frame was already allocated, return straight away.
	else {
		unsigned idx = page_first_frame (page_cover); // idx is now the index of the first free frame.

		if (idx == (unsigned) -1) {
			// PANIC is just a macro that prints a message to the screen then hits an infinite loop.
			kprintf ("No free frames!");
		}

		page_set_frame (page_cover, idx * 0x1000); // this frame is now ours!
		page->present = 1; // Mark it as present.
		page->rw = (rw) ? 1 : 0; // Should the page be writeable?
		page->user = (supervisor) ? 0 : 1; // Should the page be user-mode?
		page->frame = idx;
	}
}

// Function to deallocate a frame.
void page_free_frame (page_ent_t *page_cover, page_t *page)
{
	unsigned frame;
	if (!(frame = page->frame)) {
		return; // The given page didn't actually have an allocated frame!
	} else {
		page_clear_frame (page_cover, frame); // Frame is now free again.
		page->frame = 0x0; // Page now doesn't have a frame.
	}
}

void page_dir_switch (page_dir_t *page_dir)
{
#ifdef CONFIG_MEM_PAGING
	page_cover_curr = page_dir;

	write_cr3 (&page_dir->tables_phys); // put that page directory address into CR3
#endif
}

page_t *page_get (page_dir_t *dir, unsigned address, int make)
{
	// Turn the address into an index.
	address /= 0x1000;
	// Find the page table containing this address.
	unsigned table_idx = address / 1024;

	// If this table is already assigned
	if (dir->tables[table_idx])
		return &dir->tables[table_idx]->pages[address % 1024];
	else if (make) {
		unsigned tmp;
		dir->tables[table_idx] = (page_table_t *) pmalloc_ext (sizeof (page_table_t), (unsigned *) &tmp);

		memset (dir->tables[table_idx], 0, 0x1000);
		dir->tables_phys[table_idx] = tmp | 0x7; // PRESENT, RW, US.

		return &dir->tables[table_idx]->pages[address % 1024];
	}
	
	return 0;
}

page_ent_t *page_cover_create ()
{
#ifdef CONFIG_MEM_PAGING
	/* Alloc memory for new page_ent_t * structure */
	page_ent_t *page_cover = (page_ent_t *) kmalloc (sizeof (page_ent_t));

	if (!page_cover)
		return 0;

	// Alloc page aligned memory for new page directory
	page_cover->page_dir = (page_dir_t *) pmalloc (sizeof (page_dir_t));

	if (!page_cover->page_dir) {
		kfree (page_cover);
		return 0;
	}

	memset (page_cover->page_dir, 0, sizeof (page_dir_t));

	page_cover->frames = (unsigned *) kmalloc (index_from_bit (page_nframes));

	if (!page_cover->frames) {
		pfree (page_cover->page_dir);
		kfree (page_cover);
		return 0;
	}

	memset (page_cover->frames, 0, index_from_bit (page_nframes));

	return page_cover;
#else
	return 0;
#endif
}

unsigned page_mmap (page_ent_t *page_cover, void *from, void *to, unsigned kernel, unsigned writeable)
{
#ifdef CONFIG_MEM_PAGING
	if (!page_cover)
		return 0;

	page_cover_curr = page_cover->page_dir;

	/* There is start address, where we begin mapping */
	unsigned i = (unsigned) from;
	
	/* Loop for increase next 4kB of memory from start */
	while (i < ((unsigned) to) ) {
		page_alloc_frame (page_cover, page_get (page_cover->page_dir, i, 1), kernel, writeable);
		i += 0x1000;
	}
#endif
	return 1;
}

unsigned page_fault (struct regs *r)
{
	// A page fault has occurred.
	// The faulting address is stored in the CR2 register.
	unsigned faulting_address;
	//asm volatile ("movl %%cr2, %0" : "=r" (faulting_address));
	faulting_address = 0;
	// The error code gives us details of what happened.
	int present = !(r->err_code & 0x1);   // Page not present
	int rw = r->err_code & 0x2;           // Write operation?
	int us = r->err_code & 0x4;           // Processor was in user-mode?
	int reserved = r->err_code & 0x8;     // Overwritten CPU-reserved bits of page entry?
	int id = r->err_code & 0x10;          // Caused by an instruction fetch?

	settextcolor (14, 0);

   	// Output an error message.
 	kprintf ("\nERROR -> page flags: ");

   	if (present)
		kprintf ("present ");
   	if (rw)
		kprintf ("read-only ");
   	if (us)
		kprintf ("user-mode ");
   	if (reserved)
		kprintf ("reserved ");
 
	kprintf ("\n\tFault at 0x%x address\n", faulting_address);

	return proc_page_fault ();
}

unsigned paging_enable ()
{
#ifdef CONFIG_MEM_PAGING
	unsigned long flags = read_cr0 ();

	if (flags & PAGING_BIT)
		return 0;

	write_cr0 (flags | PAGING_BIT); // set the paging bit in CR0 to 1
#endif
	return 1;
}

unsigned paging_disable ()
{
#ifdef CONFIG_MEM_PAGING
	unsigned long flags = read_cr0 ();

	if (!(flags & PAGING_BIT))
		return 0;

	flags &= ~PAGING_BIT;

	write_cr0 (flags); // set the paging bit in CR0 to 0
#endif
}

unsigned int init_paging ()
{
#ifdef CONFIG_MEM_PAGING
	// The size of physical memory.
	mem_end_page = mem_ext * 1024 * 1024;
	
	page_nframes = mem_end_page / 0x1000;
#endif

	return 1;
}

