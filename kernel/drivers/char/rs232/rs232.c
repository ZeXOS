/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <config.h>
#include <system.h>
#include <arch/io.h>
#include <rs232.h>

unsigned int init_rs232 ()
{
#ifdef ARCH_i386
	int baud = CONFIG_DRV_RS232BAUDS;	// Set to 9600 baud

	unsigned divisor = (unsigned) (115200L / baud);
	
	outb (PORT + 1, 0x00);    // Disable all interrupts
	outb (PORT + 4, 0x00);    // Disable all interrupts

	outb (PORT + 3, 0x80);    // Enable DLAB (set baud rate divisor)
	outb (PORT + 0, divisor & 0xff);    // Set divisor to 3 (lo byte) 38400 baud
	outb (PORT + 1, divisor >> 8);    //                  (hi byte)
	outb (PORT + 3, 0x02);    // 8 bits, no parity, one stop bit
	outb (PORT + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
	outb (PORT + 4, 0x03);    // IRQs enabled, RTS/DSR set
#endif
#ifdef ARCH_arm

#endif
	return 1;
}

int serial_recieved()
{
#ifdef ARCH_i386
	return inb (PORT + 5) & 1;
#endif
}

char rs232_read_nonblock ()
{
#ifdef ARCH_i386
	int r = serial_recieved ();

	if (r == 0)
		return 0;

   	return inb (PORT);
#endif
#ifdef ARCH_arm
	return (char) armbd_uart_read (0);
#endif
}


char rs232_read ()
{
#ifdef ARCH_i386
	while (serial_recieved () == 0)
		schedule ();

   	return inb (PORT);
#endif
#ifdef ARCH_arm
	if (armbd_uart_received (0))
		return (char) armbd_uart_read (0);
	else
		return 0;
	
#endif
}

#ifdef ARCH_i386
int is_transmit_empty ()
{
	return inb (PORT + 5) & 0x20;
}
#endif

void rs232_write (char a)
{
#ifdef ARCH_i386
	while (is_transmit_empty() == 0) {
		schedule ();
		continue;
	}

	outb (PORT, a);
#endif

#ifdef ARCH_arm
	armbd_uart_write (0, (unsigned char) a);
#endif
}

bool rs232_acthandler (unsigned act, char *block, unsigned block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			init_rs232 ();

			return 1;
		}
		break;
		case DEV_ACT_READ:
		{
			unsigned len = 0;

			while (len != block_len) {
				block[len] = rs232_read ();
				len ++;
			}

			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{
			unsigned len = 0;

			while (len != block_len) {
				rs232_write (block[len]);
				len ++;
			}

			return 1;
		}
		break;
	}

	return 0;
}
