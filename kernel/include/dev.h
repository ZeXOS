/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DEV_H
#define _DEV_H

#define DEFAULT_MAX_DEVNLENGTH		15
#define	DEFAULT_MAX_DESCLENGTH		20

#define	DEV_ACT_INIT		0x1
#define	DEV_ACT_READ		0x2
#define	DEV_ACT_WRITE		0x4
#define	DEV_ACT_RESET		0x8
#define	DEV_ACT_STOP		0x10
#define	DEV_ACT_MOUNT		0x20
#define	DEV_ACT_PLAY		0x20
#define	DEV_ACT_UPDATE		0x40
#define	DEV_ACT_UMOUNT		0x80

#define DEV_ATTR_BLOCK		0x1
#define DEV_ATTR_CHAR		0x2
#define DEV_ATTR_SPECIAL	0x4
#define DEV_ATTR_NET		0x8
#define DEV_ATTR_SOUND		0x10

typedef bool (dev_handler_t) (unsigned act, ...);

/* Device structure */
typedef struct dev_context {
	struct dev_context *next, *prev;

	char devname[DEFAULT_MAX_DEVNLENGTH];
	char desc[DEFAULT_MAX_DESCLENGTH];
	unsigned attrib;
	dev_handler_t *handler;
} dev_t;

typedef struct {
	void *iomem;
	unsigned iolen;
} dev_flags_t;

/* externs */
extern void dev_display ();
extern dev_t *dev_find (char *devname);
extern bool dev_handler (dev_t *dev, unsigned act, ...);
extern dev_t *dev_register (char *devname, char *desc, unsigned attrib, dev_handler_t *handler);
extern unsigned int init_dev ();

#endif

