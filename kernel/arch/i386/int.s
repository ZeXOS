;  ZeX/OS
;  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
;
;  This program is free software: you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this program.  If not, see <http://www.gnu.org/licenses/>.


%macro	IMP 1
%ifdef UNDERBARS
	EXTERN _%1
	%define %1 _%1
%else
	EXTERN %1
%endif
%endmacro

%macro	EXP	1
	GLOBAL $_%1
	$_%1:
	GLOBAL $%1
	$%1:
%endmacro

; IMPORTS
; from KRNL.LD
IMP bss
IMP end

; from MAIN.C
IMP g_curr_thread
IMP fault
IMP main

[BITS 32]
SECTION .text
; This will set up our new segment registers. We need to do
; something special in order to set CS. We do what is called a
; far jump. A jump that includes a segment as well as an offset.
; This is declared in C as 'extern void gdt_flush();'

EXP gdt_flush
IMP gp            ; Says that '_gp' is in another file
;_gdt_flush:
    lgdt [gp]        ; Load the GDT with our '_gp' which is a special pointer
    mov ax, 0x10      ; 0x10 is the offset in the GDT to our data segment
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    jmp 0x08:flush2   ; 0x08 is the offset to our code segment: Far jump!
flush2:
    ret               ; Returns back to the C code!

EXP gdt_smp
IMP gp
    lgdt [gp]        ; Load the GDT with our '_gp' which is a special pointer
    jmp 0x08:ccode   ; 0x08 is the offset to our code segment: Far jump!
ccode:
    ret


; Loads the IDT defined in '_idtp' into the processor.
; This is declared in C as 'extern void idt_load();'
EXP idt_load
IMP idtp
    lidt [idtp]
    ret

;  0: Divide By Zero Exception
EXP isr0
    cli
    push byte 0
    push byte 0
    jmp isr_common_stub

;  1: Debug Exception
EXP isr1
    cli
    push byte 0
    push byte 1
    jmp isr_common_stub

;  2: Non Maskable Interrupt Exception
EXP isr2
    cli
    push byte 0
    push byte 2
    jmp isr_common_stub

;  3: Int 3 Exception
EXP isr3
    cli
    push byte 0
    push byte 3
    jmp isr_common_stub

;  4: INTO Exception
EXP isr4
    cli
    push byte 0
    push byte 4
    jmp isr_common_stub

;  5: Out of Bounds Exception
EXP isr5
    cli
    push byte 0
    push byte 5
    jmp isr_common_stub

;  6: Invalid Opcode Exception
EXP isr6
    cli 
    push byte 0
    push byte 6
    jmp isr_common_stub

;  7: Coprocessor Not Available Exception
EXP isr7
    cli
    push byte 0
    push byte 7
    jmp isr_common_stub

;  8: Double Fault Exception (With Error Code!)
EXP isr8
    cli
    push byte 8
    jmp isr_common_stub

;  9: Coprocessor Segment Overrun Exception
EXP isr9
    cli
    push byte 0
    push byte 9
    jmp isr_common_stub

; 10: Bad TSS Exception (With Error Code!)
EXP isr10
    cli
    push byte 10
    jmp isr_common_stub

; 11: Segment Not Present Exception (With Error Code!)
EXP isr11
    cli
    push byte 11
    jmp isr_common_stub

; 12: Stack Fault Exception (With Error Code!)
EXP isr12
    cli
    push byte 12
    jmp isr_common_stub

; 13: General Protection Fault Exception (With Error Code!)
EXP isr13
    cli
    push byte 13
    jmp isr_common_stub

; 14: Page Fault Exception (With Error Code!)
EXP isr14
    cli
    push byte 14
    jmp isr_common_stub

; 15: Reserved Exception
EXP isr15
    cli
    push byte 0
    push byte 15
    jmp isr_common_stub

; 16: Floating Point Exception
EXP isr16
    cli
    push byte 0
    push byte 16
    jmp isr_common_stub

; 17: Alignment Check Exception
EXP isr17
    cli
    push byte 0
    push byte 17
    jmp isr_common_stub

; 18: Machine Check Exception
EXP isr18
    cli
    push byte 0
    push byte 18
    jmp isr_common_stub

; 19: Reserved
EXP isr19
    cli
    push byte 0
    push byte 19
    jmp isr_common_stub

; 20: Reserved
EXP isr20
    cli
    push byte 0
    push byte 20
    jmp isr_common_stub

; 21: Reserved
EXP isr21
    cli
    push byte 0
    push byte 21
    jmp isr_common_stub

; 22: Reserved
EXP isr22
    cli
    push byte 0
    push byte 22
    jmp isr_common_stub

; 23: Reserved
EXP isr23
    cli
    push byte 0
    push byte 23
    jmp isr_common_stub

; 24: Reserved
EXP isr24
    cli
    push byte 0
    push byte 24
    jmp isr_common_stub

; 25: Reserved
EXP isr25
    cli
    push byte 0
    push byte 25
    jmp isr_common_stub

; 26: Reserved
EXP isr26
    cli
    push byte 0
    push byte 26
    jmp isr_common_stub

; 27: Reserved
EXP isr27
    cli
    push byte 0
    push byte 27
    jmp isr_common_stub

; 28: Reserved
EXP isr28
    cli
    push byte 0
    push byte 28
    jmp isr_common_stub

; 29: Reserved
EXP isr29
    cli
    push byte 0
    push byte 29
    jmp isr_common_stub

; 30: Reserved
EXP isr30
    cli
    push byte 0
    push byte 30
    jmp isr_common_stub

; 31: Reserved
EXP isr31
    cli
    push byte 0
    push byte 31
    jmp isr_common_stub


; We call a C function in here. We need to let the assembler know
; that '_fault_handler' exists in another file


; This is our common ISR stub. It saves the processor state, sets
; up for kernel mode segments, calls the C-level fault handler,
; and finally restores the stack frame.
isr_common_stub:
IMP fault_handler
    pusha
    push ds
    push es
    push fs
    push gs
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, esp
    push eax
    mov eax, fault_handler
    call eax
    pop eax
    pop gs
    pop fs
    pop es
    pop ds
    popa
    add esp, 8
    iret

;global _irq0
;global _irq1
;global _irq2
;global _irq3
;global _irq4
;global _irq5
;global _irq6
;global _irq7
;global _irq8
;global _irq9
;global _irq10
;global _irq11
;global _irq12
;global _irq13
;global _irq14
;global _irq15

; 32: IRQ0
EXP irq0
    cli
    push byte 0
    push byte 32
    jmp irq_common_stub

; 33: IRQ1
EXP irq1
    cli
    push byte 0
    push byte 33
    jmp irq_common_stub

; 34: IRQ2
EXP irq2
    cli
    push byte 0
    push byte 34
    jmp irq_common_stub

; 35: IRQ3
EXP irq3
    cli
    push byte 0
    push byte 35
    jmp irq_common_stub

; 36: IRQ4
EXP irq4
    cli
    push byte 0
    push byte 36
    jmp irq_common_stub

; 37: IRQ5
EXP irq5
    cli
    push byte 0
    push byte 37
    jmp irq_common_stub

; 38: IRQ6
EXP irq6
    cli
    push byte 0
    push byte 38
    jmp irq_common_stub

; 39: IRQ7
EXP irq7
    cli
    push byte 0
    push byte 39
    jmp irq_common_stub

; 40: IRQ8
EXP irq8
    cli
    push byte 0
    push byte 40
    jmp irq_common_stub

; 41: IRQ9
EXP irq9
    cli
    push byte 0
    push byte 41
    jmp irq_common_stub

; 42: IRQ10
EXP irq10
    cli
    push byte 0
    push byte 42
    jmp irq_common_stub

; 43: IRQ11
EXP irq11
    cli
    push byte 0
    push byte 43
    jmp irq_common_stub

; 44: IRQ12
EXP irq12
    cli
    push byte 0
    push byte 44
    jmp irq_common_stub

; 45: IRQ13
EXP irq13
    cli
    push byte 0
    push byte 45
    jmp irq_common_stub

; 46: IRQ14
EXP irq14
    cli
    push byte 0
    push byte 46
    jmp irq_common_stub

; 47: IRQ15
EXP irq15
    cli
    push byte 0
    push byte 47
    jmp irq_common_stub

IMP irq_handler

irq_common_stub:
    pusha
    push ds
    push es
    push fs
    push gs

    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov eax, esp

    push eax
    mov eax, irq_handler
    call eax
    pop eax

    pop gs
    pop fs
    pop es
    pop ds
    popa
    add esp, 8
    iret

;extern testf
;global testf_c

;testf_c:
;	mov ecx, testf
;	jmp syscall0

; syscall with 0 args

;IMP syscall_handler
;EXP system_call
;	push ds				; switch to kernel space
;	push es
;	mov eax, 0x10
;	mov ds, eax
;	mov es, eax
	
;	call syscall_handler			; make call
	
;	pop es				; back to userland
;	pop ds	
;	retf 0 
;	iret

IMP syscall_handler
EXP system_call
	pusha
	push ds
	push es
	push fs
	push gs
	
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

;	mov eax, esp

;	push edx
;	push ecx
;	push ebx
	push esp

	call syscall_handler			; make call

	add esp, 4
;	pop eax
;	pop ebx
;	pop ecx
;	pop edx

	pop gs
	pop fs
	pop es
	pop ds

	popa
	
	iret


;IMP syscall_handler
;EXP system_call
;    pusha
;    push ds
;    push es
;    push fs
;    push gs
;    push edx
;    push ecx
;    push ebx

;    mov ax, 0x10
;    mov ds, ax
;    mov es, ax
;    mov fs, ax
;    mov gs, ax
;    mov eax, esp

;    push eax
;    mov eax, syscall_handler
;    call eax

;    pop eax
;    pop ebx
;    pop ecx
;    pop edx
;    pop gs
;    pop fs
;    pop es
;    pop ds
;    popa
    ;add esp, 8
;    iret

EXP prot_to_real
IMP gp
pmode32:  mov     ax,[gp]    ; now we are in protected mode
        mov     ds,ax           ; set all selector limits to 4 GB
        mov     es,ax
        mov     fs,ax
        mov     gs,ax

        mov     eax,cr0         ; restore real mode
        and     al,0feh
        mov     cr0,eax

        ;db      0eah            ; far jump to rmode label
        ;dw      offset rmode
        ;dw      Code
	;jmp far [rmode_ip]
	clc
	sti
	ret

;rmode:  clc                     ; now we are back in real mode, zero carry
 ;       sti                     ; to indicate ok and enable interrupts

;leave4gb:
 ;       ret

;rmode_ip:
;	dw rmode
;rmode_cs:
;	dw 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; return to real mode procedure, modified from section 14.5 of 386INTEL.TXT
; (...continued from below...)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; 4. load SS with selector to descriptor that is "appropriate for real mode":
;	Limit = 64K (FFFFh)	Byte-granular (G=0)
;	Expand-up (E=0)		Writable (W=1)
;	Present (P=1)		Base address = any value
; You may leave the other data segment registers with limits >64K if you
; want 'unreal mode', otherwise load them with a similar selector.

pmode16:
	mov ax,_16BIT_DATA_SEL
	mov ss,ax
	mov ds,ax
	mov es,ax
	mov fs,ax
	mov gs,ax

; 5. Clear the PE bit in register CR0
	mov eax,cr0
	and al,0FEh
	mov cr0,eax

; 6. Jump to a 16:16 real-mode far address
	jmp far [_real_ip]
real:

; 7. Load all other segment registers (SS, DS, ES, FS, GS)
	mov ax,cs
	mov ss,ax
	mov ds,ax
	mov es,ax
	mov fs,ax
	mov gs,ax

; 8. Use the LIDT instruction to load an IDT appropriate for real mode,
; with base address = 0 and limit = 3FFh. Use the 32-bit operand size
; override prefix so all 32 bits of the IDT base are set (otherwise,
; only the bottom 24 bits will be set).
	o32 lidt [_real_idt]

; 9. Zero the high 16 bits of 32-bit registers. If the register value is
; not important, just zero the entire 32-bit register, otherwise use 'movzx'
	xor eax,eax
	xor ebx,ebx
	xor ecx,ecx
	xor edx,edx
	xor esi,esi
	xor edi,edi
	movzx ebp,bp
	movzx esp,sp

; 10. Enable interrupts
	sti
	jmp $

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; pmode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ALIGN 512 ; so NDISASM knows where the 32-bit code starts

BITS 32
GLOBAL pmode ; so "objdump --source ..." can find it
pmode:
	mov ax,KRNL_DATA_SEL
	mov ds,ax
	mov ss,ax
	mov es,ax
	mov fs,ax
	mov gs,ax

; LTR is illegal in real mode, so do it now
	mov ax,TSS_SEL
	ltr ax

; zero the kernel BSS
	xor eax,eax
	mov edi,bss		; BSS starts here
	mov ecx,end
	sub ecx,edi		; BSS size is (g_end - g_bss) bytes
	cld
	rep stosb

; call C code
	call main

; return to real mode procedure, modified from section 14.5 of 386INTEL.TXT
; 1. Disable interrupts (already done)
; 2. If paging is enabled... (it isn't)
; 3. Jump to a segment with limit 64K-1. CS must have this limit
; before you return to real mode.

	jmp _16BIT_CODE_SEL:pmode16


EXP start_v86

; save registers used by C: EBP, EBX, ESI, EDI
; Actually, just use PUSHA to save them all
	pusha

; copy V86 mode registers to top of stack
	sub esp,92
		mov esi,[esp + 128] ; (uregs_t *regs)
		mov edi,esp
		mov ecx,92 ; sizeof(uregs_t)
		cld
		rep movsb
		jmp short to_user

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; name:		all_ints
; action:	unified IRQ/interrupt/exception handler
; in:		IRET frame, error code, and exception number
; 		all pushed on stack
; out:		(nothing)
; modifies:	(nothing)
; notes:	prototype for C-language interrupt handler:
; 		int fault(uregs_t *regs);
;
; *** WARNING: The order in which registers are pushed and popped here
; must agree with the layout of the uregs_t structure in the C code.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

all_ints:
	push gs			; push segment registers
	push fs
	push ds
	push es
	pusha			; push EDI...EAX (7 regs; 8 dwords)
		mov ax,KRNL_DATA_SEL
		mov ds,eax	; put known-good values in segment registers
		mov es,eax
		mov fs,eax
		mov gs,eax
		push esp	; push pointer to stacked registers
			call fault
		add esp,4
		or eax,eax
		jne end_v86

; *** FALL-THROUGH to to_user ***

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; name:		to_user
; action:	kernel exit code
; in:		uregs_t frame on kernel stack
; out:		(nothing)
; modifies:	(nothing)
; notes:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to_user:

; IRET to Ring 3 pmode will pop these values
; 8 GP regs, 4 seg regs, int_num/err_code, EIP/CS/EFLAGS/ESP/SS
; making the kernel stack 76 bytes smaller
		lea eax,[esp + 76]
; IRET to V86 mode...
		;test dword [esp + 64],20000h ; test VM bit in stacked EFLAGS
		test byte [esp + 66],2
		je set_tss_esp0

; ...will pop these values
; 8 GP regs, 4 seg regs, int_num/err_code, EIP/CS/EFLAGS/ESP/SS/ES/DS/FS/GS
; making the kernel stack 92 bytes smaller
		lea eax,[esp + 92]

; For IRET to Ring 0 pmode, _tss_esp0 is not used,
; so this value doesn't matter
set_tss_esp0:
		mov [_tss_esp0],eax

	popa			; pop EAX...EDI (7 regs; 8 dwords)
	pop es			; pop segment registers
	pop ds
	pop fs
	pop gs

	add esp,byte 8		; drop int_num and err_code
	
	iret			; pop EIP...GS (9 regs)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; name:		end_v86
; action:	cleans up after virtual 8086 mode session;
;		returns to pmode kernel
; in:		(see start_v86, above)
; out:		registers in uregs_t may be modified
; modifies:	registers in uregs_t may be modified
; notes:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

end_v86:

; get modified V86 mode registers
		mov esi,esp
		mov edi,[esp + 128]
		mov ecx,92
		cld
		rep movsb

; remove V86 registers from stack
		add esp,92

; restore C registers and return to pmode kernel
	popa
	ret



;    We need it for halt pc
EXP halt
	hlt
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SECTION .data

_cpu_msg:
	db "32-bit CPU required", 10, 13, 0

_v86_msg:
	db "CPU already in Virtual-8086 mode "
	db "(Windows DOS box or EMM386 loaded?)"
	db 10, 13, 0

_real_idt:
	dw 1023
	dd 0
_dos:
	db 0

; 16:16 far address for return to real mode. Don't change the order
; of '_real_ip' and '_real_cs', and don't put anything between them,
; because these two values are the operand of a far JMP
_real_ip:
	;dw real
_real_cs:
	dw 0

EXP g_kvirt_to_phys
	dd 0

;;;;;;;;;;;;;
;;;; TSS ;;;;
;;;;;;;;;;;;;

	ALIGN 4

; most of the TSS is unused
; I need only ESP0, SS0, and the I/O permission bitmap
_tss:
	dw 0, 0			; back link
_tss_esp0:
	dd 0			; ESP0
	dw KRNL_DATA_SEL, 0	; SS0, reserved

	dd 0			; ESP1
	dw 0, 0			; SS1, reserved

	dd 0			; ESP2
	dw 0, 0			; SS2, reserved

	dd 0			; CR3
	dd 0, 0			; EIP, EFLAGS
	dd 0, 0, 0, 0		; EAX, ECX, EDX, EBX
	dd 0, 0, 0, 0		; ESP, EBP, ESI, EDI
	dw 0, 0			; ES, reserved
	dw 0, 0			; CS, reserved
	dw 0, 0			; SS, reserved
	dw 0, 0			; DS, reserved
	dw 0, 0			; FS, reserved
	dw 0, 0			; GS, reserved
	dw 0, 0			; LDT, reserved
	dw 0, g_tss_iopb - _tss	; debug, IO permission bitmap base
EXP g_tss_iopb

; no I/O permitted
	times 8192 db 0FFh

; The TSS notes in section 12.5.2 of volume 1 of
; "IA-32 Intel Architecture Software Developer's Manual"
; are confusing as hell. I think they mean simply that the IOPB
; must contain an even number of bytes; so pad here if necessary.
	;dw 0FFFFh
EXP g_tss_end

;;;;;;;;;;;;;
;;;; GDT ;;;;
;;;;;;;;;;;;;

_gdt:
	dd 0
	dd 0

TSS_SEL		EQU ($ - _gdt)
_gdt_tss:
	dw g_tss_end - _tss - 1
	dw 0
	db 0
	db 89h			; ring 0 available 32-bit TSS
	db 0
	db 0

KRNL_DATA_SEL	EQU ($ - _gdt)
_gdt_krnl_ds:
	dw 0FFFFh
	dw 0			; base; gets set above
	db 0
	db 92h			; present, ring 0, data, expand-up, writable
	db 0CFh
	db 0

KRNL_CODE_SEL	EQU ($ - _gdt)
_gdt_krnl_cs:
	dw 0FFFFh
	dw 0			; base; gets set above
	db 0
	db 9Ah			; present, ring 0, code, non-conforming, readable
	db 0CFh			; 32-bit
	db 0

USER_DATA_SEL	EQU (($ - _gdt) | 3) ; RPL=3 (ring 3)
_gdt_user_ds:
	dw 0FFFFh
	dw 0			; base
	db 0
	db 0F2h			; present, ring 3, data, expand-up, writable
	db 0CFh
	db 0

USER_CODE_SEL	EQU (($ - _gdt) | 3)
_gdt_user_cs:
	dw 0FFFFh
	dw 0			; base; gets set above
	db 0
	db 0FAh			; present, ring 3, code, non-conforming, readable
	db 0CFh			; 32-bit
	db 0

LINEAR_DATA_SEL	EQU ($ - _gdt)
	dw 0FFFFh
	dw 0
	db 0
	db 92h			; present, ring 0, data, expand-up, writable
	db 0CFh
	db 0

LINEAR_CODE_SEL	EQU ($ - _gdt)
	dw 0FFFFh
	dw 0
	db 0
	db 9Ah			; present, ring 0, code, non-conforming, readable
	db 0CFh
	db 0

_16BIT_DATA_SEL	EQU ($ - _gdt)
_gdt_16bit_ds:
	dw 0FFFFh
	dw 0			; base; gets set above
	db 0
	db 92h			; present, ring 0, data, expand-up, writable
	db 0
	db 0

_16BIT_CODE_SEL	EQU ($ - _gdt)
_gdt_16bit_cs:
	dw 0FFFFh
	dw 0			; base; gets set above
	db 0
	db 9Ah			; present, ring 0, code, non-conforming, readable
	db 0			; 16-bit
	db 0
_gdt_end:

_gdt_ptr:
	dw _gdt_end - _gdt - 1	; GDT limit
	dd _gdt			; linear adr of GDT; gets set above

;;;;;;;;;;;;;
;;;; IDT ;;;;
;;;;;;;;;;;;;

_idt:
; 48 ring 0 interrupt gates
	times 48	dw 0, KRNL_CODE_SEL, 8E00h, 0

; one ring 3 interrupt gate for syscalls (INT 30h)
	dw 0			; offset 15:0
	dw KRNL_CODE_SEL	; selector
	db 0			; (always 0 for interrupt gates)
	db 0EEh			; present, ring 3, '386 interrupt gate
	dw 0			; offset 31:16
_idt_end:

_idt_ptr:
	dw _idt_end - _idt - 1	; IDT limit
	dd _idt			; linear, physical address of IDT (patched)
