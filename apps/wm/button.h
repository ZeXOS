/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _BUTTON_H
#define _BUTTON_H

#define BUTTON_FLAG_CLICKED	0x1
#define BUTTON_FLAG_INVISIBLE	0x2
#define BUTTON_FLAG_DISABLED	0x4

typedef unsigned (wmbutton_handler) ();

typedef struct wmbutton_context {
	struct wmbutton_context *next, *prev;

	wmbutton_handler *handler;
	unsigned x;
	unsigned y;
	unsigned size_x;
	unsigned size_y;
	unsigned char flags;
	char *caption;
} wmbutton;

/* externs */
extern wmbutton *button_create (unsigned x, unsigned y, unsigned size_x, unsigned size_y, unsigned char flags, const char *caption);
extern unsigned button_sethandler (wmbutton *button, wmbutton_handler *handler);
extern unsigned char button_flags (wmbutton *button);
extern unsigned button_draw (wmbutton *button);
extern unsigned button_draw_all ();
extern unsigned init_button ();

#endif
