/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PROC_H
#define _PROC_H

#include <tty.h>

#define PROC_USER_PRIORITY	128

#define PROC_FLAG_DAEMON	0x1

/* Process id */
typedef unsigned pid_t;

/* Process structure */
typedef struct proc_context {
	struct proc_context *next, *prev;
	
	task_t *task;
	pid_t pid;
	tty_t *tty;
	char name[12];

	unsigned argc;
	char **argv;

	unsigned short flags;
	
	unsigned start;
	unsigned code;
	unsigned data;
	unsigned data_off;
	unsigned bss;
	unsigned end;
	unsigned heap;
} proc_t;

/* externs */
extern proc_t proc_kernel;

extern void proc_display ();
extern proc_t *proc_find (task_t *task);
extern proc_t *proc_findbypid (pid_t pid);
extern proc_t *proc_create (tty_t *tty, char *name, unsigned entry);
extern bool proc_done (proc_t *proc);
extern bool proc_signal (proc_t *proc, unsigned signal);
extern bool proc_arg_set (proc_t *proc, char *arg, unsigned argl);
extern bool proc_arg_free (proc_t *proc);
extern bool proc_arg_get (proc_t *proc, unsigned *argc, char **argv);
extern unsigned proc_page_fault ();
extern unsigned proc_vmem_map (proc_t *proc);
extern unsigned proc_vmem_unmap (proc_t *proc);
extern unsigned proc_thread_create (void *entry, void *arg);
extern unsigned proc_thread_destroy (proc_t *proc, task_t *task);
extern unsigned int init_proc (void);

#endif
