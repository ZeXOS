/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int mkstemp (char *tmplate)
{
	static const char letters[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	int len, r;
	char *c;

	errno = 0;

	len = strlen (tmplate);

	if (len >= 6) {
		c = tmplate + len - 6;
		srand((unsigned int) time (0));

		if (strncmp (c, "XXXXXX", 6) == 0) {
			int iChr;
			for (iChr = 0; iChr < 6; iChr ++) {
				r = rand() / 528.5;
				*(c ++) = letters[r > 0 ? r - 1 : 0];
			}
		} else {
			errno = EINVAL;
			return -1;
		}
	} else {
		errno = EINVAL;
		return -1;
	}

	return open (tmplate, O_CREAT | O_RDWR);
}