/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _BASE_H
#define _BASE_H

#define	SYSV_GVGAFB		(unsigned *) 0x9040

unsigned short *vgafb;
unsigned short *vgadb;

unsigned vgafb_res_x;
unsigned vgafb_res_y;

/* externs */
extern void xcls (unsigned color);
extern unsigned xinit ();
extern void xpixel (unsigned x, unsigned y, unsigned color);
extern unsigned xpixelget (unsigned x, unsigned y);
extern void xfbswap ();
extern void xexit ();

#endif
