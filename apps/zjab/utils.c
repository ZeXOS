/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static inline unsigned char to_uchar (char ch)
{
	return ch;
}

static void base64_encode (const char *in, unsigned inlen, char *out, unsigned outlen)
{
	static const char b64str[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	while (inlen && outlen) {
		*out ++ = b64str[(to_uchar (in[0]) >> 2) & 0x3f];
		
		if (!-- outlen)
			break;
		
		*out ++ = b64str[((to_uchar (in[0]) << 4) + (-- inlen ? to_uchar (in[1]) >> 4 : 0)) & 0x3f];
		
		if (!-- outlen)
			break;
		
		*out ++ = (inlen ? b64str[((to_uchar (in[1]) << 2) + (-- inlen ? to_uchar (in[2]) >> 6 : 0)) & 0x3f] : '=');

		if (!-- outlen)
			break;
		
		*out ++ = inlen ? b64str[to_uchar (in[2]) & 0x3f] : '=';
		
		if (!-- outlen)
			break;
		
		if (inlen)
			inlen --;
		
		if (inlen)
			in += 3;
	}

	if (outlen)
		*out = '\0';
}

char *base64 (const char *input, int length)
{
	char *buff = (char *) malloc (4 * length + 1);
	
	if (!buff)
		return 0;

	base64_encode (input, length, buff, 4 * length + 1);
	
	return buff;
}
