/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Martin 'povik' Poviser (martin.povik@gmail.com)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <file.h>
#include <dev.h>
#include <fd.h>
#include <pipe.h>
#include <cache.h>
#include <errno.h>
#include <proc.h>

int read (unsigned fd, void *buf, unsigned len)
{
	fd_t *d = fd_get (fd);

	if (!d)
		return 0;

	if (d->flags & O_WRONLY)
		return 0;

	if (!d->s)
		return 0;
	
	if (d->flags & FD_PIPE) {
		pipe_t *p = pipe_get (fd);
		
		if (p)
			return pipe_read (p, buf, len);
		else
			return 0;
	}

	if (len > d->e)
		len = d->e;

	/* copy data from cache to buffer */
	memcpy ((char *) buf, d->s+d->p, len);

	d->e -= len;
	d->p += len;

	/* HACK: this should call UPDATE handler */
	if (d->dev)
		d->dev->handler (DEV_ACT_UPDATE);

	return len;
}
