/*
 *  ZeX/OS
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define IMAGE_FILE	"inst.img"

int main (int argc, char **argv)
{
	printf ("ZeX/OS Installer\n");
	
	if (argc < 2) {
		printf ("syntax: inst <device>\n\texample: inst /dev/hda\n");
		return -1;
	}
		
	char *dev = argv[1];
	unsigned dev_len = strlen (dev);
	
	printf ("Device: %s\n----------------", dev);

	printf ("\nAre you sure you want lose all data on the current device ? (y/N)\n");
	
	char c = getchar ();
	
	if (c == 'n' || c == 'N') {
		printf ("Installer was aborted\n");
		return 0;
	} else if (c == 'y' || c == 'Y')
		printf ("Loading data ..\n");
	else {
		printf ("Installer was aborted - wrong option '%c'\n", c);
		return 0;
	}
	
	int fd = open (IMAGE_FILE, O_RDONLY);

	if (fd < 0) {
		printf ("ERROR -> image file '%s' not found\n", IMAGE_FILE);
		return -1;
	}

	unsigned flen = 2048*1024;	/* 2MB image */

	printf ("Image size: %dkB\n", flen/1024);
	
	struct ioatarq_t rq;
	memcpy (rq.dev, dev, dev_len);
	rq.dev[dev_len] = '\0';
	
	unsigned long s = 0;
	for (s = 0; s < flen/512; s ++) {
		rq.sector = s;
		
		memset (rq.data, 0, 512);
		int l = read (fd, rq.data, 512);
		
		if (!l) {
			printf ("ERROR -> read () == 0; image data are probably corrupted\n");
			break;
		}
		
		int r = ioctl (IOATAWRITE, &rq, sizeof (struct ioatarq_t));

		if (r == -1) {
			printf ("WARNING -> ioctl (IOATAWRITE) == -1; repeating\n");
			sleep (1);
			r = ioctl (IOATAWRITE, &rq, sizeof (struct ioatarq_t));
			if (r == -1) {
				printf ("ERROR -> ioctl (IOATAWRITE) == -1; installation failed !\n");
				break;
			}
		}
		
		usleep (1);
		printf ("\b\b\b\b\b%d %c", s/41+1, '%');
	}
	
	close (fd);
	
	printf ("\nInstallation was finished\nCongratulation !\n");
	
	return 0;
}

 