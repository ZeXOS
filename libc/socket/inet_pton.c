/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define NET_IPV4_TO_ADDR(a, b, c, d) \
	(((unsigned)(d) << 24) | (((unsigned)(c) & 0xff) << 16) | (((unsigned)(b) & 0xff) << 8) | ((unsigned)(a) & 0xff))

void NET_IPV6_TO_ADDR (struct in6_addr *ip, unsigned short a, unsigned short b, unsigned short c, unsigned short d, 
			  unsigned short e, unsigned short f, unsigned short g, unsigned short h) {

	unsigned short buf[8];

	buf[0] = htons (a);
	buf[1] = htons (b);
	buf[2] = htons (c);
	buf[3] = htons (d);
	buf[4] = htons (e);
	buf[5] = htons (f);
	buf[6] = htons (g);
	buf[7] = htons (h);

	memcpy (ip, (void *) buf, sizeof (struct in6_addr));
}

int inet_pton (int af, const char *src, void *dst)
{
	if (!src || !dst)
		return 0;

	if (af == AF_INET) {
		unsigned *ip = (unsigned *) src;

		unsigned char a = 0;
		unsigned char b = 0;
		unsigned char c = 0;
		unsigned char d = 0;

		unsigned g = 0;
		unsigned i = 0;
		unsigned y = strlen (src);

		if (!y)
			return 0;

		char *str = (char *) malloc (sizeof (char) * (y + 1));

		if (!str)
			return 0;

		memcpy (str, ip, y);
		str[y] = '\0';

		unsigned h[4];

		while (i < y) {
			if (str[i] == '.') {
				str[i] = '\0';
				h[g] = i+1;
				g ++;
			}

			i ++;
		}

		if (g != 3) {
			free (str);
			return -1;
		}
		
		a = atoi (str);
		b = atoi (str+h[0]);
		c = atoi (str+h[1]);
		d = atoi (str+h[2]);

		free (str);

		g = NET_IPV4_TO_ADDR (a, b, c, d);

		memcpy (dst, &g, sizeof (struct in_addr));
		
		return 0;
	} else if (af == AF_INET6) {
		unsigned short a;
		unsigned short b;
		unsigned short c;
		unsigned short d;
		unsigned short e;
		unsigned short f;
		unsigned short g;
		unsigned short h;

		unsigned j = 0;
		unsigned i = 0;
		unsigned y = strlen (src);
		
		char *ip = strndup (src, y);

		if (!y || !ip)
			return -1;

		unsigned k[8];

		while (i < y) {
			if (ip[i] == ':') {
				ip[i] = '\0';
				k[j] = i+1;
				j ++;
			}

			i ++;
		}

		if (j != 7)
			return 0;

		char *endptr;

		a = strtol (ip, &endptr, 16);
		b = strtol (ip+k[0], &endptr, 16);
		c = strtol (ip+k[1], &endptr, 16);
		d = strtol (ip+k[2], &endptr, 16);
		e = strtol (ip+k[3], &endptr, 16);
		f = strtol (ip+k[4], &endptr, 16);
		g = strtol (ip+k[5], &endptr, 16);
		h = strtol (ip+k[6], &endptr, 16);

		NET_IPV6_TO_ADDR ((struct in6_addr *) dst, a, b, c, d, e, f, g, h);
		
		free (ip);

		return 0;
	}

	return -1;
}

