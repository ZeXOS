/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _VIDEO_H
#define _VIDEO_H

extern const unsigned char font5x8[];

extern unsigned int init_video (void);
extern void video_putch (unsigned char c);
extern void video_cls ();
extern void video_setattrib (int attr);
extern int video_getattrib ();
extern void video_color (unsigned char forecolor, unsigned char backcolor);
extern void video_gotoxy (unsigned x, unsigned y);
extern bool video_acthandler (unsigned act, char *block, char *more, int block_len);
extern void video_gfx_pixel (unsigned x, unsigned y, unsigned color);
extern void video_gfx_cls (unsigned color);
extern void video_gfx_exit ();
extern char *init_vgafb ();

#endif
