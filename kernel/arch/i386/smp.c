/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <smp.h>
#include <arch/ipi.h>

#define MPC_ENTRY_CPU		0
#define MPC_ENTRY_BUS		1
#define MPC_ENTRY_IOAPIC	2
#define MPC_ENTRY_IOINT		3
#define MPC_ENTRY_LINT		4

#define __PACKED__	__attribute__ ((packed))

struct mp_cpu_entry {
	unsigned char type;
	unsigned char lapic_id;
	unsigned char lapic_ver;
	unsigned char cpu_flags;
	unsigned cpu_signature;
	unsigned cpu_features;
} __PACKED__;

struct mp_cfg_table {
	unsigned char signature[4];
	unsigned short base_table_len;
	unsigned char rev;
	unsigned char checksum;
	unsigned char oem_id[8];
	unsigned char product_id[12];
	unsigned oem_table_ptr;
	unsigned short oem_table_size;
	unsigned short entry_cnt;
	unsigned lapic_addr;
	unsigned short ext_table_len;
	unsigned char ext_table_sum;
} __PACKED__;

struct mp_flt_ptr {
	unsigned char signature[4];	/* ASCII signature "_MP_" */
	struct mp_cfg_table *mpc_ptr;	/* pointer to the MP configuration structure */
	unsigned char len;		/* length of this structure in 16 byte paragraphs */
	unsigned char rev;		/* version of the multiprocessing specification */
	unsigned char checksum;		/* sum of all bytes in this structure */
	unsigned char feature_1;	/* feature flags */
	unsigned char feature_2;	/* presence of the ICMR */
	unsigned char feature_3;	/* reserved */
	unsigned char feature_4;	/* reserved */
	unsigned char feature_5;	/* reserved */
} __PACKED__;

struct smp_scan_spots_struct {
	unsigned start;
	unsigned stop;
};

static struct smp_scan_spots_struct smp_scan_spots[] = {
	{ 0x9fc00, 0xa0000},
	{ 0xf0000, 0x100000},
	{ 0, 0 }
};


static unsigned *arch_smp_probe (unsigned base, unsigned limit)
{
	char *ptr;

	for (ptr = (char *) base; (unsigned) ptr < limit; ptr ++) {
		if (*ptr == '_') {
			if (!strncmp (ptr, "_MP_", 4))
				return (unsigned *) ptr;
		}
	}
	return 0;
}

static struct mp_flt_ptr *arch_smp_find_mp_config ()
{
	int i;
	struct mp_flt_ptr *mp_flt = 0;

	for (i = 0; smp_scan_spots[i].stop; i ++) {
		mp_flt = (struct mp_flt_ptr *) arch_smp_probe (smp_scan_spots[i].start, smp_scan_spots[i].stop);

		if (mp_flt)
			break;
	}

	return mp_flt;
}

static char *arch_smp_mpc_entry (char *entry)
{
	unsigned type = (unsigned) *entry;
	struct mp_cpu_entry *cpu;

	switch (type) {
		case MPC_ENTRY_CPU:
			cpu = (struct mp_cpu_entry *) entry;
			entry += 20;

			if (!(cpu->cpu_flags & 1))
				break;

			smp_arch_x86 *arch_spec = (smp_arch_x86 *) kmalloc (sizeof (smp_arch_x86));

			if (!arch_spec)
				return 0;

			arch_spec->lapic_id = cpu->lapic_id;
			arch_spec->lapic_ver = cpu->lapic_ver;
			arch_spec->cpuid = cpu->cpu_signature;
			arch_spec->features = cpu->cpu_features;

			if (!smp_cpu_register (SMP_ARCH_x86, (cpu->cpu_flags & 2) ? SMP_STATE_CPU_UP : SMP_STATE_CPU_DOWN,
			    (cpu->cpu_flags & 2) ? SMP_FLAGS_CPU_BS : SMP_FLAGS_CPU_AP, arch_spec))
				return 0;

			break;
		default:
			entry += 8;
			break;
	}

	return entry;
}

static void arch_smp_sleep (int t)
{
	int_enable ();
	timer_wait (t);
	int_enable ();
}

static unsigned arch_smp_cpuboot (unsigned lapic_id)
{
	arch_ipi_send (lapic_id, IPI_TYPE_INIT, 0);

	arch_smp_sleep (100);

	arch_ipi_send (lapic_id, IPI_TYPE_SIPI, SMP_ARCH_x86_TRAMPOLINE / 0x1000);

	/*arch_smp_sleep (200);
	arch_ipi_send (lapic_id, IPI_TYPE_SIPI, 0x07);
	arch_smp_sleep (200);*/

	return 1;
}

extern void gdt_smp ();

unsigned arch_smp_trampoline ()
{
/*	asm volatile (
		"mov %cs, %ax;"
		"mov %ax, %ds;"
	);*/

	/* enable protected mode on AP cpu */
	/*asm volatile (
		"movl %cr0, %eax;"
		"orl $(1<<0), %eax;"
		"movl %eax, %cr0;"
	);*/

/*asm volatile (
	"mov $0x4F02, %ax;"
	"int $0x10;"
	"sti;"
);*/

	/* hang up processor - TODO */
	while (1)
		asm ("hlt");

	return 1;
}

unsigned arch_smp_init ()
{
	struct mp_cfg_table *mpc_ptr = 0;
	struct mp_flt_ptr *mp_flt = arch_smp_find_mp_config ();

	/* this pc is not smp capable */
	if (!mp_flt)
		return 0;

	/* check for intel spec. */
	if (mp_flt->feature_1) {
		/* TODO */
		return 0;
	} else
		mpc_ptr = mp_flt->mpc_ptr;

	if (!mpc_ptr)
		return 0;

	/* verify MP Configuration Table signature */
	if (strncmp (mpc_ptr->signature, "PCMP", 4))
		return 0;

	/* save lapic address */
	ipi_lapic_init (mpc_ptr->lapic_addr);

	unsigned i;
	char *entry_ptr = (char *) mpc_ptr + 44;
	
	for (i = 0; i < mpc_ptr->entry_cnt; i ++)
		entry_ptr = arch_smp_mpc_entry (entry_ptr);

	memcpy (SMP_ARCH_x86_TRAMPOLINE, &arch_smp_trampoline, sizeof (&arch_smp_trampoline));

	smp_cpu_t *cpu = 0;

	while (cpu = smp_cpu_getnextcpu (cpu)) {
		/* check for AP processor */
		if (cpu->flags != SMP_FLAGS_CPU_AP)
			continue;

		smp_arch_x86 *arch_spec = cpu->arch_spec;

		if (!arch_spec)
			continue;

		/* try to boot AP processor */
		if (arch_smp_cpuboot (arch_spec->lapic_id))
			cpu->state = SMP_STATE_CPU_UP;
	}

	return 1;
}
