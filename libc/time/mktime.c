/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <time.h>

#define MINUTE		60
#define HOUR		(60 * MINUTE)
#define DAY		(24 * HOUR)
#define YEAR		(365 * DAY)

static int is_leap_year (int year)
{
	if (year % 4 == 0) {
		if (year % 100 == 0) {
			if (year % 400 == 0)
				return 1;
			else 
				return 0;
		}

		return 1;
	}
	return 0;
}

static int count_leap_years (int epoch, int year)
{
	int i, result = 0;
	for (i = epoch; i < year; i ++)
		if (is_leap_year(i))
			result ++;

	return result;
}

static int get_day (int year, int mon, int day)
{
	int result;

	switch (mon) {
		case 0:
			result = 0;
			break;
		case 1:
			result = 31;
			break; /* 1: 31 */
		case 2:
			result = 59;
			break; /* 2: 31+28 */
		case 3:
			result = 90;
			break; /* 3: 59+31 */
		case 4:
			result = 120;
			break; /* 4: 90+30 */
		case 5:
			result = 151;
			break; /* 5: 120+31 */
		case 6:
			result = 181;
			break; /* 6: 151+30 */
		case 7:
			result = 212;
			break; /* 7: 181+31 */
		case 8:
			result = 243;
			break; /* 8: 212+31 */
		case 9:
			result = 273;
			break; /* 9: 243+30 */
		case 10:
			result = 304;
			break; /* 10: 273+31 */
		case 11:
			result = 334;
			break; /* 11: 304+30 */
		default:
			break;
	}

	if (is_leap_year (year) && mon > 2)
		result ++;

	result += day - 1;

	return result;
}

time_t mktime (struct tm *tm)
{
	if (!tm)
		return -1;

	unsigned long result = 0;

	result = tm->tm_sec;
	result += tm->tm_min * MINUTE;
	result += tm->tm_hour * HOUR;
	result += get_day (tm->tm_year, tm->tm_mon - 1, tm->tm_mday) * DAY;
	result += (tm->tm_year - EPOCH_YEAR) * YEAR;
	result += count_leap_years (EPOCH_YEAR, tm->tm_year) * DAY;
	
	return result;
}
