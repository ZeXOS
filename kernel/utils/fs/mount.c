/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <mount.h>
#include <partition.h>
#include <dev.h>
#include <vfs.h>

mount_t mount_list;

void mount_display ()
{
	mount_t *mnt;
	for (mnt = mount_list.next; mnt != &mount_list; mnt = mnt->next)
		kprintf ("%s on %s\n", mnt->p->name, mnt->mountpoint);
}

partition_t *mount_find (char *mountpoint)
{
	mount_t *mnt;
	for (mnt = mount_list.next; mnt != &mount_list; mnt = mnt->next)
		if (!strcmp (mnt->mountpoint, mountpoint))
			return mnt->p;

	return 0;
}

unsigned mount_verify (partition_t *p, char *mountpoint)
{
	unsigned mp_len = strlen (mountpoint);

	if (!mp_len)	// HACK
		return 2;

	if (mount_find (mountpoint))
		return 1;

	if (!vfs_find (mountpoint, mp_len))
		return 3;

	return 0;
}

unsigned mount_register (partition_t *p, char *mountpoint)
{	
	unsigned mp_len = strlen (mountpoint);

	mount_t *mnt;
	
	/* alloc and init context */
	mnt = (mount_t *) kmalloc (sizeof (mount_t));

	if (!mnt)
		return 0;

	memcpy (mnt->mountpoint, mountpoint, mp_len);
	mnt->mountpoint[mp_len] = '\0';

	mnt->p = p;

	/* add into list */
	mnt->next = &mount_list;
	mnt->prev = mount_list.prev;
	mnt->prev->next = mnt;
	mnt->next->prev = mnt;

	return 1;
}

bool mount (partition_t *p, char *dir, char *mountpoint)
{
	unsigned err = mount_verify (p, mountpoint);

	if (err == 1) {
		kprintf ("ERROR -> partition %s is already mounted to %s\n", p->name, mountpoint);
		return 0;
	}
	if (err == 3) {
		kprintf ("ERROR -> mountpoint %s does not exists\n", mountpoint);
		return 0;
	}

 	dev_t *dev = (dev_t *) dev_findbypartition (p);

	if (!dev)
		return 0;

	if (!dev->handler (DEV_ACT_MOUNT, p, mountpoint, dir, NULL)) {
		kprintf ("ERROR -> mount failed: %s\n", p->name);
		return 0;
	}

	return mount_register (p, mountpoint);
}

bool umount (partition_t *p, char *mountpoint)
{
	bool f = 0;

	char mdir[64];
	unsigned mdir_len = strlen (mountpoint);

	/* FIXME: you can umount wrong local directory */
	if (mountpoint[0] != '/') {
		mdir[0] = '/';
		memcpy (mdir+1, mountpoint, mdir_len);
		mdir_len ++;
	} else
		memcpy (mdir, mountpoint, mdir_len);

	mdir[mdir_len] = '\0';

	mount_t *mnt;
	for (mnt = mount_list.next; mnt != &mount_list; mnt = mnt->next) {
		if (!strcmp (mnt->mountpoint, mdir)) {
			if (mnt->p == p) {
				f = 1;
				break;
			}
		}
	}

	if (f) {
		/*dev_t *dev = (dev_t *) dev_findbypartition (p);

		if (!dev)
			return 0;

		if (!dev->handler (DEV_ACT_UMOUNT, p, mnt->mountpoint, dir, NULL)) {
			kprintf ("ERROR -> umount failed: %s\n", p->name);
			return 0;
		}*/

		while (vfs_list_delbymp (mdir))
			schedule ();

		mnt->next->prev = mnt->prev;
		mnt->prev->next = mnt->next;

		//kfree (mnt);
		return 1;
	}
	
	return 0;
}
