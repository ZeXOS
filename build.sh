#!/bin/bash

VERSION="0.6.8"
ARCHITECTURE="i386"

APPPREFIX="/usr/bin"

if [ -z $ARCH ] ; then
	ARCH=$ARCHITECTURE
fi

export ARCH

if [ "$ARCH" = "i386" ] ; then
	CC="gcc -m32"
	LD="ld -m elf_i386"
	AR="ar"
elif [ "$ARCH" = "x86_64" ] ; then
	CC="gcc -m64"
	LD="ld -m elf_x86_64"
	AR="ar"
elif [ "$ARCH" = "arm" ] ; then
	CC=$(ls /usr/bin/arm*-gcc)
	LD=$(ls /usr/bin/arm*-ld)
	AR=$(ls /usr/bin/arm*-ar)
fi

export CC
export LD
export AR

cd `dirname $0`

if ! [ -e $APPPREFIX/make ] ; then
      echo "Please install package: make"
      exit -1
fi

if [ "$1" = "clean" ] ; then
	echo "Cleaning source from object files .." 
	make clean -C libc &&
	make clean -C libx &&
	make clean -C libm &&
	make clean -C libipc &&
	make clean -C libpthread &&
	make clean -C libsnd && \
	rm -f tools/ips2ip/ips2ip ; \
	rm -f iso/boot/kernel.bin ; \
	rm -f iso/README ; \
	rm -f zexos-$VERSION-*.iso ; \
	rm -f zexos-$VERSION-$ARCH.img ; \
	make clean -C kernel ; \
	echo "OK"
	exit 0
fi

if ! [ -e $APPPREFIX/gcc ] ; then
      echo "Please install package: gcc"
      exit -1;
fi
if ! [ -e $APPPREFIX/nasm ] ; then
      echo "Please install package: nasm"
      exit -1;
fi

if [ "$1" = "qemu" ] ; then
	if ! [ -e $APPPREFIX/qemu ] ; then
	      echo "Please install package: qemu"
	      exit -1
	fi

	make -C kernel

	if [ "$ARCH" = "arm" ] ; then
		qemu-system-arm -kernel kernel/kernel.bin -serial stdio
		exit 1
	fi

	if [ "$ARCH" = "ppc" ] ; then
		qemu-system-powerpc -kernel kernel/kernel.bin
		exit 1
	else
		qemu -cdrom zexos-$VERSION-$ARCH.iso
		exit 1
	fi

	exit 0
fi

if [ -e kernel/.config ]
then
	echo "Checking for .config: OK"
else
	echo "Using default kernel config - defconfig .."
	cp kernel/defconfig kernel/.config

	if [ -e kernel/.config ] ; then
		echo " :)"
	else
		echo "Please check your .config file in kernel directory and try it again !"
		exit 0
	fi
fi

make -C kernel
if [ "$ARCH" = "i386" ] ; then
	make -C libc
	make -C libx
	make -C libm
	make -C libipc
	make -C libpthread
	make -C libsnd
fi

if [ -e kernel/kernel.bin ] ; then
	echo "Source was compiled succefully"
fi

if [ "$1" = "iso" ] ; then
	if ! [ -e $APPPREFIX/mkisofs ] ; then
	      echo "Please install package: cdrtools (mkisofs)"
	      exit -1
	fi

	if [ -n "$2" ] ; then
		make -C apps/$2 iso
	fi

	cp kernel/kernel.bin iso/boot/kernel.bin &&
	cp README iso/README &&
	mkisofs -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 60 -boot-info-table -o zexos-$VERSION-$ARCH.iso iso &&
	echo iso created OK && exit 0
fi

if [ "$1" = "img" ] || [ "$1" = "floppy" ] ; then
	wget -c http://zexos.sf.net/zexos.img && \
	mkdir zexos ; \
	mount -oloop zexos.img zexos && \

	if [ -e zexos-$VERSION-$ARCH.iso ] ; then
		echo " :)"
	else
		echo "I have to make .iso image first"
		cp kernel/kernel.bin iso/boot/kernel.bin &&
		cp libc/libc.a iso/libc.a &&
		cp README iso/README &&
		mkisofs -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 -boot-info-table -o zexos-$VERSION-$ARCH.iso iso
	fi

	mkdir zexosiso ; \
	mount -oloop zexos-$VERSION-$ARCH.iso zexosiso && \
	cp zexosiso/boot/kernel.bin zexos/boot/ && \
	umount zexosiso && \
	umount zexos && \
	rmdir zexosiso && \
	rmdir zexos && \
	mv zexos.img zexos-$VERSION-$ARCH.img && \
	rm zexos-$VERSION-$ARCH.iso && \
	echo "OK - Image 'zexos-$VERSION-$ARCH.img' was created succefully."
	echo "Please wait until installer copy image to floppy ..."
	if [ "$1" = "floppy" ] ; then
		dd if=zexos-$VERSION-$ARCH.img of=/dev/fd0 bs=1440k && \
		echo "Installation on floppy is done, enjoy !"
	fi
fi