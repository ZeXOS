/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libx/base.h>
#include <libx/image.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

xbitmap *ximage_open (const char *filename)
{
	int fd = open (filename, O_RDONLY);
	
	if (!fd)
		return 0;

	unsigned char *header = (unsigned char *) malloc (sizeof (unsigned char) * 1080);

	if (!header)
		return 0;

	int len = read (fd, header, 1078);

	if (!len)
		return 0;

	if (header[0] != 'B' && header[1] != 'M')
		return 0;

	/* read in the width and height of the image, and the
		number of colors used; ignore the rest */
	xbitmap *bitmap = (xbitmap *) malloc (sizeof (xbitmap));
	if (!bitmap)
		return 0;

	bitmap->width = header[18] | (header[19] << 8);
	bitmap->height = header[22] | (header[23] << 8);
	unsigned short num_colors = header[46] | (header[47] << 8);

  	/* assume we are working with an 8-bit file */
 	if (num_colors == 0)
		num_colors = 256;

	bitmap->data = (unsigned char *) malloc (sizeof (unsigned char) * (bitmap->width * bitmap->height));

	if (!bitmap->data)
		return 0;

	len = read (fd, bitmap->data, (bitmap->width * bitmap->height));

	return bitmap;
}

void ximage_draw (xbitmap *bitmap, unsigned x, unsigned y, int transparency)
{
	unsigned a = 0;
	unsigned b = 0;
	unsigned j = 0;

	if (transparency == -1) {
		for(a = 0; a < (unsigned) bitmap->height * (unsigned) bitmap->width; a ++) {
			if (j >= (unsigned) bitmap->width) {
				j = 0;
				b ++;
			}
			xpixel (j+x, b+y, bitmap->data[a]);
			j ++;
		}
	} else {
		for(a = 0; a < (unsigned) bitmap->height * (unsigned) bitmap->width; a ++) {
			if (j >= bitmap->width) {
				j = 0;
				b ++;
			}

			if (bitmap->data[a] != (unsigned char) transparency)
				xpixel (j+x, b+y, bitmap->data[a]);

			j ++;
		}
	}
}

void ximage_draw_spec (xbitmap *bitmap, unsigned x, unsigned y, unsigned size_x, unsigned size_y, unsigned offset_x, unsigned offset_y, int transparency)
{
	unsigned a = 0;
	unsigned b = 0;
	unsigned j = 0;
	unsigned k = ((offset_y*2) * size_x + offset_x);

	if (transparency == -1) {
		for(a = 0; a < size_x * size_y; a ++) {
			if (j >= size_x) {
				j = 0;
				b ++;
				a += bitmap->width - size_x;
			}
			xpixel (j+x, b+y, bitmap->data[a+k]);
			j ++;
		}
	} else {
		for(a = 0; a < (unsigned) bitmap->height * (unsigned) bitmap->width; a ++) {
			if (j >= size_x) {
				j = 0;
				b ++;

				if (b >= size_y)
					return;

				a += bitmap->width - size_x;
			}

			if (bitmap->data[a+k] != (unsigned char) transparency)
				xpixel (j+x, b+y, bitmap->data[a+k]);

			j ++;
		}
	}
}
