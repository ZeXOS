/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "client.h"
#include "game.h"

game_t game_list;

/** Reset game board to default positions */
int game_board_reset (game_t *game)
{	
	//Vez,Jezdec,Strelec,Dama,Kral,Strelec,Jezdec,Vez
	memcpy (game->board[0], "VJSDKSJV", 8);		// radek 1 (bily)
	memcpy (game->board[1], "PPPPPPPP", 8);		// radek 2 (bily)
	memcpy (game->board[2], "........", 8);
	memcpy (game->board[3], "........", 8);
	memcpy (game->board[4], "........", 8);
	memcpy (game->board[5], "........", 8);
	memcpy (game->board[6], "pppppppp", 8);		// radek 7 (cerny)
	memcpy (game->board[7], "vjsdksjv", 8);		// radek 8 (cerny)

	return 1;
}

game_t *game_find (char *name)
{
	game_t *game;
	for (game = game_list.next; game != &game_list; game = game->next)
		if (!strcmp (game->name, name))
			return game;

	return 0;
}

game_t *game_findbyclient (client_t *client)
{
	game_t *game;
	for (game = game_list.next; game != &game_list; game = game->next)
		if (game->white == client ||
			game->black == client)
			return game;

	return 0;
}

/** Create new game's structure */
game_t *game_new (client_t *client, char *name, unsigned name_len)
{
	game_t *game = game_find (name);

	if (game)
		return 0;

	/* alloc and init context */
	game = (game_t *) malloc (sizeof (game_t));

	if (!game)
		return 0;
	
	/* setup game name */
	game->name = (char *) malloc (sizeof (char) * (name_len + 1));

	if (!game->name)
		return 0;

	memcpy (game->name, name, name_len);
	game->name[name_len] = '\0';

	/* set variables to default value */
	game->play = 0;
	game->round = 0;
	game->player = 'w';

	/* white player is each time game creator */
	game->white = client;
	/* black player have to join to game*/
	game->black = 0;

	/* reset chess board */
	game_board_reset (game);

	/* add into list */
	game->next = &game_list;
	game->prev = game_list.prev;
	game->prev->next = game;
	game->next->prev = game;

	return game;
}

/** Client would to join to game */
int game_join (client_t *client, game_t *game)
{
	/* you can join to game, when it not running */
	if (game->play)
		return 0;

	/* player, what create game have to be connected */
	if (!game->white)
		return 0;

	/* player, what create game, cant join as opponent too */
	if (game->white == client)
		return 0;

	game->play = 1;
	game->black = client;

	unsigned white_len = strlen (game->white->nick);
	unsigned black_len = strlen (game->black->nick);

	char *str = (char *) malloc (white_len+white_len+3);

	if (!str)
		return 0;

	memcpy (str, "s   ", 4);

	str[2] = 'b';

	memcpy (str+4, game->white->nick, white_len);

	client_pmmsg (game->black, str, 4+white_len);


	str[2] = 'w';

	memcpy (str+4, game->black->nick, black_len);

	client_pmmsg (game->white, str, 4+black_len);

	return 1;
}

/** Delete game's structure */
int game_quit (game_t *game)
{
	game->next->prev = game->prev;
	game->prev->next = game->next;

	free (game->name);

	free (game);

	return 1;
}

/** Synchronize game with client's positions */
int game_sync (client_t *client, game_t *game)
{
	char str[73];

	/*unsigned i = 0;
	unsigned y = 0;

	while (i < 8) {
		y = 0;

		while (y < 8) {
			str[y+i*9] = game->board[i][y];
			y ++;
		}

		str[8+i*9] = '\n';

		i ++;
	}*/

	int x;
	int y;

	int i = 0;
	
	for (x = 7; x >= 0; x --) {
		for (y = 0; y < 8; y ++) {
			str[i] = game->board[x][y];
			i ++;
		}

		str[i] = '\n';
		i ++;
	}

	client_pmmsg (client, str, 72);

	return 1;
}

int game_pos (client_t *client, game_t *game, unsigned char x_old, unsigned char y_old, unsigned char x, unsigned char y)
{
	char f = game->board[y_old][x_old];

	game->board[y_old][x_old] = '.';
	
	game->board[y][x] = f;

	return 1;
}

int game_getlist (client_t *client)
{
	int len = 0;

	game_t *game;

	/* first calculate length of all names */
	for (game = game_list.next; game != &game_list; game = game->next)
		len += (strlen (game->name) + 3);

	if (!len)
		return 0;

	char *list = (char *) malloc (sizeof (char) * (len + 1));
	
	if (!list)
		return 0;

	len = 0;

	unsigned l;

	/* we can save all game names to list now */
	for (game = game_list.next; game != &game_list; game = game->next) {
		l = strlen (game->name);

		if (game->play)
			list[len] = '-';
		else
			list[len] = '+';

		memcpy (list+1+len, " ", 1);
		memcpy (list+2+len, game->name, l);
		memcpy (list+2+len+l, "\n", 1);

		len += (l + 3);
	}

	list[len] = '\0';

	client_pmmsg (client, list, len);

	free (list);

	return 1;
}

/** Client Init function */
int init_game ()
{
	game_list.next = &game_list;
	game_list.prev = &game_list;

	return 1;
}

