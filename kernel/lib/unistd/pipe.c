/*
 *  ZeX/OS
 *  Copyright (C) 2009  Martin 'povik' Poviser (martin.povik@gmail.com)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */                                                                                                                                                                  

#include <system.h>
#include <string.h>
#include <fd.h>
#include <pipe.h>
#include <errno.h>

pipe_t *pipe_list_start;
pipe_t *pipe_list_end;

int pipe (int fds[2])
{
	pipe_t *p = (pipe_t *) kmalloc (sizeof (pipe_t));

	fd_t *fd = fd_create (FD_PIPE);
	fds[0] = fd->id;
	
	fd = fd_create (FD_PIPE);
	fds[1] = fd->id;

	if (!pipe_list_start) {
		pipe_list_end = p;
		pipe_list_start = p;
		p->prev = NULL;
		p->next = NULL;
	} else {
		pipe_list_end->next = p;
		p->prev = pipe_list_end;
		pipe_list_end = p;
		p->next = NULL;
	}

	p->fd_a = fds[0];
	p->fd_b = fds[1];

	p->buffer_list_start = (pipe_buffer_t *) kmalloc (sizeof(pipe_buffer_t));
	p->buffer_list_end = p->buffer_list_start;
	p->buffer_list_start->next = NULL;
	p->buffer_list_start->prev = NULL;
	p->buffer_end_len = 0;
	p->buffer_start_pos = 0;

	return 0;
}

pipe_t *pipe_get (int fd)
{
	pipe_t *p = pipe_list_start;

	if (!p)
		return NULL;
	
	while (true) {
                if (p->fd_a == fd
                    || p->fd_b == fd)
			return p;

                if (!p->next)
                        break;
                else
                        p = p->next;
	}

	return NULL;
}

int pipe_write (pipe_t *p, BYTE *buffer, unsigned int buffer_len)
{
	if ((!p) || (!buffer) || (!buffer_len) || (!p->buffer_list_start)) {
		errno_set (EINVAL);
		return -1;
	}
	
	{ // hardcore code :)
		unsigned int block_size = 0;
		unsigned int buffer_pos = 0;
		pipe_buffer_t *ptr;
		while (buffer_pos < buffer_len) {
			if ((buffer_len - buffer_pos) >= (PIPE_BUFFER_PART_SIZE - p->buffer_end_len)) {
				ptr = p->buffer_list_end;
				block_size = PIPE_BUFFER_PART_SIZE - p->buffer_end_len;
				memcpy (((ptr->buffer) + p->buffer_end_len), ((buffer) + buffer_pos), block_size);
				buffer_pos += block_size;
				
				p->buffer_list_end = ((pipe_buffer_t*) kmalloc(sizeof (pipe_buffer_t)));
				p->buffer_end_len = 0;
				ptr->next = p->buffer_list_end;
				p->buffer_list_end->prev = ptr;
			} else {
				ptr = p->buffer_list_end;
				block_size = buffer_len - buffer_pos;
				memcpy (((ptr->buffer) + p->buffer_end_len), ((buffer) + buffer_pos), block_size);
				p->buffer_end_len += block_size;
				buffer_pos += block_size;
			}
		}
		
		return buffer_pos;
	}
}

int pipe_read (pipe_t *p, BYTE *buffer, unsigned int buffer_len)
{
	unsigned int buffer_pos = 0;
		
	if ((!p) || (!buffer) || (!buffer_len) || (!p->buffer_list_start)) {
		errno_set (EINVAL);
		return -1;
	}

	while (true) { // hardcore code :)
		while (buffer_pos < buffer_len) {
			pipe_buffer_t *ptr;
			unsigned int block_size = 0;
			unsigned int buffer_space = buffer_len - buffer_pos;

			if (p->buffer_list_start == p->buffer_list_end) {
				unsigned int pipe_buffer_len = p->buffer_end_len - p->buffer_start_pos; 
				block_size = (pipe_buffer_len < buffer_space) ?	(pipe_buffer_len) : (buffer_space);

				if (p->buffer_start_pos == p->buffer_end_len)
					break;
				ptr = p->buffer_list_start;
				memcpy (((buffer) + buffer_pos), ((ptr->buffer) + p->buffer_start_pos), block_size);
				p->buffer_start_pos += block_size;
				buffer_pos += block_size;
			} else if (buffer_space >= PIPE_BUFFER_PART_SIZE - p->buffer_start_pos) {
				ptr = p->buffer_list_start;

				block_size = PIPE_BUFFER_PART_SIZE - p->buffer_start_pos;
				memcpy (((buffer) + buffer_pos), ((ptr->buffer) + p->buffer_start_pos), block_size);
				buffer_pos += block_size;
				
				p->buffer_start_pos = 0;
				p->buffer_list_start = ptr->next;
				p->buffer_list_start->prev = NULL;
				kfree(ptr);
			} else {
				ptr = p->buffer_list_start;
				memcpy (((buffer) + buffer_pos), ((ptr->buffer) + p->buffer_start_pos), buffer_space);
				p->buffer_start_pos += buffer_space;
			}
		}

		if (buffer_pos == buffer_len)
			return buffer_pos;
		else 
			schedule();
	}
}

void pipe_remove (pipe_t *pipe)
{
	if (pipe->next)
		pipe->next->prev = pipe->prev;
	else {
		if (pipe->prev)
			pipe_list_end = pipe->prev;
		else
			pipe_list_end = NULL;
	}

	if (pipe->prev)
		pipe->prev->next = pipe->next;
	else {
		if (pipe->next)
			pipe_list_start = pipe->next;
		else
			pipe_list_start = NULL;
	}

	{
		pipe_buffer_t *tmp;
		pipe_buffer_t *p = pipe->buffer_list_start;
		
		while (p) {
			tmp = p;
			p = p->next;
			kfree (tmp);
		}
	}

	kfree (pipe);
}

void pipe_close (int fd) 
{
	pipe_t *p = pipe_list_start;
	
	if (!p)
		return;
	
	while (true) {
		if (p->fd_a == fd) {
			fd_delete (fd_get (fd));
			p->fd_a = -1;
			if (0 > p->fd_b)
				pipe_remove(p);
			return;
		}		
                if (p->fd_b == fd) {
			fd_delete (fd_get (fd));
                        p->fd_b = -1;
                        if (0 > p->fd_a)
                                pipe_remove(p);
			return;
                }

		if (!p->next)
			break;
		else
			p = p->next;
	}

	return;
}
