/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <snd/audio.h>

int audio_write (snd_audio_t *aud, char *buf, unsigned len)
{
	int *ret = 0;

	asm volatile (
		"movl $61, %%eax;"
	     	"movl %1, %%ebx;"
	     	"movl %2, %%ecx;"
	     	"movl %3, %%edx;"
	     	"int $0x80;"
		"movl %%eax, %0;": "=g" (ret) : "b" (aud), "g" (buf), "g" (len) : "%eax", "%ecx", "%edx", "memory");

	return *ret;
}
