/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <build.h>
#include <proc.h>

#define SWMEM_PAGE	0x2000

#define SWMEM_LOW	0x500000
#define SWMEM_HIGH	0x800000

#define SWMEM_ARRAY	(SWMEM_HIGH - SWMEM_LOW) / SWMEM_PAGE

#define SWMEM_PAGE_FREE	0x0
#define SWMEM_PAGE_USED 0x1

unsigned char swmempage[SWMEM_ARRAY];

void *swmalloc (unsigned size)
{
	unsigned i, y, r;

	for (i = 0; i < SWMEM_ARRAY; i ++) {
		if (swmempage[i] == SWMEM_PAGE_FREE) {	/* FREE */
			r = 0;
			if (size > SWMEM_PAGE) {
				for (y = 0; y < (size/SWMEM_PAGE)+1; y ++) {
					if (swmempage[i+y] == SWMEM_PAGE_FREE)
						swmempage[i+y] = SWMEM_PAGE_USED;
					else {
						r = 1;
						break;
					}

					if (r)
						break;
				}
			} else
				swmempage[i] = SWMEM_PAGE_USED;

			if (r) {
				i += y;
				continue;
			}

			return (void *) ((i * SWMEM_PAGE) + SWMEM_LOW);
		}
	}

	return (void *) 0;
}

unsigned swfree (void *ptr, unsigned size)
{
	unsigned i = ((unsigned) ptr - SWMEM_LOW) / SWMEM_PAGE;
	unsigned y;

	if (swmempage[i] != SWMEM_PAGE_USED)
		return 0;

	if (size > SWMEM_PAGE) {
		for (y = 0; y < (size/SWMEM_PAGE)+1; y ++) {
			if (swmempage[i+y] == SWMEM_PAGE_USED)
				swmempage[i+y] = SWMEM_PAGE_FREE;
			else
				return 0;
		}
	} else
		swmempage[i] = SWMEM_PAGE_FREE;

	return 1;
}

unsigned int swmem_init ()
{
	memset (swmempage, 0, SWMEM_ARRAY);

	return 1;
}
