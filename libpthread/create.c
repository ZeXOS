/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <pthread.h>

int pthread_create (pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg)
{
	*SYSV_THREADOPEN = (unsigned) arg;

	asm volatile (
		"movl $50, %%eax;"
	     	"movl %0, %%ebx;"
	     	"int $0x80;" :: "b" (start_routine) : "%eax", "memory");

	unsigned *ret = SYSV_THREADOPEN;

	if (!ret)
		return -1;

	thread->start_routine = start_routine;
	thread->arg = arg;
	thread->attr = (pthread_attr_t *) attr;

	if (*ret == 1)
		return 0;

	return -1;
}
