/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xmpp.h"
#include "cmds.h"

char *get_user ()
{
	printf ("Your username: ");
	
	char buf[80];
	scanf ("%s", buf);
	
	return strdup (buf);
}

char *get_pwd ()
{
	printf ("Password: ");
	
	char buf[80];
	scanf ("%s", buf);
	
	return strdup (buf);
}

int main (int argc, char **argv)
{
	printf ("zjab\n");
 
	if (argc < 2) {
		printf ("syntax: exec zjab <server> [port]\n");
		return -1;
	}
	
	char *server = argv[1];
	int port = argc > 2 ? atoi (argv[2]) : 5222;
	
	/* get username and password from keyboard */
	char *user = get_user ();
	char *pwd = get_pwd ();
	
	if (!user || !pwd)
		return -1;
	
	xmpp_setup (user, pwd, "ZeXOS");
	
	/* connect to jabber server */
	if (xmpp_connect (server, port) == -1)
		goto clean;
	
	/* login */
	if (cmds_init () == -1)
		return -1;
	
	/* main loop */
	xmpp_loop ();
	
	xmpp_close ();
clean:
	free (pwd);
	free (user);
	
	return 0;
}
