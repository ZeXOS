/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <time.h>

/* use BCD mode, since binary mode seems to be buggy */
static unsigned read_register (unsigned reg)
{
	unsigned high_digit, low_digit;

	outb (0x70, reg);
	high_digit = low_digit = inb (0x71);

	/* convert from BCD to binary */
	high_digit >>= 4;
	high_digit &= 0x0F;
	low_digit &= 0x0F;
	return 10 * high_digit + low_digit;
}

/*****************************************************************************
Finds the number of days between two dates in the Gregorian calendar.
- it's a leap year if the year is divisible by 4,
    - UNLESS the year is also divisible by 100,
	- UNLESS the year is also divisible by 400

To compute Julian Day Number (JDN;
days since Nov 24, 4714 BC/BCE in Gregorian calendar):
	days_between_dates(-4713, 327, curr_day_in_year, curr_year);

To compute days since Jan 1, 1970 (UNIX epoch):
	days_between_dates(1970, 0, curr_day_in_year, curr_year);
or
    days_between_dates(-4713, 327, curr_day_in_year, curr_year) + 2440588L;

This code divides the time between start_day/start_year and end_day/end_year
into "slices": fourcent (400-year) slices in the middle, bracketed on
either end by century slices, fouryear (4-year) slices, and year slices.

When used to compute JDN, this code produces the same results as the
code shown here:
	http://serendipity.magnet.ch/hermetic/cal_stud/jdn.htm

IMHO, it's easier to see how the algorithm for this code works,
versus the code at the URL above.
*****************************************************************************/
static long days_between_dates(unsigned start_year, unsigned start_day,
		int end_year, unsigned end_day)
{
	int fourcents, centuries, fouryears, years;
	long days;

	fourcents = end_year / 400 - start_year / 400;
	centuries = end_year / 100 - start_year / 100 -
	/* subtract from 'centuries' the centuries already accounted for by
	'fourcents' */
		fourcents * 4;
	fouryears = end_year / 4 - start_year / 4 -
	/* subtract from 'fouryears' the fouryears already accounted for by
	'fourcents' and 'centuries' */
		fourcents * 100 - centuries * 25;
	years = end_year - start_year -
	/* subtract from 'years' the years already accounted for by
	'fourcents', 'centuries', and 'fouryears' */
		400 * fourcents - 100 * centuries - 4 * fouryears;
	/* add it up: 97 leap days every fourcent */
	days = (365L * 400 + 97) * fourcents;
	/* 24 leap days every residual century */
	days += (365L * 100 + 24) * centuries;
	/* 1 leap day every residual fouryear */
	days += (365L * 4 + 1) * fouryears;
	/* 0 leap days for residual years */
	days += (365L * 1) * years;
	/* residual days (need the cast!) */
	days += ((long)end_day - start_day);

	/* account for terminal leap year */
	if(end_year % 4 == 0 && end_day >= 60)
	{
		days++;
		if(end_year % 100 == 0)
			days--;
		if(end_year % 400 == 0)
			days++;
	}

	/* xxx - what have I wrought? I don't know what's going on here,
	but the code won't work properly without it */
	if(end_year >= 0)
	{
		days++;
		if(end_year % 4 == 0)
			days--;
		if(end_year % 100 == 0)
			days++;
		if(end_year % 400 == 0)
			days--;
	}

	if(start_year > 0)
		days--;

	return days;
}

/*****************************************************************************
		month and date start with 1, not with 0
*****************************************************************************/
#define	EPOCH_YEAR	1970
#define	EPOCH_DAY	0 /* Jan 1 */

unsigned long date_time_to_time_t(unsigned year, unsigned month,
		unsigned date, unsigned hour, unsigned min,
		unsigned sec)
{
	static const unsigned days_to_date[12] =
	{
	/*      jan  feb  mar  apr  may  jun  jul  aug  sep  oct  nov  dec */
		0,
		31,
		31 + 28,
		31 + 28 + 31,
		31 + 28 + 31 + 30,
		31 + 28 + 31 + 30 + 31,
		31 + 28 + 31 + 30 + 31 + 30,
		31 + 28 + 31 + 30 + 31 + 30 + 31,
		31 + 28 + 31 + 30 + 31 + 30 + 31 + 31,
		31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30,
		31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31,
		31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30
	};

	unsigned long ret_val;
	unsigned day;

	/* convert month and year to day-in-year */
	if(month < 1 || month > 12 || date < 1 || date > 31)
		return 0;

	month--;
	date--;
	day = date + days_to_date[month];

	/* convert to Unix JDN (UJDN) */
	ret_val = days_between_dates(EPOCH_YEAR, EPOCH_DAY, year, day);

	/* convert from days to seconds, adding time as you go */
	ret_val *= 24;
	ret_val += hour;
	ret_val *= 60;
	ret_val += min;
	ret_val *= 60;
	ret_val += sec;

	return ret_val;
}

/*****************************************************************************
NOTE: this function works only with local time, stored in the CMOS clock.
It knows nothing of GMT or timezones. This is a feature, not a bug :)
*****************************************************************************/
tm *rtc_getcurrtime ()
{
	static char init;
	unsigned date, month, hour, minute, second;
	int year;

	if (!init)
	{
		/* b2=0 BCD mode, vs. binary (binary mode seems to be buggy)
		b1=1	24-hour mode, vs. 12-hour mode */
		outb (0x70, 11);
		outb (0x71, (inb (0x71) & ~6) | 2);
		init = 1;
	}
	/* wait for stored time value to stop changing */
	outb(0x70, 10);

	while (inb (0x71) & 128);

	/* get year/month/date
		year = read_register (9) + 1900;	xxx - OH NO, Y2K!
		year = read_register (9) + 2000;
	use the Microsoft method -- this should be good from 1970-2069
	signed 32-bit time_t will overflow before then, in 2038 */

	year = read_register (9);	/* 0-99 */

	if(year < 70)
		year += 2000;
	else
		year += 1900;

	month = read_register (8);	/* 1-12 */
	date = read_register (7);	/* 1-31 */
	/* get time */
	hour = read_register (4);	/* 0-23 */
	minute = read_register (2);	/* 0-59 */
	second = read_register (0);	/* 0-59 */

	realtime->tm_sec = second;
	realtime->tm_min = minute;
	realtime->tm_hour = hour;
	realtime->tm_mday = date;
	realtime->tm_mon = month;
	realtime->tm_year = year;
	realtime->tm_isdst = 0; 
	realtime->__tm_gmtoff = date_time_to_time_t (year, month, date, hour, minute, second);

	return realtime;
}
