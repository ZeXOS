/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "net.h"
#include "xml.h"
#include "xmpp.h"
#include "cmds.h"
#include "utils.h"

static xmpp_t xmpp;

int xmpp_session ()
{
	char buf[256];

	int r = sprintf (buf, "<iq xmlns='jabber:client' type='set' id='%s_1'><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/></iq>", xmpp.session);

	net_send (buf, r);

	return 0;
}
 
int xmpp_bind ()
{
	char buf[256];
	
	int r = sprintf (buf, "<iq type='set' id='%s'><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'><resource>%s</resource></bind></iq>", xmpp.session, xmpp.resource);
  
	net_send (buf, r);
	
	return 0;
}
 
int xmpp_presence (char *show, char *status)
{
	char buf[256];
	
	int r = sprintf (buf, "<presence type='available'><show>%s</show><status>%s</status></presence>", show, status);
  
	net_send (buf, r);
	
	return 0;
}

int xmpp_message (char *to, char *message, unsigned len)
{
	if (len > 480)
		return -1;
	
	char buf[512];
	
	int r = sprintf (buf, "<message from='%s@%s' to='%s' type='chat'><body>%s</body></message>", xmpp.user, xmpp.server, to, message);
  
	net_send (buf, r);
  
	return 0;
}

void xmpp_setup (char *user, char *password, char *resource)
{
	xmpp.user = user;
	xmpp.password = password;
	xmpp.resource = resource;
}

int xmpp_connect (char *server, int port)
{
	if (net_connect (server, port) == -1)
		return -1;

	xmpp.session = 0;
	xmpp.server = server;
	xmpp.port = port;
	xmpp.mechanism = 0;
	
	char buf[512];
	
	int r = sprintf (buf, "<?xml version='1.0'?><stream:stream to='%s' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>", server);
	
	net_send (buf, r);
	
	int len = net_recv (buf, 511);
	
	if (len > 0) {
		buf[len] = '\0';
		xml_handler (buf, len);
	}

	if (xmpp.mechanism)
		goto success;
	
	len = net_recv (buf, 511);
	
	if (len > 0) {
		buf[len] = '\0';
		xml_handler (buf, len);
	}
success:
	r = sprintf (buf, "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='%s'/>", xmpp.mechanism);
	
	net_send (buf, r);
	
	len = net_recv (buf, 511);
	
	if (len > 0) {
		buf[len] = '\0';
		xml_handler (buf, len);
	}

	unsigned user_len = strlen (xmpp.user);
	unsigned pwd_len = strlen (xmpp.password);
	
	buf[0] = '\0';
	strcpy (buf+1, xmpp.user);
	buf[1+user_len] = '\0';
	strcpy (buf+2+user_len, xmpp.password);
	
	r = sprintf (buf, "<response xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>%s</response>", base64 (buf, 2+user_len+pwd_len));
		
	net_send (buf, r);

	len = net_recv (buf, 511);
	
	if (len > 0) {
		buf[len] = '\0';
		xml_handler (buf, len);
	}
	
	r = sprintf (buf, "<?xml version='1.0'?><stream:stream to='%s' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>", server);
	
	net_send (buf, r);
	
	len = net_recv (buf, 511);

	if (len > 0) {
		buf[len] = '\0';
		xml_handler (buf, len);
	}
	
	len = net_recv (buf, 511);

	if (len > 0) {
		buf[len] = '\0';
		xml_handler (buf, len);
	}
	
	xmpp_bind ();
	
	len = net_recv (buf, 511);

	if (len > 0) {
		buf[len] = '\0';
		xml_handler (buf, len);
	}
	
	xmpp_session ();
	
	len = net_recv (buf, 511);

	if (len > 0) {
		buf[len] = '\0';
		xml_handler (buf, len);
	}
	
	xmpp_presence ("online", "ZeXOS");
	
	return 0;
}

int xmpp_loop ()
{
	if (net_nonblock () == -1)
		return -1;
  
	for (;;) {
		char buf[1024];
		
		int len = net_recv (buf, 1023);

		if (len > 0) {
			buf[len] = '\0';
			xml_handler (buf, len);
		}
		
		if (cmds_get () == -1)
			break;
#ifndef LINUX
		schedule ();
#endif
	}

	return 0;
}

int xmpp_close ()
{
	net_send ("</stream>", 9);
  
	printf ("> Disconnected from server\n");
	
	if (xmpp.mechanism)
		free (xmpp.mechanism);
	
	return net_close ();
}

int xmpp_session_set (char *id, unsigned len)
{
	if (!id || !len)
		return -1;
  
	if (xmpp.session)
		free (xmpp.session);
	
	xmpp.session = strndup (id, len);
  
	if (!xmpp.session)
		return -1;
	
//	printf ("XMPP -> session id: '%s'\n", xmpp.session);
	
	return 0;
}

int xmpp_mechanism_set (char *mechanism, unsigned len)
{
	if (!mechanism || !len)
		return -1;
  
	if (!strncmp (mechanism, "PLAIN", 5))
		xmpp.mechanism = strndup (mechanism, 5);
	if (!strncmp (mechanism, "DIGEST-MD5", 9))
		return 0; /*TODO */
	
	if (!xmpp.mechanism)
		return -1;
	
//	printf ("XMPP -> mechanism: '%s'\n", xmpp.mechanism);
	
	return 0;
}

int xmpp_message_from (char *from, unsigned from_len, char *msg, unsigned msg_len)
{
 	setcolor (15, 0);
	
	unsigned i;
	for (i = 0; i < from_len; i ++)
		putchar (from[i]);
	
	printf (": ");
	
	setcolor (14, 0);
	
	for (i = 0; i < msg_len; i ++)
		putchar (msg[i]);
	
	putchar ('\n');
	
	setcolor (7, 0);

	return 0;
}
