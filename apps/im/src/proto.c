/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "platform.h"
#include "config.h"
#include "user.h"
#include "net.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
"REGISTER <name> <password> -- registruje noveho uzivatela\n"
"LOGIN <name> <password> -- prihasuje uzivatela\n"
"LOGOUT -- odhasuje uzivatela\n"
"LIST -- vypise zoznam kontaktov\n"
"ADD <name> -- prida uzivatela do zoznamu kontaktov\n"
"DEL <name> -- odoberie uzivatela zo zoznamu kontaktov\n"
"STATUS <new_status> -- zmeni status uzivatelovy\n"
"MSG <name> <message> -- odosle spravu uzivatelovy\n";
*/

struct proto_cmd_t {
	char *name;
	unsigned char len;
};

static struct proto_cmd_t proto_cmd[] = {
	{ "REGISTER", 8 },
	{ "LOGIN", 5 },
	{ "LOGOUT", 6 },
	{ "LIST", 4 },
	{ "ADD", 3 },
	{ "DEL", 3 },
	{ "STATUS", 6 },
	{ "MSG", 3 },
	{ "CHANGE", 6 },
	{ "MY:", 3 },
	{ "ERR", 3 },
	{ "OK", 2 },
	{ (char *) NULL, (unsigned char) NULL }
};

int proto_login (char *nick, char *password)
{
	unsigned nick_len = strlen (nick);
	unsigned password_len = strlen (password);

	char *str = (char *) malloc (sizeof (char) * (proto_cmd[1].len + nick_len + password_len + 4));

	if (!str)
		return 0;

	sprintf (str, "%s %s %s\n", proto_cmd[1].name, nick, password);

	net_send (str, proto_cmd[1].len + nick_len + password_len + 3);

	free (str);

	return 1;
}

int proto_msg (char *target, char *msg, unsigned len)
{
	unsigned target_len = strlen (target);

	char *str = (char *) malloc (sizeof (char) * (proto_cmd[7].len + target_len + len + 4));

	if (!str)
		return 0;

	sprintf (str, "%s %s %s\n", proto_cmd[7].name, target, msg);

	net_send (str, proto_cmd[7].len + target_len + len + 3);

	free (str);

	return 1;
}

int proto_add (char *target)
{
	unsigned target_len = strlen (target);

	char *str = (char *) malloc (sizeof (char) * (proto_cmd[4].len + target_len + 3));

	if (!str)
		return 0;

	sprintf (str, "%s %s\n", proto_cmd[4].name, target);

	net_send (str, proto_cmd[4].len + target_len + 2);

	free (str);

	return 1;
}

int proto_del (char *target)
{
	unsigned target_len = strlen (target);

	char *str = (char *) malloc (sizeof (char) * (proto_cmd[5].len + target_len + 3));

	if (!str)
		return 0;

	sprintf (str, "%s %s\n", proto_cmd[5].name, target);

	net_send (str, proto_cmd[5].len + target_len + 2);

	free (str);

	return 1;
}

int proto_list ()
{
	char *str = (char *) malloc (sizeof (char) * (proto_cmd[3].len + 2));

	if (!str)
		return 0;

	sprintf (str, "%s\n", proto_cmd[3].name);

	net_send (str, proto_cmd[3].len + 1);

	free (str);

	return 1;
}

int proto_status (char *msg, unsigned len)
{
	char *str = (char *) malloc (sizeof (char) * (proto_cmd[6].len + len + 3));

	if (!str)
		return 0;

	sprintf (str, "%s %s\n", proto_cmd[6].name, msg);

	net_send (str, proto_cmd[6].len + len + 2);

	free (str);

	return 1;
}

int proto_register (char *nick, char *password)
{
	unsigned nick_len = strlen (nick);
	unsigned password_len = strlen (password);

	if (!nick_len || !password_len)
		return 0;

	char *str = (char *) malloc (sizeof (char) * (proto_cmd[0].len + nick_len + password_len + 4));

	if (!str)
		return 0;

	sprintf (str, "%s %s %s\n", proto_cmd[0].name, nick, password);

	net_send (str, proto_cmd[0].len + nick_len + password_len + 3);

	free (str);

	return 1;
}

int proto_handler (char *str, unsigned len)
{
	if (!strncmp (str, proto_cmd[8].name, proto_cmd[8].len))
		return user_statusincoming (str+4, len-4);

	if (!strncmp (str, proto_cmd[7].name, proto_cmd[7].len))
		return user_msgincoming (str+4, len-4);

	if (!strncmp (str, proto_cmd[9].name, proto_cmd[9].len))
		return user_list_get (str+4, len-4);

	if (!strncmp (str, proto_cmd[10].name, proto_cmd[10].len))
		return user_error (str+4, len-4);

	if (!strncmp (str, proto_cmd[11].name, proto_cmd[11].len))
		return user_info (str+3, len-3);

	return 1;
}

/** INIT PROTOCOL function */
int init_proto ()
{
	/* nick and password are not filled in config */
	if (!cfg->autolog)
		return 1;

	if (!proto_login (cfg->nick, cfg->password))
		return 0;

	if (!proto_list ())
		return 0;

	return 1;
}
