/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _MODULE_H
#define _MODULE_H

#include <task.h>

/* Module structure */
typedef struct module_context {
  struct module_context *next, *prev;

  char *name;
  char *image;
  task_t *task;
  unsigned mem_usage;
} module_t;

#define KMOD_MODULE_PRIORITY		255

/* externs */
extern void module_display ();
extern module_t *module_find (task_t *task);
extern unsigned int init_module ();

#endif
 
