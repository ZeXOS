/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <string.h>
#include <dev.h>
#include <fs.h>
#include <partition.h>
#include <net/socket.h>
#include <cache.h>
#include <vfs.h>

#define PROTO_MKDIR	0x1
#define PROTO_RMDIR	0x2
#define PROTO_CREAT	0x3
#define PROTO_RM	0x4
#define PROTO_CHDIR	0x5
#define PROTO_READ	0x6

partition_t *znfs_part;
int sock;
unsigned char znfs_dir_id;

/* clear and pre-set dir[] structure */
void znfs_dir_flush (unsigned n)
{
	strcpy (dir[0].name, ".");
	strcpy (dir[1].name, "..");

	znfs_dir_id = 2;

	unsigned i = znfs_dir_id;

	for (; i < 223; i ++)
		memset (dir[i].name, 0, 10);
}

int znfs_send (int fd, unsigned char cmd, char *data, unsigned len)
{
	char *s = kmalloc (sizeof (char) * len + 2);

	if (!s)
		return 0;

	s[0] = cmd;
	memcpy (s+1, data, len);

	int ret = send (fd, s, len+1, 0);

	kfree (s);

	return ret;
}

int znfs_recv (int fd, char *data, unsigned len)
{
	return recv (fd, data, len, 0);
}

/* function for read file content */
unsigned znfs_read (partition_t *p, unsigned char *name, vfs_content_t *content)
{
	unsigned char l = strlen (name);

	if (l > 10)
		l = 10;

	char data[12];

	data[0] = l;
	memcpy (data+1, name, l);

	// Set socket to blocking mode
	int oldFlag = fcntl (sock, F_GETFL, 0);
	oldFlag &= ~O_NONBLOCK;

	if (fcntl (sock, F_SETFL, oldFlag) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	znfs_send (sock, PROTO_READ, data, l+1);

	int r = 0;

	cache_t *cache = cache_create (0, 0, 0);

	if (!cache)
		return 0;

	char block[513];
	
	while (1) {
		r = znfs_recv (sock, block, 512);

		cache_add (block, r);

		if (r != 512)
			break;
	}

	// Set socket to non-blocking mode
	oldFlag = fcntl (sock, F_GETFL, 0);
	if (fcntl (sock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	content->ptr = (char *) &cache->data;
	content->len = cache->limit;

	return 1;
}

/* function for create clear directory */
unsigned znfs_create_directory (partition_t *p, unsigned char *name)
{
	unsigned char l = strlen (name);

	if (l > 10)
		l = 10;

	char data[12];

	data[0] = l;
	memcpy (data+1, name, l);

	znfs_send (sock, PROTO_MKDIR, data, l+1);

	
	memcpy (dir[znfs_dir_id].name, name, l);
	dir[znfs_dir_id].name[l] = '\0';

	dir[znfs_dir_id].dir = 1;

	znfs_dir_id ++;

	return 1;
}

/* function for create blank file */
unsigned znfs_create_file (partition_t *p, unsigned char *name)
{
	unsigned char l = strlen (name);

	if (l > 10)
		l = 10;

	char data[12];

	data[0] = l;
	memcpy (data+1, name, l);

	znfs_send (sock, PROTO_CREAT, data, l+1);

	memcpy (dir[znfs_dir_id].name, name, l);
	dir[znfs_dir_id].name[l] = '\0';

	dir[znfs_dir_id].dir = 0;

	znfs_dir_id ++;

	return 1;
}

/* go to next directory placed in current directory */
void znfs_cd (partition_t *p, unsigned char *name)
{
	unsigned short l = strlen (name);

	if (l > 1023)
		l = 1023;

	char data[1025];

	data[0] = l;
	memcpy (data+1, name, l);

	znfs_send (sock, PROTO_CHDIR, data, l+1);

	znfs_dir_flush (1);

	// Set socket to blocking mode
	int oldFlag = fcntl (sock, F_GETFL, 0);
	oldFlag &= ~O_NONBLOCK;

	if (fcntl (sock, F_SETFL, oldFlag) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
	}

	int r = znfs_recv (sock, data, 1024);

	// Set socket to non-blocking mode
	oldFlag = fcntl (sock, F_GETFL, 0);
	if (fcntl (sock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
	}

	if (r < 1) {
		kprintf ("ZNFS -> error in receiving directory list\n");
		return;
	}

	unsigned char n = r/13;

	struct dent {
		unsigned char type;
		char name[12];
	} __attribute__ ((__packed__));

	struct dent *d = (struct dent *) data;

	for (l = 0; l < n; l ++) {
		memcpy (dir[l].name, d[l].name, 12);

		if (d[l].type == 0x4)
			dir[l].dir = 1;
		else
			dir[l].dir = 0;
	}
}

/* go to root '/' directory on partition */
void znfs_cd_root (partition_t *p)
{
	char data[1025];

	data[0] = 1;
	data[1] = '/';

	znfs_send (sock, PROTO_CHDIR, data, 2);

	znfs_dir_flush (1);

	// Set socket to blocking mode
	int oldFlag = fcntl (sock, F_GETFL, 0);
	oldFlag &= ~O_NONBLOCK;

	if (fcntl (sock, F_SETFL, oldFlag) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
	}

	int r = znfs_recv (sock, data, 1024);

	// Set socket to non-blocking mode
	oldFlag = fcntl (sock, F_GETFL, 0);
	if (fcntl (sock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
	}

	if (r < 1) {
		kprintf ("ZNFS -> error in receiving directory list\n");
		return;
	}

	unsigned char n = r/13;
	unsigned char l;

	struct dent {
		unsigned char type;
		char name[12];
	} __attribute__ ((__packed__));

	struct dent *d = (struct dent *) data;

	for (l = 0; l < n; l ++) {
		memcpy (dir[l].name, d[l].name, 12);

		if (d[l].type == 0x4)
			dir[l].dir = 1;
		else
			dir[l].dir = 0;
	}
}

/* zexos filesystem initialization function */
unsigned znfs_init (partition_t *p, char *addr)
{
#ifdef CONFIG_DRV_ZNFS
	znfs_part =  p;

	int port = 5333;
	hostent *host;
	sockaddr_in serverSock;

	// Let's get info about remote computer
	if ((host = gethostbyname ((char *) addr)) == NULL) {
		printf ("Wrong address -> %s:%d\n", addr, port);
		return 0;
	}

	// Create socket
	if ((sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return 0;
	}
	
	// Fill structure sockaddr_in
	// 1) Family of protocols
	serverSock.sin_family = AF_INET;
	// 2) Number of server port
	serverSock.sin_port = swap16 (port);
	// 3) Setup ip address of server, where we want to connect
	memcpy (&(serverSock.sin_addr), host->h_addr, host->h_length);

	kfree (host);

	// Now we are able to connect to remote server
	if (connect (sock, (void *) &serverSock, sizeof (serverSock)) == -1) {
		printf ("Connection cant be estabilished -> %s:%d\n", addr, port);
		return -1;
	}

	// Set socket to non-blocking mode
	int oldFlag = fcntl (sock, F_GETFL, 0);
	if (fcntl (sock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	znfs_dir_id = 0;
#endif
	return 1;
}

/* handler which is used by device management and pretty needed
	 for communication between zexos and this filesystem */
bool znfs_handler (unsigned act, char *block, unsigned n, unsigned long l)
{
	switch (act) {
		case FS_ACT_INIT:
		{
			return 1;
		}
		break;
		case FS_ACT_READ:
		{
			return znfs_read (curr_part, dir[n].name, (vfs_content_t *) block);
		}
		break;
		case FS_ACT_WRITE:
		{
			/* TODO */
			//znfs_write (curr_part, block);
	
			return 0;
		}
		break;
		case FS_ACT_CHDIR:
		{
			if (n == -1) {
				znfs_cd_root (curr_part);
				return 1;
			}

			/* re-load content of current directory */
			if (n == 0)
				return 1;

			/* go to previous directory */
			if (n == 1) {
				znfs_cd (curr_part, "..");
				return 1;
			}

			/* go to another directory */
			znfs_cd (curr_part, dir[n].name);

			return 1;
		}
		break;
		case FS_ACT_MKDIR:
		{
			znfs_create_directory (curr_part, block);

			return 1;
		}
		break;
		case FS_ACT_TOUCH:
		{
			znfs_create_file (curr_part, block);

			return 1;
		}
	}

	return 0;
}
#endif
