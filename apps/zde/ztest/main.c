/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <appcl.h>

void ztest_draw ()
{
	zgui_puts (80, 80, "Hello World from ZTEST APP !", 0);
}

int main (int argc, char **argv)
{
	int exit = 0;

	if (zgui_init () == -1)
		return -1;

	while (!exit) {
		unsigned state = zgui_event ();

		if (state & APPCL_STATE_REDRAW)
			ztest_draw ();

		if (state & APPCL_STATE_EXIT)
			exit = 1;
	}

	return zgui_exit ();
}
 
