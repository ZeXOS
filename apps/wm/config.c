/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "config.h"

config_t *cfg;

void config_set (char *variable, char *value)
{
	puts ("config -> ");
	puts (variable);
	puts (" : ");
	puts (value);
	puts ("\n");

	if (!strcmp (variable, "wm_mouse"))
		cfg->mouse = atoi (value);
}

void config_parse (char *cfgfile)
{
	unsigned i = 0, y = strlen (cfgfile), z = 0;
	unsigned a = 0;

	while (i < y) {
		if (cfgfile[i] == '\n') {
			char var[16];
			char val[32];

			a = 0;

			while (a < 16) {
				if (cfgfile[a+z] == ' ')
					break;

				a ++;
			}

			if (a >= 15)
				continue;

			memcpy (var, cfgfile+z, a);
			var[a] = '\0';

			a ++;

			memcpy (val, cfgfile+z+a, i-z-a);
			val[i-z-a] = '\0';

			config_set (var, val);

			i ++;

			z = i;
		}

		i ++;
	}
}

unsigned init_config ()
{
	char *cfgfile = (char *) malloc (sizeof (char) * 2048);

	if (!cfgfile)
		return 0;

	// html web page
	int fd = open ("config", O_RDONLY);
	
	if (!fd) {
		puts ("error -> file 'config' not found\n");
		return 0;
	}
	
	if (!read (fd, cfgfile, 2048)) {
		puts ("error -> something was wrong !\n");
		return 0;
	}

	cfgfile[2048] = '\0';

	cfg = (config_t *) malloc (sizeof (config_t));

	if (!cfg)
		return 0;

	config_parse (cfgfile);

	return 1;
}

