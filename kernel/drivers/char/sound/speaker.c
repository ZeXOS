/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <dev.h>
#include <arch/io.h>
#include <speaker.h>

#define	PIT_TICK_RATE		1193182UL

static void pcspkr_do_sound (unsigned int freq)
{
	unsigned int count = 0;

	if (freq)
		count = PIT_TICK_RATE/freq;
	else
		count = 0;	// disable sound

	if (count) {
		/* enable counter 2 */
		outb_p(inb_p(0x61) | 3, 0x61);
		/* set command for counter 2, 2 byte write */
		outb_p(0xB6, 0x43);
		/* select desired HZ */
		outb_p(count & 0xff, 0x42);
		outb((count >> 8) & 0xff, 0x42);
	} else {
		/* disable counter 2 */
		outb(inb_p(0x61) & 0xFC, 0x61);
	}
}

bool pcspk_acthandler (unsigned act, unsigned freq)
{
	switch (act) {
		case DEV_ACT_INIT:
		case DEV_ACT_STOP:
		{
			pcspkr_do_sound (0);

			return 1;
		}
		break;
		case DEV_ACT_PLAY:
		{
			pcspkr_do_sound (freq);

			return 1;
		}
		break;
	}

	return 0;
}
#endif
