/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <config.h>
#include <build.h>
#include <cache.h>
#include <task.h>
#include <proc.h>

static cache_t *cache_last;

cache_t *cache_create (char *buf, unsigned len, unsigned char prealloc)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		proc = (proc_t *) &proc_kernel;
#ifdef ARCH_i386
	paging_disable ();
#endif
	char *ptr = (char *) umalloc (proc, sizeof (cache_t) + len);
#ifdef ARCH_i386
	paging_enable ();
#endif
	cache_t *cache = (cache_t *) ptr;

	if (!cache)
		goto ret;

	cache->magic = CACHE_MAGIC;
	cache->prealloc = prealloc;
	cache->limit = len;
	cache->curr = 0;

	if (len && buf)
		memcpy (&cache->data, buf, len);
	else
		cache->data = '\0';

	cache_last = cache;
ret:
	return (cache_t *) ((char *) cache);
}

cache_t *cache_add (char *buf, unsigned len)
{
	if (!cache_last)
		return 0;

	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		proc = (proc_t *) &proc_kernel;

	unsigned l = cache_last->limit;
	char *p = (char *) &cache_last->data;

	if (!cache_last->prealloc) {
#ifdef ARCH_i386
		paging_disable ();
#endif
		cache_last = (cache_t *) urealloc (proc, p - sizeof (cache_t) + 4, l + len + sizeof (cache_t));
#ifdef ARCH_i386
		paging_enable ();
#endif
		if (!cache_last)
			return 0;

		p = (char *) &cache_last->data;

		memcpy (p + l, buf, len);
		
		cache_last->limit += len;
	} else 
		memcpy (p + cache_last->curr, buf, len);
	
	cache_last->curr += len;

	return cache_last;
}

cache_t *cache_read ()
{
	if (!cache_last)
		return 0;

	proc_t *proc = proc_find (_curr_task);

	cache_t *cache = (cache_t *) cache_last;

	if (cache->magic != CACHE_MAGIC)
		return 0;

	cache_last = 0;

	if (!proc)
		proc = (proc_t *) &proc_kernel;

	return (cache_t *) ((char *) cache);
}

int cache_close (cache_t *cache)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		proc = (proc_t *) &proc_kernel;

	/*if (cache->magic != CACHE_MAGIC)
		return 0;*/
#ifdef ARCH_i386
	paging_disable ();
#endif
	ufree (proc, (cache_t *) ((char *) cache));
#ifdef ARCH_i386
	paging_enable ();
#endif
	return 1;
}

int cache_closebyptr (void *ptr)
{
	char *cache = (char *) ptr - sizeof (cache_t) + 4;
	
	return cache_close ((cache_t *) cache);
}

unsigned int init_cache ()
{
	cache_last = 0;
}