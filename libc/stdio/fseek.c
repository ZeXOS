/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <unistd.h>

int fseek (FILE *stream, long offset, int whence)
{
	if (!stream)
		return -1;

	long r = lseek (stream->fd, offset, whence);

	if (r > 0)
		stream->off = r;

	if (whence == SEEK_END)
		return 0;

	return r;
}

int fseeko (FILE *stream, off_t offset, int whence)
{
	if (!stream)
		return -1;

	return lseek (stream->fd, offset, whence);
}