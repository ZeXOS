/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <config.h>
#include <task.h>

unsigned short *vesafb = 0;
extern unsigned char vgagui;

typedef struct PMInfoBlock {
	char Signature[4];
	unsigned short EntryPoint;
	unsigned short PMInitialize;
	unsigned short BIOSDataSel;
	unsigned short A0000Sel;
	unsigned short B0000Sel;
	unsigned short B8000Sel;
	unsigned short CodeSegSel;
	char InProtectedMode;
	char Checksum;
} PMInfoBlock;

typedef void (*void_fn_void_t)(char *arg, int flags);
extern task_t *_curr_task;

unsigned init_video_vesa ()
{
#ifdef CONFIG_DRV_VESA
	if (kernel_attr & KERNEL_NOVESA)
		return 0;

	//prot_to_real ();

	/* get address from hacked grub */
	unsigned *grub_baseaddr = (unsigned *) 0x60000;
	
	vesafb = (unsigned short *) *grub_baseaddr;

	if (!vesafb)
		return 0;

	vgagui = 2;

	//printf ("vesa: 0x%x\n", vesafb);

	/* draw "clear" screen first */
	unsigned p = 0;
	for (p = 0; p < 800*600; p ++)
		vesafb[p] = p/32768;

#endif
	return 1;
}

unsigned vesa_16cto16b (unsigned char color)
{
	switch (color) {
		case 0:
			return 0;
		case 1:
			return 0x0000f1;	// ok
		case 2:
			return 0xee8d00;	// ok
		case 3:
			return 0x808000;	// ok
		case 4:
			return 0xffa000;	// ok
		case 5:
			return 0x018051;	// ok
		case 6:
			return 0xC3993D;	// ??
		case 7:
			return 0xf007fd;	// ??
		case 8:
			return 0xb457fd;	// ??
		case 9:
			return 0x0a02fa;	// ok
		case 10:
			return 0xffef00;	// ok
		case 11:
			return 0x0a05fa;	// ok
		case 12:
			return 0x4f8800;	// ok
		case 13:
			return 0xf8faff;	// ok
		case 14:
			return 0xffffd0;	// ok
		case 15:
			return 0xffffff;	// ok
	}

	return 0;
}
