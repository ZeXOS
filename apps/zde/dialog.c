/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>

#include "kbd.h"
#include "appcl.h"
#include "dialog.h"
#include "cursor.h"
#include "window.h"
#include "appsrv.h"
#include "handler.h"

zde_dobj_t zde_dobj_list;

extern zde_cursor_t *zde_cursor;
extern unsigned short *bitmap_button;
static char mouse_trigger;

zde_dobj_t *create_dialog_object (int type, void *arg, unsigned x, unsigned y, zde_win_t *window)
{
	if (!window)
		return 0;

	if (!arg)
		return 0;

	zde_dobj_t *dobj = (zde_dobj_t *) malloc (sizeof (zde_dobj_t));

	if (!dobj)
		return 0;

	dobj->x = x;
	dobj->y = y;

	switch (type) {
		case ZDE_DOBJ_TYPE_TEXT:
			dobj->arg = malloc (sizeof (zde_dobj_text_t));

			if (!dobj->arg)
				return 0;

			memcpy (dobj->arg, arg, sizeof (zde_dobj_text_t));
			break;
		case ZDE_DOBJ_TYPE_APPCL:
			dobj->arg = malloc (sizeof (zde_dobj_appcl_t));

			if (!dobj->arg)
				return 0;

			memcpy (dobj->arg, arg, sizeof (zde_dobj_appcl_t));
			break;
	}

	dobj->object = (void *) window;
	dobj->type = type;

	/* add into list */
	dobj->next = &zde_dobj_list;
	dobj->prev = zde_dobj_list.prev;
	dobj->prev->next = dobj;
	dobj->next->prev = dobj;

	return dobj;
}

unsigned destroy_dialog_object (zde_win_t *window)
{
	unsigned i;

	while (1) {
		i = 0;

		zde_dobj_t *dobj;
		for (dobj = zde_dobj_list.next; dobj != &zde_dobj_list; dobj = dobj->next) {
			if (dobj->object == (void *) window) {
				dobj->next->prev = dobj->prev;
				dobj->prev->next = dobj->next;

				/* send exit state to client's app */
				if (dobj->type == ZDE_DOBJ_TYPE_APPCL)
					appsrv_exit (dobj->arg);

				free (dobj);

				i = 1;
				break;
			}
		}

		if (!i)
			break;
	}

	return 1;
}
/*
static void draw_bitmap (unsigned short x, unsigned short y)
{
	unsigned i = 0;
	unsigned j = 0;
	unsigned k = 0;
	unsigned pix = 0;

	for (i = 0; i < 32*32; i ++) {
		if (k >= 32) {
			j ++;
			k = 0;
		}
		
		pix = (1024-i)+34;

		if (pix > 2100)
			continue;

		if (bitmap_folder[pix] != (unsigned short) 0)
		if (k <= 32 && j <= 32)
			xpixel ((unsigned) x+k, (unsigned) y+j, (unsigned) bitmap_folder[pix]);

		k ++;
	}
}
*/
int draw_dialog_object (zde_win_t *window)
{
	unsigned x, y;

	zde_dobj_t *dobj;
	for (dobj = zde_dobj_list.next; dobj != &zde_dobj_list; dobj = dobj->next) {
		if (dobj->object != (void *) window)
			continue;

		/* update dobj position */
		x = window->x + dobj->x + 4;
		y = window->y + dobj->y + 25;

		/* we click on left button */
		/*if (zde_cursor->state == XCURSOR_STATE_LBUTTON)
		if (!(zde_cursor->flags & CURSOR_FLAG_MOVE)) {
			if (zde_cursor->x >= dobj->x && zde_cursor->x <= dobj->x+32)
			if (zde_cursor->y >= dobj->y && zde_cursor->y <= dobj->y+32) {
				//zde_cursor->state = 0;

				//pthread_t thread;

				//pthread_create (&thread, NULL, dobj->entry, dobj->arg);
			}
		}*/

		zde_dobj_text_t *dobj_text;
		zde_dobj_appcl_t *dobj_appcl;
		unsigned i;
		unsigned j = 0;
		unsigned k = 0;

		switch (dobj->type) {
			case ZDE_DOBJ_TYPE_TEXT:
				dobj_text = (zde_dobj_text_t *) dobj->arg;

				if (!dobj_text)
					break;

				for (i = 0; i < dobj_text->len; i ++) {
					if (dobj_text->data[i] == '\n') {
						j += 6;
						k = 0;
						continue;
					} else
					if (dobj_text->data[i] == '\t') {
						k += 4*5;
						continue;
					}

					if (!dobj_text->sizex && window->sizex < k+15) {
						j += 6;
						k = 0;
						continue;
					}
 
					if (!dobj_text->sizey && j < window->sizey-21)
						xtext_putch (x+k, y+j, 0, dobj_text->data[i]);

					k += 6;
				}

				break;
			case ZDE_DOBJ_TYPE_APPCL:
				dobj_appcl = (zde_dobj_appcl_t *) dobj->arg;

				if (!dobj_appcl)
					break;

				/* setup windows properties*/
				dobj_appcl->x = window->x;
				dobj_appcl->y = window->y;
				dobj_appcl->sizex = window->sizex;
				dobj_appcl->sizey = window->sizey;
				dobj_appcl->mousex = (short) (zde_cursor->x - window->x);
				dobj_appcl->mousey = (short) (zde_cursor->y - window->y);

				if (isactive_window () == window) {
					if (!mouse_trigger) {
						if (zde_cursor->state == XCURSOR_STATE_LBUTTON) {
							dobj_appcl->state |= APPCL_STATE_LBUTTON;
							mouse_trigger = 1;
						}
					}
					
					if (!dobj_appcl->kbd)
						dobj_appcl->kbd = kbd_getkey ();
				}
				
				if (zde_cursor->state != XCURSOR_STATE_LBUTTON) {
					mouse_trigger = 0;
					dobj_appcl->state &= ~APPCL_STATE_LBUTTON;
				}
				
				/* redraw client's window */
				appsrv_redraw (dobj_appcl);

				if (dobj_appcl->state & APPCL_STATE_RESIZE) {
					dobj_appcl->state &= ~APPCL_STATE_RESIZE;

					window->x = dobj_appcl->x;
					window->y = dobj_appcl->y;
					window->sizex = dobj_appcl->sizex;
					window->sizey = dobj_appcl->sizey;
				}

				break;
		}
		/*if (dobj->bitmap)
			draw_bitmap (dobj->x, dobj->y);
		else
			xrectfill (dobj->x, dobj->y, dobj->x+32, dobj->y+32, ~0);

		if (dobj->name)
			xtext_puts (dobj->x-7, dobj->y+35, 0, dobj->name);*/
	}

	return 0;
}

int init_dialog ()
{
	zde_dobj_list.next = &zde_dobj_list;
	zde_dobj_list.prev = &zde_dobj_list;

	mouse_trigger = 0;
	
	return 0;
}
