/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define		ESC	1

static struct sockaddr_in serverSock;	// Remote connection
static struct hostent *host;            // Remote host
static int ncsock;               	// Socket
static int size;                   	// Count of input/output bytes
static char *buf;			// Buffer
static char *msg;

unsigned key_pressed (int keycode)
{
	int scancode = getkey ();

	if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;
}

static int send_to_socket (char *data, unsigned len)
{
	int ret;
	if ((ret = send (ncsock, data, len, 0)) == -1) {
		printf ("ERROR -> send () = -1\n");
		return -1;
	}

	return ret;
}

static int handle_input ()
{
	char i = getch ();

	if (i) {
		setcolor (7, 0);

		putch (i);
		
		if (size < 64) {
			msg[size] = i;
			size ++;
		}

		if (i == '\n') {
			send_to_socket (msg, size);
			size = 0;
		}
	}

	if (key_pressed (ESC))
		return -1;

	return 1;
}

static int nc_init (const char *addr, int port)
{
	// Let's get info about remote computer
	if ((host = gethostbyname ((char *) addr)) == NULL) {
		printf ("Wrong address -> %s:%d\n", addr, port);
		return 0;
	}

	// Create socket
	if ((ncsock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return 0;
	}
	
	// Fill structure sockaddr_in
	// 1) Family of protocols
	serverSock.sin_family = AF_INET;
	// 2) Number of server port
	serverSock.sin_port = htons (port);
	// 3) Setup ip address of server, where we want to connect
	memcpy (&(serverSock.sin_addr), host->h_addr, host->h_length);
	
	// Now we are able to connect to remote server
	if (connect (ncsock, (struct sockaddr *) &serverSock, sizeof (serverSock)) == -1) {
		printf ("Connection cant be estabilished -> %s:%d\n", addr, port);
		return -1;
	}

	// Set socket to non-blocking mode
	int oldFlag = fcntl (ncsock, F_GETFL, 0);
	if (fcntl (ncsock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	printf ("netcat -> connected to %s:%d\n", addr, port);

	buf = (char *) malloc (sizeof (char) * 500);          		// receive buffer

	if (!buf)
		return 0;

	msg = (char *) malloc (sizeof (char) * 64);

	if (!msg)
		return 0;
	
	size = 0;

	return 1;
}

static int nc_loop ()
{
	int ret = recv (ncsock, buf, 499, 0);
  
	if (handle_input () == -1)
		return -1;
	
	if (ret > 0) {
		setcolor (14, 0);
		buf[ret] = '\0';
		printf (buf);
	} else {
		if (ret == -1)
			printf ("HOST -> Connection refused\n");
	}

	return ret;
}

void syntax ()
{
	printf ("nc: <address> <port>\nGNU/GPL3 - Coded by ZeXx86\n");
}

int main (int argc, char **argv)
{
	if (argc < 3) {
		syntax ();
		return 0;
	}

	const int port = atoi (argv[2]);

	if (!port) {
		syntax ();
		return 0;
	}

	int ret = nc_init ((char *) argv[1], port);

	if (ret < 1)
		goto end;

	while (1) {
		ret = nc_loop ();
		if (ret == -1)
			break;

		schedule ();
	}

	free (msg);
	free (buf);
end:
	printf ("netcat -> Bye !\n");

	close (ncsock);

	return 0;
}
 
