/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>

#define		ESC				1
#define		ARROWLEFT			75
#define		ARROWRIGHT			77
#define		ARROWUP				72
#define		ARROWDOWN			80


unsigned key_pressed (int keycode)
{
	int scancode = getkey ();

	if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;
}

int main (int argc, char **argv)
{
	if (argc < 1) {
		puts ("Pls specify bmp image filename\nimgshow <bmpimage>\n");
		exit (0);
	}

	/* Try open image */
	xbitmap *image = ximage_open (argv[1]);

	if (!image)
		return 0;

	/* Switch to VGA graphics mode */
	xinit ();

	/* Clean screen */
	xcls (0);

	/* Draw image to vga buffer */
	ximage_draw (image, 0, 0, -1);

	xtext_puts (10, image->width+10, 30, (char *) argv[1]);

	char buf_x[4];
	itoa (image->height, buf_x, 10);

	char buf_y[4];
	itoa (image->width, buf_y, 10);

	xtext_puts (10, image->width+20, 30, "X: ");
	xtext_puts (25, image->width+20, 30, buf_x);

	xtext_puts (10, image->width+29, 30, "Y: ");
	xtext_puts (25, image->width+29, 30, buf_y);

	/* Flip double buffer - show image */
	xfbswap ();

	/* Repeat until escape is pressed */
	while (!key_pressed (ESC))
		schedule ();

	/* Go back to textual mode */
	xexit ();

	/* Exit app */
	return 0;
}
