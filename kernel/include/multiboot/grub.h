/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _GRUB_H
#define _GRUB_H

typedef struct multiboot_info
{
	unsigned long flags;
	unsigned long mem_lower;
	unsigned long mem_upper;
	unsigned long boot_device;
	unsigned long cmdline;
	unsigned long mods_count;
	unsigned long mods_addr;

	union
	{
		unsigned long *aout_sym;
		unsigned long *elf_sec;
	} u;

	/* Memory Mapping buffer */
	unsigned long mmap_length;
	unsigned long mmap_addr;
	
	/* Drive Info buffer */
	unsigned long drives_length;
	unsigned long drives_addr;
	
	/* ROM configuration table */
	unsigned long config_table;
	
	/* Boot Loader Name */
	unsigned long boot_loader_name;
	
	/* APM table */
	unsigned long apm_table;
	
	/* Video */
	unsigned long vbe_control_info;
	unsigned long vbe_mode_info;
	unsigned short vbe_mode;
	unsigned short vbe_interface_seg;
	unsigned short vbe_interface_off;
	unsigned short vbe_interface_len;
} multiboot_info_t;

typedef struct multiboot_memory_map {
	unsigned int size;
	unsigned int base_addr_low,base_addr_high;
	//You can also use: unsigned long long int base_addr; if supported.
	unsigned int length_low,length_high;
	//You can also use: unsigned long long int length; if supported.
	unsigned int type;
} multiboot_memory_map_t;

#endif

