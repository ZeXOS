/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "window.h"
#include "cursor.h"

#define VFS_FILEATTR_FILE	0x1
#define VFS_FILEATTR_DIR	0x2
#define VFS_FILEATTR_HIDDEN	0x4
#define VFS_FILEATTR_SYSTEM	0x8
#define VFS_FILEATTR_BIN	0x10
#define VFS_FILEATTR_READ	0x20
#define VFS_FILEATTR_WRITE	0x40
#define VFS_FILEATTR_MOUNTED	0x80

typedef struct {
  char *name;
  unsigned attrib;
  unsigned char next;
} dirent_t;

extern wmcursor *cursor;

unsigned winfm_act = 0;

wmwindow *winfm;

dirent_t *dirent;

unsigned short *bitmap_folder;
unsigned short *bitmap_file;

void getdir ()
{
	asm volatile (
		"movl $25, %%eax;"
	     	"int $0x80;"
		"movl %%eax, %0;"
		: "=g" (dirent) :: "%eax");
}

unsigned filemanager_exit ()
{
	winfm_act = 0;

	return 1;
}

void ifilemanager ()
{
	getdir ();

	winfm = window_create ("Filemanager");

	if (!winfm)
		return;

	winfm_act = 1;

	//winfm->hnd_exit = (unsigned *) &filemanager_exit;
}

unsigned filemanager_draw ()
{
	if (!winfm_act || !winfm)
		return 0;

	/* when error or directory is clear */
	if (!dirent) {
		if (cursor->state == XCURSOR_STATE_RBUTTON) {
			chdir ("..");

			getdir ();
		}

		return 0;
	}


	unsigned id = 0;

	while (1) {
		xtext_puts (winfm->x+1+(id*42), winfm->y+44, 0, dirent[id].name);

		if ((winfm->x+33+(id*42)) > winfm->x+winfm->size_x)
			winfm->size_x += 42;

		unsigned i = 0;
		unsigned j = 0;
		unsigned k = 0;
		unsigned pix = 0;
	
		unsigned short *bitmap = 0;

		if (dirent[id].attrib & VFS_FILEATTR_DIR)
			bitmap = bitmap_folder;

		if (dirent[id].attrib & VFS_FILEATTR_FILE)
			bitmap = bitmap_file;

		if (!bitmap)
			continue;

		for (i = 0; i < 32*32; i ++) {
			if (k >= 32) {
				j ++;
				k = 0;
			}
	
			pix = (1024-i)+34;

			if (bitmap[pix] != 0)
				xpixel (winfm->x+1+k+(id*42), winfm->y+12+j, (unsigned short) bitmap[pix]);
	
			k ++;
		}

		if (cursor->state == XCURSOR_STATE_LBUTTON)
		if (cursor->x > (signed) (winfm->x+1+(id*42)) && cursor->x < (signed) (winfm->x+33+(id*42)) && 
			cursor->y > (signed) winfm->y+12 && cursor->y <= (signed) winfm->y+54) {
				if (dirent[id].attrib & VFS_FILEATTR_DIR) {
					if (dirent[id].name[0] == '.') {
						if (dirent[id].name[1] == '.') {
							chdir ("..");
			
							free (dirent);
			
							dirent = 0;
			
							getdir ();
						}
						return 1;
					}
	
					chdir (dirent[id].name);
	
					free (dirent);
	
					dirent = 0;
	
					getdir ();
				}

				if (dirent[id].attrib & VFS_FILEATTR_FILE) {
					tview_open (dirent[id].name);
				}

				return 1;
			}

		if (cursor->state == XCURSOR_STATE_RBUTTON) {
			if (cursor->x > (signed) winfm->x && cursor->x < (signed) winfm->x+(signed) winfm->size_x &&
				cursor->y > (signed) winfm->y+11 && cursor->y <= (signed) winfm->y+(signed) winfm->size_y) {

				dirent = 0;

				free (dirent);

				chdir ("..");

				getdir ();

				return 1;
			}	
		}

		if (!dirent[id].next)
			break;

		id ++;
	}

	return 1;
}

unsigned init_filemanager ()
{
	/* FOLDER bitmap */
	bitmap_folder = (unsigned short *) malloc (2100);

	if (!bitmap_folder)
		return 0;

	memset (bitmap_folder, 0, 2048+70);

	// html web page
	int fd = open ("folder", O_RDONLY);
	
	if (!fd) {
		puts ("error -> file 'folder' not found\n");
		return 0;
	}
	
	/*if (!read (fd, (unsigned char *) bitmap_folder, 2048+70)) {
		puts ("error -> something was wrong !\n");
		return 0;
	}*/

	/* FILE bitmap */
	bitmap_file = (unsigned short *) malloc (2100);

	if (!bitmap_file)
		return 0;

	memset (bitmap_file, 0, 2048+70);

	// html web page
	fd = open ("file", O_RDONLY);
	
	if (!fd) {
		puts ("error -> file 'file' not found\n");
		return 0;
	}
	
	/*if (!read (fd, (unsigned char *) bitmap_file, 2048+70)) {
		puts ("error -> something was wrong !\n");
		return 0;
	}*/

	winfm = 0;

	winfm_act = 0;

	return 1;
}
