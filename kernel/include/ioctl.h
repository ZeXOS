/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _IOCTL_H
#define _IOCTL_H

#include <net/if.h>
#include <net/ip.h>

struct ioifaddr_t {
	unsigned char dev[16];
	net_ipv4 ipv4;
	net_ipv6 ipv6;
};

struct ioatarq_t {
	unsigned char dev[16];
	unsigned sector;
	unsigned char data[512];
};

#define IOIFADDRSET	1
#define IOIFADDRGET	2
#define IOATAREAD	3
#define IOATAWRITE	4

/* externs */
extern int ioctl_call (unsigned id, void *buf, int l);

#endif

