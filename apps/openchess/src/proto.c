/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "config.h"
#include "client.h"
#include "rules.h"
#include "game.h"


/** PROTOCOL **
 * 	Action 		Command
 *	Chat msg	m <textmessage>
 *	Join to game	j <game>
 *	Setup nick	n <nick>
 *	Create game	g <game>
 *	Delete game	d
 *	Synchronize	s
 *	List all games	l
 */

/* Protocol definition */
#define PROTO_CLIENT_CHAT	'm'
#define PROTO_CLIENT_JOIN	'j'
#define PROTO_CLIENT_NICK	'n'
#define PROTO_CLIENT_GAME	'g'
#define PROTO_CLIENT_DELG	'd'
#define PROTO_CLIENT_SYNC	's'
#define PROTO_CLIENT_PPOS	'p'
#define PROTO_CLIENT_LIST	'l'

/* Send message to all players except sender client */
int proto_client_chat (client_t *client, char *buf, unsigned len)
{
	unsigned nick_len = 0;;

	/* check for nick */
	if (client->nick)
		nick_len =  strlen (client->nick);

	char *str = (char *) malloc (sizeof (char) * ( + len + 4));

	if (!str)
		return 0;

	/* build protocol chat message */
	memcpy (str, "m ", 2);

	if (nick_len)
		memcpy (str+2, client->nick, nick_len);
	else {
		memcpy (str+2, "Anonym", 6);
		nick_len = 6;
	}

	memcpy (str+2+nick_len, " ", 1);
	memcpy (str+3+nick_len, buf, len);

	int ret = 0;
	
	/* when player play, chat will works only on opponent */
	game_t *game = game_findbyclient (client);

	if (game) {
		client_t *c = 0;

		if (game->white == client)
			c = game->black;
		else
			c = game->white;

		ret = client_pmmsg (c, str, 3+nick_len+len);
	} else
		ret = client_msgtoall2 (client, str, 3+nick_len+len);

	free (str);

	printf ("-> Message '%s' : '%s'\n", client->nick, buf);

	return ret;
}

int proto_client_join (client_t *client, char *buf, unsigned len)
{
	game_t *game = game_findbyclient (client);

	if (game)
		return 0;

	game = game_find (buf);

	if (!game)
		return 0;

	if (!game_join (client, game))
		return 0;

	printf ("-> Player '%s' join to game '%s'\n", client->nick, game->name);

	return 1;
}

int proto_client_nick (client_t *client, char *buf, unsigned len)
{
	if (!len)
		return 0;

	if (client->nick)
		free (client->nick);

	client->nick = (char *) malloc (sizeof (char) * (len + 1));

	if (!client->nick)
		return 0;

	memcpy (client->nick, buf, len);
	client->nick[len] = '\0';

	printf ("-> Nick setup: '%s'\n", client->nick);

	return 1;
}

int proto_client_game (client_t *client, char *buf, unsigned len)
{
	game_t *game = game_findbyclient (client);

	if (game)
		return 0;

	game = game_find (buf);

	if (game)
		return 0;

	game = game_new (client, buf, len);

	if (!game)
		return 0;

	printf ("-> Game '%s' was created by '%s'\n", game->name, client->nick);

	return 1;
}

int proto_client_delg (client_t *client, char *buf, unsigned len)
{
	game_t *game = game_findbyclient (client);

	if (!game)
		return 0;

	if (!game_quit (game))
		return 0;

	printf ("-> Player '%s' close game\n", client->nick);

	return 1;
}

int proto_client_sync (client_t *client, char *buf, unsigned len)
{
	game_t *game = game_findbyclient (client);

	if (!game)
		return 0;

	if (!game_sync (client, game))
		return 0;

	printf ("-> Player '%s' make sync\n", client->nick);

	return 1;
}

int proto_client_ppos (client_t *client, char *buf, unsigned len)
{
	if (len != 5)
		return 0;

	game_t *game = game_findbyclient (client);

	if (!game)
		return 0;

	if (!game->play)
		return 0;

	if (game->player == 'w' && game->white != client)
		return 0;
	else if (game->player == 'b' && game->black != client)
		return 0;

	int x;
	int y;
	int ok = 4;
	
	char zx = 0;
	char zy = 0;
	char cx = 0;
	char cy = 0;
	
	x = buf[0];		// pismena souradnice
	x -= 'a';
	
	if ((x >= 0) && (x <= 7)) {
		zx = x;
		ok --;
	}
	
	y = buf[1];		// ciselna souradnice
	y -= ('0' + 1);
	
	if ((y >= 0) && (y <= 7)){
		zy = y;
		ok --;
	}

	x = buf[3];		// pismena souradnice
	x -= 'a';
	
	if ((x >= 0) && (x <= 7)){
		cx = x;
		ok --;
	}
	
	y = buf[4];		// ciselna souradnice
	y -= ('0' + 1);
	
	if ((y >= 0) && (y <= 7)){
		cy = y;
		ok --;
	}
	
	if (ok) {
		puts ("-> Bad move position");
		return 0;
	}

	if (rules_verify (game, zx, zy, cx, cy))
		return 0;

	if (!game_pos (client, game, zx, zy, cx, cy))
		return 0;

	if (game->player == 'w')
		game->player = 'b';
	else if (game->player == 'b')
		game->player = 'w';

	/* send new valid position to players of this game */
	if (game->black)
		client_pmmsg (game->black, buf-2, len+2);
	if (game->white)
		client_pmmsg (game->white, buf-2, len+2);

	printf ("-> Player '%s' change %c:%c to %c:%c\n", client->nick, buf[0], buf[1], buf[3], buf[4]);

	return 1;
}

int proto_client_list (client_t *client, char *buf, unsigned len)
{
	if (!game_getlist (client))
		return 0;

	printf ("-> Player '%s' get list of games\n", client->nick);

	return 1;
}

/** Protocol data handler */
int proto_handler (client_t *client, char *buf, unsigned len)
{
	/* stuff for netcat - rewrite \n character with \0 */
	if (len > 1)
		if (buf[len-1] == '\n') {
			len --;
			buf[len] = '\0';
		}

	char c = buf[0];

	if (c == PROTO_CLIENT_CHAT)
		return proto_client_chat (client, buf+2, len-2);
	if (c ==  PROTO_CLIENT_JOIN)
		return proto_client_join (client, buf+2, len-2);
	if (c == PROTO_CLIENT_NICK)
		return proto_client_nick (client, buf+2, len-2);
	if (c == PROTO_CLIENT_GAME)
		return proto_client_game (client, buf+2, len-2);
	if (c == PROTO_CLIENT_DELG)
		return proto_client_delg (client, buf+2, len-2);
	if (c == PROTO_CLIENT_SYNC)
		return proto_client_sync (client, buf+2, len-2);
	if (c == PROTO_CLIENT_PPOS)
		return proto_client_ppos (client, buf+2, len-2);
	if (c == PROTO_CLIENT_LIST)
		return proto_client_list (client, buf+2, len-2);
	

	return 1;
}

/** Protocol Init function */
int init_proto ()
{
	

	return 1;
}

