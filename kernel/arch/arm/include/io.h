/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ARM_H
#define	_ARM_H

#include <system.h>
#include <build.h>

/*
#define outb(v,a)	(*(volatile unsigned char __force  *)(a) = (v))
#define outw(v,a)	(*(volatile unsigned short __force *)(a) = (v))
#define outl(v,a)	(*(volatile unsigned int __force   *)(a) = (v))

#define inb(a)		*(volatile unsigned char __force  *)(a)
#define inw(a)		*(volatile unsigned short __force *)(a)
#define inl(a)		*(volatile unsigned int __force   *)(a)
*/
#endif

