/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ENV_H
#define _ENV_H

#define	DEFAULT_MAX_NAMELENGTH		12
#define	DEFAULT_MAX_VALUELENGTH		32

/* Environment cfg structure */
typedef struct env_context {
	struct env_context *next, *prev;

	char name[DEFAULT_MAX_NAMELENGTH];
	char value[DEFAULT_MAX_VALUELENGTH];
} env_t;

/* externs */
extern unsigned int init_env ();
extern void env_display ();
extern unsigned int env_set (char *name, char *value);
extern char *env_get (char *name);

#endif
