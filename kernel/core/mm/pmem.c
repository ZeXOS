/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <build.h>
#include <proc.h>

typedef struct pmalloc_context {
  struct pmalloc_context *next;
  struct pmalloc_context *prev;

  void *phys;
  void *virt;
} pmalloc_t;

pmalloc_t pmalloc_list;

extern unsigned *code, *bss, *data, *end;

void *palign (void *p)
{
	unsigned long c = 0;
	void *aligned = 0;

	/* is mem currently aligned ? */
	if (((unsigned long) p & 0xfffff000)) {
		c = (unsigned long) p;
		c &= 0xfffff000;
		c += 0x1000;

		aligned = (void *) c;
	} else
		aligned = p;

	return aligned;
}

/* returns page aligned memory */
void *pmalloc (size_t size)
{
	void *phys_mem;
	void *virt_mem;
	unsigned long calc = 0;

	/* well, it not seems too nice, but our memory will 100% avaliable */
	phys_mem = (void *) kmalloc (size + 0x1000);

	/* out of physical memory */
	if (!phys_mem)
		return 0;

	virt_mem = palign (phys_mem);

	// alloc and init context
	pmalloc_t *alloc = (pmalloc_t *) kmalloc (sizeof (pmalloc_t));

	if (!alloc)
		return 0;

	alloc->phys = phys_mem;
	alloc->virt = virt_mem;

	alloc->next = &pmalloc_list;
	alloc->prev = pmalloc_list.prev;
	alloc->prev->next = alloc;
	alloc->next->prev = alloc;

	return virt_mem;
}

void *pmalloc_ext (size_t size, unsigned *phys)
{
	void *phys_mem;
	void *virt_mem;
	unsigned long calc = 0;

	/* well, it not seems too nice, but our memory will 100% avaliable */
	phys_mem = (void *) kmalloc (size + 0x1000);

	/* out of physical memory */
	if (!phys_mem)
		return 0;

	virt_mem = palign (phys_mem);

	// alloc and init context
	pmalloc_t *alloc = (pmalloc_t *) kmalloc (sizeof (pmalloc_t));

	if (!alloc)
		return 0;

	alloc->phys = phys_mem;
	alloc->virt = virt_mem;

	alloc->next = &pmalloc_list;
	alloc->prev = pmalloc_list.prev;
	alloc->prev->next = alloc;
	alloc->next->prev = alloc;

	if (phys)
		*phys = (unsigned) virt_mem;

	return virt_mem;
}

void pfree (void *blk)
{
	pmalloc_t *alloc;

	for (alloc = pmalloc_list.next; alloc != &pmalloc_list; alloc = alloc->next) {
		if (alloc->virt == blk) {
			kfree (alloc->phys);

			alloc->next->prev = alloc->prev;
			alloc->prev->next = alloc->next;
		
			kfree (alloc);
			break;
		}
	}
}

unsigned pmem_init ()
{
	pmalloc_list.next = &pmalloc_list;
	pmalloc_list.prev = &pmalloc_list;

	return 1;
}

