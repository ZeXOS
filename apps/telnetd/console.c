/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "client.h"

char *login_msg;
unsigned login_msg_len;

char *tty2;
unsigned tty2_len;

char *buf;
unsigned buf_len;

char *tty;
char *tty_len;

unsigned zlen;

/*int tty_update (client_t *c)
{
	int i = 0;

	for (i = 0; i < 2000; i ++)
		tty[i] = c->tty[(i*2)+1];

	return 1;
}*/

int console_init ()
{
	login_msg = (char *) malloc (sizeof (char) * 100);

	if (!login_msg)
		return 0;

	memcpy (login_msg, "�����!", 6);
	memcpy (login_msg+6, "ZeX/OS telnet deamon\n�\n", 23);

	login_msg[29] = '\0';

	login_msg_len = 29;


	buf_len = 0;
	buf = (char *) malloc (sizeof (char) * 128);

	if (!buf)
		return 0;

	tty_len = 0;
	tty = (char *) malloc (sizeof (char) * 101);

	if (!tty)
		return 0;

	tty2_len = 0;
	tty2 = (char *) malloc (sizeof (char) * 101);

	if (!tty2)
		return 0;

	zlen = 0;

	return 1;
}

int console_motd (client_t *c)
{
	client_send (c, login_msg, login_msg_len);

	c->state = CLIENT_STATE_LOGIN;
	
	return 1;
}

int console_login (client_t *c)
{
	if (!c)
		return -1;



/*	int ret = client_recv (c, login_buf, login_buf_len);
	
	if (ret > 0) {
		memcpy (buf+c->buf_len, login_buf, ret);
		c->buf_len += ret;
		buf[c->buf_len] = '\0';
		puts ("he: ");
		puts (login_buf);
		puts ("\n");

		int log = 0;
		if (ret > 1) {
			int cislo = login_buf[0];
	
			itoa (cislo, tty, 10);
			puts (tty);
			if (login_buf[1] == 0 && login_buf[0] == 13)
				log = 1;
		}

		if (ret > 1 || log)
		if (buf[c->buf_len-1] == '\n' || log) {
			buf[c->buf_len-1] = '\0';

			puts ("nekdo zadal login: '");
			puts (buf+c->buf_len-6);
			puts ("'\n");

			if (!strcmp (buf+c->buf_len-6, "root")) {
				puts ("New user was succefully logged !\n");

				//c->tty = (char *) tty_init ();

				//if (!c->tty) {
				//	c->state = CLIENT_STATE_DONE;
				//	return 0;
				//}

				c->state = CLIENT_STATE_READY;
			}

			return 1;
		}
	}*/

	c->state = CLIENT_STATE_READY;

	return 1;
}

int console_handler (client_t *c)
{
	int ret = client_recv (c, tty, 100);

	if (ret == -1) {
		puts ("Some client was disconnected\n");
		c->state = CLIENT_STATE_DONE;
		return 1;
	}

	if (ret > 0) {
		unsigned i = 0; 
		for (i = 0; i < (unsigned) ret; i ++) {
			/*int cislo = tty[i];
	
			itoa (cislo, tty2, 10);
			puts (tty2); puts ("|");*/
			if (tty[i] == 13 && tty[i+1] == 10) {
				tty[i] = '\0';
				tty[i+1] = '\n';
			}

			if (tty[i] == 127)
				tty[i] = '\b';

			if (tty[i] == 32)
				tty[i] = ' ';
		}

		write (1, tty, ret);
	}

	unsigned len = read (1, tty2, 100);
	
	if (len > 0) {
		/*if (!strncmp (tty2, tty, ret))
			clients_send (tty2+ret, len-ret);
		else*/
		/*if (strlen (tty2)) {
			puts ("data: '");
			puts (tty);
			puts ("' - '");
			puts (tty2);
			puts ("\n");
		}*/

		clients_send (tty2, len);
	}

	return 1;
}
