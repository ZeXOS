/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _STDIO_H
#define _STDIO_H

#define SEEK_SET	0
#define SEEK_CUR	1
#define SEEK_END	2

extern void puts (unsigned char *text);
extern void printf (const char *fmt, ...);
extern int sprintf (char *buffer, const char *fmt, ...);
extern int open (const char *pathname, int flags);
extern int close (int fd);
extern long lseek (int fd, long offset, int whence);

#endif
