/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _CLIENT_H
#define _CLIENT_H

#define CLIENT_STATE_CONNECTED		0x1
#define CLIENT_STATE_TRANSFER		0x2
#define CLIENT_STATE_DONE		0x4
#define CLIENT_STATE_TIMEOUT		0x8

/* Client structure */
typedef struct client_context {
	struct client_context *next, *prev;
	
	int fd;
	unsigned char state;
	unsigned ret;
	char page[32];
} client_t;

#endif
