/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "window.h"
#include "cursor.h"

wmwindow wmwindow_list;

xbitmap *window_img;

unsigned window_last_x;
unsigned window_last_y;

extern wmcursor *cursor;

wmwindow *window_create (const char *caption)
{
	wmwindow *window = (wmwindow *) malloc (sizeof (wmwindow));

	if (!window)
		return 0;

	window_last_x += 10;
	window_last_y += 10;

	window->x = window_last_x;
	window->y = window_last_y;
	window->size_x = 200;
	window->size_y = 150;
	window->active = 1;

	unsigned l = strlen (caption);
	if (l) {
		window->caption = (char *) malloc (sizeof (char) * l + 1);
		memcpy (window->caption, caption, l);
		window->caption[l] = '\0';
	} else
		window->caption = 0;

	//window->hnd_exit = 0;

	/* add into list */
	window->next = &wmwindow_list;
	window->prev = wmwindow_list.prev;
	window->prev->next = window;
	window->next->prev = window;

	return window;
}

unsigned window_sendquit (wmwindow *window)
{
	if (!window)
		return 0;

	window->active = 0;
}

//typedef void (*void_fn_void_t)(void);
unsigned window_delete (wmwindow *window)
{
	if (!window)
		return 0;

	//if (window->hnd_exit)
	//	((void_fn_void_t) window->hnd_exit) ();

	if (window->active)
		return 0;

	if (window->caption)
		free (window->caption);

	/* delete from list */
	window->next->prev = window->prev;
	window->prev->next = window->next;

	free (window);

	return 1;
}

unsigned window_draw (wmwindow *window)
{
	if (!window)
		return 0;

	/* 	50 - zluta 
		60 - cervena
		70 - cerna
		30 - vybledle zluta
		35 - seda
		40 - ruzova
		45 - fialova
		55 - jasne zluta
		15 - jasne modra
		18 - jasne razici zelena
		1 - modra
		3 - trochu tmavsi seda
		4 - tmavsi cervena
		5 - fialova
		6 - zluto-hneda
		7 - jasne seda
		8 - tmave modra
		9 - pekna modra
		27 - biele-modra
		
	*/

	xrect (window->x, window->y, window->x+window->size_x, window->y+window->size_y, 0xd3d3d3);

	if (window->active) {
		xline (window->x+1, window->y+1, window->x+window->size_x-1, window->y+1, 60*256);
		xline (window->x+1, window->y+2, window->x+window->size_x-1, window->y+2, 70*256);
		xline (window->x+1, window->y+3, window->x+window->size_x-1, window->y+3, 80*256);
		xline (window->x+1, window->y+4, window->x+window->size_x-1, window->y+4, 100*256);
		xline (window->x+1, window->y+5, window->x+window->size_x-1, window->y+5, 125*256);
		xline (window->x+1, window->y+6, window->x+window->size_x-1, window->y+6, 150*256);
		xline (window->x+1, window->y+7, window->x+window->size_x-1, window->y+7, 175*256);
		xline (window->x+1, window->y+8, window->x+window->size_x-1, window->y+8, 180*256);
		xline (window->x+1, window->y+9, window->x+window->size_x-1, window->y+9, 190*256);
		xline (window->x+1, window->y+10, window->x+window->size_x-1, window->y+10, 200*256);
	} else
		xrectfill (window->x+1, window->y+1, window->x+window->size_x-1, window->y+10, 1*256);

	xrectfill (window->x+1, window->y+11, window->x+window->size_x-1, window->y+window->size_y-1, 0xffffff);

	if (window->caption)
		xtext_puts (window->x+5, window->y+2, 0, window->caption);

	return 1;
}

unsigned window_draw_all ()
{
	wmwindow *window;
	for (window = wmwindow_list.next; window != &wmwindow_list; window = window->next) {
		if (!window)
			continue;

		if (cursor->action)
		if (cursor->state != XCURSOR_STATE_LBUTTON) {
			if (window->flags & WINDOW_FLAG_DRAG) {
				window->flags &= ~WINDOW_FLAG_DRAG;
				cursor->action = 0;
			}
		}

		/* mame mys na zahlavi okna ? */
		if (cursor->x > (signed) window->x && cursor->x < (signed) window->x+(signed) window->size_x && 
			cursor->y > (signed) window->y && cursor->y <= (signed) window->y+11) {

			if (!cursor->action) {
				if (cursor->state == XCURSOR_STATE_LBUTTON) {
					if (!(window->flags & WINDOW_FLAG_DRAG)) {
						window->flags |= WINDOW_FLAG_DRAG;
						cursor->action = 1;
					}
				}
			}

			if (cursor->state == XCURSOR_STATE_RBUTTON) {
				if (window_delete (window))
					return 1;
			}
		}

		if (window->flags & WINDOW_FLAG_DRAG) {
			window->x = cursor->x;
			window->y = cursor->y;
		}

		window_draw (window);
	}

	return 1;
}

unsigned init_window ()
{
	window_last_x = 0;
	window_last_y = 0;

	wmwindow_list.next = &wmwindow_list;
	wmwindow_list.prev = &wmwindow_list;

	return 1;
}
