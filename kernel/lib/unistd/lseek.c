/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fd.h>
#include <file.h>
#include <stdio.h>
#include <system.h>

long lseek (int fd, long offset, int whence)
{
	fd_t *d = fd_get (fd);

	if (!d)
		return offset-1;

	unsigned l = d->p + d->e;

	switch (whence) {
		case SEEK_SET:
			d->p = offset;
			d->e = l;
			break;
		case SEEK_CUR:
			d->p += offset;
			break;
		case SEEK_END:
			d->p = (l + offset);
			d->e = 0;
			break;
	}

	return d->p;
}
