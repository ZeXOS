/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __TL_SETJMP_H
#define	__TL_SETJMP_H

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
/* setjmp() and longjmp() rely on the order of these registers,
so do not re-arrange them */
	unsigned edi, esi, ebp, esp, ebx, edx, ecx, eax;
	unsigned eip, eflags;
} jmp_buf[1];

int setjmp(jmp_buf b);
void longjmp(jmp_buf b, int ret_val);

#ifdef __cplusplus
}
#endif

#endif
