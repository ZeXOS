/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _GAME_H
#define _GAME_H

#include "client.h"

/* game structure */
typedef struct game_context {
	struct game_context *next, *prev;

	char *name;
	unsigned char play;
	unsigned short round;
	char player;

	client_t *white;
	client_t *black;

	char board[8][8];

} game_t;

/* externs */
extern game_t *game_find (char *name);
extern game_t *game_findbyclient (client_t *client);
extern game_t *game_new (client_t *client, char *name, unsigned name_len);
extern int game_quit (game_t *game);
extern int game_join (client_t *client, game_t *game);
extern int game_sync (client_t *client, game_t *game);
extern int game_pos (client_t *client, game_t *game, unsigned char x_old, unsigned char y_old, unsigned char x, unsigned char y);
extern int game_getlist (client_t *client);
extern int init_game ();

#endif
