/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <system.h>
#include <string.h>
#include <partition.h>
#include <dev.h>
#include <fs.h>
#include <cache.h>
#include <config.h>

fs_t fs_list;

static unsigned char tolower (char c)
{
	return ((c >= 'A' && c <= 'Z') ? c + 32: c);
}

bool fs_list_add (char *name, fs_handler_t *handler)
{
	unsigned name_len = strlen (name);

	if (!name_len || !handler)
		return 0;

	/* alloc and init context */
	fs_t *fs = (fs_t *) kmalloc (sizeof (fs_t));

	if (!fs)
		return 0;

	memset (fs, 0, sizeof (fs_t));

	fs->name = (char *) kmalloc (sizeof (char) * FS_NAME_LEN + 1);

	if (!fs->name) {
		kfree (fs);
		return 0;
	}

	memset (fs->name, 0, FS_NAME_LEN);
	memcpy (fs->name, name, name_len);
	fs->name[name_len] = '\0';

	fs->handler = handler;

	/* add into list */
	fs->next = &fs_list;
	fs->prev = fs_list.prev;
	fs->prev->next = fs;
	fs->next->prev = fs;

	return 1;
}

bool fs_list_del (fs_t *fs)
{
	if (fs) {
		fs->next->prev = fs->prev;
		fs->prev->next = fs->next;

		//kfree (fs);
		return 1;
	}

	return 0;
}

fs_t *fs_supported (char *name)
{
	fs_t *fs;
	for (fs = fs_list.next; fs != &fs_list; fs = fs->next) {
		if (!strncmp (fs->name, name, strlen (fs->name)))
			return fs;
	}

	return 0;
}

extern partition_t partition_list;
fs_t *fs_detect (partition_t *p)
{
	if (!p)
		return 0;

	fs_t *fs;

	char block[512];

	dev_t *dev = (dev_t *) dev_findbypartition (p);

	if (!dev)
		return 0;
	
	if (!dev->handler (DEV_ACT_READ, p, block, "", 63))
		return 0;
;
	char fstype[11];

	int c;
	for (c = 54; c < 64; c ++)
		fstype[c-54] = block[c];

	memset (block, 0, sizeof (block));

	for (c = 0; c < 10; c ++)
		block[c] = tolower (fstype[c]);

	fs = fs_supported (block);

	return fs;
}

extern bool fat_handler (unsigned act, char *block, unsigned n, unsigned long l);
extern bool fat16_handler (unsigned act, char *block, unsigned n, unsigned long l);
extern bool zexfs_handler (unsigned act, char *block, unsigned n, unsigned long l);
extern bool isofs_handler (unsigned act, char *block, unsigned n, unsigned long l);
extern bool znfs_handler (unsigned act, char *block, unsigned n, unsigned long l);
#ifdef CONFIG_DRV_EXT2
extern bool ext2_handler (unsigned act, char *block, unsigned n, unsigned long l);
#endif
unsigned int init_fs ()
{
	fs_list.next = &fs_list;
	fs_list.prev = &fs_list;

	init_cache ();

#ifdef ARCH_i386
#ifdef CONFIG_DRV_ISOFS
	fs_list_add ("isofs", &isofs_handler);
#endif

#ifdef CONFIG_DRV_ZEXFS
	fs_list_add ("zexfs", &zexfs_handler);
#endif

#ifdef CONFIG_DRV_EXT2
	fs_list_add ("ext2", &ext2_handler);
#endif

#ifdef CONFIG_DRV_FAT16
	fs_list_add ("fat16", &fat16_handler);
#endif

#ifdef CONFIG_DRV_FAT12
	fs_list_add ("fat12", &fat_handler);
#endif

	fs_list_add ("znfs", &znfs_handler);
#endif
	return 1;
}
