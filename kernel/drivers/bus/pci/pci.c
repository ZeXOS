/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifndef ARCH_i386
#undef CONFIG_DRV_PCI
#endif

#ifdef CONFIG_DRV_PCI

#include <system.h>
#include <arch/io.h>
#include <pci.h>
#include <string.h>
#include <stdio.h>
#include "../drivers/bus/pci/devlist.h"

pcidev_t pcidev_list;

#define	inportb(P)	inb(P)
#define	inportw(P)	inw(P)
#define	outportb(P,V)	outb(P,V)
#define	outportw(P,V)	outw_p(P,V)
#define	inportl(P)	inl(P)
#define	outportl(P,V)	outl(P,V)


/* else [in|out]portl() defined below */
#define PCI_READ_CONFIG_BYTE	0xB108
#define PCI_READ_CONFIG_WORD	0xB109
#define PCI_READ_CONFIG_DWORD	0xB10A
#define PCI_WRITE_CONFIG_BYTE	0xB10B
#define PCI_WRITE_CONFIG_WORD	0xB10C
#define PCI_WRITE_CONFIG_DWORD	0xB10D

#define	PCI_ADR_REG		0xCF8
#define	PCI_DATA_REG		0xCFC

/*****************************************************************************
If you have PnP code available, device PNP0A03 also
indicates the presence of a PCI controller in the system.
*****************************************************************************/
static int pci_detect ()
{
	kprintf ("PCI controller: ");

	/* poke 32-bit I/O register at 0xCF8 to see if
	there's a PCI controller there */
	outportl (PCI_ADR_REG, 0x80000000L); /* bus 0, dev 0, fn 0, reg 0 */

	unsigned long ret = inportl (PCI_ADR_REG);

	if(ret != 0x80000000L) {
		kprintf ("not found / %x\n", ret);
		return -1;
	}

	kprintf ("found\n");

	return 0;
}
/*****************************************************************************
*****************************************************************************/
int pci_read_config_byte (pcidev_t *pci, unsigned reg, unsigned char *val)
{
	outportl(PCI_ADR_REG,
		0x80000000L | /* "enable configuration space mapping" */
		((unsigned long)pci->bus << 16) |	/* b23-b16=bus */
		((unsigned)pci->dev << 11) |		/* b15-b11=dev */
		((unsigned)pci->fn << 8) |		/* b10-b8 =fn  */
		(reg & ~3));				/* b7 -b2 =reg */

	*val = inportb(PCI_DATA_REG + (reg & 3));	// xxx - is this legit?

	return 0;
}
/*****************************************************************************
*****************************************************************************/
int pci_read_config_word (pcidev_t *pci, unsigned reg, unsigned short *val)
{
	outportl (PCI_ADR_REG, 0x80000000L |
		((unsigned long) pci->bus << 16) |
		((unsigned) pci->dev << 11) |
		((unsigned) pci->fn << 8) | (reg & ~3));

	*val = inportw (PCI_DATA_REG + (reg & 2));

	return 0;
}
/*****************************************************************************
*****************************************************************************/
int pci_read_config_dword (pcidev_t *pci, unsigned reg, unsigned long *val)
{
	outportl (PCI_ADR_REG, 0x80000000L |
		((unsigned long) pci->bus << 16) |
		((unsigned) pci->dev << 11) |
		((unsigned) pci->fn << 8) | (reg & ~3));

	*val = inportl (PCI_DATA_REG + 0);

	return 0;
}
/*****************************************************************************
*****************************************************************************/
int pci_write_config_byte (pcidev_t *pci, unsigned reg,
		unsigned val)
{
	outportl (PCI_ADR_REG, 0x80000000L |
		((unsigned long)pci->bus << 16) |
		((unsigned)pci->dev << 11) |
		((unsigned)pci->fn << 8) | (reg & ~3));

	outportb (PCI_DATA_REG + (reg & 3), val);

	return 0;
}
/*****************************************************************************
*****************************************************************************/
int pci_write_config_word(pcidev_t *pci, unsigned reg,
		unsigned val)
{
	outportl (PCI_ADR_REG, 0x80000000L |
		((unsigned long)pci->bus << 16) |
		((unsigned)pci->dev << 11) |
		((unsigned)pci->fn << 8) | (reg & ~3));

	outportw (PCI_DATA_REG + (reg & 2), val);

	return 0;
}
/*****************************************************************************
*****************************************************************************/
int pci_write_config_dword(pcidev_t *pci, unsigned reg,
		unsigned long val)
{
	outportl(PCI_ADR_REG, 0x80000000L |
		((unsigned long)pci->bus << 16) |
		((unsigned)pci->dev << 11) |
		((unsigned)pci->fn << 8) | (reg & ~3));

	outportl (PCI_DATA_REG + 0, val);

	return 0;
}
/*****************************************************************************
*****************************************************************************/
static int pci_iterate (pcidev_t *pci)
{
	unsigned char hdr_type = 0x80;

	/* if first function of this device, check if multi-function device
	(otherwise fn==0 is the _only_ function of this device) */
	if(pci->fn == 0) {
		if(pci_read_config_byte(pci, 0x0E, &hdr_type))
			return -1;	/* error */
	}

	/* increment iterators
	fn (function) is the least significant, bus is the most significant */
	pci->fn ++;
	
	if(pci->fn >= 8 || (hdr_type & 0x80) == 0) {
		pci->fn = 0;
		pci->dev++;
		if(pci->dev >= 32) {
			pci->dev = 0;
			pci->bus++;
//			if(pci->bus > g_last_pci_bus)
			if(pci->bus > 7)
				return 1; /* done */
		}
	}

	return 0;
}
/*****************************************************************************
*****************************************************************************/
/*
 *	Set device to be a busmaster in case BIOS neglected to do so.
 *	Also adjust PCI latency timer to a reasonable value, 32.
 */

void pci_device_adjust (pcidev_t *pci)
{
	unsigned short	new_command, pci_command;
	unsigned char	pci_latency;

	pci_read_config_word (pci, PCI_command, &pci_command);

	new_command = pci_command | PCI_command_master | PCI_command_io;

	if (pci_command != new_command) {
		kprintf("The PCI BIOS has not enabled this device!\nUpdating PCI command %X->%X\n",
			   pci_command, new_command);

		pci_write_config_word (pci, PCI_command, new_command);
	}

	pci_read_config_byte (pci, PCI_latency, &pci_latency);

	if (pci_latency < 32) {
		kprintf("PCI latency timer (CFLT) is unreasonably low at %d. Setting to 32 clocks.\n", pci_latency);
		pci_write_config_byte(pci, PCI_latency, 32);
	}
}
/*****************************************************************************
*****************************************************************************/
int pci_device_enable (pcidev_t *pci)
{
	unsigned char r;
	pci_read_config_byte (pci, PCI_pm_status, &r);

	if (r & PCI_pm_state_d0)
		pci_write_config_byte (pci, PCI_pm_status, PCI_pm_state_d0);

	return 1;
}
/*****************************************************************************
*****************************************************************************/
void pcidev_display ()
{
	pcidev_t *pcidev;
	for (pcidev = pcidev_list.next; pcidev != &pcidev_list; pcidev = pcidev->next) {
		if (!pcidev)
			continue;

		unsigned i = 0;
		for (i = 0; i < PCI_DEVTABLE_LEN; i ++)
			if (PciDevTable[i].VenId == (unsigned short) (pcidev->id & 0xFFFF)) {
				if (PciDevTable[i].DevId == (unsigned short) (pcidev->id >> 16))
					break;
			}
		

		printf ("%02u:%02u:%u %04lX:%04lX  %s: %s\n",
			pcidev->bus, pcidev->dev, pcidev->fn,
			pcidev->id & 0xFFFF, pcidev->id >> 16,
			//pcidev->u.h0.base_registers[0], pcidev->u.h0.base_register_sizes[0], pcidev->u.h0.base_registers[3],
			i == PCI_DEVTABLE_LEN ? "Unknown" : PciDevTable[i].Chip,
			i == PCI_DEVTABLE_LEN ? "Unknown" : PciDevTable[i].ChipDesc);
	}
}

/*****************************************************************************
*****************************************************************************/
pcidev_t *pcidev_register (unsigned char bus, unsigned char dev, unsigned char fn, unsigned long id)
{
	pcidev_t *pcidev;
	
	/* alloc and init context */
	pcidev = (pcidev_t *) kmalloc (sizeof (pcidev_t));

	pcidev->bus = bus;
	pcidev->dev = dev;
	pcidev->fn = fn;
	pcidev->id = id;

	/* add into list */
	pcidev->next = &pcidev_list;
	pcidev->prev = pcidev_list.prev;
	pcidev->prev->next = pcidev;
	pcidev->next->prev = pcidev;

	return pcidev;
}

pcidev_t *pcidev_find (unsigned short vendor, unsigned short device)
{
	pcidev_t *pcidev;
	for (pcidev = pcidev_list.next; pcidev != &pcidev_list; pcidev = pcidev->next) {
		if (!pcidev)
			continue;

		if (vendor == (unsigned short) (pcidev->id & 0xFFFF))
			if (device == (unsigned short) (pcidev->id >> 16))
				return pcidev;
	}

	return 0;
}

bool bus_pci_acthandler (unsigned act, char *block, unsigned block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			pcidev_list.next = &pcidev_list;
			pcidev_list.prev = &pcidev_list;

			pcidev_t pci;
			int err;
		
			/* check for PCI BIOS */
			if(pci_detect ())
				return 1;
		
			/* display numeric ID of all PCI devices detected */
			memset (&pci, 0, sizeof(pci));

			do
			{
				/* 00=PCI_VENDOR_ID */
				err = pci_read_config_dword (&pci, 0x00, &pci.id);
		
				if(err)	{
		ERR:			kprintf ("PCI: error 0x%02X reading PCI config\n", err);
					return 1;
				}
		
				pcidev_t *pcidev = 0;
				/* anything there? */
				if(pci.id != 0xFFFFFFFFL) {
					pcidev = pcidev_register (pci.bus, pci.dev, pci.fn, pci.id);
					if (pcidev) {
						/* base info */
						pci_read_config_byte (pcidev, PCI_revision, &pcidev->revision);
						pci_read_config_byte (pcidev, PCI_class_api, &pcidev->class_api);
						pci_read_config_byte (pcidev, PCI_class_sub, &pcidev->class_sub);
						pci_read_config_byte (pcidev, PCI_class_base, &pcidev->class_base);
						
						pci_read_config_byte (pcidev, PCI_header_type, &pcidev->header_type);
						pcidev->header_type &= PCI_header_type_mask;
					
						pci_read_config_byte (pcidev, PCI_latency, &pcidev->latency);
						pci_read_config_byte (pcidev, PCI_bist, &pcidev->bist);
						pci_read_config_byte (pcidev, PCI_line_size, &pcidev->line_size);

						unsigned i = 0;
						unsigned temp, temp2;
	
						if(pcidev->header_type == 0) {
							for (i = 0; i < 6; i++) {
								pci_read_config_dword (pcidev, PCI_base_registers + i*4, (unsigned long *) &temp);
								if(temp) {
									pci_write_config_dword (pcidev, PCI_base_registers + i*4, 0xffffffff);
									pci_read_config_dword (pcidev, PCI_base_registers + i*4, (unsigned long *) &temp2);
									temp2 &= 0xfffffff0;

									pci_write_config_dword (pcidev, PCI_base_registers + i*4, temp);

									temp2 = 1 + ~temp2;
									if(temp & 1) {
										pcidev->u.h0.base_registers[i] = temp & 0xfff0;
										pcidev->u.h0.base_register_sizes[i] = temp2 & 0xffff;
									} else {
										pcidev->u.h0.base_registers[i] = temp & 0xfffffff0;
										pcidev->u.h0.base_register_sizes[i] = temp2;
									}
								} else {
									pcidev->u.h0.base_registers[i] = 0;
									pcidev->u.h0.base_register_sizes[i] = 0;
								}
							
								pci_read_config_byte (pcidev, PCI_interrupt_line, &pcidev->u.h0.interrupt_line);
								pci_read_config_byte (pcidev, PCI_interrupt_pin, &pcidev->u.h0.interrupt_pin);
								pcidev->u.h1.interrupt_pin &= PCI_pin_mask;
					//			dprintf("basereg %d:%d:%d 0x%x\n", bus, dev, func, pcii->u.h0.base_registers[i]);
					//			dprintf("size    %d:%d:%d 0x%x\n", bus, dev, func, pcii->u.h0.base_register_sizes[i]);
							}
						}
					}
				}
				

			} while (!pci_iterate (&pci));


			/* find a USB controller */
			memset (&pci, 0, sizeof (pci));
		
			do
			{
				unsigned char major, minor;
				unsigned long id;


				/* 00=vendor */
				err = pci_read_config_dword (&pci, 0x00, &id);

				if(err)
					goto ERR;

				/* 0B=class */
				err = pci_read_config_byte (&pci, 0x0B, &major);
		
				if(err)
					goto ERR;
		
				/* 0A=sub-class */
				err = pci_read_config_byte (&pci, 0x0A, &minor);
		
				if(err)
					goto ERR;
		
				/* anything there? */
				if (major != 0xFF || minor != 0xFF) {
					//printf("detected device of class %u.%u\n", major, minor);
					if(major == 12 && minor == 3) {
						//printf("USB controller detected: 0x%x, 0x%x\n", id & 0xFFFF, id >> 16);
						pcidev_t *pcidev = pcidev_find (id & 0xFFFF, id >> 16);

						if (!pcidev)
							continue;

						usb_controller_register (pcidev);
						
						break;
					}
				}
		
			} while (!pci_iterate (&pci));

			return 1;
		}
		break;
		case DEV_ACT_READ:
		{

			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{

			return 1;
		}
		break;
	}

	return 0;
}
#endif 
