
/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <system.h>
#include <string.h>
#include <dev.h>
#include <vfs.h>
#include <rs232.h>
#include <drive.h>
#include <partition.h>
#include <speaker.h>
#include <pci.h>
#include <mouse.h>
#include <config.h>

/* pcnet32 - ethernet card */
#include "../drivers/net/pcnet32/pcnet32_dev.h"
/* rtl8029 - ethernet card */
#include "../drivers/net/rtl8029/rtl8029_dev.h"
/* rtl8139 - ethernet card */
#include "../drivers/net/rtl8139/rtl8139_dev.h"
/* rtl8169 - ethernet card */
#include "../drivers/net/rtl8169/rtl8169_dev.h"
/* es1370 - sound card */
#include "../drivers/char/sound/es1370_dev.h"
/* ac97 - sound card */
#include "../drivers/char/sound/ac97_dev.h"
/* ps2 - mouse driver */
#include "../drivers/char/mouse/ps2_dev.h"

dev_t dev_list;

void dev_display ()
{
	dev_t *dev;
	for (dev = dev_list.next; dev != &dev_list; dev = dev->next)
		kprintf ("%s: %s\n", dev->devname, dev->desc);
}

dev_t *dev_find (char *devname)
{
	dev_t *dev;
	for (dev = dev_list.next; dev != &dev_list; dev = dev->next) {
		if (!strcmp (dev->devname, devname))
			return dev;
	}

	return 0;
}

dev_t *dev_findbypartition (partition_t *p)
{
	dev_t *dev;
	for (dev = dev_list.next; dev != &dev_list; dev = dev->next) {
		if (!strncmp (dev->devname, p->name, strlen (dev->devname)))
			return dev;
	}

	return 0;
}

dev_t *dev_register (char *devname, char *desc, unsigned attrib, dev_handler_t *handler)
{
	/* first check length of device name */
	/*if (strlen (desc) >= DEFAULT_MAX_DESCLENGTH ||
	    strlen (devname) >= DEFAULT_MAX_DEVNLENGTH)
		return 0;*/

	/* when noeth is active, we can't register ethernet devices */
	if (attrib == DEV_ATTR_NET)
		if (kernel_attr & KERNEL_NOETH)
			return 0;

	/* dev name havent to exists */
	if (dev_find (devname))
		return 0;

	unsigned l = strlen (devname);
	
	video_color (9, 0);
	kprintf (":");
	
	video_color (7, 0);
	kprintf (" %s -> init %s\n", devname, desc);

	dev_flags_t flags;
	memset (&flags, 0, sizeof (dev_flags_t));

	/* chance of getting device name on DEV_ACT_INIT */
	flags.iomem = (void *) devname;
	flags.iolen = l;

	vfs_t *vfs = (vfs_t *) 0;

	if (handler (DEV_ACT_INIT, (char *) &flags, sizeof (dev_flags_t))) {
		vfs = vfs_list_add (devname, VFS_FILEATTR_FILE | VFS_FILEATTR_SYSTEM | VFS_FILEATTR_DEVICE, "/dev/");

		if (!vfs)
			return 0;
	} else
		return 0;

	/* alloc and init context */
	dev_t *dev = (dev_t *) kmalloc (sizeof (dev_t));
	
	if (!dev)
		return 0;

	memcpy (dev->devname, "/dev/", 5);
	memcpy (dev->devname+5, devname, l);
	dev->devname[l+5] = '\0';

	strcpy (dev->desc, desc);

	dev->attrib = attrib;
	dev->handler = handler;

	/* when it's block or character device, let's mmap IO memory to VFS device file */
	if (flags.iomem && flags.iolen) {
		vfs_content_t content;
		content.ptr = flags.iomem;
		content.len = flags.iolen;
		
		vfs_mmap (dev->devname, l+5, &content);
	}
		
	/* add into list */
	dev->next = &dev_list;
	dev->prev = dev_list.prev;
	dev->prev->next = dev;
	dev->next->prev = dev;

	return dev;
}

//extern unsigned ext2_init (partition_t *p);
void dev_install ()
{
	dev_t *dev = 0;

#ifdef CONFIG_DRV_RS232
	dev_register ("com", "Serial port", DEV_ATTR_CHAR, (dev_handler_t *) &rs232_acthandler);
#endif

#ifdef ARCH_i386
#ifdef CONFIG_DRV_VGA
	dev_register ("vga", "Graphics adapter", DEV_ATTR_CHAR, (dev_handler_t *) &video_acthandler);
#endif

#ifdef CONFIG_DRV_FLOPPY
	dev = dev_register ("fd", "Floppy drive", DEV_ATTR_BLOCK, (dev_handler_t *) &floppy_acthandler);
	partition_add (dev, fs_supported ("fat12"), 63);	// small HACK
#endif

#ifdef CONFIG_DRV_ATA
	dev_register ("ata", "ATA computer bus", DEV_ATTR_SPECIAL, (dev_handler_t *) &ata_acthandler);
#endif

#ifdef CONFIG_DRV_SPEAKER
	dev_register ("pcspk", "PC-Speaker", DEV_ATTR_CHAR, (dev_handler_t *) &pcspk_acthandler);
#endif

#ifdef CONFIG_DRV_PCI
	dev_register ("pci", "PCI computer bus", DEV_ATTR_SPECIAL, (dev_handler_t *) &bus_pci_acthandler);
#endif

#ifdef CONFIG_DRV_ES1370
	dev_register ("es1370", "ES1370", DEV_ATTR_CHAR | DEV_ATTR_SOUND, (dev_handler_t *) &es1370_acthandler);
#endif

#ifdef CONFIG_DRV_AC97
	dev_register ("ac97", "AC'97", DEV_ATTR_CHAR | DEV_ATTR_SOUND, (dev_handler_t *) &ac97_acthandler);
#endif

#ifdef CONFIG_DRV_PCNET32
	dev_register ("pcnet32", "Ethernet card", DEV_ATTR_NET, (dev_handler_t *) &pcnet32_acthandler);
#endif

#ifdef CONFIG_DRV_RTL8029
	dev_register ("rtl8029", "Ethernet card", DEV_ATTR_NET, (dev_handler_t *) &rtl8029_acthandler);
#endif

#ifdef CONFIG_DRV_RTL8139
	dev_register ("rtl8139", "Ethernet card", DEV_ATTR_NET, (dev_handler_t *) &rtl8139_acthandler);
#endif

#ifdef CONFIG_DRV_RTL8169
	dev_register ("rtl8169", "Ethernet card", DEV_ATTR_NET, (dev_handler_t *) &rtl8169_acthandler);
#endif

#ifdef CONFIG_DRV_COMMOUSE
	dev_register ("mousecom", "Serial mouse", DEV_ATTR_CHAR, (dev_handler_t *) &commouse_acthandler);
#endif

#ifdef CONFIG_DRV_PS2MOUSE
	dev_register ("mouseps2", "PS/2 mouse", DEV_ATTR_CHAR, (dev_handler_t *) &ps2mouse_acthandler);
#endif
#endif
}

unsigned int init_dev ()
{
	dev_list.next = &dev_list;
	dev_list.prev = &dev_list;

	// set default dev values
	dev_list.devname[0] = '\0';
	dev_list.desc[0] = '\0';

	dev_install ();

	return 1;
}
