#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "client.h"

#define PROTO_MKDIR	0x1
#define PROTO_RMDIR	0x2
#define PROTO_CREAT	0x3
#define PROTO_RM	0x4
#define PROTO_CHDIR	0x5
#define PROTO_READ	0x6

int proto_handle (client_t *c, char *data, unsigned len)
{
	char name[1024];
	unsigned char l = 0;
	int r = -1;

	printf ("proto_handle () - '%s' : %d\n", data, len);

	switch (data[0]) {
		case PROTO_MKDIR:
			if (len < 3)
				return 0;

			l = data[1];

			if (l > len-2)
				return 0;

			memcpy (name, data+2, l);
			name[l] = '\0';

			r = mkdir (name, 0777);

			printf ("mkdir (%s, %x) = %d\n", name, 0x0777, r);
			break;
		case PROTO_RMDIR:
			if (len < 3)
				return 0;

			l = data[1];

			if (l > len-2)
				return 0;

			memcpy (name, data+2, l);
			name[l] = '\0';

			r = rmdir (name);

			printf ("rmdir (%s) = %d\n", name, r);
			break;
		case PROTO_CREAT:
			if (len < 3)
				return 0;

			l = data[1];

			if (l > len-2)
				return 0;

			memcpy (name, data+2, l);
			name[l] = '\0';

			r = creat (name, 0777);

			printf ("creat (%s, %x) = %d\n", name, 0x0777, r);
			break;
		case PROTO_RM:
			if (len < 3)
				return 0;

			l = data[1];

			if (l > len-2)
				return 0;

			memcpy (name, data+2, l);
			name[l] = '\0';

			r = remove (name);

			printf ("remove (%s) = %d\n", name, r);
			break;
		case PROTO_CHDIR:
			if (len < 3)
				return 0;

			l = data[1];

			if (l > len-2)
				return 0;

			memcpy (name, data+2, l);
			name[l] = '\0';

			r = chdir (name);

			printf ("chdir (%s) = %d\n", name, r);

			struct dent {
				unsigned char type;
				char name[12];
			} __attribute__ ((__packed__));

			struct dirent **namelist;
			struct dent *d = (struct dent *) name;

			int n = scandir (".", &namelist, 0, alphasort);

			if (n * 13 > 1024) {
				printf ("scandir () : too big namelist\n");
				break;
			}

			if (n < 0)
				perror ("scandir");
			else {
				for (r = 0; r < n; r ++) {
					printf ("%s : 0x%x\n", namelist[r]->d_name, namelist[r]->d_type);
					l = strlen (namelist[r]->d_name);

					if (l > 12)
						l = 12;

					memcpy (d[r].name, namelist[r]->d_name, l);
					d[r].name[l] = '\0';
					d[r].type = namelist[r]->d_type;

					free (namelist[r]);
				}

				free (namelist);

				client_send (c, (char *) name, n * 13);
			}

			break;
		case PROTO_READ:
			if (len < 3)
				return 0;

			l = data[1];

			if (l > len-2)
				return 0;

			memcpy (name, data+2, l);
			name[l] = '\0';

			FILE *fr;
			char s[513];
			
			fr = fopen (name, "r");

			if (!fr) {
			      printf ("fopen (%s) == 0\n", name);
			      goto done;
			}

			while (1) {
				int r = fread (s, sizeof (char), 512, fr);

				if (!r)
					break;

				client_send (c, s, r);
				usleep (10);
			}

			fclose (fr);

			r = 0;

done:
			printf ("read (%s) = %d\n", name, r);
			break;
	}

	return 1;
}
