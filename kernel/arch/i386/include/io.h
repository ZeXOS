/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _X86_H
#define	_X86_H

#include <system.h>
#include <build.h>

#define M_PIC  0x20     /* I/O for master PIC              */
#define M_IMR  0x21     /* I/O for master IMR              */
#define S_PIC  0xA0     /* I/O for slave PIC               */
#define S_IMR  0xA1     /* I/O for slace IMR               */

#define EOI    0x20     /* EOI command                     */

#define ICW1   0x11     /* Cascade, Edge triggered         */
                        /* ICW2 is vector                  */
                        /* ICW3 is slave bitmap or number  */
#define ICW4   0x01     /* 8088 mode                       */

#define M_VEC  0x68     /* Vector for master               */
#define S_VEC  0x70     /* Vector for slave                */

#define OCW3_IRR  0x0A  /* Read IRR                        */
#define OCW3_ISR  0x0B  /* Read ISR                        */


#ifdef ARCH_i386

#define inw(port) ({ \
unsigned short _v; \
__asm__ volatile ("inw %%dx, %%ax":"=a" (_v):"dN" (port)); \
_v; \
})

/*#define outw(value, port) \
__asm__ ("outw %%ax, %%dx"::"a" (value),"dN" (port))*/

#define outb_p(value, port) \
__asm__ ("outb %%al, %%dx\n" \
		"\tjmp 1f\n" \
		"1:\tjmp 1f\n" \
		"1:"::"a" (value),"d" (port))

#define outw_p(value,port) \
__asm__ ("outw %%ax,%%dx\n" \
		"\tjmp 1f\n" \
		"1:\tjmp 1f\n" \
		"1:"::"a" (value),"dN" (port))

#define inb_p(port) ({ \
unsigned char _v; \
__asm__ volatile ("inb %%dx,%%al\n" \
	"\tjmp 1f\n" \
	"1:\tjmp 1f\n" \
	"1:":"=a" (_v):"d" (port)); \
_v; \
})


/* This defines what the stack looks like after an ISR was running */
struct regs
{
	unsigned int ds, es, fs, gs;
	unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax;
	unsigned int int_no, err_code;
	unsigned int eip, cs, eflags, useresp, ss;
};

/* registers pushed on the stack by exceptions or interrupts that
switch from user privilege to kernel privilege
	*** WARNING ***
The layout of this struct must agree with the order in which
registers are pushed and popped in the assembly-language
interrupt handler code. */
typedef struct
{
	unsigned edi, esi, ebp, esp, ebx, edx, ecx, eax; /* PUSHA/POP */
	unsigned ds, es, fs, gs;
	unsigned which_int, err_code;
	unsigned eip, cs, eflags, user_esp, user_ss; /* INT nn/IRET */
	unsigned v_es, v_ds, v_fs, v_gs; /* V86 mode only */
} uregs_t;

#define _set_gate(gate_addr, type, dpl, addr) \
__asm__ ("movw %%dx,%%ax\n\t" \
	"movw %0,%%dx\n\t" \
	"movl %%eax,%1\n\t" \
	"movl %%edx,%2" \
	: \
	: "i" ((short) (0x8000+(dpl<<13)+(type<<8))), \
	"o" (*((char *) (gate_addr))), \
	"o" (*(4+(char *) (gate_addr))), \
	"d" ((char *) (addr)),"a" (0x00080000))

void dma_xfer (unsigned char channel, unsigned long address, unsigned int length, unsigned char read);

void enable_irq (unsigned short irq_num);
void disable_irq (unsigned short irq_num);

extern void outb (unsigned port, unsigned val);

extern void init_v86_regs(uregs_t *regs);
extern void do_v86_int(uregs_t *regs, unsigned int_num);

/* gtd.c */
extern void gdt_set_gate (int num, unsigned long base, unsigned long limit, unsigned char access, unsigned char gran);
extern unsigned int gdt_install ();

/* idt.c */
extern void idt_set_gate (unsigned char num, unsigned long base, unsigned short sel, unsigned char flags);
extern unsigned int idt_install ();

/* isrs.c */
extern unsigned int isrs_install ();

/* irq.c */
extern void irq_install_handler (int irq, void (*handler)(struct regs *r));
extern void irq_uninstall_handler (int irq);
extern unsigned int irq_install ();

/* timer.c */
extern void timer_wait (int ticks);
extern unsigned int timer_install ();

#endif

#endif

