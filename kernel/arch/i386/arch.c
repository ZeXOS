/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>

static unsigned long read_pentium_clock ()
{
	register unsigned lb;
	register unsigned hb;
	
	__asm__ __volatile__ ("rdtsc" : "=a" (lb), "=d" (hb));
	
	return( ((unsigned long) hb) << 24 | lb );
}

void cpu_info ()
{
	printf ("CPU: i386 compatibile\nClock: %lu MHz\n", read_pentium_clock ());
}

/* enable interrupts */
unsigned int_enable ()
{
	__asm__ __volatile__("sti"
		:
		:
		);

	return 1;
}

/* disable interrupts */
unsigned int_disable ()
{
	unsigned ret;

	__asm__ __volatile__("pushfl\n"
		"popl %0\n"
		"cli"
		: "=a"(ret)
		:);

	return ret;
}

/* wait one tick */
void arch_cpu_hlt ()
{
#ifndef CONFIG_COMPAT_QEMU	/* dont know, but it is probably Qemu bug, because it hard halt cpu */
	asm volatile ("hlt");
#endif
}

unsigned arch_cpu_reset ()
{
	outb (0x64, 0xFE);

	return 1;
}

unsigned arch_cpu_shutdown ()
{

	return 1;
}

/* architecture initialization process */
unsigned init_arch ()
{
	if (!gdt_install ())
		return 0;

	if (!idt_install ())
		return 0;

	if (!isrs_install ())
		return 0;

	if (!irq_install ())
		return 0;

	if (!timer_install ())
		return 0;

	return 1;
}
