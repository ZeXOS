/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define		ESC	1

#define		STATE_CONNECTED		1
#define		STATE_PASS		2
#define		STATE_WAIT		3
#define		STATE_STAT		4
#define		STATE_LOGGED		5

static struct sockaddr_in serverSock;	// Remote connection
static struct hostent *host;            // Remote host
static int zsock;               	// Socket
static int size;                   	// Count of input/output bytes
static char *buf;			// Buffer
static char *msg;

typedef struct {
	unsigned char state;
	unsigned char mails;
	char *user;
	char *pass;
	char *server;
	unsigned short port;
} CONFIG;
CONFIG config;

unsigned key_pressed (int keycode)
{
	int scancode = getkey ();

	if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;
}

static int send_to_socket (char *data, unsigned len)
{
	int ret;
	if ((ret = send (zsock, data, len, 0)) == -1) {
		printf ("ERROR -> send () = -1\n");
		return -1;
	}

	return ret;
}

int zmail_commmand (char *buf, int len)
{
	if (!strncmp (buf, "quit", 4)) {
		send_to_socket ("QUIT\n", 5);
	} else if (!strncmp (buf, "read", 4)) {
		if (!config.mails) {
			printf ("You've got no email to read\n");
			return 0;
		}
	  
		char str[64];
		sprintf (str, "RETR %d\n", config.mails --);
		
		send_to_socket (str, strlen (str));
	} 
  
	return 0;
}

static int handle_input ()
{
	char i = getch ();

	if (i) {
		setcolor (7, 0);

		putch (i);
		
		if (size < 64) {
			if (i != '\b') {
				msg[size] = i;
				size ++;
			} else {
				size --;
				msg[size] = '\0';
			}
		}

		if (i == '\n') {
			zmail_commmand (msg, size);
			size = 0;
		}
	}

	if (key_pressed (ESC))
		return -1;

	return 1;
}

void zmail_proto_user ()
{
	char str[64];
	sprintf (str, "USER %s\n", config.user);

	send_to_socket (str, strlen (str));
}

void zmail_proto_pass ()
{
	char str[64];
	sprintf (str, "PASS %s\n", config.pass);

	send_to_socket (str, strlen (str));
}

void zmail_proto_stat ()
{
	char str[64];
	sprintf (str, "STAT\n");

	send_to_socket (str, strlen (str));
}

int zmail_proto (char *buf, int len)
{
	if (buf[0] == '-') {
		printf ("ERROR -> %s\n", buf+5);
		return -1;
	}
	
	if (buf[0] != '+') {
		printf ("UNKNOWN cmd: %s\n", buf);
		
		return 0;
	}
	
	char *cmd = buf+1;
	char *msg = buf+4;
	
	/* OK - succefull action */
	if (!strncmp (cmd, "OK", 2)) {
		if (config.state == STATE_CONNECTED) {
			if (msg[0] == '<') {
				char *s = strchr (msg, '@');
				
				if (!s) {
					printf ("ERROR -> invalid authentication\n");
					return -1;
				}
				
				s ++;
				
				if (!strncmp (s, config.server, strlen (config.server))) {
					zmail_proto_user ();
					config.state = STATE_PASS;
				} else {
					printf ("ERROR -> invalid authentication\n");
					return -1;
				}
			}
		} else if (config.state == STATE_PASS) {
			config.state = STATE_WAIT;
			zmail_proto_pass ();
		} else if (config.state == STATE_WAIT) {
			config.state = STATE_STAT;
			printf ("You are logged succefully as %s\n", config.user);
			zmail_proto_stat ();
		} else if (config.state == STATE_STAT) {
			config.state = STATE_LOGGED;
			char *s = strchr (msg, ' ');
			if (s)
				*s = '\0';

			printf ("You've got %s new email(s)\n", msg);
			
			config.mails = atoi (msg);
		}
	}
  
	return 0;
}

static int zmail_init (const char *addr, int port)
{
	// Let's get info about remote computer
	if ((host = gethostbyname ((char *) addr)) == NULL) {
		printf ("Wrong address -> %s:%d\n", addr, port);
		return 0;
	}

	// Create socket
	if ((zsock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("Cant create socket\n");
		return 0;
	}
	
	// Fill structure sockaddr_in
	// 1) Family of protocols
	serverSock.sin_family = AF_INET;
	// 2) Number of server port
	serverSock.sin_port = htons (port);
	// 3) Setup ip address of server, where we want to connect
	memcpy (&(serverSock.sin_addr), host->h_addr, host->h_length);
	
	// Now we are able to connect to remote server
	if (connect (zsock, (struct sockaddr *) &serverSock, sizeof (serverSock)) == -1) {
		printf ("Connection cant be estabilished -> %s:%d\n", addr, port);
		return -1;
	}

	// Set socket to non-blocking mode
	int oldFlag = fcntl (zsock, F_GETFL, 0);
	if (fcntl (zsock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("Cant set socket to nonblocking mode\n");
		return 0;
	}

	printf ("zmail -> connected to %s:%d\n", addr, port);

	buf = (char *) malloc (sizeof (char) * 500);          		// receive buffer

	if (!buf)
		return 0;

	msg = (char *) malloc (sizeof (char) * 64);

	if (!msg)
		return 0;
	
	size = 0;
	
	config.state = STATE_CONNECTED;
	
	return 1;
}

static int zmail_loop ()
{
	int ret = recv (zsock, buf, 100, 0);
  
	if (handle_input () == -1)
		return -1;
	
	if (ret > 0) {
		setcolor (14, 0);
		buf[ret] = '\0';
		printf (buf);
		
		zmail_proto (buf, ret);
	} else {
		if (ret == -1)
			printf ("HOST -> Connection refused\n");
	}

	return ret;
}

int main (int argc, char **argv)
{
	config.mails = 0;
	config.state = 0;
	config.user = strdup ("login@server");
	config.pass = strdup ("pwd");
	config.server = strdup ("server");
	config.port = 110;
	
	printf ("user: %s\nserver: %s\n", config.user, config.server);

	int ret = zmail_init ((char *) config.server, config.port);

	if (ret < 1)
		goto end;

	for (;; schedule ()) {
		ret = zmail_loop ();
		
		if (ret == -1)
			break;
	}

	free (msg);
	free (buf);
end:
	printf ("zmail -> Bye !\n");

	close (zsock);

	return 0;
}
 
