/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <config.h>

static unsigned char vgaused = 0;	/* some program using graphics mode ? */
unsigned char vgagui = 0;		/* 0 - textual; 1 - 8bpp graphics; 2 - 16bpp graphics mode*/
char *vgafb;				/* frame buffer */
char *vgadb;				/* double buffer */

unsigned vgafb_res_x, vgafb_res_y;	/* screen resolution */
int attrib = 0x0F;
int csr_x = 0, csr_y = 0;

extern unsigned short *vesafb;

#ifdef ARCH_i386

/* These define our textpointer, our background and foreground
*  colors (attributes), and x and y cursor coordinates */
unsigned short *textmemptr;

const int MiscOutputReg = 0x3c2;
const int DataReg = 0x3c0;
const int AddressReg = 0x3c0;

const char mode03[][32] = {
	{ 0x03, 0x00, 0x03, 0x00, 0x02 },
	{ 0x5F, 0x4F, 0x50, 0x82, 0x55, 0x81, 0xBF, 0x1F,
	0x00, 0x4F, 0x0D, 0x0E, 0x00, 0x00, 0x00, 0x50,
	0x9C, 0x0E, 0x8F, 0x28, 0x1F, 0x96, 0xB9, 0xA3,
	0xFF },
	{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x0E, 0x00,
	0xFF },
	{ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x14, 0x07,
	0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
	0x0C, 0x00, 0x0F, 0x08, 0x00 }
};

const char mode13[][32] = {
  { 0x03, 0x01, 0x0f, 0x00, 0x0e },            /* 0x3c4, index 0-4*/
  { 0x5f, 0x4f, 0x50, 0x82, 0x54, 0x80, 0xbf, 0x1f,
    0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x9c, 0x0e, 0x8f, 0x28, 0x40, 0x96, 0xb9, 0xa3,
    0xff },                                    /* 0x3d4, index 0-0x18*/
  { 0, 0, 0, 0, 0, 0x40, 0x05, 0x0f, 0xff },   /* 0x3ce, index 0-8*/ 
  { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 
    0x41, 0, 0x0f, 0, 0 }                      /* 0x3c0, index 0-0x14*/
};

/*const char mode18[][32] = {

	{ 0x03, 0x01, 0x08, 0x00, 0x06 },
	{ 0x5F, 0x4F, 0x50, 0x82, 0x54, 0x80, 0x0B, 0x3E,
	0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0xEA, 0x0C, 0xDF, 0x28, 0x00, 0xE7, 0x04, 0xE3,
	0xFF },
	{ 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x05, 0x0F,
	0xFF },
	{ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x14, 0x07,
	0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
	0x01, 0x00, 0x0F, 0x00, 0x00 }
};*/

/* Scrolls the screen */
void video_scroll (void)
{
	unsigned blank, temp;

	/* A blank is defined as a space... we need to give it
	*  backcolor too */
	blank = 0x20 | (attrib << 8);

	/* Row 25 is the end, this means we need to scroll up */
	if (csr_y >= 25)	{
		/* Move the current text chunk that makes up the screen
		*  back in the buffer by a line */
		temp = csr_y - 25 + 1;
		memcpy (textmemptr, textmemptr + temp * 80, (25 - temp) * 80 * 2);

		/* Finally, we set the chunk of memory that occupies
		*  the last line of text to our 'blank' character */
		memsetw (textmemptr + (25 - temp) * 80, blank, 80);
		csr_y = 25 - 1;
	}
}

/* Updates the hardware cursor: the little blinking line
*  on the screen under the last character pressed! */
void video_move_csr (void)
{
	unsigned temp;

	/* The equation for finding the index in a linear
	*  chunk of memory can be represented by:
	*  Index = [(y * width) + x] */
	temp = csr_y * 80 + csr_x;

	/* This sends a command to indicies 14 and 15 in the
	*  CRT Control Register of the VGA controller. These
	*  are the high and low bytes of the index that show
	*  where the hardware cursor is to be 'blinking'. */
	outb (0x3D4, 14);
	outb (0x3D5, temp >> 8);
	outb (0x3D4, 15);
	outb (0x3D5, temp);
}

/* Clears the screen */
void video_cls ()
{
	unsigned blank;
	int i;

	/* Again, we need the 'short' that will be used to
	*  represent a space with color */
	blank = 0x20 | (attrib << 8);

	/* Sets the entire screen to spaces in our current
	*  color */
	for (i = 0; i < 25; i ++)
		memsetw (textmemptr + i * 80, blank, 80);

	/* Update out virtual cursor, and then move the
	*  hardware cursor */
	csr_x = 0;
	csr_y = 0;

	video_move_csr();
}

/* Puts a single character on the screen */
void video_putch (unsigned char c)
{
#ifndef CONFIG_UI_CONSOLESERIAL
	unsigned short *where;
	unsigned att = attrib << 8;
	
	/* Handle a backspace, by moving the cursor back one space */
	if (c == 0x08) {
		if (csr_x != 0)
			csr_x --;
	
		/* delete last character */
		where = textmemptr + (csr_y * 80 + csr_x);
		*where = ' ' | att;	/* Character AND attributes: color */
	}

	/* Handles a tab by incrementing the cursor's x, but only
	 *  to a point that will make it divisible by 8 */
	else if (c == 0x09)
		csr_x = (csr_x + 8) & ~(8 - 1);

	/* Handles a 'Carriage Return', which simply brings the
	 *  cursor back to the margin */
	else if (c == '\r')
		csr_x = 0;

	/* We handle our newlines the way DOS and the BIOS do: we
	 *  treat it as if a 'CR' was also there, so we bring the
	 *  cursor to the margin and we increment the 'y' value */
	else if (c == '\n') {
		csr_x = 0;
		csr_y ++;
	}
	/* Any character greater than and including a space, is a
	 *  printable character. The equation for finding the index
	 *  in a linear chunk of memory can be represented by:
	 *  Index = [(y * width) + x] */
	else if (c >= ' ') {
		where = textmemptr + (csr_y * 80 + csr_x);
		*where = c | att;	/* Character AND attributes: color */
		csr_x ++;
	}
	
	/* If the cursor has reached the edge of the screen's width, we
	 *  insert a new line in there */
	if (csr_x >= 80) {
		csr_x = 0;
		csr_y ++;
	}
	
	/* Scroll the screen if needed, and finally move the cursor */
	video_scroll ();
	video_move_csr ();
#else
#ifdef CONFIG_DRV_RS232
	/* Serial console output */
	rs232_write ((char) c);
#endif
#endif
}

void video_setattrib (int attr)
{
	attrib = attr;
}

int video_getattrib ()
{
	return attrib;
}

/* Sets the forecolor and backcolor that we will use */
void video_color (unsigned char forecolor, unsigned char backcolor)
{
	/* Top 4 bytes are the background, bottom 4 bytes
	 *  are the foreground color */
	attrib = (backcolor << 4) | (forecolor & 0x0F);
}

/* Set the cursor to selected position */
void video_gotoxy (unsigned x, unsigned y)
{
	csr_x = x;
	csr_y = y;

	video_scroll ();
	video_move_csr ();
}

/* video card mono/colour detection by Dark Fiber
 * returns 0=mono, 1=colour
 */
int video_type (void)
{
	char c = (*(unsigned short*) 0x410&0x30);

	/* C can be 0x00 or 0x20 for colour, 0x30 for mono */
	if(c == 0x30)
		return 0;   // mono
	else
		return 1;   // colour
}

/* Sets our text-mode VGA pointer, then clears the screen for us */
unsigned int init_video (void)
{
	if (video_type ())	// check type of video card - color/mono
		textmemptr = (unsigned short *) 0xB8000;
	else
		textmemptr = (unsigned short *) 0xA0000;

	vgagui = 0;
	vgaused = 0;

	video_cls ();

	if (init_video_vesa ())
		init_vgafb ();

	return 1;
}

/**
	Graphics mode
*/

char *init_vgafb ()
{
	//if (vgaused)
	//	return (char *) ~0;
  
	vgaused ++;
	
	vgadb = (char *) 0;
	vgafb = (char *) 0;

#ifdef CONFIG_DRV_VGA
	int i;

	/* VGA mode */
	if (!vgagui) {
		outb_p (0x63, MiscOutputReg);
		outb_p (0x00, 0x3da);
		
		for (i = 0; i < 5; i ++) {
			outb_p (i, 0x3c4);
			outb_p (mode13[0][i], 0x3c4+1);
		}
		
		outw_p (0x0e11, 0x3d4);
		
		for (i = 0; i < 0x19; i ++) {
			outb_p (i, 0x3d4);
			outb_p (mode13[1][i], (0x3d4 + 1));
		}
		
		for (i = 0; i < 0x9; i ++) {
			outb_p (i, 0x3ce);
			outb_p (mode13[2][i], (0x3ce + 1));
		}
		
		inb_p (0x3da);
		
		for (i = 0; i < 0x15; i ++) {
			inw (DataReg);
			outb_p (i, AddressReg);
			outb_p (mode13[3][i], DataReg);
		}
		
		outb_p (0x20, 0x3c0);
	
		vgagui = 1;
	
		vgafb_res_x = 320;
		vgafb_res_y = 200;

		vgafb = (char *) 0xA0000;
		vgadb = 0;//(char *) kmalloc (sizeof (char) * vgafb_res_x * vgafb_res_y);
	}

#endif
#ifdef CONFIG_DRV_VESA
	/* VESA mode */
	if (vgagui == 2) {
		vgafb_res_x = 800;
		vgafb_res_y = 600;

		vgafb = (char *) vesafb;
		vgadb = (char *) 0x400000;//kmalloc (sizeof (short) * vgafb_res_x * vgafb_res_y);
		//vgatb = (char *) PAGE_MEM_LOW+0x200000;//kmalloc (sizeof (char) * vgafb_res_x * vgafb_res_y);
	}

	if (!vgadb)
		vgadb = vgafb;
#endif

  	return vgadb;
}

void video_gfx_exit ()
{
	/* we dont use VGA anymore */
	if (vgaused)
		vgaused = 0;
  
	if (vgagui != 1) {
		/* for VESA mode */
		if (vgagui == 2)
			video_gfx_cls (0);

		return;
	}

	int i;
	
	outb_p (0x67, MiscOutputReg);
	outb_p (0x00, 0x3da);
	
	for (i = 0; i < 5; i ++) {
		outb_p (i, 0x3c4);
		outb_p (mode03[0][i], 0x3c4+1);
	}
	
	outw_p (0x0e11, 0x3d4);
	
	for (i = 0; i < 0x19; i ++) {
		outb_p (i, 0x3d4);
		outb_p (mode03[1][i], (0x3d4 + 1));
	}
	
	for (i = 0; i < 0x9; i ++) {
		outb_p (i, 0x3ce);
		outb_p (mode03[2][i], (0x3ce + 1));
	}
	
	inb_p (0x3da);
	
	for (i = 0; i < 0x15; i ++) {
		inw (DataReg);
		outb_p (i, AddressReg);
		outb_p (mode03[3][i], DataReg);
	}
	
	outb_p (0x20, 0x3c0);

	vgagui = 0;

	vgafb_res_x = 0;
	vgafb_res_y = 0;

	if (vgadb != vgafb)
		kfree (vgadb);
}

/* Draw pixel (dot) on the screen with specified color */
void video_gfx_pixel (unsigned x, unsigned y, unsigned color)
{
	if (x >= vgafb_res_x || y >= vgafb_res_y)
		return;

	unsigned i = y * vgafb_res_x + x;

	if (vgagui == 2) {
		unsigned short *db = (unsigned short *) vgadb;

		db[i] = color;

		return;
	}

	if (vgagui == 1) {
		vgadb[i] = color;

		return;
	}
}

/* Swap double-buffered screen */
void video_gfx_fbswap ()
{
	paging_disable ();

#ifdef CONFIG_DRV_GMM
	unsigned l = (vgafb_res_x * vgafb_res_y) / 2;
	unsigned i = 0;
	unsigned *fb = (unsigned *) vgafb;
	unsigned *db = (unsigned *) vgadb;

	for (; i < l; i ++) {
		fb[i] = db[i];
		db[i] = 0;
	}
#else
	if (vgadb != vgafb) {
		unsigned l = vgafb_res_x * vgafb_res_y * vgagui;
		unsigned i = 0;
		for (i = 0; i < l; i ++) {
			vgafb[i] = vgadb[i];
			vgadb[i] = 0;
		}
	}
#endif

	paging_enable ();
}

/* Clear screen with specified color*/
void video_gfx_cls (unsigned color)
{
	unsigned short *db = (unsigned short *) vgadb;

	unsigned x;
	unsigned l = vgafb_res_x * vgafb_res_y;

	for (x = 0; x < l; x ++)
		db[x] = color;
}

#endif

/** ARM architecture **/
#ifdef ARCH_arm
/* Draw pixel (dot) on the screen with specified color */
void video_gfx_pixel (unsigned x, unsigned y, unsigned color)
{
	unsigned i = y * 640 + x;
	unsigned short *db = (unsigned short *) 0x500000;

	db[i] = color;
}

/* Clear screen with specified color*/
void video_gfx_cls (unsigned color)
{
	unsigned short *db = (unsigned short *) vgadb;

	unsigned x;
	unsigned l = vgafb_res_x * vgafb_res_y;

	for (x = 0; x < l; x ++)
		db[x] = color;
}

/* Swap double-buffered screen */
void video_gfx_fbswap ()
{
/*#ifdef CONFIG_DRV_GMM
	unsigned l = (vgafb_res_x * vgafb_res_y) / 2;
	unsigned i = 0;
	unsigned *fb = (unsigned *) vgafb;
	unsigned *db = (unsigned *) vgadb;

	for (; i < l; i ++) {
		fb[i] = db[i];
		db[i] = 0;
	}
#else
	if (vgadb != vgafb) {
		unsigned l = vgafb_res_x * vgafb_res_y * vgagui;
		unsigned i = 0;
		for (i = 0; i < l; i ++) {
			vgafb[i] = vgadb[i];
			vgadb[i] = 0;
		}
	}
#endif*/
}

void video_putch (unsigned char c)
{
	/* uart output for embedded devices */
	rs232_write ((char) c);
}

/* Sets the forecolor and backcolor that we will use */
void video_color (unsigned char forecolor, unsigned char backcolor)
{
	/* Top 4 bytes are the background, bottom 4 bytes
	 *  are the foreground color */
	attrib = (backcolor << 4) | (forecolor & 0x0F);
}

void video_cls ()
{

}

/* Set the cursor to selected position */
void video_gotoxy (unsigned x, unsigned y)
{

}

unsigned int init_video (void)
{
	return 0;
}
#endif

/* Device handler */
bool video_acthandler (unsigned act, char *block, char *more, int block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{
			vgaused = 0;
			return 1;
		}
		break;
	}

	return 0;
}
