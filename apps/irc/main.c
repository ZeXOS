/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "commands.h"
#include "config.h"
#include "proto.h"
#include "net.h"

CONFIG config;

extern char *buf;

int pick_nick ()
{
	char nick[64];

	printf ("Pick a nick: ");

	scanf ("%s", nick);

	printf ("\nPlease wait ..\n");

	config.nick = strdup (nick);

	return 1;
}

int init (char *address, int port, int flags)
{
	pick_nick ();

	if (!init_net (address, port, flags))
		return 0;

	config.server = address;
	config.server_len = strlen (address);

	if (!init_proto ())
		return 0;

	if (!commands_init ())
		return 0;

	return 1;
}

int loop ()
{
	while (1) {
		if (!net_loop ())
			return 0;

		if (!proto_parser ())
			return 0;

		if (!commands_get ())
			return 0;
#ifndef LINUX
		schedule ();
#endif
	}

	return 1;
}

int quit ()
{
	net_close ();

	printf ("Bye !\n");

	return 1;
}

void syntax ()
{
	printf ("irc: <address> <port> [-6]\nGNU/GPL3 - Coded by ZeXx86\n");
}

int main (int argc, char **argv)
{
	if (argc == 1) {
		syntax ();
		return 0;
	}

	int port = 0;
	int proto = 0;

	if (argc > 2) {
		port = atoi (argv[2]);

		if (argc > 3)
		if (!strncmp (argv[argc-1], "-6", 2)) {
			proto |= NET_IPV6;
			printf ("> IPv6 mode\n");
		}
	}

	if (!port)
		port = 6667;	/* set default port, when user not specify any */

	printf ("-= IRC Client =-\n");

	if (!init (argv[1], port, proto)) {
		printf ("ERROR -> init () failed\n");
		return -1;
	}

	loop ();

	quit ();

	/* free buffers */
	free (buf);
	free (config.nick);
	free (config.server);

	if (config.channel_len)
		free (config.channel);

	return 0;
}
