/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "config.h"
#include "client.h"
#include "proto.h"
#include "net.h"

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>

int sock;				// main socket
char buf[257];				// receive buffer
unsigned addrlen;			// length of clientInfo


struct sockaddr_in sockName;         	// name of socket
struct sockaddr_in clientInfo;       	// connected client information

extern int errno;

extern client_t client_list;

/** Network Send function */
int net_send (int sock, char *buf, unsigned len)
{
	return send (sock, buf, len, 0);
}

/** Network Init function */
int init_net ()
{
	// Create mainsocket
	if ((sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("> ERROR -> I can't create main socket");
		return 0;
	}

	/* Set to nonblocking socket mode */
	int oldFlag = fcntl (sock, F_GETFL, 0);
	if (fcntl (sock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("> ERROR -> Problem with set socket mode");
		return 0;
	}

#ifdef __linux__
	/* Unblock re-use of previously binded port, when server die or something else */
	unsigned long yes = 1;
	setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, (char *) &yes, sizeof (yes));
#endif

	// Put data to structure sockaddr_in
	// 1) Family of protocol
	sockName.sin_family = AF_INET;
	// 2) Number of listen port
	sockName.sin_port = htons (DEFAULT_NET_PORT);
	// 3) Set local ip address. You can connect in with whatever IP
	sockName.sin_addr.s_addr = INADDR_ANY;

	/* Bind port */
	if (bind (sock, (struct sockaddr *) &sockName, sizeof (sockName)) == -1) {
		printf ("> ERROR -> Port is used or danied");
		return 0;
	}

	// Crete front (max 100 sessions in one time)
	if (listen (sock, 100) == -1) {
		printf ("> ERROR -> I can't create front");
		return 0;
	}

	addrlen = sizeof (clientInfo);

	return 1;
}

/** Network Loop function */
int net_loop ()
{
	int newsock = accept (sock, (struct sockaddr *) &clientInfo, &addrlen);

	/* new client is connected */
#ifdef __linux__
	if (errno == EAGAIN && newsock != -1) {
#else
	if (newsock > 0) {
#endif
		client_new (newsock);

		/* Set to nonblocking socket mode */
		int oldFlag = fcntl (newsock, F_GETFL, 0);
		if (fcntl (newsock, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
			printf ("> ERROR -> Problem with set socket mode\n");
			return 0;
		}

		printf ("-> New client was connected\n");
	}

	/* HACK: this is pretty needed for less cpu load */
	usleep (60);

	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next) {
		int ret = recv (c->sock, buf, 256, 0);

		/* new data are available */
		if (ret > 0) {
			buf[ret] = '\0';
			
			/* handle received data */
			proto_handler (c, buf, ret);
		}

		/* client go out */
#ifdef __linux__
		if (!ret) {
#else
		if (ret == -1) {
#endif
			close (c->sock);

			client_quit (c);

			printf ("-> Client was disconnected\n");

			break;
		}
	}

	return 1;
}
