/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define SHELL_VERSION 		"0.0.3"

char *cmdline;
int cmdline_len;

unsigned key_pressed (int keycode)
{
	int scancode = getkey ();

	if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;
}

int init ()
{
	cmdline = (char *) malloc (sizeof (char) * 64);

	if (!cmdline) {
		printf ("ERROR -> not enough memory !\n");
		return 0;
	}
	
	cmdline_len = 0;

	return 1;
}

int refresh_cmd ()
{
	setcolor (7, 0);
	printf (" $ ");
	setcolor (15, 0);

	return 1;
}

int check_cmd ()
{
	if (!cmdline_len)
		return 1;

	printf ("\n");

	/* exit app */
	if (!strcmp (cmdline, "exit"))
		return 0;

	system (cmdline);

	refresh_cmd ();

	return 1;
}

int loop ()
{
	int ret = 1;
	unsigned char c;

	c = getch ();

	if (c && cmdline_len < 64) {
		cmdline[cmdline_len] = (char) c;

		if (c == '\n') {
			cmdline[cmdline_len] = '\0';
			ret = check_cmd ();
			cmdline_len = 0;
		} else {
			if (c == '\b') {
				if (cmdline_len > 0)
					cmdline_len --;
			} else
				cmdline_len ++;
  
			if (cmdline_len >= 0)
				putch (c);
		}
	}

	schedule ();

	return ret;
}

int main (int argc, char **argv)
{
	printf ("Shell v%s\n", SHELL_VERSION); 

	int ret = init ();

	refresh_cmd ();

	while (ret)
		ret = loop ();

	printf ("Bye !\n");

	free (cmdline);

	return 0;
}
 
