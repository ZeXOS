/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "window.h"
#include "cursor.h"
#include "button.h"
#include "menu.h"
#include "dialog.h"
#include "filemanager.h"
#include "config.h"

void taskbar_draw ()
{
	/* main line */
	xline (0, 587, 799, 587, 0x0d4ef4);

	/* bar lines */

	unsigned i = 0;

	for (i = 0; i < 12; i ++)
		xline (41, 588+i, 799, 588+i, 0x0d4ed4+i);

}

unsigned init_taskbar ()
{


	return 1;
}
