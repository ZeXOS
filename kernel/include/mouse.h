/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _MOUSE_H
#define _MOUSE_H

#define	MOUSE_FLAG_BUTTON1	0x1	/* Left mouse button */
#define	MOUSE_FLAG_BUTTON2	0x2	/* Right mouse button */
#define	MOUSE_FLAG_BUTTON3	0x4	/* Middle mouse button */
#define	MOUSE_FLAG_BUTTON4	0x8	/* Extra mouse button */
#define	MOUSE_FLAG_BUTTON5	0x10	/* Extra mouse button */
#define	MOUSE_FLAG_SCROLLUP	0x20	/* Scroll button/wheel - step up */
#define	MOUSE_FLAG_SCROLLDOWN	0x40	/* Scroll button/wheel - step down  */

/* low-level mouse structure */
typedef struct {
	unsigned short flags;		/* Mouse button flags */
	short pos_x;			/* Difference - horizontal position */
	short pos_y;			/* Difference - vertical position */
	void *update;			/* Address of update function*/
} dev_mouse_t;

/* externs */
extern bool commouse_acthandler (unsigned act, char *block, unsigned block_len);

#endif
