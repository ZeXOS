;  ZeX/OS
;  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
;  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@gmail.com)
;
;  This program is free software: you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  You should have received a copy of the GNU General Public License
;  along with this program.  If not, see <http://www.gnu.org/licenses/>.


; This is the kernel's entry point. We could either call main here,
; or we can use this to setup the stack or other nice stuff, like
; perhaps setting up the GDT and segments. Please note that interrupts
; are disabled at this point: More on interrupts later!

[SECTION .text]
[BITS 32]

extern main
global _start

_start:
	; first we get arguments
	mov eax, 26
	mov ecx, 0
	int 0x80

	push eax	; argv

	; lets get count of arguments
	mov eax, 26
	mov ecx, 1
	int 0x80

	mov ebx, eax
	push ebx	; argc

	call main	; call our program

	pop ebx
	pop eax

	; proc exit
	mov eax, 1
	int 0x80

	ret