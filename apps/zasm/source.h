/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SOURCE_H
#define __SOURCE_H

/* Variable structure */
typedef struct var_context {
	struct var_context *next, *prev;

	char type;
	char *name;
	void *address;
	unsigned offset;
} var_t;

#define VAR_TYPE_GLOBAL		0x1
#define VAR_TYPE_DB		0x2

extern int source_parse (char *source, unsigned size);
extern char *source_open (char *file, unsigned *size);
extern int source_init ();

#endif
