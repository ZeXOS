/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _MENU_H
#define _MENU_H

#define MENU_FLAG_SHOW		0x1
#define MENU_FLAG_DISABLED	0x2

#define MENUITEM_FLAG_MOUSE	0x1
#define MENUITEM_FLAG_CLICKED	0x2

typedef unsigned (wmmenu_handler) ();

typedef struct wmmenuitem_context {
	struct wmmenuitem_context *next, *prev;

	wmmenu_handler *handler;
	unsigned char flags;
	char *caption;
} wmmenuitem;

typedef struct wmmenu_context {
	struct wmmenu_context *next, *prev;

	wmmenuitem item_list;
	unsigned x;
	unsigned y;
	unsigned size_x;
	unsigned size_y;
	unsigned char flags;
	char *caption;
} wmmenu;

/* externs */
extern wmmenu *menu_create (unsigned x, unsigned y, const char *caption);
extern wmmenuitem *menu_additem (wmmenu *menu, const char *caption, wmmenu_handler *handler);
extern unsigned char menuitem_flags (wmmenuitem *item);
extern unsigned menu_draw_all ();
extern unsigned menu_show (wmmenu *menu);
extern unsigned init_menu ();

#endif
