/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "commands.h"
#include "config.h"
#include "proto.h"
#include "net.h"

char *parser;
char *parser_tmp;
char *pong_msg;
unsigned parser_len;

#ifdef LINUX
int setcolor (unsigned char fg, unsigned char bg)
{
	return 1;
}
#endif

int proto_command (char *buffer, unsigned len)
{
	buffer[len] = '\0';

	if (buffer[0] == ':') {
		/* server message */

		if (!strncmp (config.server, buffer+1, config.server_len)) {
			char *str = strchr (buffer+2+config.server_len, ':');

			/* servername 353 - user list */
			setcolor (2, 0);
			printf ("SERVER -> %s\n", str ? str+1 : "!MESSAGE PARSE ERROR!\n");
			setcolor (15, 0);
			return 1;
		}

		char *str = strchr (buffer+2, '!');

		/* cut of nick */
		if (str)
			str[0] = '\0';

		char *str2 = strchr (str+1, ' ');

		if (!str2) {
			printf ("ERROR -> unknown command from server\n");
			return 1;
		}

		/* chat message */
		if (!cstrcmp ("PRIVMSG", str2+1)) {
			char *msg = strchr (str2+10, ':');

			if (!msg)
				return 1;

			msg --;
			/* cut of channel name */
			msg[0] = '\0';

			msg += 2;

			//if (!strncmp (str2+9, config.channel, config.channel_len)) {
				if (strncmp (msg+1, "ACTION", 6)) {
					setcolor (14, 0);
					printf ("> %s: %s\n", buffer+1, msg);
				} else {
					setcolor (10, 0);
					printf ("> ***%s %s\n", buffer+1, msg+8);
				}
			/*} else {
				if (strncmp (msg+1, "ACTION", 6)) {
					setcolor (11, 0);
					printf ("> (%s) %s: %s\n", str2+9, buffer+1, msg);
				} else {
					setcolor (3, 0);
					printf ("> (%s) ***%s %s\n", str2+9, buffer+1, msg+8);
				}
			}*/

			setcolor (15, 0);
			return 1;
		} else if (!cstrcmp ("QUIT", str2+1)) {
			setcolor (4, 0);
			printf ("User %s was disconnected\n", buffer+1);
			setcolor (15, 0);
			return 1;
		}

		return 1;

	}

	if (!strncmp (buffer, "PING", 4))
		return net_send (pong_msg, 10+(3*config.server_len));

	setcolor (7, 0);
	printf (">> %s\n", buffer);
	setcolor (15, 0);

	return 1;
}

int proto_parser ()
{
	unsigned i = 0;

	while (i < parser_len) {
		if (parser[i] == '\n') {
			proto_command (parser, i);

			i ++;

			//memcpy (parser+parser_len-len+i, buffer, len);
			if (i == parser_len) {
				parser_len = 0;
				parser = parser_tmp;
				//printf ("parser_len == i - NULL\n");
				//memset (parser, 0, PROTO_BUFSIZE_PARSER);
				break;
			} else {
				//printf ("parser_len: %d / %d\n", i, parser_len);

				parser += i;
				parser_len -= i;

				proto_parser ();
				break;
			}
		}

		i ++;
	}

	return 1;
}

int proto_handler (char *buffer, unsigned len)
{
	if (parser_len > (PROTO_BUFSIZE_PARSER-len)) {
		printf ("ERROR -> receive buffer is full, this cause loss of last packets\n");

		return 0;
	}

	memcpy (parser+parser_len, buffer, len);
	parser_len += len;

	//printf ("proto_handler () - %d\n", len);

	return 1;
}

int init_proto ()
{
	parser = (char *) malloc (sizeof (char) * PROTO_BUFSIZE_PARSER);

	if (!parser)
		return 0;

	parser_tmp = parser;

	parser_len = 0;

	char str[64];
	sprintf (str, "USER %s blah blah :zexos irc client\nNICK %s\n", config.nick, config.nick);

	net_send (str, strlen (str));

	pong_msg = (char *) malloc (sizeof (char) * (config.server_len*3 + 10));

	if (!pong_msg)
		return 0;

	pong_msg[0] = ':';
	memcpy (pong_msg+1, config.server, config.server_len);
	memcpy (pong_msg+1+config.server_len, " PONG ", 6);
	memcpy (pong_msg+7+config.server_len, config.server, config.server_len);
	memcpy (pong_msg+7+(2*config.server_len), " :", 2);
	memcpy (pong_msg+9+(2*config.server_len), config.server, config.server_len);
	pong_msg[9+(3*config.server_len)] = '\n';

	return 1;
}
