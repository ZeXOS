/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _AUDIO_H
#define _AUDIO_H

#include <dev.h>

#define SOUND_FORMAT_U8		1
#define SOUND_FORMAT_S8		2
#define SOUND_FORMAT_U16	3
#define SOUND_FORMAT_S16	4

/* Device structure */
typedef struct snd_audio_context {
	struct snd_audio_context *next, *prev;

	dev_t *dev;
	unsigned short rate;
	unsigned char format;
	unsigned char channels;
	unsigned char flags;
	unsigned char state;
} snd_audio_t;

/* Config */
typedef struct {
	char *dev;

	unsigned short rate;
	unsigned char format;
	unsigned char channels;
	unsigned char flags;
} snd_cfg_t;

/* externs */
extern unsigned int init_audio ();
extern snd_audio_t *audio_open (snd_cfg_t *cfg);
extern int audio_write (snd_audio_t *aud, char *buf, unsigned len);
extern int audio_close (snd_audio_t *aud);

#endif 
