/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <setjmp.h>
#include <task.h>
#include <spinlock.h>

task_t task_list;
task_t *_curr_task;
extern unsigned long timer_ticks;

unsigned task_id = NULL;

SPINLOCK_CREATE (sched_spinlock);


/** 	SCHEDULE
 *	Function, which handle each task.
 *	It jump from task to another task in moment, when schedule () is called.
 */

void schedule (void)
{
	/* avoid for crash */
	if (!_curr_task)
		return;

	if (_curr_task->spinlock)
		if (_curr_task->spinlock->locked)
			return;

	/* we dont need check spinlock now, because we check it below, 
		when we are sure about running task */
	spinlock_lock (&sched_spinlock);

	/* check task with priority > 0 */
	if (_curr_task->priority) {	/* is it on kernel level ? No ? Let's continue */
		_curr_task->attick ++;
		
		_curr_task->lasttick = timer_ticks;			/* remember current tick time */
#ifdef ARCH_i386
		if (_curr_task->attick < 256/_curr_task->priority)	/* calculate when process have to be block and when not */
			return;
		else
			_curr_task->attick = 0;				/* handle current task and null attick counter */
#else
		/*if (_curr_task->attick < 256/_curr_task->priority)*/	/* calculate when process have to be block and when not */
		//	return;
		/*else*/
			_curr_task->attick = 0;				/* handle current task and null attick counter */
#endif
	}

	/* set current task */
	if (arch_task_set (_curr_task))
		return;

	for (;;) {
		/* continue on next task */
		_curr_task = _curr_task->next;

		/* task is out of task_list structure */
		if (_curr_task == &task_list)	
			_curr_task = task_list.next;	/* current task is first task in task_list structure */

		/* Is current task running ? */
		if (_curr_task->status == TS_RUNNABLE) {
#ifdef ARCH_i386
			/* switch page directory for current task */
			if (_curr_task->page_cover)
				page_dir_switch (_curr_task->page_cover->page_dir);
			else    /* when page_cover pointer is NULL, it mean that kernel task is current */
				page_dir_switch (task_list.next->page_cover->page_dir);
#endif
			if (!int_disable ())
				return;

			if (_curr_task->spinlock)
				if (_curr_task->spinlock->locked)
					spinlock_unlock (&sched_spinlock);


			/* switch to new/current task */
			arch_task_switch (_curr_task);

			return;
		}
	}

}
/*****************************************************************************
*****************************************************************************/
void task_schedule ()
{
	for (;;)
		schedule ();
}

extern unsigned short *vesafb;
extern unsigned char vgagui;
task_t *task_create (char *name, unsigned entry, unsigned priority)
{
	if (priority > 255)	/* priority 256 is max */
		priority = 255;

	/* alloc and init context */
	task_t *task = (task_t *) kmalloc (sizeof (task_t));

	/* out of memory ? */
	if (!task)
		return 0;

	/* kernel ? */
	if (entry == NULL) {
		/* mark task #0 (idle task) runnable */
		task->status = TS_RUNNABLE;

		task->priority = priority;
		// add name of the task
		strcpy (task->name, name);

		task->spinlock = &sched_spinlock;

		task->id = task_id;
#ifdef ARCH_i386
		task->page_cover = (page_ent_t *) page_cover_create ();

		/* kernel covering by paging */
		page_mmap (task->page_cover, 0, (void *) PAGE_MEM_LOW, 1, 0);

		/* framebuffer covering by paging */
		page_mmap (task->page_cover, (void *) PAGE_MEM_LOW, (void *) PAGE_MEM_FB, 1, 1);

		/* user-space covering by paging */
		page_mmap (task->page_cover, (void *) PAGE_MEM_FB, (void *) PAGE_MEM_HIGH, 1, 0);

		page_dir_switch (task->page_cover->page_dir);

 		paging_enable ();
#endif
		/* add into list */
		task->next = &task_list;
		task->prev = task_list.prev;
		task->prev->next = task;
		task->next->prev = task;

		/* set _curr_task so schedule() will save state of task #0 */
		_curr_task = task;
#ifdef ARCH_arm
		/* NOTE: ARM need to start new fake of main kernel task */
		arch_task_init (task, (unsigned) &task_schedule);
#endif
		return task;
	}

	arch_task_init (task, entry);

	task_id ++;	// increase id number for new task

	// mark it runnable
	task->status = TS_RUNNABLE;
	// task priority - how offten will be active
	task->priority = priority;
	// task attick - how ofter per all time line handle this task (not in %)
	task->attick = task_id - 1;
	// add name of task
	strcpy (task->name, name);
	// spinlock is not available by default
	task->spinlock = 0;
	// page covering of process
	task->page_cover = 0;
	// task identification number - usefully on process level (multi-threaded apps)
	task->id = task_id;
	// task's time stamp of last tick
	task->lasttick = 0;

	/* add into list */
	task->next = &task_list;
	task->prev = task_list.prev;
	task->prev->next = task;
	task->next->prev = task;

	DPRINT (DBG_SCHED, "task -> %s (%d)", name, task_id);

	return task;
}

bool task_done (task_t *task)
{
	if (task) {
		DPRINT (DBG_SCHED, "task -> %s done", task->name);

		task->status = TS_NULL;

		task->next->prev = task->prev;
		task->prev->next = task->next;

		kfree (task);

		return 1;
	}

	return 0;
}

task_t *task_find (unsigned short id)
{
	task_t *task;
	for (task = task_list.next; task != &task_list; task = task->next) {
		if (task->id == id)
			return task;
	}

	return 0;
}

unsigned int init_tasks (void)
{
	task_list.next = &task_list;
	task_list.prev = &task_list;

	/* NOTE: NULL entry point task can't be created multiple times ! */
	task_create ("kernel", NULL, 0);

	return 1;
}
