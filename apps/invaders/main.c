/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define		ESC				1
#define		ARROWLEFT			75
#define		ARROWRIGHT			77
#define		ARROWUP				72
#define		ARROWDOWN			80

typedef struct {
	unsigned char x, y;
	unsigned char znak;
	unsigned char live;
	unsigned char zmena;
} CLOVEK;

CLOVEK mujhrac;

#define PLOCHA_X	80
#define PLOCHA_Y	25

unsigned key_pressed (int keycode)
{
	int scancode = getkey ();

	if (scancode == keycode)
		return 1;

	if (scancode == keycode+128)
		return 2;
	else
		return 0;
}

int main (int argc, char **argv)
{
	cls ();

	printf ("This program is marked as pre-alpha !\n");

	mujhrac.x = 0;
	mujhrac.y = 1;
	mujhrac.znak = '#';
	mujhrac.zmena = 1;

	while (1) {
		int scancode = getkey ();

		if (scancode == ESC)
			break;

		if (scancode == ARROWLEFT && mujhrac.x > 0) {
			gotoxy (mujhrac.x, mujhrac.y);
			putch (' ');
			mujhrac.x --;
			mujhrac.zmena = 1;
		}
		if (scancode == ARROWRIGHT && mujhrac.x < PLOCHA_X-1) {
			gotoxy (mujhrac.x, mujhrac.y);
			putch (' ');
			mujhrac.x ++;
			mujhrac.zmena = 1;
		}
		if (scancode == ARROWUP && mujhrac.y > 0) {
			gotoxy (mujhrac.x, mujhrac.y);
			putch (' ');
			mujhrac.y --;
			mujhrac.zmena = 1;
		}
		if (scancode == ARROWDOWN && mujhrac.y < PLOCHA_Y) {
			gotoxy (mujhrac.x, mujhrac.y);
			putch (' ');
			mujhrac.y ++;
			mujhrac.zmena = 1;
		}

		if (mujhrac.zmena) {
			gotoxy (mujhrac.x, mujhrac.y);
			putch (mujhrac.znak);
			mujhrac.zmena = 0;
		}
	}

	printf ("\nInvaders are coming !\n");

	return 0;
}
