/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "net.h"

#define		ESC	1

int main (int argc, char **argv)
{
	int port = DEFAULT_TELNET_PORT;

	printf ("telnetd\n");

	if (argc > 1)
		port = atoi (argv[1]);

	if (init (port) < 1) {
		printf ("telnetd -> init (0) !\n");
		return 1;
	}


	while (1) {
		if (loop () == -1)
			break;

		schedule ();
	}

	printf ("telnetd -> Bye !\n");

	return 0;
}
