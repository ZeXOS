/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <system.h>
#include <string.h>
#include <arch/io.h>
#include <config.h>
#include <env.h>
#include <dev.h>
#include <vfs.h>
#include <user.h>
#include <tty.h>
#include <proc.h>
#include <partition.h>
#include <net/if.h>
#include <net/socket.h>
#include <net/packet.h>
#include <sound/audio.h>
#include <module.h>
#include <commands.h>
#include <console.h>
#include <paging.h>
#include <errno.h>
#include <smp.h>
#include <build.h>
#include <its.h>
#include <fs.h>
#include <fd.h>

/* Command structure */
typedef unsigned int (init_handler_t) ();

/* OS information stamp */
void osinfo ()
{
	video_color (14, 0);
	kprintf ("ZeX/OS");
	video_color (7, 0);
	kprintf (" v" ZEXOS_MAJOR "." ZEXOS_MINOR "." ZEXOS_REVISION " " ZEXOS_DATE " " ZEXOS_ARCH " " ZEXOS_COMPILER "\n");
}

/* Bootloader command parse system */
void oscmd (char *cmdline)
{
	kernel_attr = 0;

	char cmd[80];

	int x = 0, y, l;

	l = strlen (cmdline);

	if (l >= 80)
		l = 79;

	while (x < l) {
		if (cmdline[x] == ' ') {
			x ++;
			
			if (!cstrcmp ("nolive", cmdline+x))	/* Disable Live form of the OS */
				kernel_attr |= KERNEL_NOLIVE;
			if (!cstrcmp ("noata", cmdline+x))	/* Ignore ATA devices - cdrom, hdd */
				kernel_attr |= KERNEL_NOATA;
			if (!cstrcmp ("18hz", cmdline+x))	/* Ticks on 18hz only - IBM default tickrate */
				kernel_attr |= KERNEL_18HZ;
			if (!cstrcmp ("noeth", cmdline+x))	/* Ignore ethernet cards */
				kernel_attr |= KERNEL_NOETH;
			if (!cstrcmp ("novesa", cmdline+x))	/* Disable vesa support when its available */
				kernel_attr |= KERNEL_NOVESA;
		}

		x ++;
	}
}

/* Eye-candy status */
unsigned int checkinfo (char *msg, unsigned int ret)
{
	if (msg == NULL)
		return 2;

	video_color (14, 0);
	kprintf (" * ");
	video_color (7, 0);
	kprintf (msg);
	video_color (1, 0);
	kprintf ("\t\t[ ");
	video_color (14, 0);
	
	if (ret) {
		video_color (14, 0);
		kprintf ("OK");
	} else {
		video_color (4, 0);
		kprintf ("Fail");
	}

	video_color (1, 0);
	kprintf (" ]\n");

	video_color (15, 0);

	return ret;
}

/* Handler-caller */
unsigned int _init (init_handler_t *handler, char *msg)
{
	return checkinfo (msg, handler ());
}

extern void calc_freqency ();
extern task_t *_curr_task;

/* initialization of kernel */
int init (char *cmdline)
{
	debug = 0;
	_curr_task = 0;

	_init (&init_arch, NULL);			/* architecture dependent stuff */

	oscmd (cmdline);				/* bootloader command parsing */

	_init (&init_video, NULL);			/* video initialization */

	osinfo ();					/* print OS information - kernel ver.; compiler; .. */

	_init (&init_mm, "Initialize mm");		/* Memory management */
	_init (&init_smp, "Initialize SMP");		/* Symmetric multiprocessing */
	_init (&init_keyboard, "Initialize keyboard");	/* Keyboard should be accessible from here */
#ifdef ARCH_i386
	_init (&init_paging, "Initialize paging");	/* Page covering, etc */
#endif
	_init (&init_errno, "Initialize errno");	/* Errno global variable */
	_init (&init_env, "Initialize env vars");	/* Environment variables */
	_init (&init_fd, "Initialize file desc");	/* File descriptors - stdin, stdout, .. */
	_init (&init_tasks, "Initialize tasks");	/* Thread and scheduler stuff */	
	_init (&init_vfs, "Initialize VFS");		/* Virtual filesystem */
	_init (&init_fs, "Initialize fs");		/* Device filesystem */
	_init (&init_partition, "Initialize parts");	/* Device partitions - hda0, hda1, .. */
	_init (&init_its, "Initialize ITS");		/* Interval Time Scheduler */
	_init (&init_netif, "Initialize net if");	/* Network interface */
	_init (&init_dev, "Initialize devices");	/* Devices (PCI, RS232, Ethernet, ..) */
	_init (&init_socket, "Initialize sockets");	/* Network socket API */
	_init (&init_packet, "Initialize netcore");	/* Network handler - traffic parser */
	_init (&init_audio, "Initialize audio");	/* Sound System */
	_init (&init_proc, "Initialize procs");		/* Program's (software) process */
	_init (&init_module, "Loading modules");	/* Kernel additional modules (device drivers) */
	_init (&init_commands, "Registering cmds");	/* Built-in shell command registration */
	_init (&init_console, "Initialize console");	/* Console (terminal) stuff */

	calc_swtimer ();				/* Calculate by software time unit */
	calc_freqency ();				/* Calculate CPU clock */

	_init (&init_tty, NULL);			/* Start TTY console */
	_init (&init_user, NULL);			/* Register default user accounts */

	return 1;
}
 
