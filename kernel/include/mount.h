/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <partition.h>

#define VFS_MOUNTPOINT_LEN	32

/* Mount structure */
typedef struct mount_context {
  struct mount_context *next, *prev;

  partition_t *p;
  char mountpoint[VFS_MOUNTPOINT_LEN];
} mount_t;

extern void mount_display ();
extern partition_t *mount_find (char *mountpoint);
extern bool mount (partition_t *p, char *dir, char *mountpoint);
extern bool umount (partition_t *p, char *mountpoint);
