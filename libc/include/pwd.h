/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _PWD_H
#define _PWD_H

#include <sys/types.h>

struct passwd {
	char *pw_name;	/* user's login name */
	uid_t pw_uid;	/* numerical user ID */
	gid_t pw_gid;	/* numerical group ID */
	char *pw_dir;	/* initial working directory */
	char *pw_shell;	/* program to use as shell */
}

#endif

