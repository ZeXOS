/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>

#include "handler.h"
#include "window.h"
#include "cursor.h"
#include "icon.h"
#include "dialog.h"

extern zde_win_t *zde_win_last;
extern zde_cursor_t *zde_cursor;

typedef struct {
	char *name;
	unsigned attrib;
	unsigned char next;
	unsigned char unused;
} __attribute__ ((__packed__)) dirent_t;

dirent_t *dirent;


static void getdir ()
{
	dirent = 0;

	asm volatile (
		"movl $25, %%eax;"
	     	"int $0x80;"
		"movl %%eax, %0;"
		: "=g" (dirent) :: "%eax");
}

void *handler_icon_reader (void *r)
{
	if (!r)
		return (0);

	char desc[64];
	sprintf (desc, "Reader - %s", r);

	/* create test window */
	zde_win_t *window = create_window (desc, 1);

	if (!window)
		return (0);

	int fd = open (r, O_RDONLY);

	if (!fd)
		return 0;

	int l = lseek (fd, 0, SEEK_END);

	lseek (fd, 0, SEEK_SET);

	char *buf = (char *) malloc (l+1);

	if (!buf)
		return 0;

	read (fd, buf, l);

	zde_dobj_text_t dobj_text;

	dobj_text.data = (char *) buf;
	dobj_text.len = l;
	dobj_text.sizex = 0;
	dobj_text.sizey = 0;

	create_dialog_object (ZDE_DOBJ_TYPE_TEXT, &dobj_text, 0, 0, window);

	while (!(window->state & WINDOW_STATE_CLOSE)) {
		schedule ();

		if (window != zde_win_last)
			continue;

		if (zde_cursor->state == XCURSOR_STATE_LBUTTON) {
			
		}
	}

	free (buf);
	free (window);

	return (0);
}

void *handler_icon_terminal (void *r)
{
	if (!r)
		return (0);

	char desc[64];
	sprintf (desc, "Terminal");

	/* create test window */
	zde_win_t *window = create_window (desc, 1);

	if (!window)
		return (0);
	
	window->sizex = 6 * 80;
	window->sizey = 9 * 65;
	
	int fd = open ("/dev/tty1", O_RDWR);

	if (!fd)
		return 0;
	
	if (fcntl (fd, F_SETFL, 0x2000) == -1) {
		printf ("Cant set fd to terminal mode\n");
		return 0;
	}

	int l = lseek (fd, 0, SEEK_END);

	lseek (fd, 0, SEEK_SET);

	char *buf = (char *) malloc (l+1);

	if (!buf)
		return 0;
	
	char *buf2 = (char *) malloc (l+1);

	if (!buf2) {
		free (buf);
		return 0;
	}
		
	zde_dobj_text_t dobj_text;

	dobj_text.sizex = 0;
	dobj_text.sizey = 0;
	dobj_text.data = (char *) "tty";
	dobj_text.len = 3;

	zde_dobj_t *dobj = create_dialog_object (ZDE_DOBJ_TYPE_TEXT, &dobj_text, 0, 0, window);

	if (!dobj)
		return 0;
	
	zde_dobj_text_t *text = (zde_dobj_text_t *) dobj->arg;
	
	if (!text)
		return 0;
	
	while (!(window->state & WINDOW_STATE_CLOSE)) {
		schedule ();
		
		if (window != zde_win_last)
			continue;
		
		read (fd, buf, l);
		
		lseek (fd, 0, SEEK_SET);
		
		unsigned i = 0;
		unsigned j = 0;
		unsigned k = 0;
		unsigned m = 80 * 65;
		unsigned pos = 0;
			
		while (i < m) {
			if (buf[i*2])
				buf2[pos ++] = buf[i*2];
			else
				buf2[pos ++] = ' ';
			
			j ++;

			if (j >= 80) {
				j = 0;
				k ++;
				buf2[pos ++] = '\n';
			}
			
			i ++;
		}
		
		text->data = (char *) buf2;
		text->len = pos;
		
		char c[2];
		c[0] = kbd_getkey ();
		
		if (c[0])
			write (fd, c, 1);

		if (zde_cursor->state == XCURSOR_STATE_LBUTTON) {
			
		}
	}

	free (buf2);
	free (buf);
	free (window);

	return (0);
}

void *handler_icon_exec (void *r)
{
	if (!r)
		return (0);

	char desc[64];
	sprintf (desc, "ZDE app - %s", r);

	/* create test window */
	zde_win_t *window = create_window (desc, 1);

	if (!window)
		return (0);

	zde_dobj_appcl_t dobj_appcl;

	dobj_appcl.x = window->x;
	dobj_appcl.y = window->y;
	dobj_appcl.sizex = window->sizex;
	dobj_appcl.sizey = window->sizey;
	dobj_appcl.kbd = 0;
	dobj_appcl.fd = -1;

	create_dialog_object (ZDE_DOBJ_TYPE_APPCL, &dobj_appcl, 0, 0, window);

	unsigned l = strlen (r);
	unsigned i;

	char *str = (char *) strdup (r);

	for (i = l; i > 1; i --) {
		if (str[i] == '/') {
			str[i] = '\0';
			break;
		}
	}

	chdir ((char *) "/");

	chdir ((char *) str);

	sprintf (desc, "exec %s", str+i+1);

	free (str);

	system (desc);

	schedule ();

	while (!(window->state & WINDOW_STATE_CLOSE)) {
		schedule ();

		if (window != zde_win_last)
			continue;

		if (zde_cursor->state == XCURSOR_STATE_LBUTTON) {
			
		}
	}

	free (window);

	return (0);
}

void *handler_icon_filesystem (void *r)
{
	if (!r)
		return (0);

	char desc[64];
	sprintf (desc, "Filesystem - %s", r);

	/* create test window */
	zde_win_t *window = create_window (desc, 0);

	if (!window)
		return (0);

	chdir ((char *) "/");

	if (strlen (r) > 0)
		chdir ((char *) r);

	getdir ();

	dirent_t *dir = dirent;

	if (!dir)
		return (0);

	unsigned id = 0;

	for (;;) {
		if (dir[id].attrib & VFS_FILEATTR_DIR)
			create_icon_window (dir[id].name, 0, handler_icon_filesystem, (void *) r, window);
		if (dir[id].attrib & VFS_FILEATTR_FILE) {
			if (dir[id].attrib & VFS_FILEATTR_BIN || strstr (dir[id].name, ".x"))
				create_icon_window (dir[id].name, 1, handler_icon_exec, (void *) r, window);
			else
				create_icon_window (dir[id].name, 1, handler_icon_reader, (void *) r, window);
		}

		if (!dir[id].next)
			break;

		id ++;
	}

	free (dir);

	while (!(window->state & WINDOW_STATE_CLOSE)) {
		schedule ();

		if (window != zde_win_last)
			continue;

		if (zde_cursor->state == XCURSOR_STATE_LBUTTON) {
			
		}
	}

	free (window);

	return (0);
}
