/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h>


void *malloc (size_t size)
{
	void *p = 0;

	asm volatile (
		"movl $14, %%eax;"
	     	"movl %1, %%ebx;"
	     	"int $0x80;"
		"movl %%eax, %0;": "=g" (p) : "g" ((unsigned) size) : "%eax", "%ebx");

	return (void *) p;
}

void *calloc (size_t nmemb, size_t size)
{
	void *p = malloc (nmemb * size);

	if (p)
		memset (p, 0, nmemb * size);

	return p;
}

void free (void *blk)
{
	asm volatile (
		"movl $43, %%eax;"
	     	"movl %0, %%ebx;"
	     	"int $0x80;" :: "b" (blk) : "%eax", "memory");
}

void *realloc (void *blk, size_t size)
{
	void *p = 0;

	asm volatile (
		"movl $44, %%eax;"
	     	"movl %1, %%ebx;"
		"movl %2, %%ecx;"
	     	"int $0x80;"
		"movl %%eax, %0;": "=g" (p) : "b" (blk), "g" ((unsigned) size) : "%eax", "memory", "%ecx");

	return (void *) p;
}
