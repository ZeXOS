/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "http.h"
#include "client.h"
#include "net.h"

#define SUPPORT_IPV6	1

struct sockaddr *sockName;    // "Jmeno" portu
struct sockaddr *clientInfo;  // Klient, ktere se pripojil 

int mainSocket;               // Soket
socklen_t addrlen;            // Velikost adresy vzdaleneho pocítace
char *buffer;		      // Buffer
int client_count;	      // Pocet pripojenych clientu
int proto;		      // Protocol

client_t client_list;


int send_to_socket (int fd, char *data, unsigned len)
{
	int ret;
	if ((ret = send (fd, data, len, 0)) == -1) {
		printf ("ERROR -> send () = -1\n");
		return -1;
	}

	return ret;
}

int client_get (client_t *c)
{
	if (c->ret > 32566) {
		c->state = CLIENT_STATE_TIMEOUT;
		return 1;
	}

	memset (buffer, 0, 1024);
	int ret = recv (c->fd, buffer, 1024, 0);

	if (ret < 1) {
		c->ret ++;
		return ret;
	}

	buffer[ret] = '\0';
	
//	printf ("DATA: %s\n", buffer);
	
	if (!strncmp (buffer, "GET /", 5)) {

		int i;
		for (i = 4; i < ret; i ++)
			if (buffer[i] == ' ')
				break;

		if ((i-5) >= 32)
			i = 36;
			
		memcpy (c->page, buffer+5, i-5);
		c->page[i-5] = '\0';

		c->state = CLIENT_STATE_TRANSFER;

		return 1;
	} else
		printf ("ERROR -> corrupted http session ..\n");

	c->state = CLIENT_STATE_DONE;

	return ret;
}

client_t *client_connected (int fd)
{
	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next)
		if (c->fd == fd)
			return 0;

	// alloc and init context
	client_t *ctx = (client_t *) malloc (sizeof (client_t));

	if (!ctx)
		return 0;

	ctx->fd = fd;
	ctx->state = CLIENT_STATE_CONNECTED;
	ctx->ret = 0;

	ctx->next = &client_list;
	ctx->prev = client_list.prev;
	ctx->prev->next = ctx;
	ctx->next->prev = ctx;

	return ctx;
}

int client_handle (client_t *c)
{
	switch (c->state) {
		case CLIENT_STATE_CONNECTED:
			return client_get (c);
		case CLIENT_STATE_TRANSFER:
			return http_transfer (c);
		case CLIENT_STATE_DONE:
		case CLIENT_STATE_TIMEOUT:
			/* disconnect socket */
			close (c->fd);

			/* delete client_t * struct from list */
			c->next->prev = c->prev;
			c->prev->next = c->next;

			free (c);
			return 0;
	}

	return 0;
}

int init (int port, int flags)
{
	proto = AF_INET;

	/* switch to IPv6 */
	if (flags & FLAG_IPV6) {
		 proto = AF_INET6;
		 printf ("> initializing IPv6\n");
	} else
		 printf ("> initializing IPv4\n");

	if ((mainSocket = socket (proto, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		printf ("ERROR -> socket ()\n");
		return -1;
	}

	if (proto == AF_INET6) {
		struct sockaddr_in6 *sockName6 = (struct sockaddr_in6 *) malloc (sizeof (struct sockaddr_in6));
		clientInfo = (struct sockaddr *) malloc (sizeof (struct sockaddr_in6));

		if (!sockName6 || !clientInfo)
			return 0;

		sockName6->sin6_family = proto;
		sockName6->sin6_port = htons (port);
		//sockName6->sin6_addr.s6_addr = INADDR6_ANY;

		sockName = (struct sockaddr *) sockName6;

		addrlen = sizeof (struct sockaddr_in6);
	} else {
		struct sockaddr_in *sockName4 = (struct sockaddr_in *) malloc (sizeof (struct sockaddr_in));
		clientInfo = (struct sockaddr *) malloc (sizeof (struct sockaddr_in));

		if (!sockName4 || !clientInfo)
			return 0;

		sockName4->sin_family = proto;
		sockName4->sin_port = htons (port);
		sockName4->sin_addr.s_addr = INADDR_ANY;

		sockName = (struct sockaddr *) sockName4;

		addrlen = sizeof (struct sockaddr_in);
	}

	if ((bind (mainSocket, (struct sockaddr *) sockName, addrlen)) == -1) {
		printf ("ERROR -> bind () - %d\n", port);
		return -1;
	}

	/* max 10 connecting clients */
	if (listen (mainSocket, 10) == -1) {
		printf ("ERROR -> listen ()\n");
		return -1;
	}

	// Let's switch socket mode to non-blocking
	int oldFlag = fcntl (mainSocket, F_GETFL, 0);
	if (fcntl (mainSocket, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
		printf ("ERROR -> fcntl ()\n");
		return 0;
	}

	printf ("> listen on port: %d\n", port);

	buffer = (char *) malloc (sizeof (char) * 1025);

	if (!buffer) {
		printf ("Buffer is out of memory\n");
		return 0;
	}

	client_list.next = &client_list;
	client_list.prev = &client_list;

	client_count = 0;

	return http_init ();
}

int loop ()
{
	int client = 0;
	
	client = accept (mainSocket, (struct sockaddr *) clientInfo, &addrlen);

	if (client > 0) {
		char str[40];

		if (proto == AF_INET) {
			struct sockaddr_in *clientInfoS = (struct sockaddr_in *) clientInfo;
			inet_ntop (proto, (void *) &clientInfoS->sin_addr, str, 40);

			printf ("> New client (%d) -- %s:%d connected !\n", client, str, clientInfoS->sin_port);
		} else if (proto == AF_INET6) {
			struct sockaddr_in6 *clientInfoS = (struct sockaddr_in6 *) clientInfo;
			inet_ntop (proto, (void *) &clientInfoS->sin6_addr, str, 40);

			printf ("> New client (%d) -- [%s]:%d connected !\n", client, str, clientInfoS->sin6_port);
		}

		// Nastavime soket do neblokovaciho rezimu
		int oldFlag = fcntl (client, F_GETFL, 0);
		if (fcntl (client, F_SETFL, oldFlag | O_NONBLOCK) == -1) {
			printf ("Cant set socket to nonblocking mode\n");
			return 0;
		}

		client_t *c = client_connected (client);

		if (!c)
			close (client);
	}

	unsigned i = 0;

	client_t *c;
	for (c = client_list.next; c != &client_list; c = c->next) {
		if (!client_handle (c))
			break;
		
		i ++;
	}

	if (!i)
		usleep (64);

	return 1;
} 
