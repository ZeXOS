/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include "buffer.h"

static char *buffer;

int buffer_copy (unsigned offset, char *data, unsigned size)
{
	memcpy (buffer+offset, data, size);

	return 1;
}

char *buffer_get ()
{
	return buffer;
}

int buffer_write (char *file)
{
	int fd = open (file, O_CREAT | O_WRONLY);

	if (fd < 1) {
		printf ("error -> file '%s' not found !\n", file);
		return 0;
	}

	if (!write (fd, buffer, 10240-1)) {
		printf ("error -> something is wrong !\n");
		return 0;
	}

	close (fd);

	return 1;
}

int buffer_init ()
{
	buffer = (char *) malloc (sizeof (char) * 10240);

	if (!buffer)
		return 0;

	elf_file_t elf;

	memset (&elf, 0, sizeof (elf_file_t));

	elf.magic = 0x464C457FL;
	elf.bitness = 1;
	elf.endian = 1;
	elf.elf_ver_1 = 1;
	elf.file_type = 2;
	elf.machine = 3;
	elf.elf_ver_2 = 1;
	elf.entry_pt = 0x0;
	elf.phtab_offset = 0x34;
	elf.shtab_offset = 0x204c;
	elf.flags = 0x0;
	elf.file_hdr_size = 0x34;
	elf.phtab_ent_size = 0x20;
	elf.num_phtab_ents = 0x2;
	elf.shtab_ent_size = 0x28;
	elf.num_sects = 0x7;
	elf.shstrtab_index = 0x6;

	elf_sect_t sect;

	memset (&sect, 0, sizeof (elf_sect_t));

	sect.sect_name = 0x1;
	sect.type = 0x1000; 
	sect.flags = 0;
	sect.virt_adr = 0x0;
	sect.offset = 0x1000;
	sect.size = 0x1000;
	sect.link = 0x5;
	sect.info = 0x1000;
	sect.align = 0x65041580;
	sect.ent_size = 0x0;

	buffer_copy (0, (void *) &elf, sizeof (elf_file_t));
	buffer_copy (sizeof (elf_file_t), (void *) &sect, sizeof (elf_sect_t));


	/* sections */
	unsigned s = elf.shtab_ent_size; // section size

	/* s = 0 */
	memset (&sect, 0, sizeof (elf_sect_t));
	buffer_copy (elf.shtab_offset, (void *) &sect, sizeof (elf_sect_t));

	/* s = 1 - .text */
	sect.sect_name = 0x0b;
	sect.type = 0x1; 
	sect.flags = 0x6;
	sect.virt_adr = 0x801000;
	sect.offset = 0x1000;
	sect.size = 0x1000;
	sect.link = 0x0;
	sect.info = 0x0;
	sect.align = 0x10;
	sect.ent_size = 0x0;
	buffer_copy (elf.shtab_offset+s, (void *) &sect, sizeof (elf_sect_t));

	/* s = 2 - .data */
	sect.sect_name = 0x11;
	sect.type = 0x1; 
	sect.flags = 0x1;
	sect.virt_adr = 0x802000;
	sect.offset = 0x1000;
	sect.size = 0x0;
	sect.link = 0x0;
	sect.info = 0x0;
	sect.align = 0x1;
	sect.ent_size = 0x0;
	buffer_copy (elf.shtab_offset+s*2, (void *) &sect, sizeof (elf_sect_t));

	/* s = 3 - .rodata */
	sect.sect_name = 0x17;
	sect.type = 0x1; 
	sect.flags = 0x1;
	sect.virt_adr = 0x802000;
	sect.offset = 0x2000;
	sect.size = 0x0;
	sect.link = 0x0;
	sect.info = 0x0;
	sect.align = 0x1;
	sect.ent_size = 0x0;
	buffer_copy (elf.shtab_offset+s*3, (void *) &sect, sizeof (elf_sect_t));

	/* s = 4 - .bss */
	sect.sect_name = 0x1f;
	sect.type = 0x1; 
	sect.flags = 0x32;
	sect.virt_adr = 0x802c00;
	sect.offset = 0x500;
	sect.size = 0x0;
	sect.link = 0x0;
	sect.info = 0x0;
	sect.align = 0x1;
	sect.ent_size = 0x1;
	buffer_copy (elf.shtab_offset+s*4, (void *) &sect, sizeof (elf_sect_t));

	/* s = 5 - .comment */
	sect.sect_name = 0x24;
	sect.type = 0x1; 
	sect.flags = 0x0;
	sect.virt_adr = 0x803000;
	sect.offset = 0x2000;
	sect.size = 0x1c;
	sect.link = 0x0;
	sect.info = 0x0;
	sect.align = 0x1;
	sect.ent_size = 0x0;
	buffer_copy (elf.shtab_offset+s*5, (void *) &sect, sizeof (elf_sect_t));

	/* s = 6 - .shstrtab */
	sect.sect_name = 0x1;
	sect.type = 0x3; 
	sect.flags = 0x0;
	sect.virt_adr = 0x0;
	sect.offset = 0x201c0000;
	sect.size = 0x2d;
	sect.link = 0x0;
	sect.info = 0x0;
	sect.align = 0x1;
	sect.ent_size = 0x0;
	buffer_copy (elf.shtab_offset+s*6, (void *) &sect, sizeof (elf_sect_t));

	return 1;
}
