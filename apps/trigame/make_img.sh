#!/bin/bash
# Folder name of your application
APPNAME="trigame"

# Compile source
make

# Optimized for 1,4MB floppy
dd if=/dev/zero of=$APPNAME.img bs=1440k count=1
mkfs.vfat $APPNAME.img

mkdir floppy
mount -oloop $APPNAME.img floppy
cp $APPNAME floppy
umount floppy
rmdir floppy