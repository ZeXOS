/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include "button.h"
#include "cursor.h"

wmbutton wmbutton_list;

extern wmcursor *cursor;

wmbutton *button_create (unsigned x, unsigned y, unsigned size_x, unsigned size_y, unsigned char flags, const char *caption)
{
	wmbutton *button = (wmbutton *) malloc (sizeof (wmbutton));

	if (!button)
		return 0;

	button->x = x;
	button->y = y;
	button->flags = flags;

	unsigned l = strlen (caption);
	if (l) {
		button->caption = (char *) malloc (sizeof (char) * l + 1);
		memcpy (button->caption, caption, l);
		button->caption[l] = '\0';
	} else
		button->caption = 0;

	if (size_x < (l*6+4))
		button->size_x = l*6+4;
	else
		button->size_x = size_x;

	if (size_y < 12)
		button->size_y = 12;
	else
		button->size_y = size_y;

	/* add into list */
	button->next = &wmbutton_list;
	button->prev = wmbutton_list.prev;
	button->prev->next = button;
	button->next->prev = button;

	return button;
}

unsigned button_sethandler (wmbutton *button, wmbutton_handler *handler)
{
	if (!button)
		return 0;

	if (!handler)
		button->handler = 0;
	else {
		button->handler = handler;
		return 1;
	}

	return 0;
}

unsigned char button_flags (wmbutton *button)
{
	if (!button)
		return 0;

	return button->flags;
}

unsigned button_draw (wmbutton *button)
{
	if (button->flags & BUTTON_FLAG_CLICKED) {
		xrect (button->x, button->y, button->x+button->size_x, button->y+button->size_y, 35*256);
		xrect (button->x+1, button->y+1, button->x+button->size_x-1, button->y+button->size_y-1, 0*256);
		xrectfill (button->x+2, button->y+2, button->x+button->size_x-2, button->y+button->size_y-2, 0x0084FF);
	
		if (button->caption)
			xtext_puts (button->x+3, button->y+3, 1*256, button->caption);

		/* FIXME: general protection fault ! Something with pointer, fault is on OS side, 'cause syntax is correctly, and works on other systems .. */
		/*if (button->handler)
			button->handler ();*/	// call handler
	} else {
		xrect (button->x, button->y, button->x+button->size_x, button->y+button->size_y, 35*256);
		xrect (button->x+1, button->y+1, button->x+button->size_x-1, button->y+button->size_y-1, 3*256);
		xrectfill (button->x+2, button->y+2, button->x+button->size_x-2, button->y+button->size_y-2, 0x0075E2);
	
		if (button->caption)
			xtext_puts (button->x+2, button->y+2, 0, button->caption);
	}

	return 1;
}

unsigned button_draw_all ()
{
	wmbutton *button;
	for (button = wmbutton_list.next; button != &wmbutton_list; button = button->next) {
		if (!button)
			continue;

		if (cursor->action)
		if (cursor->state != XCURSOR_STATE_LBUTTON) {
			if (button->flags & BUTTON_FLAG_CLICKED) {
				button->flags &= ~BUTTON_FLAG_CLICKED;
				cursor->action = 0;
			}
		}

		/* mame mys na tlacitku ? */
		if (cursor->x > (signed) button->x && cursor->x < (signed) button->x+(signed) button->size_x && 
			cursor->y > (signed) button->y && cursor->y <= (signed) button->y+(signed) button->size_y) {

			if (!cursor->action)
 			if (cursor->state == XCURSOR_STATE_LBUTTON) {
				if (!(button->flags & BUTTON_FLAG_CLICKED)) {
					button->flags |= BUTTON_FLAG_CLICKED;
					cursor->action = 1;
				}
			}
		}

		button_draw (button);
	}

	return 1;
}

unsigned init_button ()
{
	wmbutton_list.next = &wmbutton_list;
	wmbutton_list.prev = &wmbutton_list;

	return 1;
}
