/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h>

char *strchr (const char *s, int c)
{
	for (; *s != (char) c; ++ s)
		if (*s == '\0')
			return (char *) 0;

	return (char *) s;
}

char *strrchr (const char *s, int c)
{
	char *r = (char *) 0;
  
	unsigned i;
	for (i = 0; s[i]; i ++)
		if (s[i] == c)
			r = (char *) s;
		
	return (char *) r;
}