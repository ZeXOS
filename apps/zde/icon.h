/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _ICON_H
#define _ICON_H

#include "window.h"

#define VFS_FILEATTR_FILE	0x1
#define VFS_FILEATTR_DIR	0x2
#define VFS_FILEATTR_HIDDEN	0x4
#define VFS_FILEATTR_SYSTEM	0x8
#define VFS_FILEATTR_BIN	0x10
#define VFS_FILEATTR_READ	0x20
#define VFS_FILEATTR_WRITE	0x40
#define VFS_FILEATTR_MOUNTED	0x80
#define VFS_FILEATTR_DEVICE	0x100

typedef struct zde_icon_context {
	struct zde_icon_context *next, *prev;

	unsigned short x;
	unsigned short y;
	unsigned short *bitmap;
	unsigned short name_len;
	char *name;
	void *(*entry) (void *);
	void *arg;
	void *object;
	char type;
} zde_icon_t;

extern zde_icon_t *create_icon (char *name, int type, void *(*entry) (void *), void *arg);
extern zde_icon_t *create_icon_window (char *name, int type, void *(*entry) (void *), void *arg, zde_win_t *window);
extern unsigned destroy_icon_window (zde_win_t *window);
extern int draw_icon_desktop ();
extern int draw_icon_window (zde_win_t *window);
extern int init_icon ();

#endif
