#!/bin/bash

echo "-= Instant Messenger =-"

cd src

if [ "$1" = "clean" ] ; then
	rm -rf *.o platform/zexos/*.o im ../bin/im ../bin/im.img

	echo "All object and binary files ware deleted"

	exit
fi

echo "Compilation process starting, please wait ..."

if [ "$1" = "linux" ] ; then
make -f makefile-linux && OK=1
else
make -f makefile-zexos && OK=1
fi

cd ..

if [ $OK ]; then
	cp src/im bin/
	
	echo
	echo "! Congratulations ! -- IM is compiled and prepared to run"
	echo "Please start im client over: bin/im"
else
	echo "ERROR - Compilation fault ! Please contact authors or try update your sources"
fi