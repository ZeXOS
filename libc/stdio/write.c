/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <_libc.h>

int write (unsigned fd, void *buf, unsigned len)
{
	asm volatile (
		"movl $11, %%eax;"
	     	"movl %0, %%ebx;"
		"movl %1, %%ecx;"
		"movl %2, %%edx;"
	     	"int $0x80;" :: "b" (buf), "g" (len), "g" (fd) : "%eax", "%ecx", "%edx", "memory");

	/* get return value */
	int *ret = (int *) SYSV_WRITE;

	errno_update ();

	return (int) *ret;
}
 
