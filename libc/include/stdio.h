/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _STDIO_H
#define _STDIO_H

#include <sys/types.h>
#include <stdarg.h>

typedef struct {
	unsigned long off;
	unsigned long e;
	unsigned mode;
	unsigned fd;
	unsigned char error;
} FILE;

FILE *stdin;
FILE *stdout;
FILE *stderr;

extern int errno;

extern void putch (char c);
extern void puts (const char *text);
extern void printf (const char *fmt, ...);
extern void cls ();
extern unsigned char getch ();
extern unsigned char getkey ();
extern void gotoxy (int x, int y);
extern void setcolor (int t, int f);
extern int sprintf (char *buffer, const char *fmt, ...);
extern void beep (unsigned freq);
extern int open (const char *pathname, int flags);
extern int close (int fd);
extern int read (unsigned fd, void *buf, unsigned len);
extern int write (unsigned fd, void *buf, unsigned len);
extern int scanf (const char *fmt, ...);
extern int getchar (void);
extern int putchar (int c);
extern FILE *fopen (const char *filename, const char *mode);
extern int fclose (FILE *stream);
extern size_t fread (void *ptr, size_t size, size_t nmemb, FILE *stream);
extern size_t fwrite (const void *ptr, size_t size, size_t nmemb, FILE *stream);
extern int fgetc (FILE *stream);
extern char *fgets (char *s, int size, FILE *stream);
extern int feof (FILE *stream);
extern int fileno (FILE *stream);
extern int fseek (FILE *stream, long offset, int whence);
extern long ftell (FILE *stream);
extern int fprintf (FILE *stream, const char *format, ...);
extern int fputc (int c, FILE *stream);
extern FILE *fdopen (int fd, const char *mode);
extern int ferror (FILE *stream);
extern int fflush (FILE *stream);
extern int clearerr (FILE *stream);
extern void perror (const char *s);
extern int putc (int c, FILE *stream);
extern int vscanf (const char *fmt, va_list args);
extern int vsscanf (const char *str, const char *fmt, va_list args);
extern int sscanf (const char *str, const char *fmt, ...);
extern int fputs (const char *s, FILE *stream);
extern int vsprintf (char *buffer, const char *fmt, va_list args);
extern int vsnprintf (char *buffer, size_t size, const char *fmt, va_list args);
extern int fseeko (FILE *stream, off_t offset, int whence);
extern off_t ftello (FILE *stream);
extern int remove (const char *pathname);
extern int rename (const char *oldpath, const char *newpath);

#endif
