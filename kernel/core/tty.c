/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <build.h>
#include <system.h>
#include <string.h>
#include <vfs.h>

#ifdef ARCH_i386			/* i386 specific things */
extern int gets (void);
extern unsigned short *textmemptr;
#endif
extern int attrib;
extern int csr_x, csr_y;
extern unsigned char vgagui;

static char con_screen[TTY_CON_BUF];	/* console buffer for image of screen */
static unsigned short tty_count;	/* number of created ttys */
tty_font_t tty_font;			/* global font settings */
tty_char_t tty_char;			/* global character array settings */

tty_t tty_list;				/* list for all created ttys */
tty_t *tty0, *tty1;			/* structures for default ttys (0, 1) */

static unsigned task_tty ();

/* setup of font */
bool tty_font_set (char *c, unsigned char x, unsigned char y)
{
	if (!c || !x || !y)
		return 0;

	tty_font.c = c;
	tty_font.x = x;
	tty_font.y = y;

	return 1;
}

/* setup of character array */
bool tty_char_set (unsigned short x, unsigned short y)
{
	if ((x * y * 2) > TTY_CON_BUF)
		return 0;

	tty_char.x = x;
	tty_char.y = y;

	return 1;
}

/* clear selected screen */
bool tty_cls (tty_t *tty)
{
	if (!tty)
		return 0;

	if (!tty->screen)
		return 0;

	memset (tty->screen, 0, tty_char.x * tty_char.y * 2);

	tty->screen_x = 0;
	tty->screen_y = 0;

	if (tty != currtty)
		return 1;

#ifdef ARCH_i386
	if (vgagui) {
		if (vgagui == 2)
			video_gfx_cls (0);

		return 1;
	}

	csr_x = 0;
	csr_y = 0;

	video_scroll ();
	video_move_csr ();
#endif
	video_cls ();

	return 1;
}

/* lock manipulation with selected tty */
bool tty_lock (tty_t *tty)
{
	if (!tty)
		return 0;

	tty->active = false;
	
	return 1;
}

/* unlock manipulation with selected tty */
bool tty_unlock (tty_t *tty)
{
	if (!tty)
		return 0;

	tty->active = true;
	
	return 1;
}

/* startup function for tty init */
bool tty_startup ()
{
	tty_t *tty = tty0;

	if (tty == NULL)
		return 0;

	currtty = tty;

	strcpy (currtty->pwd, (char *) env_get ("PWD"));

	currtty = tty;

	env_set ("PWD", tty->pwd);

	/* always default colors */
	attrib = 0x07;

	/* clear screen*/
	video_cls ();

	return 1;
}

/* change current tty to selected one */
bool tty_change (tty_t *tty)
{
#ifndef ARCH_i386
	return 0;
#endif
	if (tty == NULL)
		return 0;

#ifdef ARCH_i386
	/* redraw screen with selected tty */
	memcpy (textmemptr, tty->screen, tty_char.x * tty_char.y * 2);

	csr_x = tty->screen_x;
	csr_y = tty->screen_y;

	video_move_csr ();
#endif
	strcpy (currtty->pwd, (char *) env_get ("PWD"));
	
	if (currtty->user && currtty->user->name)
		strcpy (currtty->user->name, (char *) env_get ("USER"));

	currtty = tty;

	env_set ("PWD", tty->pwd);

	if (tty->user)
		env_set ("USER", tty->user->name);

	return 1;
}

/* move cursor position to selected place */
bool tty_gotoxy (tty_t *tty, unsigned char x, unsigned char y)
{
	if (tty == NULL)
		return 0;

	if (x >= tty_char.x || y >= tty_char.y)
		return 0;

	video_gotoxy (x, y);

	tty->screen_x = x;
	tty->screen_y = y;

	return 1;
}

/* find wanted tty by name */
tty_t *tty_find (char *name)
{
	tty_t *tty;

	for (tty = tty_list.next; tty != &tty_list; tty = tty->next) {
		if (!strcmp (tty->name, name))
			return tty;
	}

	return NULL;
}

/* find tty by task */
tty_t *tty_findbytask (task_t *task)
{
	tty_t *tty;

	for (tty = tty_list.next; tty != &tty_list; tty = tty->next) {
		if (tty->task == task)
			return tty;
	}

	return NULL;
}

/* display list of created tty consoles */
void tty_listview ()
{
	unsigned short id = 1;

	tty_t *tty;

	for (tty = tty_list.next; tty != &tty_list; tty = tty->next) {
		printf ("%d.\t%s\t%s\n", id, tty->name, currtty == tty ? "<- current" : "");

		id ++;
	}
}

/* enable logging for TTY */
int tty_log_create (tty_t *tty, unsigned short len)
{
	if (!tty || !len)
		return 0;
	
	tty->log = (tty_log_t *) kmalloc (sizeof (tty_log_t));
	
	if (!tty->log)
		return 0;
	
	tty->log->buf = (char *) kmalloc (sizeof (char) * len);
	
	if (!tty->log->buf) {
		kfree (tty->log);
		return 0;
	}
	
	tty->log->len = len;
	tty->log->pos = 0;
	
	return 1;
}

/* main function for print strings into selected tty */
bool tty_write (tty_t *tty, char *str, unsigned len)
{
	if (!tty)
		return 0;

	unsigned l = 0;
	while (l != len) {
		if (!tty_putnch (tty, str[l]))
			break;

		l ++;
	}

	return 1;
}

/* read strings from selected tty */
int tty_read (tty_t *tty, char *str, unsigned len)
{
	if (!tty || !str)
		return 0;

	/* create log buffer for current tty when not exist */
	if (!tty->log)
		if (!tty_log_create (tty, 512))
			return 0;

	unsigned l = len;
	
	if (l > tty->log->pos)
		l = tty->log->pos;
	
	memcpy (str, tty->log->buf, l);
	
	tty->log->pos -= l;

	return l;
}

/* print character to current tty */
bool tty_putch (char c)
{
	tty_putnch (currtty, c);

	return 1;
}

/* print character to selected tty */
bool tty_putnch (tty_t *tty, char c)
{
	/* we wont null character */
	if (!c)
		return 0;
	
	switch (c) {
		case '\n':
			tty->screen_y ++;
			tty->screen_x = 0;
			break;
		case '\b':
			if (tty->screen_x)
				tty->screen_x --;

			/* save font attributes (colors) */
			tty->screen[(tty->screen_x+(tty->screen_y*tty_char.x))*2] = ' ';
			/* save character into buffer */
			tty->screen[((tty->screen_x+(tty->screen_y*tty_char.x))*2)+1] = attrib;

			break;
		case '\t':
			tty->screen_x = (tty->screen_x + 8) & ~7;
			break;
		default:
			/* save font attributes (colors) */
			tty->screen[(tty->screen_x+(tty->screen_y*tty_char.x))*2] = c;
			/* save character into buffer */
			tty->screen[((tty->screen_x+(tty->screen_y*tty_char.x))*2)+1] = attrib;
			tty->screen_x ++;
			break;
	}

	if (tty->screen_x >= tty_char.x) {
		tty->screen_x -= tty_char.x;
		tty->screen_y ++;
	}

	if (tty->screen_y >= tty_char.y) {
		unsigned i;
		/* save old screen, but with one line offset */
		for (i = 0; i < (tty_char.x * tty_char.y * 2) - (tty_char.x * 2); i ++)
			con_screen[i] = tty->screen[i+(tty_char.x * 2)];
		/* copy new image into tty screen */
		for (i = 0; i < (tty_char.x * tty_char.y * 2); i ++)
			tty->screen[i] = con_screen[i];

		tty->screen_y = tty_char.y - 1;
	}
	
	if (tty == currtty) {
		if (!vgagui)
			video_putch (c);
	}
	
	if (tty->log) {
		if (tty->log->pos+1 == tty->log->len) {
			memcpy (tty->log->buf, tty->log->buf+1, tty->log->len-1);
			tty->log->buf[tty->log->pos] = c;
		} else
			tty->log->buf[tty->log->pos ++] = c;
	}
	
	return 1;
}

/* create Message Of The Day in /etc/motd */
bool tty_motd ()
{
	vfs_list_add ("motd", VFS_FILEATTR_FILE | VFS_FILEATTR_READ | VFS_FILEATTR_SYSTEM, "/etc/");

	char *motd =	"\t\t\t\tWelcome !\n"
			"This is ZeX/OS - operating system created by Tomas 'ZeXx86' Jedrzejek\n"
			"Please visit web page www.zexos.org for more information ..\n"
			"Latest source code is available on git repository: git://repo.or.cz/ZeXOS.git\n"
			"You can display list of the built-in commands by \"help\".\n"
			"Default login name is \"root\" secured by password \"root\".\n";

	vfs_content_t content;
	content.ptr = motd;
	content.len = strlen (motd);
			
	vfs_mmap ("/etc/motd", 9, &content);

	video_color (15, 0);

	vfs_cat ("/etc/motd", 9);

	video_color (7, 0);

	return 1;
}

/* virtual terminal (device handler) */
bool vterm_acthandler (unsigned act, char *block, unsigned block_len)
{
	switch (act) {
		case DEV_ACT_INIT:
		{	
			dev_flags_t *flags = (dev_flags_t *) block;

			if (!flags)
				return 0;

			if (block_len != sizeof (dev_flags_t))
				return 0;

			tty_t *tty = tty_find ((char *) flags->iomem);
			
			if (!tty)
				return 0;
			
			flags->iomem = (void *) tty->screen;
			flags->iolen = tty_char.x * tty_char.y * 2;

			return 1;
		}
		break;
	}

	return 0;
}

/* create new tty console */
tty_t *tty_create ()
{
	/* Software limit - max. 1K of ttys */
	if (tty_count > TTY_COUNT_MAX)
		return 0;

	char name[8];
	sprintf (name, "tty%d", tty_count);

	unsigned name_len = strlen (name);
	
	/* alloc and init context */
	tty_t *tty = (tty_t *) kmalloc (sizeof (tty_t));

	if (!tty)
		return 0;

	memset (tty, 0, sizeof (tty_t));

	tty->screen = (char *) kmalloc (tty_char.x * tty_char.y * 2);

	if (!tty->screen) {
		kfree (tty);
		return 0;
	}

	tty->name = (char *) kmalloc (sizeof (char) * name_len + 1);

	if (!tty->name) {
		kfree (tty->screen);
		kfree (tty);
		return 0;
	}

	memcpy (tty->name, name, name_len);
	tty->name[name_len] = '\0';

	memset (tty->screen, 0, tty_char.x * tty_char.y * 2);

	tty->screen_x = 0;
	tty->screen_y = 0;

	tty->logged = false;

	tty->active = true;

	tty->log = 0;

	strcpy (tty->pwd, (char *) env_get ("PWD"));

	tty->task = (task_t *) task_create (name, (unsigned) task_tty, 32);

	/* add into list */
	tty->next = &tty_list;
	tty->prev = tty_list.prev;
	tty->prev->next = tty;
	tty->next->prev = tty;

	/* create device which correspnd with current TTY */
	dev_register (name, "Virtual terminal", DEV_ATTR_CHAR, (dev_handler_t *) &vterm_acthandler);
	
	tty_count ++;

	return tty;
}

/* thread for running tty consoles - handle selected console */
void task_thread (tty_t *tty)
{
	schedule ();

	if (!tty)
		return;

	if (tty != currtty)
		return;

	if (tty->active) {
		/* handle console on kernel level */
		int id = gets ();
		
		if (id == 1)
			if (tty->user) {
				console (id);
			} else
				getlogin (id);

		/* VESA mode - graphical font */
		if (vgagui == 2) {
			unsigned i = 0;
			unsigned j = 0;
			unsigned k = 0;
			unsigned m = tty_char.x * tty_char.y;

			while (i < m) {
				gputch (1+(j*tty_font.x), 11+(k*(tty_font.y+1)), vesa_16cto16b (tty->screen[i*2+1] & 0x0F), tty->screen[i*2]);
			
				j ++;

				if (j >= tty_char.x) {
					j = 0;
					k ++;
				}
			
				i ++;
			}

			video_gfx_fbswap ();
		}
	}
}

/* startup function for created tty console */
static unsigned task_tty ()
{
	tty_t *tty = tty_findbytask (_curr_task);

	if (!tty)
		return 0;

	for (;;)
		task_thread (tty);
}

/* change tty console to next (+) or previous (-) in list */
void tty_switch (char act)
{
	switch (act) {
		case '+':
		{
			tty_t *tty = currtty->next;

			if (tty == &tty_list)	
				tty = tty_list.next;
			
			if (tty)
				tty_change (tty);
	      
			break;
		}
		case '-':
		{
			tty_t *tty = currtty->prev;

			if (tty == &tty_list)	
				tty = tty_list.prev;
			
			if (tty)
				tty_change (tty);
	      
			break;
		}
	}
}

/* special function for initialization of graphical tty */
tty_t *gtty_init ()
{
	tty_t *tty = tty_create ();

	tty_change (tty);

	return tty;
}

/* initialization of tty */
unsigned int init_tty ()
{
	/* pre-set tty list */
	tty_list.next = &tty_list;
	tty_list.prev = &tty_list;

	/* default values */
	tty_count = 0;

	tty_font_set ((char *) font5x8, 5, 8);

	switch (vgagui) {
		case 0:	/* VGA-Textual console */
			tty_char_set (80, 25);
			break;
		case 1:	/* VGA console */
			tty_char_set (53, 18);
			break;
		case 2:	/* VESA concole */
			tty_char_set (80, 65);
			break;
	}

	video_color (7, 0);

	/* create consoles */
	tty0 = tty_create ();

	if (!tty0)
		return 0;

#ifdef ARCH_i386
	tty1 = tty_create ();

	if (!tty1)
		return 0;
#endif
	tty_startup ();

	/* printf MOTD into TTY0 console */
	tty_motd ();

	/* then print login messages */
	tty_write (tty0, "\nlogin: ", 8);
#ifdef ARCH_i386
	tty_write (tty1, "\nlogin: ", 8);
#endif
	return 1;
}
 
