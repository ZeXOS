/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <libx/base.h>
#include <libx/object.h>
#include <libx/image.h>
#include <libx/cursor.h>
#include <libx/text.h>
#include <pthread.h>
#include "window.h"
#include "cursor.h"
#include "button.h"
#include "menu.h"
#include "dialog.h"
#include "filemanager.h"
#include "config.h"

#define		ESC	1

extern wmcursor *cursor;

void draw_clock ()
{
	char sec[2], min[2], hour[2];
	time_t t;
	time_t ret = time (&t);

	if (!ret)
		return;

	struct tm *ct = localtime (&t);

	itoa (sec, ct->tm_sec, 10);

	xtext_puts (0, 0, 3, sec);
}

unsigned iexit ()
{
	xexit ();
	exit (1);
	return 1;
}

int main (int argc, char **argv)
{
	/* ERROR message :( */
	printf ("Please start this program under VESA mode, because it can crash\n");

	if (!init_config ()) {
		printf ("Something is wrong with init_config () sequence !\n");
		exit (0);
		return 0;
	}

	if (!init_cursor ()) {
		printf ("Your mouse is crappy, try it again :P\n");
		exit (0);
		return 0;
	}

	if (!init_window ()) {
		exit (0);
		return 0;
	}

	if (!init_button ()) {
		exit (0);
		return 0;
	}

	if (!init_menu ()) {
		exit (0);
		return 0;
	}

	if (!init_dialog ()) {
		exit (0);
		return 0;
	}

	xinit ();
	xcls (0);
	printf ("PRD\n");

	/*if (!init_filemanager ()) {
		exit (0);
		return 0;
	}*/

	init_tview ();

	wmbutton *btn_menu = button_create (0, 588, 0, 0, 0, "ZeX/OS");

	wmmenu *mainmenu = menu_create (0, 0, "Menu");

	if (!mainmenu)
		return 0;

	wmmenuitem *item_filemanager = menu_additem (mainmenu, "Filemanager", 0);
	wmmenuitem *item_terminal = menu_additem (mainmenu, "Terminal", 0);
	wmmenuitem *item_authors = menu_additem (mainmenu, "Authors", 0);
	menu_additem (mainmenu, "-----------", 0);
	wmmenuitem *item_exit = menu_additem (mainmenu, "Exit", 0);

	mainmenu->y = btn_menu->y-mainmenu->size_y;

	if (!init_terminal ()) {
		/* ERROR */
	}
	
	if (!init_authors ()) {
		/* ERROR */
	}

	//ifilemanager ();

	while (1) {
		window_draw_all ();

		if (button_flags (btn_menu) & BUTTON_FLAG_CLICKED)
			menu_show (mainmenu);

		//if (menuitem_flags (item_filemanager) & MENUITEM_FLAG_CLICKED)
		//	ifilemanager ();

		if (menuitem_flags (item_terminal) & MENUITEM_FLAG_CLICKED)
			iterminal ();

		if (menuitem_flags (item_authors) & MENUITEM_FLAG_CLICKED)
			iauthors ();

		if (menuitem_flags (item_exit) & MENUITEM_FLAG_CLICKED)
			iexit ();


		filemanager_draw ();

		tview_draw ();

		terminal_handle ();

		dialog_draw_all ();

		authors_handle ();

		button_draw_all ();

		taskbar_draw ();

		menu_draw_all ();
		//draw_clock ();	/* FIXME: freeze */

		cursor_draw ();
	
		schedule ();
	
		xfbswap ();
	}

	return 1;
}
