/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <config.h>
#include <build.h>

#ifdef ARCH_i386

#include <system.h>
#include <dev.h>
#include <arch/io.h>
#include <pci.h>
#include <its.h>
#include <task.h>
#include <sound/audio.h>

#define AC97_VENDORID 0x8086
#define AC97_DEVICEID 0x2415

enum
{
	GLOB_CNT = 0x2c,
	GLOB_STA = 0x30,
	CAS      = 0x34
};

enum {
	PIBDBAR	= 0x00,
	POBDBAR = 0x10,
	MCBDBAR = 0x20
};

enum {
	PICIV = 0x04,
	POCIV = 0x14,
	MCCIV = 0x24
};

enum {
	PISR = 0x06,
	POSR = 0x16,
	MCSR = 0x26
};

enum {
	PILVI = 0x05,
	POLVI = 0x15,
	MCLVI = 0x25
};


enum {
	PIPICB = 0x08,
	POPICB = 0x18,
	MCPICB = 0x28
};

enum {
	PICR = 0x0b,
	POCR = 0x1b,
	MCCR = 0x2b
};

enum {
	AC97_Reset                     = 0x00,
	AC97_Master_Volume_Mute        = 0x02,
	AC97_Headphone_Volume_Mute     = 0x04,
	AC97_Master_Volume_Mono_Mute   = 0x06,
	AC97_Master_Tone_RL            = 0x08,
	AC97_PC_BEEP_Volume_Mute       = 0x0A,
	AC97_Phone_Volume_Mute         = 0x0C,
	AC97_Mic_Volume_Mute           = 0x0E,
	AC97_Line_In_Volume_Mute       = 0x10,
	AC97_CD_Volume_Mute            = 0x12,
	AC97_Video_Volume_Mute         = 0x14,
	AC97_Aux_Volume_Mute           = 0x16,
	AC97_PCM_Out_Volume_Mute       = 0x18,
	AC97_Record_Select             = 0x1A,
	AC97_Record_Gain_Mute          = 0x1C,
	AC97_Record_Gain_Mic_Mute      = 0x1E,
	AC97_General_Purpose           = 0x20,
	AC97_3D_Control                = 0x22,
	AC97_AC_97_RESERVED            = 0x24,
	AC97_Powerdown_Ctrl_Stat       = 0x26,
	AC97_Extended_Audio_ID         = 0x28,
	AC97_Extended_Audio_Ctrl_Stat  = 0x2A,
	AC97_PCM_Front_DAC_Rate        = 0x2C,
	AC97_PCM_Surround_DAC_Rate     = 0x2E,
	AC97_PCM_LFE_DAC_Rate          = 0x30,
	AC97_PCM_LR_ADC_Rate           = 0x32,
	AC97_MIC_ADC_Rate              = 0x34,
	AC97_6Ch_Vol_C_LFE_Mute        = 0x36,
	AC97_6Ch_Vol_L_R_Surround_Mute = 0x38,
	AC97_Vendor_Reserved           = 0x58,
	AC97_Vendor_ID1                = 0x7c,
	AC97_Vendor_ID2                = 0x7e
};

/* buffer descriptor */
typedef struct {
	unsigned zero : 1;
	unsigned buffer : 31;
	unsigned len : 16;
	unsigned reserved : 14;
	unsigned bup : 1;
	unsigned ioc : 1;
} __attribute__ ((__packed__)) ac97_desc_t;

/* device structure */
typedef struct {
	unsigned char state;
	unsigned mixer_base;
	unsigned bus_master;
	int irq;
	unsigned char pobdbar_imax;
	unsigned char pobdbar_icurr;
	ac97_desc_t *pibdbar;
	ac97_desc_t *pobdbar;
	ac97_desc_t *mcbdbar;
} ac97_t;
ac97_t *dev_ac97;

unsigned task_audio ();

/* NABM */
unsigned char ac97_inb_nabm (ac97_t *dev, int port)
{	
	return inb (dev->bus_master + port);
}

unsigned short ac97_inw_nabm (ac97_t *dev, int port)
{	
	return inw (dev->bus_master + port);
}

unsigned ac97_inl_nabm (ac97_t *dev, int port)
{
	return inl (dev->bus_master + port);
}

void ac97_outb_nabm (ac97_t *dev, int port, unsigned char value)
{
	outb (dev->bus_master + port, value);
}

void ac97_outw_nabm (ac97_t *dev, int port, unsigned short value)
{
	outw (dev->bus_master + port, value);
}

void ac97_outl_nabm (ac97_t *dev, int port, int value)
{
	outl (dev->bus_master + port, value);
}

/* NAM */
void ac97_outb_nam (ac97_t *dev, int port, unsigned char value)
{
	outb (dev->mixer_base + port, value);
}

void ac97_outw_nam (ac97_t *dev, int port, unsigned short value)
{
	outw (dev->mixer_base + port, value);
}

void ac97_outl_nam (ac97_t *dev, int port, int value)
{
	outl (dev->mixer_base + port, value);
}

/* detect ac97 device in PC */
pcidev_t *ac97_detect ()
{
	/* First detect sound card - is connected to PCI bus ?*/
	pcidev_t *pcidev = pcidev_find (AC97_VENDORID, AC97_DEVICEID);

	if (!pcidev)
		return 0;

	return pcidev;
}

unsigned init_ac97 ()
{
	pcidev_t *pcidev = ac97_detect ();

	if (!pcidev)
		return 0;

	ac97_t *dev = (ac97_t *) kmalloc (sizeof (ac97_t));

	if (!dev)
		return 0;

	/* get irq id */
	dev->irq = pcidev->u.h0.interrupt_line;
	dev->mixer_base = pcidev->u.h0.base_registers[0];
	dev->bus_master = pcidev->u.h0.base_registers[1];

	kprintf ("ac97 -> found at 0x%x, 0x%x; irq: %d\n", dev->mixer_base, dev->bus_master, dev->irq);

	unsigned char *c = (unsigned char *) dev->mixer_base;

	//ac97_outb (dev, 0x0, 1);

	unsigned i, y;
	//for (i = 0; i < 100; i ++)
	//kprintf ("| 0x%x", c[i]);

	unsigned mask = 0x1 << 5;
	unsigned char r = ac97_inb_nabm (dev, CAS);

	dev->pibdbar = (ac97_desc_t *) 0x65000;
	dev->pobdbar = (ac97_desc_t *) 0x65100;
	dev->mcbdbar = (ac97_desc_t *) 0x65200;

	ac97_outl_nabm (dev, PIBDBAR, (unsigned) dev->pibdbar);
	ac97_outl_nabm (dev, POBDBAR, (unsigned) dev->pobdbar);
	ac97_outl_nabm (dev, MCBDBAR, (unsigned) dev->mcbdbar);

	/* open */
	ac97_outw_nam (dev, AC97_Reset, 0);
	ac97_outw_nam (dev, AC97_Powerdown_Ctrl_Stat, 0);

	ac97_outw_nam (dev, AC97_Master_Volume_Mute, 0x1 | 0x1 << 8);
	ac97_outw_nam (dev, AC97_PCM_Out_Volume_Mute, 0x1 | 0x1 << 8);

	dev_ac97 = dev;
/*
unsigned int val = 0x8808;
int mute = (val >> MUTE_SHIFT) & 1;
    unsigned char rvol = VOL_MASK - (val & VOL_MASK);
    unsigned char lvol = VOL_MASK - ((val >> 8) & VOL_MASK);
    rvol = 255 * rvol / VOL_MASK;
    lvol = 255 * lvol / VOL_MASK;

	kprintf ("lvol: 0x%x | rvol: 0x%x | mute: %d\n", lvol, rvol, mute);
*/

	/*unsigned char civ = ac97_inb_nabm (dev, POCIV);
	kprintf ("civ: 0x%x\n", civ);

	unsigned r2 = ac97_inl_nabm (dev, POBDBAR);
	kprintf ("r: 0x%x\n", r2);*/

	//dev->pobdbar_imax = 0x1;
	//ac97_outb_nabm (dev, POLVI, dev->pobdbar_imax);
//dev->pobdbar_icurr = 0;
	/* start new task/thread for network core */
	task_create ("audio", (unsigned) task_audio, 255);

	//ac97_outl (dev, 0xab, mask);
	//ac97_outb (dev, 54, 1);

	return 1;
}

int setup_ac97 (ac97_t *dev, snd_audio_t *aud)
{
	if (!aud)
		return 0;

	if (!aud->rate || !aud->format || !aud->channels) {
		ac97_outb_nabm (dev, POCR, 0x0);
		return 1;
	}

	/* we want to setup device for specific frequency rate, etc */
	ac97_outw_nam (dev, AC97_Extended_Audio_Ctrl_Stat, 1);
	ac97_outw_nam (dev, AC97_PCM_Front_DAC_Rate, aud->channels == 1 ? aud->rate/2 : aud->rate);

	ac97_outb_nabm (dev, POCR, 0x1);

	return 1;
}

unsigned task_audio ()
{
	unsigned i;
	ac97_desc_t *pobdbar = (ac97_desc_t *) 0x65100;
	ac97_t *dev = dev_ac97;

	unsigned char civ = ac97_inb_nabm (dev, POCIV);

	for (;; schedule ()) {
		unsigned r3 = ac97_inw_nabm (dev, POSR);
		
		//kprintf ("POSR: 0x%x\n", r3);
		//kprintf ("POCIV: 0x%x\n", dev->pobdbar_icurr);
		
		if (r3 & 0x8) {
			//kprintf ("0x8\n");
			ac97_outw_nabm (dev, POSR, r3);
		}
		if (r3 & 0x1) {
			//kprintf ("0x1\n");
			ac97_outb_nabm (dev, POLVI, 0x1f);
		}

		/*unsigned c = ac97_inb_nabm (dev, POCIV);

		if (c != civ) {
			civ = c;
			printf ("civ: %d\n", c);
		}*/
	}

	return 1;
}

bool ac97_acthandler (unsigned act, char *buf, unsigned len)
{
	ac97_t *dev = dev_ac97;

	switch (act) {
		case DEV_ACT_INIT:
		{
			return init_ac97 ();
		}
		break;
		case DEV_ACT_UPDATE:
		{
			if (buf && len == sizeof (snd_audio_t))
				return setup_ac97 (dev, (snd_audio_t *) buf);
		}
		break;
		case DEV_ACT_STOP:
		{
			return 1;
		}
		break;
		case DEV_ACT_WRITE:
		{
			unsigned r3 = 0;

			for (; ac97_inb_nabm (dev, POCIV) != 0x1f; schedule ());

			//unsigned char *dma = 0x600000;
			
			unsigned i = 0, l = 0;
			unsigned p = len;
			for (i = 0; i < 32; i ++) {
				if (p > 0x1FFE0)
					l = 0x1FFE0;
				else
					l = p;

				dev->pobdbar[i].zero = 0;
				dev->pobdbar[i].buffer = ((unsigned) (buf + (i*0x1FFE0)) >> 1);
				dev->pobdbar[i].len = (unsigned short) (l / 2);
				dev->pobdbar[i].reserved = ~1;
				dev->pobdbar[i].bup = 0;
				dev->pobdbar[i].ioc = 0;

				p -= l;
			}

			/*	r3 = ac97_inw_nabm (dev, POSR);

				if (r3 & 0x8) {
					kprintf ("0x8\n");
					ac97_outw_nabm (dev, POSR, r3);
				}
				if (r3 & 0x1) {
					kprintf ("0x1\n");
					ac97_outb_nabm (dev, POLVI, 0x1f);
				}
			ac97_outb_nabm (dev, POLVI, 0x1);


			printf ("picb: %d\n", ac97_inw_nabm (dev, POPICB));*/

			//printf ("dev->pobdbar_icurr: %d\n", dev->pobdbar_icurr);
			//dev->pobdbar_icurr ++;
			
			//ac97_outb_nabm (dev, POLVI, 0x1);
			//if (dev->pobdbar_icurr)


			return 1;
		}
		break;
	}

	return 0;
}
#endif
 
