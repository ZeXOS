/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h>

/**
 * strcmp - Compare two case-insensitive strings
 * @s1: One string
 * @s2: Another string
 */
int strcasecmp (const char *s1, const char *s2)
{
	unsigned i;
	unsigned n = strlen (s1);
	
	for (i = 0; i < n; i ++) {
		if (s1[i] != s2[i] && s1[i] != s2[i] + 32)
			return i;
	}

	return 0;
}

/**
 * strncasecmp - Compare two case-insensitive, length-limited strings
 * @s1: One string
 * @s2: Another string
 * @n: The maximum number of bytes to compare
 */
int strncasecmp (const char *s1, const char *s2, unsigned n)
{
	unsigned i;
	
	for (i = 0; i < n; i ++) {
		if (s1[i] != s2[i] && s1[i] != s2[i] + 32)
			return i;
	}

	return 0;
}
