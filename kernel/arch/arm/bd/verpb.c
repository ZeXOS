/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 

#include <config.h>
#ifdef CONFIG_DRV_ARM_BOARD_VERPB
#include <arch/bd/verpb.h>

/**
  * PL011 
  * Uart device 
  * Serial cable input/output
  */

typedef struct {
	unsigned char c : 8;
	unsigned fr_err : 1;
	unsigned par_err : 1;
	unsigned break_err : 1;
	unsigned ovrrun_err : 1;
} __attribute__ ((__packed__)) arm_rcv_block;

int armbd_uart_write (int port, unsigned char c)
{
	unsigned char *uart_addr = (unsigned char *) ARM_VERPB_PL011_BASE0;

	if (port)
		uart_addr = (unsigned char *) ARM_VERPB_PL011_BASE1;

	uart_addr += ARM_VERPB_PL011_DR * 4; 

	*uart_addr = c;

	return 1;
}

unsigned char armbd_uart_received (int port)
{
	unsigned *uart_addr = (unsigned *) ARM_VERPB_PL011_BASE0;

	if (port)
		uart_addr = (unsigned *) ARM_VERPB_PL011_BASE1;

	uart_addr += 6; 

	if (*uart_addr & ARM_VERPB_PL011_RXFE)
		return 0;

	return 1;
}

unsigned char armbd_uart_read (int port)
{
	arm_rcv_block *uart_addr = (arm_rcv_block *) ARM_VERPB_PL011_BASE0;

	if (port)
		uart_addr = (arm_rcv_block *) ARM_VERPB_PL011_BASE1;

#ifdef ARM_UART_ERR_LOG
	unsigned char err = 0;

	/* there is error on uart, lets clean it */
	if (uart_addr->ovrrun_err)
		err ++;

	if (uart_addr->break_err)
		err ++;

	if (uart_addr->par_err)
		err ++;

	if (uart_addr->fr_err)
		err ++;
#endif
	return uart_addr->c;
}


/**
  * PL110 
  * ARM PrimeCell Color LCD Controller
  * Display output
  */

void armbd_pl110_write (unsigned offset, unsigned val)
{
	char *c = (char *) ARM_VERPB_PL110_BASE;
	unsigned *p = (unsigned *) &c[offset];

	*p = val;
}

unsigned armbd_pl110_read (unsigned offset)
{
	char *c = (char *) ARM_VERPB_PL110_BASE;
	unsigned *p = (unsigned *) &c[offset];

	return *p;
}

void armbd_display_enable ()
{
	armbd_pl110_write ((0x4 << 2), 0x500000);			/* upper video memory address */
	armbd_pl110_write ((0x5 << 2), 0x600000);			/* lower video memory address */
	armbd_pl110_write ((0x7 << 2), ARM_VERPB_PL110_CR_EN |
				       ARM_VERPB_PL110_CR_PWR);		/* enable device */
	armbd_pl110_write ((0x0 << 2), 157);				/* 640 */ 
	armbd_pl110_write ((0x1 << 2), 399);				/* 400 */
}

/**
  * PL050 
  * ARM PrimeCell PS2 Keyboard/Mouse Interface
  * Keyboard & Mouse over PS2
  */

#define KBD_CMD_ENABLE		0xF4	/* Enable scanning */
#define KBD_REPLY_ACK		0xFA	/* Command ACK */
#define KBD_CMD_RESET_ENABLE   	0xF6    /* reset and enable scanning */

void armbd_pl050_write (unsigned offset, unsigned val)
{
	char *c = (char *) ARM_INTCP_PL050_KB_BASE;
	unsigned *p = (unsigned *) &c[offset];

	*p = val;
}

unsigned armbd_pl050_read (unsigned offset)
{
	char *c = (char *) ARM_INTCP_PL050_KB_BASE;
	unsigned *p = (unsigned *) &c[offset];

	return *p;
}

unsigned armbd_kbd_data ()
{
	return armbd_pl050_read (1 << 2) == ARM_INTCP_PL050_TXEMPTY ? 0 : 1;
}

unsigned armbd_kbd_scancode ()
{
	return armbd_pl050_read (2 << 2);
}

unsigned armbd_kbd_ack ()
{
	armbd_pl050_write (2 << 2, KBD_REPLY_ACK);
}

unsigned armbd_kbd_init ()
{
	armbd_pl050_write (2 << 2, KBD_CMD_RESET_ENABLE);

//	armbd_pl050_write (2 << 2, KBD_CMD_SET_RATE);

	return 1;
}

#endif
