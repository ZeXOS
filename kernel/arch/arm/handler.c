/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <arch/ctx.h>

void arm_syscall_handler (struct arm_fault_frame *frame)
{
	printf ("unhandled syscall\n");
}

void arm_undefined_handler (struct arm_fault_frame *frame)
{
	printf ("unhandled undefined state\n");
}

void arm_data_abort_handler (struct arm_fault_frame *frame)
{
	printf ("unhandled abort\n");
}

void arm_prefetch_abort_handler (struct arm_fault_frame *frame)
{
	printf ("unhandled prefetch abort\n");
}
