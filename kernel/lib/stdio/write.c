/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Martin 'povik' Poviser (martin.povik@gmail.com)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <console.h>
#include <system.h>
#include <string.h>
#include <stdio.h>
#include <file.h>
#include <mount.h>
#include <env.h>
#include <vfs.h>
#include <tty.h>
#include <fd.h>
#include <pipe.h>
#include <cache.h>
#include <proc.h>

/* TODO: flags */

int write (unsigned fd, void *buf, unsigned len)
{
	fd_t *d = fd_get (fd);

	if (!d) {
		DPRINT (DBG_LIB | DBG_STDIO, "write () -> !fd %d", fd);
		return 0;
	}

	if (d->flags & O_RDONLY)
		return 0;

	if (d->flags & FD_PIPE) {
		pipe_t *p = pipe_get (fd);
		
		if (p)
			return pipe_write (p, buf, len);
		else
			return 0;
	}
	
	if (d->flags & FD_TERM) {
		tty_t *ttyold = currtty;
		tty_t *tty = 0;
		
		if (d->dev)
			tty = tty_find (d->dev->devname+5);
	
		if (tty)
			tty_change (tty);
		else
			return -1;
		
		//task_t *taskold = _curr_task;
		//_curr_task = tty->task;

		char *str = (char *) buf;
		
		int i = 0;
		
		for (i = len; i >= 0; i --)
			setkey (str[i]);
		
		int id = gets ();
		
		if (id == 1) {
			if (tty->user) {
				console (id);
			} else
				getlogin (id);
		}
		
		//_curr_task = taskold;
		
		tty_change (ttyold);
		
		return 1;
	}

	char *file = d->path;

	if (!file) {
		DPRINT (DBG_LIB | DBG_STDIO, "write () -> !file");
		return 0;
	}

	unsigned file_len = strlen (file);

	/* create file when not exist */
	if (d->flags & O_CREAT)
		vfs_touch ((char *) file, file_len);

	if (!d->s) {
		cache_t *cache = cache_create (buf, len, 1);

		if (!cache)
			return 0;

		d->s = (char *) &cache->data;
		//memcpy (d->s, (char *) buf, len);
		//d->s[len] = '\0';
	} else {
		DPRINT (DBG_LIB | DBG_STDIO, "write () - file descriptor got wrong address space");
		return 0;
	}

	if (!d->e) {
	  	proc_t *proc = proc_find (_curr_task);

		if (!proc)
			proc = (proc_t *) &proc_kernel;
		
		/* calculate size of process image (binary file) */
		unsigned p = (unsigned) palign ((void *) (proc->end - proc->start));

		vfs_content_t content;
		content.ptr = d->s - p;
		content.len = len;
		
		if (!vfs_mmap (file, file_len, &content)) {
			DPRINT (DBG_LIB | DBG_STDIO, "write () -> !vfs_mmap");
			return 0;
		}
	} else {
		DPRINT (DBG_LIB | DBG_STDIO, "write () - d->e > 0 (not implemented) - O_APPEND..");
		return 0;
	}

	d->e += len;

	return 1;
}
 
 
