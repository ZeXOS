/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2010  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <string.h>
#include <build.h>
#include <config.h>
#include <mutex.h>
#include <errno.h>
#include <signal.h>

#define UMEM_HEAP_ADDR		0x800000	// MEMORY HEAP is placed at 8MB address
#define UMEM_MALLOC_MAGIC	0xebc9		// MALLOC STRUCTURE signature

#define UMEM_MALLOC_STATE_FREE	0xaa		// MEMORY is FREE at moment
#define UMEM_MALLOC_STATE_USED	0xcc		// MEMORY is USED at moment

typedef struct {
	unsigned short magic;
	unsigned char state;
	void *next;
	proc_t *proc;
} __attribute__ ((__packed__)) malloc_t;

typedef struct {
	void *addr;		/* heap structure is covered by own heap structure address */
	void *first;		/* first malloc structure */
	void *last;		/* last malloc structure - usefully for calculate memory usage */
	void *curr;		/* current malloc structure */
} __attribute__ ((__packed__)) heap_t;

static heap_t *heap;			/* heap structure of memory */
static MUTEX_CREATE (mutex_umalloc);
static MUTEX_CREATE (mutex_ufree);

void *umalloc (proc_t *proc, size_t size)
{
	if (!size)
		return 0;

	mutex_lock (&mutex_umalloc);

	unsigned malloc_len = sizeof (malloc_t);		/* check malloc structure size */
	unsigned need_alloc = 0;

	/* let's find new or free malloc structure in memory */
	malloc_t *malloc = (malloc_t *) heap->first;

	for (;;) {
		//DPRINT (DBG_MM | DBG_UMEM, "malloc: 0x%x\n", malloc);

		if (!malloc) {
			DPRINT (DBG_MM | DBG_UMEM, "!malloc");
			need_alloc = 1;
			break;
		}

		if (malloc->magic != UMEM_MALLOC_MAGIC) {
			DPRINT (DBG_MM | DBG_UMEM, "malloc->magic != UMEM_MALLOC_MAGIC");
			need_alloc = 1;
			break;
		}

		if (malloc->state == UMEM_MALLOC_STATE_USED) {	/* It's OK */
			DPRINT (DBG_MM | DBG_UMEM, "malloc->state == UMEM_MALLOC_STATE_USED");
			//need_alloc = 1;
			malloc = (malloc_t *) malloc->next;
			continue;
		}

		/* we've found FREE memory block, at moment it is not used */
		if (malloc->state == UMEM_MALLOC_STATE_FREE) {
			/* read next malloc structure, right behind free block */
			malloc_t *next = (malloc_t *) malloc;

			DPRINT (DBG_MM | DBG_UMEM, "malloc->state == UMEM_MALLOC_STATE_FREE");
			
			for (;;) {
				if (next->magic != UMEM_MALLOC_MAGIC) {
					DPRINT (DBG_MM | DBG_UMEM, "2: next->magic != UMEM_MALLOC_MAGIC (%x/%x)", next->magic, UMEM_MALLOC_MAGIC);
					break;
				}

				/* unused memory size between current and next block */
				long diff = ((unsigned) (next->next) - (unsigned) (malloc));
				
				/* does needed block fit in unused one ? */
				if (diff >= (unsigned) size + malloc_len) {
					need_alloc = 2;
				  
					/* we have to join all continuous free blocks into one and match last (next) */
					if (next != malloc)
						malloc->next = (malloc_t *) ((void *) malloc + size + malloc_len);
					
					if (diff > size + malloc_len) {
						
						unsigned mnext = (unsigned) next->next;
					  
						malloc_t *n = (malloc_t *) ((void *) malloc + size + malloc_len);
					  
						n->magic = UMEM_MALLOC_MAGIC;
						n->state = UMEM_MALLOC_STATE_FREE;
						n->next = (void *) mnext;
						
						next->next = n;
						
						//malloc = (char *) next;
						
						//heap->curr = mnext;
						//kprintf ("DIFF: %d - 0x%x / 0x%x\n", diff - malloc_len, mnext, n);
					}
					break;
				} else {
					if (next->state == UMEM_MALLOC_STATE_USED)
						need_alloc = 1;
				}
				if (next->state == UMEM_MALLOC_STATE_USED)
					break;

				if (next->state != UMEM_MALLOC_STATE_FREE)
					break;
				
				next = (malloc_t *) next->next;
			}

			/*if (need_alloc)
				break;

			unsigned diff = (unsigned) ((void *) next - (void *) malloc);

			DPRINT (DBG_MM | DBG_UMEM, "diff: %d", diff);

			if (diff < size) {
				malloc = (malloc_t *) malloc->next;
				continue;
			}*/

			break;
		}
		
		if (need_alloc)
			break;
		
		malloc = (malloc_t *) malloc->next;
	}

	if (need_alloc == 1) {
		/* when old malloc structures are full, let's grab new block of memory */
		malloc = heap->curr;

		malloc->magic = UMEM_MALLOC_MAGIC;
		
		malloc->next = (void *) ((void *) malloc + malloc_len + size);

		heap->curr = malloc->next;
	}

	malloc->state = UMEM_MALLOC_STATE_USED;

	malloc->proc = proc;

	/* calculate size of process image (binary file) */
	unsigned p = (unsigned) palign ((void *) (proc->end - proc->start));

	/* save last malloc structure pointer to heap */
	if ((unsigned) heap->curr > (unsigned) heap->last)
		heap->last = heap->curr;

	unsigned l;
	unsigned s = proc->heap;
	for (l = s; l < (unsigned) ((char *) malloc + malloc_len + size); l += 0x1000) {
#ifdef ARCH_i386
		page_mmap (proc->task->page_cover, (void *) proc->heap+p, (void *) proc->heap+p + 0x1000, 0, 1);
#endif
		//kprintf ("OD: 0x%x DO: 0x%x | 0x%x | 0x%x\n", (void *) proc->heap + p, (void *) proc->heap + 0x1000 + p, s, size);
		proc->heap += 0x1000;
	}

	mutex_unlock (&mutex_umalloc);
	//kprintf ("MEM: 0x%x - %d - 0x%x\n", ((void *) malloc + malloc_len + p), size, p);
	return (void *) ((void *) malloc + malloc_len + p);
}

void ufree (proc_t *proc, void *ptr)
{
	if (!ptr || !proc)
		return;

	mutex_lock (&mutex_ufree);

	/* calculate size of process image (binary file) */
	unsigned p = (unsigned) palign ((void *) (proc->end - proc->start));

	/* we need find malloc structure - it could be risk working with lower memory address */
	malloc_t *malloc = (void *) ((void *) ptr - sizeof (malloc_t) - p);

	/* check malloc signature, when not agree, ptr pointing to wrong memory address */
	if (malloc->magic != UMEM_MALLOC_MAGIC) {
		DPRINT (DBG_MM | DBG_UMEM, "free: malloc->magic != UMEM_MALLOC_MAGIC");
		goto end;
	}

	/* is this memory block our ? */
	if (malloc->proc != proc) {
		DPRINT (DBG_MM | DBG_UMEM, "free: malloc->proc != proc");
		goto end;
	}

	if (malloc->state == UMEM_MALLOC_STATE_FREE) {
		DPRINT (DBG_MM | DBG_UMEM, "free () - memory is FREE - double free !");
		goto end;
	}

	if (malloc->state != UMEM_MALLOC_STATE_USED) {
		DPRINT (DBG_MM | DBG_UMEM, "malloc->state != UMEM_MALLOC_STATE_USED");
		goto end;
	}

	/* match malloc as FREE */
	malloc->state = UMEM_MALLOC_STATE_FREE;

end:
	mutex_unlock (&mutex_ufree);
}

void *urealloc (proc_t *proc, void *ptr, size_t size)
{
	if (!proc)
		return 0;

	if (!ptr)
		return umalloc (proc, size);
	
	if (!size) {
		ufree (proc, ptr);
		return 0;
	}
	
	/* calculate size of process image (binary file) */
	unsigned p = (unsigned) palign ((void *) (proc->end - proc->start));

	/* we need find malloc structure - it could be risk working with lower memory address */
	malloc_t *malloc = (void *) ((char *) ptr - sizeof (malloc_t) - p);

	/* check malloc signature, when not agree, ptr pointing to wrong memory address */
	if (malloc->magic != UMEM_MALLOC_MAGIC) {
		DPRINT (DBG_MM | DBG_UMEM, "urealloc: malloc->magic != UMEM_MALLOC_MAGIC");
		return 0;
	}
	
	int s = (int) (malloc->next - ptr) + p;

	if (s >= size)
		return 0;

	char *ptr_new = umalloc (proc, size);

	if (!ptr_new) {
		ufree (proc, ptr);
		errno_set (ENOMEM);
		return 0;
	}

	/* copy old memory block to new one (-p because ptr and ptr_new got virtual mapped address) */
	memcpy (ptr_new - p, ptr - p, s);

	ufree (proc, ptr);

	return ptr_new;
}

/* free all memory blocks allocated by process */
unsigned ufree_proc (proc_t *proc)
{
#ifndef ARCH_arm
	paging_disable ();
#endif
	unsigned used = 0;
	malloc_t *malloc;

	for (malloc = (malloc_t *) heap->first; malloc->magic == UMEM_MALLOC_MAGIC; malloc = (malloc_t *) malloc->next)
		if (malloc->proc == proc)
			if (malloc->state == UMEM_MALLOC_STATE_USED)
				malloc->state = UMEM_MALLOC_STATE_FREE;
#ifndef ARCH_arm
	paging_enable ();
#endif
	return used;
}

/* calculate used memory by process */
unsigned umem_used_proc (proc_t *proc)
{
#ifndef ARCH_arm
	paging_disable ();
#endif
	unsigned used = 0;
	malloc_t *malloc;

	for (malloc = (malloc_t *) heap->first; malloc->magic == UMEM_MALLOC_MAGIC; malloc = (malloc_t *) malloc->next)
		if (malloc->proc == proc)
			if (malloc->state == UMEM_MALLOC_STATE_USED)
				used += (int) ((unsigned) malloc->next - (unsigned) malloc);
#ifndef ARCH_arm
	paging_enable ();
#endif
	return used;
}

/* calculate used memory from all apps */
unsigned umem_used ()
{
#ifndef ARCH_arm
	paging_disable ();
#endif
	unsigned used = 0;
	malloc_t *malloc;

	for (malloc = (malloc_t *) heap->first; malloc->magic == UMEM_MALLOC_MAGIC; malloc = (malloc_t *) malloc->next)
		if (malloc->state == UMEM_MALLOC_STATE_USED)
			used += (int) ((unsigned) malloc->next - (unsigned) malloc);
#ifndef ARCH_arm
	paging_enable ();
#endif
	return used;
}

unsigned umem_init ()
{
	heap = (heap_t *) UMEM_HEAP_ADDR;

	/* we rather clear heap structure */
	heap->addr = (void *) heap;
	heap->first = (void *) ((void *) heap + sizeof (heap_t)); 	/* first malloc structure should be right behind heap structure */
	heap->last = heap->first;					/* last malloc structure - it is first at init */
	heap->curr = heap->first;					/* point to first malloc structure - well, it is clear and not used yet */

	return 1;
}
