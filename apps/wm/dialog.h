/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _DIALOG_H
#define _DIALOG_H

#define DIALOG_FLAG_DRAG	0x1

typedef struct wmdialog_context {
	struct wmdialog_context *next, *prev;

	unsigned x;
	unsigned y;
	unsigned size_x;
	unsigned size_y;
	unsigned char active;
	unsigned char flags;
	char *caption;
} wmdialog;

/* externs */
extern wmdialog *dialog_create (unsigned x, unsigned y, unsigned size_x, unsigned size_y, const char *caption);
extern unsigned dialog_delete (wmdialog *dialog);
extern unsigned dialog_draw (wmdialog *dialog);
extern unsigned dialog_draw_all ();
extern unsigned init_dialog ();

#endif
