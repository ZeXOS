/*
 *  ZeX/OS
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SYS_TIME_H
#define _SYS_TIME_H

struct timeval {
	long tv_sec;		/* seconds */
	long tv_usec;		/* microseconds */
};

struct timezone {
	int tz_minuteswest;     /* minutes west of Greenwich */
	int tz_dsttime;         /* type of DST correction */
};

enum {
        DST_NONE,	/* not on dst */
        DST_USA,	/* USA style dst */
        DST_AUST,	/* Australian style dst */
        DST_WET,	/* Western European dst */
        DST_MET,	/* Middle European dst */
        DST_EET,	/* Eastern European dst */
        DST_CAN,	/* Canada */
        DST_GB,		/* Great Britain and Eire */
        DST_RUM,	/* Rumania */
        DST_TUR,	/* Turkey */
        DST_AUSTALT	/* Australian style with shift in 1986 */ 
};

#define timerisset(tvp)\
	((tvp)->tv_sec || (tvp)->tv_usec)
#define timercmp(tvp, uvp, cmp)\
	((tvp)->tv_sec cmp (uvp)->tv_sec ||\
	(tvp)->tv_sec == (uvp)->tv_sec &&\
	(tvp)->tv_usec cmp (uvp)->tv_usec)
#define timerclear(tvp)\
	((tvp)->tv_sec = (tvp)->tv_usec = 0)
	
extern int gettimeofday (struct timeval *tv, struct timezone *tz);

#endif
