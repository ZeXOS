/*
 *  ZeX/OS
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "client.h"
#include "proto.h"
#include "game.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define OK 0
#define FAIL -1

int rules_verify (game_t *game, int zx, int zy, int cx, int cy)
{
	int i;
	int ok;

	char f = game->board[(int)zy][(int)zx];

	if (game->player == 'w') {
		if (f > 'a' && f < 'z')
			return 1;
	} else if (game->player == 'b')
		if (f > 'A' && f < 'Z')
			return 1;	

	switch (f) {
		case 'p':				// cerny pesec
			
			ok = 1;
			if ((game->board[cy][cx] == '.') && (abs (cy-zy)== 1))
				ok --;
			// pokud je cilove policko volne a pohyb je o 1 pole tak je to OK
			if ((game->board[cy][cx] == '.') && (game->board[cy+1][cx] == '.') && (zy == 6) && (abs (cy-zy) == 2))
				ok --; // pocatecni pozice

			// pokud je cilove a predcilove policko volne a pohyb je o 2 pole tak je to OK
			if ((abs (cx-zx)== 1) && ((zy-cy)== 1) && (game->board[cy][cx] < 'Z') && (game->board[cy][cx] > '.'))
				ok --; // jiny sloupec -> bere figuru, pouze souperovu
				

			break;
			
		case 'P':				// BILY pesec	
			ok = 1;
			if ((game->board[cy][cx] == '.') && ((cy - zy)== 1))
				ok --;
				// pokud je cilove policko volne a pohyb je o 1 pole tak je to OK

			if ((game->board[cy][cx] == '.') && 
				(game->board[cy-1][cx] == '.') &&
				(zy == 1) &&									// pocatecni pozice	
				(abs (cy - zy) == 2))
					ok --;
				// pokud je cilove a predcilove policko volne a pohyb je o 2 pole tak je to OK
			
			if ((abs (cx - zx) == 1) && ((cy - zy) == 1) &&
				(game->board[cy][cx] > 'Z'))
					ok --;
				// jiny sloupec -> bere figuru, pouze souperovu

			break;
		
		case 'K':				// kral
		case 'k':
			ok = 2;

			if (abs (cx - zx) <= 1)
				ok --;	// pohyb pouze o jedno pole v libovolnem smeru

			if (abs (cy - zy) <= 1)
				ok --;
			
			break;
		case 'D':				// dama
		case 'd':	
		 	ok = 1;

			if ((cx == zx) || (cy == zy)) {		// pohyb pouze horizontalne nebo vertikalne
				for (i = zx; i < cx; i ++)
					if (game->board[zy][i] != '.')
						ok ++;		// f stoji v ceste 
				
				for (i = zx; i > cx; i --) 
					if (game->board[zy][i] != '.')
						ok ++;		// f stoji v ceste  
				
				for (i = zy; i<cy; i ++) 
					if (game->board[i][zx] != '.')
						ok ++;		// f stoji v ceste  
			
				for (i = zy; i>cy; i --) 
					if (game->board[i][zx] != '.')
						ok ++;		// f stoji v ceste  
					
				ok -= 2;
			} else {
				for (i = 0; i < (cx - zx); i ++) {
					if (cy > zy) {
						if (game->board[zy+i][zx+i] != '.')
							ok ++;
					} else
						if (game->board[zy-i][zx+i] != '.')
							ok ++;
				}
				
				for (i = 0; i > (cx - zx); i --) {
					if (cy < zy) {
						if (game->board[zy+i][zx+i] != '.')
							ok ++;
					} else {
						if (game->board[zy-i][zx+i] != '.')
							ok ++;	// f stoji v ceste 
					}
				}
			
				ok -= 2;
			}
		
			break;
		case 'S':				// strelec
		case 's':	
			
			ok = 1;	
		
			for (i = 0; i < (cx - zx); i ++) {
				if (cy > zy) {
					if (game->board[zy+i][zx+i] != '.')
						ok ++;
				} else
					if (game->board[zy-i][zx+i] != '.')
						ok ++;

			}
			
			for (i = 0; i > (cx - zx); i --) {
				if (cy < zy) {
					if (game->board[zy+i][zx+i] != '.')
						ok ++;
				} else {
					if (game->board[zy-i][zx+i] != '.')
						ok ++; // f stoji v ceste
				}
			}
					
					
					
			ok -= 2;

			break;
		case 'J':				// jezdec / kun
		case 'j':	
			ok = 1;
			if (((abs (cx - zx) == 2) && (abs (cy - zy) == 1))
			|| ((abs (cy - zy) == 2) && (abs (cx - zx) == 1)))
				ok --;	// pohyb pouze do "L"
			
			break;
		case 'V':				// vez
		case 'v':
			ok = 1;
			if ((cx == zx) || (cy == zy))
				ok --;	// pohyb pouze horizontalne nebo vertikalne
			 
			for (i = zx; i < cx; i ++)
				if (game->board[zy][i] != '.')
					ok ++;		// f stoji v ceste 
			
			for (i = zx; i > cx; i --)
				if (game->board[zy][i] != '.') ok ++;		// f stoji v ceste
			 
			for (i = zy; i < cy; i ++)
				if (game->board[i][zx] != '.')
					ok ++;		// f stoji v ceste  
		
			for (i = zy; i > cy; i --)
				if (game->board[i][zx] != '.')
					ok ++;		// f stoji v ceste  
			
			ok -= 1;	// 1x se testuje pozice veze samotne
			break;

		default: return FAIL;	
	}

	if (!ok)
		return OK;	
	
	return FAIL;
}
