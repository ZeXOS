/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <system.h>
#include <arch/io.h>
#include <string.h>
#include <tty.h>
#include <proc.h>
#include <net/socket.h>
#include <file.h>
#include <signal.h>
#include <module.h>
#include <rs232.h>

int sattrib = 0x0F;

extern task_t *_curr_task;
void sys_exit (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->task != _curr_task)
		return;

	if (proc) {
		task_t *task = proc->tty->task;

		proc->pid = 0;

		/*if (int_disable ())
			longjmp (task->state, 1);
		else
			while (1)
				schedule ();*/
	}
}

void sys_getch (struct regs *r)
{
    	unsigned att = sattrib << 8;
	unsigned short *where;

	unsigned char c = (unsigned char) getkey ();

	if (c != 0) {
		setkey (0);
	}

	unsigned short *memptr = (unsigned short *) 0x9000;
	where = memptr;
        *where = c | att;
	
}

void sys_sleep (struct regs *r)
{
	timer_wait (1000 * (unsigned) r->r9);
}

void sys_putch (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->task != _curr_task)
		return;

	if (proc->tty != currtty)
		return;

	tty_putnch (proc->tty, r->r9);
}

void sys_color (struct regs *r)
{
	settextcolor (r->r9, r->r10);
}


void sys_cls (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->task != _curr_task)
		return;

	if (proc->tty != currtty)
		return;

	int_disable ();
	tty_cls (proc->tty);
	int_enable ();
}

extern unsigned char scancode;
void sys_getkey (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->task != _curr_task)
		return;

	if (proc->tty != currtty)
		return;

    	unsigned att = sattrib << 8;
	unsigned short *where;
	unsigned char c = (unsigned char) scancode;

	scancode = 0;

	unsigned short *memptr = (unsigned short *) 0x9000;
	where = memptr;
        *where = c | att;
}

void sys_gotoxy (struct regs *r)
{
	gotoxy (r->r9, r->r10);
}

void sys_fork (struct regs *r)
{
    	unsigned att = sattrib << 8;
	unsigned short *where;
	unsigned char c = (unsigned char) fork ();

	unsigned short *memptr = (unsigned short *) 0x9000;
	where = memptr;
        *where = c | att;
}

void sys_schedule (struct regs *r)
{
	schedule ();
}

extern kbd_quaue kbd_q;
void sys_write (struct regs *r)
{
	unsigned len = r->r10;
	unsigned fd = r->r11;

	unsigned char *buf = (unsigned char *) r->r9;

	if (!buf)
		return;

	//kprintf ("#buf: 0x%x / '%s'\n", buf, buf);

	/*char buf[len+1];
	if (!memcpy_to_user (&buf, (char *) r->r9, len))
		return;*/

	int ret = 0;

	proc_t *proc = 0;

	switch (fd) {
		case 0:
			proc = proc_find (_curr_task);
		
			if (!proc)
				return;
		
			if (proc->task != _curr_task)
				return;

			//if (proc->tty != currtty)
			//	return;

			//printf ("data: 0x%x | 0x%x | 0x%x\n", buf, proc->data, proc->data_off);
			//printf ("hehe: 0x%x+0x%x / '%s' len: %d\n", buf+proc->code, proc->data_off, buf+proc->code, len);
			//if (!memtest_data (buf, (void *) proc->data, proc->data_off))
			tty_write (proc->tty, (char *) buf, len);
			//else {
			//	tty_write (proc->tty, (char *) buf+proc->code, len);
			//}
			ret = len;
			break;
		case 1:
			proc = proc_find (_curr_task);
		
			if (!proc)
				return;
		
			if (proc->task != _curr_task)
				return;

			if (len > KBD_MAX_QUAUE)
				len = KBD_MAX_QUAUE;

			int i = 0;
			for (i = len; i >= 0; i --) {
				setkey (buf[i]);
				kbd_q.state[kbd_q.p] = 1;	// down
				kbd_q.p ++;
			}

			buf[len] = '\0';

			ret = len;
			break;
		default:
			ret = write (fd, (char *) buf, len);
			break;
	}

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;

	//printf ("yeah, sys_write: '%d' '%s' '0x%x'\n", len, buf, buf);
}

void sys_socket (struct regs *r)
{
	int ret = socket (r->r9, r->r10, r->r11);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_connect (struct regs *r)
{
	sockaddr_in *addr = (sockaddr_in *) r->r9;

	/*if (!memtest_data (addr, (void *) proc->data, proc->data_off))
		tty_write (proc->tty, (char *) buf, len);
	else {
//		printf ("hehe: 0x%x+0x%x / '%s' len: %d\n", buf+proc->code, proc->data_off, buf+proc->code, len);
		tty_write (proc->tty, (char *) buf+proc->code, len);
	}	*/
	//printf ("adresa mem: %s - 0x%x\n", addr->sin_addr, addr);
	int ret = connect (r->r10, addr, r->r11);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_malloc (struct regs *r)
{
        r->r8 = (unsigned) malloc ((int) r->r9);

	DPRINT ("malloc (): 0x%x\n", r->r8);
}

void sys_send (struct regs *r)
{
/*	char msg[r->r11+1];
	if (!memcpy_to_user (&msg, (char *) r->r9, r->r11))
		return;*/

	int ret = send ((int) r->r10, (char *) r->r9, (unsigned) r->r11, 0);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_recv (struct regs *r)
{
	unsigned char *msg = (unsigned char *) r->r9;

	int ret = recv ((int) r->r10, (char *) msg, (unsigned) r->r11, 0);

//	printf ("recv> r->r9: %d, r->r11: %u, ret: %d\n", (int) r->r9, (unsigned) r->r11, ret);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_close (struct regs *r)
{
	close (r->r9);
}

void sys_open (struct regs *r)
{
	unsigned char *pathname = (unsigned char *) r->r9;

	int ret = open ((char *) pathname, (unsigned) r->r10);

//	printf ("%d = sys_open (%s, %u)\n", ret, (char *) pathname, (unsigned) r->r10);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_pcspk (struct regs *r)
{
	dev_t *dev = dev_find ("/dev/pcspk");

	if (dev)
		dev->handler (DEV_ACT_PLAY, (unsigned) r->r9);
}

void sys_usleep (struct regs *r)
{
	usleep ((unsigned) r->r9);
}

void sys_read (struct regs *r)
{
	unsigned fd = r->r10;
	unsigned len = r->r11;

	unsigned char *buf = (unsigned char *) r->r9;

	int ret = 0;

	proc_t *proc = 0;

	switch (fd) {
		case 0:
			break;
		case 1:
			proc = proc_find (_curr_task);
				
			if (!proc)
				break;
				
			if (proc->task != _curr_task)
				break;

			//if (proc->tty != currtty)
			//	return;
			
			ret = tty_read (proc->tty, (char *) buf, len);
			break;
		default:
			ret = read (fd, (char *) buf, len);
			break;
	}

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_time (struct regs *r)
{
	r->r8 = (unsigned) rtc_getcurrtime ();
}

void sys_system (struct regs *r)
{
	unsigned char *cmd = (unsigned char *) r->r9;

	command_parser ((char *) cmd, strlen (cmd));
}

void sys_chdir (struct regs *r)
{
	unsigned char *dir = (unsigned char *) r->r9;

	cd ((char *) dir);
}

void sys_getdir (struct regs *r)
{
	r->r8 = (unsigned) &dir;
}

void sys_procarg (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->task != _curr_task)
		return;

	switch (r->r10) {
		case 0:
			r->r8 = (unsigned) proc->argv;
			break;
		case 1:
			r->r8 = (unsigned) proc->argc;
			break;
		default:
			r->r8 = 0;
	}
}

void sys_signal (struct regs *r)
{
	signal (r->r9, (sighandler_t) r->r10);
}

void sys_mount (struct regs *r)
{
	partition_t *p = partition_find ((char *) r->r9);

	if (p) {
		mount (p, "", (char *) r->r10);
		r->r8 = 1;
	} else
		r->r8 = 0;
}

void sys_kputs (struct regs *r)
{
	int_disable ();

	module_t *kmod = module_find (_curr_task);

	if (!kmod)
		return;

	if (kmod->task != _curr_task)
		return;

	unsigned char *buf = (unsigned char *) r->r9;
	kprintf (buf);

	//kprintf ("yeah, sys_kputs: '%s' '0x%x'\n", buf, buf);

	int_enable ();
}

void sys_bind (struct regs *r)
{
	int ret = bind (r->r10, r->r9, r->r11);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_listen (struct regs *r)
{
	int ret = listen (r->r9, r->r10);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_accept (struct regs *r)
{
	int ret = accept (r->r10, r->r9, r->r11);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_fcntl (struct regs *r)
{
	int ret = fcntl (r->r9, r->r10, r->r11);

	int *where;
	int *memptr = (int *) 0x9000;
	where = memptr;
        *where = ret;
}

void sys_gvgafb (struct regs *r)
{
	r->r8 = (unsigned) init_vgafb ();
}

void sys_gcls (struct regs *r)
{
	gcls (r->r9);
}

void sys_gpixel (struct regs *r)
{
	gpixel (r->r9, r->r10, r->r11);
}

void sys_rs232read (struct regs *r)
{
	char *where;
	char *memptr = (char *) 0x9000;
	where = memptr;
        *where = rs232_read ();
}

void sys_rs232write (struct regs *r)
{
	rs232_write ((char) r->r9);
}

void sys_gttyexit (struct regs *r)
{
	proc_t *proc = proc_find (_curr_task);

	if (!proc)
		return;

	if (proc->task != _curr_task)
		return;

	tty_change (proc->tty);
}

void sys_gexit (struct regs *r)
{
	gexit ();
}

extern unsigned char vgagui;
extern tty_t *gtty;
void sys_gttyinit (struct regs *r)
{
	gtty_init ();

	if (!gtty)
		return;

	r->r8 = (unsigned) &gtty->screen;
}

void sys_mkdir (struct regs *r)
{
	unsigned char *dir = (unsigned char *) r->r9;

	mkdir ((char *) dir);
}

void sys_free (struct regs *r)
{
        free ((void *) r->r9);

	DPRINT ("free (): 0x%x\n", r->r9);
}

void sys_realloc (struct regs *r)
{
        r->r8 = (unsigned) realloc ((void *) r->r9, (size_t) r->r10);

	DPRINT ("realloc (): 0x%x\n", r->r8);
}

void syscall_handler (struct regs *r)
{
	switch (r->r8) {
		case 1:
			return sys_exit (r);
		case 2:
			return sys_getch (r);
		case 3:
			return sys_sleep (r);
		case 4:
			return sys_putch (r);
		case 5:
			return sys_color (r);
		case 6:
			return sys_cls (r);
		case 7:
			return sys_getkey (r);
		case 8:
			return sys_gotoxy (r);
		case 9:
			return sys_fork (r);
		case 10:
			return sys_schedule (r);
		case 11:
			return sys_write (r);
		case 12:
			return sys_socket (r);
		case 13:
			return sys_connect (r);
		case 14:
			return sys_malloc (r);
		case 15:
			return sys_send (r);
		case 16:
			return sys_recv (r);
		case 17:
			return sys_close (r);
		case 18:
			return sys_open (r);
		case 19:
			return sys_pcspk (r);
		case 20:
			return sys_usleep (r);
		case 21:
			return sys_read (r);
		case 22:
			return sys_time (r);
		case 23:
			return sys_system (r);
		case 24:
			return sys_chdir (r);
		case 25:
			return sys_getdir (r);
		case 26:
			return sys_procarg (r);
		case 27:
			return sys_signal (r);
		case 28:
			return sys_mount (r);
		case 29:
			return sys_kputs (r);
		case 30:
			return sys_bind (r);
		case 31:
			return sys_listen (r);
		case 32:
			return sys_accept (r);
		case 33:
			return sys_fcntl (r);
		case 34:
			return sys_gvgafb (r);
		case 35:
			return sys_gcls (r);
		case 36:
			return sys_gpixel (r);
		case 37:
			return sys_rs232read (r);
		case 38:
			return sys_rs232write (r);	
		case 39:
			return sys_gttyexit (r);
		case 40:
			return sys_gexit (r);
		case 41:
			return sys_gttyinit (r);
		case 42:
			return sys_mkdir (r);
		case 43:
			return sys_free (r);
		case 44:
			return sys_realloc (r);
	}
}
