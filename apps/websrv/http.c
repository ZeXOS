/*
 *  ZeX/OS
 *  Copyright (C) 2007  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2008  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *  Copyright (C) 2009  Tomas 'ZeXx86' Jedrzejek (zexx86@zexos.org)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#ifdef LINUX
# include <dirent.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/param.h>
#endif

#include "net.h"
#include "client.h"

typedef struct {
	char *name;
	unsigned attrib;
	unsigned char next;
	unsigned char unused;
} __attribute__ ((__packed__)) dirent_t;

#define VFS_FILEATTR_FILE	0x1

typedef struct web_context {
	struct web_context *next, *prev;

	char *name;
	char *page;
	unsigned page_len;
} web_t;

web_t web_list;

web_t *web_find (char *name)
{
 	web_t *web;
	for (web = web_list.next; web != &web_list; web = web->next)
		if (!strcmp (web->name, name))
			return web;
		
	return 0;
}

web_t *web_register (char *name)
{
	unsigned page_len = 0;

	char *page = (char *) malloc (sizeof (char) * 80);

	if (!page)
		return 0;

	/*
	HTTP/1.0 200 OK
	Content-Type: text/html
	Content-Length: 1354
	*/

	// http header
	memcpy (page, "HTTP/1.0 200 OK\n", 16);
	memcpy (page+16, "Content-Type: text/html\n", 24);
	memcpy (page+40, "Content-Length: ", 16);
	memcpy (page+56, "     ", 5);
	page[61] = '\n';
	page[62] = '\n';

	page_len = 63;

	FILE *f = fopen (name, "r");

	if (!f) {
		printf ("error -> file '%s' not found\n", name);
		return 0;
	}

	fseek (f, 0, SEEK_END);
	unsigned flen = ftell (f);
	fseek (f, 0, SEEK_SET);

	page = (char *) realloc ((void *) page, sizeof (char) * (flen + 80));

	if (!page)
		return 0;

	fread (page+page_len, flen, 1, f);

	page_len += flen;

	page[page_len] = '\0';

	char num[11];
	memset (num, 0, 11);
	sprintf (num, "%d", page_len-63);	// convert int to char *
	unsigned num_len = strlen (num);

	memcpy (page+56, num, num_len);

	fclose (f);
  
	web_t *web = (web_t *) malloc (sizeof (web_t));

	if (!web)
		return 0;

	web->name = strdup (name);
	web->page = page;
	web->page_len = page_len;

	/* add into list */
	web->next = &web_list;
	web->prev = web_list.prev;
	web->prev->next = web;
	web->next->prev = web;

	printf ("> adding to cache: '%s'\n", name);
	
	return web;
}

dirent_t *getdir ()
{
	dirent_t *dirent;
  
	asm volatile (
		"movl $25, %%eax;"
	     	"int $0x80;"
		"movl %%eax, %0;"
		: "=g" (dirent) :: "%eax");
		
	return dirent;
}

int http_loadfiles ()
{
#ifndef LINUX
	dirent_t *dir = getdir ();

	if (!dir)
		return 0;

	unsigned id;

	for (id = 0; dir[id].next; id ++) {
		if (dir[id].attrib & VFS_FILEATTR_FILE) {
			if (strstr (dir[id].name, ".htm"))
				if (!web_register (dir[id].name))
					return 0;
		}
	}
#else
	struct dirent *entry;          /* Only used as a pointer - see dirent.h */
	struct stat statbuf;           /* So we can check which files are directories. */
	char *pathname = ".";

	/* Open a directory for directory reads. */
	DIR *directory = opendir (pathname);
	
	if (!directory) {
		printf ("ERROR -> cannot open %s\n", pathname);
		return 0;
	}

	while ((entry = readdir (directory))) {
		/* Skip if . or .. */
		if (!strcmp (entry->d_name, ".") || !strcmp (entry->d_name, ".."))
			continue;

		/* stat it to see if it is a regular file. */
		int retval = stat (entry->d_name, &statbuf);
		
		if (retval == 0 && statbuf.st_mode & S_IFREG)
			if (strstr (entry->d_name, ".htm"))
				if (!web_register (entry->d_name))
					return 0;

	}
	
	closedir (directory);
#endif
	
	return 1;
}

int http_init ()
{
  	web_list.next = &web_list;
	web_list.prev = &web_list;

	if (!http_loadfiles ())
		return 0;
	
	return 1;
}

int http_transfer (client_t *c)
{
	web_t *web = web_find (strlen (c->page) == 0 ? "i.htm" : c->page);
  
	if (web) {
		printf ("> sending '%s' web page\n", web->name);

		send_to_socket (c->fd, web->page, web->page_len);
	} else
		printf ("> web page '%s' not found\n", c->page);
		
	c->state = CLIENT_STATE_DONE;

	return 1;
}

int http_destroy ()
{
 	web_t *web;
	for (web = web_list.next; web != &web_list; web = web->next)
		free (web->page);

	return 1;
}
